# README #

### What is this repository for? ###

Visual Composer (VC)

## Requirements  
| Framework                                                                        |Version  |License             |Note 																	|
|--------------------------------------------------------------------------------------------|---------|--------------------|-------------------------------------------------------------------------|
|[Java SE Development Kit](https://www.java.com/it/download/help/index_installing.xml?j=7)|7.0|[Oracle Binary Code License](http://www.oracle.com/technetwork/java/javase/terms/license/index.html)||
|[postgreSQL](https://www.postgresql.org/download/)|9.3|[PostgreSQL License](https://www.postgresql.org/about/licence/)||
|[NodeJS](https://nodejs.org)|0.10.25|[NodeJS License](https://raw.githubusercontent.com/nodejs/node/master/LICENSE)|
|[Apache Cordova](https://cordova.apache.org/)|6.5.0|[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|

## Libraries

VC is based on the following software libraries and frameworks.

|Framework                           |Version       |Licence                                      |
|------------------------------------|--------------|---------------------------------------------|
|[Ace](https://github.com/ajaxorg/ace) | 1.1.3 |[BSD](https://github.com/ajaxorg/ace/blob/master/LICENSE)|
|[Apache Abdera2](https://cwiki.apache.org/confluence/display/ABDERA/Abdera2) | 2.0 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Commons Fileupload](https://commons.apache.org/proper/commons-fileupload/) | 1.3 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Commons IO](https://commons.apache.org/proper/commons-io/) | 2.4 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Freemarker](https://freemarker.apache.org/) | 2.3.25-incubating |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache HttpComponents](https://hc.apache.org/) | 4.2.5 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Log4j](http://logging.apache.org/log4j/1.2/) | 1.2.15 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[BoneCP](https://jolbox.com/) | 0.8.0.RELEASE |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[canvg](https://github.com/canvg/canvg) | ??? |[MIT](https://opensource.org/licenses/mit-license)|
|[cglib](https://github.com/cglib/cglib) | 2.2 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[domReady](https://github.com/requirejs/domReady) | 2.0.1 |[MIT](https://github.com/requirejs/domReady/blob/master/LICENSE)|
|[doT](http://olado.github.io/doT/index.html) | 1.0.0 |[MIT](https://opensource.org/licenses/mit-license)|
|[Draw2d](http://www.draw2d.org/draw2d/) | ??? |GPL2|
|[Encache](http://www.ehcache.org/) | 2.6.5 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[FasterXML Jackson Databind](https://github.com/FasterXML/jackson-databind) | 2.4.0 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[FasterXML Jackson Dataformat CSV](https://github.com/FasterXML/jackson-dataformat-csv) | 2.4.0 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[GMAP3 Plugin for jQuery](http://gmap3.net/) | 6.0.0 |[GPL3](http://www.gnu.org/licenses/gpl.html)|
|[Groovy](https://hc.apache.org/) | 2.1.5 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Guava](https://github.com/google/guava) | 14.0.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Hammer.JS](http://hammerjs.github.io/) | 2.0.8 |[MIT](https://opensource.org/licenses/mit-license)|
|[Handlebars.JS](http://handlebarsjs.com/) | 1.3.0 |[MIT](https://opensource.org/licenses/mit-license)|
|[Hibernate Core](http://hibernate.org/orm/) | 4.2.2.Final |[LGPL](https://github.com/hibernate/hibernate-orm/blob/master/lgpl.txt) |
|[Hibernate EntityManager](http://hibernate.org/orm/) | 4.2.2.Final |[LGPL](https://github.com/hibernate/hibernate-orm/blob/master/lgpl.txt) |
|[Hibernate Validator](http://hibernate.org/validator/) | 5.0.1.Final |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[html2canvas](http://html2canvas.hertzen.com/) | 0.4.1 |[MIT](https://opensource.org/licenses/mit-license)|
|[HTML Clean for jQuery](http://www.antix.co.uk) | 1.3.1 |[BSD](http://www.opensource.org/licenses/bsd-license)|
|[Jayway JsonPath](https://github.com/json-path/JsonPath) | 0.9.0 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Jersey Client](https://jersey.github.io/) | 1.19 |[CDDL+GPL](https://jersey.github.io/license.html) |
|[Jersey Core](https://jersey.github.io/) | 1.19 |[CDDL+GPL](https://jersey.github.io/license.html) |
|[jQuery](https://jquery.org/) | 2.1.1 |[jQuery License](https://jquery.org/license/) |
|[jQuery UI](http://jqueryui.com) | 1.10.3 |[MIT](https://opensource.org/licenses/mit-license)|
|[jQuery contextMenu](http://medialize.github.com/jQuery-contextMenu/) | git-master |[MIT](https://opensource.org/licenses/mit-license)|
|[jQuery Cookie Plugin](https://github.com/carhartl/jquery-cookie) | 1.4.1 |[MIT](https://opensource.org/licenses/mit-license)|
|[jQuery jsTree](https://www.jstree.com/) | 1.0-rc3 |[MIT](https://opensource.org/licenses/mit-license)|
|[jQuery Migrate](https://github.com/jquery/jquery-migrate) | 1.2.1 |[jQuery License](https://jquery.org/license/)|
|[jQuery MiniColors](https://github.com/claviska/jquery-minicolors) | ??? |[MIT](https://opensource.org/licenses/mit-license)|
|[jQuery Pagination Plugin](https://github.com/gbirke/jquery_pagination) | 1.2 |[GPL2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt)|
|[jQuery QueryBuilder](http://querybuilder.js.org/) | 2.3.0 |[MIT](https://opensource.org/licenses/mit-license)|
|[jQuery Select2](https://select2.org/) | 3.4.3 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|
|[JsonML](http://www.jsonml.org/) | ??? |[MIT](http://jsonml.org/license)|
|[JSONPath](http://goessner.net/articles/JsonPath/) | 0.8.0 |[MIT](https://opensource.org/licenses/mit-license)|
|[JSON Schema View](https://github.com/mohsen1/json-schema-view-js) | 0.4.1 |[MIT](https://opensource.org/licenses/mit-license)|
|[jsoup](https://jsoup.org/) | 1.8.3 |[MIT](https://jsoup.org/license) |
|[Knockout](http://knockoutjs.com/) | 3.1.0 |[MIT](https://opensource.org/licenses/mit-license)|
|[List.js](http://listjs.com/) | 1.5.0 |[MIT](https://opensource.org/licenses/mit-license)|
|[Materialize](http://materializecss.com)| 0.97.7 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[PathFinding.js](https://github.com/qiao/PathFinding.js)| ??? |[MIT](https://opensource.org/licenses/mit-license) |
|[PostgreSQL JDBC Driver](https://jdbc.postgresql.org/) | 9.1-901.jdbc4 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Raphael](https://github.com/DmitryBaranovskiy/raphael)| 2.1.0 |[MIT](http://dmitrybaranovskiy.github.io/raphael/license.html) |
|[RequireJS](https://github.com/requirejs/requirejs)| 2.1.14 |[MIT](https://opensource.org/licenses/mit-license) |
|[RGB Color](http://www.phpied.com/rgb-color-parser-in-javascript/)| ??? |None|
|[Rythm Template Engine](http://rythmengine.org/) | 1.0.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Rythm Template Engine for Spring Web](https://github.com/lawrence0819/spring-webmvc-rythm) | 1.4.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[ScribeJava](https://github.com/scribejava/scribejava) | 1.3.5 |[MIT License](https://www.slf4j.org/license.html) |
|[Shifty](https://github.com/jeremyckahn/shifty)| 0.6.1 |[MIT](https://opensource.org/licenses/mit-license) |
|[SLF4J API](http://www.slf4j.org) | 1.6.6 |[MIT License](https://www.slf4j.org/license.html) |
|[SLF4J LOG4J-12 Binding](http://www.slf4j.org) | 1.6.6 |[MIT License](https://www.slf4j.org/license.html) |
|[SOA Model Core](https://github.com/membrane/soa-model) | 1.4.0.2 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Spring Framework](https://spring.io/) | 3.2.5.RELEASE |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Swagger Parser](https://github.com/swagger-api/swagger-parser) | 1.0.21 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[vkBeautify](http://www.eslinstructor.net/vkbeautify/)| 0.99.00.beta |MIT+GPL|
|[XMLBeans](https://xmlbeans.apache.org/) | 2.6.0 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |

## Dependencies
|Library                             |Project       |License                                      |
|------------------------------------|--------------|---------------------------------------------|
|MetamodelParser-3.11.4.jar| welive-metamodel-parser | [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)|

### How do I get set up? ###

For details about configuration/installation you can see "D2.12 - OPEN SERVICE LAYER V2"

Remember to set the username/password for the Basic Authentication on the file src\main\webapp\WEB-INF\spring\config.properties

### Who do I talk to? ###

ENG team, Giovanni Aiello, Ivan Ligotino

### Copying and License

This code is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
