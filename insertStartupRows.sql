INSERT INTO profile (id, created_at, email, first_name, last_name, updated_at) VALUES (1, '2013-07-05 12:29:26.77', 'admin@admin.com', 'admin', 'admin', '2013-07-05 12:29:26.77');
INSERT INTO account (id, created_at, email, expire, provider, psw, role, status, token, updated_at, validated_oauth_id, profile_id) VALUES (1, '2013-07-05 12:29:26.77', 'admin@admin.com', NULL, 'vme', '21232f297a57a5a743894a0e4a801fc3', 1, 2, NULL, '2014-11-20 12:04:09.483', NULL, 1);
INSERT INTO authprovidertype (type, description) VALUES ('1.0', 'OAuth 1.0a type');
INSERT INTO authprovidertype (type, description) VALUES ('2.0', 'OAuth 2.0 type');
INSERT INTO authprovidertype (type, description) VALUES ('BASIC', 'Basic Authentication');
INSERT INTO category (id, description, fk_id) VALUES (0, 'Categories', NULL);
INSERT INTO category (id, description, fk_id) VALUES (1, 'Meteo', 0);
INSERT INTO category (id, description, fk_id) VALUES (2, 'Financial', 0);
INSERT INTO category (id, description, fk_id) VALUES (3, 'Generic', 0);
INSERT INTO category (id, description, fk_id) VALUES (4, 'Social', 3);
INSERT INTO category (id, description, fk_id) VALUES (5, 'Facebook', 4);
INSERT INTO category (id, description, fk_id) VALUES (6, 'Twitter', 4);
INSERT INTO category (id, description, fk_id) VALUES (7, 'LinkedIn', 4);
INSERT INTO category (id, description, fk_id) VALUES (8, 'Google+', 4);


SELECT setval('profile_id_seq', 1);
SELECT setval('account_id_seq', 1);
SELECT setval('category_id_seq', 8);



