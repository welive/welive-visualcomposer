#!/bin/bash

PROJECT_DIR=$1
TARGET_DIR=$2
APK_PATH=$1"/platforms/android/build/outputs/apk/android-release-unsigned.apk"

cd $PROJECT_DIR
export ANDROID_HOME=/usr/lib/android-sdk-linux
sudo -E ./platforms/android/cordova/build --release && cp $APK_PATH $TARGET_DIR
