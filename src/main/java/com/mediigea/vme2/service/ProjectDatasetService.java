package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.ODSResource;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectDataset;

@Transactional
public interface ProjectDatasetService {
	
	public void create(int idProject, int idResource);
	
	public void deleteAllByProject(int idProject);
	
	public List<ProjectDataset> getAllByProject(int idProject);

	public List<ODSResource> getAllDatasetsByProject(int idProject);
	
	public List<Project> findMashupsByResourceId(Integer id);
}
