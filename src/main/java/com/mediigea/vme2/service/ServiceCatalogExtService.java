package com.mediigea.vme2.service;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.ServiceCatalogExt;

@Transactional
public interface ServiceCatalogExtService 
{
	public ServiceCatalogExt add(ServiceCatalogExt service);
	public ServiceCatalogExt add(int id);
	public ServiceCatalogExt add(int id, String endpoint);	
	public ServiceCatalogExt update(ServiceCatalogExt service);	
	public void delete(int id);
	
	public ServiceCatalogExt getById(int id);
	public ServiceCatalogExt getByEndpoint(String endpoint);
}
