package com.mediigea.vme2.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dto.InnerProcessDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.exceptions.SelectShapeException;


public class InnerProcessServiceImpl implements InnerProcessService {

	private static final Logger logger = LoggerFactory.getLogger(InnerProcessServiceImpl.class);
	
	@Autowired
	private ProjectService projectService;

	@Autowired
	private ProjectInnerProcessService projectInnerProcessService;
	
	@Autowired
	private ObjectMapperExt objectMapper;	
	
	private TypeReference<List<ServiceParameterDTO>> typeRef = new TypeReference<List<ServiceParameterDTO>>(){};
	
	@Override
	public List<InnerProcessDTO> findAllByProfileAsDTO(Profile p, int project_id) throws SelectShapeException {
		//List<InnerProcess> entityList = findAllByProfile(p);
		
		List<Project> entityList = projectService.findAllByProfile(p);
		
		List<InnerProcessDTO> dtoList = new ArrayList<InnerProcessDTO>();
		
		for(Project entity : entityList){
			
			if(entity.getId()!=project_id){
				//Restituisce tutte gli inner process tranne quella corrispondente eventualmente
				//al progetto di mashup che si sta editando				
				try
				{
					InnerProcessDTO dto = new InnerProcessDTO();
					dto.setId(entity.getId());
					dto.setName(entity.getName());
					dto.setDescription(entity.getDescription());	
					
					List<ServiceParameterDTO> params = this.projectService.getProjectInputParams(entity.getId());				
					
					dto.setParameters(params);				
					
					dtoList.add(dto);
				}
				catch(Exception e)
				{
					continue;
				}
			}
		}
		
		return dtoList;
	}
	
//	private List<InnerProcess> findAllByProfile(Profile p) {
//
//		if (p == null)
//			return null;
//		
//		String hql = " from InnerProcess b "
//				+ "join fetch b.mashup "				
//				+ "join fetch b.mashup.profileProjects pp " 
//				+ "where pp.pk.profile.id=:profileId";
//		
//		HashMap<String, Object> parameters = new HashMap<String, Object>();
//		parameters.put("profileId", p.getId());
//
//		List<InnerProcess> result = findAll(hql, parameters);
//		
//		return result;
//	}
		
//	private List<InnerProcess> findAll(String hql, HashMap<String, Object> parameters) {
//
//		if (hql == null || hql.isEmpty())
//			return innerProcessDAO.getAll();
//
//		return innerProcessDAO.getAllBy(hql, parameters);
//	}	
	
//	@Override
//	public InnerProcessDTO getAsDTO(int id){
//		InnerProcess entity = innerProcessDAO.getById(id);
//		
//		return this.readDTO(entity);
//	}
//	
//	private InnerProcessDTO readDTO(InnerProcess entity){
//		InnerProcessDTO dto = null;			
//		
//		try {
//			dto = new InnerProcessDTO();
//			dto.setId(entity.getMashup().getId());
//			dto.setName(entity.getName());
//			dto.setDescription(entity.getDescription());	
//			
//			List<ServiceParameterDTO> params = objectMapper.readValue(entity.getDefinition(), typeRef);
//			
//			dto.setParameters(params);				
//			
//		} catch (JsonParseException e) {
//			logger.error(e.getStackTrace().toString());
//		} catch (JsonMappingException e) {
//			logger.error(e.getStackTrace().toString());
//		} catch (IOException e) {
//			logger.error(e.getStackTrace().toString());
//		}		
//		
//		return dto;
//	}
}
