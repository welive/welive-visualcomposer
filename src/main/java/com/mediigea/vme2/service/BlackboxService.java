package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.dto.BlackboxDTO;
import com.mediigea.vme2.entity.Blackbox;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.exceptions.UsedBlackboxException;

/**
 * Interface to define operations about Blackboxes management
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface BlackboxService {

	Blackbox addBlackbox(int projectId);

	void deleteBlackbox(int blackboxId) throws UsedBlackboxException;
	
	public List<BlackboxDTO> findAllByProfileAsDTO(Profile p, int project_id);

	BlackboxDTO getAsDTO(int id);
}
