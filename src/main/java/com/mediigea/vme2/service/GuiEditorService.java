package com.mediigea.vme2.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mediigea.vme2.dto.GadgetPrefsDTO;
import com.mediigea.vme2.dto.GuiFileResourceDTO;
import com.mediigea.vme2.dto.GuiProjectDTO;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProfileGuiProject;
import com.mediigea.vme2.entity.ProfileProject;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.exceptions.WriteResourceException;
import com.mediigea.vme2.service.GuiEditorServiceImpl.ResourceContentType;

/**
 * Interface to define operations about Gui (Mockup) management
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface GuiEditorService {

	public GuiProject findProject(Integer id);
		
	public GuiProject addProject(GuiProject project);
	public GuiProject addProject(GuiProject project, Profile owner);
	
	public GuiProject updateProject(GuiProject a, boolean updateDate);
		
	public void saveFile(Integer projectId, String filename, String filecontent, ResourceContentType type) throws WriteResourceException;
	
	public GuiFileResourceDTO writeResources(Integer projectId, MultipartFile content) throws WriteResourceException;
	public List<GuiFileResourceDTO> getResourcesByProject(int idProject, ResourceContentType type);
	public void removeResources(Integer resourceId) throws WriteResourceException;
	
	public List<GuiProject> findAll(String hql, HashMap<String, Object> parameters);
	public List<GuiProject> findAllByProfile(Profile p);
	
	public List<GuiProject> findAllByWorkgroup(Workgroup w);
	
	public Profile findProjectOwner(Integer projectId);
	
	public GuiFileResourceDTO getDefaultHtmlFile(Integer id) throws IOException;
	public GuiFileResourceDTO getDefaultPublicHtmlFile(Integer id) throws IOException;
	
	public List<String> getPaletteComponent();
	public void deleteProject(Integer projectId) throws IOException;

	void updateGadgetPrefs(Integer projectId, GadgetPrefsDTO prefs);

	InputStream getHTMLArchive(Integer projectId) throws IOException;
	
	InputStream getCordovaArchive(Integer projectId) throws IOException;

	GuiFileResourceDTO newHtml(Integer projectId, String filename)
			throws WriteResourceException;

	List<GuiProject> searchProject(Profile p, String criteria);

	public void memberAdd(Integer projectId, Integer profileId);
	
	public void memberAddWithRole(Integer projectId, Integer profileId, Integer role);

	public void memberRemove(Integer projectId, Integer profileId);

	public List<Profile> findProjectMembers(GuiProject project);
	
	public List<ProfileGuiProject> findProfileGuiProject(GuiProject project);

	public List<Comment> findProjectComments(GuiProject p, int limit);

	public List<Comment> findProjectComments(GuiProject p);

	List<GuiProjectDTO> findAllProjectDTOByProfile(Profile p);
	
	public void mashupProjectAdd(Integer projectId, Integer mashupProjectId);
	
	public void mashupRemove(Integer projectId, Integer mashupProjectId);
	public List<Project> findMashupProjects(GuiProject project);

	public GuiProject removeAllMashup(GuiProject project);

	public void addAllMashup(GuiProject project, String mashupProjectList);

}

