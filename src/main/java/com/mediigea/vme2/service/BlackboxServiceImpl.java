package com.mediigea.vme2.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dao.BlackboxDAO;
import com.mediigea.vme2.dto.BlackboxDTO;
import com.mediigea.vme2.dto.InputField;
import com.mediigea.vme2.dto.InputShapeDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.dto.UD_InputShapeDTO;
import com.mediigea.vme2.entity.Blackbox;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectBlackbox;
import com.mediigea.vme2.exceptions.SelectShapeException;
import com.mediigea.vme2.exceptions.UsedBlackboxException;
import com.mediigea.vme2.util.VmeConstants;


public class BlackboxServiceImpl implements BlackboxService {

	private static final Logger logger = LoggerFactory.getLogger(BlackboxServiceImpl.class);
	
	@Autowired
	private ProjectService projectService;

	@Autowired
	private ProjectBlackboxService projectBlackboxService;
	
	@Autowired
	private ObjectMapperExt objectMapper;
	
	@Autowired
	private BlackboxDAO blackboxDAO;
	
	private TypeReference<List<ServiceParameterDTO>> typeRef = new TypeReference<List<ServiceParameterDTO>>(){};
	
	@Override
	public Blackbox addBlackbox(int projectId) {
		Blackbox blackbox = null;
		
		try {
			Project proj = this.projectService.findProject(projectId);
			
			List<ServiceParameterDTO> serviceParamsList = this.projectService.getProjectInputParams(projectId);			
			
			String jsonInputShapes = objectMapper.writeValueAsString(serviceParamsList);
			
			blackbox = new Blackbox();
			
			blackbox.setProject_id(projectId);
			blackbox.setName(proj.getName());
			blackbox.setDescription(proj.getDescription());
			blackbox.setDefinition(jsonInputShapes);
			blackbox.setMashup(proj);
			
			blackboxDAO.create(blackbox);
			
		} catch (SelectShapeException e) {
			logger.error("Error SelectShapeException:" + e.getStackTrace().toString());
		} catch (JsonProcessingException e) {
			logger.error("Error JsonProcessingException:" + e.getStackTrace().toString());
		}
		
		return blackbox;
	}
	
	@Override	
	public void deleteBlackbox(int blackboxId) throws UsedBlackboxException {
		Blackbox entity = blackboxDAO.getById(blackboxId);
		
		List<ProjectBlackbox> list = projectBlackboxService.getAllByBlackbox(blackboxId);
		
		if(!list.isEmpty()){
			throw new UsedBlackboxException();
		}
		
		blackboxDAO.delete(entity);			
	}	
	
	@Override
	public List<BlackboxDTO> findAllByProfileAsDTO(Profile p, int project_id) {
		List<Blackbox> entityList = findAllByProfile(p);
		
		List<BlackboxDTO> dtoList = new ArrayList<BlackboxDTO>();
		
		for(Blackbox entity : entityList){
			
			if(entity.getMashup().getId()!=project_id){
				//Restituisce tutte le blackbox tranne quella corrispondente eventualmente
				//al progetto di mashup che si sta editando
				BlackboxDTO dto = this.readDTO(entity);	
			
				dtoList.add(dto);
			}
		}
		
		return dtoList;
	}
	
	private List<Blackbox> findAllByProfile(Profile p) {

		if (p == null)
			return null;
		
		String hql = " from Blackbox b "
				+ "join fetch b.mashup "				
				+ "join fetch b.mashup.profileProjects pp " 
				+ "where pp.pk.profile.id=:profileId";
		
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("profileId", p.getId());

		List<Blackbox> result = findAll(hql, parameters);
		
		return result;
	}
		
	private List<Blackbox> findAll(String hql, HashMap<String, Object> parameters) {

		if (hql == null || hql.isEmpty())
			return blackboxDAO.getAll();

		return blackboxDAO.getAllBy(hql, parameters);
	}	
	
	@Override
	public BlackboxDTO getAsDTO(int id){
		Blackbox entity = blackboxDAO.getById(id);
		
		return this.readDTO(entity);
	}
	
	private BlackboxDTO readDTO(Blackbox entity){
		BlackboxDTO dto = null;			
		
		try {
			dto = new BlackboxDTO();
			dto.setId(entity.getMashup().getId());
			dto.setName(entity.getName());
			dto.setDescription(entity.getDescription());	
			
			List<ServiceParameterDTO> params = objectMapper.readValue(entity.getDefinition(), typeRef);
			
			dto.setParameters(params);				
			
		} catch (JsonParseException e) {
			logger.error(e.getStackTrace().toString());
		} catch (JsonMappingException e) {
			logger.error(e.getStackTrace().toString());
		} catch (IOException e) {
			logger.error(e.getStackTrace().toString());
		}		
		
		return dto;
	}
}
