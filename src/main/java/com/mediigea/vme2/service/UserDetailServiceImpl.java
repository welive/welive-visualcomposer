package com.mediigea.vme2.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.swagger.util.Json;
import it.eng.PropertyGetter;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.auth.AuthenticationException;
import com.mediigea.vme2.controller.WebServiceController;
import com.mediigea.vme2.dao.AccountDAO;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.util.VmeConstants;

public class UserDetailServiceImpl implements AuthenticationUserDetailsService{
	@Autowired
	private AccountDAO accountDAO;
	
	@Autowired
	private AccountService accountService;	
	
	@Autowired
	private ProfileService profileService;
	
	private static final Logger logger = LoggerFactory.getLogger(UserDetailServiceImpl.class);

	@Override
	public UserDetails loadUserDetails(Authentication authenticationToken) throws UsernameNotFoundException 
	{		
		Account a; 
		
		String profileName = authenticationToken.getName();
		
		if(profileName.contains("#")){
		
			String oauthId = profileName.substring(profileName.indexOf("#")+1);			
			try {
				a = this.accountService.verifyAccount(oauthId);
			} catch (AuthenticationException e) {
				throw new UsernameNotFoundException(e.getMessage(), e);
			}
		} else {
			
			a = accountDAO.getByUserName(authenticationToken.getName());
		}
		
		AccountDTO dto = new AccountDTO();
		
		dto.setEmail(a.getEmail());
		dto.setId(a.getId());
		dto.setProfile(a.getProfile());
		dto.setPsw(a.getPsw());
		dto.setStatus(a.getStatus());
		dto.setRole(a.getRole());
		
		accountService.loginUser(a);
		
		Profile loggedUser = a.getProfile();
		
		// IVAN
		// update user info
		try
		{			
			String apiUrl = PropertyGetter.getProperty("platform.getuserroles").replace("!CC_USER_ID!", Integer.toString(loggedUser.getCcUserId()));
			
			RestTemplate restTemplate = new RestTemplate();
			
			HttpHeaders headers = new HttpHeaders();
			
			if(Boolean.parseBoolean(PropertyGetter.getProperty("basic.auth.enabled")))
			{
				String plainCreds = PropertyGetter.getProperty("welive.tools.username") + ":" + PropertyGetter.getProperty("welive.tools.password");
				byte[] plainCredsBytes = plainCreds.getBytes();
				byte[] base64CredsBytes = Base64.encode(plainCredsBytes);
				String base64Creds = new String(base64CredsBytes);
				
				headers.add("Authorization", "Basic " + base64Creds);
			}
			
			HttpEntity<String> request = new HttpEntity<String>(headers);
			ResponseEntity<ObjectNode> response = restTemplate.exchange(apiUrl, HttpMethod.POST, request, ObjectNode.class);
			
			ObjectNode res = response.getBody();
			
			if(res.has("error") && res.get("error").asBoolean() == true)
			{
				throw new Exception(res.get("message").asText());
			}
			
			boolean isDeveloper = res.get("isDeveloper").asBoolean();
			String role = res.get("role").asText();
			
			
			loggedUser.setLevel(isDeveloper ? VmeConstants.USER_LEVEL_DEVELOPER_ADVANCED : VmeConstants.USER_LEVEL_MOCKUPDESIGNER);
			loggedUser.setExtRole(role);
			
			profileService.updateProfile(loggedUser);
		}
		catch(Exception e)
		{
			logger.error("Can't update user info due to: " + e.getMessage());
			//e.printStackTrace();
		}
		
		// IVAN
		// notify Logging BB		
		if(Boolean.parseBoolean(PropertyGetter.getProperty("logbb.notify.enabled")))
		{
			try 
			{
				RestTemplate restTemplate = new RestTemplate();	
				
				String url = PropertyGetter.getProperty("logbb.notify.serviceUrl");
				
				HttpHeaders headers = new HttpHeaders();
			    headers.setContentType(MediaType.APPLICATION_JSON);
			    
			    if(Boolean.parseBoolean(PropertyGetter.getProperty("basic.auth.enabled")))
				{
					String plainCreds = PropertyGetter.getProperty("welive.tools.username") + ":" + PropertyGetter.getProperty("welive.tools.password");
					byte[] plainCredsBytes = plainCreds.getBytes();
					byte[] base64CredsBytes = Base64.encode(plainCredsBytes);
					String base64Creds = new String(base64CredsBytes);
					
					headers.add("Authorization", "Basic " + base64Creds);
				}
			    
			    JSONObject custom_attr = new JSONObject();
			    JSONObject body = new JSONObject();
			    
			    custom_attr.put("userid", loggedUser.getCcUserId() + "");
				custom_attr.put("pilot", getLoggingBBPilotByLiferayPilot(loggedUser.getPilotId()));
				
				body.put("appId", "vc");
			    body.put("timestamp", new Date().getTime());
			    body.put("type", "DeveloperIsActive");
			    body.put("msg", "User '" + loggedUser.getEmail() + "' logged in Visual Composer");
			    body.put("custom_attr", custom_attr);	
			    
			    HttpEntity<String> request = new HttpEntity<String>(body.toString(), headers);
			    
			    ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
				
			    logger.debug("Logging BB notified: " + "User '" + loggedUser.getEmail() + "' logged in Visual Composer");
				logger.debug(response.getBody());
			} 
			catch (Exception e) 
			{
				logger.error("Can't notify Logging BB due to: " + e.getMessage());
				//e.printStackTrace();
			}
		}
		
		return dto;
	}
	
	private String getLoggingBBPilotByLiferayPilot(String liferayPilot)
	{		  
	  String val = "";
	  
	  Map<String, String> pilots = new HashMap<String, String>();
	  pilots.put("Bilbao", "Bilbao");
	  pilots.put("Novi Sad", "Novisad");
	  pilots.put("Region on Uusimaa-Helsinki", "Uusimaa");
	  pilots.put("Trento", "Trento");	 
	  
	  val = pilots.get(liferayPilot);  
	  
	  return val;
	}
}
