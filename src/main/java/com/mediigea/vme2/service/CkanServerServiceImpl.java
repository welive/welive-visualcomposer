package com.mediigea.vme2.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dao.CkanServerDAO;
import com.mediigea.vme2.dao.OpenDataResourceDAO;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.dao.ProjectOperationDAO;
import com.mediigea.vme2.dao.RestOperationDAO;
import com.mediigea.vme2.dao.ServiceCatalogDAO;
import com.mediigea.vme2.dao.SoapOperationDAO;
import com.mediigea.vme2.dao.WorkgroupDAO;
import com.mediigea.vme2.dto.OpenDataResourceDTO;
import com.mediigea.vme2.dto.ServiceItemDTO;
import com.mediigea.vme2.dto.ServiceOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.entity.CkanServer;
import com.mediigea.vme2.entity.OpenDataResource;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.parser.WadlParser;
import com.mediigea.vme2.parser.WsdlParser;
import com.mediigea.vme2.rest.RestApplication;
import com.mediigea.vme2.rest.RestPackage;
import com.mediigea.vme2.rest.RestResource;
import com.mediigea.vme2.service.GuiEditorServiceImpl.ResourceContentType;
import com.mediigea.vme2.soap.SoapMethod;
import com.mediigea.vme2.soap.SoapPackage;
import com.mediigea.vme2.util.FormatConverter;
import com.mediigea.vme2.util.VmeConstants;

public class CkanServerServiceImpl implements CkanServerService {	

	private static final Logger logger = LoggerFactory
			.getLogger(CkanServerServiceImpl.class);

	@Autowired
	private CkanServerDAO ckanServerDAO;

	@Autowired
	private RestOperationDAO restDAO;

	@Autowired
	private WorkgroupDAO wokgroupDAO;

	@Autowired
	private ProfileDAO profileDAO;

	@Autowired
	private SoapOperationDAO soapOperationDAO;

	@Autowired
	private RestOperationDAO restOperationDAO;

	@Autowired
	private ProjectOperationDAO projectOperationDAO;

	@Autowired
	private RestOperationService restOperationService;

	@Autowired
	private SoapOperationService soapOperationService;

	@Autowired
	private ObjectMapperExt objectMapper;

	@Override
	public CkanServer setVisibility(Integer id, Integer code) {

		if (id == null || code == null)
			return null;

		CkanServer s = ckanServerDAO.getById(id);

		if (s == null)
			return null;

		s.setVisibility(code);

		ckanServerDAO.update(s);

		return s;
	}	

	/**
	 * @throws IOException
	 * 
	 */
	@Override
	public CkanServer add(String name, String description,
			String url, Profile profile) {

		CkanServer ckanServer = null;

			ckanServer = new CkanServer();
			ckanServer.setCreatedAt(new Date());
			ckanServer.setUpdatedAt(new Date());
			ckanServer.setName(name);
			ckanServer.setProfile(profile);
			ckanServer.setDescription(description);
			ckanServer.setUrl(url);

			ckanServer.setVisibility(
					profile.getAccount().getRole() == VmeConstants.ACCOUNT_ROLE_ADMIN ? VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC
							: VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE);

			ckanServerDAO.create(ckanServer);

		return ckanServer;
	}

//	@Override
//	public List<OpenDataResourceDTO> getOpenDataResourcesDTO(Profile p) {
//
//		List<OpenDataResource> myList = this.findVisibleResources(p.getId());
//		List<OpenDataResource> publicList = this.findPublicResources();
//
//		if (myList == null && publicList == null)
//			return null;
//
//		ArrayList<OpenDataResourceDTO> catalogResult = new ArrayList<OpenDataResourceDTO>();
//
//		for (OpenDataResource odata : myList) {
//
//			OpenDataResourceDTO odataItem = new OpenDataResourceDTO();
//
//			odataItem.setId(odata.getId());
//			odataItem.setName(odata.getName());
//			odataItem.setType(odata.getType());
//			
//			catalogResult.add(odataItem);
//		}
//		for (OpenDataResource odata : publicList) {
//
//			OpenDataResourceDTO odataItem = new OpenDataResourceDTO();
//
//			odataItem.setId(odata.getId());
//			odataItem.setName(odata.getName());
//			odataItem.setType(odata.getType());
//			catalogResult.add(odataItem);
//		}
//		return catalogResult;
//
//	}

	@Override
	public List<CkanServer> getAll() {
		String hql = " from CkanServer s join fetch s.profile p where p.account.role = "
				+ VmeConstants.ACCOUNT_ROLE_ADMIN + " order by s.name ";
		return ckanServerDAO.getAllBy(hql, null);
	}

	@Override
	public List<CkanServer> findPublicCkanServer() {
		String hql = " from CkanServer s where s.visibility="
				+ VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC
				+ " order by s.name ";
		return ckanServerDAO.getAllBy(hql, null);
	}
//
//	@Override
//	public List<OpenDataResource> findVisibleResources(Integer profileId) {
//		String hql = " Select distinct s from OpenDataResource s join fetch s.profile p join p.profileWorkgroups pw "
//				+ " where pw.pk.workgroup.id in (select pw2.pk.workgroup.id from ProfileWorkgroup pw2 where pw2.pk.profile.id = :pId)"
//				+ " and ( s.profile.id = :pId or s.visibility ="
//				+ VmeConstants.SERVICECATALOG_VISIBILITY_WORKGROUP
//				+ " )  order by s.visibility desc, s.name ";
//
//		HashMap<String, Object> params = new HashMap<String, Object>();
//		params.put("pId", profileId);
//		return ckanServerDAO.getAllBy(hql, params);
//	}

	@Override
	public void delete(Integer id) throws Exception {

		try {
			CkanServer s = ckanServerDAO.getById(id);

			ckanServerDAO.delete(s);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public CkanServer getById(Integer id) {

		return ckanServerDAO.getById(id);
	}
}
