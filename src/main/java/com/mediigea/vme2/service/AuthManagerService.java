package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.dto.OAuthServiceInfo;
import com.mediigea.vme2.entity.AuthProviderType;

@Transactional
public interface AuthManagerService {
	public void init();
	public OAuthServiceInfo getService(Integer id);
	public List<AuthProviderType> getAdminAuthProviders();
	public List<AuthProviderType> getUserAuthProviders();
	public void reloadProviders();
}
