package com.mediigea.vme2.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.ODSDatasetDAO;
import com.mediigea.vme2.dao.ODSResourceDAO;
import com.mediigea.vme2.entity.ODSDataset;
import com.mediigea.vme2.entity.ODSResource;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.util.VmeConstants;

public class ODSDatasetServiceImpl implements ODSDatasetService
{
	private static final Logger logger = LoggerFactory.getLogger(ODSDatasetServiceImpl.class);	
	
	@Autowired
	private ODSDatasetDAO odsDatasetDAO;
	
	@Autowired
	private ODSResourceDAO odsResourceDAO;
	
	@Autowired
	private ODSResourceService odsResourceService;

	@Override
	public ODSDataset addODSDataset(ODSDataset dataset, Profile profile) 
	{
		if (dataset == null || profile == null)
			return null;
		
		dataset.setVisibility(VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE);
		dataset.setProfile(profile);
		
		odsDatasetDAO.create(dataset);
		
		return dataset;
	}

	@Override
	public ODSDataset setVisibility(Integer id, Integer code) 
	{
		if (id == null || code == null)
			return null;

		ODSDataset s = odsDatasetDAO.getById(id);

		if (s == null)
			return null;

		s.setVisibility(code);

		odsDatasetDAO.update(s);

		return s;
	}

	@Override
	public ODSDataset updateODSDataset(ODSDataset dataset) 
	{
		if (dataset == null)
			return null;

		dataset = odsDatasetDAO.update(dataset);

		return dataset;
	}

	@Override
	public void deleteODSDataset(Integer id) throws Exception 
	{
		try 
		{
			ODSDataset dataset = odsDatasetDAO.getById(id);

			List<ODSResource> odsResources = odsResourceService.findAllByODSDataset(id);
			
			for (ODSResource resource : odsResources) 
			{	
				odsResourceDAO.delete(resource);
			}

			odsDatasetDAO.delete(dataset);
		} 
		catch (Exception ex) 
		{
			logger.error(ex.getMessage());
			ex.printStackTrace();
			throw ex;
		}
		
	}

	@Override
	public List<ODSDataset> findAll(String hql,	HashMap<String, Object> parameters) 
	{
		if (hql == null || hql.isEmpty())
			return odsDatasetDAO.getAll();

		return odsDatasetDAO.getAllBy(hql, parameters);
	}

	@Override
	public List<ODSDataset> findPublicDatasets() 
	{
		String hql = " from ODSDataset d where d.visibility=" + VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC + " order by d.title ";
		return odsDatasetDAO.getAllBy(hql, null);
	}

	@Override
	public List<ODSDataset> findVisibleDatasets(Integer profileId) 
	{
		String hql = " Select distinct s from ODSDataset s join fetch s.profile p join p.profileWorkgroups pw "
				+ " where pw.pk.workgroup.id in (select pw2.pk.workgroup.id from ProfileWorkgroup pw2 where pw2.pk.profile.id = :pId)"
				+ " and ( s.profile.id = :pId or s.visibility =" + VmeConstants.SERVICECATALOG_VISIBILITY_WORKGROUP + " )  order by s.visibility desc, s.title ";

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("pId", profileId);
		return odsDatasetDAO.getAllBy(hql, params);
	}

	@Override
	public ODSDataset findDataset(Integer id) 
	{
		ODSDataset dataset = odsDatasetDAO.getById(id);
		return dataset;
	}

	@Override
	public List<ODSDataset> findDatasetsByODSId(Integer odsId) 
	{
		String hql = "Select d from ODSDataset d where d.odsdatasetid=:odsId";

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("odsId", odsId);
		
		return odsDatasetDAO.getAllBy(hql, params);
	}

}
