package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.ProjectBlackbox;
import com.mediigea.vme2.entity.ProjectInnerProcess;

/**
 * Interface to define operations about inner processes used inside a mashup project
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface ProjectInnerProcessService {	
	
	public void create(int idProject, int idInnerProcess);
	
	public void deleteAllByProject(int idProject);
	
	public List<ProjectInnerProcess> getAllByProject(int idProject);
	
//	public List<ProjectInnerProcess> getAllByInnerProcess(int idInnerProcess);
}
