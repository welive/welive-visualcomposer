package com.mediigea.vme2.service;

import it.eng.PropertyGetter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dao.CommentDAO;
import com.mediigea.vme2.dao.GuiProjectDAO;
import com.mediigea.vme2.dao.GuiProjectFileDAO;
import com.mediigea.vme2.dao.GuiProjectProjectDAO;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.dao.ProfileGuiProjectDAO;
import com.mediigea.vme2.dao.ProjectDAO;
import com.mediigea.vme2.dao.UnreadCommentDAO;
import com.mediigea.vme2.dto.GadgetPrefsDTO;
import com.mediigea.vme2.dto.GuiFileResourceDTO;
import com.mediigea.vme2.dto.GuiProjectDTO;
import com.mediigea.vme2.dto.ProjectDTO;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.GuiProjectFile;
import com.mediigea.vme2.entity.GuiProjectProject;
import com.mediigea.vme2.entity.GuiProjectProjectId;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProfileGuiProject;
import com.mediigea.vme2.entity.ProfileGuiProjectId;
import com.mediigea.vme2.entity.ProfileProject;
import com.mediigea.vme2.entity.ProfileProjectId;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.UnreadComment;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.exceptions.NotSupportedMimeTypeException;
import com.mediigea.vme2.exceptions.WriteResourceException;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.util.io.ZipHelper;

public class GuiEditorServiceImpl implements GuiEditorService {

	public enum ResourceContentType {
		CSS("text/css", "css"), JAVASCRIPT("application/javascript", "js"), XJAVASCRIPT("application/x-javascript", "js"), TEXTJAVASCRIPT("text/javascript", "js"), HTML("text/html", ""), IMAGE("image", "images");
		private final String type;
		private final String folder;

		// Reverse-lookup map for getting a day from an abbreviation
        private static final List<ResourceContentType> lookup = new ArrayList<ResourceContentType>();
        static {
            for (ResourceContentType d : ResourceContentType.values())
                lookup.add(d);
        }
		
		ResourceContentType(String type, String folder) {
			this.type = type;
			this.folder = folder;
		}

		public String getType() {
			return this.type;
		}
		
		public String getFolder(){
			return this.folder;
		}
		
		public static ResourceContentType get(String type) {
			ResourceContentType result = null;
			
			for(ResourceContentType tmpType : lookup)
			{
				if(type.matches(tmpType.type + "(.*)")){
					result = tmpType;
					break;
				}
			}
			
            return result;
        }
	};

	private String guiEditorResources;

	@Autowired
	private GuiProjectDAO guiProjectDAO;

	@Autowired
	private ProfileDAO profileDAO;	
	
	@Autowired
	private CommentDAO commentDAO;	
	
	@Autowired
	private UnreadCommentDAO unreadCommentDAO;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private ProfileGuiProjectDAO profileprojectDAO;

	@Autowired
	private GuiProjectFileDAO projectFileDAO;

	@Autowired
	private ProjectDAO projectDAO;
	
	@Autowired
	private GuiProjectProjectDAO guiproject_projectDAO;
	
	/**
	 * @return the htmlProjectPath
	 */
	public String getGuiEditorResources() {
		return guiEditorResources;
	}

	/**
	 * @param htmlProjectPath
	 *            the htmlProjectPath to set
	 */
	public void setGuiEditorResources(String htmlProjectPath) {
		this.guiEditorResources = htmlProjectPath;
	}

	private static final Logger logger = LoggerFactory
			.getLogger(GuiEditorServiceImpl.class);

	public void saveFile(Integer projectId, String filename, String filecontent, ResourceContentType type) throws WriteResourceException {
		
		logger.debug("SAVE FILE");
		try {
			String destDir = getFilePath(projectId, type);
			
			String fullPath = destDir + "/" + filename;					
			File file = new File(fullPath);
			
			logger.debug("FILECONTENT:\n" + filecontent);
			
			FileUtils.writeStringToFile(file, filecontent);
			
		} catch (NotSupportedMimeTypeException e) {
			String msg = "Writing resource file failed: Mime Type not supported"+ (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);		
		} catch (IOException e) {
			String msg = "Writing resource file failed: IO Error" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);		
		}
	}
	
	@Override
	public List<GuiProject> findAll(String hql,
			HashMap<String, Object> parameters) {

		if (hql == null || hql.isEmpty())
			return guiProjectDAO.getAll();

		return guiProjectDAO.getAllBy(hql, parameters);
	}

	public List<GuiProject> findAllByProfile(Profile p) {

		if (p == null)
			return null;

		String hql = " from GuiProject p join fetch p.workgroup join fetch p.profileGuiProjects pp "
				+ " where pp.pk.profile.id=:profileId ORDER BY p.updatedAt DESC";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("profileId", p.getId());

		return findAll(hql, parameters);
	}
	
	public List<GuiProject> findAllByWorkgroup(Workgroup w) {

		if (w == null)
			return null;

		String hql = " from GuiProject p join fetch p.workgroup w where w.id=:workgroupId";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("workgroupId", w.getId());

		return findAll(hql, parameters);
	}

	@Override
	public List<GuiProjectDTO> findAllProjectDTOByProfile(Profile p) {

		List<GuiProject> list = findAllByProfile(p);
		
		List<GuiProjectDTO> listDTO = new ArrayList<GuiProjectDTO>();
		int nuc = 0;
		GuiProjectDTO pd;
		for (GuiProject item :list) {
			pd = new GuiProjectDTO();
			pd.setGuiProject(item);
			List <UnreadComment> uc = commentService.getUnreadCommentsByGuiProjectIdProfileId(item.getId(), p.getId());
			if (uc != null) {
				nuc = uc.size();
			}
            pd.setUnreadComment(nuc);
            pd.setOwner(findProjectOwner(item.getId()));
            listDTO.add(pd);
		}
		return listDTO;
	}
	@Override
	public GuiProject addProject(GuiProject project) {

		project.setCreatedAt(new Date());
		project.setUpdatedAt(new Date());
		guiProjectDAO.create(project);
		return project;
	}

	@Override
	public GuiProject addProject(GuiProject project, Profile owner) {

		logger.info("Add Project");

		if (owner == null)
			return null;

		try {
			project = addProject(project);

			ProfileGuiProject pp = new ProfileGuiProject();

			pp.setProfile(owner);
			pp.setProject(project);
			pp.setProjectRole(VmeConstants.PROFILE_PROJECT_ROLE_OWNER);
			pp.setCreatedAt(new Date());

			profileprojectDAO.create(pp);

			// Crea il record che mappa il file html di default del progetto
			String defaultFileName = VmeConstants.GUIPROJECT_HTML_FILE
					+ VmeConstants.GUIPROJECT_HTML_WORKING_EXT;

			GuiProjectFile gpf = new GuiProjectFile();
			gpf.setFilename(defaultFileName);
			gpf.setContentType(ResourceContentType.HTML.getType());
			gpf.setIsDefault(Boolean.TRUE);
			gpf.setGuiProject(project);
			projectFileDAO.create(gpf);

			// Crea la directory di progetto
			File projectDir = new File(this.guiEditorResources + "/"
					+ VmeConstants.GUIPROJECT_REPOSITORY + "/"
					+ VmeConstants.GUIPROJECT_DIR_PREFIX + project.getId());

			FileUtils.forceMkdir(projectDir);

			// Crea il primo file html di default
			File file = new File(projectDir.getAbsolutePath() + "/"
					+ defaultFileName);
			FileUtils.touch(file);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return project;
	}

	@Override
	public GuiProject findProject(Integer id) {

		return guiProjectDAO.getById(id);
	}

	public GuiFileResourceDTO getDefaultHtmlFile(Integer id) throws IOException {
		GuiProjectFile htmlFile = this.projectFileDAO.getDefaultByProject(id);

		String path = htmlFile.getFilename();

		File file = new File(this.guiEditorResources + "/"
				+ VmeConstants.GUIPROJECT_REPOSITORY + "/"
				+ VmeConstants.GUIPROJECT_DIR_PREFIX + id + "/" + path);


		return fillHtmlFileResource(id, htmlFile, file);
	}
	
	public GuiFileResourceDTO getDefaultPublicHtmlFile(Integer id) throws IOException {
		GuiProjectFile htmlFile = this.projectFileDAO.getDefaultByProject(id);

		String path = htmlFile.getFilename();
		path = path.replace(VmeConstants.GUIPROJECT_HTML_WORKING_SUFFIX, "");

		File file = new File(this.guiEditorResources + "/"
				+ VmeConstants.GUIPROJECT_REPOSITORY + "/"
				+ VmeConstants.GUIPROJECT_DIR_PREFIX + id + "/" + path);
		
		return fillHtmlFileResource(id, htmlFile, file);
	}	
	
	private GuiFileResourceDTO fillHtmlFileResource(Integer id, GuiProjectFile htmlFile, File file) throws IOException{		
		
		GuiFileResourceDTO dto = new GuiFileResourceDTO();

		dto.setId(htmlFile.getId());
		dto.setProject_id(id);
		dto.setIsdefault(htmlFile.getIsDefault());
		dto.setContentType(htmlFile.getContentType());
		dto.setFilename(htmlFile.getFilename());
		dto.setContent(FileUtils.readFileToString(file));
		
		return dto;
	}

	public List<String> getPaletteComponent() {
		List<String> jsonMetadataFiles = new ArrayList<String>();

		File dir = new File(this.guiEditorResources + "/"
				+ VmeConstants.GUIPROJECT_PALETTE_COMPONENT_PATH + "/"
				+ VmeConstants.GUIPROJECT_PALETTE_METADATA);

		Collection<File> files = FileUtils.listFiles(dir,
				FileFilterUtils.suffixFileFilter(".json"),
				TrueFileFilter.INSTANCE);

		for (File file : files) {
			jsonMetadataFiles
					.add(VmeConstants.GUIPROJECT_PALETTE_COMPONENT_PATH + "/"
							+ VmeConstants.GUIPROJECT_PALETTE_METADATA + "/"
							+ file.getName());
		}

		return jsonMetadataFiles;
	}
	
	@Override
	public Profile findProjectOwner(Integer projectId) {
		GuiProject prj = guiProjectDAO.getById(projectId);
		String hql = "from Profile p join fetch p.profileGuiProjects pp where pp.pk.project.id=:pId " + " and pp.projectRole="
				+ VmeConstants.PROFILE_PROJECT_ROLE_OWNER;
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pId", prj.getId());
		List<Profile> members = profileDAO.getAllBy(hql, parameters);							

		return (members.size() == 1 ? members.get(0) : null);
	}	
	
	@Override
	public GuiProject updateProject(GuiProject project, boolean updateDate) {

		//Project p = project.getProject();
		
		// l'utente ha scollegato il progetto di mockup dal mashup
//        if ((p !=  null) && (p.getId() == -1)) 
//        	project.setProject(null);
        		
		if(updateDate)
			project.setUpdatedAt(new Date());
		
		guiProjectDAO.update(project);
		return project;		
	}				
	
	@Override
	public List<Profile> findProjectMembers(GuiProject project) {
		String hql = "from Profile p join fetch p.profileGuiProjects pp where pp.pk.project.id=:pId";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pId", project.getId());
		List<Profile> members = profileDAO.getAllBy(hql, parameters);
		return members;
	}
	
	@Override
	public List<ProfileGuiProject> findProfileGuiProject(GuiProject project)
	{
		String hql = "from ProfileGuiProject pp join fetch pp.pk.project where pp.pk.project.id=:pId";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pId", project.getId());
		List<ProfileGuiProject> members = profileprojectDAO.getAllBy(hql, parameters);
		
		return members;
	}
	
	@Override
	public void memberAdd(Integer projectId, Integer profileId) {
		GuiProject prj = guiProjectDAO.getById(projectId);
		Profile profile = profileDAO.getById(profileId);
		ProfileGuiProject entity = new ProfileGuiProject();
		entity.setProfile(profile);
		entity.setProject(prj);
		entity.setProjectRole(VmeConstants.PROFILE_PROJECT_ROLE_USER);
		entity.setCreatedAt(new Date());
		profileprojectDAO.create(entity);
	}
	
	@Override
	public void memberAddWithRole(Integer projectId, Integer profileId, Integer role) {
		GuiProject prj = guiProjectDAO.getById(projectId);
		Profile profile = profileDAO.getById(profileId);
		ProfileGuiProject entity = new ProfileGuiProject();
		entity.setProfile(profile);
		entity.setProject(prj);
		entity.setProjectRole(role);
		entity.setCreatedAt(new Date());
		profileprojectDAO.create(entity);
	}

	@Override
	public void memberRemove(Integer projectId, Integer profileId) {
		EntityManager em = guiProjectDAO.getEntityManager();

		GuiProject prj = guiProjectDAO.getById(projectId);
		Profile profile = profileDAO.getById(profileId);

		ProfileGuiProjectId pk = new ProfileGuiProjectId(profile, prj);

		ProfileGuiProject pp = em.find(ProfileGuiProject.class, pk);
		em.remove(pp);
	}
	

	@Override
	public void deleteProject(Integer projectId) throws IOException {

		GuiProject prj = guiProjectDAO.getById(projectId);
		
		/*
		Profile member = this.findProjectOwner(projectId);	
		
		memberRemove(projectId, member.getId());
		
		List<Project> projects=this.findMashupProjects(prj);
		
		for(Project mashupProject : projects)
		{
			mashupRemove(projectId, mashupProject.getId());
		}*/
		//mashupRemove(projectId, member.getId());
		//EntityManager em = guiProjectDAO.getEntityManager();

		//Profile profile = profileDAO.getById(member.getId());

		//ProfileGuiProjectId pk = new ProfileGuiProjectId(profile, prj);

		//ProfileGuiProject pp = em.find(ProfileGuiProject.class, pk);
		//em.remove(pp);
		
		removeAllMashup(prj);
		
		List<Profile> members = this.findProjectMembers(prj);
		for (Profile profile : members) {
			memberRemove(projectId, profile.getId());
		}

		List<Comment> comments = this.findProjectComments(prj);
		for (Comment comment : comments) {
			commentDAO.delete(comment);
			unreadCommentDAO.deleteByCommentId(comment.getId());
		}
		
		
		for (UnreadComment uc: unreadCommentDAO.getUnreadCommentsByProjectId(projectId)) {
			unreadCommentDAO.delete(uc);
		}
		
		//Rimuove la directory di progetto
		// Crea la directory di progetto
		File projectDir = new File(this.guiEditorResources + "/"
				+ VmeConstants.GUIPROJECT_REPOSITORY + "/"
				+ VmeConstants.GUIPROJECT_DIR_PREFIX + projectId);
		
		FileUtils.deleteDirectory(projectDir);
		
		guiProjectDAO.delete(prj);
	}	
	
	public List<GuiFileResourceDTO> getResourcesByProject(int idProject, ResourceContentType type){				
		
		List<GuiProjectFile> rawData = projectFileDAO.getAllByProject(idProject, type);
		
		List<GuiFileResourceDTO> result = new ArrayList<GuiFileResourceDTO>();
		
		for(GuiProjectFile file : rawData){
			GuiFileResourceDTO dto = new GuiFileResourceDTO();
			dto.setId(file.getId());
			dto.setContentType(file.getContentType());
			dto.setFilename(file.getFilename());
			dto.setIsdefault(file.getIsDefault());
			dto.setProject_id(file.getGuiProject().getId());
						
			result.add(dto);
		}
		
		return result;
	}
	
	@Transactional
	public void removeResources(Integer resourceId) throws WriteResourceException{		

		GuiProjectFile resource = projectFileDAO.getById(resourceId);

		int projectId = resource.getGuiProject().getId();			
		
		String destDir;
		try {
			projectFileDAO.delete(resource);
			
			ResourceContentType cType = ResourceContentType.get(resource.getContentType());
			
			destDir = getFilePath(projectId, cType);
			
			String fullPath = destDir + "/" + resource.getFilename();		
			
			File file = new File(fullPath);

			FileUtils.deleteQuietly(file);

			if(cType == ResourceContentType.HTML){
				File workingfile = new File(fullPath.replace(VmeConstants.GUIPROJECT_HTML_WORKING_SUFFIX, ""));

				FileUtils.deleteQuietly(workingfile);				
			}						
			
		} catch (NotSupportedMimeTypeException e) {
			logger.error("Removing resource file failed: Mime Type not supported" + e.getMessage());
			throw new WriteResourceException(e);
		}		
	}	
	
	@Transactional
	public GuiFileResourceDTO writeResources(Integer projectId, MultipartFile content) throws WriteResourceException {				
		
		GuiFileResourceDTO resource = null;
		
		String contentType = content.getContentType();
		
		try {
			
			ResourceContentType cType = ResourceContentType.get(contentType);
			
			if(cType !=null){
			
				String destDir = getFilePath(projectId, cType);
				
				String fullPath = destDir + "/" + content.getOriginalFilename();
	
				File file = new File(fullPath);
	
				if(!file.exists()){
				
					GuiProject prj = guiProjectDAO.getById(projectId);
					
					GuiProjectFile gpf = new GuiProjectFile();
					gpf.setFilename(content.getOriginalFilename());						
					gpf.setContentType(contentType);
					gpf.setIsDefault(Boolean.FALSE);
					gpf.setGuiProject(prj);
					projectFileDAO.create(gpf);
										
					FileUtils.copyInputStreamToFile(content.getInputStream(), file);						
					
					resource = new GuiFileResourceDTO();
					resource.setId(gpf.getId());
					resource.setFilename(gpf.getFilename());
					resource.setContentType(gpf.getContentType());
				} else {
					throw new FileExistsException();
				}
			} else {
				throw new NotSupportedMimeTypeException("Mime type not supported!");
			}
		} catch (FileNotFoundException e) {
			String msg = "Writing resource file failed: file not found" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);
		} catch (FileExistsException e) {
			String msg = "Writing resource file failed: file already exists" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);		
		} catch (IOException e) {
			String msg = "Writing resource file failed: IO Error" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);
		} catch (NotSupportedMimeTypeException e) {
			String msg = "Writing resource file failed: Mime Type not supported" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);
		}
		
		return resource;
	}	
	
	@Transactional
	@Override
	public GuiFileResourceDTO newHtml(Integer projectId, String filename) throws WriteResourceException {				
		
		GuiFileResourceDTO resource = null;				
		
		try {
			ResourceContentType cType = ResourceContentType.HTML;
			
			if(cType !=null){
			
				String destDir = getFilePath(projectId, cType);
				
				if(filename.lastIndexOf(".")==-1) {
					filename+=".html";
				}
				
				String fullPath = destDir + "/" + filename;
	
				//File file = new File(fullPath);
				File workingfile = new File(fullPath+VmeConstants.GUIPROJECT_HTML_WORKING_SUFFIX);
	
				if(!workingfile.exists()){
				
					GuiProject prj = guiProjectDAO.getById(projectId);
					
					GuiProjectFile gpf = new GuiProjectFile();
					gpf.setFilename(filename+VmeConstants.GUIPROJECT_HTML_WORKING_SUFFIX);						
					gpf.setContentType(cType.getType());
					gpf.setIsDefault(Boolean.FALSE);
					gpf.setGuiProject(prj);
					projectFileDAO.create(gpf);
										
					//FileUtils.touch(file);
					FileUtils.touch(workingfile);
										
					resource = new GuiFileResourceDTO();
					resource.setId(gpf.getId());
					resource.setFilename(gpf.getFilename());
					resource.setContentType(gpf.getContentType());
				} else {
					throw new FileExistsException();
				}
			} else {
				throw new NotSupportedMimeTypeException("Mime type not supported!");
			}
		} catch (FileNotFoundException e) {
			String msg = "Writing resource file failed: file not found" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);
		} catch (FileExistsException e) {
			String msg = "Writing resource file failed: file already exists" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);		
		} catch (IOException e) {
			String msg = "Writing resource file failed: IO Error" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);
		} catch (NotSupportedMimeTypeException e) {
			String msg = "Writing resource file failed: Mime Type not supported" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);
		}
		
		return resource;
	}	
	
	private String getFilePath(Integer projectId, ResourceContentType contentType) throws NotSupportedMimeTypeException{
		// construct the complete absolute path of the file
		String destDir = this.guiEditorResources + "/" 
				+ VmeConstants.GUIPROJECT_REPOSITORY + "/" 
				+ VmeConstants.GUIPROJECT_DIR_PREFIX + projectId + "/";						
		
		destDir += contentType.getFolder();
		
		return destDir;
	}
	
	@Override
	public void updateGadgetPrefs(Integer projectId, GadgetPrefsDTO prefs) {
		GuiProject prj = guiProjectDAO.getById(projectId);

		prj.setGadgetTitle(prefs.getTitle());
		prj.setGadgetDescription(prefs.getDescription());

		prj.setGadgetAuthor(prefs.getAuthor());
		prj.setGadgetAuthorEmail(prefs.getAuthorEmail());

		prj.setGadgetWidth(prefs.getWidth());
		prj.setGadgetHeight(prefs.getHeight());

		prj.setHasGadget(true);
		
		prj.setUpdatedAt(new Date());

		guiProjectDAO.update(prj);
	}	
	
	@Override
	public InputStream getHTMLArchive(Integer projectId) throws IOException {
		InputStream is = null;		

		//Path della directory sorgente dei file di progetto HTML
		String guiProjectPath = guiEditorResources + "/" + VmeConstants.GUIPROJECT_REPOSITORY +"/"+ VmeConstants.GUIPROJECT_DIR_PREFIX + projectId;		
		File guiProjectDirectory = new File(guiProjectPath);
		
		String spaName = this.findProject(projectId).getSpaName();
		
		if (guiProjectDirectory.exists()) 
		{
			//Path temporaneo
			String tmpPath = FileUtils.getTempDirectoryPath()+"/"+VmeConstants.GUIPROJECT_DIR_PREFIX + projectId;				
			File tmpDir = new File(tmpPath);
			
			//Schedula l'eliminazione dell'area temporanea sullo shutdown della JVM
			FileUtils.forceDeleteOnExit(tmpDir);
			
			//Copia la directory sorgente nell'area temporanea
			FileUtils.copyDirectory(guiProjectDirectory, tmpDir);				
			
			//Rimuove i file ".working" 
			String[] extensions = new String[] { "html.working", "zip" };
			
			List<File> files = (List<File>) FileUtils.listFiles(tmpDir, extensions, true);
			
			for (File file : files) {
				FileUtils.deleteQuietly(file);
			}
			
			files = (List<File>) FileUtils.listFiles(tmpDir, new RegexFileFilter("^(.*?)"), null);
			
			for (File file : files) 
			{
				if(file.getName().compareTo(spaName + ".html") != 0)
					FileUtils.deleteQuietly(file);
			}

			//Instanzia l'helper per creare l'archivio compresso zip
			ZipHelper zipHelper = new ZipHelper();
			
			//Crea il file name dell'archivio
			String archiveName = VmeConstants.GUIPROJECT_ARCHIVE_FILENAME_PREFIX + projectId + ".zip";
			
			//Esegue la compressione
			zipHelper.zipDirectory(tmpDir, tmpPath +"/"+ archiveName);
			
			//Crea l'input stream da restituire
			FileSystemResource resource = new FileSystemResource(tmpPath +"/"+ archiveName);

			is = resource.getInputStream();
		} else {
			throw new IOException("Gui project directory doesn't exist");
		}
		return is;
	}
	
	@Override
	public InputStream getCordovaArchive(Integer projectId) throws IOException {
		InputStream is = null;		

		String cordovaProjectDir = PropertyGetter.getProperty("app.appRepository") + File.separator + "prj_" + projectId;		
		File cordovaProjectDirectory = new File(cordovaProjectDir);
		
		if (cordovaProjectDirectory.exists()) 
		{
			//Path temporaneo
			String tmpPath = FileUtils.getTempDirectoryPath() + File.separator + VmeConstants.GUIPROJECT_DIR_PREFIX + projectId;				
			File tmpDir = new File(tmpPath);
			
			if(tmpDir.exists())
			{
				FileUtils.cleanDirectory(tmpDir);
			}
			
			//Schedula l'eliminazione dell'area temporanea sullo shutdown della JVM
			FileUtils.forceDeleteOnExit(tmpDir);		
			
			//Copia la directory sorgente nell'area temporanea
			FileUtils.copyDirectory(cordovaProjectDirectory, tmpDir);	
			
			/*
			//Rimuove i file ".zip" 
			String[] extensions = new String[] { "zip" };
			
			List<File> files = (List<File>) FileUtils.listFiles(tmpDir, extensions, true);
			
			for (File file : files) {
				FileUtils.deleteQuietly(file);
			}
			*/

			//Instanzia l'helper per creare l'archivio compresso zip
			ZipHelper zipHelper = new ZipHelper();
			
			//Crea il file name dell'archivio
			String archiveName = VmeConstants.GUIPROJECT_CORDOVA_ARCHIVE_FILENAME_PREFIX + projectId + ".zip";
			
			//Esegue la compressione
			zipHelper.zipDirectory(tmpDir, tmpPath + File.separator + archiveName);
			
			//Crea l'input stream da restituire
			FileSystemResource resource = new FileSystemResource(tmpPath + File.separator + archiveName);

			is = resource.getInputStream();
		} 
		else 
		{
			throw new IOException("Cordova project directory doesn't exist");
		}
		
		return is;
	}
	
	@Override
	public List<GuiProject> searchProject(Profile p, String criteria) {
		
		return guiProjectDAO.searchProject(p.getId(), criteria);
	}	
	
	@Override
	public List<Comment> findProjectComments(GuiProject p) {
		return findProjectComments(p, 0);
	}

	@Override
	public List<Comment> findProjectComments(GuiProject p, int limit) {
		String hql = "from Comment as comment join fetch comment.profile where comment.guiProject.id=:projectId order by comment.createdAt desc";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("projectId", p.getId());

		List<Comment> comments = commentDAO.getAllBy(hql, parameters, limit, 0);
		return comments;
	}
	
	@Override
	public void mashupProjectAdd(Integer guiprojectId, Integer mashupProjectId) {
		// TODO Auto-generated method stub
		//System.out.println("guiprojectId "+guiprojectId);
		GuiProject prj = guiProjectDAO.getById(guiprojectId);
		Project mashupProject=projectDAO.getById(mashupProjectId);
		GuiProjectProject entity = new GuiProjectProject();
		entity.setGuiProject(prj);
		entity.setProject(mashupProject);
		entity.setProjectRole(VmeConstants.PROFILE_PROJECT_ROLE_OWNER);
		entity.setCreatedAt(new Date());
		guiproject_projectDAO.create(entity);
		
	}

	@Override
	public void mashupRemove(Integer guiProjectId, Integer mashupProjectId) {
		// TODO Auto-generated method stub
		EntityManager em = guiProjectDAO.getEntityManager();

		GuiProject prj = guiProjectDAO.getById(guiProjectId);
		Project mashupProject = projectDAO.getById(mashupProjectId);

		GuiProjectProjectId pk = new GuiProjectProjectId(prj, mashupProject);

		GuiProjectProject pp = em.find(GuiProjectProject.class, pk);
		em.remove(pp);
	}

	@Override
	public List<Project> findMashupProjects(GuiProject guiProject) {
		// TODO Auto-generated method stub
		String hql = "from Project p join fetch p.guiProjectProject pp where pp.pk.guiProject.id=:pId";
		
		//String hql2 = "from Comment as comment join fetch comment.profile where comment.guiProject.id=:projectId order by comment.createdAt desc";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pId", guiProject.getId());
		List<Project> mashupProjects = projectDAO.getAllBy(hql, parameters);
		//System.out.println("size mashups prj "+mashupProjects.size());
		//System.out.println("first mashup prj "+mashupProjects.get(0));
		return mashupProjects;
	}
	
	public List<Project> findMashupProject(GuiProject guiProject, int mashupProjectId) {
		// TODO Auto-generated method stub
		String hql = "from Project p join fetch p.guiProjectProject pp where pp.pk.guiProject.id=:pId and pp.pk.project.id=mashupProjectId";
		
		//String hql2 = "from Comment as comment join fetch comment.profile where comment.guiProject.id=:projectId order by comment.createdAt desc";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pId", guiProject.getId());
		List<Project> mashupProjects = projectDAO.getAllBy(hql, parameters);
		return mashupProjects;
	}
	
	@Override
	public GuiProject removeAllMashup(GuiProject guiProject){
		List<Project> projects=this.findMashupProjects(guiProject);
		if(!projects.isEmpty()){
		for(Project mashupProject : projects){
			mashupRemove(guiProject.getId(), mashupProject.getId());
		}
		}
		guiProjectDAO.update(guiProject);
		return guiProject;
	}
	
	@Override
	public void addAllMashup(GuiProject guiProject,String mashupProjectList){
		for (String s : mashupProjectList.split(",")) {
			Integer mashupProjectId = Integer.valueOf(s);
			try {
				if(mashupProjectId!=-1)
				mashupProjectAdd(guiProject.getId(), mashupProjectId);			
				
			} catch (DataIntegrityViolationException e) {
				logger.debug(e.getMessage());
			}
		}
		//guiProjectDAO.update(guiProject);
		//return guiProject;
	}
}
