package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.dto.ProfileDTO;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.util.PagedSearch;

@Transactional
public interface ProfileService {

	public Profile addProfile(Profile a);
	public Profile updateProfile(Profile a);
	public void deleteProfile(Integer id);
	
	public void addAdministrator(Profile p);
	
	public Profile findProfile(Integer id);
	public Profile findProfileByServiceCatalog(Integer id);
	public Profile findProfileByOpenDataCatalog(Integer id);
	
	public List<Profile> findAll();
	
	public List<Profile> findByWorkgroup(Workgroup w);
		
	public List<Profile> findByRole(int role);
	
	public PagedSearch<ProfileDTO> pagedSearch(String searchTerm, int page, int pageCount);
	public PagedSearch<ProfileDTO> pagedSearchByWorkgroup(Integer workgroupId, String searchTerm, int page, int pageCount);
	
	public void workgroupsAdd(Integer profileId, List<Integer> workgroupIds);
	public void workgroupAdd(Integer profileId, Integer workgroupId);
	public void workgroupRemove(Integer profileId, Integer workgroupId);
	
	// IVAN
	
	public Profile findProfileByEmail(String email);
	public Profile findProfileByCcUserId(Integer ccUserId);	
	
	public List<Profile> findWorkgroupMembers(Integer workgroupId);
	
}
