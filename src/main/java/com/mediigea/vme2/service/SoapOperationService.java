package com.mediigea.vme2.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.soap.SOAPMessage;

import org.json.JSONArray;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mediigea.vme2.dto.OperationTemplateDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.dto.SoapOperationDTO;
import com.mediigea.vme2.entity.RestOperationTemplate;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.entity.SoapOperationTemplate;

/**
 * Interface to define management operations about operations of Soap services
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface SoapOperationService {

	public List<SoapOperation> findAll(String hql, HashMap<String, Object> parameters);

	public List<SoapOperation> findAllByService(Integer id);

	public SoapOperation findSoapOperation(Integer id);
	
	public SoapOperationDTO findSoapOperationAsDTO(Integer id);
	
	public SOAPMessage createSoapRequest(SoapOperationDTO soapOp, JSONArray parametersFromClient);
	
	public SOAPMessage createSoapRequestPrototype(SoapOperationDTO soapOp, Map<String, String> parameters);
	
	public List<ServiceParameterDTO> getSoapOperationParamsDTO(int operationId);
	
	public void addSoapOperationTemplate(OperationTemplateDTO operationTemplate) throws JsonProcessingException;
	
//	public void updateSoapOperationTemplate(SoapOperationTemplate operationTemplate) throws JsonProcessingException;
	
	public SoapOperation findOperationByTemplate(Integer templateId);
	
	public void deleteSoapOperationTemplate(Integer templateId);
	
	public List<ServiceParameterDTO> getSoapTemplateParamsDTO(int operationId) throws Exception;
	
	public List<SoapOperationTemplate> findTemplateByOperation(Integer operationId);
	
	public SoapOperationTemplate findTemplateById(Integer templateId);

	public OperationTemplateDTO findTemplateByIdDTO(Integer templateId) throws Exception;
}
