package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.ProjectBlackbox;

/**
 * Interface to define operations about blackboxes used inside a mashup project
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface ProjectBlackboxService {	
	
	public void create(int idProject, int idBlackbox);
	
	public void deleteAllByProject(int idProject);
	
	public List<ProjectBlackbox> getAllByProject(int idProject);
	
	public List<ProjectBlackbox> getAllByBlackbox(int idBlackbox);
}
