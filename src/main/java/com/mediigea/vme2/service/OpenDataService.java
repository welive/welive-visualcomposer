package com.mediigea.vme2.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.ckan.Dataset;
import org.ckan.Resource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mediigea.vme2.dto.OpenDataResourceDTO;
import com.mediigea.vme2.entity.CkanServer;
import com.mediigea.vme2.entity.OpenDataResource;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.exceptions.FileExceedsSizeLimitException;
import com.mediigea.vme2.exceptions.NotSupportedMimeTypeException;

/**
 * Interface to define management operations about services (Rest/Soap) catalog
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface OpenDataService {
	
	public OpenDataResource addFromUrl(String sourceName, String resourceUrl, Profile profile, char separator) throws IOException, NotSupportedMimeTypeException, FileExceedsSizeLimitException;
	
	public OpenDataResource addFromFile(String sourceName, MultipartFile file, Profile profile, char separator) throws IOException, NotSupportedMimeTypeException, FileExceedsSizeLimitException;
	
	public OpenDataResource addFromFile(String sourceName, String file, String filename, String type, long size, Profile profile, char separator) throws IOException, NotSupportedMimeTypeException, FileExceedsSizeLimitException;
	
	public OpenDataResource addFromCkan(CkanServer ckanServer, Dataset dataset,	Resource resource, Profile profile, char separator) throws IOException, NotSupportedMimeTypeException, FileExceedsSizeLimitException;
	
	public OpenDataResource setVisibility(Integer serviceId, Integer code);	

	public void delete(Integer id) throws Exception;
		
	public List<OpenDataResource> findAdministratorsResources();
	
	public List<OpenDataResource> findPublicResources();
	
	public List<OpenDataResource> findVisibleResources(Integer profileId);

	public OpenDataResource getById(Integer id);

	public List<OpenDataResourceDTO> getOpenDataResourcesDTO(Profile p);
}
