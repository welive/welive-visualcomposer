package com.mediigea.vme2.service;

import java.util.HashMap;
import java.util.List;

import org.apache.abdera2.activities.model.Collection;
import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Activity;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.UnreadComment;

@Transactional
public interface ActivityService {

	
	public List<org.apache.abdera2.activities.model.Activity> findAllActivity();
	public List <org.apache.abdera2.activities.model.Activity> findActivityByProfile(Integer profileId);
	public void activityStreamRecordSave(Profile p, String activityType);
	public void activityStreamRecordSave(Profile p, String activityType, String description);
}
