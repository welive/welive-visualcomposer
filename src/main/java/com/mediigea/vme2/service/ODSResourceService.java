package com.mediigea.vme2.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.ODSResource;

@Transactional
public interface ODSResourceService 
{
	public ODSResource addODSResource(ODSResource resource);
	
	public ODSResource updateODSResource(ODSResource resource);
	
	public void deleteODSResource(Integer id) throws Exception;
	
	public List<ODSResource> findAll(String hql, HashMap<String, Object> parameters);
	
	public List<ODSResource> findAllByODSDataset(Integer id);
	
	public ODSResource findODSResource(Integer id);
}
