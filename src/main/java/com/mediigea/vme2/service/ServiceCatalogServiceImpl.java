package com.mediigea.vme2.service;

import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Response;
import io.swagger.models.SecurityRequirement;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.parameters.HeaderParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.parameters.PathParameter;
import io.swagger.models.parameters.QueryParameter;
import io.swagger.parser.SwaggerParser;
import io.swagger.parser.util.SwaggerDeserializationResult;
import it.eng.JsonUtils;
import it.eng.PropertyGetter;
import it.eng.metamodel.definitions.AuthenticationMeasure;
import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.IdentityProvider;
import it.eng.metamodel.definitions.InteractionPoint;
import it.eng.metamodel.definitions.Permission;
import it.eng.metamodel.mapping.BuildingBlockMapping;
import it.eng.metamodel.mapping.MappingResponse;
import it.eng.model.SwaggerParsingResponse;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.dao.ProjectOperationDAO;
import com.mediigea.vme2.dao.RestOperationDAO;
import com.mediigea.vme2.dao.ServiceCatalogDAO;
import com.mediigea.vme2.dao.SoapOperationDAO;
import com.mediigea.vme2.dao.WorkgroupDAO;
import com.mediigea.vme2.dto.OperationTemplateDTO;
import com.mediigea.vme2.dto.ServiceItemDTO;
import com.mediigea.vme2.dto.ServiceOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.entity.AuthProviderType;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.RestOperationTemplate;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.entity.SoapOperationTemplate;
import com.mediigea.vme2.parser.WadlParser;
import com.mediigea.vme2.parser.WsdlParser;
import com.mediigea.vme2.rest.RestApplication;
import com.mediigea.vme2.rest.RestAuth;
import com.mediigea.vme2.rest.RestDoc;
import com.mediigea.vme2.rest.RestMethod;
import com.mediigea.vme2.rest.RestPackage;
import com.mediigea.vme2.rest.RestParameter;
import com.mediigea.vme2.rest.RestResource;
import com.mediigea.vme2.rest.RestResponse;
import com.mediigea.vme2.soap.SoapMethod;
import com.mediigea.vme2.soap.SoapPackage;
import com.mediigea.vme2.util.VmeConstants;

public class ServiceCatalogServiceImpl implements ServiceCatalogService {

	public static final String SERVICE_BY_PROFILE = "serviceByProfile";
	public static final String SERVICE_BY_VISIBILITY = "serviceByVisibility";
	public static final String SERVICE_BY_WORKGROUP = "serviceByWorkgroup";

	private static final Logger logger = LoggerFactory.getLogger(ServiceCatalogServiceImpl.class);

//	private TypeReference<HashMap<String, List<SoapParameter>>> typeRef = new TypeReference<HashMap<String, List<SoapParameter>>>() {
//	};

//	private TypeReference<RestMethod> typeRefRestMethod = new TypeReference<RestMethod>() {
//	};
	
	@Autowired
	private ServiceCatalogDAO serviceCatalogDAO;
	
	@Autowired
	private RestOperationDAO restDAO;

	@Autowired
	private WorkgroupDAO wokgroupDAO;

	@Autowired
	private ProfileDAO profileDAO;

	@Autowired
	private SoapOperationDAO soapOperationDAO;

	@Autowired
	private RestOperationDAO restOperationDAO;
	
	@Autowired
	private ProjectOperationDAO projectOperationDAO;
	
	@Autowired
	private RestOperationService restOperationService;
	
	@Autowired
	private SoapOperationService soapOperationService;	
	
	@Autowired
	private ObjectMapperExt objectMapper;

	@Override
	public ServiceCatalog addService(ServiceCatalog service, Profile profile, int type) {

		if (service == null || profile == null)
			return null;

		service.setType(type);
		service.setVisibility(VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE);
		service.setProfile(profile);
		service.setCreatedAt(new Date());
		service.setUpdatedAt(new Date());

		serviceCatalogDAO.create(service);

		return service;
	}

	@Override
	public ServiceCatalog updateService(ServiceCatalog service) {

		if (service == null)
			return null;

		service.setUpdatedAt(new Date());

		service = serviceCatalogDAO.update(service);

		return service;
	}

	@Override
	public void deleteService(Integer id) throws Exception {

		try {

			ServiceCatalog s = serviceCatalogDAO.getById(id);
			if (s.getType() == VmeConstants.SERVICECATALOG_TYPE_SOAP) {
				List<SoapOperation> soapOperations = soapOperationDAO.getAllByService(s);
				for (SoapOperation item : soapOperations) {
					List<ProjectOperation> prjopers = projectOperationDAO.getAllBySoapOperation(item.getId());
					for (ProjectOperation po: prjopers){
						projectOperationDAO.delete(po);
					}					
					soapOperationDAO.delete(item);
				}

			} else {
				List<RestOperation> restOperations = restOperationDAO.getAllByService(s);
				for (RestOperation item : restOperations) {
					List<ProjectOperation> prjopers = projectOperationDAO.getAllByRestOperation(item.getId());
					for (ProjectOperation po: prjopers){
						projectOperationDAO.delete(po);
					}	
					restOperationDAO.delete(item);
				}

			}

			serviceCatalogDAO.delete(s);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			ex.printStackTrace();
			throw ex;
		}

	}

	@Override
	public ServiceCatalog setVisibility(Integer serviceId, Integer code) {

		if (serviceId == null || code == null)
			return null;

		ServiceCatalog s = serviceCatalogDAO.getById(serviceId);

		if (s == null)
			return null;

		s.setVisibility(code);

		serviceCatalogDAO.update(s);

		return s;
	}

	@Override
	public List<ServiceCatalog> findAll(String hql, HashMap<String, Object> parameters) {

		if (hql == null || hql.isEmpty())
			return serviceCatalogDAO.getAll();

		return serviceCatalogDAO.getAllBy(hql, parameters);
	}

	@Override
	public ServiceCatalog findService(int id) {

		ServiceCatalog scItem = serviceCatalogDAO.getById(id);
		return scItem;
	}

	private ServiceCatalog parseWSDLAndCreateServiceCatalog(Profile profile, String url, byte[] bytes) throws Exception {

		logger.info("parseWSDLAndCreateServiceCatalog");

		WsdlParser wsdlParser = new WsdlParser();

		SoapPackage soapPackage = null;

		if (bytes == null || bytes.length == 0)
			throw new Exception("Invalid wsdl");

		soapPackage = wsdlParser.parse(bytes);

		logger.info("Wsdl parsing ok");

		ServiceCatalog serviceCatalog = new ServiceCatalog();

		serviceCatalog.setName(soapPackage.getName());
		serviceCatalog.setType(VmeConstants.SERVICECATALOG_TYPE_SOAP);
		serviceCatalog.setVisibility(profile.getAccount().getRole() == VmeConstants.ACCOUNT_ROLE_ADMIN ? VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC
				: VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE);
		serviceCatalog.setProfile(profile);
		serviceCatalog.setContent(new String(bytes));

		serviceCatalog.setCreatedAt(new Date());
		serviceCatalog.setUpdatedAt(new Date());

		logger.info("Create service");
		serviceCatalogDAO.create(serviceCatalog);

		ArrayList<SoapMethod> soapMethodList = soapPackage.getOperations();

		logger.info("Verify serviceOperationDTOList");

		if (soapMethodList != null && !soapMethodList.isEmpty()) {

			for (SoapMethod op : soapMethodList) {

				SoapOperation soapOperation = new SoapOperation();
				soapOperation.setServiceCatalog(serviceCatalog);

				soapOperation.setOperationName(op.getName());
				soapOperation.setWsdlUrl(url);
				soapOperation.setEndPoint(op.getEndPoint());
				soapOperation.setBindingName(op.getBindingName());
				soapOperation.setPortTypeName(op.getPortTypeName());
				soapOperation.setSoapAction(op.getSoapAction());
				soapOperation.setInputParams(objectMapper.writeValueAsString(op.getInputParams()));
				soapOperation.setOutputParams(objectMapper.writeValueAsString(op.getOutputParams()));

				logger.info("Create Operation");
				soapOperationDAO.create(soapOperation);
			}
		}

		return serviceCatalog;

	}

	/***
	 * Parse WSDL and create entities
	 */
	@Override
	public ServiceCatalog addWsdlService(String url, Profile profile) throws Exception {

		URL wsdlUrl = new URL(url);
		byte[] bytes = IOUtils.toByteArray(wsdlUrl.openStream());

		return parseWSDLAndCreateServiceCatalog(profile, url, bytes);
	}

	/***
	 * Parse WSDL and create entities
	 */
	@Override
	public ServiceCatalog addWsdlService(byte[] bytes, Profile profile) throws Exception {

		return parseWSDLAndCreateServiceCatalog(profile, "", bytes);
	}

	/**
	 * 
	 */
	@Override
	public ServiceCatalog addFromWadl(String appName, String wadl, Profile profile) throws XmlException, JsonProcessingException {
		RestApplication app = WadlParser.parseString(appName, wadl);

		ServiceCatalog serviceCatalog = new ServiceCatalog();
		serviceCatalog.setCreatedAt(new Date());
		serviceCatalog.setUpdatedAt(new Date());
		serviceCatalog.setName(appName);
		serviceCatalog.setProfile(profile);
		serviceCatalog.setContent(wadl);
		serviceCatalog.setType(VmeConstants.SERVICECATALOG_TYPE_REST);

		serviceCatalog.setVisibility(profile.getAccount().getRole() == VmeConstants.ACCOUNT_ROLE_ADMIN ? VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC
				: VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE);

		serviceCatalogDAO.create(serviceCatalog);

		for (RestPackage pack : app.getPackages()) {
			for (RestResource res : pack.getResources()) {
				RestOperation restOp = new RestOperation();
				restOp.setServiceCatalog(serviceCatalog);
				restOp.setBaseURL(pack.getBaseURL());
				restOp.setName(res.getName());
				restOp.setLabel(res.getLabel());
				restOp.setPath(res.getExecuteMethod().getUrl());
				restOp.setTag(res.getTag());
				
				restOp.setDoc(null);
				
				if(res.getDoc().size() > 0)
				{
					restOp.setDoc(res.getDoc().get(0).getText());
				}
				
				restOp.setAuthentication(res.getExecuteMethod().getAuthentication() == null ? "false" : res.getExecuteMethod().getAuthentication());
				restOp.setExecMethod(objectMapper.writeValueAsString(res.getExecuteMethod()));
				restOperationDAO.create(restOp);
			}
		}

		return serviceCatalog;
	}

	/**
	 * Al presente metodo e' demandato il filtraggio del ServiceCatalog in base
	 * al livello di accesso dell'utente e visibilita' del service stesso. Il
	 * metodo restituisce una struttura dati adatta all'esposizione di dati JSON
	 * oppure XML
	 */
	@Override
	public List<ServiceItemDTO> getServicesDTO(Profile p) {

		List<ServiceCatalog> myList = this.findVisibleServices(p.getId());
		List<ServiceCatalog> publicList = this.findPublicServices();

		if (myList == null && publicList == null)
			return null;

		ArrayList<ServiceItemDTO> catalogResult = new ArrayList<ServiceItemDTO>();

		for (ServiceCatalog service : myList) {

			ServiceItemDTO serviceItem = new ServiceItemDTO();

			serviceItem.setId(service.getId());
			serviceItem.setName(service.getName());
			serviceItem.setType(((service.getType() == VmeConstants.SERVICECATALOG_TYPE_SOAP) ? "wsdl" : "restful"));
			catalogResult.add(serviceItem);
		}
		for (ServiceCatalog service : publicList) {

			ServiceItemDTO serviceItem = new ServiceItemDTO();

			serviceItem.setId(service.getId());
			serviceItem.setName(service.getName());
			serviceItem.setType(((service.getType() == VmeConstants.SERVICECATALOG_TYPE_SOAP) ? "wsdl" : "restful"));
			catalogResult.add(serviceItem);
		}
		return catalogResult;

	}

	/**
	 * 
	 */
	@Override
	public List<ServiceOperationDTO> getServiceOperationsDTO(int serviceId) {
		ServiceCatalog serviceItem = serviceCatalogDAO.getById(serviceId);

		if (serviceItem != null) {
			if (serviceItem.getType() == VmeConstants.SERVICECATALOG_TYPE_SOAP) {
				return getSoapOperationsDTO(serviceItem);
			} else
				return getRestOperationsDTO(serviceItem);
		}

		return null;
	}

	/**
	 * 
	 * @param serviceItem
	 * @return
	 */
	private List<ServiceOperationDTO> getRestOperationsDTO(ServiceCatalog serviceItem) {
		List<ServiceOperationDTO> result = null;

		List<RestOperation> restOperations = restOperationDAO.getAllByService(serviceItem);
		if (restOperations != null && restOperations.size() > 0) {
			result = new ArrayList<ServiceOperationDTO>();
			for (RestOperation restOp : restOperations) {
				ServiceOperationDTO dto = new ServiceOperationDTO();
				dto.setId(restOp.getId());
				dto.setName(restOp.getName());
				dto.setServiceId(serviceItem.getId());
				dto.setServiceType(serviceItem.getType());
				dto.setDoc(restOp.getDoc());
				dto.setTag(restOp.getTag());
				
//				dto.setHasTemplate(restOp.getTemplate()!=null ? true : false);
				
				List<OperationTemplateDTO> templates = new ArrayList<OperationTemplateDTO>();
				
				for(RestOperationTemplate template: restOp.getTemplate()){
					OperationTemplateDTO opertempl = new OperationTemplateDTO();
					
					opertempl.setName(template.getName());
					opertempl.setDescription(template.getDescription());
					opertempl.setTemplateId(template.getId());
					
					templates.add(opertempl);					
				}
				
				dto.setTemplates(templates);
				
				dto.setAuthenticationRequired(restOp.getAuthentication().equalsIgnoreCase("true"));

				// TODO: troppo pesante... i parametri vanno richiesti per la
				// singola operation
//				List<ServiceParameterDTO> parameters = new ArrayList<ServiceParameterDTO>();
//				dto.setParameters(parameters);
//
//				try {
//					RestMethod method = objectMapper.readValue(restOp.getExecMethod(), typeRefRestMethod);
//					for (RestParameter rp : method.getParameters()) {
//						if (rp.isFixed() == false) {
//							ServiceParameterDTO param = fillfromRestParameter(rp);
//							parameters.add(param);
//						}
//					}
//					if (method.getRepresentations() != null && method.getRepresentations().size() > 0) {
//						for (RestRepresentation repr : method.getRepresentations()) {
//							if (repr.getParameters() != null) {
//								for (RestParameter rp : repr.getParameters()) {
//									if (rp.isFixed() == false) {
//										ServiceParameterDTO param = fillfromRestParameter(rp);
//										parameters.add(param);
//									}
//								}
//							}
//							RestAttachment attach = repr.getAttachment();
//							if (attach != null) {
//								ServiceParameterDTO param = new ServiceParameterDTO();
//								param.setAttachment(true);
//								param.setName(attach.getName());
//								if (attach.getDoc() != null) {
//									param.setDoc(attach.getDoc().getText());
//								}
//								param.setRequired(attach.isRequired());
//								parameters.add(param);
//							}
//							RestPayload payload = repr.getPayload();
//							if (payload != null) {
//								ServiceParameterDTO param = new ServiceParameterDTO();
//								param.setPayload(true);
//								if (payload.getDoc() != null) {
//									param.setDoc(payload.getDoc().getText());
//								}
//								param.setRequired(payload.isRequired());
//							}
//						}
//					}
//
//				} catch (JsonParseException e) {
//					logger.error("Error JsonParseException:" + e.getStackTrace().toString());
//				} catch (JsonMappingException e) {
//					logger.error("Error JsonMappingException:" + e.getStackTrace().toString());
//				} catch (IOException e) {
//					logger.error("Error IOException:" + e.getStackTrace().toString());
//				}

				result.add(dto);
			}
		}

		return result;
	}		

	/**
	 * 
	 * @param serviceItem
	 * @return
	 */
	private List<ServiceOperationDTO> getSoapOperationsDTO(ServiceCatalog serviceItem) {

		List<ServiceOperationDTO> result = null;

		List<SoapOperation> soapOperations = soapOperationDAO.getAllByService(serviceItem);
		if (soapOperations != null && soapOperations.size() > 0) {
			result = new ArrayList<ServiceOperationDTO>();
			for (SoapOperation sOp : soapOperations) {
				ServiceOperationDTO dto = new ServiceOperationDTO();
				dto.setId(sOp.getId());
				dto.setName(sOp.getOperationName());
				dto.setServiceId(serviceItem.getId());
				dto.setServiceType(serviceItem.getType());
				dto.setDoc("");
				dto.setTag("");
				dto.setAuthenticationRequired(false);
//				dto.setHasTemplate(sOp.getTemplate()!=null ? true : false);

				List<OperationTemplateDTO> templates = new ArrayList<OperationTemplateDTO>();
				
				for(SoapOperationTemplate template: sOp.getTemplate()){
					OperationTemplateDTO opertempl = new OperationTemplateDTO();
					
					opertempl.setName(template.getName());
					opertempl.setDescription(template.getDescription());
					opertempl.setTemplateId(template.getId());
					
					templates.add(opertempl);					
				}
				
				dto.setTemplates(templates);

				result.add(dto);
			}
		}

		return result;
	}

	@Override
	public List<ServiceCatalog> findAdministratorsServices() {
		//String hql = " from ServiceCatalog s join fetch s.profile p where p.account.role = " + VmeConstants.ACCOUNT_ROLE_ADMIN + " order by s.name ";
		String hql = " from ServiceCatalog s join fetch s.profile p order by s.name ";
		return serviceCatalogDAO.getAllBy(hql, null);
	}

	@Override
	public List<ServiceCatalog> findPublicServices() {
		String hql = " from ServiceCatalog s where s.visibility=" + VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC + " order by s.name ";
		return serviceCatalogDAO.getAllBy(hql, null);
	}
	
	@Override
	public List<ServiceCatalog> findVisibleServices(Integer profileId) {
		String hql = " Select distinct s from ServiceCatalog s join fetch s.profile p join p.profileWorkgroups pw "
				+ " where pw.pk.workgroup.id in (select pw2.pk.workgroup.id from ProfileWorkgroup pw2 where pw2.pk.profile.id = :pId)"
				+ " and ( s.profile.id = :pId or s.visibility =" + VmeConstants.SERVICECATALOG_VISIBILITY_WORKGROUP + " )  order by s.visibility desc, s.name ";

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("pId", profileId);
		return serviceCatalogDAO.getAllBy(hql, params);
	}		
	
	public List<ServiceParameterDTO> getOperationParamsDTO (int operationId, int serviceType){
		
		if(serviceType == VmeConstants.SERVICECATALOG_TYPE_REST){
			return this.restOperationService.getRestOperationParamsDTO(operationId);
		} else {
			return this.soapOperationService.getSoapOperationParamsDTO(operationId);
		}		
	}
	
	public List<ServiceParameterDTO> getTemplateParamsDTO (int templateId, int serviceType) throws Exception{
		
		if(serviceType == VmeConstants.SERVICECATALOG_TYPE_REST){
			return this.restOperationService.getRestTemplateParamsDTO(templateId);
		} else {
			return this.soapOperationService.getSoapTemplateParamsDTO(templateId);
		}		
	}
	
	// IVAN
	// swagger extension: map Swagger object to ServiceCatalog
	
	private SwaggerParsingResponse serviceFromSwagger(String swagger)
	{
		SwaggerParsingResponse response = new SwaggerParsingResponse();
		
		ServiceCatalog serviceCatalog = new ServiceCatalog();
		
		Swagger service = null;
		
		try 
		{			
			// dereference schemas
			ObjectNode root = (ObjectNode) objectMapper.readTree(swagger);
			
			JsonNode defs = root.findValue("definitions");
			
			JsonUtils.dereferenceJson(defs, root);
			JsonUtils.dereferenceJson(root, root);
			
			//
			
			service = new SwaggerParser().parse(root.toString());

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
			
			response.addError("ParsingError: " + e.getMessage());
			return response;
		}
		
		// set general info
		serviceCatalog.setName(service.getInfo().getTitle());
		serviceCatalog.setCreatedAt(new Date());
		serviceCatalog.setUpdatedAt(new Date());
		serviceCatalog.setContent(swagger);
		serviceCatalog.setType(VmeConstants.SERVICECATALOG_TYPE_REST);
		serviceCatalog.setSubType(VmeConstants.SERVICECATALOG_TYPE_REST_SWAGGER);
		
		// retrieve general security
		// if operations don't specify security requirements, general security will be applied to all the operations			
		List<SecurityRequirement> secs = service.getSecurity();
		
		List<String> generalScopes = null;
		
		if(secs != null)
		{
			for(int i = 0; i < secs.size(); i++)
			{
				generalScopes = secs.get(i).getRequirements().get("weliveUserAuth");
				
				if(generalScopes != null)
					break;
			}
		}
		//		
		
		// check if host is specified
		if(service.getHost() == null)
		{
			response.addError("attribute host is missing");
			return response;
		}
		
		// operation base URL
		String baseUrl = ((service.getSchemes() != null && service.getSchemes().size() > 0 && service.getSchemes().get(0).name() != "") ? service.getSchemes().get(0).name().toLowerCase() : "http")
			+ "://" + service.getHost() + (service.getBasePath() != null ? service.getBasePath() : "");
		
		// retrieve paths
		Map<String, Path> paths = service.getPaths();
		Iterator<String> keys = paths.keySet().iterator();
		
		// variables to reuse into loops
		Map<HttpMethod, Operation> ops;
		Operation op;
		List<Map<String, List<String>>> opSecurities;
		
		RestOperation restOp = null;
		RestMethod method = null;
		RestAuth methodAuth = null;
		RestParameter restPar = null;
		List<RestParameter> restParameters = null;
		List<RestResponse> restResponses = null;
		RestDoc restDoc = null;
		RestResponse restRes = null;
		
		Iterator<HttpMethod> methodNames;
		HttpMethod methodName;
		
		List<String> opScopes = null;
		List<Parameter> opPars = null;
		Map<String, Response> opRes = null;
		Parameter par;
		Response res;
		//
		
		// iterate through paths
		String path;
		while(keys.hasNext())
		{				
			path = keys.next();
			
			ops = paths.get(path).getOperationMap();
			
			if(ops.keySet().isEmpty())
			{
				response.addError("no operation defined for path " + path);
				return response;
			}
			
			// iterate through path operations
			methodNames = ops.keySet().iterator();			
			
			while(methodNames.hasNext())
			{
				methodName = methodNames.next();								
				op = ops.get(methodName); // GET, POST, DELETE etc.	
				
				restOp = new RestOperation();	
				restOp.setBaseURL(baseUrl);
				restOp.setPath(path);
				restOp.setName(op.getOperationId() != null && op.getOperationId() != "" ? op.getOperationId() : (methodName.name() + " " + path));
				restOp.setLabel(op.getOperationId() != null && op.getOperationId() != "" ? op.getOperationId() : (methodName.name() + " " + path));
				restOp.setDoc(op.getDescription());
				
				method = new RestMethod();
				
				restParameters = new ArrayList<RestParameter>();
				restResponses = new ArrayList<RestResponse>();
				
				opSecurities = op.getSecurity();
				
				// populate RestAuth
				methodAuth = new RestAuth();
				methodAuth.setTokenUrl(PropertyGetter.getProperty("aac.authorization.token.url"));
				methodAuth.setAuthorizationUrl(PropertyGetter.getProperty("aac.authorization.authorize.baseurl"));
				methodAuth.setType("oauth2");
				methodAuth.setName("Bearer");
				methodAuth.setIn("header");
				methodAuth.setFlow("accessCode");
				methodAuth.setDescription("WeLive User Authentication");
				methodAuth.setScopes(generalScopes);
				// end populate RestAuth
				
				if(opSecurities != null)
				{	
					opScopes = null;
					
					for(int i = 0; i < opSecurities.size(); i++)
					{
						opScopes = opSecurities.get(i).get("weliveUserAuth");							
						
						if(opScopes != null)
							break;
					}
					
					// override general auth
					if(opScopes != null)
					{							
						methodAuth.setScopes(opScopes);
					}
				}
				
				// populate RestMethod parameters
				opPars = op.getParameters();
				
				boolean supported = true;
				
				if(opPars != null)
				{						
					// iterate through parameters
					for(int i = 0; i < opPars.size(); i++)
					{
						supported = true;
						
						par = opPars.get(i);
						
						restPar = new RestParameter();
						restPar.setName(par.getName());
						restPar.setLabel(par.getName());
						restPar.setRequired(par.getRequired());
						
						restDoc = new RestDoc();
						restDoc.setText(par.getDescription());
						restPar.setDoc(restDoc);
						
						if(par.getIn().equalsIgnoreCase("QUERY"))
						{
							QueryParameter qPar = (QueryParameter) par;
							
							restPar.setStyle(RestParameter.ParameterStyle.QUERY);								
							restPar.setType(qPar.getType());
							restPar.setDefaultValue(qPar.getDefaultValue());
						}
						else if(par.getIn().equalsIgnoreCase("PATH"))
						{
							PathParameter pPar = (PathParameter) par;
							
							restPar.setStyle(RestParameter.ParameterStyle.TEMPLATE);								
							restPar.setType(pPar.getType());
							restPar.setDefaultValue(pPar.getDefaultValue());
						}
						else if(par.getIn().equalsIgnoreCase("HEADER"))
						{
							HeaderParameter hPar = (HeaderParameter) par;
							
							restPar.setStyle(RestParameter.ParameterStyle.HEADER);								
							restPar.setType(hPar.getType());
							restPar.setDefaultValue(hPar.getDefaultValue());
						}
						else if(par.getIn().equalsIgnoreCase("BODY"))
						{
							BodyParameter bPar = (BodyParameter) par;
							
							restPar.setStyle(RestParameter.ParameterStyle.BODY);	
							
							if(bPar.getSchema() != null)
							{
								try 
								{
									restPar.setSchema(objectMapper.writeValueAsString(bPar.getSchema()));
								} 
								catch (JsonProcessingException e) 
								{
									e.printStackTrace();
									
									response.addError("Mapping schema error: " + e.getMessage());
									return response;
								}
							}
						}
						else
						{
							response.addWarning("(" + methodName.name() + " " + path + ") '" + par.getIn() + "' parameter not supported");
							
							supported = false;
						}
						
						if(supported)
							restParameters.add(restPar);
						
					} // end iterate through parameters
				}
				else
				{
					response.addWarning("(" + methodName.name() + " " + path + ") parameters are missing"); 
				}
				
				// populate RestMethod responses
				
				opRes = op.getResponses();
				
				if(opRes != null)
				{
					String status;
					
					Iterator<String> resKeys = opRes.keySet().iterator();
					
					List<Long> statusCodes;
					List<RestDoc> docs;
					
					while(resKeys.hasNext())
					{
						supported = true;
						
						status = resKeys.next();
						res = opRes.get(status);
						
						restRes = new RestResponse();
						
						statusCodes = new ArrayList<Long>();
						
						try {
							statusCodes.add(Long.parseLong(status));
						} catch(NumberFormatException e) {
							response.addWarning("(" + methodName.name() + " " + path + ") '" + status + "' response status not supported");
							supported = false;
						}
						
						restDoc = new RestDoc();
						restDoc.setText(res.getDescription());
						docs = new ArrayList<RestDoc>();
						docs.add(restDoc);
						
						restRes.setStatusCodes(statusCodes);							
						restRes.setDoc(docs);	
						
						if(res.getSchema() != null)
						{
							try 
							{
								restRes.setSchema(objectMapper.writeValueAsString(res.getSchema()));
							} 
							catch (JsonProcessingException e) 
							{
								e.printStackTrace();
								
								response.addError("Mapping schema error: " + e.getMessage());
								return response;
							}
						}
						
						if(supported)
							restResponses.add(restRes);
					}
				}
				else
				{
					response.addWarning("(" + methodName.name() + " " + path + ") responses are missing"); 
				}
				
				// populate RestMethod
				
				method.setHttpMethod(methodName.name().toUpperCase());
				method.setUrl(path);
				method.setParameters(restParameters);
				method.setResponses(restResponses);
				//method.setRepresentations(representations);
				
				if(methodAuth.getScopes() != null)
				{
					method.setAuthentication("true");
					method.setAuthInfo(methodAuth);
					
					restOp.setAuthentication("true");
					
					AuthProviderType oauthType = new AuthProviderType();
					oauthType.setType(VmeConstants.WELIVEAUTH);
					
					restOp.setAuthType(oauthType);
				}
				else
				{
					method.setAuthentication("false");	
					restOp.setAuthentication("false");
				}
				// end populate RestMethod

				try {
					restOp.setExecMethod(objectMapper.writeValueAsString(method));
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				response.addOperation(restOp);
				
			} // end iterate through path operations
		} // end iterate through paths
		
		response.setServiceCatalog(serviceCatalog);

		return response;
	}
	
	public SwaggerParsingResponse validateSwaggerService(String swagger)
	{
		SwaggerParsingResponse response;
		
		SwaggerDeserializationResult swaggerResults = new SwaggerParser().readWithInfo(swagger);
		
		// errors generated by SwaggerParser
		List<String> errors = swaggerResults.getMessages();
		
		if(errors.size() > 0)
		{
			response = new SwaggerParsingResponse();
			response.setErrors(errors);
			
			return response;
		}
		
		response = serviceFromSwagger(swagger);
		
		if(errors.size() > 0)
			response.setServiceCatalog(null);
		
		return response;
	}
	
	public ServiceCatalog addFromSwagger(String swagger, Profile profile)
	{
		ServiceCatalog serviceCatalog = null;

		SwaggerParsingResponse parsingResponse = validateSwaggerService(swagger);
		
		if(parsingResponse.getServiceCatalog() != null)
		{
			serviceCatalog = parsingResponse.getServiceCatalog();
			
			serviceCatalog.setProfile(profile);
			serviceCatalog.setVisibility(profile.getAccount().getRole() == VmeConstants.ACCOUNT_ROLE_ADMIN ? VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC : VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE);

			serviceCatalogDAO.create(serviceCatalog);
			
			Iterator<RestOperation> operations = parsingResponse.getOperations().iterator();
			RestOperation restOp;
			
			while(operations.hasNext())
			{
				restOp = operations.next();
				restOp.setServiceCatalog(serviceCatalog);
				restOperationDAO.create(restOp);
			}
		}
		
		return serviceCatalog;
	}
	
	// IVAN
	// map Metamodel object to ServiceCatalog
	
	public ServiceCatalog addRESTFromMetamodel(String descriptor, Profile profile)
	{
		MappingResponse mappingResponse = new BuildingBlockMapping().toBuildingBlock(descriptor);
		
		if(mappingResponse.getErrors().size() > 0)
		{
			logger.error(mappingResponse.getErrors().toString());
			return null;
		}
		
		return addRESTFromMetamodel(mappingResponse.getBuildingBlock(), profile);
	}
	
	public ServiceCatalog addRESTFromMetamodel(BuildingBlock descriptor, Profile profile)
	{
		ServiceCatalog serviceCatalog = new ServiceCatalog();
		
		BuildingBlock bb = descriptor;
		
		// set general info
		serviceCatalog.setName(bb.getTitle());
		serviceCatalog.setCreatedAt(new Date());
		serviceCatalog.setUpdatedAt(new Date());
		
		try 
		{
			serviceCatalog.setContent(new ObjectMapper().writeValueAsString(bb));
		} 
		catch (JsonProcessingException e) 
		{
			e.printStackTrace();
		}
		serviceCatalog.setType(VmeConstants.SERVICECATALOG_TYPE_REST);
		serviceCatalog.setSubType(VmeConstants.SERVICECATALOG_TYPE_REST_METAMODEL);	
		
		serviceCatalog.setProfile(profile);
		serviceCatalog.setVisibility(profile.getAccount().getRole() == VmeConstants.ACCOUNT_ROLE_ADMIN ? VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC : VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE);
		
		List<RestOperation> restOps = new ArrayList<RestOperation>();
		
		RestOperation restOp = null;
		RestMethod method = null;
		RestAuth methodAuth = null;		
		List<RestParameter> restParameters = null;
		List<RestResponse> restResponses = null;
		RestParameter restPar = null;
		RestResponse restRes = null;
		List<String> scopes = null;
		List<RestDoc> restDocs = null;
		RestDoc restDoc = null;
		
		Iterator<it.eng.metamodel.definitions.Operation> ops = null;
		it.eng.metamodel.definitions.Operation op = null;
		AuthenticationMeasure authMeasure = null;
		Iterator<Permission> permissions = null;
		Permission permission = null;
		Iterator<it.eng.metamodel.definitions.Parameter> parameters = null;
		it.eng.metamodel.definitions.Parameter parameter = null;
		Iterator<it.eng.metamodel.definitions.Response> responses = null;
		it.eng.metamodel.definitions.Response response = null;
		
		ops = bb.getOperations().iterator();
			
		// iterate through operations
		while(ops.hasNext())
		{
			op = ops.next();
			
			restOp = new RestOperation();
			
			restOp.setName(op.getTitle());
			restOp.setLabel(op.getTitle());
			restOp.setBaseURL("");
			restOp.setDoc(op.getDescription());
			restOp.setPath(op.getEndpoint());
			
			method = new RestMethod();
			
			method.setHttpMethod(op.getMethod());
			method.setUrl(op.getEndpoint());
			
			boolean oauth2Protocol = false;
			
			// set method authorization (if any)
			if(op.getSecurityMeasure() != null)
			{
				oauth2Protocol = false;
				
				authMeasure = (AuthenticationMeasure) op.getSecurityMeasure();
				
				oauth2Protocol = authMeasure.getProtocolType().compareToIgnoreCase("OAUTH2") == 0;
				
				if(oauth2Protocol)
				{
					methodAuth = new RestAuth();
					
					methodAuth.setTokenUrl(PropertyGetter.getProperty("aac.authorization.token.url"));
					methodAuth.setAuthorizationUrl(PropertyGetter.getProperty("aac.authorization.authorize.baseurl"));
					methodAuth.setType("oauth2");
					methodAuth.setName("Bearer");
					methodAuth.setIn("header");
					methodAuth.setFlow("accessCode");
					methodAuth.setDescription(authMeasure.getDescription());
					
					permissions = authMeasure.getPermissions().iterator();
					scopes = new ArrayList<String>();
					
					while(permissions.hasNext())
					{
						permission = permissions.next();
						
						scopes.add(permission.getIdentifier());
					}
					
					methodAuth.setScopes(scopes);
					
					method.setAuthentication("true");
					method.setAuthInfo(methodAuth);
					
					restOp.setAuthentication("true");
					
					AuthProviderType oauthType = new AuthProviderType();
					oauthType.setType(VmeConstants.WELIVEAUTH);
					
					restOp.setAuthType(oauthType);
				}
				else
				{
					method.setAuthentication("false");
					restOp.setAuthentication("false");
				}
			}
			else
			{
				method.setAuthentication("false");
				restOp.setAuthentication("false");
			}
			
			// set method parameters
			
			parameters = op.getParameters().iterator();
			restParameters = new ArrayList<RestParameter>();
			
			while(parameters.hasNext())
			{
				parameter = parameters.next();
				
				restPar = new RestParameter();
				
				restPar.setName(parameter.getName());
				restPar.setLabel(parameter.getName());
				
				restDoc = new RestDoc();
				restDoc.setText(parameter.getDescription());
				
				restPar.setDoc(restDoc);
				restPar.setRequired(parameter.isRequired());
				restPar.setType(parameter.getType());
				restPar.setSchema(parameter.getSchema());
				
				if(parameter.getIn().compareToIgnoreCase("QUERY") == 0)
					restPar.setStyle(RestParameter.ParameterStyle.QUERY);
				else if(parameter.getIn().compareToIgnoreCase("PATH") == 0)
					restPar.setStyle(RestParameter.ParameterStyle.TEMPLATE);
				else if(parameter.getIn().compareToIgnoreCase("BODY") == 0)
					restPar.setStyle(RestParameter.ParameterStyle.BODY);
				else if(parameter.getIn().compareToIgnoreCase("HEADER") == 0)
					restPar.setStyle(RestParameter.ParameterStyle.HEADER);
				else if(parameter.getIn().compareToIgnoreCase("MATRIX") == 0)
					restPar.setStyle(RestParameter.ParameterStyle.MATRIX);
				else if(parameter.getIn().compareToIgnoreCase("PLAIN") == 0)
					restPar.setStyle(RestParameter.ParameterStyle.PLAIN);
				
				if(!(oauth2Protocol && restPar.getName().compareToIgnoreCase("authorization") == 0 && restPar.getStyle() == RestParameter.ParameterStyle.HEADER))
					restParameters.add(restPar);
			}
			
			method.setParameters(restParameters);
			
			// set method responses
			
			responses = op.getResponses().iterator();
			restResponses = new ArrayList<RestResponse>();
			
			while(responses.hasNext())
			{
				response = responses.next();
				
				restRes = new RestResponse();
				
				restDoc = new RestDoc();
				restDocs = new ArrayList<RestDoc>();
				restDoc.setText(response.getDescription());
				restDocs.add(restDoc);
				
				restRes.setDoc(restDocs);
				restRes.setSchema(response.getSchema());
				
				List<Long> statusCodes = new ArrayList<Long>();					
				statusCodes.add(Long.valueOf((long)response.getCode()));
				
				restRes.setStatusCodes(statusCodes);
				
				restResponses.add(restRes);
			}
			
			method.setResponses(restResponses);
			//method.setRepresentations();	
			
			try 
			{
				restOp.setExecMethod(objectMapper.writeValueAsString(method));
			} 
			catch (JsonProcessingException e) 
			{
				e.printStackTrace();
			}
			
			restOps.add(restOp);

		} // end iterate through operations
		
		// create db entries
		if(restOps.size() > 0)
		{
			serviceCatalogDAO.create(serviceCatalog);
			
			Iterator<RestOperation> operations = restOps.iterator();
			
			while(operations.hasNext())
			{
				restOp = operations.next();
				restOp.setServiceCatalog(serviceCatalog);
				restOperationDAO.create(restOp);
			}
		}
		else
			return null;
				
		return serviceCatalog;
	}
}
