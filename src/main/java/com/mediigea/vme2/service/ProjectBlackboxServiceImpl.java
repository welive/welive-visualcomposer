package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.BlackboxDAO;
import com.mediigea.vme2.dao.ProjectBlackboxDAO;
import com.mediigea.vme2.dao.ProjectDAO;
import com.mediigea.vme2.entity.Blackbox;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectBlackbox;

public class ProjectBlackboxServiceImpl implements ProjectBlackboxService {

	@Autowired
	private ProjectBlackboxDAO projectBlackboxDAO;
	
	@Autowired
	private BlackboxDAO blackboxDAO;	
	
	@Autowired
	private ProjectDAO projectDAO;
	
	@Override
	public void create(int idProject, int idBlackbox) {
		
		Project project = projectDAO.getById(idProject);
		
		ProjectBlackbox pblackbox = new ProjectBlackbox();
		pblackbox.setProject(project);		
		
		Blackbox blackbox = blackboxDAO.getById(idBlackbox);
		pblackbox.setBlackbox(blackbox);
		
		projectBlackboxDAO.create(pblackbox);
	}
	
	public void deleteAllByProject(int idProject){
		
		projectBlackboxDAO.deleteAllByProject(idProject);
	}
	
	public List<ProjectBlackbox> getAllByProject(int idProject){
		return projectBlackboxDAO.getAllByProject(idProject);
	}

	public List<ProjectBlackbox> getAllByBlackbox(int idBlackbox){
		return projectBlackboxDAO.getAllByBlackbox(idBlackbox);
	}
}
