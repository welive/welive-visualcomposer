package com.mediigea.vme2.service;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.IdeaWorkgroup;

@Transactional
public interface IdeaWorkgroupService 
{
	public IdeaWorkgroup addIdeaWorkgroup(IdeaWorkgroup iw);
	public IdeaWorkgroup updateIdeaWorkgroup(IdeaWorkgroup iw);
	public void deleteIdeaWorkgroup(int ideaId);
	
	public IdeaWorkgroup getIdeaWorkgroupByIdeaId(int ideaId);
	public IdeaWorkgroup getIdeaWorkgroupByWorkgroupId(int wgId);
}
