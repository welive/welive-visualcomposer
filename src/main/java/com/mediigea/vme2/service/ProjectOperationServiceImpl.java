package com.mediigea.vme2.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.ProjectDAO;
import com.mediigea.vme2.dao.ProjectOperationDAO;
import com.mediigea.vme2.dao.RestOperationDAO;
import com.mediigea.vme2.dao.SoapOperationDAO;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.util.VmeConstants;

public class ProjectOperationServiceImpl implements ProjectOperationService {

	@Autowired
	private ProjectOperationDAO projectOperationDAO;
	
	@Autowired
	private RestOperationDAO restDAO;
	
	@Autowired
	private SoapOperationDAO soapDAO;
	
	@Autowired
	private ProjectDAO projectDAO;
	
	@Override
	public void create(int idProject, int idOperation, OperationType type) {
		
		Project project = projectDAO.getById(idProject);
		
		ProjectOperation operation = new ProjectOperation();
		operation.setProject(project);		
		
		if(OperationType.REST == type){
			RestOperation restOp = restDAO.getById(idOperation);

			operation.setRestOperation(restOp);
						
		} else{
			SoapOperation soapOp = soapDAO.getById(idOperation);

			operation.setSoapOperation(soapOp);
		}
		
		projectOperationDAO.create(operation);
	}
	
	public void deleteAllByProject(int idProject){
		
		projectOperationDAO.deleteAllByProject(idProject);
	}
	
	public List<ProjectOperation> getAllByProject(int idProject){
		return projectOperationDAO.getAllByProject(idProject);
	}
	
	public List<Project> findMashupsByOperationId(Integer id, int type)
	{
		String hql = "";
		
		if(type == VmeConstants.SERVICECATALOG_TYPE_REST)
			hql = " from ProjectOperation p join fetch p.restOperation o join fetch p.project pp where o.id=:opId";
		else
			hql = " from ProjectOperation p join fetch p.soapOperation o join fetch p.project pp where o.id=:opId";
		
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("opId", id);

		List<ProjectOperation> projOp = projectOperationDAO.getAllBy(hql, parameters);
		
		List<Project> projects = new ArrayList<Project>();
		
		for(int i = 0; i < projOp.size(); i++)
			projects.add(projOp.get(i).getProject());
		
		return projects;
	}

}
