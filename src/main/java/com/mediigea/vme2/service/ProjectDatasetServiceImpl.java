package com.mediigea.vme2.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.ODSResourceDAO;
import com.mediigea.vme2.dao.ProjectDAO;
import com.mediigea.vme2.dao.ProjectDatasetDAO;
import com.mediigea.vme2.entity.ODSResource;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectDataset;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.util.VmeConstants;

public class ProjectDatasetServiceImpl implements ProjectDatasetService {
	
	@Autowired
	private ProjectDatasetDAO projectDatasetDAO;
	
	@Autowired
	private ODSResourceDAO odsResourceDAO;
	
	@Autowired
	private ProjectDAO projectDAO;
	
	@Override
	public void create(int idProject, int idResource) {
		
		ProjectDataset pd = new ProjectDataset();
		pd.setProject(projectDAO.getById(idProject));		
		
		pd.setResource(odsResourceDAO.getById(idResource));
		
		projectDatasetDAO.create(pd);
	}
	
	public void deleteAllByProject(int idProject){
		
		projectDatasetDAO.deleteAllByProject(idProject);
	}
	
	public List<ProjectDataset> getAllByProject(int idProject){
		return projectDatasetDAO.getAllByProject(idProject);
	}
	
	public List<ODSResource> getAllDatasetsByProject(int idProject){
		List<ProjectDataset> list = getAllByProject(idProject);
		
		List<ODSResource> resources = new ArrayList<ODSResource>();
		
		for(int i = 0; i < list.size(); i++)
			resources.add(list.get(i).getResource());
		
		return resources;			
	}
	
	public List<Project> findMashupsByResourceId(Integer id)
	{
		String hql = " from ProjectDataset p join fetch p.resource r join fetch p.project pp where r.id=:resId";
		
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("resId", id);

		List<ProjectDataset> projRes = projectDatasetDAO.getAllBy(hql, parameters);
		
		List<Project> projects = new ArrayList<Project>();
		
		for(int i = 0; i < projRes.size(); i++)
			projects.add(projRes.get(i).getProject());
		
		return projects;
	}
}
