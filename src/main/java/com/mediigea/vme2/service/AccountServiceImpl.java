package com.mediigea.vme2.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediigea.vme2.auth.AuthenticationException;
import com.mediigea.vme2.auth.RegistrationException;
import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dao.AccountDAO;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Activity;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.util.SimpleCrypto;
import com.mediigea.vme2.util.VmeConstants;

@Service("accountService")
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;

	@Autowired
	private ProfileDAO profileDAO;

	@Override
	public Account addAccount(Account a) {

		accountDAO.create(a);
		
		/*****
		 * activity log
		 */
		Activity ac = new Activity();
		ac.setActivityType(AbstractDAO.ActivityType.REGISTER_USER.getValue());
		ac.setDescription("Register User:" + a.getEmail());
		ac.setProfile(a.getProfile());
		accountDAO.createAudit(ac);
		/*****
		 * activity log
		 */
		
		return a;
	}

	@Override
	public Account updateAccount(Account a) {
		a.setUpdatedAt(new Date());
		accountDAO.update(a);
		
		return a;
	}


	@Override
	public void logoutUser(Account a) {
		
		/*****
		 * activity log
		 */
		Activity ac = new Activity();
		ac.setActivityType(AbstractDAO.ActivityType.USER_LOGOUT.getValue());
		ac.setDescription("User Logout:" + a.getEmail());
		ac.setProfile(a.getProfile());
		accountDAO.createAudit(ac);
		/*****
		 * activity log
		 */	
	}	

	@Override
	public void loginUser(Account a) {
		
		/*****
		 * activity log
		 */
		Activity ac = new Activity();
		ac.setActivityType(AbstractDAO.ActivityType.USER_LOGIN.getValue());
		ac.setDescription("User Login:" + a.getEmail());
		ac.setProfile(a.getProfile());
		accountDAO.createAudit(ac);
		/*****
		 * activity log
		 */	
	}		
	
	
	@Override
	public Account verifyAccount(String email, String password, int accountRole) throws AuthenticationException {
		if (email == null || password == null || "".equals(email) || "".equals(password)) 
			throw new AuthenticationException("invalid credential");

		Account account = accountDAO.getByUserName(email);

		if (account == null) {
			throw new AuthenticationException("message.login.error.notfound");
		}

		if (account.getStatus() != VmeConstants.ACCOUNT_STATUS_ENABLED) {
			throw new AuthenticationException("Account is not enabled");
		}

		if (account.getRole() != accountRole) {
			throw new AuthenticationException("Not authorized");
		}

		if (!account.getPsw().equals(SimpleCrypto.md5(password))) {
			throw new AuthenticationException("invalid password");
		}

		/*****
		 * activity log
		 */
		Activity ac = new Activity();
		ac.setActivityType(AbstractDAO.ActivityType.USER_LOGIN.getValue());
		ac.setDescription("User Login:" + email);
		ac.setProfile(account.getProfile());
		accountDAO.createAudit(ac);
		/*****
		 * activity log
		 */
		
		return account;
	}

	@Override
	public Account verifyAccount(String validatedOauthId) throws AuthenticationException {

		if (validatedOauthId == null || validatedOauthId.isEmpty())
			throw new AuthenticationException("invalid credential");

		String hql = " from Account as account where account.validatedOauthId = :validatedOauthId ";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("validatedOauthId", SimpleCrypto.md5(validatedOauthId));

		List<Account> list = accountDAO.getAllBy(hql, parameters);

		if (list == null || list.size() != 1) {
			throw new AuthenticationException(VmeConstants.MESSAGE_ACCOUNT_NOT_FOUND);
		}

		Account account = list.get(0);
		if (account.getRole() != VmeConstants.ACCOUNT_ROLE_USER) {
			throw new AuthenticationException(VmeConstants.MESSAGE_ACCOUNT_NOT_AUTHORIZED);
		}

		if (account.getStatus() != VmeConstants.ACCOUNT_STATUS_ENABLED) {
			throw new AuthenticationException(VmeConstants.MESSAGE_ACCOUNT_NOT_ENABLED);
		}

		return account;
	}

	@Override
	public Account registrationTo(Account a, int userType) throws RegistrationException {

		if (!a.getPsw().equals(a.getConfirmPsw())) {
			throw new RegistrationException("message.registration.error.passwordsnotmatch");
		}

		// check email
		Account a1 = accountDAO.getByUserName(a.getEmail());

		if (a1 != null) {
			throw new RegistrationException("message.registration.error.emailexists");
		}

		Profile p = a.getProfile();

		p.setCreatedAt(new Date());
		p.setUpdatedAt(new Date());
		p.setEmail(a.getEmail());

		a.setPsw(SimpleCrypto.md5(a.getPsw()));
		a.setValidatedOauthId(SimpleCrypto.md5(a.getValidatedOauthId()));
		a.setRole(userType);
		a.setStatus(VmeConstants.ACCOUNT_STATUS_PENDING);
		a.setCreatedAt(new Date());
		a.setUpdatedAt(new Date());

		profileDAO.create(p);
		a.setProfile(p);
		accountDAO.create(a);

		/*****
		 * activity log
		 */
		Activity ac = new Activity();
		ac.setActivityType(AbstractDAO.ActivityType.REGISTER_USER.getValue());
		ac.setDescription("User Registration:" + a.getEmail());
		ac.setProfile(p);
		accountDAO.createAudit(ac);
		/*****
		 * activity log
		 */		
		
		
		return a;
	}

	
	@Override
	public List<Account> findAll() {

		return accountDAO.getAll();

	}

	@Override
	public Account getById(Integer accountId){
		return accountDAO.getById(accountId);
	}
	
	@Override
	public Account deleteAccount(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Account setStatus(Integer accountId, boolean enabled) {

		Account a = accountDAO.getById(accountId);
		a.setStatus(enabled ? VmeConstants.ACCOUNT_STATUS_ENABLED : VmeConstants.ACCOUNT_STATUS_DISABLED);

		accountDAO.update(a);
		
		/*****
		 * activity log
		 */
		Activity ac = new Activity();
		if (enabled) {
			ac.setActivityType(AbstractDAO.ActivityType.ENABLE_USER.getValue());
			ac.setDescription("Enable User:" + a.getEmail());
		}
		else { 
			ac.setActivityType(AbstractDAO.ActivityType.DISABLE_USER.getValue());
			ac.setDescription("Disable User:" + a.getEmail());
		}
		
		ac.setProfile(a.getProfile());
		accountDAO.createAudit(ac);
		/*****
		 * activity log
		 */
		
		
		return a;
	}

}
