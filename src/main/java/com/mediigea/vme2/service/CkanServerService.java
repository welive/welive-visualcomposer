package com.mediigea.vme2.service;

import java.io.IOException;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.CkanServer;
import com.mediigea.vme2.entity.Profile;

/**
 * Interface to define management operations about services (Rest/Soap) catalog
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface CkanServerService {
	
	public CkanServer add(String name, String description, String url, Profile profile);		
	
	public CkanServer setVisibility(Integer id, Integer code);

	public void delete(Integer id) throws Exception;

	public List<CkanServer> getAll();
	
	public List<CkanServer> findPublicCkanServer();
	
//	public List<CkanServer> findVisibleResources(Integer profileId);

	public CkanServer getById(Integer id);

//	public List<OpenDataResourceDTO> getOpenDataResourcesDTO(Profile p);
}
