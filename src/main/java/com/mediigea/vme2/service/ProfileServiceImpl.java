package com.mediigea.vme2.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediigea.vme2.dao.AccountDAO;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.dao.ProfileProjectDAO;
import com.mediigea.vme2.dao.ProfileWorkgroupDAO;
import com.mediigea.vme2.dao.UnreadCommentDAO;
import com.mediigea.vme2.dao.WorkgroupDAO;
import com.mediigea.vme2.dto.ProfileDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProfileProject;
import com.mediigea.vme2.entity.ProfileWorkgroup;
import com.mediigea.vme2.entity.ProfileWorkgroupId;
import com.mediigea.vme2.entity.UnreadComment;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.util.PagedSearch;
import com.mediigea.vme2.util.SimpleCrypto;
import com.mediigea.vme2.util.VmeConstants;

@Service("profileService")
public class ProfileServiceImpl implements ProfileService {
	
	private static final Logger logger = LoggerFactory.getLogger(ProfileServiceImpl.class);

	@Autowired
	private ProfileDAO profileDAO;

	@Autowired
	private ProfileProjectDAO profileProjectDAO;	
	
	@Autowired
	private AccountDAO accountDAO;

	@Autowired
	private WorkgroupDAO workgroupDAO;

	@Autowired
	private ProfileWorkgroupDAO profileworkgroupDAO;

	@Autowired
	private UnreadCommentDAO unreadCommentDAO;
	
	//private static final Logger logger = LoggerFactory.getLogger(ProfileServiceImpl.class);

	@Override
	public Profile addProfile(Profile a) {

		profileDAO.create(a);
		return a;
	}

	@Override
	public Profile updateProfile(Profile a) {

		profileDAO.update(a);
		return a;
	}

	@Override
	public void deleteProfile(Integer id) {
		
		logger.info("Delete profile");

		Profile p = profileDAO.getById(id);
		
		for (Comment c: p.getComments()) {
			unreadCommentDAO.deleteByCommentId(c.getId());
		}

		for (UnreadComment uc: unreadCommentDAO.getUnreadCommentsByProfileId(id)) {
			unreadCommentDAO.delete(uc);
		}
		
		
		
		for (ProfileProject pp : p.getProfileProjects()) {
			profileProjectDAO.delete(pp);
		}
		
		profileDAO.delete(p);
		
	}

	@Override
	public Profile findProfile(Integer id) {

		return profileDAO.getById(id);
	}

	@Override
	public Profile findProfileByServiceCatalog(Integer id) {

		if (id == null)
			return null;

		String hql = "select profile from Profile as profile join profile.catalogs as service where service.id=:serviceId";
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("serviceId", id);

		List<Profile> pList = profileDAO.getAllBy(hql, params);

		if (pList == null || pList.isEmpty() || (pList != null && pList.size() > 1))
			return null;

		return pList.get(0);
	}
	
	@Override
	public Profile findProfileByOpenDataCatalog(Integer id) {

		if (id == null)
			return null;

		String hql = "select profile from Profile as profile join profile.openDataCatalogs as odata where odata.id=:odataId";
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("odataId", id);

		List<Profile> pList = profileDAO.getAllBy(hql, params);

		if (pList == null || pList.isEmpty() || (pList != null && pList.size() > 1))
			return null;

		return pList.get(0);
	}	

	@Override
	public List<Profile> findAll() {

		return profileDAO.getAll();

	}

	@Override
	public List<Profile> findByWorkgroup(Workgroup w) {
		String hql = "from Profile p join fetch p.profileWorkgroups pw where pw.pk.workgroup.id=:wid";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("wid", w.getId());
		return profileDAO.getAllBy(hql, parameters);

	}

	@Override
	public List<Profile> findByRole(int role) {

		String hql = " Select distinct p from Profile p left join fetch p.profileWorkgroups pw " +
				     " where p.account.role = :rl order by p.firstName, p.lastName ";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("rl", role);
		return profileDAO.getAllBy(hql, parameters);
	}

	@Override
	public PagedSearch<ProfileDTO> pagedSearch(String searchTerm, int page, int pageCount) {
		EntityManager em = profileDAO.getEntityManager();

		String from = " from Profile p " +
				      " where  ( lower(p.firstName) like :st or lower(p.lastName) like :st or lower(p.email) like :st ) " +
				      " and p.account.role=" + VmeConstants.ACCOUNT_ROLE_USER;

		String hql = " Select new com.mediigea.vme2.dto.ProfileDTO(p.id, p.email, p.firstName || ' ' || p.lastName) " + from;
		String hqlCount = " Select count(*)  " + from;

		String st = "%" + searchTerm.toLowerCase() + "%";

		Query q = em.createQuery(hql);
		q.setParameter("st", st);
		if (pageCount > 0 && page > 0) {
			q.setFirstResult((page - 1) * pageCount);
			q.setMaxResults(pageCount);
		}

		Query qCount = em.createQuery(hqlCount);
		qCount.setParameter("st", st);

		@SuppressWarnings("unchecked")
		List<ProfileDTO> list = q.getResultList();
		Long count = (Long) qCount.getSingleResult();

		return new PagedSearch<ProfileDTO>(list, count.intValue());

	}

	@Override
	public void workgroupsAdd(Integer profileId, List<Integer> workgroupIds) {

		for (Integer id : workgroupIds) {
			this.workgroupAdd(profileId, id);			
		}

	}

	@Override
	public void workgroupAdd(Integer profileId, Integer workgroupId) {

		Profile profile = profileDAO.getById(profileId);
		Workgroup w = workgroupDAO.getById(workgroupId);
		ProfileWorkgroup entity = new ProfileWorkgroup();
		entity.setProfile(profile);
		entity.setWorkgroup(w);
		entity.setWorkgroupRole(0);
		entity.setCreatedAt(new Date());
		profileworkgroupDAO.create(entity);

	}

	@Override
	public void workgroupRemove(Integer profileId, Integer workgroupId) {
		Profile p = profileDAO.getById(profileId);
		Workgroup w = workgroupDAO.getById(workgroupId);
		ProfileWorkgroupId pk = new ProfileWorkgroupId(p, w);

		ProfileWorkgroup pw = profileworkgroupDAO.getById(pk);
		profileworkgroupDAO.delete(pw);

	}

	@Override
	public void addAdministrator(Profile p) {
		
		
		p.setCreatedAt(new Date());
		p.setUpdatedAt(new Date());				
		
		
		Account a = p.getAccount();
		
		a.setEmail(p.getEmail());
		a.setPsw(SimpleCrypto.md5(a.getPsw()));
		a.setCreatedAt(new Date());
		a.setUpdatedAt(new Date());
		a.setProvider(VmeConstants.AUTH_PROVIDER_VME);
		a.setRole(VmeConstants.ACCOUNT_ROLE_ADMIN);
		a.setStatus(VmeConstants.ACCOUNT_STATUS_ENABLED);
		
		
		profileDAO.create(p);
		a.setProfile(p);
		accountDAO.create(a);
		
	}
	
	public PagedSearch<ProfileDTO> pagedSearchByWorkgroup(Integer workgroupId, String searchTerm, int page, int pageCount){
		EntityManager em = profileDAO.getEntityManager();

		String from = " from Profile p join p.profileWorkgroups pw " +
				      " where  ( lower(p.firstName) like :st or lower(p.lastName) like :st or lower(p.email) like :st ) " +
				      " and pw.pk.workgroup.id=:wId ";

		String hql = " Select new com.mediigea.vme2.dto.ProfileDTO(p.id, p.email, p.firstName || ' ' || p.lastName) " + from;
		String hqlCount = " Select count(*)  " + from;

		String st = "%" + searchTerm.toLowerCase() + "%";

		Query q = em.createQuery(hql);
		q.setParameter("st", st);
		q.setParameter("wId", workgroupId);
		
		if (pageCount > 0 && page > 0) {
			q.setFirstResult((page - 1) * pageCount);
			q.setMaxResults(pageCount);
		}

		Query qCount = em.createQuery(hqlCount);
		qCount.setParameter("st", st);
		qCount.setParameter("wId", workgroupId);

		@SuppressWarnings("unchecked")
		List<ProfileDTO> list = q.getResultList();
		Long count = (Long) qCount.getSingleResult();

		return new PagedSearch<ProfileDTO>(list, count.intValue());
		
	}
	
	// IVAN
	public Profile findProfileByCcUserId(Integer ccUserId) {

		if (ccUserId == null)
			return null;

		String hql = "select profile from Profile as profile where profile.ccUserId=:ccUserId";
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("ccUserId", ccUserId);

		List<Profile> pList = profileDAO.getAllBy(hql, params);

		if (pList == null || pList.isEmpty() || (pList != null && pList.size() > 1))
			return null;

		return pList.get(0);
	}
	
	
	// IVAN
	public Profile findProfileByEmail(String email) {

		if (email == null)
			return null;

		String hql = "select profile from Profile as profile where profile.email=:email";
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("email", email);

		List<Profile> pList = profileDAO.getAllBy(hql, params);

		if (pList == null || pList.isEmpty() || (pList != null && pList.size() > 1))
			return null;

		return pList.get(0);
	}
	
	// IVAN
	public List<Profile> findWorkgroupMembers(Integer workgroupId)
	{
		if (workgroupId == null)
			return null;

		String hql = "select p from Profile as p join fetch p.profileWorkgroups as pw where pw.pk.workgroup.id=:wid";
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("wid", workgroupId);

		List<Profile> pList = profileDAO.getAllBy(hql, params);

		if (pList == null || pList.isEmpty())
			return null;
		
		return pList;
	}
}
