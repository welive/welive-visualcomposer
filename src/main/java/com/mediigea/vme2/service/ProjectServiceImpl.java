package com.mediigea.vme2.service;

import io.swagger.util.Json;
import it.eng.metamodel.definitions.Dependency;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dao.BlackboxDAO;
import com.mediigea.vme2.dao.CommentDAO;
import com.mediigea.vme2.dao.GuiProjectDAO;
import com.mediigea.vme2.dao.GuiProjectProjectDAO;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.dao.ProfileProjectDAO;
import com.mediigea.vme2.dao.ProjectDAO;
import com.mediigea.vme2.dao.ProjectInnerProcessDAO;
import com.mediigea.vme2.dao.ProjectOperationDAO;
import com.mediigea.vme2.dao.UnreadCommentDAO;
import com.mediigea.vme2.dto.GuiFileResourceDTO;
import com.mediigea.vme2.dto.InputField;
import com.mediigea.vme2.dto.InputShapeDTO;
import com.mediigea.vme2.dto.ProjectDTO;
import com.mediigea.vme2.dto.ProjectExportBean;
import com.mediigea.vme2.dto.ServiceOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.dto.ServiceShapeDTO;
import com.mediigea.vme2.dto.ShapeContainer;
import com.mediigea.vme2.dto.ShapeDTO;
import com.mediigea.vme2.dto.UD_InputShapeDTO;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.GuiProjectFile;
import com.mediigea.vme2.entity.GuiProjectProject;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProfileProject;
import com.mediigea.vme2.entity.ProfileProjectId;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectDataset;
import com.mediigea.vme2.entity.ProjectInnerProcess;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.ServiceCatalogExt;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.entity.UnreadComment;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.exceptions.ImportProjectException;
import com.mediigea.vme2.exceptions.NotSupportedMimeTypeException;
import com.mediigea.vme2.exceptions.SelectShapeException;
import com.mediigea.vme2.exceptions.WorkflowScriptException;
import com.mediigea.vme2.exceptions.WriteResourceException;
import com.mediigea.vme2.service.GuiEditorServiceImpl.ResourceContentType;
import com.mediigea.vme2.util.VmeConstants;

@Service("projectService")
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectDAO projectDAO;

	@Autowired
	private ProfileDAO profileDAO;
	

	@Autowired
	private ProfileProjectDAO profileprojectDAO;
	
	@Autowired
	private ProjectOperationDAO projectOperationDAO;

	@Autowired
	private CommentDAO commentDAO;	
	
	@Autowired
	private GuiProjectDAO guiProjectDAO;	
	
	@Autowired
	private GuiProjectProjectDAO guiProjectProjectDAO;

	@Autowired
	private ObjectMapperExt objectMapper;

	@Autowired
	private SoapOperationService soapOperationService;

	@Autowired
	private RestOperationService restOperationService;
	
	@Autowired
	private ProjectOperationService projectOperationService;	
	
	@Autowired
	private ProjectDatasetService projectDatasetService;
	
	@Autowired
	private ProjectInnerProcessDAO projectInnerProcessDAO;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private CommentService commentService;

	@Autowired
	private BlackboxDAO blackBoxDAO;
	
	@Autowired
	private WorkflowService workflowService;		
	
	@Autowired
	private UnreadCommentDAO unreadCommentDAO;
	
	@Autowired
	private ServiceCatalogExtService catalogExtService;
	
	private static final Logger logger = LoggerFactory.getLogger(ProjectServiceImpl.class);

	@Override
	public Project addProject(Project project) {

		project.setCreatedAt(new Date());
		project.setUpdatedAt(new Date());
		projectDAO.create(project);
		return project;
	}

	@Override
	public Project addProject(Project project, Profile owner) {

		logger.info("Add Project");

		if (owner == null)
			return null;

		project = addProject(project);

		ProfileProject pp = new ProfileProject(); // ppId, project, owner,
													// VmeConstants.PROFILE_PROJECT_ROLE_OWNER,
													// new Date(), new Date())

		pp.setProfile(owner);
		pp.setProject(project);
		pp.setProjectRole(VmeConstants.PROFILE_PROJECT_ROLE_OWNER);
		pp.setCreatedAt(new Date());

		profileprojectDAO.create(pp);

		return project;
	}

	@Override
	public Project updateProject(Project project) {

		project.setUpdatedAt(new Date());
		projectDAO.update(project);
		return project;
	}

	@Override
	public Project findProject(Integer id) {

		return projectDAO.getById(id);
	}

	@Override
	public List<Project> findAll(String hql, HashMap<String, Object> parameters) {

		if (hql == null || hql.isEmpty())
			return projectDAO.getAll();

		return projectDAO.getAllBy(hql, parameters);
	}

	public List<Project> findAllByProfile(Profile p) {

		if (p == null)
			return null;

		String hql = " from Project p join fetch p.workgroup join fetch p.profileProjects pp " + " where pp.pk.profile.id=:profileId ORDER BY p.updatedAt DESC";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("profileId", p.getId());

		return findAll(hql, parameters);
	}
	
	public List<Project> findAllByWorkgroup(Workgroup w) {

		if (w == null)
			return null;

		String hql = " from Project p join fetch p.workgroup w where w.id=:workgroupId";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("workgroupId", w.getId());

		return findAll(hql, parameters);
	}

	public List<ProjectDTO> findAllProjectDTOByProfile(Profile p) {

		List<Project> list = findAllByProfile(p);
		
		List<ProjectDTO> listDTO = new ArrayList<ProjectDTO>();
		int nuc = 0;
		ProjectDTO pd;
		for (Project item :list) {
			pd = new ProjectDTO();
			pd.setProject(item);
			List <UnreadComment> uc = commentService.getUnreadCommentsByProjectIdProfileId(item.getId(), p.getId());
			if (uc != null) {
				nuc = uc.size();
			}
            pd.setUnreadComment(nuc);
            pd.setOwner(findProjectOwner(item.getId()));
            listDTO.add(pd);
		}
		return listDTO;
	}
	
	@Override
	public List<ProjectDTO> searchProjectDTO(Profile p, String criteria) {

		List<Project> list = projectDAO.searchProject(p.getId(), criteria);
		
		List<ProjectDTO> listDTO = new ArrayList<ProjectDTO>();
		int nuc = 0;
		ProjectDTO pd;
		for (Project item :list) {
			pd = new ProjectDTO();
			pd.setProject(item);
            pd.setUnreadComment(nuc);
            listDTO.add(pd);
		}
		return listDTO;
	}	
	
	/**
	 * 
	 */
	@Override
	public String getDashboard(Integer projectId) {
		Project p = projectDAO.getById(projectId);
		return p.getDashboard();
	}

	
	private void addScopes(List<String> scopeList, List<String> scopes)
	{
		boolean isIn = false;
		
		for(int i = 0; i < scopes.size(); i++)
		{
			isIn = false;
			
			for(int j = 0; j < scopeList.size(); j++)
			{
				if(scopes.get(i).compareTo(scopeList.get(j)) == 0)
				{
					isIn = true;
					break;
				}
			}
			
			if(!isIn)
			{
				scopeList.add(scopes.get(i));
			}
		}
	}
	
	/**
	 * 
	 */
	@Override
	public void setDashboard(Integer projectId, String dashboard) {
		Project p = projectDAO.getById(projectId);
		p.setDashboard(dashboard);
		//p.setRenderingSchema(renderingSchema);
		
		// collect scopes
		List<String> scopes = new ArrayList<String>();
		
		List<ProjectOperation> services = projectOperationService.getAllByProject(projectId);
		
		List<ProjectInnerProcess> innerProcesses = projectInnerProcessDAO.getAllByProject(projectId);
		
		boolean authRequired = false;
		
		RestOperation restOp = null;
		Project innerProcess = null;
		
		try
		{
			// add scopes from rest services
			for(int i = 0; i < services.size(); i++)
			{
				if(services.get(i).getRestOperation() != null)
				{
					restOp = services.get(i).getRestOperation();
					restOp = restOperationService.findRestOperationById(restOp.getId());
					
					if(Boolean.parseBoolean(restOp.getAuthentication()) && restOp.getAuthType().getType().compareTo("WELIVEAUTH") == 0)
					{
						authRequired = true;
						addScopes(scopes, restOp.getRestMethod().getAuthInfo().getScopes());
					}
				}
			}
			
			// add scopes from inner processes
			for(int i = 0; i < innerProcesses.size(); i++)
			{
				innerProcess = innerProcesses.get(i).getInnerProcess();
				
				if(innerProcess.getAuthRequired() == 1)
				{
					authRequired = true;
					
					addScopes(scopes, Arrays.asList(innerProcess.getScopes().split(",")));
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		String scopeString = "";
		
		if(scopes.size() > 0)
		{
			scopeString += scopes.get(0);
			
			for(int i = 1; i < scopes.size(); i++)
			{
				scopeString += "," + scopes.get(i);
			}
		}
		
		p.setAuthRequired(authRequired ? 1 : 0);
		p.setScopes(scopeString);
		
		p.setUpdatedAt(new Date());
		projectDAO.update(p);
	}

	@Override
	public List<Comment> findProjectComments(Project p) {
		return findProjectComments(p, 0);
	}

	@Override
	public List<Comment> findProjectComments(Project p, int limit) {
		String hql = "from Comment as comment join fetch comment.profile where comment.project.id=:projectId order by comment.createdAt desc";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("projectId", p.getId());

		List<Comment> comments = commentDAO.getAllBy(hql, parameters, limit, 0);
		return comments;
	}

	@Override
	public int countComments(Project p) {
		String hql = "select count(*) from Comment as comment  where comment.project.id=:projectId";

		EntityManager em = commentDAO.getEntityManager();
		Long cc = (Long) em.createQuery(hql).setParameter("projectId", p.getId()).getSingleResult();

		// List<Object> row = commentDAO.getAllBy(hql, parameters);
		return cc.intValue();
	}

	@Override
	public List<Profile> findProjectMembers(Project project) {
		String hql = "from Profile p join fetch p.profileProjects pp where pp.pk.project.id=:pId";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pId", project.getId());
		List<Profile> members = profileDAO.getAllBy(hql, parameters);
		return members;
	}
	
	@Override
	public List<ProfileProject> findProfileProject(Project project) {
		String hql = "from ProfileProject pp join fetch pp.pk.project where pp.pk.project.id=:pId";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pId", project.getId());
		List<ProfileProject> members = profileprojectDAO.getAllBy(hql, parameters);
		
		return members;
	}
	

	@Override
	public void memberAdd(Integer projectId, Integer profileId) {
		Project prj = projectDAO.getById(projectId);
		Profile profile = profileDAO.getById(profileId);
		ProfileProject entity = new ProfileProject();
		entity.setProfile(profile);
		entity.setProject(prj);
		entity.setProjectRole(VmeConstants.PROFILE_PROJECT_ROLE_USER);
		entity.setCreatedAt(new Date());
		profileprojectDAO.create(entity);

	}
	
	@Override
	public void memberAddWithRole(Integer projectId, Integer profileId, Integer role) {
		Project prj = projectDAO.getById(projectId);
		Profile profile = profileDAO.getById(profileId);
		ProfileProject entity = new ProfileProject();
		entity.setProfile(profile);
		entity.setProject(prj);
		entity.setProjectRole(role);
		entity.setCreatedAt(new Date());
		profileprojectDAO.create(entity);
	}

	@Override
	public void memberRemove(Integer projectId, Integer profileId) {
		EntityManager em = projectDAO.getEntityManager();

		Project prj = projectDAO.getById(projectId);
		Profile profile = profileDAO.getById(profileId);

		ProfileProjectId pk = new ProfileProjectId(profile, prj);

		ProfileProject pp = em.find(ProfileProject.class, pk);
		em.remove(pp);
	}

	@Override
	public void deleteProject(Integer projectId) {

		Project prj = projectDAO.getById(projectId);
		
		if (prj.getBlackbox() != null) {
			
			blackBoxDAO.delete(prj.getBlackbox());
			
		}
		
		List<Profile> members = this.findProjectMembers(prj);
		for (Profile profile : members) {
			memberRemove(projectId, profile.getId());
		}

		List<Comment> comments = this.findProjectComments(prj);
		for (Comment comment : comments) {
			commentDAO.delete(comment);
			unreadCommentDAO.deleteByCommentId(comment.getId());
		}
		
		
		for (UnreadComment uc: unreadCommentDAO.getUnreadCommentsByProjectId(projectId)) {
			unreadCommentDAO.delete(uc);
		}
		
		
		List<ProjectOperation> opers = projectOperationDAO.getAllByProject(projectId);
		for(ProjectOperation po: opers){
			projectOperationDAO.delete(po);
		}
		
		Set <GuiProject> guiProjects = prj.getGuiProjects();
		for(GuiProject gp: guiProjects){
			gp.setProject(null);
		}		

		projectDAO.delete(prj);
	}

	@Override
	public OutputStream exportProject(Integer projectId) {

		Project prj = projectDAO.getById(projectId);

		String dashboard = prj.getDashboard();
		String renderingSchema = prj.getRenderingSchema();

		ProjectExportBean bean = new ProjectExportBean();
		bean.setDashboard(dashboard);

		/*
		 * int p1 = renderingSchema.indexOf("<?xml "); int p2 =
		 * renderingSchema.indexOf("?>"); if ( p1 != -1 && p2 != -1 && p2>p1){
		 * renderingSchema
		 * =renderingSchema.substring(0,p1)+renderingSchema.substring(p2+2); }
		 */
		bean.setRenderingSchema(renderingSchema);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		XMLEncoder e = new XMLEncoder(out);
		e.writeObject(bean);
		e.close();

		return out;
	}

	@Override
	public Profile findProjectOwner(Integer projectId) {
		Project prj = projectDAO.getById(projectId);
		String hql = "from Profile p join fetch p.profileProjects pp where pp.pk.project.id=:pId " + " and pp.projectRole="
				+ VmeConstants.PROFILE_PROJECT_ROLE_OWNER;
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pId", prj.getId());
		List<Profile> members = profileDAO.getAllBy(hql, parameters);

		return (members.size() == 1 ? members.get(0) : null);
	}

	public void importProject(MultipartFile mpf, Profile profile, String name, String description, int workgroupId) throws ImportProjectException {
		if (mpf != null) {

			logger.debug("Import resources from file");
			try {
				String xmlContent = IOUtils.toString(mpf.getInputStream(), "UTF-8");

				ByteArrayInputStream out = new ByteArrayInputStream(xmlContent.getBytes());
				XMLDecoder decoder = new XMLDecoder(out);

				ProjectExportBean projectBean = (ProjectExportBean) decoder.readObject();

				decoder.close();

				ShapeContainer shapeContainer = new ShapeContainer();
				shapeContainer.shapeSelector(projectBean.getDashboard(), objectMapper);

				// Prende tutti gli shape servizio per verificare che tutte le
				// operation
				// del progetto che si sta importando siano coerenti con quelle
				// esistenti nel db
				Map<String, ShapeDTO> services = shapeContainer.getServices();

				for (Map.Entry<String, ShapeDTO> service : services.entrySet()) {

					ServiceShapeDTO serviceDTO = (ServiceShapeDTO) service.getValue();

					ServiceOperationDTO operation = serviceDTO.getUserData().getOperation();

					int operationId = operation.getId();
					String operationName = operation.getName();

					int serviceType = operation.getServiceType();

					String matchingName = null;
					if (serviceType == VmeConstants.SERVICECATALOG_TYPE_SOAP) {
						SoapOperation operationEntity = soapOperationService.findSoapOperation(operationId);

						matchingName = operationEntity.getOperationName();

					} else if (serviceType == VmeConstants.SERVICECATALOG_TYPE_REST) {
						RestOperation operationEntity = restOperationService.findRestOperationById(operationId);

						matchingName = operationEntity.getName();
					}

					if (!operationName.equals(matchingName)) {
						throw new ImportProjectException("Operation: " + operationName + " [id:" + operationId + "] not found in database");
					}
				}

				

				Project project = new Project();
				project.setName(name);
				project.setDescription(description);
				project.setDashboard(projectBean.getDashboard());				

				Workgroup workgroup = new Workgroup();
				workgroup.setId(workgroupId);

				project.setWorkgroup(workgroup);

				this.addProject(project, profile);


				logger.debug("Building workflow schema");
				workflowService.buildWorkflowSchema(project.getId(), shapeContainer);

				
			} catch (IOException e) {
				String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new ImportProjectException(error);

			} catch (SelectShapeException e) {
				String error = messageSource.getMessage("message.error.importprojectarchive", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new ImportProjectException(error);
				
			} catch (WorkflowScriptException e) {
				String error = messageSource.getMessage("message.error.importprojectarchive", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new ImportProjectException(error);
			}
		}
	}
	
	@Override
	public List<InputShapeDTO> readInputShapesList(Integer projectId) throws SelectShapeException{
		Project project = this.findProject(projectId);

		ShapeContainer shapeContainer = new ShapeContainer();

		shapeContainer.shapeSelector(project.getDashboard(), objectMapper);

		List<InputShapeDTO> inputList = new ArrayList<InputShapeDTO>();

		for (ShapeDTO shape : shapeContainer.getInputs().values()) {

			InputShapeDTO input = (InputShapeDTO) shape;

			inputList.add(input);
		}
		
		return inputList;
	}	
		
	@Override
	public List<ServiceParameterDTO> getProjectInputParams(Integer projectId) throws SelectShapeException{
		List<InputShapeDTO> inputShapes = this.readInputShapesList(projectId);						

		List<ServiceParameterDTO> serviceParamsList = new ArrayList<ServiceParameterDTO>();
		for(InputShapeDTO input : inputShapes){
			
			UD_InputShapeDTO userData = input.getUserData();

			ServiceParameterDTO param = new ServiceParameterDTO();
			param.setRequired(true);				
			param.setName(input.getId());
			param.setDefaultValue(userData.getDefaultValue());
			param.setLabel(userData.getLabel());
			
			if(VmeConstants.SHAPE_INPUT_COMBO_TYPE.equals(userData.getInputType())){
				Set<String> options = new HashSet<String>();
				for(InputField infield : userData.getCombooptions()){
					options.add(infield.getText());
				}					
				
				param.setOptions(options);
			}			
			serviceParamsList.add(param);
		}
		
		return serviceParamsList;
	}		
	
	@Override
	public List<GuiProject> findMockupsProjects(Integer id) {
		// TODO Auto-generated method stub
		String hql = "from GuiProjectProject pp where pp.pk.project.id=:pId";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pId", id);
		List<GuiProjectProject> tmp = guiProjectProjectDAO.getAllBy(hql, parameters);
		
		List<GuiProject> mockupProjects = new ArrayList<GuiProject>();
		
		for(int i = 0; i < tmp.size(); i++)
			mockupProjects.add(tmp.get(i).getGuiProject());

		return mockupProjects;
	}
	
	@Override
	public void getAllDependencies(Project project, List<Dependency> dependencies)
	{
		Dependency dependency = null;
		
		List<ProjectOperation> services = projectOperationService.getAllByProject(project.getId());
		List<ProjectDataset> datasets = projectDatasetService.getAllByProject(project.getId());
		
		ServiceCatalog sc = null;
		ServiceCatalogExt ext = null;
		RestOperation restOp = null;
		
		boolean isIn = false;
		
		for(int i = 0; i < services.size(); i++)
		{			
			if(services.get(i).getRestOperation() != null)
			{
				restOp = services.get(i).getRestOperation();
				restOp = restOperationService.findRestOperationById(restOp.getId());
				sc = restOp.getServiceCatalog();
			}
			else
			{
				sc = soapOperationService.findSoapOperation(services.get(i).getSoapOperation().getId()).getServiceCatalog();
			}
			
			isIn = false;				
			
			ext = catalogExtService.getById(sc.getId());
			
			if(ext != null && ext.isFrom_marketplace())
			{
				for(int j = 0; j < dependencies.size(); j++)
				{
					if(dependencies.get(j).getId() == ext.getWsmodel_id())
					{
						isIn = true;
						break;
					}
				}
				
				// avoid duplicates
				if(!isIn)
				{
					dependency = new Dependency();
					dependency.setId((long) ext.getWsmodel_id());
					dependency.setTitle(sc.getName());
					
					dependencies.add(dependency);
				}
			}				
		}
		
		for(int i = 0; i < datasets.size(); i++)
		{
			isIn = false;
			
			for(int j = 0; j < dependencies.size(); j++)
			{
				if(dependencies.get(j).getId() == datasets.get(i).getResource().getOdsDataset().getMkpId())
				{
					isIn = true;
					break;
				}
			}
			
			// avoid duplicates
			if(!isIn)
			{
				dependency = new Dependency();
				dependency.setId((long) datasets.get(i).getResource().getOdsDataset().getMkpId());
				dependency.setTitle(datasets.get(i).getResource().getOdsDataset().getTitle());
				
				dependencies.add(dependency);
			}
		}
		
		// add dependencies from inner processes
        
        Iterator<ProjectInnerProcess> inners = projectInnerProcessDAO.getAllByProject(Integer.valueOf(project.getId())).iterator();
        
        ProjectInnerProcess inner = null;
        
        while(inners.hasNext())
        {
        	inner = inners.next();
        	
        	getAllDependencies(inner.getInnerProcess(), dependencies);
        }
        
        //
	}
	
}