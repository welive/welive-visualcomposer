package com.mediigea.vme2.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.mediigea.vme2.dto.GuiFileResourceDTO;
import com.mediigea.vme2.dto.ShapeContainer;
import com.mediigea.vme2.exceptions.InvokeWorkflowException;
import com.mediigea.vme2.exceptions.UserNotAuthorizedException;
import com.mediigea.vme2.exceptions.WorkflowScriptException;
import com.mediigea.vme2.exceptions.WriteResourceException;

/**
 * Interface to define operations for workflow engine (emml)
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface WorkflowService {
	public void buildWorkflowSchema(Integer projectId, ShapeContainer shapeContainer) throws WorkflowScriptException;
	
	public void buildServiceChainWorkflowSchema(Integer projectId, ShapeContainer shapeContainer, String currentServiceId) throws WorkflowScriptException;
	
	public String invokeWorkflow(int projectId, Map<String, String> inputParameters, String userConnectorId) throws InvokeWorkflowException, UserNotAuthorizedException;
	
	public InputStream getWorkflowArchive(Integer projectId) throws IOException;
	
	public void writeXsl(MultipartFile content) throws WriteResourceException;

	public Collection<File> getXslFileList();

	public String invokeServiceChain(int projectId,
			Map<String, String> inputParameters, String userConnectorId)
			throws InvokeWorkflowException, UserNotAuthorizedException;
	
	public JsonNode runMashup(int mashupId, @RequestParam Map<String, String> params, String access_token) throws Exception;
}
