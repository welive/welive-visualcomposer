package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.dto.WorkgroupDTO;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.util.PagedSearch;

@Transactional
public interface WorkgroupService {
	public Workgroup findWorkgroup(Integer id);
	public Workgroup addWorkgroup(Workgroup a);
	public Workgroup updateWorkgroup(Workgroup a);
	public Workgroup deleteWorkgroup(Integer id);
	
	
	public List<Workgroup> findByProfile(Profile p);
	public List<Workgroup> findAll();
	public List<Workgroup> findAllWithProfiles();
	public Workgroup findByName(String name);
	
	public void membersAdd(Integer workgroupId, List<Integer> profileIds);
	public void memberAdd(Integer workgroupId, Integer profileId);
	public void memberRemove(Integer workgroupId, Integer memberId);
	public PagedSearch<WorkgroupDTO> pagedSearch(String searchterm, int page, int pagesize);
}
