package com.mediigea.vme2.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dao.RestOperationDAO;
import com.mediigea.vme2.dao.RestOperationTemplateDAO;
import com.mediigea.vme2.dao.ServiceCatalogDAO;
import com.mediigea.vme2.dto.OperationTemplateDTO;
import com.mediigea.vme2.dto.OperationTemplateParameter;
import com.mediigea.vme2.dto.RestOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.RestOperationTemplate;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.rest.RestAttachment;
import com.mediigea.vme2.rest.RestMethod;
import com.mediigea.vme2.rest.RestParameter;
import com.mediigea.vme2.rest.RestPayload;
import com.mediigea.vme2.rest.RestRepresentation;

public class RestOperationServiceImpl implements RestOperationService {
	
	@Autowired
	private ServiceCatalogDAO serviceCatalogDAO;
	
	@Autowired
	private RestOperationDAO restDAO;
	
	@Autowired
	private RestOperationTemplateDAO restOperationTemplateDAO;
	
	@Autowired
	private ObjectMapperExt objectMapper;
	
	private TypeReference<RestMethod> typeRefRestMethod = new TypeReference<RestMethod>() {
	};
	
	private TypeReference<List<OperationTemplateParameter>> typeRefOperationTemplateParams = new TypeReference<List<OperationTemplateParameter>>() {
	};	
	
	private static final Logger logger = LoggerFactory.getLogger(RestOperationServiceImpl.class);
	
	@Override
	public RestOperation addRestResource(RestOperation resource) {

		if(resource==null)
			return null;
		
		
		
		restDAO.create(resource);
		
		return resource;
	}

	@Override
	public RestOperation updateRestResource(RestOperation resource) {

		if(resource==null)
			return null;
		
		
		restDAO.update(resource);
		
		return resource;
	}

	@Override
	public RestOperation deleteRestResource(Integer restResourceId) {
		
		if(restResourceId==null)
			return null;
		
		RestOperation restRes = restDAO.getById(restResourceId);
		
		if(restRes==null)
			return null;
		
		restDAO.delete(restRes);
		
		return restRes;
	}

	@Override
	public List<RestOperation> findAll(String hql, HashMap<String, Object> parameters) {
		
		if(hql == null || hql.isEmpty())
			return null;
		
		return restDAO.getAllBy(hql, parameters);
	}
	
	public List<RestOperation> findAllByService(Integer id){
		
		if(id==null)
			return null;
		
		String hql = "from RestOperation as rr where rr.serviceCatalog.id=:serviceId order by rr.tag, rr.path";
		HashMap<String, Object> params = new HashMap<String, Object>();		
		params.put("serviceId", id);
		
		return findAll(hql, params);
	}

	@Override
	public RestOperation findRestOperationById(Integer id){
		
		if(id==null)
			return null;
		
		return restDAO.getById(id);
	}
	
	@Override
	public RestOperationDTO findRestOperationByIdAsDTO(Integer id){
		
		logger.info("Find RestOperation ById As DTO");
		
		RestOperation restOperation = restDAO.getById(id);
		
		if(restOperation==null)
			return null;
		
		logger.debug("RestOperation found");
		
		RestOperationDTO restOperationDTO = new RestOperationDTO();
		
		RestMethod method;
		
		try {
			
			method = objectMapper.readValue(restOperation.getExecMethod(), typeRefRestMethod);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		logger.debug("Get RestOperationDTO");
		
		restOperationDTO.setAuthentication(restOperation.getAuthentication());
		restOperationDTO.setBaseURL(restOperation.getBaseURL());
		restOperationDTO.setExecMethod(method);
		restOperationDTO.setId(restOperation.getId());
		restOperationDTO.setName(restOperation.getName());
		restOperationDTO.setPath(restOperation.getPath());
		
		return restOperationDTO;
	}
	
	@Override
	public RestOperation findRestResource(Integer id) {
		
		if(id==null)
			return null;
		
		RestOperation restResource = restDAO.getById(id);
		
		
		String hql = "select sc from RestOperation as rr " +
				"join rr.serviceCatalog as sc " +
				"where rr.id=:restResourceId";
		
		HashMap<String, Object> parameters = new HashMap<String, Object>();		
		parameters.put("restResourceId", id);
		
		List<ServiceCatalog> serviceList = serviceCatalogDAO.getAllBy(hql, parameters);
		
		
		if(serviceList!=null && !serviceList.isEmpty() && serviceList.size()==1)
			restResource.setServiceCatalog(serviceList.get(0));
		
		return restResource;
	}
	
	@Override
	public void addRestOperationTemplate(OperationTemplateDTO operationTemplate) throws JsonProcessingException{
			
		RestOperationDTO operationDTO = this.findRestOperationByIdAsDTO(operationTemplate.getOperationId());
		List<RestParameter> operationParameters = operationDTO.getExecMethod().getParameters();
		
		List<OperationTemplateParameter> templateParamsToStore = new ArrayList<OperationTemplateParameter>();
		
		for(OperationTemplateParameter templateParameter : operationTemplate.getParameters()){
			if(templateParameter.isHide()){
				templateParamsToStore.add(templateParameter);
			} else{
				for(RestParameter parameter : operationParameters){
					if(parameter.getName().equalsIgnoreCase(templateParameter.getName())){
						if(
								(templateParameter.getDefaultValue()!=null || parameter.getDefaultValue()!=null) && 
								!templateParameter.getDefaultValue().equalsIgnoreCase(parameter.getDefaultValue())
						){
							
							templateParamsToStore.add(templateParameter);
						}
						break;
					}
				}
			}
		}
		
		RestOperationTemplate rot = new RestOperationTemplate();
		rot.setName(operationTemplate.getName());
		rot.setDescription(operationTemplate.getDescription());
		rot.setOperation(restDAO.getById(operationTemplate.getOperationId()));
		rot.setDefinition(objectMapper.writeValueAsString(templateParamsToStore));
		
		restOperationTemplateDAO.create(rot);				
	}
	
	@Override
	public void deleteRestOperationTemplate(Integer templateId) {				
		
		RestOperationTemplate rot = restOperationTemplateDAO.getById(templateId);		
		
		restOperationTemplateDAO.delete(rot);		
	}
	
	@Override
	public OperationTemplateDTO findTemplateByIdDTO(Integer templateId) throws Exception{
		RestOperationTemplate rot = this.findTemplateById(templateId);						
		
		List<OperationTemplateParameter> templateParams = objectMapper.readValue(rot.getDefinition(), typeRefOperationTemplateParams);
		
		OperationTemplateDTO otDTO = new OperationTemplateDTO();
		otDTO.setTemplateId(templateId);		
		otDTO.setName(rot.getName());
		otDTO.setDescription(rot.getDescription());
		otDTO.setOperationId(rot.getOperation().getId());
		otDTO.setParameters(templateParams);
		
		return otDTO;
	}
	
	
//	@Override
//	public void updateRestOperationTemplate(
//			RestOperationTemplate operationTemplate)
//			throws JsonProcessingException {
//		
//		restOperationTemplateDAO.update(operationTemplate);		
//	}	
	
	
	public RestOperation findOperationByTemplate(Integer templateId){
		
		return restOperationTemplateDAO.getById(templateId).getOperation();
	}	
	
	public RestOperationTemplate findTemplateById(Integer templateId){
		
		return restOperationTemplateDAO.getById(templateId);
	}
	
	public List<RestOperationTemplate> findTemplateByOperation(Integer operationId){
		
		return restDAO.getById(operationId).getTemplate();
	}		
	
	public List<ServiceParameterDTO> getRestOperationParamsDTO (int operationId){
		
		RestOperation restOp = this.findRestOperationById(operationId);
		
		List<ServiceParameterDTO> parameters = new ArrayList<ServiceParameterDTO>();

		try {
			RestMethod method = objectMapper.readValue(restOp.getExecMethod(), typeRefRestMethod);
			for (RestParameter rp : method.getParameters()) {
				if (rp.isFixed() == false) {
					ServiceParameterDTO param = fillfromRestParameter(rp);
					parameters.add(param);
				}
			}
			if (method.getRepresentations() != null && method.getRepresentations().size() > 0) {
				for (RestRepresentation repr : method.getRepresentations()) {
					if (repr.getParameters() != null) {
						for (RestParameter rp : repr.getParameters()) {
							if (rp.isFixed() == false) {
								ServiceParameterDTO param = fillfromRestParameter(rp);
								parameters.add(param);
							}
						}
					}
					RestAttachment attach = repr.getAttachment();
					if (attach != null) {
						ServiceParameterDTO param = new ServiceParameterDTO();
						param.setAttachment(true);
						param.setName(attach.getName());
						if (attach.getDoc() != null) {
							param.setDoc(attach.getDoc().getText());
						}
						param.setRequired(attach.isRequired());
						parameters.add(param);
					}
					RestPayload payload = repr.getPayload();
					if (payload != null) {
						ServiceParameterDTO param = new ServiceParameterDTO();
						param.setPayload(true);
						if (payload.getDoc() != null) {
							param.setDoc(payload.getDoc().getText());
						}
						param.setRequired(payload.isRequired());
					}
				}
			}
		} catch (JsonParseException e) {
			logger.error("Error JsonParseException:" + e.getStackTrace().toString());
		} catch (JsonMappingException e) {
			logger.error("Error JsonMappingException:" + e.getStackTrace().toString());
		} catch (IOException e) {
			logger.error("Error IOException:" + e.getStackTrace().toString());
		}
		
		return parameters;
	}	
	
	public List<ServiceParameterDTO> getRestTemplateParamsDTO (int templateId) throws Exception{		
		
		RestOperationTemplate rot = findTemplateById(templateId);		
		
		List<ServiceParameterDTO> operation = this.getRestOperationParamsDTO(rot.getOperation().getId());
		
		List<OperationTemplateParameter> template = objectMapper.readValue(rot.getDefinition(), typeRefOperationTemplateParams);
				
		for(ServiceParameterDTO opParam : new ArrayList<ServiceParameterDTO>(operation)){
			for(OperationTemplateParameter tmplParam : template){
				if(opParam.getName().equalsIgnoreCase(tmplParam.getName())){
					if(tmplParam.isHide()){
						operation.remove(opParam);
					} else {
						opParam.setDefaultValue(tmplParam.getDefaultValue());
					}					
					break;
				}
			}
		}
		
		return operation;
	}	
	
	private ServiceParameterDTO fillfromRestParameter(RestParameter rp) {
		ServiceParameterDTO param = new ServiceParameterDTO();
		param.setName(rp.getName());
		param.setLabel(rp.getLabel());
		param.setDefaultValue(rp.getDefaultValue());
		if (rp.getDoc() != null) {
			param.setDoc(rp.getDoc().getText());
		}
		param.setRequired(rp.isRequired());
		if (rp.getOptions() != null) {
			param.setOptions(rp.getOptions().keySet());
		}
		return param;
	}
}
