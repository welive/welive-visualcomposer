package com.mediigea.vme2.service;

import java.util.Date;

import org.scribe.model.Token;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.AccessTokenDAO;
import com.mediigea.vme2.dao.ServiceCatalogDAO;
import com.mediigea.vme2.entity.AccessToken;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;

public class AccessTokenServiceImpl implements AccessTokenService {

	@Autowired
	private AccessTokenDAO accessTokenDao;	
	
	@Autowired
	private ServiceCatalogDAO serviceCatalogDao;	
	
	@Autowired
	private RestOperationService restOperationService;
	
	@Override
	public void addToken(String userConnector, Token token, Integer idProvider, Integer operationId) {
		
		AccessToken entity = new AccessToken();
		entity.setUserConnectorId(userConnector);
		entity.setAccessToken(token.getToken());
		entity.setTokenSecret(token.getSecret());
		entity.setCreatedAt(new Date());

		ServiceCatalog provider = serviceCatalogDao.getById(idProvider); 
		
		entity.setProvider(provider);
		
		// IVAN
		RestOperation operation = null;
		if(operationId != null)
			operation = restOperationService.findRestOperationById(operationId);
		entity.setOperation(operation);
		//
		
		accessTokenDao.create(entity);		
	}
	@Override
	public AccessToken getToken(String userConnectorId, Integer idProvider, Integer operationId){
		return accessTokenDao.getByKey(userConnectorId, idProvider, operationId);
	}
	
}
