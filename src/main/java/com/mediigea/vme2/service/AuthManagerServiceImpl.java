package com.mediigea.vme2.service;

import it.eng.PropertyGetter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.Api;
import org.scribe.oauth.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediigea.vme2.controller.AdminController;
import com.mediigea.vme2.dao.AuthProviderTypeDAO;
import com.mediigea.vme2.dao.ServiceCatalogDAO;
import com.mediigea.vme2.dto.OAuthServiceInfo;
import com.mediigea.vme2.entity.AuthProviderType;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.util.oauth.OAuth10aGenericApi;
import com.mediigea.vme2.util.oauth.OAuth20GenericApi;

@Service("oauthManagerService")
public class AuthManagerServiceImpl implements AuthManagerService {

	@Autowired
	private ServiceCatalogDAO serviceCatalogDAO;

	@Autowired
	private AuthProviderTypeDAO authProviderTypeDAO;
	
	private String callbackContext;
	
	private static final Logger logger = LoggerFactory.getLogger(AuthManagerServiceImpl.class);

	/**
	 * @return the callbackContext
	 */
	public String getCallbackContext() {
		return callbackContext;
	}

	/**
	 * @param callbackContext
	 *            the callbackContext to set
	 */
	public void setCallbackContext(String callbackContext) {
		this.callbackContext = callbackContext;
	}

	private HashMap<Integer, OAuthServiceInfo> providers;

	public HashMap<Integer, OAuthServiceInfo> getProviders() {
		return providers;
	}
	
	private static List<AuthProviderType> authProviderTypes;

	@Override
	public void init() {
		this.providers = new HashMap<Integer, OAuthServiceInfo>();

		List<ServiceCatalog> persistenceProviders = serviceCatalogDAO.getAll();

		Api api = null;

		for (ServiceCatalog provider : persistenceProviders) 
		{			
			try
			{			
				if (provider.getAuthType() != null &&
						!provider.getAuthType().getType().equals(VmeConstants.OAUTHNONE) &&
						!provider.getAuthType().getType().equals(VmeConstants.BASICAUTH) ) {
					
					if (VmeConstants.OAUTH10VERSION.equals(provider.getAuthType().getType())) {
	
						api = new OAuth10aGenericApi(provider.getAuthorizeUrl(), provider.getRequestTokenUrl(), provider.getAccessTokenUrl());
	
					} else if (VmeConstants.OAUTH20VERSION.equals(provider.getAuthType().getType()) || VmeConstants.WELIVEAUTH.equals(provider.getAuthType().getType())) {
						api = new OAuth20GenericApi(provider.getAuthorizeUrl(), null, provider.getAccessTokenUrl());
					}
	
					String tmpCallBack = callbackContext + "/oauth/" + provider.getId() + "/tokencallback";
					OAuthService service = new ServiceBuilder().provider(api).apiKey(provider.getApiKey()).apiSecret(provider.getApiSecret()).callback(tmpCallBack)
							.build();

					OAuthServiceInfo serviceInfo = new OAuthServiceInfo();
					serviceInfo.setService(service);
					serviceInfo.setApiKey(provider.getApiKey());
					serviceInfo.setApiSecret(provider.getApiSecret());
	
					this.providers.put(provider.getId(), serviceInfo);
				}
			}
			catch(Exception e)
			{
				/*System.out.println("### ACCESS TOKEN URL: " + provider.getAccessTokenUrl());
				System.out.println("### AUTHORIZE TOKEN URL: " + provider.getAuthorizeUrl());
				System.out.println("### AAPI KEY: " + provider.getApiKey());
				System.out.println("### API SECRET: " + provider.getApiSecret());
				System.out.println("### SCOPE: " + provider.getScope());*/
				e.printStackTrace();
				continue;
			}
		}
		
		// IVAN
		
		boolean weliveAuthEnabled = Boolean.parseBoolean(PropertyGetter.getProperty("aac.authorization.enabled"));
		
		AuthProviderType weliveAuth = null;
		
		// load auth provider types
		try
		{
			authProviderTypes = authProviderTypeDAO.getAll();
			
			// check if provider types include welive auth...
			boolean flag = true;
			for(int i = 0; i < authProviderTypes.size(); i++)
			{
				//System.out.println("### AUTH TYPE: " + authProviderTypes.get(i).getType());
				if(authProviderTypes.get(i).getType().equals(VmeConstants.WELIVEAUTH))
				{
					flag = false;
				}				
			}
			
			// ...if not, add it to db
			if(flag && weliveAuthEnabled)
			{
				//System.out.println("### Adding WeLive Auth...");
				weliveAuth = new AuthProviderType();
				weliveAuth.setType("WELIVEAUTH");
				weliveAuth.setDescription("WeLive Authorization");
				
				authProviderTypeDAO.create(weliveAuth);
				
				authProviderTypes.add(weliveAuth);
				
				//System.out.println("### ...done");
			}
			
			// if welive auth is not enabled, remove it from the list
			if(!weliveAuthEnabled)
			{
				for(int i = 0; i < authProviderTypes.size(); i++)
				{
					if(authProviderTypes.get(i).getType().equals(VmeConstants.WELIVEAUTH))
					{
						authProviderTypes.remove(i);
						break;
					}
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public OAuthServiceInfo getService(Integer id) {
		return this.providers.get(id);
	}

	

	@Override
	public void reloadProviders() {
		init();		
	}

	@Override
	public List<AuthProviderType> getAdminAuthProviders() {
		
		// IVAN
		//return authProviderTypeDAO.getAll();
		return authProviderTypes;
	}

	@Override
	public List<AuthProviderType> getUserAuthProviders() {
		// IVAN
		//List<AuthProviderType> list = authProviderTypeDAO.getAll();
		List<AuthProviderType> list = authProviderTypes;
		
		List<AuthProviderType> result = new ArrayList<AuthProviderType>();
		for (AuthProviderType ap: list){
			if (!(ap.getType().equals(VmeConstants.OAUTH10VERSION) || ap.getType().equals(VmeConstants.OAUTH20VERSION))){
				result.add(ap);
			}
		}
		
		return result;
	}

}