package com.mediigea.vme2.service;

import groovy.xml.MarkupBuilder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dao.SoapOperationDAO;
import com.mediigea.vme2.dao.SoapOperationTemplateDAO;
import com.mediigea.vme2.dto.OperationTemplateDTO;
import com.mediigea.vme2.dto.OperationTemplateParameter;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.dto.SoapOperationDTO;
import com.mediigea.vme2.entity.RestOperationTemplate;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.entity.SoapOperationTemplate;
import com.mediigea.vme2.soap.SoapMethod;
import com.mediigea.vme2.soap.SoapParameter;
import com.predic8.wsdl.Definitions;
import com.predic8.wsdl.WSDLParser;
import com.predic8.wstool.creator.RequestCreator;
import com.predic8.wstool.creator.SOARequestCreator;

public class SoapOperationServiceImpl implements SoapOperationService {
	private static final Logger logger = LoggerFactory.getLogger(SoapOperationServiceImpl.class);


	private final static String JSON_OBJECT_CLIENT_PARAM_NAME = "name";
	private final static String JSON_OBJECT_CLIENT_PARAM_VALUE = "value";

	@Autowired
	private SoapOperationDAO soapOperationDAO;

	@Autowired
	private SoapOperationTemplateDAO soapOperationTemplateDAO;
	
	@Autowired
	private ObjectMapperExt objectMapper;

	private TypeReference<HashMap<String, List<SoapParameter>>> typeRef = new TypeReference<HashMap<String, List<SoapParameter>>>() {
	};

	private TypeReference<List<OperationTemplateParameter>> typeRefOperationTemplateParams = new TypeReference<List<OperationTemplateParameter>>() {
	};	

	
	@Override
	public List<SoapOperation> findAll(String hql, HashMap<String, Object> parameters) {

		if(hql == null || hql.isEmpty())
			return null;

		return soapOperationDAO.getAllBy(hql, parameters);
	}

	public List<SoapOperation> findAllByService(Integer serviceCatalogId){

		if(serviceCatalogId==null)
			return null;

		String hql = "from SoapOperation as so where so.serviceCatalog.id=:serviceId";
		HashMap<String, Object> params = new HashMap<String, Object>();

		params.put("serviceId", serviceCatalogId);

		return findAll(hql, params);
	}

	@Override
	public SoapOperation findSoapOperation(Integer id) {

		if(id==null)
			return null;

		return soapOperationDAO.getById(id);
	}

	@Override
	public SoapOperationDTO findSoapOperationAsDTO(Integer id) {

		if(id==null)
			return null;

		SoapOperation soapOp = soapOperationDAO.getById(id);

		if(soapOp==null)
			return null;

		SoapOperationDTO soapOperationDTO = new SoapOperationDTO();

		soapOperationDTO.setBindingName(soapOp.getBindingName());
		soapOperationDTO.setEndPoint(soapOp.getEndPoint());
		soapOperationDTO.setId(soapOp.getId());
		soapOperationDTO.setInputParams(soapOp.getInputParams());
		soapOperationDTO.setOperationName(soapOp.getOperationName());
		soapOperationDTO.setOutputParams(soapOp.getOutputParams());
		soapOperationDTO.setPortTypeName(soapOp.getPortTypeName());
		soapOperationDTO.setSoapAction(soapOp.getSoapAction());
		soapOperationDTO.setWsdlStream(soapOp.getServiceCatalog().getContent());
		soapOperationDTO.setWsdlUrl(soapOp.getWsdlUrl());

		try {
			HashMap<String, List<SoapParameter>> inMap = objectMapper.readValue(soapOp.getInputParams(), typeRef);
			SoapMethod method = new SoapMethod();
			method.setInputParams(inMap);
			soapOperationDTO.setMethod(method);
			
		} catch (JsonParseException e) {
			logger.error("JsonParseException: " + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			logger.error("JsonMappingException: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("IOException: " + e.getMessage());
			e.printStackTrace();
		}
		
		return soapOperationDTO;
	}
	
	public SOAPMessage createSoapRequestPrototype(SoapOperationDTO soapOp, Map<String, String> parameters){
		List<SoapParameter> parametersFromDbForSoap = getParametersFromDB(soapOp);

		SoapParameter paramFromDb = null;

		HashMap<String, String> formParams = new HashMap<String, String>();
		
		for (int i = 0; i < parametersFromDbForSoap.size(); i++) {

			paramFromDb = parametersFromDbForSoap.get(i);

			if(parameters.containsKey(paramFromDb.getName())){
				String param = "xpath:" + paramFromDb.getXpath() + "/" + paramFromDb.getName();
				//String placeholder = paramFromDb.getXpath() + "/" + paramFromDb.getName();
				// setto il valore dei parametri, ad esempio: ZIP = 48888
				formParams.put(param, parameters.get(paramFromDb.getName()));
			}			
		}

		return createRequest(soapOp, formParams);	
	}

	public SOAPMessage createSoapRequest(SoapOperationDTO soapOp, JSONArray parametersFromClient){

		List<SoapParameter> parametersFromDbForSoap = getParametersFromDB(soapOp);

		JSONObject paramFromClient = null;
		SoapParameter paramFromDb = null;

		HashMap<String, String> formParams = new HashMap<String, String>();

		for (int i = 0; i < parametersFromDbForSoap.size(); i++) {

			try {
				paramFromDb = parametersFromDbForSoap.get(i);
				paramFromClient = existObjectIn(paramFromDb, parametersFromClient);

				if (paramFromClient != null) {
					logger.debug("JSONObject from client found");

					/*
					 * Teniamo conto della seguente struttura "{ "input" : [ {
					 * "name" : "IPAddress", "label" : "IPAddress", "value" :
					 * "", "required" : false, "type" : "string", "xpath" :
					 * "/GetGeoIP", "ref" : "", "minOccurs" : "", "maxOccurs" :
					 * "" } ] }"
					 */

					String param = "xpath:" + paramFromDb.getXpath() + "/" + paramFromDb.getName();

					// setto il valore dei parametri, ad esempio: ZIP = 48888
					formParams.put(param, paramFromClient.getString(JSON_OBJECT_CLIENT_PARAM_VALUE));
				}
			} catch (JSONException e) {
				logger.error("JSONException: " + e.getMessage());
			}
		}

		return createRequest(soapOp, formParams);		
	}

	private List<SoapParameter> getParametersFromDB(SoapOperationDTO soapOp){
		List<SoapParameter> parametersFromDbForSoap = new ArrayList<SoapParameter>();

		try {
			HashMap<String, List<SoapParameter>> inMap = objectMapper.readValue(soapOp.getInputParams(), typeRef);
			for (String key : inMap.keySet()) {
				List<SoapParameter> inParams = inMap.get(key);
				for (SoapParameter sp : inParams) {
					if (!sp.getType().equals("complex")) {
						parametersFromDbForSoap.add(sp);
					}
				}
			}			
		} catch (JsonParseException e) {
			logger.error("JsonParseException: " + e.getMessage());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			logger.error("JsonMappingException: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("IOException: " + e.getMessage());
			e.printStackTrace();
		}

		return parametersFromDbForSoap;
	}


	private SOAPMessage createRequest(SoapOperationDTO soapOp, HashMap<String, String> formParams){
		SOAPMessage soapRequest = null;

		/* dati presi dalla tabella soapoperation */

		String wsdlStream = soapOp.getWsdlStream();		
		String portTypeName = soapOp.getPortTypeName();
		String bindingName = soapOp.getBindingName();
		String operationName = soapOp.getOperationName();
		String soapAction = soapOp.getSoapAction();

		try {
			InputStream in = new ByteArrayInputStream(wsdlStream.getBytes());

			WSDLParser parser = new WSDLParser();
			Definitions defs = parser.parse(in);

			StringWriter writer = new StringWriter();
			SOARequestCreator creator = new SOARequestCreator(defs, new RequestCreator(), new MarkupBuilder(writer));

			// setto i parametri nel creator
			creator.setFormParams(formParams);

			// creo il Soap xml dentro writer
			creator.createRequest(portTypeName, operationName, bindingName);

			MessageFactory messageFactory = MessageFactory.newInstance();
			MimeHeaders mimeHeaders = new MimeHeaders();

			// creo soap message dal writer
			soapRequest = messageFactory.createMessage(mimeHeaders, new ByteArrayInputStream(writer.toString().getBytes()));

			// imposto soap action
			soapRequest.getMimeHeaders().addHeader("SOAPAction", soapAction);

			in.close();
		} catch (Exception e) {

			logger.error("Exception: " + e.getMessage());
		}

		return soapRequest;
	}

	/**
	 * 
	 * @param param
	 * @return
	 */
	private JSONObject existObjectIn(SoapParameter soapParam, JSONArray parametersFromClient) {

		JSONObject paramFromClient = null;
		if (soapParam != null) {
			for (int i = 0; i < parametersFromClient.length(); i++) {
				try {
					JSONObject param = parametersFromClient.getJSONObject(i);
					if (param.getString(JSON_OBJECT_CLIENT_PARAM_NAME).equals(soapParam.getName())) {
						paramFromClient = param;
						break;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return paramFromClient;
	}	
	
	public List<ServiceParameterDTO> getSoapOperationParamsDTO(int operationId){
		
		SoapOperation sOp = this.soapOperationDAO.getById(operationId);
		
		List<ServiceParameterDTO> parameters = new ArrayList<ServiceParameterDTO>();		

		try {
			HashMap<String, List<SoapParameter>> inMap = objectMapper.readValue(sOp.getInputParams(), typeRef);
			for (String key : inMap.keySet()) {
				List<SoapParameter> inParams = inMap.get(key);
				for (SoapParameter sp : inParams) {
					ServiceParameterDTO param = fillfromSoapParameter(sp);
					parameters.add(param);
				}
			}
		} catch (JsonParseException e) {
			logger.error("Error JsonParseException:" + e.getStackTrace().toString());
		} catch (JsonMappingException e) {
			logger.error("Error JsonMappingException:" + e.getStackTrace().toString());
		} catch (IOException e) {
			logger.error("Error IOException:" + e.getStackTrace().toString());
		}
		
		return parameters;
	}	
	
	private ServiceParameterDTO fillfromSoapParameter(SoapParameter sp) {
		ServiceParameterDTO param = new ServiceParameterDTO();
		param.setName(sp.getName());
		param.setLabel(sp.getLabel());
		param.setDefaultValue("");
		param.setRequired(sp.isRequired());
		param.setMinOccurs(new Integer(sp.getMinOccurs()));
		param.setMaxOccurs(new Integer(sp.getMaxOccurs()));
		if (sp.getEnumerations() != null) {
			param.setOptions(sp.getEnumerations().keySet());
		}
		return param;
	}	
	
	@Override
	public void addSoapOperationTemplate(OperationTemplateDTO operationTemplate) throws JsonProcessingException{
			
		SoapOperationDTO operationDTO = this.findSoapOperationAsDTO(operationTemplate.getOperationId());
		
		HashMap<String, List<SoapParameter>> operationParameters = operationDTO.getMethod().getInputParams();
		
		List<OperationTemplateParameter> templateParamsToStore = new ArrayList<OperationTemplateParameter>();
		
		for(OperationTemplateParameter templateParameter : operationTemplate.getParameters()){
			if(templateParameter.isHide()){
				templateParamsToStore.add(templateParameter);
			} else{
				for(List<SoapParameter> parameterList : operationParameters.values()){
					for(SoapParameter parameter : parameterList){
						if(parameter.getName().equalsIgnoreCase(templateParameter.getName())){
							if(
									(templateParameter.getDefaultValue()!=null || parameter.getValue()!=null) && 
									!templateParameter.getDefaultValue().equalsIgnoreCase(parameter.getValue())
							){
								
								templateParamsToStore.add(templateParameter);
							}
							break;
						}
					}
				}
			}
		}
		
		SoapOperationTemplate rot = new SoapOperationTemplate();
		rot.setName(operationTemplate.getName());
		rot.setDescription(operationTemplate.getDescription());
		rot.setOperation(soapOperationDAO.getById(operationTemplate.getOperationId()));
		rot.setDefinition(objectMapper.writeValueAsString(templateParamsToStore));
		
		soapOperationTemplateDAO.create(rot);				
	}	
	
	@Override
	public SoapOperation findOperationByTemplate(Integer templateId){
		
		return soapOperationTemplateDAO.getById(templateId).getOperation();
	}	
	
	@Override
	public void deleteSoapOperationTemplate(Integer templateId) {				
		
		SoapOperationTemplate sot = soapOperationTemplateDAO.getById(templateId);		
		
		soapOperationTemplateDAO.delete(sot);		
	}	
	
	@Override
	public OperationTemplateDTO findTemplateByIdDTO(Integer templateId) throws Exception{
		SoapOperationTemplate rot = this.findTemplateById(templateId);						
		
		List<OperationTemplateParameter> templateParams = objectMapper.readValue(rot.getDefinition(), typeRefOperationTemplateParams);
		
		OperationTemplateDTO otDTO = new OperationTemplateDTO();
		otDTO.setTemplateId(templateId);
		otDTO.setName(rot.getName());
		otDTO.setDescription(rot.getDescription());
		otDTO.setOperationId(rot.getOperation().getId());
		otDTO.setParameters(templateParams);
		
		return otDTO;
	}
	
	
//	@Override
//	public void updateSoapOperationTemplate(
//			SoapOperationTemplate operationTemplate)
//			throws JsonProcessingException {
//
//		soapOperationTemplateDAO.update(operationTemplate);	
//		
//	}	
	
	public List<SoapOperationTemplate> findTemplateByOperation(Integer operationId){
		
		return soapOperationDAO.getById(operationId).getTemplate();
	}		
	
	@Override
	public SoapOperationTemplate findTemplateById(Integer templateId){
		
		return soapOperationTemplateDAO.getById(templateId);
	}
	
	@Override
	public List<ServiceParameterDTO> getSoapTemplateParamsDTO (int templateId) throws Exception{				
		
		//SoapOperationTemplate rot = findTemplateByOperation(operationId);
		
		SoapOperationTemplate rot = findTemplateById(templateId);
		
		List<ServiceParameterDTO> operation = this.getSoapOperationParamsDTO(rot.getOperation().getId());
		
		List<OperationTemplateParameter> template = objectMapper.readValue(rot.getDefinition(), typeRefOperationTemplateParams);
				
		for(ServiceParameterDTO opParam : new ArrayList<ServiceParameterDTO>(operation)){
			for(OperationTemplateParameter tmplParam : template){
				if(opParam.getName().equalsIgnoreCase(tmplParam.getName())){
					if(tmplParam.isHide()){
						operation.remove(opParam);
					} else {
						opParam.setDefaultValue(tmplParam.getDefaultValue());
					}					
					break;
				}
			}
		}
		
		return operation;
	}		
}
