package com.mediigea.vme2.service;

import org.scribe.model.Token;
import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.AccessToken;

/**
 * Interface to define operations about OAuth tokens management
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface AccessTokenService {

	public void addToken(String userConnector, Token accessToken, Integer idProvider, Integer operationId);
	public AccessToken getToken(String userConnectorId, Integer idProvider, Integer operationId);
}
