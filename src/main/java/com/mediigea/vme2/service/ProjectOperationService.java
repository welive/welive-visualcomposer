package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectOperation;

/**
 * Interface to define operations about rest/soap operations used inside a mashup project
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface ProjectOperationService {
	
	public enum OperationType{SOAP, REST};
	
	public void create(int idProject, int idOperation, OperationType type);
	
	public void deleteAllByProject(int idProject);
	
	public List<ProjectOperation> getAllByProject(int idProject);
	
	public List<Project> findMashupsByOperationId(Integer id, int type);
}
