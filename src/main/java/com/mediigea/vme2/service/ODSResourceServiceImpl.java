package com.mediigea.vme2.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.ODSDatasetDAO;
import com.mediigea.vme2.dao.ODSResourceDAO;
import com.mediigea.vme2.entity.ODSDataset;
import com.mediigea.vme2.entity.ODSResource;
import com.mediigea.vme2.entity.RestOperation;

public class ODSResourceServiceImpl implements ODSResourceService
{
	@Autowired
	private ODSDatasetDAO odsDatasetDAO;
	
	@Autowired
	private ODSResourceDAO odsResourceDAO;

	@Override
	public ODSResource addODSResource(ODSResource resource) 
	{
		if(resource == null)
			return null;
		
		odsResourceDAO.create(resource);
		
		return resource;
	}

	@Override
	public ODSResource updateODSResource(ODSResource resource) 
	{
		if(resource == null)
			return null;		
		
		odsResourceDAO.update(resource);
		
		return resource;
	}

	@Override
	public void deleteODSResource(Integer id) throws Exception
	{
		if(id == null)
			return;
		
		ODSResource resource = odsResourceDAO.getById(id);
		
		if(resource == null)
			odsResourceDAO.delete(resource);
	}

	@Override
	public List<ODSResource> findAll(String hql, HashMap<String, Object> parameters) 
	{
		if (hql == null || hql.isEmpty())
			return odsResourceDAO.getAll();

		return odsResourceDAO.getAllBy(hql, parameters);
	}

	@Override
	public List<ODSResource> findAllByODSDataset(Integer id) 
	{
		if(id == null)
			return null;
		
		String hql = "from ODSResource as r where r.odsDataset.id=:datasetId order by r.title";
		HashMap<String, Object> params = new HashMap<String, Object>();		
		params.put("datasetId", id);
		
		return findAll(hql, params);
	}

	@Override
	public ODSResource findODSResource(Integer id)
	{
		ODSResource resource = odsResourceDAO.getById(id);
		return resource;
	}

}
