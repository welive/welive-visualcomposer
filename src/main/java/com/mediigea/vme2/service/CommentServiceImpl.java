package com.mediigea.vme2.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediigea.vme2.dao.CommentDAO;
import com.mediigea.vme2.dao.UnreadCommentDAO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.UnreadComment;

@Service("commentService")
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDAO commentDAO;
	
	@Autowired 
	private UnreadCommentDAO unreadCommentDAO;
	
	@Autowired 
	private ProjectService projectService;
	
	@Autowired 
	private GuiEditorService guiEditorService;
	
	@Override
	public Comment addComment(Comment comment) {
		
		if(comment==null)
			return null;
		
		comment.setCreatedAt(new Date());
		commentDAO.create(comment);
		
		// invia i messaggi a tutti i membri del progetto
		// tranne a me stesso (creatore del commento)
		addUnreadComment(comment);
		
		
		return comment;
	}

	@Override
	public Comment updateComment(Comment comment) {

		if(comment==null)
			return null;
		
		return commentDAO.update(comment);
	}

	@Override
	public Comment deleteComment(Integer id) {
		
		if(id==null)
			return null;
		
		Comment c = commentDAO.getById(id);
		
		if(c==null)
			return null;
		
		setReadedComment(c);
		commentDAO.delete(c);
		
		
		return c;
	}
	
	@Override
	public void deleteAllByProject(int idProject){
		
		commentDAO.deleteAllByProject(idProject);
	}
	
	@Override
	public void deleteAllByGuiProject(int idProject){
		
		commentDAO.deleteAllByGuiProject(idProject);
	}

	@Override
	public Comment findComment(Integer id) {
		
		if(id==null)
			return null;
		
		return commentDAO.getById(id);
	}

	@Override
	public List<Comment> findAll(String hql, HashMap<String, Object> parameters) {
		
		if(hql == null || hql.isEmpty())
			return commentDAO.getAll();
		
		return commentDAO.getAllBy(hql, parameters);
	}
	
	public List<Comment> findAllByProject(Integer id){
		
		if(id==null)
			return null;
		
		String hql = "select new Comment(comment.id, comment.project, comment.profile, comment.content, comment.createdAt) from Comment as comment where comment.project.id=:projectId order by comment.createdAt desc";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("projectId", id);
		
		return findAll(hql, parameters);
	}

	@Override
	public void addUnreadComment(Comment comment) {
		
		UnreadComment unreadComment;
		
		Project pj = comment.getProject();
		GuiProject gpj = comment.getGuiProject();
		
		List <Profile> destinations = null;
		
		if(pj!=null){
			destinations = projectService.findProjectMembers(pj);
		}
		
		if(gpj!=null){
			destinations = guiEditorService.findProjectMembers(gpj);
		}
		
		for (Profile p : destinations) {
			
			// non faccio le notifiche a me stesso !
			if (comment.getProfile().getId()!=p.getId()) {
				unreadComment = new UnreadComment();
				unreadComment.setComment(comment);
				unreadComment.setProfile(p);
//				unreadComment.setProject(pj);
				unreadCommentDAO.create(unreadComment);
			}
		}
		
	}

	@Override
	public void setReadedComment(Project project, Profile profile) {

		List <UnreadComment> listToDelete = 
				unreadCommentDAO.getUnreadCommentsByProjectIdProfileId(project.getId(), profile.getId());
		deleteUnreadCommentList(listToDelete);
	}
	
	@Override
	public void setReadedComment(GuiProject project, Profile profile) {

		List <UnreadComment> listToDelete = 
				unreadCommentDAO.getUnreadCommentsByGuiProjectIdProfileId(project.getId(), profile.getId());
		deleteUnreadCommentList(listToDelete);
	}	
	
	@Override
	public void setReadedComment(Comment comment) {
		List <UnreadComment> listToDelete = unreadCommentDAO.getUnreadCommentsByCommentId(comment.getId());
		deleteUnreadCommentList(listToDelete);
	}
	
	private void deleteUnreadCommentList(List <UnreadComment> listToDelete) {
		for (UnreadComment toDelete : listToDelete)
			unreadCommentDAO.delete(toDelete);
	}
	
	@Override
	public List<UnreadComment> getUnreadCommentsByCommentId(int idComment){
		return unreadCommentDAO.getUnreadCommentsByCommentId(idComment);		
	}
	@Override
	public List<UnreadComment> getUnreadCommentsByProjectIdProfileId(int idProject, int idProfile){
		return unreadCommentDAO.getUnreadCommentsByProjectIdProfileId(idProject, idProfile);				
	}
	@Override
	public List<UnreadComment> getUnreadCommentsByGuiProjectIdProfileId(int idProject, int idProfile){
		return unreadCommentDAO.getUnreadCommentsByGuiProjectIdProfileId(idProject, idProfile);				
	}
	@Override
	public List<UnreadComment> getUnreadCommentsByProfileId(int idProfile){
		return unreadCommentDAO.getUnreadCommentsByProfileId(idProfile);				
	}
	
	
	
}
