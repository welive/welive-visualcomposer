package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.auth.AuthenticationException;
import com.mediigea.vme2.auth.RegistrationException;
import com.mediigea.vme2.entity.Account;


@Transactional
public interface AccountService {

	public Account addAccount(Account a);
	public Account updateAccount(Account a);
	public Account deleteAccount(Integer id);
	public void logoutUser(Account a);
	public void loginUser(Account a);
	
	public Account verifyAccount(String validatedOauthId) throws AuthenticationException;
	public Account verifyAccount(String userName, String password, int accountRole) throws AuthenticationException;
	
	//public Account registrationTo(org.brickred.socialauth.Profile userProfile, org.brickred.socialauth.AuthProvider provider) throws RegistrationException;	
	public Account registrationTo(Account a, int userType) throws RegistrationException;
	
	public List<Account> findAll();
	
	public Account setStatus(Integer id, boolean enabled);
	public Account getById(Integer accountId);
}
