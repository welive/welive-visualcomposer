package com.mediigea.vme2.service;

import it.eng.metamodel.definitions.Dependency;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mediigea.vme2.dto.InputShapeDTO;
import com.mediigea.vme2.dto.ProjectDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProfileProject;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.exceptions.ImportProjectException;
import com.mediigea.vme2.exceptions.SelectShapeException;

/**
 * Interface to define management operations about projects (Mashup)
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface ProjectService {

	public Project addProject(Project a);

	public Project addProject(Project a, Profile owner);

	public Project updateProject(Project a);

	public List<Project> findAll(String hql, HashMap<String, Object> parameters);

	public List<Project> findAllByProfile(Profile p);
	
	public List<Project> findAllByWorkgroup(Workgroup w);
	
	public List<ProjectDTO> findAllProjectDTOByProfile(Profile p);
	
	public Project findProject(Integer id);
	
	public List<Comment> findProjectComments(Project p, int limit);
	
	public List<Comment> findProjectComments(Project p);	
		
	public String getDashboard(Integer projectId);

	public void setDashboard(Integer projectId, String dashboard);
	
	public int countComments(Project p);

	public List<Profile> findProjectMembers(Project project);
	
	public List<ProfileProject> findProfileProject(Project project);
	
	public Profile findProjectOwner(Integer projectId);

	public void memberAdd(Integer projectId, Integer profileId);
	
	public void memberAddWithRole(Integer projectId, Integer profileId, Integer role);

	public void memberRemove(Integer projectId, Integer profileId);

	public void deleteProject(Integer projectId);

	public OutputStream exportProject(Integer projectId);
	
	public void importProject(MultipartFile mpf, Profile profile, String name, String description, int workgroupId) throws ImportProjectException;

	public List<InputShapeDTO> readInputShapesList(Integer projectId) throws SelectShapeException;

	public List<ProjectDTO> searchProjectDTO(Profile p, String criteria);

	public List<ServiceParameterDTO> getProjectInputParams(Integer projectId) throws SelectShapeException;	
	
	public List<GuiProject> findMockupsProjects(Integer id);
	
	public void getAllDependencies(Project project, List<Dependency> dependencies);
}
