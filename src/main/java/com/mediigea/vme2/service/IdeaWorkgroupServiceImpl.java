package com.mediigea.vme2.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediigea.vme2.dao.IdeaWorkgroupDAO;
import com.mediigea.vme2.entity.IdeaWorkgroup;
import com.mediigea.vme2.entity.Workgroup;

@Service("ideawgService")
public class IdeaWorkgroupServiceImpl implements IdeaWorkgroupService
{
	@Autowired
	private IdeaWorkgroupDAO ideawgDAO;

	@Override
	public IdeaWorkgroup addIdeaWorkgroup(IdeaWorkgroup iw) 
	{
		ideawgDAO.create(iw);
		return iw;
	}

	@Override
	public IdeaWorkgroup updateIdeaWorkgroup(IdeaWorkgroup iw) 
	{
		ideawgDAO.update(iw);
		return iw;
	}

	@Override
	public void deleteIdeaWorkgroup(int ideaId) 
	{
		ideawgDAO.deleteById(ideaId);		
	}
	
	@Override
	public IdeaWorkgroup getIdeaWorkgroupByIdeaId(int ideaId) 
	{
		return ideawgDAO.getById(ideaId);
	}
	
	@Override
	public IdeaWorkgroup getIdeaWorkgroupByWorkgroupId(int wgId) 
	{
		String hql = "from IdeaWorkgroup w where w.wg.id=:wgId";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("wgId", wgId);

		List<IdeaWorkgroup> list = ideawgDAO.getAllBy(hql, parameters);
		
		if(list.isEmpty()) return null;
		
		return list.get(0);
	}

}
