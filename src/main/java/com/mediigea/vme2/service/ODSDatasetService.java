package com.mediigea.vme2.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.ODSDataset;
import com.mediigea.vme2.entity.Profile;

@Transactional
public interface ODSDatasetService 
{
	public ODSDataset addODSDataset(ODSDataset dataset, Profile profile);
	
	public ODSDataset setVisibility(Integer id, Integer code);
	
	public ODSDataset updateODSDataset(ODSDataset dataset);
	public void deleteODSDataset(Integer id) throws Exception;
	
	public List<ODSDataset> findAll(String hql, HashMap<String, Object> parameters);		
	
	public List<ODSDataset> findPublicDatasets();
	public List<ODSDataset> findVisibleDatasets(Integer profileId);
	
	public ODSDataset findDataset(Integer id);
	public List<ODSDataset> findDatasetsByODSId(Integer odsId);
}
