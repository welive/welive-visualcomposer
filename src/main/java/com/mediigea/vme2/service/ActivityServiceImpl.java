package com.mediigea.vme2.service;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dao.AccountDAO;
import com.mediigea.vme2.dao.ActivityDAO;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.entity.Profile;



import org.apache.abdera2.activities.model.Activity;
import org.apache.abdera2.activities.model.Collection;
import org.apache.abdera2.activities.model.IO;
import org.apache.abdera2.activities.model.Verb;
import org.apache.abdera2.activities.model.objects.PersonObject;
 

import static org.apache.abdera2.activities.model.Verb.CHECKIN;
import static org.apache.abdera2.activities.model.Verb.APPROVE;
import static org.apache.abdera2.activities.model.Verb.LEAVE;
import static org.apache.abdera2.activities.model.Activity.makeActivity;
import static org.apache.abdera2.activities.model.objects.PersonObject.makePerson;
import static org.apache.abdera2.activities.model.objects.MovieObject.makeMovie;
import static org.apache.abdera2.activities.model.objects.NoteObject.makeNote;
 

@Service("activityService")
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	private ActivityDAO activityDAO;

	@Autowired
	private ProfileDAO profileDAO;

	@Autowired
	private AccountDAO accountDAO;
	
	
	private List<Activity> streamList (List <com.mediigea.vme2.entity.Activity> listActivity) {
		PersonObject person;
		List<Activity> streamList = new ArrayList<Activity> ();
		Activity streamActivity = null;
		
		for (com.mediigea.vme2.entity.Activity activity : listActivity) {
        	
        	
        	person = makePerson(activity.getProfile().getFirstName()+ " " + 
        	                    activity.getProfile().getLastName()).email(activity.getProfile().getEmail()).get();

        	switch (activity.getActivityType()) {
    			case "REGISTER_USER":
	            	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
	    			break;  
    			case "USER_LOGIN":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;
        		case "ENABLE_USER":
        			streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;
        		case "DISABLE_USER":
        			streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;
        		case "USER_LOGOUT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;
        		case "USER_UPDATE_PASSWORD":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;   
        		case "ADD_MEMBER_TO_WORKGROUP":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "REMOVE_MEMBER_FROM_WORKGROUP":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;       
        		case "CREATE_WORKGROUP":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;   
        		case "UPDATE_WORKGROUP":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "DELETE_WORKGROUP":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;          			
        		case "IMPORT_WADL":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;   
        		case "IMPORT_WSDL":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "CREATE_CATEGORY":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;   
        		case "UPDATE_CATEGORY":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "DELETE_CATEGORY":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;          
        		case "CREATE_MASHUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "UPDATE_MASHUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;   
        		case "DELETE_MASHUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "EXPORT_MASHUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;   
        		case "COMMENT_MASHUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "DELETE_COMMENT_MASHUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;   
        		case "ADD_MEMBER_TO_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "REMOVE_MEMBER_FROM_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;          			
        		case "CREATE_MOCKUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;   
        		case "EDIT_MOCKUP_METADATA":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "DELETE_MOCKUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;          
        		case "EXPORT_MOCKUP_GADGET":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "EDIT_MOCKUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;   
        		case "UPDATE_MOCKUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "PUBLISH_MOCKUP_GADGET_TO_MARKETPLACE":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "ADD_MEMBER_TO_MOCKUP":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "REMOVE_MEMBER_FROM_MOCKUP":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;         			
        		case "COMMENT_MOCKUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "DELETE_COMMENT_MOCKUP_PROJECT":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;           			
        		case "EDIT_PUBLISHED_GADGET_METADATA":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;  
        		case "DELETE_PUBLISHED_GADGET":
                	streamActivity = makeActivity().published(new DateTime(activity.getCreatedAt())).actor(person).verb(Verb.get(activity.getActivityType())).summary(activity.getDescription()).get();
        			break;          			        			        			
        	}
        	
        	streamList.add(streamActivity);
        }		
		
		return streamList;
	}
	
	
	@Override
	public List<Activity> findAllActivity() {		
		
        List <com.mediigea.vme2.entity.Activity> listActivity = activityDAO.getAllOrdered();

        return streamList(listActivity);
         

/*
		    REGISTER_USER("REGISTER_USER"),
		    USER_LOGIN("USER_LOGIN"),
		    ENABLE_USER("ENABLE_USER"),
		    DISABLE_USER("DISABLE_USER"),		    
		    USER_LOGOUT("USER_LOGOUT"), 
		    USER_UPDATE_PASSWORD("USER_UPDATE_PASSWORD"), /* not implemented  * /
		    ADD_MEMBER_TO_WORKGROUP("ADD_MEMBER_TO_WORKGROUP"),
		    REMOVE_MEMBER_FROM_WORKGROUP("REMOVE_MEMBER_FROM_WORKGROUP"),
		    CREATE_WORKGROUP("CREATE_WORKGROUP"),
		    UPDATE_WORKGROUP("UPDATE_WORKGROUP"),
		    DELETE_WORKGROUP("DELETE_WORKGROUP"),
		    CREATE_ADMIN("CREATE_ADMIN"),
		    IMPORT_WADL("IMPORT_WADL"),
		    IMPORT_WSDL("IMPORT_WSDL"),		    
		    CREATE_CATEGORY("CREATE_CATEGORY"),
		    UPDATE_CATEGORY("UPDATE_CATEGORY"),
		    DELETE_CATEGORY("DELETE_CATEGORY"),
	        CREATE_MASHUP_PROJECT("CREATE_MASHUP_PROJECT"),
	        DELETE_MASHUP_PROJECT("DELETE_MASHUP_PROJECT"), 
	        COMMENT_MASHUP_PROJECT("COMMENT_MASHUP_PROJECT"),
	        DELETE_COMMENT_MASHUP_PROJECT("DELETE_COMMENT_MASHUP_PROJECT"),
	        ADD_MEMBER_TO_PROJECT("ADD_MEMBER_TO_PROJECT"),
	        REMOVE_MEMBER_FROM_PROJECT("REMOVE_MEMBER_FROM_PROJECT"),
	        EXPORT_MASHUP_PROJECT("EXPORT_MASHUP_PROJECT"),
	        EDIT_MASHUP_PROJECT("EDIT_MASHUP_PROJECT"),
	        


		    CREATE_MOCKUP_PROJECT("CREATE_MOCKUP_PROJECT"),
	        EDIT_MOCKUP_METADATA("EDIT_MOCKUP_METADATA"),
		    DELETE_MOCKUP_PROJECT("DELETE_MOCKUP_PROJECT"), 
		    EXPORT_MOCKUP_GADGET("EXPORT_MOCKUP_GADGET"),
	        EDIT_MOCKUP_PROJECT("EDIT_MOCKUP_PROJECT"),
	        UPDATE_MOCKUP_PROJECT("UPDATE_MOCKUP_PROJECT"), 
			PUBLISH_MOCKUP_GADGET_TO_MARKETPLACE("PUBLISH_MOCKUP_GADGET_TO_MARKETPLACE"),
		    EDIT_PUBLISHED_GADGET_METADATA("EDIT_PUBLISHED_GADGET_METADATA"),
	        DELETE_PUBLISHED_GADGET("DELETE_PUBLISHED_GADGET");	        




*******************************************
		    ENABLE_WEBSERVICE("ENABLE_WEBSERVICE"),
		    DISABLE_WEBSERVICE("DISABLE_WEBSERVICE"),
		    DELETE_WEBSERVICE("DELETE_WEBSERVICE"),
	        
		    UPDATE_PUBLISHED_GADGET_METADATA("UPDATE_PUBLISHED_GADGET_METADATA"), 
	        
	        UPDATE_MOCKUP_METADATA("UPDATE_MOCKUP_METADATA"), 		        
	        UPDATE_MASHUP_PROJECT("UPDATE_MASHUP_PROJECT"),
	        EDIT_MASHUP_METADATA("EDIT_MASHUP_METADATA"),
	        UPDATE_MASHUP_METADATA("UPDATE_MASHUP_METADATA"), 	        

        
 */
		
	}

	@Override
	public List <Activity> findActivityByProfile(Integer profileId) {
		if(profileId==null) return null;
		
        List <com.mediigea.vme2.entity.Activity> listActivity = activityDAO.getByProfileIdOrdered(profileId);
        return streamList(listActivity);		
 
	}
	
	@Override	
	public void activityStreamRecordSave(Profile p, String activityType) {
		com.mediigea.vme2.entity.Activity ac = new com.mediigea.vme2.entity.Activity();
		ac.setActivityType(activityType);
		ac.setDescription("User:" + p.getEmail());
		ac.setProfile(p);
		accountDAO.createAudit(ac);
	}
	
	@Override	
	public void activityStreamRecordSave(Profile p, String activityType, String description) {
		com.mediigea.vme2.entity.Activity ac = new com.mediigea.vme2.entity.Activity();
		ac.setActivityType(activityType);
		ac.setDescription(description);
		ac.setProfile(p);
		accountDAO.createAudit(ac);
	}	
	
}
