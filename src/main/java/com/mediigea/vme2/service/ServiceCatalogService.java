package com.mediigea.vme2.service;

import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.model.SwaggerParsingResponse;

import java.util.HashMap;
import java.util.List;

import org.apache.xmlbeans.XmlException;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mediigea.vme2.dto.ServiceItemDTO;
import com.mediigea.vme2.dto.ServiceOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ServiceCatalog;

/**
 * Interface to define management operations about services (Rest/Soap) catalog
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface ServiceCatalogService {

	public ServiceCatalog addService(ServiceCatalog service, Profile profile, int type);
	public ServiceCatalog addWsdlService(String Url, Profile profile) throws Exception ;
	public ServiceCatalog addWsdlService(byte[] bytes, Profile profile) throws Exception ;
	
	public ServiceCatalog addFromWadl(String serviceName, String wadl, Profile profile) throws XmlException, JsonProcessingException;
	
	public ServiceCatalog setVisibility(Integer serviceId, Integer code);
	
	public ServiceCatalog updateService(ServiceCatalog a);
	public void deleteService(Integer id) throws Exception;
	
	public List<ServiceCatalog> findAll(String hql, HashMap<String, Object> parameters);		
	
	public List<ServiceCatalog> findAdministratorsServices();
	public List<ServiceCatalog> findPublicServices();
	public List<ServiceCatalog> findVisibleServices(Integer profileId);
	
	public ServiceCatalog findService(int id);
	
	//json 
	public List<ServiceItemDTO> getServicesDTO(Profile p);
	public List<ServiceOperationDTO> getServiceOperationsDTO(int serviceId);	
	public List<ServiceParameterDTO> getOperationParamsDTO (int operationId, int serviceType);
	public List<ServiceParameterDTO> getTemplateParamsDTO (int operationId, int serviceType) throws Exception;
	
	public SwaggerParsingResponse validateSwaggerService(String swagger);
	public ServiceCatalog addFromSwagger(String swagger, Profile profile);
	
	public ServiceCatalog addRESTFromMetamodel(String descriptor, Profile profile);
	public ServiceCatalog addRESTFromMetamodel(BuildingBlock descriptor, Profile profile);
}
