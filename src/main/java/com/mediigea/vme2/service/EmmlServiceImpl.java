package com.mediigea.vme2.service;

import io.swagger.util.Json;
import it.eng.PropertyGetter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.CharEncoding;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dto.BlackboxDTO;
import com.mediigea.vme2.dto.BlackboxShapeDTO;
import com.mediigea.vme2.dto.GuiFileResourceDTO;
import com.mediigea.vme2.dto.InputShapeDTO;
import com.mediigea.vme2.dto.OpenDataShapeDTO;
import com.mediigea.vme2.dto.OperatorParameterDTO;
import com.mediigea.vme2.dto.OperatorShapeDTO;
import com.mediigea.vme2.dto.OutputShapeDTO;
import com.mediigea.vme2.dto.ServiceOperationDTO;
import com.mediigea.vme2.dto.ServiceShapeDTO;
import com.mediigea.vme2.dto.ShapeContainer;
import com.mediigea.vme2.dto.ShapeDTO;
import com.mediigea.vme2.dto.UD_InputShapeDTO;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.VariablesType;
import com.mediigea.vme2.emml.operator.IOperator;
import com.mediigea.vme2.emml.operator.Operator;
import com.mediigea.vme2.emml.service.BuildProjectEmmlInvoke;
import com.mediigea.vme2.emml.service.BuildOpenDataEmmlInvoke;
import com.mediigea.vme2.emml.service.BuildRestEmmlInvoke;
import com.mediigea.vme2.emml.service.BuildSoapEmmlInvoke;
import com.mediigea.vme2.emml.service.IBuildEmmlInvoke;
import com.mediigea.vme2.entity.AccessToken;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.GuiProjectFile;
import com.mediigea.vme2.entity.ODSResource;
import com.mediigea.vme2.entity.OpenDataResource;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectBlackbox;
import com.mediigea.vme2.entity.ProjectInnerProcess;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.exceptions.InvokeWorkflowException;
import com.mediigea.vme2.exceptions.NotSupportedMimeTypeException;
import com.mediigea.vme2.exceptions.UserNotAuthorizedException;
import com.mediigea.vme2.exceptions.WorkflowScriptException;
import com.mediigea.vme2.exceptions.WriteResourceException;
import com.mediigea.vme2.service.ProjectOperationService.OperationType;
import com.mediigea.vme2.util.Digraph;
import com.mediigea.vme2.util.EmmlScriptUtils;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.util.io.ZipHelper;


public class EmmlServiceImpl implements WorkflowService{

	private static final Logger logger = LoggerFactory.getLogger(EmmlServiceImpl.class);	

	@Autowired
	private AutowireCapableBeanFactory autowireBeanFactory;
	
	@Autowired
	private AccessTokenService accessTokenService;		
	
	@Autowired
	private ProjectService projectService;	

	@Autowired
	private BlackboxService blackboxService;
	
	@Autowired
	private OpenDataService openDataService;	
	
	@Autowired
	private ODSDatasetService odsDatasetService;
	
	@Autowired
	private ODSResourceService odsResourceService;
	
	@Autowired
	private ProjectOperationService projectOperationService;
	
	@Autowired
	private RestOperationService restOperationService;
	
	@Autowired
	private ProjectDatasetService projectDatasetService;

	@Autowired
	private ProjectBlackboxService projectBlackboxService;			

	@Autowired
	private ProjectInnerProcessService projectInnerProcessService;			
	
	@Autowired
	private IBuildEmmlInvoke buildProjectEmmlInvoke;
	
	@Autowired
	@Qualifier("oauthManagerService")
	private AuthManagerService providerManager;

	@Autowired
	private ObjectMapperExt objectMapper;

	private String emmlScriptFolder;
	private String emmlEngineContext;
	private String odataEngineContext;

	//private TypeReference<List<InputField>> typeRef = new TypeReference<List<InputField>>() {};
	
	@Override
	public JsonNode runMashup(int mashupId, @RequestParam Map<String, String> params, String access_token) throws Exception
	{
		URL emmlEngineUrl = new URL(PropertyGetter.getProperty("emml.engineContext") + "/" + mashupId);		 
        
		InputStream in = null;
        byte[] postDataBytes = null;
        
        String query = "";
        
        // retrieve mashup inputs
        Iterator<InputShapeDTO> inputsDTO = projectService.readInputShapesList(mashupId).iterator();
        
        InputShapeDTO inputDTO = null;
        UD_InputShapeDTO userData = null;
        
        String label = "";
        
        String field = "";
        String value = "";
        
        while(inputsDTO.hasNext())
        {
        	inputDTO = inputsDTO.next();
        	userData = inputDTO.getUserData();
        	
        	field = inputDTO.getId();
        	
        	if(query.compareTo("") != 0)
        		query += "&";
        	
        	// if constant input, set value to default value
        	if(userData.isIsConstant())
        	{
        		value = userData.getDefaultValue();
        	}
        	else
        	{
        		label = userData.getLabel();
        		
        		// search param for label
        		if(params.containsKey(label))
	        	{
        			value = params.get(label);
	        	}
        		else
        		{
        			// search param for input id
        			if(params.containsKey(field))
        			{
        				value = params.get(field);
        			}
        			else
        			{
        				ObjectNode response = JsonNodeFactory.instance.objectNode();
        				
        				response.put("error", true);
        				response.put("message", "Missing input parameters");
        				
        				return response;
        			}
        		}
        	}        	
        	
        	query += field + "=" + value;
        }
        
        // if services requires authentication, add token to query
        List<ProjectOperation> services = projectOperationService.getAllByProject(mashupId);
        
        RestOperation restOp = null;
        ServiceCatalog sc = null;
        
        for(int i = 0; i < services.size(); i++)
		{
			if(services.get(i).getRestOperation() != null)
			{
				restOp = services.get(i).getRestOperation();
				restOp = restOperationService.findRestOperationById(restOp.getId());
				sc = restOp.getServiceCatalog();
				
				if(Boolean.parseBoolean(restOp.getAuthentication()))
				{
					if(access_token == null)
					{
						ObjectNode response = JsonNodeFactory.instance.objectNode();
						
						response.put("error", true);
						response.put("message", "Authorization required");
						
						return response;
					}
					
					if(query.compareTo("") != 0)
		        		query += "&";
					
					query += VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX + sc.getId() + "_" + restOp.getId() + "=" + access_token;
				}
			}
		}
        
        //System.out.println("URL: " + emmlEngineUrl.toString() + "\nQuery: " + query);
        postDataBytes = query != "" ? query.toString().getBytes("UTF-8") : null;
        
        HttpURLConnection conn = (HttpURLConnection)emmlEngineUrl.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        
        int length = postDataBytes != null ? postDataBytes.length : 0;
        conn.setRequestProperty("Content-Length", String.valueOf(length));
        conn.setDoOutput(true);
        
        if(postDataBytes!=null)
        {
        	conn.getOutputStream().write(postDataBytes);
        }
        
        in=conn.getInputStream();
        String res = IOUtils.toString(in, "UTF-8");
		in.close();
		
		ObjectNode mashupResponse = (ObjectNode) new ObjectMapper().readTree(res);
		
		Iterator<String> keys = mashupResponse.fieldNames();
		
		List<JsonNode> nodes = new ArrayList<JsonNode>();
		JsonNode node = null;
		String key = "";
		
		while(keys.hasNext())
		{
			key = keys.next();
			
			if(key.compareTo("__EMML_ERROR") == 0)
			{
				return mashupResponse;
			}
			
			node = mashupResponse.get(key);
			
			nodes.add(mashupResponse.get(key));
		}
		
		if(nodes.size() == 1)
			return nodes.get(0);
		else
			return mashupResponse;
	}
	
	public void buildWorkflowSchema(Integer projectId, ShapeContainer shapeContainer) throws WorkflowScriptException {		

		//Costruisce il grafo dei servizi a partire da servizi, input e connessioni
		logger.debug("Building oriented graph of shapes...");
		Digraph<ShapeDTO> graph = EmmlScriptUtils.graphBuilder(shapeContainer);

		//Effettua l'ordinamento dei nodi secondo topographic sort
		logger.debug("Applying topological sort to graph...");
		List<ShapeDTO> sortedNodes = graph.topSort();

		//Costruisce lo script EMML a partire dal grafo ordinato
		logger.debug("Building workflow schema...");
		projectOperationService.deleteAllByProject(projectId);
		// IVAN
		projectDatasetService.deleteAllByProject(projectId);
		projectBlackboxService.deleteAllByProject(projectId);
		projectInnerProcessService.deleteAllByProject(projectId);
		
		Mashup rootTag = this.buildMashup(projectId, sortedNodes, true);				

		//Scrive lo script su file system
		logger.debug("Writing emml script file to file system...");
		try {
			//Esegue il marshalling degli oggetti rappresentanti lo script emml
			ByteArrayOutputStream xmlScript = EmmlScriptUtils.marshalEmmlXml(rootTag);

			EmmlScriptUtils.writeEmmlScriptFile(emmlScriptFolder, projectId.toString(), xmlScript.toByteArray());

		} catch (IOException e) {
			logger.error("Error writing emml script file");
			throw new WorkflowScriptException(e);

		} catch (JAXBException e) {
			logger.error("Error marshalling emml script");
			throw new WorkflowScriptException(e);
		}		
	}

	@Override
	public void buildServiceChainWorkflowSchema(Integer projectId, ShapeContainer shapeContainer, String currentServiceId) throws WorkflowScriptException{
		logger.debug("Buildin service chain workflow...");
		
		//Costruisce il grafo dei servizi a partire da servizi, input e connessioni
		logger.debug("Building oriented graph of shapes...");
		Digraph<ShapeDTO> graph = EmmlScriptUtils.graphBuilder(shapeContainer);

		//Effettua l'ordinamento dei nodi secondo topographic sort
		logger.debug("Applying topological sort to get order invocation...");
		List<ShapeDTO> sortedNodes = graph.topSort();

		ShapeDTO selectedShape = null;
		
		// IVAN
		if(sortedNodes.size() == 0)
		{
			if(shapeContainer.getServices().containsKey(currentServiceId))
			{
				sortedNodes.add(shapeContainer.getServices().get(currentServiceId));
				sortedNodes.add(shapeContainer.getOutput());
			}
			else if(shapeContainer.getOpenData().containsKey(currentServiceId))
			{
				sortedNodes.add(shapeContainer.getOpenData().get(currentServiceId));
				sortedNodes.add(shapeContainer.getOutput());
			}
		}
		
		//System.out.println("currentServiceId: "+ currentServiceId);
		
		for(ShapeDTO service : sortedNodes)
		{
			//System.out.println("ShapeId: "+ service.getId());
			
			if (currentServiceId.equalsIgnoreCase(service.getId())){
				selectedShape = service;
				break;
			}
		}
		
		// IVAN
		// fix dataset preview issue 
		if(selectedShape == null)
		{
			if(shapeContainer.getServices().containsKey(currentServiceId))
			{
				selectedShape = shapeContainer.getServices().get(currentServiceId);
				sortedNodes.remove(sortedNodes.size() - 1);
				sortedNodes.add(shapeContainer.getServices().get(currentServiceId));
			}
			else if(shapeContainer.getOpenData().containsKey(currentServiceId))
			{
				selectedShape = shapeContainer.getOpenData().get(currentServiceId);
				sortedNodes.remove(sortedNodes.size() - 1);
				sortedNodes.add(shapeContainer.getOpenData().get(currentServiceId));
			}
		}

		if(selectedShape!=null){
			int currentShapeIdx = sortedNodes.indexOf(selectedShape);

			logger.debug("Getting a nodes sub list for selected service chain...");
			List<ShapeDTO> sublist = sortedNodes.subList(0, currentShapeIdx+1);

			//Costruisce lo script EMML a partire dal grafo ordinato
			logger.debug("Building workflow schema...");
			projectOperationService.deleteAllByProject(projectId);
			// IVAN
			projectDatasetService.deleteAllByProject(projectId);
			projectBlackboxService.deleteAllByProject(projectId);
			projectInnerProcessService.deleteAllByProject(projectId);
			
			Mashup rootTag = this.buildMashup(projectId, sublist, false);
			
			List<String> outputNode = new ArrayList<String>();
			outputNode.add(selectedShape.getId());
			
			EmmlScriptUtils.buildOutputScript(rootTag, projectId, outputNode);			
			
			try {
				//Esegue il marshalling degli oggetti rappresentanti lo script emml
				ByteArrayOutputStream xmlScript = EmmlScriptUtils.marshalEmmlXml(rootTag);

				logger.debug("Xml Workflow: "+xmlScript.toString());

				//Scrive lo script su file system
				logger.debug("Writing emml script file to file system...");
				EmmlScriptUtils.writeEmmlScriptFile(emmlScriptFolder, projectId.toString()+"_chain", xmlScript.toByteArray());	
			} catch (IOException e) {
				logger.error("Error writing emml script file");
				throw new WorkflowScriptException(e);

			} catch (JAXBException e) {
				logger.error("Error marshalling emml script");
				throw new WorkflowScriptException(e);
			}	
		}
	}	

	private Mashup buildMashup(int projectId, List<ShapeDTO> sortedNodes, boolean full) throws WorkflowScriptException{
		ObjectFactory of = new ObjectFactory();

		//Crea il rootTag
		Mashup rootTag =  of.createMashup();										

		//Crea il tag variables contenitore delle variabili esplicite 
		VariablesType variables = new VariablesType();

		//aggiunge il contenitore variables al roottag
		rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupVariables(variables));
		
		// IVAN: wadl params	
		List<String> wadlParams = new ArrayList<String>();
		//
		
		for(ShapeDTO node : sortedNodes)
		{
			if(node!=null)
			{							
				String nodeId = node.getId().replaceAll("[^A-Za-z0-9]", "_");//.replace('.', '_').replace('-', '_');
	
				if(node instanceof InputShapeDTO)
				{	
					//Selezionato nodo di input
					InputShapeDTO inputNode = (InputShapeDTO) node;
					
					wadlParams.add(node.getId());
	
					//Crea un tag di tipo parametro di input 
					Mashup.Input inputParam = new Mashup.Input();
					inputParam.setType(VmeConstants.EMML_VAR_TYPE_STRING);
					inputParam.setName(inputNode.getId());
	
					//Lo aggiunge al root tag
					rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupInput(inputParam));
	
				} 
				else if(node instanceof ServiceShapeDTO)
				{	
					//Selezionato nodo servizio
					ServiceShapeDTO serviceNode = (ServiceShapeDTO) node;
	
					//Prende l'operazione inclusa nello shape servizio
					ServiceOperationDTO operation = serviceNode.getUserData().getOperation();
	
					//Prende il tipo di servizio (REST O WSDL)
					int serviceType = operation.getServiceType();					
					
					if (serviceType == VmeConstants.SERVICECATALOG_TYPE_REST) 
					{
						BuildRestEmmlInvoke rsi = new BuildRestEmmlInvoke(of, rootTag, variables, operation, nodeId);
						autowireBeanFactory.autowireBean(rsi);
						
						rsi.buildInvokeEmml();

						projectOperationService.create(projectId, operation.getId(), OperationType.REST);

					} 
					else 
					{	
						try {
							BuildSoapEmmlInvoke ssi = new BuildSoapEmmlInvoke(of, rootTag, variables, operation, nodeId);
							autowireBeanFactory.autowireBean(ssi);
							
							ssi.buildInvokeEmml();

							projectOperationService.create(projectId, operation.getId(), OperationType.SOAP);
						} catch (Exception e) {
							
							logger.error("Error during building script for soap operation: "+ node.getId());
							throw new WorkflowScriptException(e);
						}
					}					
				} 
				else if(node instanceof OperatorShapeDTO)
				{
					try {
						OperatorShapeDTO operatorNode = ((OperatorShapeDTO) node);										
						
						String emmlScriptGenerator = operatorNode.getUserData().getEmmlscriptgenerator();
						
						IOperator operatore = null;
						
						if(emmlScriptGenerator!=null && !emmlScriptGenerator.isEmpty()){
							Class<?> c = Class.forName(emmlScriptGenerator);
							
							Constructor<?> cons = c.getConstructor(
									ObjectFactory.class, 
									Mashup.class, 
									VariablesType.class, 
									OperatorShapeDTO.class,
									Integer.class
									);
							
							operatore = (IOperator) cons.newInstance(
									of, 
									rootTag, 									
									variables, 
									operatorNode,
									projectId
									);
							autowireBeanFactory.autowireBean(operatore);
							
						} else {
							operatore = new Operator(of, rootTag, variables, operatorNode, projectId);							
						}						
						
						operatore.buildEmmlCommand();
						
					} catch (Exception e) {
						logger.error("Error during building script for operator: " + node.getId());
						throw new WorkflowScriptException(e);
					} 
					
				} 
				else if(node instanceof BlackboxShapeDTO)
				{
					try {
						//Selezionato nodo servizio
						BlackboxShapeDTO serviceNode = (BlackboxShapeDTO) node;																		
						BlackboxDTO blackbox = blackboxService.getAsDTO(serviceNode.getUserData().getId());
						
						buildProjectEmmlInvoke.buildInvokeEmml(of, rootTag, serviceNode.getUserData().getParameters(), variables, blackbox.getId(), nodeId);															
	
						projectBlackboxService.create(projectId, blackbox.getId());
					} catch (Exception e) {
						logger.error("Error during building script for blackbox: " + node.getId());
						throw new WorkflowScriptException(e);
					} 
				} 
				else if(node instanceof OpenDataShapeDTO)
				{					
					//Selezionato nodo servizio
					OpenDataShapeDTO odataNode = (OpenDataShapeDTO) node;																		
					
					// IVAN
					//OpenDataResource openDataResource = openDataService.getById(odataNode.getUserData().getId());
					int datasetId = odataNode.getUserData().getId();
					int resourceId = odataNode.getUserData().getResourceId();
					String query = odataNode.getUserData().getQuery();
					
					//BuildOpenDataEmmlInvoke rsi = new BuildOpenDataEmmlInvoke(of, rootTag, variables, openDataResource, odataEngineContext, nodeId);
					BuildOpenDataEmmlInvoke rsi = new BuildOpenDataEmmlInvoke(of, rootTag, variables, 
							odsDatasetService.findDataset(datasetId).getOdsDatasetId(), 
							odsResourceService.findODSResource(resourceId).getOdsResourceId(),
							query, 
							nodeId);
					autowireBeanFactory.autowireBean(rsi);
					rsi.buildInvokeEmml2();		
					
					// add dependency to the project
					projectDatasetService.create(projectId, resourceId);
									
				} 
				else if(node instanceof OutputShapeDTO)
				{
					List<String> outputNodes = ((OutputShapeDTO) node).getInputs();
					
					EmmlScriptUtils.buildOutputScript(rootTag, projectId, outputNodes);
				}
			}
		}	
		
		// IVAN
		if(full)
		{
			// Generate WADL
			try
			{
				DocumentBuilderFactory docFactory;
				DocumentBuilder docBuilder;
				
				docFactory = DocumentBuilderFactory.newInstance();
				docBuilder = docFactory.newDocumentBuilder();
				
				Document doc = docBuilder.newDocument();
				Element rootElement = doc.createElement("application");
				doc.appendChild(rootElement);
				
				rootElement.setAttribute("xmlns", "http://wadl.dev.java.net/2009/02");
				rootElement.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
				
				Element resources = doc.createElement("resources");
				rootElement.appendChild(resources);				
				resources.setAttribute("base", PropertyGetter.getProperty("oauth.callbackContext") + "/invokemashup");
				
				Element resource = doc.createElement("resource");
				resources.appendChild(resource);				
				resource.setAttribute("path", Integer.toString(projectId));
				
				Element method = doc.createElement("method");
				resource.appendChild(method);
				method.setAttribute("name", "POST");				
				method.setAttribute("id", projectService.findProject(projectId).getName());
				
				Element request = doc.createElement("request");
				method.appendChild(request);
				
				Element param;
				
				for(int i = 0; i < wadlParams.size(); i++)
				{
					param = doc.createElement("param");
					request.appendChild(param);
					
					param.setAttribute("name", wadlParams.get(i));
					param.setAttribute("type", "xsd:string");
					param.setAttribute("style", "query");
					param.setAttribute("required", "true");
				}
				
				Element response = doc.createElement("response");
				method.appendChild(response);
				
				Element representation = doc.createElement("representation");
				response.appendChild(representation);
				representation.setAttribute("mediaType", "application/json");
				
				// write the content into xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				
				//StreamResult result = new StreamResult(System.out);
				StreamResult result = new StreamResult(new File(PropertyGetter.getProperty("app.baseDir") 
						+ File.separatorChar + "vme-editor" + File.separatorChar + "wadl" + File.separatorChar + projectId + ".xml"));
				
				transformer.transform(source, result);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return rootTag;
	}
	
	@Override
	public String invokeServiceChain(int projectId, Map<String, String> inputParameters, String userConnectorId) throws InvokeWorkflowException, UserNotAuthorizedException{
		
		String scriptUrl = emmlEngineContext+"/"+projectId+"_chain";	
		//System.out.println("!!!! " + scriptUrl + " !!!!");
		
		return invoke(projectId, inputParameters, userConnectorId, scriptUrl);		
	}
	
	@Override
	public String invokeWorkflow(int projectId, Map<String, String> inputParameters, String userConnectorId) throws InvokeWorkflowException, UserNotAuthorizedException{
		
		String scriptUrl = emmlEngineContext+"/"+projectId;	
		//System.out.println("!!!! " + scriptUrl + " !!!!");
		
		return invoke(projectId, inputParameters, userConnectorId, scriptUrl);		
	}

	private String invoke(int projectId, Map<String, String> inputParameters, String userConnectorId, String scriptUrl) throws InvokeWorkflowException, UserNotAuthorizedException
	{
		String response = null;
		
		try 
		{
			/*
			int i = 0;
			Iterator<Entry<String, String>> it = inputParameters.entrySet().iterator();
			
			while (it.hasNext()) {
				Entry<String, String> pairs = it.next();

				if(i++==0) {
					scriptUrl+="?";
				}else{
					scriptUrl+="&";
				}

				String value = URLEncoder.encode(pairs.getValue(), "UTF-8");
				scriptUrl+=pairs.getKey()+"="+value;
			}	*/	
			
			// IVAN allow constant inputs
			
			Map<String, String> params = new HashMap<String, String>();
			
			Iterator<Entry<String, String>> it = inputParameters.entrySet().iterator();
			
			while (it.hasNext()) 
			{
				Entry<String, String> pairs = it.next();
				
				params.put(pairs.getKey(), URLEncoder.encode(pairs.getValue(), "UTF-8"));
			}			
			
			// retrieve mashup inputs
	        Iterator<InputShapeDTO> inputsDTO = projectService.readInputShapesList(projectId).iterator();
	        
	        InputShapeDTO inputDTO = null;
	        UD_InputShapeDTO userData = null;
	        
	        String field = "";
	        String value = "";
	        
	        int i = 0;
	        
	        while(inputsDTO.hasNext())
	        {
	        	inputDTO = inputsDTO.next();
	        	userData = inputDTO.getUserData();
	        	
	        	field = inputDTO.getId();
	        	
	        	if(i++==0) 
	        	{
					scriptUrl+="?";
				}
	        	else
	        	{
					scriptUrl+="&";
	        	}				
	        	
	        	// if constant input, set value to default value
	        	if(userData.isIsConstant())
	        	{
	        		value = userData.getDefaultValue();
	        	}
	        	else
	        	{
	        		value = params.get(field);
	        	}        	
	        	
	        	scriptUrl += field + "=" + value;
	        }
			
			//			

			List<Integer> projectScanned = new ArrayList<Integer>();

			List<ProjectOperation> prjOperations = projectOperationService.getAllByProject(projectId);
			scriptUrl = this.oauthTokenManager(prjOperations, projectScanned, userConnectorId, scriptUrl);
			
			List<ProjectBlackbox> prjBlackboxes = projectBlackboxService.getAllByProject(projectId);
			for(ProjectBlackbox prjBlack : prjBlackboxes){
				List<ProjectOperation> blackboxOperations = projectOperationService.getAllByProject(prjBlack.getBlackbox().getMashup().getId());
				scriptUrl = this.oauthTokenManager(blackboxOperations, projectScanned, userConnectorId, scriptUrl);
			}

			List<ProjectInnerProcess> prjInnerProcess = projectInnerProcessService.getAllByProject(projectId);
			for(ProjectInnerProcess prjInner : prjInnerProcess){
				List<ProjectOperation> innerProcessOperations = projectOperationService.getAllByProject(prjInner.getInnerProcess().getId());
				scriptUrl = this.oauthTokenManager(innerProcessOperations, projectScanned, userConnectorId, scriptUrl);
			}
			
			InputStream in = null;
			/*codice usato per la richiesta GET all'applicazione emml
			URL emmlEngileUrl = new URL(scriptUrl);
			in = emmlEngileUrl.openStream();
			response = IOUtils.toString(in, "UTF-8");

			in.close();
			 */
			//codice usato per richiesta POST all'applicazione emml nel caso in cui i parametri da passare hanno valori troppo lunghi
			URL emmlEngileUrl = new URL(scriptUrl.split("\\?")[0]);
			//System.out.println("emmlEngileUrl "+emmlEngileUrl);			 
	        String[] urlParts = scriptUrl.split("\\?");
	        //System.out.println("urlParts "+urlParts.length);
	        
	        byte[] postDataBytes=null;
	        if (urlParts.length > 1) {
	            String query = urlParts[1];
	            
	            System.out.println("Execute Mashup\nURL: " + emmlEngileUrl.toString() + "\nQuery: " + query);
	            
	            //System.out.println("query "+query);
	            postDataBytes = query!=null?query.toString().getBytes("UTF-8"):null;
	        }
	        HttpURLConnection conn = (HttpURLConnection)emmlEngileUrl.openConnection();
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        int length=postDataBytes!=null?postDataBytes.length:0;
	        conn.setRequestProperty("Content-Length", String.valueOf(length));
	        conn.setDoOutput(true);
	        if(postDataBytes!=null){
	        conn.getOutputStream().write(postDataBytes);
	        }
	        in=conn.getInputStream();
	        response = IOUtils.toString(in, "UTF-8");
			in.close();

				
		} catch (MalformedURLException e) {
			logger.error("Error invoking workflow", e);
			throw new InvokeWorkflowException(e);

		} catch (IOException e) {
			logger.error("Error invoking workflow", e);
			throw new InvokeWorkflowException(e);
		} catch (UserNotAuthorizedException e) {// IVAN forward UserNotAuthorizedException to ProjectController
			logger.error("UserNotAuthorizedException", e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Error invoking workflow", e);
			throw new InvokeWorkflowException(e);
		}

		return response;
	}
	
	private String oauthTokenManager(List<ProjectOperation> prjOperations, List<Integer> projectScanned, String userConnectorId, String scriptUrl) throws UserNotAuthorizedException{
		for(ProjectOperation operation : prjOperations){
			Integer serviceId = null;

			ServiceCatalog service = null;
			String authorizationRequired = null;
			Integer operationId = null;
			
			String authType = "";

			if(operation.getRestOperation()!=null){
				RestOperation restOperation = operation.getRestOperation();
				authorizationRequired = restOperation.getAuthentication();
				service = restOperation.getServiceCatalog();
				serviceId = service.getId();
				
				operationId = restOperation.getId();
				
				if(restOperation.getAuthentication().compareTo("true") == 0)
					authType = restOperation.getAuthType().getType();

			}else if(operation.getSoapOperation()!=null){				
				service = operation.getSoapOperation().getServiceCatalog();
				serviceId = service.getId();
				
				operationId = operation.getSoapOperation().getId();
				
				// TODO: same stuff for SOAP
			}
			
			// IVAN: allow auth at operation level			
			boolean oauth = false;
			
			if(service.getAuthType() == null)
			{
				if(authType.compareTo("") != 0 && authType.compareTo(VmeConstants.BASICAUTH) != 0)
					oauth = true;
			}
			else
			{
				oauth = !VmeConstants.BASICAUTH.equalsIgnoreCase(service.getAuthType().getType());
				authType = service.getAuthType().getType();
			}
			//

			if(!projectScanned.contains(serviceId) && "true".equalsIgnoreCase(authorizationRequired) && oauth)
			{
				AccessToken token = accessTokenService.getToken(userConnectorId, serviceId, operationId);

				if(token!=null)
				{
					if(authType.compareTo(VmeConstants.OAUTH10VERSION) != 0)
					{
						// IVAN: bug fixed
						scriptUrl += (scriptUrl.indexOf("?") != -1 ? "&" : "?") + VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX + serviceId + "_" + operationId + "=" + token.getAccessToken();
					}
					else
					{
						scriptUrl+="&"+VmeConstants.EMML_OAUTH_TOKENSECRET_PREFIX+serviceId+"="+token.getTokenSecret();					
					}
					
					projectScanned.add(serviceId);
				} 
				else 
				{
					throw new UserNotAuthorizedException(serviceId, operationId);
				}
			}		
		}	
		
		return scriptUrl;
	}
	
	
	public InputStream getWorkflowArchive(Integer projectId) throws IOException{
		//Crea la directory temporanea wflow
		String tmpPath = FileUtils.getTempDirectoryPath();
		String wflowPath = tmpPath + "/" + VmeConstants.WORKFLOW_DIR_PREFIX + projectId;		
		File wflowDir = new File(wflowPath);		
		FileUtils.forceMkdir(wflowDir);

		//Prende la path dello script emml
		String emmlScript = emmlScriptFolder+"/"+projectId+".emml";

		//Copia lo script emml nella directory creata
		File emmlFile = new File(emmlScript);		
		FileUtils.copyFileToDirectory(emmlFile, wflowDir);

		//Legge lo schema di rendering dal db
		Project p = projectService.findProject(projectId);

		//Crea il file relativo nella directory temporanea
		String renderingSchema = p.getRenderingSchema();
		File renderingFile = new File(wflowPath+"/"+VmeConstants.RENDERSCHEMA_FILENAME);
		FileUtils.writeStringToFile(renderingFile, renderingSchema);		

		//Comprime la directory e la restituisce
		ZipHelper zipHelper = new ZipHelper();

		String archiveName = VmeConstants.WORKFLOW_ARCHIVE_FILENAME_PREFIX+ projectId+".zip";

		zipHelper.zipDirectory(wflowDir, wflowPath+"/"+archiveName);		

		FileSystemResource resource = new FileSystemResource(wflowPath+"/"+archiveName);

		return resource.getInputStream();
	}
	/**
	 * @return the emmlScriptFolder
	 */
	public String getEmmlScriptFolder() {
		return emmlScriptFolder;
	}


	/**
	 * @param emmlScriptFolder the emmlScriptFolder to set
	 */
	public void setEmmlScriptFolder(String emmlScriptFolder) {
		this.emmlScriptFolder = emmlScriptFolder;
	}


	/**
	 * @return the emmlEngineContext
	 */
	public String getEmmlEngineContext() {
		return emmlEngineContext;
	}


	/**
	 * @param emmlEngineContext the emmlEngineContext to set
	 */
	public void setEmmlEngineContext(String emmlEngineContext) {
		this.emmlEngineContext = emmlEngineContext;
	}

	/**
	 * @return the odataEngineContext
	 */
	public String getOdataEngineContext() {
		return odataEngineContext;
	}

	/**
	 * @param odataEngineContext the odataEngineContext to set
	 */
	public void setOdataEngineContext(String odataEngineContext) {
		this.odataEngineContext = odataEngineContext;
	}		
	
	@Override
	public void writeXsl(MultipartFile content) throws WriteResourceException {						
		
		String contentType = content.getContentType();
		
		try {			
			if("text/xml".equals(contentType)||"text/xsl".equals(contentType) || "application/xslt+xml".equals(contentType)){
			
				String destDir = emmlScriptFolder+"/xsl";
				
				String fullPath = destDir + "/" + content.getOriginalFilename();
	
				File file = new File(fullPath);
	
				if(!file.exists()){																		
					FileUtils.copyInputStreamToFile(content.getInputStream(), file);						
					
				} else {
					throw new FileExistsException();
				}
			} else {
				throw new NotSupportedMimeTypeException("Mime type not supported!");
			}
		} catch (FileNotFoundException e) {
			String msg = "Writing xsl file failed: file not found" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);
		} catch (FileExistsException e) {
			String msg = "Writing xsl file failed: file already exists" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);		
		} catch (IOException e) {
			String msg = "Writing xsl file failed: IO Error" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);
		} catch (NotSupportedMimeTypeException e) {
			String msg = "Writing xsl file failed: Mime Type not supported" + (e.getMessage()!=null ? " - " + e.getMessage() : "");
			logger.error(msg);
			throw new WriteResourceException(msg, e);
		}				
	}
	
	@Override
	public Collection<File> getXslFileList() {			
		
		File dir = new File(emmlScriptFolder+"/xsl");
		
		return FileUtils.listFiles(dir, null, false);
	}

}
