package com.mediigea.vme2.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediigea.vme2.dao.OrganizationDAO;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.entity.Organization;
import com.mediigea.vme2.entity.Profile;

@Service("organizationService")
public class OrganizationServiceImpl implements OrganizationService {
	
	private static final Logger logger = LoggerFactory.getLogger(OrganizationServiceImpl.class);

	@Autowired
	private OrganizationDAO organizationDAO;
	
	@Autowired
	private ProfileDAO profileDAO;
	
	@Autowired
	private ProfileService profileService;

	@Override
	public Organization addOrganization(Organization o) 
	{
		organizationDAO.create(o);
		return o;
	}

	@Override
	public Organization updateOrganization(Organization o)
	{
		return organizationDAO.update(o);
	}

	@Override
	public void deleteOrganization(Integer id) 
	{
		Organization o = organizationDAO.getById(id);
		
		if(o != null)
			organizationDAO.delete(o);		
	}

	@Override
	public List<Organization> findAll() 
	{
		return organizationDAO.getAll();
	}

	@Override
	public Organization findOrganizationById(Integer id) 
	{
		return organizationDAO.getById(id);
	}

	@Override
	public Organization findOrganizationByExtId(Integer extId) 
	{
		if (extId == null)
			return null;

		String hql = "from Organization as o where o.extId=:extId";
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("extId", extId);

		List<Organization> list = organizationDAO.getAllBy(hql, params);

		if (list == null || list.isEmpty() || (list != null && list.size() > 1))
			return null;

		return list.get(0);
	}

	@Override
	public List<Profile> findOrganizationMembersById(Integer id) 
	{
		if (id == null)
			return null;

		String hql = "from Profile as profile where profile.organization.id=:organizationId and profile.organizationRole=1";
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("organizationId", id);

		List<Profile> pList = profileDAO.getAllBy(hql, params);

		if (pList == null || pList.isEmpty())
			return null;

		return pList;
	}
	
	@Override
	public List<Profile> findOrganizationMembersByExtId(Integer extId) 
	{
		if (extId == null)
			return null;

		String hql = "from Profile p join fetch p.organization o where o.extId=:extId and profile.organizationRole=1";
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("extId", extId);

		List<Profile> pList = profileDAO.getAllBy(hql, params);

		if (pList == null || pList.isEmpty())
			return null;

		return pList;
	}

}
