package com.mediigea.vme2.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.ServiceCatalogExtDAO;
import com.mediigea.vme2.entity.ServiceCatalogExt;

public class ServiceCatalogExtServiceImpl implements ServiceCatalogExtService
{
	private static final Logger logger = LoggerFactory.getLogger(ServiceCatalogExtServiceImpl.class);
	
	@Autowired
	private ServiceCatalogExtDAO serviceCatalogExtDAO;
	
	@Override
	public ServiceCatalogExt add(ServiceCatalogExt service)
	{
		if(service == null)
			return null;
		
		serviceCatalogExtDAO.create(service);
		
		return service;
	}
	
	@Override
	public ServiceCatalogExt add(int id)
	{
		ServiceCatalogExt service = new ServiceCatalogExt();
		service.setId(id);
		service.setFrom_marketplace(false);
		service.setEndpoint_url("");
		service.setLink_page("");
		service.setDescription("");
				
		return add(service);
	}
	
	@Override
	public ServiceCatalogExt add(int id, String endpoint)
	{
		ServiceCatalogExt service = new ServiceCatalogExt();
		service.setId(id);
		service.setFrom_marketplace(false);
		service.setEndpoint_url(endpoint);
		service.setLink_page("");
		service.setDescription("");
				
		return add(service);
	}
	
	@Override
	public ServiceCatalogExt update(ServiceCatalogExt service)
	{
		if (service == null)
			return null;

		service = serviceCatalogExtDAO.update(service);

		return service;
	}
	
	@Override
	public void delete(int id)
	{
		try
		{
			serviceCatalogExtDAO.deleteById(id);
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public ServiceCatalogExt getById(int id)
	{
		return serviceCatalogExtDAO.getById(id);
	}
	
	@Override
	public ServiceCatalogExt getByEndpoint(String endpoint)
	{
		String hql = "from ServiceCatalogExt as sc where sc.endpoint_url=:endpoint";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		parameters.put("endpoint", endpoint);
		
		List<ServiceCatalogExt> findList =  serviceCatalogExtDAO.getAllBy(hql, parameters);
		
		if(findList.size() > 0)
			return findList.get(0);
		
		return null;
	}
}
