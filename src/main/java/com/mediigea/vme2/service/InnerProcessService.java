package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.dto.InnerProcessDTO;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.exceptions.SelectShapeException;
import com.mediigea.vme2.exceptions.UsedInnerProcessException;

/**
 * Interface to define operations about Inner Processes management
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface InnerProcessService {
	
	public List<InnerProcessDTO> findAllByProfileAsDTO(Profile p, int project_id) throws SelectShapeException;

//	InnerProcessDTO getAsDTO(int id);
}
