package com.mediigea.vme2.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mediigea.vme2.dao.IdeaWorkgroupDAO;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.dao.ProfileWorkgroupDAO;
import com.mediigea.vme2.dao.WorkgroupDAO;
import com.mediigea.vme2.dto.WorkgroupDTO;
import com.mediigea.vme2.entity.IdeaWorkgroup;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProfileWorkgroup;
import com.mediigea.vme2.entity.ProfileWorkgroupId;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.util.PagedSearch;

@Service("workgroupService")
public class WorkgroupServiceImpl implements WorkgroupService {

	@Autowired
	private WorkgroupDAO workgroupDAO;
	
	@Autowired
	private IdeaWorkgroupDAO ideawgDAO;
	
	@Autowired
	private IdeaWorkgroupService ideawgService;

	@Autowired
	private ProfileDAO profileDAO;

	@Autowired
	private ProfileWorkgroupDAO profileworkgroupDAO;

	//private static final Logger logger = LoggerFactory.getLogger(WorkgroupServiceImpl.class);

	@Override
	public Workgroup addWorkgroup(Workgroup a) {

		a.setUpdatedAt(new Date());
		a.setCreatedAt(new Date());

		workgroupDAO.create(a);
		return a;
	}

	@Override
	public Workgroup updateWorkgroup(Workgroup a) {

		a.setUpdatedAt(new Date());

		workgroupDAO.update(a);
		return a;
	}

	@Override
	public Workgroup deleteWorkgroup(Integer id) {

		if (id == null)
			return null;

		Workgroup w = workgroupDAO.getById(id);

		if (w == null)
			return null;
		
		IdeaWorkgroup ideawg = ideawgService.getIdeaWorkgroupByWorkgroupId(id);
		if(ideawg != null) ideawgDAO.deleteById(ideawg.getIdeaId());

		workgroupDAO.delete(w);

		return w;
	}

	@Override
	public Workgroup findWorkgroup(Integer id) {

		return workgroupDAO.getById(id);
	}
	
	@Override
	public Workgroup findByName(String name) 
	{
		String hql = "from Workgroup w where w.name=:wName";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("wName", name);

		List<Workgroup> list = workgroupDAO.getAllBy(hql, parameters);
		
		if(list.isEmpty()) return null;
		
		return list.get(0);
	}

	@Override
	public List<Workgroup> findByProfile(Profile p) {

		String hql = "from Workgroup w join fetch w.profileWorkgroups pw where pw.pk.profile.id=:pId";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pId", p.getId());

		return workgroupDAO.getAllBy(hql, parameters);
	}

	@Override
	public List<Workgroup> findAll() {
		String sql = "from Workgroup as w order by w.name";

		return workgroupDAO.getAllBy(sql, null);

	}

	@Override
	public List<Workgroup> findAllWithProfiles() {
		String sql = "Select distinct w from Workgroup w left join fetch w.profileWorkgroups pw order by w.name";

		return workgroupDAO.getAllBy(sql, null);

	}

	@Override
	public void membersAdd(Integer workgroupId, List<Integer> profileIds) {

		for (Integer id : profileIds) {
			this.memberAdd(workgroupId, id);			
		}
	}

	@Override
	public void memberAdd(Integer workgroupId, Integer profileId) {

		Workgroup w = workgroupDAO.getById(workgroupId);
		Profile profile = profileDAO.getById(profileId);
		ProfileWorkgroup entity = new ProfileWorkgroup();
		entity.setProfile(profile);
		entity.setWorkgroup(w);
		entity.setWorkgroupRole(0);
		entity.setCreatedAt(new Date());
		profileworkgroupDAO.create(entity);

	}

	@Override
	public void memberRemove(Integer workgroupId, Integer memberId) {
		Profile p = profileDAO.getById(memberId);
		Workgroup w = workgroupDAO.getById(workgroupId);
		ProfileWorkgroupId pk = new ProfileWorkgroupId(p, w);

		ProfileWorkgroup pw = profileworkgroupDAO.getById(pk);
		profileworkgroupDAO.delete(pw);

	}

	@Override
	public PagedSearch<WorkgroupDTO> pagedSearch(String searchTerm, int page, int pageCount) {
		EntityManager em = profileDAO.getEntityManager();

		String from = " from Workgroup w where lower(w.name) like :st  ";

		String hql = " Select new com.mediigea.vme2.dto.WorkgroupDTO(w.id, w.name, w.description) " + from;
		String hqlCount = " Select count(*)  " + from;

		String st = "%" + searchTerm.toLowerCase() + "%";

		Query q = em.createQuery(hql);
		q.setParameter("st", st);
		if (pageCount > 0 && page > 0) {
			q.setFirstResult((page - 1) * pageCount);
			q.setMaxResults(pageCount);
		}

		Query qCount = em.createQuery(hqlCount);
		qCount.setParameter("st", st);

		@SuppressWarnings("unchecked")
		List<WorkgroupDTO> list = q.getResultList();
		Long count = (Long) qCount.getSingleResult();

		return new PagedSearch<WorkgroupDTO>(list, count.intValue());
	}
}
