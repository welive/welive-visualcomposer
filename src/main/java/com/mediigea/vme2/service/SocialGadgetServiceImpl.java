package com.mediigea.vme2.service;

import it.eng.PropertyGetter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import com.mediigea.vme2.dto.GuiFileResourceDTO;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.exceptions.WriteGadgetException;
import com.mediigea.vme2.opensocial.Content;
import com.mediigea.vme2.opensocial.Locale;
import com.mediigea.vme2.opensocial.Module;
import com.mediigea.vme2.opensocial.ModulePrefs;
import com.mediigea.vme2.opensocial.Require;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.util.io.ZipHelper;
import com.sun.xml.bind.marshaller.CharacterEscapeHandler;
import com.sun.xml.bind.marshaller.DataWriter;

@Service("socialGadgetService")
public class SocialGadgetServiceImpl implements SocialGadgetService {

	private static final Logger logger = LoggerFactory.getLogger(SocialGadgetServiceImpl.class);

	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private GuiEditorService guiEditorService;

	private String extResourcesPath;	

	/**
	 * @return the extResourcePath
	 */
	public String getExtResourcesPath() {
		return extResourcesPath;
	}

	/**
	 * @param extResourcePath
	 *            the extResourcePath to set
	 */
	public void setExtResourcesPath(String extResourcePath) {
		this.extResourcesPath = extResourcePath;
	}

	@Override
	public void writeGadget(Integer id, String serviceInvokeURL) throws WriteGadgetException {

		// construct the complete absolute path of the file
		String destDir = extResourcesPath + File.separator + VmeConstants.GADGET_DIR_PREFIX + id;
		String fullPath = destDir + File.separator + VmeConstants.GADGET_FILENAME;

		File dir = new File(destDir);
		File file = new File(fullPath);

		try {
			String xml = createGadget(id, serviceInvokeURL);
			FileUtils.forceMkdir(dir);
			FileUtils.writeStringToFile(file, xml);

		} catch (Exception e) {
			logger.error("Writing gadget fail" + e.getMessage());
			throw new WriteGadgetException(e);
		}
		
		// IVAN
		// modifico il gadget in modo che possa essere pubblicato sul marketplace
		try
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(Module.class);
	 
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Module module = (Module) jaxbUnmarshaller.unmarshal(file);
			
			// aggiungo la proprieta' dynamic-height in modo che il gadget venga visualizzato full size sul marketplace
			ModulePrefs prefs = module.getModulePrefs();
			Require require = new Require();
			require.setFeature("dynamic-height");
			prefs.addRequire(require);
			module.setModulePrefs(prefs);
			
			// rimuovo il wrapper html
			String content = module.getContents().get(0).getContent()
					.replace("<!DOCTYPE html>", "")
					.replace("<html>", "")
					.replace("</html>", "");
			
			int head_start = content.indexOf("<head>", 0) + 6;
			int head_end = content.indexOf("</head>", 0);
			String head_content = content.substring(head_start, head_end);
			
			int body_start = content.indexOf("<body>", 0);
			content = content.substring(body_start).replace("<body>", head_content).replace("</body>", "");		
			
			content = content.replace("<div class=\"container\">", "<div class=\"container\" onclick=\"javascript:gadgets.window.adjustHeight();\">");
			content += "<script>gadgets.window.adjustHeight();</script>";
			
			Content c = new Content();
			c.setType("html");						
			c.setContent("<![CDATA[" + content + "]]>");
			
			List<Content> list = new ArrayList<Content>();
			list.add(c);
			
			module.setContents(list);
			
			StringWriter sw = new StringWriter();
			Marshaller m = jaxbContext.createMarshaller();

			PrintWriter printWriter = new PrintWriter(sw);
			DataWriter dataWriter = new DataWriter(printWriter, "UTF-8", new JaxbCharacterEscapeHandler());

			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(module, dataWriter);

			String gadgetXml = sw.toString();
			
			//logger.debug("Opensocial Gadget: \n" + gadgetXml);
			
			// scrivo il nuovo xml
			File gadgetFile = new File(destDir + File.separator + "opensocialgadget.xml");
			
			FileUtils.forceMkdir(dir);
			FileUtils.writeStringToFile(gadgetFile, gadgetXml);
		}
		catch (Exception e) 
		{
			logger.error("Writing opensocial gadget fail" + e.getMessage());
			throw new WriteGadgetException(e);
	    }
			
		
	}

	private String createGadget(Integer projectId, String serviceInvokeURL) throws Exception {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Module>ERROR</Module>";

		// Access accessToken = new Access();
		// accessToken.setMethod("GET");
		// accessToken.setUrl("https://www.google.com/accounts/OAuthGetAccessToken");

		// Request requestToken = new Request();
		// requestToken.setMethod("GET");
		// requestToken.setUrl("https://www.google.com/accounts/OAuthGetRequestToken?scope=http://www.google.com/m8/feeds/");

		// Authorization authorization = new Authorization();
		// authorization.setMethod("GET");
		// authorization.setUrl("https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&state=%2Fprofile&redirect_uri=https%3A%2F%2Foauth2-login-demo.appspot.com%2Fcode&response_type=code&client_id=812741506391.apps.googleusercontent.com&approval_prompt=force");

		// Service service = new Service();
		// service.setName("google");
		// service.setAuthorization(authorization);
		// service.setRequestToken(requestToken);
		// service.setAccessToken(accessToken);

		// OAuth oauth = new OAuth();
		// oauth.setService(service);

		// <Locale lang="en" country="us" />
		// Locale localeEn = new Locale();
		// localeEn.setCountry("us");
		// localeEn.setLang("en");

		// <Locale lang="it" country="it" />
		Locale localeIt = new Locale();
		localeIt.setCountry("it");
		localeIt.setLang("it");

		// <Require feature="locked-domain"/>
		// Require require = new Require();
		// require.setFeature("locked-domain");

		Module gadget = new Module();
		ModulePrefs modulePrefs = new ModulePrefs();

		//Project p = projectService.findProject(projectId);

		GuiProject p = guiEditorService.findProject(projectId);
		
		modulePrefs.setTitle(p.getGadgetTitle());
		modulePrefs.setDescription(p.getGadgetDescription());
		modulePrefs.setAuthor(p.getGadgetAuthor());
		modulePrefs.setAuthor_email(p.getGadgetAuthorEmail());
		
		if (p.getGadgetWidth()!=null && p.getGadgetWidth() > 0) {
			modulePrefs.setWidth(p.getGadgetWidth().toString());
		}		
		if (p.getGadgetHeight()!=null && p.getGadgetHeight() > 0) {
			modulePrefs.setHeight(p.getGadgetHeight().toString());
		}
		
		modulePrefs.addLocale(localeIt);

		/*
		 * List<UserPref> userPrefs = new ArrayList<UserPref>();
		 * 
		 * for(ShapeDTO shape: shapeContainer.getInputs().values()){ UserPref
		 * userPref = new UserPref();
		 * 
		 * InputShapeDTO input = (InputShapeDTO) shape;
		 * 
		 * UD_InputShapeDTO ud = input.getUserData();
		 * 
		 * userPref.setDisplay_name(ud.getLabel());
		 * userPref.setDefault_value(ud.getDefaultValue());
		 * userPref.setRequired(UserPref.REQUIRED_TRUE);
		 * 
		 * if(VmeConstants.SHAPE_INPUT_TEXT_TYPE.equals(ud.getInputType())){
		 * 
		 * userPref.setDatatype(UserPref.DATATYPE_STRING_TYPE);
		 * 
		 * } else
		 * if(VmeConstants.SHAPE_INPUT_COMBO_TYPE.equals(ud.getInputType())){
		 * userPref.setDatatype(UserPref.DATATYPE_ENUM_TYPE);
		 * 
		 * for(InputField field : ud.getCombooptions()){
		 * 
		 * userPref.addEnumValue(new EnumValue(field.getValue(),
		 * field.getText())); } }
		 * 
		 * userPrefs.add(userPref); }
		 */

		try {
			//String htmlContent = getHtmlContent(id, serviceInvokeURL, shapeContainer, null);

			//GuiFileResourceDTO htmlFile = this.guiEditorService.getDefaultPublicHtmlFile(projectId);
			
			//String htmlContent = htmlFile.getContent();
			
			// IVAN			
			String path = PropertyGetter.getProperty("app.baseDir") + File.separator + VmeConstants.MAIN_DIRECTORY + File.separator + VmeConstants.GUIEDITOR_DIRECTORY + File.separator;
			path += VmeConstants.GUIPROJECT_REPOSITORY + File.separator + VmeConstants.GUIPROJECT_DIR_PREFIX + projectId + File.separator;
			
			File file = new File(path + p.getSpaName() + ".html");
			
			String htmlContent = FileUtils.readFileToString(file);
			
			Content content = new Content();
			content.setType("html");						
			content.setContent("<![CDATA[" + htmlContent + "]]>");

			gadget.setModulePrefs(modulePrefs);
			// gadget.setUserPref(userPrefs);
			gadget.addContent(content);

			StringWriter sw = new StringWriter();

			JAXBContext context = JAXBContext.newInstance(Module.class);
			Marshaller m = context.createMarshaller();

			PrintWriter printWriter = new PrintWriter(sw);
			DataWriter dataWriter = new DataWriter(printWriter, "UTF-8", new JaxbCharacterEscapeHandler());

			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(gadget, dataWriter);

			xml = sw.toString();
		} catch (Exception gnEx) {

			logger.error("Exception error: " + gnEx.getMessage());
			throw gnEx;
		}

		return xml;
	}		
	
	public class JaxbCharacterEscapeHandler implements CharacterEscapeHandler {

		public void escape(char[] buf, int start, int len, boolean isAttValue, Writer out) throws IOException {

			for (int i = start; i < start + len; i++) {
				char ch = buf[i];
				out.write(ch);
			}
		}
	}

	public InputStream getGadgetArchive(Integer projectId) throws IOException {
		InputStream is = null;

		ZipHelper zipHelper = new ZipHelper();

		String gadgetPath = extResourcesPath + "/" + VmeConstants.GADGET_DIR_PREFIX + projectId;
		String archiveName = VmeConstants.GADGET_ARCHIVE_FILENAME_PREFIX + projectId + ".zip";

		File gadgetDirectory = new File(gadgetPath);

		if (gadgetDirectory.exists()) {

			zipHelper.zipDirectory(gadgetDirectory, extResourcesPath + "/" + archiveName);

			FileSystemResource resource = new FileSystemResource(extResourcesPath + "/" + archiveName);

			is = resource.getInputStream();
		} else {
			throw new IOException("Gadget directory doesn't exist");
		}
		return is;
	}
	
	/*public String getHtmlContent(Integer id, String serviceInvokeURL, ShapeContainer shapeContainer, String invokeResultHtml) throws IOException {
		return  getHtmlContent(id, serviceInvokeURL, shapeContainer, invokeResultHtml, false);
	}
	
	public String getHtmlContentPreview(Integer id, String serviceInvokeURL, ShapeContainer shapeContainer, String invokeResultHtml) throws IOException {
		return  getHtmlContent(id, serviceInvokeURL, shapeContainer, invokeResultHtml, true);
	}

	private String getHtmlContent(Integer id, String serviceInvokeURL, ShapeContainer shapeContainer, String invokeResultHtml, Boolean preview) throws IOException {

		String htmlContent = null;

		try {

			String formContainerDiv = buildInputForm(id, serviceInvokeURL, shapeContainer);

			Map<String, Object> params = new HashMap<String, Object>();
			params.put("serviceInvokeURL", serviceInvokeURL);
			params.put("formContainerDiv", formContainerDiv);

			if (invokeResultHtml != null && !invokeResultHtml.isEmpty()) {
				params.put("invokeResultHtml", invokeResultHtml);
			}

			List<GadgetFileResource> cssResources = gadgetResourceService.getAllByProject(id, GadgetResourceService.ResourceContentType.CSS.geType());
			List<GadgetFileResource> jsResources = gadgetResourceService.getAllByProject(id, GadgetResourceService.ResourceContentType.JAVASCRIPT.geType());

			if (cssResources != null && cssResources.size() > 0) {
				params.put("cssUrl", cssResources);
			}
			if (jsResources != null && jsResources.size() > 0) {
				params.put("jsUrl", jsResources);
			}
			
			String prefix = "";
			if (preview == true){
				prefix = "/extresources/" + VmeConstants.GADGET_DIR_PREFIX + id + "/";
			}
			params.put("prefix", prefix);
			

			URL resource = getClass().getResource("/");
			String path = resource.getPath();
			File template = new File(path + "/gadgetTemplate.html");
			htmlContent = Rythm.render(template, params);

		} catch (IOException gnEx) {

			logger.error("Exception error: " + gnEx.getMessage());
			throw gnEx;
		}

		return htmlContent;
	}*/
/*
	private String buildInputForm(Integer id, String serviceInvokeURL, ShapeContainer shapeContainer) throws IOException {

		String formContainerDiv = null;

		String formName = "form_" + id;
		String invokeUrl = serviceInvokeURL + "/gadget/" + id + "/render";

		HtmlCanvas html = new HtmlCanvas();

		try {
			html.div(id("main_form"));
			html.form(action("").onSubmit("return fetchData('" + formName + "', '" + invokeUrl + "')").id(formName));

			for (ShapeDTO shape : shapeContainer.getInputs().values()) {

				InputShapeDTO input = (InputShapeDTO) shape;
				UD_InputShapeDTO ud = input.getUserData();

				html.label(for_(input.getId())).content(ud.getLabel());
				if (VmeConstants.SHAPE_INPUT_TEXT_TYPE.equals(ud.getInputType())) {

					html.input(id(input.getId()).name(input.getId()).value(ud.getDefaultValue()));

				} else if (VmeConstants.SHAPE_INPUT_COMBO_TYPE.equals(ud.getInputType())) {

					html.select(id(input.getId()).name(input.getId()));

					for (InputField field : ud.getCombooptions()) {
						html.option(value(field.getValue())).content(field.getText());
					}

					html._select();
				}
			}

			html.input(type("submit").value("Invoke"));
			html._form();
			html._div();
			formContainerDiv = html.toHtml();

		} catch (IOException gnEx) {

			logger.error("Exception error: " + gnEx.getMessage());
			throw gnEx;
		}

		return formContainerDiv;
	}*/
}
