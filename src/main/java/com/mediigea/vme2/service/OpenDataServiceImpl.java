package com.mediigea.vme2.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.XmlException;
import org.ckan.Dataset;
import org.ckan.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dao.OpenDataResourceDAO;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.dao.ProjectOperationDAO;
import com.mediigea.vme2.dao.RestOperationDAO;
import com.mediigea.vme2.dao.ServiceCatalogDAO;
import com.mediigea.vme2.dao.SoapOperationDAO;
import com.mediigea.vme2.dao.WorkgroupDAO;
import com.mediigea.vme2.dto.OpenDataResourceDTO;
import com.mediigea.vme2.dto.ServiceItemDTO;
import com.mediigea.vme2.dto.ServiceOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.entity.CkanServer;
import com.mediigea.vme2.entity.OpenDataResource;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.exceptions.FileExceedsSizeLimitException;
import com.mediigea.vme2.exceptions.NotSupportedMimeTypeException;
import com.mediigea.vme2.parser.WadlParser;
import com.mediigea.vme2.parser.WsdlParser;
import com.mediigea.vme2.rest.RestApplication;
import com.mediigea.vme2.rest.RestPackage;
import com.mediigea.vme2.rest.RestResource;
import com.mediigea.vme2.service.GuiEditorServiceImpl.ResourceContentType;
import com.mediigea.vme2.soap.SoapMethod;
import com.mediigea.vme2.soap.SoapPackage;
import com.mediigea.vme2.util.FormatConverter;
import com.mediigea.vme2.util.VmeConstants;

public class OpenDataServiceImpl implements OpenDataService {

	// public static final String SERVICE_BY_PROFILE = "serviceByProfile";
	// public static final String SERVICE_BY_VISIBILITY = "serviceByVisibility";
	// public static final String SERVICE_BY_WORKGROUP = "serviceByWorkgroup";

	private Long opendataResourceMaxSize;

	/**
	 * @return the opendataResourceMaxSize
	 */
	public Long getOpendataResourceMaxSize() {
		return opendataResourceMaxSize;
	}

	/**
	 * @param opendataResourceMaxSize
	 *            the opendataResourceMaxSize to set
	 */
	public void setOpendataResourceMaxSize(Long opendataResourceMaxSize) {
		this.opendataResourceMaxSize = opendataResourceMaxSize;
	}

	public enum OpenDataType {
		CSV("text/csv"), XML_APPLICATION("application/xml"), XML_TEXT(
				"text/xml"), JSON("application/json"), CSV_EXCEL("application/vnd.ms-excel");

		private final String type;

		// Reverse-lookup map for getting a day from an abbreviation
		private static final List<OpenDataType> lookup = new ArrayList<OpenDataType>();
		static {
			for (OpenDataType d : OpenDataType.values())
				lookup.add(d);
		}

		OpenDataType(String type) {
			this.type = type;
		}

		public String getType() {
			return this.type;
		}

		public static OpenDataType get(String type) {
			OpenDataType result = null;

			for (OpenDataType tmpType : lookup) {
				if (type.matches(tmpType.type)) {
					result = tmpType;
					break;
				}
			}

			return result;
		}
	};

	private static final Logger logger = LoggerFactory
			.getLogger(OpenDataServiceImpl.class);

	@Autowired
	private OpenDataResourceDAO openDataDAO;

	@Autowired
	private RestOperationDAO restDAO;

	@Autowired
	private WorkgroupDAO wokgroupDAO;

	@Autowired
	private ProfileDAO profileDAO;

	@Autowired
	private SoapOperationDAO soapOperationDAO;

	@Autowired
	private RestOperationDAO restOperationDAO;

	@Autowired
	private ProjectOperationDAO projectOperationDAO;

	@Autowired
	private RestOperationService restOperationService;

	@Autowired
	private SoapOperationService soapOperationService;

	@Autowired
	private ObjectMapperExt objectMapper;

	@Override
	public OpenDataResource setVisibility(Integer odataId, Integer code) {

		if (odataId == null || code == null)
			return null;

		OpenDataResource s = openDataDAO.getById(odataId);

		if (s == null)
			return null;

		s.setVisibility(code);

		openDataDAO.update(s);

		return s;
	}

	// @Override
	// public List<ServiceCatalog> findAll(String hql, HashMap<String, Object>
	// parameters) {
	//
	// if (hql == null || hql.isEmpty())
	// return serviceCatalogDAO.getAll();
	//
	// return serviceCatalogDAO.getAllBy(hql, parameters);
	// }
	//
	// @Override
	// public ServiceCatalog findService(int id) {
	//
	// ServiceCatalog scItem = serviceCatalogDAO.getById(id);
	// return scItem;
	// }

	private static String csvToJson(String csvContent, char separator)
			throws JsonProcessingException, IOException {
		CsvSchema bootstrap = CsvSchema.emptySchema().withColumnSeparator(separator).withHeader();

		CsvMapper csvMapper = new CsvMapper();
		MappingIterator<Map<?, ?>> mappingIterator = csvMapper
				.reader(Map.class).with(bootstrap).readValues(csvContent);

		List<Map<?, ?>> it = mappingIterator.readAll();

		ObjectMapper mapper = new ObjectMapper();

		OutputStream os = new ByteArrayOutputStream();

		mapper.writeValue(os, it);

		return os.toString();
	}
	
	// IVAN
	private String contentTypreFromExt(String filename)
	{
		logger.debug("contentTypreFromExt conversion");
		
		String fields[] = filename.split("\\.");
		
		String ext = fields[fields.length - 1];
		
		String contentType = "";
		
		switch(ext)
		{
			case "csv":
				contentType = "text/csv";
				break;
			case "xml":
			case "rss":
			case "rdf":
			case "kml":
			case "gml":
				contentType = "text/xml";
				break;
			case "json":
				contentType = "application/json";
				break;
			default:
				break;
		}
		
		return contentType;
	}

	@Override
	public OpenDataResource addFromFile(String sourceName, MultipartFile file,
			Profile profile, char separator) throws IOException, NotSupportedMimeTypeException,
			FileExceedsSizeLimitException {

		OpenDataResource out = null;

		if (file.getSize() > opendataResourceMaxSize) {
			throw new FileExceedsSizeLimitException(
					"Selected Opendata resource is too big to be imported");

		} else {
			String content = IOUtils.toString(file.getInputStream(), "UTF-8");

			String contentType = file.getContentType();
			
			// IVAN
			if(contentType.compareTo("text/plain") == 0 || contentType.compareTo("application/octet-stream") == 0)
			{
				
				String newContentType = contentTypreFromExt(file.getOriginalFilename());
				
				contentType = (newContentType.compareTo("") != 0) ? newContentType : contentType;				
			}

			out = add(sourceName, content, contentType, profile, null, separator);
		}

		return out;
	}
	
	@Override
	public OpenDataResource addFromFile(String sourceName, String file, String filename, String type,
			long size, Profile profile, char separator) throws IOException, NotSupportedMimeTypeException,
			FileExceedsSizeLimitException {

		OpenDataResource out = null;

		if (size > opendataResourceMaxSize) 
		{
			throw new FileExceedsSizeLimitException(
					"Selected Opendata resource is too big to be imported");

		} 
		else 
		{
			String content = file;

			String contentType = type;
			
			// IVAN
			if(contentType.compareTo("text/plain") == 0 || contentType.compareTo("application/octet-stream") == 0)
			{
				
				String newContentType = contentTypreFromExt(filename);
				
				contentType = (newContentType.compareTo("") != 0) ? newContentType : contentType;				
			}

			out = add(sourceName, content, contentType, profile, null, separator);
		}

		return out;
	}

	@Override
	public OpenDataResource addFromUrl(String sourceName, String resourceUrl,
			Profile profile, char separator) throws IOException, NotSupportedMimeTypeException, FileExceedsSizeLimitException {
		return addFromUrl(sourceName, resourceUrl, profile, null, separator);
	}

	@Override
	public OpenDataResource addFromCkan(CkanServer ckanServer, Dataset dataset,
			Resource resource, Profile profile, char separator) throws IOException,
			NotSupportedMimeTypeException, FileExceedsSizeLimitException {

		String sourceName = resource.getName() != null
				&& !resource.getName().isEmpty() ? resource.getName(): dataset.getTitle();

		String resourceUrl = resource.getUrl();

		return addFromUrl(sourceName, resourceUrl, profile, ckanServer, separator);
	}

	private OpenDataResource addFromUrl(String sourceName, String resourceUrl,
			Profile profile, CkanServer ckanServer, char separator) throws IOException,
			NotSupportedMimeTypeException, FileExceedsSizeLimitException {

		OpenDataResource oDataResource = null;

		URLConnection connection = new URL(resourceUrl).openConnection();	
		
		long fileLenth = connection.getContentLength();
								
		if (fileLenth > opendataResourceMaxSize) {
			
			throw new FileExceedsSizeLimitException(
					"Selected Opendata resource is too big to be imported");
		} else {
			
			String contentType = connection.getContentType();

			if (contentType.indexOf(";") != -1) {
				contentType = contentType
						.substring(0, contentType.indexOf(";"));
			}
			
			// IVAN
			if(contentType.compareTo("text/plain") == 0 || contentType.compareTo("application/octet-stream") == 0)
			{
				String fields[] = resourceUrl.split("/");
				
				String newContentType = contentTypreFromExt(fields[fields.length - 1]);
				
				contentType = (newContentType.compareTo("") != 0)?newContentType:contentType;				
			}
			
			String content = IOUtils.toString(connection.getInputStream(), "UTF-8");					
			
			oDataResource = add(sourceName, content, contentType, profile,
					ckanServer, separator);			
		}

		return oDataResource;
	}

	private OpenDataResource add(String sourceName, String content,
			String contentType, Profile profile, CkanServer ckanServer, char separator)
			throws IOException, NotSupportedMimeTypeException {
		OpenDataResource oDataResource = null;
		
		logger.debug("Add Opendata - MimeType: " + contentType);

		OpenDataType cType = OpenDataType.get(contentType);

		if (cType != null) {

			switch (cType) {
			
			case CSV_EXCEL:
			case CSV:
				content = csvToJson(content, separator);
				break;

			case XML_APPLICATION:
			case XML_TEXT:
				content = FormatConverter.xmlToJson(content);
				break;

			case JSON:
				break;
			}

			oDataResource = new OpenDataResource();
			oDataResource.setCreatedAt(new Date());
			oDataResource.setUpdatedAt(new Date());
			oDataResource.setName(sourceName);
			oDataResource.setProfile(profile);
			oDataResource.setContent(content);
			oDataResource.setType(contentType);
			oDataResource.setCkanServer(ckanServer);

			oDataResource
					.setVisibility(profile.getAccount().getRole() == VmeConstants.ACCOUNT_ROLE_ADMIN ? VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC
							: VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE);

			openDataDAO.create(oDataResource);
		} else {
			throw new NotSupportedMimeTypeException(
					"Opendata mime type not supported: " + contentType);
		}

		return oDataResource;
	}

	@Override
	public List<OpenDataResourceDTO> getOpenDataResourcesDTO(Profile p) {

		List<OpenDataResource> myList = this.findVisibleResources(p.getId());
		List<OpenDataResource> publicList = this.findPublicResources();

		if (myList == null && publicList == null)
			return null;

		ArrayList<OpenDataResourceDTO> catalogResult = new ArrayList<OpenDataResourceDTO>();

		for (OpenDataResource odata : myList) {

			OpenDataResourceDTO odataItem = new OpenDataResourceDTO();

			odataItem.setId(odata.getId());
			odataItem.setName(odata.getName());
			odataItem.setType(odata.getType());

			catalogResult.add(odataItem);
		}
		for (OpenDataResource odata : publicList) {

			OpenDataResourceDTO odataItem = new OpenDataResourceDTO();

			odataItem.setId(odata.getId());
			odataItem.setName(odata.getName());
			odataItem.setType(odata.getType());
			catalogResult.add(odataItem);
		}
		return catalogResult;

	}

	@Override
	public List<OpenDataResource> findAdministratorsResources() {
		String hql = " from OpenDataResource s join fetch s.profile p where p.account.role = "
				+ VmeConstants.ACCOUNT_ROLE_ADMIN + " order by s.name ";
		return openDataDAO.getAllBy(hql, null);
	}

	@Override
	public List<OpenDataResource> findPublicResources() {
		String hql = " from OpenDataResource s where s.visibility="
				+ VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC
				+ " order by s.name ";
		return openDataDAO.getAllBy(hql, null);
	}

	@Override
	public List<OpenDataResource> findVisibleResources(Integer profileId) {
		String hql = " Select distinct s from OpenDataResource s join fetch s.profile p join p.profileWorkgroups pw"
				+ " where pw.pk.workgroup.id in (select pw2.pk.workgroup.id from ProfileWorkgroup pw2 where pw2.pk.profile.id = :pId)"
				+ " and ( s.profile.id = :pId or s.visibility ="
				+ VmeConstants.SERVICECATALOG_VISIBILITY_WORKGROUP
				+ " )  order by s.visibility desc, s.name ";

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("pId", profileId);
		return openDataDAO.getAllBy(hql, params);
	}

	@Override
	public void delete(Integer id) throws Exception {

		try {
			OpenDataResource s = openDataDAO.getById(id);

			openDataDAO.delete(s);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public OpenDataResource getById(Integer id) {

		return openDataDAO.getById(id);
	}
}
