package com.mediigea.vme2.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.UnreadComment;

@Transactional
public interface CommentService {

	public Comment addComment(Comment comment);
	public Comment updateComment(Comment comment);
	public Comment deleteComment(Integer id);
	public void deleteAllByProject(int idProject);
	public void deleteAllByGuiProject(int idProject);
	
	public Comment findComment(Integer id);
	
	public List<Comment> findAllByProject(Integer id);
	public List<Comment> findAll(String hql, HashMap<String, Object> parameters);
	
	public void addUnreadComment(Comment comment);
	public void setReadedComment(Project project, Profile profile);
	public void setReadedComment(Comment comment);
	
	public List<UnreadComment> getUnreadCommentsByCommentId(int idComment);
	public List<UnreadComment> getUnreadCommentsByProjectIdProfileId(int idProject, int idProfile);
	public List<UnreadComment> getUnreadCommentsByProfileId(int idProfile);
	public void setReadedComment(GuiProject project, Profile profile);
	public List<UnreadComment> getUnreadCommentsByGuiProjectIdProfileId(int idProject,	int idProfile);
	
}
