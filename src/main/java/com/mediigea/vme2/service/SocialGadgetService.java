package com.mediigea.vme2.service;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.exceptions.WriteGadgetException;

/**
 * Interface to define management operations about social gadgets
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface SocialGadgetService {

	public void writeGadget(Integer id, String serviceInvokeURL) throws WriteGadgetException;
 
	public InputStream getGadgetArchive(Integer projectId) throws IOException;	
}
