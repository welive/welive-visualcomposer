package com.mediigea.vme2.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.entity.Organization;
import com.mediigea.vme2.entity.Profile;

@Transactional
public interface OrganizationService {
	
	public Organization addOrganization(Organization o);
	public Organization updateOrganization(Organization o);
	public void deleteOrganization(Integer id);
	
	public List<Organization> findAll();
	public Organization findOrganizationById(Integer id);
	public Organization findOrganizationByExtId(Integer extId);
	
	public List<Profile> findOrganizationMembersById(Integer id);
	public List<Profile> findOrganizationMembersByExtId(Integer extId);
}
