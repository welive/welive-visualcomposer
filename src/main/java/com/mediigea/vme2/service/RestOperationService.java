package com.mediigea.vme2.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mediigea.vme2.dto.OperationTemplateDTO;
import com.mediigea.vme2.dto.RestOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.RestOperationTemplate;

/**
 * Interface to define management operations about operations of Rest services
 * @author Carmine Ruffino
 *
 */
@Transactional
public interface RestOperationService {

	public RestOperation addRestResource(RestOperation a);
	
	public RestOperation updateRestResource(RestOperation a);
	
	public RestOperation deleteRestResource(Integer restResourceId);
	
	public List<RestOperation> findAll(String hql, HashMap<String, Object> parameters);
	
	public List<RestOperation> findAllByService(Integer id);
	
	public RestOperation findRestResource(Integer id);
	
	public RestOperation findRestOperationById(Integer id);
	
	public RestOperationDTO findRestOperationByIdAsDTO(Integer id);
	
	public void addRestOperationTemplate(OperationTemplateDTO operationTemplate) throws JsonProcessingException;
	
//	public void updateRestOperationTemplate(RestOperationTemplate operationTemplate) throws JsonProcessingException;
	
	public void deleteRestOperationTemplate(Integer templateId);
	
	public RestOperation findOperationByTemplate(Integer templateId);
	
	public RestOperationTemplate findTemplateById(Integer templateId);
	
	public List<RestOperationTemplate> findTemplateByOperation(Integer operationId);	
	
	public List<ServiceParameterDTO> getRestOperationParamsDTO (int operationId);
	
	public List<ServiceParameterDTO> getRestTemplateParamsDTO (int operationId) throws Exception;

	public OperationTemplateDTO findTemplateByIdDTO(Integer templateId) throws Exception;
}
