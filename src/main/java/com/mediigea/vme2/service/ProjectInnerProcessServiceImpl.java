package com.mediigea.vme2.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.ProjectDAO;
import com.mediigea.vme2.dao.ProjectInnerProcessDAO;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectInnerProcess;
import com.mediigea.vme2.entity.ProjectInnerProcessId;

public class ProjectInnerProcessServiceImpl implements ProjectInnerProcessService {

	@Autowired
	private ProjectInnerProcessDAO projectInnerProcessDAO;		
	
	@Autowired
	private ProjectDAO projectDAO;
	
	@Override
	public void create(int idProject, int idInnerProcess) {
						
		ProjectInnerProcess pinner = new ProjectInnerProcess();
	
		Project innerProcess = projectDAO.getById(idInnerProcess);
		Project project = projectDAO.getById(idProject);
		
		pinner.setInnerProcess(innerProcess);
		pinner.setProject(project);		
				
		EntityManager em = projectInnerProcessDAO.getEntityManager();
		Session session = em.unwrap(Session.class);		
		
		ProjectInnerProcess ob = (ProjectInnerProcess) session.get(ProjectInnerProcess.class, pinner.getPk());
		
		if(ob==null){
			projectInnerProcessDAO.create(pinner);
		}
	}
	
	public void deleteAllByProject(int idProject){
				
		projectInnerProcessDAO.deleteAllByProject(idProject);
	}
	
	public List<ProjectInnerProcess> getAllByProject(int idProject){
		return projectInnerProcessDAO.getAllByProject(idProject);
	}

//	public List<ProjectInnerProcess> getAllByInnerProcess(int idInnerProcess){
//		return projectInnerProcessDAO.getAllByInnerProcess(idInnerProcess);
//	}
}
