package com.mediigea.vme2.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mediigea.vme2.entity.IdeaWorkgroup;

public class IdeaWorkgroupDAO extends AbstractDAO<IdeaWorkgroup> 
{
	private static final Logger logger = LoggerFactory.getLogger(AbstractDAO.class);

	public IdeaWorkgroupDAO() 
	{
		super(IdeaWorkgroup.class);		
	}
}
