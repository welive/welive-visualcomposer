package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import com.mediigea.vme2.entity.GuiProject;

public class GuiProjectDAO extends AbstractDAO<GuiProject> {

	public GuiProjectDAO() {
		super(GuiProject.class);
	}
	
	public List<GuiProject> searchProject(int idProfile, String criteria){
		String hql = " from GuiProject p "
				+ "join fetch p.workgroup "
				+ "join fetch p.profileGuiProjects pp "
				+ "join fetch p.project mproj " 
				+ "where pp.pk.profile.id=:profileId "
				+ "and ("
				+ "lower(p.name) like lower(:criteria) or "
				+ "lower(p.description) like lower(:criteria) or "
				+ "lower(p.tags) like lower(:criteria) or "
				+ "lower(mproj.name) like lower(:criteria)"
				+ ")";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("profileId", idProfile);
		parameters.put("criteria", "%"+criteria+"%");

		return this.getAllBy(hql, parameters);				
	}
}
