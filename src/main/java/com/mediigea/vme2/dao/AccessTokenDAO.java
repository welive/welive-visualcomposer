package com.mediigea.vme2.dao;

import javax.persistence.NoResultException;

import com.mediigea.vme2.entity.AccessToken;

public class AccessTokenDAO extends AbstractDAO<AccessToken> {

	public AccessTokenDAO() {
		super(AccessToken.class);
	}
	
	// IVAN: modified to allow auth at operation level
	public AccessToken getByKey(String userConnectorId, Integer idProvider, Integer operationId){
		String hql = "from AccessToken as accesstoken where accesstoken.userConnectorId=:userconnectorid AND accesstoken.provider.id=:providerid AND accesstoken.operation.id=:operationid";
		try
		{	
			return (AccessToken) getEntityManager().createQuery(hql).setParameter("userconnectorid", userConnectorId).setParameter("providerid", idProvider).setParameter("operationid", operationId).getSingleResult();
		}
		catch(NoResultException e)
		{
			return null;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
			return null;
		}
	}
}

