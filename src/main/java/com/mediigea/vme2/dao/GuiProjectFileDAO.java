package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import com.mediigea.vme2.entity.GuiProjectFile;
import com.mediigea.vme2.service.GuiEditorServiceImpl.ResourceContentType;

public class GuiProjectFileDAO extends AbstractDAO<GuiProjectFile> {

	public GuiProjectFileDAO() {
		super(GuiProjectFile.class);
	}
	
	public List<GuiProjectFile> getAllByProject(int idProject){
		return getAllByProject(idProject, null);				
	}
		
	
	public List<GuiProjectFile> getAllByProject(int idProject, ResourceContentType type){
		String hql = "from GuiProjectFile where project_id=:ProjectId";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ProjectId", idProject);
		
		if(type!=null){
			if(type.getType().compareTo("image") == 0)
			{
				hql += " AND contentType LIKE '" + type.getType() + "%'";
			}
			else
			{
				hql += " AND contentType=:Type";
				parameters.put("Type", type.getType());
			}
		}
						
		return this.getAllBy(hql, parameters);				
	}
	
	public GuiProjectFile getDefaultByProject(int idProject){
		String hql = "from GuiProjectFile where project_id=:ProjectId and isDefault='true'";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ProjectId", idProject);		
						
		return this.getAllBy(hql, parameters).get(0);				
	}	
}
