package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import com.mediigea.vme2.entity.ProjectOperation;

public class ProjectOperationDAO extends AbstractDAO<ProjectOperation> {

	public ProjectOperationDAO() {
		super(ProjectOperation.class);
	}
	
	public void deleteAllByProject(int idProject){
		getEntityManager().createQuery("delete from ProjectOperation where project_id = " + idProject).executeUpdate();
	}
	
	public List<ProjectOperation> getAllByProject(int idProject){
		String hql = "from ProjectOperation where project_id=:ProjectId";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ProjectId", idProject);
		return this.getAllBy(hql, parameters);				
	}
	
	public List<ProjectOperation> getAllBySoapOperation(int soapOperationId){
		String hql = "from ProjectOperation where soapoperation_id=:soapoperid";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("soapoperid", soapOperationId);
		return this.getAllBy(hql, parameters);				
	}
	
	public List<ProjectOperation> getAllByRestOperation(int restOperationId){
		String hql = "from ProjectOperation where restoperation_id=:restoperid";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("restoperid", restOperationId);
		return this.getAllBy(hql, parameters);				
	}
}

