package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.Profile;

public class ProfileDAO extends AbstractDAO<Profile> {

	public ProfileDAO() {
		super(Profile.class);
	}
	
}

