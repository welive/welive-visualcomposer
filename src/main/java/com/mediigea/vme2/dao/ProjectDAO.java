package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import com.mediigea.vme2.entity.Project;

public class ProjectDAO extends AbstractDAO<Project> {

	public ProjectDAO() {
		super(Project.class);
	}
	
	public List<Project> searchProject(int idProfile, String criteria){
		String hql = " from Project p join fetch p.workgroup join fetch p.profileProjects pp " 
				+ " where pp.pk.profile.id=:profileId "
				+ " and (lower(p.name) like lower(:criteria) or lower(p.description) like lower(:criteria) or lower(p.tags) like lower(:criteria))";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("profileId", idProfile);
		parameters.put("criteria", "%"+criteria+"%");

		return this.getAllBy(hql, parameters);				
	}
	
}
