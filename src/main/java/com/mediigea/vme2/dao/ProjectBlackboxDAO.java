package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import com.mediigea.vme2.entity.ProjectBlackbox;

public class ProjectBlackboxDAO extends AbstractDAO<ProjectBlackbox> {

	public ProjectBlackboxDAO() {
		super(ProjectBlackbox.class);
	}
	
	public void deleteAllByProject(int idProject){
		getEntityManager().createQuery("delete from ProjectBlackbox where project_id = " + idProject).executeUpdate();
	}
	
	public List<ProjectBlackbox> getAllByProject(int idProject){
		String hql = "from ProjectBlackbox where project_id=:ProjectId";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ProjectId", idProject);
		return this.getAllBy(hql, parameters);				
	}
	
	public List<ProjectBlackbox> getAllByBlackbox(int blackbox_id){
		String hql = "from ProjectBlackbox where blackbox_id=:blackbox_id";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("blackbox_id", blackbox_id);
		return this.getAllBy(hql, parameters);				
	}
		
}

