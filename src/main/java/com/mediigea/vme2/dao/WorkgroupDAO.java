package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.Workgroup;

public class WorkgroupDAO extends AbstractDAO<Workgroup> {

	public WorkgroupDAO() {
		super(Workgroup.class);
	}
}
