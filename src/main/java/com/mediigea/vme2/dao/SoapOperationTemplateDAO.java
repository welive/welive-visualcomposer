package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.SoapOperationTemplate;

public class SoapOperationTemplateDAO extends AbstractDAO<SoapOperationTemplate> {

	public SoapOperationTemplateDAO() {
		super(SoapOperationTemplate.class);
	}	
}
