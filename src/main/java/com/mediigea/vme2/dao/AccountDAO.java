package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Profile;

public class AccountDAO extends AbstractDAO<Account> {

	private static final Logger logger = LoggerFactory.getLogger(AbstractDAO.class);

	public AccountDAO() {

		super(Account.class);
		
	}

	@SuppressWarnings("unchecked")
	public List<Account> getAllByProfile(Profile p) {

		logger.info("Get all Account by profile id");

		if (p == null)
			return null;

		List<Account> accountList = getEntityManager().createQuery("from Account as account where account.profile.id = " + p.getId()).getResultList();

		if (accountList != null && !accountList.isEmpty())
			logger.debug("Found Account: " + accountList.size());

		return accountList;
	}

	/**
	 * 
	 * @param username
	 * @return
	 */	
	public Account getByUserName(String email) {
		logger.info("getByUserName");
		
		Account result = null;		
		
		if (email == null || "".equals(email))
			return null;	
		
		
		String hql = "from Account as account where account.email=:email";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("email", email);
		List<Account> accountList = getAllBy(hql, parameters);
		

		if (accountList != null && accountList.size() == 1){
			logger.debug("Found Account: " + accountList.size());
			result = accountList.get(0);
		}		
		

		return result;

	}
}
