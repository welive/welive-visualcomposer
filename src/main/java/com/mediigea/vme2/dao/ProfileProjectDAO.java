package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.ProfileProject;

public class ProfileProjectDAO extends AbstractDAO<ProfileProject> {
	
	public ProfileProjectDAO() {
		super(ProfileProject.class);
	}	
}
