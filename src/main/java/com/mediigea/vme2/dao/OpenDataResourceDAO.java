package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.OpenDataResource;

public class OpenDataResourceDAO extends AbstractDAO<OpenDataResource> {

	public OpenDataResourceDAO() {
		super(OpenDataResource.class);
	}
}

