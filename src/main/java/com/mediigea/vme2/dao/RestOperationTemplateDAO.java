package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.RestOperationTemplate;

public class RestOperationTemplateDAO extends AbstractDAO<RestOperationTemplate> {

	public RestOperationTemplateDAO() {
		super(RestOperationTemplate.class);
	}	
}
