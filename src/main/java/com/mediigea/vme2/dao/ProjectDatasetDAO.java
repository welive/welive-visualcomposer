package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import com.mediigea.vme2.entity.ProjectDataset;

public class ProjectDatasetDAO extends AbstractDAO<ProjectDataset> {

	public ProjectDatasetDAO() {
		super(ProjectDataset.class);
	}
	
	public void deleteAllByProject(int idProject){
		getEntityManager().createQuery("delete from ProjectDataset where project_id = " + idProject).executeUpdate();
	}
	
	public List<ProjectDataset> getAllByProject(int idProject){
		String hql = "from ProjectDataset where project_id=:ProjectId";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ProjectId", idProject);
		return this.getAllBy(hql, parameters);				
	}
}
