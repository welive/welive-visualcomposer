package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.Comment;

public class CommentDAO extends AbstractDAO<Comment> {

	public CommentDAO() {
		super(Comment.class);
	}
	
	public void deleteAllByProject(int projectId){
		getEntityManager().createQuery("delete from Comment where project_id = " + projectId).executeUpdate();
	}
	
	public void deleteAllByGuiProject(int projectId){
		getEntityManager().createQuery("delete from Comment where gui_project_id = " + projectId).executeUpdate();
	}

}