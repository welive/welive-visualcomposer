package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.ODSResource;

public class ODSResourceDAO extends AbstractDAO<ODSResource> {

	public ODSResourceDAO() {
		super(ODSResource.class);
	}
}
