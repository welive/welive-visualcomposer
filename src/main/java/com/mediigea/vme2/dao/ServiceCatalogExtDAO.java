package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.ServiceCatalogExt;

public class ServiceCatalogExtDAO extends AbstractDAO<ServiceCatalogExt>
{
	public ServiceCatalogExtDAO() 
	{
		super(ServiceCatalogExt.class);
	}
}
