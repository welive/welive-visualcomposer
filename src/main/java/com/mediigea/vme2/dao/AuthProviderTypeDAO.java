package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.AuthProviderType;

public class AuthProviderTypeDAO extends AbstractDAO<AuthProviderType> {

	public AuthProviderTypeDAO() {
		super(AuthProviderType.class);
	}
}

