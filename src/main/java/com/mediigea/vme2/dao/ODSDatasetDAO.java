package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.ODSDataset;

public class ODSDatasetDAO extends AbstractDAO<ODSDataset> {

	public ODSDatasetDAO() {
		super(ODSDataset.class);
	}
}
