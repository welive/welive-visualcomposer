package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.ServiceCatalog;

public class ServiceCatalogDAO extends AbstractDAO<ServiceCatalog> {

	public ServiceCatalogDAO() {
		super(ServiceCatalog.class);
	}
}

