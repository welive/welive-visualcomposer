package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import com.mediigea.vme2.entity.ProjectBlackbox;
import com.mediigea.vme2.entity.ProjectInnerProcess;

public class ProjectInnerProcessDAO extends AbstractDAO<ProjectInnerProcess> {

	public ProjectInnerProcessDAO() {
		super(ProjectInnerProcess.class);
	}
	
	public void deleteAllByProject(int idProject){
		getEntityManager().createQuery("delete from ProjectInnerProcess where project_id = " + idProject).executeUpdate();
	}	
	
	public List<ProjectInnerProcess> getAllByProject(int idProject){
		String hql = "from ProjectInnerProcess pi join fetch pi.pk.project p where p.id=:ProjectId";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ProjectId", idProject);
		return this.getAllBy(hql, parameters);				
	}
	
	public List<ProjectInnerProcess> getAllByInnerProcess(int innerprocess_id){
		String hql = "from ProjectInnerProcess pi join fetch pi.pk.innerProcess i where i.id=:innerprocess_id";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("innerprocess_id", innerprocess_id);
		return this.getAllBy(hql, parameters);				
	}
		
}

