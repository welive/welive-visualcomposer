package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.GuiProjectProject;


public class GuiProjectProjectDAO extends AbstractDAO<GuiProjectProject> {
	
	public GuiProjectProjectDAO() {
		super(GuiProjectProject.class);
	}	
}