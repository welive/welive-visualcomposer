package com.mediigea.vme2.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProfileWorkgroup;
import com.mediigea.vme2.entity.ProfileWorkgroupId;

public class ProfileWorkgroupDAO extends AbstractDAO<ProfileWorkgroup> {

	private static final Logger logger = LoggerFactory.getLogger(ProfileWorkgroupDAO.class);
	
	public ProfileWorkgroupDAO() {
		super(ProfileWorkgroup.class);
	}
	
	public ProfileWorkgroup getById(final ProfileWorkgroupId id) {
		Preconditions.checkArgument(id != null);
		return getEntityManager().find(ProfileWorkgroup.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<ProfileWorkgroup> getAllByProfile(Profile p) {
		
		logger.info("Get all ProfileWorkgroup by profile id");
		
		List<ProfileWorkgroup> profileWorkgroupList = getEntityManager().createQuery("from ProfileWorkgroup as profileWorkgroup where profileWorkgroup.profile.id = " + p.getId()).getResultList();
		
		if(profileWorkgroupList!=null && !profileWorkgroupList.isEmpty())
			logger.debug("Found ProfileWorkgroup: " + profileWorkgroupList.size());
		
		return profileWorkgroupList;
	}
	
	
	
}
