package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import com.mediigea.vme2.entity.Activity;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.UnreadComment;

public class ActivityDAO extends AbstractDAO<Activity> {

	public ActivityDAO() {
		super(Activity.class);
	}

	public List <com.mediigea.vme2.entity.Activity> getByProfileIdOrdered(int idProfile){
		String hql = "from Activity where profile_id=:ProfileId order by createdAt desc";
	
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ProfileId", idProfile);	
						
		return this.getAllBy(hql, parameters);				
	}	
	
	@SuppressWarnings("unchecked")
	public List<com.mediigea.vme2.entity.Activity> getAllOrdered() {
	
		List<com.mediigea.vme2.entity.Activity> list = getEntityManager()
				.createQuery("from Activity order by createdAt desc")
				.getResultList();
		return list;
	}
	
}