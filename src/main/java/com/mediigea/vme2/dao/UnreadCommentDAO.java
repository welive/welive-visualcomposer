package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import com.mediigea.vme2.entity.UnreadComment;

public class UnreadCommentDAO extends AbstractDAO<UnreadComment> {

	public UnreadCommentDAO() {
		super(UnreadComment.class);
	}
	
	
	public List<UnreadComment> getUnreadCommentsByCommentId(int idComment){
		String hql = "from UnreadComment where comment_id=:CommentId";
	
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("CommentId", idComment);		
						
		return this.getAllBy(hql, parameters);				
	}

	
	public List<UnreadComment> getUnreadCommentsByProjectId(int idProject){
		String hql = "from UnreadComment u join fetch u.pk.comment c where c.project.id=:ProjectId";
	
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ProjectId", idProject);		
						
		return this.getAllBy(hql, parameters);				
	}
	
	public List<UnreadComment> getUnreadCommentsByProjectIdProfileId(int idProject, int idProfile){
		String hql = "from UnreadComment u join fetch u.pk.comment c where c.project.id=:ProjectId and u.pk.profile.id=:ProfileId";
	
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ProjectId", idProject);		
		parameters.put("ProfileId", idProfile);	
						
		return this.getAllBy(hql, parameters);				
	}	
	
	public List<UnreadComment> getUnreadCommentsByGuiProjectIdProfileId(int idGuiProject, int idProfile){
		String hql = "from UnreadComment u join fetch u.pk.comment c where c.guiProject.id=:GuiProjectId and u.pk.profile.id=:ProfileId";
	
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("GuiProjectId", idGuiProject);		
		parameters.put("ProfileId", idProfile);	
						
		return this.getAllBy(hql, parameters);				
	}		
	
	public List<UnreadComment> getUnreadCommentsByProfileId(int idProfile){
		String hql = "from UnreadComment where profile_id=:ProfileId";
	
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ProfileId", idProfile);	
						
		return this.getAllBy(hql, parameters);				
	}
	

	public void deleteByCommentId(int idCommento){
		String hql = "DELETE from UnreadComment where comment_id=:CommentId";
        getEntityManager().createQuery(hql).setParameter("CommentId", idCommento).executeUpdate();
	}
	
	
}

