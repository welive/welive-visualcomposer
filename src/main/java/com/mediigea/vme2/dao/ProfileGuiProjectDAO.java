package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.ProfileGuiProject;

public class ProfileGuiProjectDAO extends AbstractDAO<ProfileGuiProject> {
	
	public ProfileGuiProjectDAO() {
		super(ProfileGuiProject.class);
	}	
}
