package com.mediigea.vme2.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.google.common.base.Preconditions;
import com.mediigea.vme2.entity.Activity;

@Repository
public abstract class AbstractDAO<T extends Serializable> {

	private Class<T> clazz;

	//@PersistenceContext(unitName="entityManagerFactoryPU")
	@PersistenceContext
	private EntityManager entityManager;

	public AbstractDAO(final Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	public T getById(final Integer id) {
		Preconditions.checkArgument(id != null);
		return getEntityManager().find(clazz, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		return getEntityManager().createQuery("from " + clazz.getName()).getResultList();
	}
	
	public List<T> getAllBy(String hql, HashMap<String, Object> parameters) {
		return getAllBy(hql, parameters, 0, 0);		
	}

	@SuppressWarnings("unchecked")
	public List<T> getAllBy(String hql, HashMap<String, Object> parameters, int limit, int offset) {
		
		if(hql == null || hql.isEmpty())
			return null;
		
		Query q = getEntityManager().createQuery(hql);
		if (limit > 0){
			q.setMaxResults(limit);
		}
		if (offset > 0){
			q.setFirstResult(offset);
		}
		
		if(parameters != null && !parameters.isEmpty()){
			
			Object[] keys = parameters.keySet().toArray();
			int size = keys.length;
			
			for(int i=0; i<size; i++)
				q.setParameter(keys[i].toString(), parameters.get(keys[i]));
		}
		
		return q.getResultList();
	}
	
	public void create(final T entity) {
		Preconditions.checkNotNull(entity);
		getEntityManager().persist(entity);
	}

	public T update(final T entity) {
		Preconditions.checkNotNull(entity);
		return (T) getEntityManager().merge(entity);
	}

	public void delete(final T entity) {
		Preconditions.checkNotNull(entity);
		getEntityManager().remove(entity);
	}

	public void deleteById(final Integer entityId) {
		final T entity = getById(entityId);
		Preconditions.checkState(entity != null);
		delete(entity);
	}

	/**
	 * @return the entityManager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * @param entityManager
	 *            the entityManager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	
	
	 /*
	  * ***********************************************************
	  * ACTIVITY STREAM AREA 
	  */
	
	 public static enum ActivityType {
		    REGISTER_USER("REGISTER_USER"),
		    ENABLE_USER("ENABLE_USER"),
		    DISABLE_USER("DISABLE_USER"),		    
		    USER_LOGIN("USER_LOGIN"),
		    USER_UPDATE_PASSWORD("USER_UPDATE_PASSWORD"), /*not implemented*/
		    USER_LOGOUT("USER_LOGOUT"),
		    
		    CREATE_ADMIN("CREATE_ADMIN"),
		    
		    CREATE_WORKGROUP("CREATE_WORKGROUP"),
		    UPDATE_WORKGROUP("UPDATE_WORKGROUP"),
		    DELETE_WORKGROUP("DELETE_WORKGROUP"),
		    ADD_MEMBER_TO_WORKGROUP("ADD_MEMBER_TO_WORKGROUP"),
		    REMOVE_MEMBER_FROM_WORKGROUP("REMOVE_MEMBER_FROM_WORKGROUP"),
		    
		    CREATE_CATEGORY("CREATE_CATEGORY"),
		    UPDATE_CATEGORY("UPDATE_CATEGORY"),
		    DELETE_CATEGORY("DELETE_CATEGORY"),
		    
		    IMPORT_WADL("IMPORT_WADL"),
		    IMPORT_WSDL("IMPORT_WSDL"),
		    ENABLE_WEBSERVICE("ENABLE_WEBSERVICE"),
		    DISABLE_WEBSERVICE("DISABLE_WEBSERVICE"),
		    DELETE_WEBSERVICE("DELETE_WEBSERVICE"),
		    
	        CREATE_MASHUP_PROJECT("CREATE_MASHUP_PROJECT"),
	        EDIT_MASHUP_PROJECT("EDIT_MASHUP_PROJECT"),
	        UPDATE_MASHUP_PROJECT("UPDATE_MASHUP_PROJECT"), 
	        EDIT_MASHUP_METADATA("EDIT_MASHUP_METADATA"),
	        UPDATE_MASHUP_METADATA("UPDATE_MASHUP_METADATA"), 	        
	        DELETE_MASHUP_PROJECT("DELETE_MASHUP_PROJECT"), 
	        ADD_MEMBER_TO_PROJECT("ADD_MEMBER_TO_PROJECT"),
	        REMOVE_MEMBER_FROM_PROJECT("REMOVE_MEMBER_FROM_PROJECT"),
	        EXPORT_MASHUP_PROJECT("EXPORT_MASHUP_PROJECT"),
	        COMMENT_MASHUP_PROJECT("COMMENT_MASHUP_PROJECT"),
	        DELETE_COMMENT_MASHUP_PROJECT("DELETE_COMMENT_MASHUP_PROJECT"),
	        
		    CREATE_MOCKUP_PROJECT("CREATE_MOCKUP_PROJECT"),
	        EDIT_MOCKUP_PROJECT("EDIT_MOCKUP_PROJECT"),
	        UPDATE_MOCKUP_PROJECT("UPDATE_MOCKUP_PROJECT"), 
	        EDIT_MOCKUP_METADATA("EDIT_MOCKUP_METADATA"),
	        UPDATE_MOCKUP_METADATA("UPDATE_MOCKUP_METADATA"), 		        
		    EXPORT_MOCKUP_GADGET("EXPORT_MOCKUP_GADGET"),
		    DELETE_MOCKUP_PROJECT("DELETE_MOCKUP_PROJECT"),
	        ADD_MEMBER_TO_MOCKUP("ADD_MEMBER_TO_MOCKUP"),
	        REMOVE_MEMBER_FROM_MOCKUP("REMOVE_MEMBER_FROM_MOCKUP"),
	        COMMENT_MOCKUP_PROJECT("COMMENT_MOCKUP_PROJECT"),
	        DELETE_COMMENT_MOCKUP_PROJECT("DELETE_COMMENT_MOCKUP_PROJECT"),	        
		    
		    PUBLISH_MOCKUP_GADGET_TO_MARKETPLACE("PUBLISH_MOCKUP_GADGET_TO_MARKETPLACE"),
		    EDIT_PUBLISHED_GADGET_METADATA("EDIT_PUBLISHED_GADGET_METADATA"),
		    UPDATE_PUBLISHED_GADGET_METADATA("UPDATE_PUBLISHED_GADGET_METADATA"), 
		    DELETE_PUBLISHED_GADGET("DELETE_PUBLISHED_GADGET");
		    
	        @SuppressWarnings("unused")
			private String value;

	        private ActivityType(String value) {
	                this.value = value;
	        }
	        
	        public String getValue() {
	        	return this.value;
	        }
	}; 
	
	public void createAudit(final Activity activity) {
		Preconditions.checkNotNull(activity);
		getEntityManager().persist(activity);
	}	
}