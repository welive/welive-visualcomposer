package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.CkanServer;

public class CkanServerDAO extends AbstractDAO<CkanServer> {

	public CkanServerDAO() {
		super(CkanServer.class);
	}	
}
