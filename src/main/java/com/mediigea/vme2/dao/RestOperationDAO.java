package com.mediigea.vme2.dao;

import java.util.List;

import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;

public class RestOperationDAO extends AbstractDAO<RestOperation> {

	public RestOperationDAO() {
		super(RestOperation.class);
	}

	@SuppressWarnings("unchecked")
	public List<RestOperation> getAllByService(ServiceCatalog s) {
		if (s == null)
			return null;

		List<RestOperation> restResourceList = getEntityManager()
				.createQuery("from RestOperation as rr where rr.serviceCatalog.id= " + s.getId() +" order by rr.tag, rr.path")
				.getResultList();
		return restResourceList;
	}
}
