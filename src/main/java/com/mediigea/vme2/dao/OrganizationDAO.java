package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.Organization;

public class OrganizationDAO extends AbstractDAO<Organization> {

	public OrganizationDAO() {
		super(Organization.class);
	}
}
