package com.mediigea.vme2.dao;

import java.util.HashMap;
import java.util.List;

import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.SoapOperation;

public class SoapOperationDAO extends AbstractDAO<SoapOperation> {

	public SoapOperationDAO() {
		super(SoapOperation.class);
	}

	public List<SoapOperation> getAllByService(ServiceCatalog s) {

		if (s == null)
			return null;

		String hql = "from SoapOperation as so where so.serviceCatalog.id=:Id";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("Id", s.getId());
		return this.getAllBy(hql, parameters);
	}
}
