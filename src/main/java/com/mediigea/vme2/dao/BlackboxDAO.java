package com.mediigea.vme2.dao;

import com.mediigea.vme2.entity.Blackbox;

public class BlackboxDAO extends AbstractDAO<Blackbox> {

	public BlackboxDAO() {
		super(Blackbox.class);
	}	
}
