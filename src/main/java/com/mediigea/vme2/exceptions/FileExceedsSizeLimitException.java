package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class FileExceedsSizeLimitException extends Exception {

	public FileExceedsSizeLimitException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FileExceedsSizeLimitException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FileExceedsSizeLimitException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FileExceedsSizeLimitException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
