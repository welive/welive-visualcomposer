package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class UsedInnerProcessException extends Exception {

	public UsedInnerProcessException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UsedInnerProcessException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UsedInnerProcessException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UsedInnerProcessException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}