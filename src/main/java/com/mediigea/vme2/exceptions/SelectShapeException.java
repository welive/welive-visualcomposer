package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class SelectShapeException extends Exception {

	public SelectShapeException() {
		super();		
	}

	public SelectShapeException(Throwable cause) {
		super(cause);	
	}		
}
