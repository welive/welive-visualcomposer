package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class ReadStylesheetException extends Exception {

	public ReadStylesheetException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReadStylesheetException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ReadStylesheetException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ReadStylesheetException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
