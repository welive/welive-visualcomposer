package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class UsedBlackboxException extends Exception {

	public UsedBlackboxException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UsedBlackboxException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UsedBlackboxException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UsedBlackboxException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}