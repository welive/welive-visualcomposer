package com.mediigea.vme2.exceptions;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public class RestJsonpException extends RestException {

	private String callback;
	
	/**
	 * @return the callback
	 */
	public String getCallback() {
		return callback;
	}

	public RestJsonpException(String callback, HttpStatus errorCode, String redirectUrl) {
		super(errorCode, redirectUrl);
		this.callback = callback;
	}
	public RestJsonpException(String callback, HttpStatus errorCode, String redirectUrl, String extendedMessage) {
		super(errorCode, redirectUrl,extendedMessage);
		this.callback = callback;
	}
	public RestJsonpException(String callback, String message, Throwable cause, String additionalMessage) {		
		super(message, cause, additionalMessage);
		this.callback = callback;
	}	
}
