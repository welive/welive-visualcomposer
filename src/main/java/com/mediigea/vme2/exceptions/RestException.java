package com.mediigea.vme2.exceptions;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public class RestException extends Exception {

	private HttpStatus errorCode;
	private String extendedMessage;
	private String redirectUrl;
	
	
	public RestException(HttpStatus errorCode) {
		super();
		this.errorCode = errorCode;
	}
	
	public RestException(HttpStatus errorCode, String redirectUrl) {
		super();
		this.errorCode = errorCode;
		this.redirectUrl = redirectUrl;
	}	
	public RestException(HttpStatus errorCode, String redirectUrl,String additionalMessage) {
		super();
		this.errorCode = errorCode;
		this.redirectUrl = redirectUrl;
		this.extendedMessage = additionalMessage;
	}
	public RestException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RestException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	
	public RestException(String message, Throwable cause, String additionalMessage) {		
		super(message, cause);
		this.extendedMessage = additionalMessage;
	}	

	public RestException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RestException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public HttpStatus getErrorCode() {
		return errorCode;
	}

	/**
	 * @return the redirectUrl
	 */
	public String getRedirectUrl() {
		return redirectUrl;
	}

	/**
	 * @return the additionalMessage
	 */
	public String getExtendedMessage() {
		return extendedMessage;
	}		
}
