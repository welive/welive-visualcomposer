package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class ImportProjectException extends Exception {

	public ImportProjectException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ImportProjectException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ImportProjectException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ImportProjectException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}