package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class ReadResourceException extends Exception {

	public ReadResourceException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReadResourceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ReadResourceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ReadResourceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
