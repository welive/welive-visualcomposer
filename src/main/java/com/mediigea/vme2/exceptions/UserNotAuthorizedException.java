package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class UserNotAuthorizedException extends Exception {

	private Integer serviceId;
	private Integer operationId; // IVAN: added to allow auth at operation level
	
	public UserNotAuthorizedException() {		
		super();
	}
	
	public UserNotAuthorizedException(Integer serviceId) {		
		super();
		this.serviceId = serviceId;
	}
	
	public UserNotAuthorizedException(Integer serviceId, Integer operationId) {		
		super();
		this.serviceId = serviceId;
		this.operationId = operationId;
	}

	public UserNotAuthorizedException(Integer serviceId, String message, Throwable cause) {
		super(message, cause);
		this.serviceId = serviceId;
	}

	public UserNotAuthorizedException(Integer serviceId, String message) {
		super(message);
		this.serviceId = serviceId;
	}

	public UserNotAuthorizedException(Integer serviceId, Throwable cause) {
		super(cause);
		this.serviceId = serviceId;
	}

	/**
	 * @return the serviceId
	 */
	public Integer getServiceId() {
		return serviceId;
	}	
	
	public Integer getOperationId() {
		return operationId;
	}
}