package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class InvokeWorkflowException extends Exception {

	public InvokeWorkflowException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvokeWorkflowException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvokeWorkflowException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvokeWorkflowException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}