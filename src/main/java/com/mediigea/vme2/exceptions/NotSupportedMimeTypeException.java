package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class NotSupportedMimeTypeException extends Exception {

	public NotSupportedMimeTypeException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NotSupportedMimeTypeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotSupportedMimeTypeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotSupportedMimeTypeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
