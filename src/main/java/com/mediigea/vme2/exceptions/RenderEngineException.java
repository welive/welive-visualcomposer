package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class RenderEngineException extends Exception {

	public RenderEngineException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RenderEngineException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RenderEngineException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RenderEngineException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
