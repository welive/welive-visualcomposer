package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class WriteGadgetException extends Exception {

	public WriteGadgetException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WriteGadgetException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WriteGadgetException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WriteGadgetException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
