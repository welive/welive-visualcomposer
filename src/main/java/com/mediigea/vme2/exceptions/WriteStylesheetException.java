package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class WriteStylesheetException extends Exception {

	public WriteStylesheetException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WriteStylesheetException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WriteStylesheetException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WriteStylesheetException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
