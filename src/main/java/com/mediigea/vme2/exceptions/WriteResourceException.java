package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class WriteResourceException extends Exception {

	public WriteResourceException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WriteResourceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WriteResourceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WriteResourceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
