package com.mediigea.vme2.exceptions;

@SuppressWarnings("serial")
public class WorkflowScriptException extends Exception {

	public WorkflowScriptException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WorkflowScriptException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WorkflowScriptException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WorkflowScriptException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
