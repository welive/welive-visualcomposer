package com.mediigea.vme2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "servicecatalog_ext", schema = "public")
public class ServiceCatalogExt implements java.io.Serializable
{
	@Id
    @Column(name = "id")
	private int id;
	
	@Column(name = "from_marketplace")
	private boolean from_marketplace;
	
	@Column(name = "endpoint_url")
	private String endpoint_url;
	
	@Column(name = "link_page")
	private String link_page;
	
	@Column(name = "description", columnDefinition = "TEXT")
	private String description;
	
	@Column(name ="wsmodel_id")
	private int wsmodel_id;

	public int getId() 
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public boolean isFrom_marketplace() 
	{
		return from_marketplace;
	}

	public void setFrom_marketplace(boolean form_marketplace) 
	{
		this.from_marketplace = form_marketplace;
	}

	public String getEndpoint_url() 
	{
		return endpoint_url;
	}

	public void setEndpoint_url(String endpoint_url) 
	{
		this.endpoint_url = endpoint_url;
	}

	public String getLink_page() 
	{
		return link_page;
	}

	public void setLink_page(String link_page) 
	{
		this.link_page = link_page;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	public int getWsmodel_id()
	{
		return wsmodel_id;
	}
	
	public void setWsmodel_id(int wsmodel_id)
	{
		this.wsmodel_id = wsmodel_id;
	}
}
