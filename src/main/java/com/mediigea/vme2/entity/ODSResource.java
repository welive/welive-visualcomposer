package com.mediigea.vme2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "odsresource", schema = "public")
public class ODSResource implements java.io.Serializable
{
	private int id;
	private String title;
	private String description;
	private String url;
	private String odsResourceId;
	private String dataSchema;
	
	private ODSDataset odsDataset;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "odsresource_seq_gen")
    @SequenceGenerator(name = "odsresource_seq_gen", sequenceName = "seq_odsresource")
    @Column(name = "id")
	public int getId() 
	{
		return id;
	}
	
	public void setId(int id) 
	{
		this.id = id;
	}
	
	@Column(name="title", columnDefinition = "TEXT", nullable = false)
	public String getTitle() 
	{
		return title;
	}
	
	public void setTitle(String title) 
	{
		this.title = title;
	}
	
	@Column(name="description", columnDefinition = "TEXT", nullable = true)
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	@Column(name="url", columnDefinition = "TEXT", nullable = true)
	public String getUrl() 
	{
		return url;
	}
	
	public void setUrl(String url) 
	{
		this.url = url;
	}
	
	@Column(name="odsresourceid", columnDefinition = "TEXT", nullable = false)
	public String getOdsResourceId() 
	{
		return odsResourceId;
	}
	
	public void setOdsResourceId(String odsResourceId) 
	{
		this.odsResourceId = odsResourceId;
	}
	
	@Column(name="dataschema", columnDefinition = "TEXT")
	public String getDataSchema() 
	{
		return dataSchema;
	}
	
	public void setDataSchema(String dataSchema) 
	{
		this.dataSchema = dataSchema;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "odsdataset_id", nullable = false)
	public ODSDataset getOdsDataset() 
	{
		return odsDataset;
	}

	public void setOdsDataset(ODSDataset odsDataset) 
	{
		this.odsDataset = odsDataset;
	}
	
	
}
