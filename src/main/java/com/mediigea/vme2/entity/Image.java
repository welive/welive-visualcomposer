package com.mediigea.vme2.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "image", schema = "public")
public class Image implements java.io.Serializable {
	
	private int id; 			
	
	private String description;
	private String pathfileImage;
	private String imageType = "S";  
	private Mashup mashup;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_image")
    @SequenceGenerator(name = "seq_image", sequenceName = "image_id_seq")
    @Column(name = "id")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	
	@Column(name = "description", length = 100)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name = "pathfile_image", nullable = false, length = 200)
	public String getPathfileImage() {
		return this.pathfileImage;
	}

	public void setPathfileImage(String pathfileImage) {
		this.pathfileImage = pathfileImage;
	}
	
	
	@Column(name = "image_type", nullable = false, length = 1)
	public String getImageType() {
		return this.imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mashup_id")
	public Mashup getMashup() {
		return mashup;
	}

	public void setMashup(Mashup mashup) {
		this.mashup = mashup;
	}	
	
		
}
