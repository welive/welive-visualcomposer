package com.mediigea.vme2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "organization", schema = "public")
public class Organization implements java.io.Serializable 
{
	private int id;
	private int extId;
	private String name;
	private String info;
	private Profile owner;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "organization_seq_gen")
    @SequenceGenerator(name = "organization_seq_gen", sequenceName = "seq_organization")
    @Column(name = "id")
	public int getId() 
	{
		return id;
	}
	
	public void setId(int id) 
	{
		this.id = id;
	}
	
	@Column(name = "ext_id", columnDefinition="integer default 0")
	public int getExtId() 
	{
		return extId;
	}
	
	public void setExtId(int extId) 
	{
		this.extId = extId;
	}
	
	@Column(name = "name", columnDefinition = "TEXT")
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	@Column(name = "info", columnDefinition = "TEXT")
	public String getInfo() 
	{
		return info;
	}
	
	public void setInfo(String info) 
	{
		this.info = info;
	}
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "owner_id", referencedColumnName="id")
	public Profile getOwner() 
	{
		return owner;
	}
	
	public void setOwner(Profile owner) 
	{
		this.owner = owner;
	}

}
