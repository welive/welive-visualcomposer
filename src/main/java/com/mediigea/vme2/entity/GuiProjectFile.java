package com.mediigea.vme2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "gui_project_file", schema = "public")
public class GuiProjectFile implements java.io.Serializable {
	
	private int id;
	private GuiProject project;
	private String filename;	
	private String contentType;
	private Boolean isdefault;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gui_project_file_seq_gen")
    @SequenceGenerator(name = "gui_project_file_seq_gen", sequenceName = "seq_gui_project_file")
    @Column(name = "id")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project_id", nullable = false, referencedColumnName="id")
	public GuiProject getGuiProject() {
		return project;
	}

	public void setGuiProject(GuiProject project) {
		this.project = project;
	}

	@Column(name = "filename", nullable = false, length = 80)
	public String getFilename() {
		return filename;
	}
	
	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Column(name = "type", nullable = false, length = 24)
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}	
	
	@Column(name = "isdefault")
	public Boolean getIsDefault() {
		return isdefault;
	}
	
	public void setIsDefault(Boolean isdefault) {
		this.isdefault = isdefault;
	}	
}
