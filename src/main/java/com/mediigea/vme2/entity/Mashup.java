package com.mediigea.vme2.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@SuppressWarnings("serial")
@Entity
@Table(name = "mashup", schema = "public")
public class Mashup implements java.io.Serializable {
	
	private Integer id; 			
	private String name;	
	private String description;
	private String version;
	private String tags;  
	private String authorGroup;
	private String pathMashupFile;
	private Integer categoryId;
	private List<Image> imageList;
	private Category category;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mashup")
    @SequenceGenerator(name = "seq_mashup", sequenceName = "mashup_id_seq")
    @Column(name = "id")
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Cascade({CascadeType.SAVE_UPDATE})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "category_id")
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Column(name = "name", length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	
	@Column(name = "description", nullable = false, length = 1000)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	@Column(name = "version", length = 50)
	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	

	@Column(name = "tags", length = 200)
	public String getTags() {
		return this.tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}	

	
	@Column(name = "author_group", nullable = false, length = 100)
	public String getAuthorGroup() {
		return this.authorGroup;
	}

	public void setAuthorGroup(String authorGroup) {
		this.authorGroup = authorGroup;
	}		


	@Column(name = "path_mashup_file", nullable = false, length = 500)
	public String getPathMashupFile() {
		return this.pathMashupFile;
	}

	public void setPathMashupFile(String pathMashupFile) {
		this.pathMashupFile = pathMashupFile;
	}	
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "mashup")
	public List<Image> getImageList() {
		return imageList;
	}

	public void setImageList(List<Image> imageList) {
		this.imageList = imageList;
	}		
	
}
