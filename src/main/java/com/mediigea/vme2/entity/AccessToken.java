package com.mediigea.vme2.entity;

// Generated 26-giu-2013 11.57.17 by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Comment generated by hbm2java
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "access_token", schema = "public")
public class AccessToken implements java.io.Serializable {
	
	private String userConnectorId;
	private ServiceCatalog provider;
	private RestOperation operation; // IVAN: added to allow auth at operation level
	private String accessToken;
	private String tokenSecret;	
	private Date createdAt;
	
	public AccessToken() {
	}

	@Id    
    @Column(name = "user_connector_id")
	public String getUserConnectorId() {
		return this.userConnectorId;
	}

	public void setUserConnectorId(String userConnectorId) {
		this.userConnectorId = userConnectorId;
	}
	
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicecatalog_id", nullable = true)
	public ServiceCatalog getProvider() {
		return provider;
	}

	public void setProvider(ServiceCatalog provider) {
		this.provider = provider;
	}
	
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "operation_id", nullable = true)
	public RestOperation getOperation() {
		return operation;
	}

	public void setOperation(RestOperation operation) {
		this.operation = operation;
	}

	@Column(name = "access_token", nullable = false, columnDefinition = "TEXT")
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	@Column(name = "token_secret", nullable = false, columnDefinition = "TEXT")
	public String getTokenSecret() {
		return tokenSecret;
	}

	public void setTokenSecret(String tokenSecret) {
		this.tokenSecret = tokenSecret;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", updatable=false, nullable = false)
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	
}
