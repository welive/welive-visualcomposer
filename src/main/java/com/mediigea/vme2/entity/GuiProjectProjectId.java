package com.mediigea.vme2.entity;


import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Embeddable
public class GuiProjectProjectId implements java.io.Serializable {
	
	private GuiProject guiProject;
	private Project project;

	public GuiProjectProjectId() {
	}
	
	public GuiProjectProjectId(GuiProject guiProject, Project project) {
		this.guiProject = guiProject;
		this.project = project;
	}
	
	
	@ManyToOne
	GuiProject getGuiProject() {
		return guiProject;
	}

	void setGuiProject(GuiProject guiProject) {
		this.guiProject = guiProject;
	}	

	@ManyToOne
	Project getProject() {
		return project;
	}

	void setProject(Project project) {
		this.project = project;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		GuiProjectProjectId that = (GuiProjectProjectId) o;

		if (guiProject != null ? !guiProject.equals(that.guiProject) : that.guiProject != null)
			return false;
		if (project != null ? !project.equals(that.project) : that.project != null)
			return false;

		return true;
	}

	public int hashCode() {
		int result;
		result = (guiProject != null ? guiProject.hashCode() : 0);
		result = 31 * result + (project != null ? project.hashCode() : 0);
		return result;
	}

}
