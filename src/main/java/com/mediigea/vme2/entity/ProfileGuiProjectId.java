package com.mediigea.vme2.entity;


import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Embeddable
public class ProfileGuiProjectId implements java.io.Serializable {
	
	private Profile profile;
	private GuiProject project;

	public ProfileGuiProjectId() {
	}
	
	public ProfileGuiProjectId(Profile profile, GuiProject guiproject) {
		this.profile = profile;
		this.project = guiproject;
	}
	
	
	@ManyToOne
	Profile getProfile() {
		return profile;
	}

	void setProfile(Profile profile) {
		this.profile = profile;
	}	

	@ManyToOne
	GuiProject getProject() {
		return project;
	}

	void setProject(GuiProject project) {
		this.project = project;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ProfileGuiProjectId that = (ProfileGuiProjectId) o;

		if (profile != null ? !profile.equals(that.profile) : that.profile != null)
			return false;
		if (project != null ? !project.equals(that.project) : that.project != null)
			return false;

		return true;
	}

	public int hashCode() {
		int result;
		result = (profile != null ? profile.hashCode() : 0);
		result = 31 * result + (project != null ? project.hashCode() : 0);
		return result;
	}

}
