package com.mediigea.vme2.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "blackbox", schema = "public")
public class Blackbox implements java.io.Serializable {
	
	private int project_id;
	private String name;
	private String description;
	private String definition;
	private Project mashup;
	
	
	
	/**
	 * @return the project_id
	 */
	@Id
	public int getProject_id() {
		return project_id;
	}

	/**
	 * @param project_id the project_id to set
	 */
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}

	@OneToOne(fetch = FetchType.LAZY, optional = false)	
	@JoinColumn(name = "project_id", nullable = false, referencedColumnName="id")	
	public Project getMashup() {
		return mashup;
	}

	public void setMashup(Project mashup) {
		this.mashup = mashup;
	}

	@Column(name = "name", nullable = false, length = 80)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Basic(fetch = FetchType.LAZY)
	@Column(name = "definition", columnDefinition = "TEXT", nullable = false)
	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}			
	
	@Column(name = "description", columnDefinition = "TEXT", nullable = true)
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}	
}
