package com.mediigea.vme2.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@SuppressWarnings("serial")
@Entity
@Table(name = "odsdataset", schema = "public")
public class ODSDataset  implements java.io.Serializable
{
	private int id;
	private String title;
	private String description;
	private int visibility;
	private String odsDatasetId;
	private int mkpId;
	
	private Set<ODSResource> resources = new HashSet<ODSResource>(0);
	private Profile profile;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "odsdataset_seq_gen")
    @SequenceGenerator(name = "odsdataset_seq_gen", sequenceName = "seq_odsdataset")
    @Column(name = "id")
	public int getId() 
	{
		return id;
	}
	
	public void setId(int id) 
	{
		this.id = id;
	}
	
	@Column(name="title", columnDefinition = "TEXT", nullable = false)
	public String getTitle() 
	{
		return title;
	}
	
	public void setTitle(String title) 
	{
		this.title = title;
	}
	
	@Column(name="description", columnDefinition = "TEXT", nullable = true)
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description) 
	{
		this.description = description;
	}
	
	@Column(name = "visibility", nullable = false)
	public int getVisibility() 
	{
		return visibility;
	}
	
	public void setVisibility(int visibility) 
	{
		this.visibility = visibility;
	}
	
	@Column(name="odsdatasetid", columnDefinition = "TEXT", nullable = false)
	public String getOdsDatasetId() 
	{
		return odsDatasetId;
	}
	
	public void setOdsDatasetId(String odsDatasetId) 
	{
		this.odsDatasetId = odsDatasetId;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "odsDataset")
	@Cascade({CascadeType.REMOVE})
	public Set<ODSResource> getResources() 
	{
		return resources;
	}
	
	public void setResources(Set<ODSResource> resources) 
	{
		this.resources = resources;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "profile_id", nullable = false)
	public Profile getProfile() 
	{
		return profile;
	}
	
	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	
	@Column(name="mkp_id", columnDefinition = "integer default 0")
	public int getMkpId() {
		return mkpId;
	}

	public void setMkpId(int mkpId) {
		this.mkpId = mkpId;
	}
	
}
