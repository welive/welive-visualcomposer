package com.mediigea.vme2.entity;


import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Embeddable
public class ProjectInnerProcessId implements java.io.Serializable {
	
	private Project innerProcess;
	private Project project;

	public ProjectInnerProcessId() {
	}
	
	public ProjectInnerProcessId(Project innerProcess, Project project) {
		this.innerProcess = innerProcess;
		this.project = project;
	}
	
	
	@ManyToOne
	Project getInnerProcess() {
		return innerProcess;
	}

	void setInnerProcess(Project innerProcess) {
		this.innerProcess = innerProcess;
	}	

	@ManyToOne
	Project getProject() {
		return project;
	}

	void setProject(Project project) {
		this.project = project;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ProjectInnerProcessId that = (ProjectInnerProcessId) o;

		if (innerProcess != null ? !innerProcess.equals(that.innerProcess) : that.innerProcess != null)
			return false;
		if (project != null ? !project.equals(that.project) : that.project != null)
			return false;

		return true;
	}

	public int hashCode() {
		int result;
		result = (innerProcess != null ? innerProcess.hashCode() : 0);
		result = 31 * result + (project != null ? project.hashCode() : 0);
		return result;
	}

}
