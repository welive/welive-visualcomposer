package com.mediigea.vme2.entity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@SuppressWarnings("serial")
@Entity
@Table(name = "activity", schema = "public")
public class Activity implements java.io.Serializable {
	
	private Integer id; 		
	private Profile profile;
	private Integer sourceId;
	private String  activityType;
	private Activity parentActivity;
	private String   description;
	private Date 	  createdAt;
	
	private List<Activity> childrenActivities = new ArrayList<Activity>(0);		  	
	
	
	public Activity() {
		this.createdAt = new Date();
	}

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "seq_identity")
    @SequenceGenerator(name = "seq_identity", sequenceName = "identity_id_seq")
    @Column(name = "id")
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "profile_id", nullable = false)
	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	
	@Column(name = "source_id")
	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	@Column(name = "activity_type", length = 100)
	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "parent_id")
	public Activity getParentActivity() {
		return this.parentActivity;
	}
	
	public void setParentActivity(Activity parentActivity) {
		this.parentActivity = parentActivity;
	}	
	
	@Column(name = "description", length = 200)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parentActivity")
	public List<Activity> getChildrenActivities() {
		return childrenActivities;
	}
	
	public void setChildrenActivities(List<Activity> childrenActivities) {
		this.childrenActivities = childrenActivities;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", nullable = false, length = 29)
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}	
	
	
}
