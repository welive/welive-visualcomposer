package com.mediigea.vme2.entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "category", schema = "public")
public class Category implements java.io.Serializable {
	
	private Integer id; 		
	private Category parentCategory;
	private String description;
	private List<Mashup> mashupList;
	
	
	private List<Category> childrenCategories = new ArrayList<Category>(0);		  	
	

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_category")
    @SequenceGenerator(name = "seq_category", sequenceName = "category_id_seq")
    @Column(name = "id")
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_id")
	public Category getParentCategory() {
		return this.parentCategory;
	}
	
	public void setParentCategory(Category parentCategory) {
		this.parentCategory = parentCategory;
	}	
	
	@Column(name = "description", length = 100)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parentCategory")
	public List<Category> getChildrenCategories() {
		return childrenCategories;
	}
	
	public void setChildrenCategories(List<Category> childrenCategories) {
		this.childrenCategories = childrenCategories;
	}
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
	public List<Mashup> getMashupList() {
		return mashupList;
	}

	public void setMashupList(List<Mashup> mashupList) {
		this.mashupList = mashupList;
	}	
	
}
