package com.mediigea.vme2.entity;

import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@SuppressWarnings("serial")
@Entity
@Table(name = "unread_comment", schema = "public")
@AssociationOverrides({
	@AssociationOverride(name = "pk.comment", joinColumns = @JoinColumn(name = "comment_id")),	
	@AssociationOverride(name = "pk.profile", joinColumns = @JoinColumn(name = "profile_id"))
	//,
	//@AssociationOverride(name = "pk.project", joinColumns = @JoinColumn(name = "project_id"))
	//,
	//@AssociationOverride(name = "pk.gui_project", joinColumns = @JoinColumn(name = "gui_project_id")) 
})
public class UnreadComment implements java.io.Serializable {


	private UnreadCommentId pk = new UnreadCommentId();

	private Date createdAt;


	public UnreadComment() {
		this.createdAt = new Date();
	}

	@EmbeddedId
	public UnreadCommentId getPk() {
		return pk;
	}

	public void setPk(UnreadCommentId pk) {
		this.pk = pk;
	}
	
//	@Transient
//	public Project getProject() {
//		return this.pk.getProject();
//	}
//
//	public void setProject(Project project) {
//		this.pk.setProject(project);
//	}
	
//	@Transient
//	public GuiProject getGuiProject() {
//		return this.pk.getGuiProject();
//	}
//
//	public void setGuiProject(GuiProject project) {
//		this.pk.setGuiProject(project);
//	}	

	@Transient
	public Profile getProfile() {
		return this.pk.getProfile();
	}

	public void setProfile(Profile profile) {
		this.pk.setProfile(profile);
	}
	

	@Transient
	public Comment getComment() {
		return this.pk.getComment();
	}
	
	public void setComment(Comment comment) {
		this.pk.setComment(comment);
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", nullable = false, length = 29)
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}	
}
