package com.mediigea.vme2.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "restoperationtemplate", schema = "public")
public class RestOperationTemplate implements java.io.Serializable {
	
	private int id;
	private RestOperation operation;
	private String name;
	private String description;
	private String definition;	
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "restoperationtemplate_seq_gen")
    @SequenceGenerator(name = "restoperationtemplate_seq_gen", sequenceName = "seq_restoperationtemplate")
    @Column(name = "id")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "operation_id", nullable = false, referencedColumnName="id")	
	public RestOperation getOperation() {
		return operation;
	}

	public void setOperation(RestOperation operation) {
		this.operation = operation;
	}

	@Column(name = "name", nullable = false, length = 80)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Basic(fetch = FetchType.LAZY)
	@Column(name = "definition", columnDefinition = "TEXT", nullable = true)
	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	/**
	 * @return the description
	 */
	@Column(name = "description", length = 200)
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}			
}
