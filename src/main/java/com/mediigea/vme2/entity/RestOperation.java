package com.mediigea.vme2.entity;

import java.io.IOException;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.rest.RestMethod;

@SuppressWarnings("serial")
@Entity
@Table(name = "restoperation", schema = "public")
public class RestOperation implements java.io.Serializable {

	private int id;

	private String baseURL;
	private String path;
	private String name;
	private String label;
	private String doc;
	private String tag;
	private String authentication;
	private AuthProviderType authType; // IVAN: added to allow auth at operation level
	private String execMethod;
	private List<RestOperationTemplate> template;

	private ServiceCatalog serviceCatalog;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "restoperation_seq_gen")
	@SequenceGenerator(name = "restoperation_seq_gen", sequenceName = "seq_restoperation")
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the baseURL
	 */
	@Column(name = "baseurl", nullable = false)
	public String getBaseURL() {
		return baseURL;
	}

	/**
	 * @param baseURL
	 *            the baseURL to set
	 */
	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	/**
	 * @return the path
	 */
	@Column(name = "path", nullable = false)
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the label
	 */
	@Column(name = "label")
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the doc
	 */
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "documentation", columnDefinition = "TEXT", nullable = true)
	public String getDoc() {
		return doc;
	}

	/**
	 * @param doc the doc to set
	 */
	public void setDoc(String doc) {
		this.doc = doc;
	}


	/**
	 * @return the tag
	 */
	@Column(name = "tag")
	public String getTag() {
		return tag;
	}

	/**
	 * @param tag
	 *            the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	/**
	 * @return the authentication
	 */
	@Column(name = "auth")
	public String getAuthentication() {
		return authentication;
	}

	/**
	 * @param authentication the authentication to set
	 */
	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "auth_type", nullable = true)
	public AuthProviderType getAuthType() {
		return this.authType;
	}

	public void setAuthType(AuthProviderType oauthType) {
		this.authType = oauthType;
	}

	/**
	 * @return the definition
	 */
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "method", columnDefinition = "TEXT", nullable = true)
	public String getExecMethod() {
		return execMethod;
	}

	/**
	 * @param definition
	 *            the definition to set
	 */
	public void setExecMethod(String definition) {
		this.execMethod = definition;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "servicecatalog_id", nullable = false)
	public ServiceCatalog getServiceCatalog() {
		return this.serviceCatalog;
	}

	/**
	 * 
	 * @param serviceCatalog
	 */
	public void setServiceCatalog(ServiceCatalog serviceCatalog) {
		this.serviceCatalog = serviceCatalog;
	}

	/**
	 * @return the template
	 */
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "operation", fetch = FetchType.EAGER)
	public List<RestOperationTemplate> getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(List<RestOperationTemplate> template) {
		this.template = template;
	}	
	
	// IVAN
	@Transient
	public RestMethod getRestMethod() throws JsonParseException, JsonMappingException, IOException
	{
		return new ObjectMapper().readValue(this.execMethod, RestMethod.class);
	}
}
