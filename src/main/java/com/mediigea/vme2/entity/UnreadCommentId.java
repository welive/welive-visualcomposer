package com.mediigea.vme2.entity;


import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Embeddable
public class UnreadCommentId implements java.io.Serializable {

	private Comment comment;	
	private Profile profile;
	//private Project project;
	//private GuiProject guiProject;

	public UnreadCommentId() {
	}

	public UnreadCommentId(Comment comment, Profile profile
		//	Project project
		) {
		this.comment = comment;
		this.profile = profile;
		//this.project = project;
	}
	
//	public UnreadCommentId(Comment comment, Profile profile, GuiProject guiProject) {
//		this.comment = comment;
//		this.profile = profile;
//		this.guiProject = guiProject;
//	}	

	
	@ManyToOne
	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}	
	
	
	@ManyToOne
	Profile getProfile() {
		return profile;
	}

	void setProfile(Profile profile) {
		this.profile = profile;
	}	

//	@ManyToOne
//	Project getProject() {
//		return project;
//	}
//
//	void setProject(Project project) {
//		this.project = project;
//	}
	
//	@ManyToOne
//	GuiProject getGuiProject() {
//		return guiProject;
//	}
//
//	void setGuiProject(GuiProject guiProject) {
//		this.guiProject = guiProject;
//	}	

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UnreadCommentId that = (UnreadCommentId) o;

		if (comment != null ? !comment.equals(that.comment) : that.comment != null)
			return false;
//		if (project != null ? !project.equals(that.project) : that.project != null)
//			return false;
		if (profile != null ? !profile.equals(that.profile) : that.profile != null)
			return false;
//		if (guiProject != null ? !guiProject.equals(that.guiProject) : that.guiProject != null)
//			return false;		
		
		return true;
	}

	public int hashCode() {
		int result;
		result = (profile != null ? profile.hashCode() : 0);
//		result = 31 * result + (project != null ? project.hashCode() : 0);
		result = 31 * result + (comment != null ? comment.hashCode() : 0);
//		result = 31 * result + (guiProject != null ? guiProject.hashCode() : 0);
		return result;
	}

}
