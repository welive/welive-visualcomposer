package com.mediigea.vme2.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@SuppressWarnings("serial")
@Entity
@Table(name = "account", schema = "public")
public class Account implements java.io.Serializable {

	private int id;
	private Profile profile;
	private String email;
	private String psw;
	private String confirmPsw;
	private int role;
	private String token;
	private String validatedOauthId;
	private Date expire;
	private String provider;
	private int status;
	private Date createdAt;
	private Date updatedAt;

	

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "account_seq_gen")
    @SequenceGenerator(name = "account_seq_gen", sequenceName = "seq_account")
    @Column(name = "id")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "profile_id", nullable = false, referencedColumnName="id")
	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@Column(name = "email", nullable = false, unique=true, length = 80)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String usr) {
		this.email = usr;
	}

	@Column(name = "psw", nullable = false, length = 80)
	public String getPsw() {
		return this.psw;
	}

	public void setPsw(String psw) {
		this.psw = psw;
	}

	@Column(name = "role", nullable = false)
	public int getRole() {
		return this.role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	@Column(name = "token")
	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expire", length = 29)
	public Date getExpire() {
		return this.expire;
	}

	public void setExpire(Date expire) {
		this.expire = expire;
	}

	@Column(name = "provider", nullable = false, length = 15)
	public String getProvider() {
		return this.provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	@Column(name = "status", nullable = false)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", nullable = false, length = 29)
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at", nullable = false, length = 29)
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "validated_oauth_id", nullable = true)
	public String getValidatedOauthId() {
		return validatedOauthId;
	}

	public void setValidatedOauthId(String validatedOauthId) {
		this.validatedOauthId = validatedOauthId;
	}

	@Transient
	public String getConfirmPsw() {
		return confirmPsw;
	}

	public void setConfirmPsw(String confirmPsw) {
		this.confirmPsw = confirmPsw;
	}

}
