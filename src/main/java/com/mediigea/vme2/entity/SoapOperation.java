package com.mediigea.vme2.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "soapoperation", schema = "public")
public class SoapOperation implements java.io.Serializable {
	private int id;
    
	private String operationName;
	private String wsdlUrl;
	private String endPoint;
	private String bindingName;
	private String portTypeName;
	
	private String soapAction;
	private String inputParams;
	private String outputParams;
	
	private List<SoapOperationTemplate> template;
	
	private ServiceCatalog serviceCatalog;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "soapoperation_seq_gen")
	@SequenceGenerator(name = "soapoperation_seq_gen", sequenceName = "seq_soapoperation")
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	
	
	@Column(name = "wsdlurl", nullable = false)
	public String getWsdlUrl() {
		return wsdlUrl;
	}

	public void setWsdlUrl(String wsdlUrl) {
		this.wsdlUrl = wsdlUrl;
	}
	
	@Column(name = "endpoint", nullable = false)
	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}	

	@Column(name = "soapbinding", nullable = false)
	public String getBindingName() {
		return bindingName;
	}

	public void setBindingName(String bindingName) {
		this.bindingName = bindingName;
	}

	@Column(name = "porttype", nullable = false)
	public String getPortTypeName() {
		return portTypeName;
	}

	public void setPortTypeName(String portTypeName) {
		this.portTypeName = portTypeName;
	}

	
	@Column(name = "operationname", nullable = false)
	public String getOperationName() {
		return operationName;
	}

	@Column(name = "operationname", nullable = false)
	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}
	
	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}	
	
	@Column(name = "input_params", columnDefinition = "TEXT", nullable = true)
	public String getInputParams() {
		return inputParams;
	}

	public void setInputParams(String inputParams) {
		this.inputParams = inputParams;
	}

	@Column(name = "output_params", columnDefinition = "TEXT", nullable = true)
	public String getOutputParams() {
		return outputParams;
	}

	public void setOutputParams(String outputParams) {
		this.outputParams = outputParams;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "servicecatalog_id", nullable = false)
	public ServiceCatalog getServiceCatalog() {
		return this.serviceCatalog;
	}

	public void setServiceCatalog(ServiceCatalog serviceCatalog) {
		this.serviceCatalog = serviceCatalog;
	}
	
	/**
	 * @return the template
	 */
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "operation", fetch = FetchType.EAGER)
	public List<SoapOperationTemplate> getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(List<SoapOperationTemplate> template) {
		this.template = template;
	}	
	
}
