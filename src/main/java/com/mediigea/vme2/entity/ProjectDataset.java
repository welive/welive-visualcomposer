package com.mediigea.vme2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.mediigea.vme2.entity.ODSResource;
import com.mediigea.vme2.entity.Project;

@SuppressWarnings("serial")
@Entity
@Table(name = "projectdataset", schema = "public")
public class ProjectDataset implements java.io.Serializable {
	
	private int id;
	private Project project;
	private ODSResource resource;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "projectdataset_seq_gen")
    @SequenceGenerator(name = "projectdataset_seq_gen", sequenceName = "seq_projectdataset")
    @Column(name = "id")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project_id", nullable = false, referencedColumnName="id")
	public Project getProject() {
		return project;
	}
	
	public void setProject(Project project) {
		this.project = project;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "odsresource_id", nullable = true, referencedColumnName="id")
	public ODSResource getResource() {
		return resource;
	}
	
	public void setResource(ODSResource resource) {
		this.resource = resource;
	}

}
