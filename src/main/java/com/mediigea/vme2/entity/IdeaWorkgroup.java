package com.mediigea.vme2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "ideawg", schema = "public")
public class IdeaWorkgroup implements java.io.Serializable
{
	private int ideaId;
	private String ideaName;
	private Workgroup wg;
	
	@Id
    @Column(name = "idea_id")
	public int getIdeaId() 
	{
		return this.ideaId;
	}
	
	public void setIdeaId(int id)
	{
		this.ideaId = id;
	}
	
	@Column(name = "idea_name", nullable = false, length = 80)
	public String getIdeaName()
	{
		return this.ideaName;
	}
	
	public void setIdeaName(String name)
	{
		this.ideaName = name;
	}
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "workgroup_id", nullable = false, referencedColumnName="id")
	public Workgroup getWg() 
	{
		return this.wg;
	}
	
	public void setWg(Workgroup w)
	{
		this.wg = w;
	}
}
