package com.mediigea.vme2.entity;


import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@SuppressWarnings("serial")
@Entity
@Table(name = "profile_workgroup", schema = "public")
@AssociationOverrides({
	@AssociationOverride(name = "pk.profile", joinColumns = @JoinColumn(name = "profile_id")),
	@AssociationOverride(name = "pk.workgroup", joinColumns = @JoinColumn(name = "workgroup_id")) 
})
public class ProfileWorkgroup implements java.io.Serializable {

	private ProfileWorkgroupId pk = new ProfileWorkgroupId();
	private int workgroupRole;
	private Date createdAt;
	
	
	public ProfileWorkgroup() {
	}

	@EmbeddedId
	public ProfileWorkgroupId getPk() {
		return pk;
	}

	public void setPk(ProfileWorkgroupId pk) {
		this.pk = pk;
	}
	
	@Transient
	public Profile getProfile() {
		return this.pk.getProfile();
	}

	public void setProfile(Profile profile) {
		this.pk.setProfile(profile);
	}
	
	@Transient
	public Workgroup getWorkgroup() {
		return this.pk.getWorkgroup();
	}

	public void setWorkgroup(Workgroup workgroup) {
		this.pk.setWorkgroup(workgroup);
	}

	@Column(name="workgroup_role", nullable=false)	
	public int getWorkgroupRole() {
		return workgroupRole;
	}

	public void setWorkgroupRole(int workgroupRole) {
		this.workgroupRole = workgroupRole;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="created_at", nullable=false)
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
 
		ProfileWorkgroup that = (ProfileWorkgroup) o;
 
		if (getPk() != null ? !getPk().equals(that.getPk())
				: that.getPk() != null)
			return false;
 
		return true;
	}
 
	public int hashCode() {
		return (getPk() != null ? getPk().hashCode() : 0);
	}

}
