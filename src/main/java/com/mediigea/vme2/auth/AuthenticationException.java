package com.mediigea.vme2.auth;

public class AuthenticationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3928952899093963267L;
	
	public AuthenticationException(String message){
		super(message);
	}

}
