package com.mediigea.vme2.auth;

public class RegistrationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3928952899093963267L;
	
	public RegistrationException(String message){
		super(message);
	}

}
