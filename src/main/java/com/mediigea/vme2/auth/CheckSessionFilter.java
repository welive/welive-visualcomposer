package com.mediigea.vme2.auth;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.OncePerRequestFilter;

import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.service.AccountService;
import com.mediigea.vme2.util.VmeConstants;

public class CheckSessionFilter extends OncePerRequestFilter {

	@Autowired
	private AccountService accountService;
	
	private static final Logger logger = LoggerFactory.getLogger(CheckSessionFilter.class);
	
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)throws ServletException, IOException {
		
		logger.info("Execute Filter");		
		String homeUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
		String requestUrl = request.getRequestURL().toString();
		
		
		if(!requestUrl.equals(homeUrl)){
			HttpSession httpSession = request.getSession();
			
			Account logged = (Account) httpSession.getAttribute(VmeConstants.LOGGED_ACCOUNT);
					
			//check admin role in admin path			
			if(logged != null && requestUrl.contains("/admin") && logged.getRole() != VmeConstants.ACCOUNT_ROLE_ADMIN){
				logged = null;
			}
				
			
			if (logged == null ) {				
				logger.debug("prepare redirection");

				String queryString = request.getQueryString();
				String completeUrl = requestUrl + ((queryString!=null && !queryString.isEmpty())?"?" + queryString:"");
				
				String redirection = homeUrl + (requestUrl.contains("/admin") ? "/auth/admin/login" : "/auth/user/login");
				
				httpSession.setAttribute(VmeConstants.REQUESTED_URL, completeUrl);	
				//redirection += "?from=" + completeUrl;
				request.setAttribute(VmeConstants.MESSAGE_TEXT, "message.login.logged.error");
				response.sendRedirect(redirection);
				return;
			}
		}
		
		filterChain.doFilter(request, response);
	}
}
