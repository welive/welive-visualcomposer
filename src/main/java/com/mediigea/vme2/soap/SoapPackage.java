package com.mediigea.vme2.soap;

import java.util.ArrayList;

public class SoapPackage {

	private Long id;
	private String name;
	private String type;
	
	private ArrayList<SoapMethod> operations;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<SoapMethod> getOperations() {
		return operations;
	}

	public void setOperations(ArrayList<SoapMethod> operations) {
		this.operations = operations;
	}

	
}
