package com.mediigea.vme2.soap;

import java.util.HashMap;
import java.util.List;

public class SoapMethod {

	private Long id;
	private String name;
	private String endPoint;
	private String portTypeName;
	private String bindingName;

	private String soapAction;
	private HashMap<String, List<SoapParameter>> inputParams;
	private HashMap<String, List<SoapParameter>> outputParams;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HashMap<String, List<SoapParameter>> getInputParams() {
		return inputParams;
	}

	public void setInputParams(HashMap<String, List<SoapParameter>> inputParams) {
		this.inputParams = inputParams;
	}

	public HashMap<String, List<SoapParameter>> getOutputParams() {
		return outputParams;
	}

	public void setOutputParams(HashMap<String, List<SoapParameter>> outputParams) {
		this.outputParams = outputParams;
	}

	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public String getPortTypeName() {
		return portTypeName;
	}

	public void setPortTypeName(String portTypeName) {
		this.portTypeName = portTypeName;
	}

	public String getBindingName() {
		return bindingName;
	}

	public void setBindingName(String bindingName) {
		this.bindingName = bindingName;
	}

}
