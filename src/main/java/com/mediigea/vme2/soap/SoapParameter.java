package com.mediigea.vme2.soap;

import java.util.Map;

public class SoapParameter {

	private String name;
	private String label = "";
	private String value = "";
	private boolean required = false;
	private String type = "";
	private String xpath = "";
	private String ref = "";
	private String minOccurs = "";
	private String maxOccurs = "";
	private String doc = "";
	
	/**
	 * The possible value for the parameter.
	 */
	private Map<String, String> enumerations = null;
	

	public SoapParameter() {

	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getXpath() {
		return xpath;
	}

	public void setXpath(String xpath) {
		this.xpath = xpath;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getMinOccurs() {
		return minOccurs;
	}

	public void setMinOccurs(String minOccurs) {
		this.minOccurs = minOccurs;
	}

	public String getMaxOccurs() {
		return maxOccurs;
	}

	public void setMaxOccurs(String maxOccurs) {
		this.maxOccurs = maxOccurs;
	}
	
	/**
	 * @return the enumerations
	 */
	public Map<String, String> getEnumerations() {
		return enumerations;
	}

	/**
	 * @param enumerations the enumerations to set
	 */
	public void setEnumerations(Map<String, String> enumerations) {
		this.enumerations = enumerations;
	}

	/**
	 * @return the doc
	 */
	public String getDoc() {
		return doc;
	}

	/**
	 * @param doc the doc to set
	 */
	public void setDoc(String doc) {
		this.doc = doc;
	}

}
