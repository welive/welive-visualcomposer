package com.mediigea.vme2.viewmodel;

import java.io.Serializable;

import java.util.List;
import org.springframework.web.multipart.MultipartFile;


public class AddWsdlServiceViewModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3065816007911817886L;

	private String wsdlUrl;
	
	private List<MultipartFile> wsdlFile;
	
	//private String wsdlFile;

	
	public List<MultipartFile> getWsdlFile() {
		return wsdlFile;
	}

	public void setWsdlFile(List<MultipartFile> files) {
		this.wsdlFile = files;
	}
	
	public String getWsdlUrl() {
		return wsdlUrl;
	}
	
	public void setWsdlUrl(String wsdlUrl) {
		this.wsdlUrl = wsdlUrl;
	}
	
//	public String getWsdlFile() {
//		return wsdlFile;
//	}
//	
//	public void setWsdlFile(String wsdlFile) {
//		this.wsdlFile = wsdlFile;
//	}
}
