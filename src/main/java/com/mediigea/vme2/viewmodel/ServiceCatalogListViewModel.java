package com.mediigea.vme2.viewmodel;

import java.util.List;

import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.service.ServiceCatalogService;

public class ServiceCatalogListViewModel {
	
	private List<ServiceCatalog> myList;
	private List<ServiceCatalog> publicList;
	
	public ServiceCatalogListViewModel(ServiceCatalogService serviceCatalogService, Profile profile) {	
		
		
		myList = serviceCatalogService.findVisibleServices(profile.getId());
		publicList = serviceCatalogService.findPublicServices();
	}

	public List<ServiceCatalog> getMyList() {
		return myList;
	}

	
	public List<ServiceCatalog> getPublicList() {
		return publicList;
	}

	
	
	

}
