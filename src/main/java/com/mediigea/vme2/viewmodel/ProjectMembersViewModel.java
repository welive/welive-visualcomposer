package com.mediigea.vme2.viewmodel;

import java.util.List;

import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.service.ProjectService;

public class ProjectMembersViewModel {
	
	private Project project;
	private List<Profile> members;
	private Profile owner;	
	private boolean loggedOwner;

	public ProjectMembersViewModel(ProjectService projectService, int projectId, Profile profile) {
		this.project = projectService.findProject(projectId);
		this.members = projectService.findProjectMembers(this.getProject());
		this.owner = projectService.findProjectOwner(projectId);
		this.loggedOwner = (getOwner().getId() == profile.getId());
	}
	
	/**
	 * @return the project
	 */
	public Project getProject() {
		return this.project;
	}

	/**
	 * @return the members
	 */
	public List<Profile> getMembers() {
		return members;
	}

	public Profile getOwner() {
		return owner;
	}

	public boolean isLoggedOwner() {
		return loggedOwner;
	}
	
}
