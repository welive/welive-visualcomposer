package com.mediigea.vme2.viewmodel;

import java.io.Serializable;

import java.util.List;
import org.springframework.web.multipart.MultipartFile;


public class AddOpenDataViewModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3065816007911817886L;

	private String sourceName;
	private String url;
	private char separator;
	
	private List<MultipartFile> file;

	/**
	 * @return the sourceName
	 */
	public String getSourceName() {
		return sourceName;
	}

	/**
	 * @param sourceName the sourceName to set
	 */
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the file
	 */
	public List<MultipartFile> getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(List<MultipartFile> file) {
		this.file = file;
	}

	public char getSeparator() {
		return separator;
	}

	public void setSeparator(char separator) {
		this.separator = separator;
	}	
}
