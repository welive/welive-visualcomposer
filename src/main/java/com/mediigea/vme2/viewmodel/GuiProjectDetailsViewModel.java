package com.mediigea.vme2.viewmodel;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.service.GuiEditorService;

public class GuiProjectDetailsViewModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7065585868157966635L;

	/**
	 * 
	 */	

	private static final Logger logger = LoggerFactory.getLogger(GuiProjectDetailsViewModel.class);

	private List<Comment> comments;
	private List<Profile> members;
	private Profile owner;
	private GuiProject project;
	
	private boolean loggedOwner;
	
	public GuiProjectDetailsViewModel(GuiEditorService projectService, int projectId, Profile profile) {

		logger.info("Find details");

		this.project = projectService.findProject(projectId);
		if (this.project != null) {
			this.comments = projectService.findProjectComments(this.project, 5);			
			this.members = projectService.findProjectMembers(this.project);
			this.owner = projectService.findProjectOwner(projectId);
			this.loggedOwner = (getOwner().getId() == profile.getId());
		}

	}

	/**
	 * @return the project
	 */
	public GuiProject getProject() {
		return project;
	}

	/**
	 * @return the comments
	 */
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 * @return the members
	 */
	public List<Profile> getMembers() {
		return members;
	}

	public boolean isLoggedOwner() {
		return loggedOwner;
	}

	public Profile getOwner() {
		return owner;
	}

	
	

	
}
