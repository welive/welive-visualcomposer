package com.mediigea.vme2.viewmodel;

import java.io.Serializable;

import java.util.List;
import org.springframework.web.multipart.MultipartFile;


public class AddWadlViewModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3065816007911817886L;

	private String serviceName;
	private String wadlUrl;
	
	private List<MultipartFile> wadlFile;
	

	
	public List<MultipartFile> getWadlFile() {
		return wadlFile;
	}

	public void setWadlFile(List<MultipartFile> files) {
		this.wadlFile = files;
	}
	
	public String getWadlUrl() {
		return wadlUrl;
	}
	
	public void setWadlUrl(String wadlUrl) {
		this.wadlUrl = wadlUrl;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
