package com.mediigea.vme2.viewmodel;

import java.io.Serializable;

import java.util.List;
import org.springframework.web.multipart.MultipartFile;


public class ImportXmlViewModel implements Serializable {
	private static final long serialVersionUID = 403110816850895904L;	
	
	private List<MultipartFile> xmlFile;
	private String description;
	private String name;
	private Integer workgroupId;

	/**
	 * @return the xmlFile
	 */
	public List<MultipartFile> getXmlFile() {
		return xmlFile;
	}

	/**
	 * @param xmlFile the xmlFile to set
	 */
	public void setXmlFile(List<MultipartFile> xmlFile) {
		this.xmlFile = xmlFile;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the workgroupId
	 */
	public Integer getWorkgroupId() {
		return workgroupId;
	}

	/**
	 * @param workgroupId the workgroupId to set
	 */
	public void setWorkgroupId(Integer workgroupId) {
		this.workgroupId = workgroupId;
	}				
}
