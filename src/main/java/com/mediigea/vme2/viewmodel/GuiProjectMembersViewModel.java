package com.mediigea.vme2.viewmodel;

import java.util.List;

import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.service.GuiEditorService;
import com.mediigea.vme2.service.ProjectService;

public class GuiProjectMembersViewModel {
	
	private GuiProject guiProject;
	private List<Profile> members;
	private Profile owner;	
	private boolean loggedOwner;

	public GuiProjectMembersViewModel(GuiEditorService projectService, int projectId, Profile profile) {
		this.guiProject = projectService.findProject(projectId);
		this.members = projectService.findProjectMembers(this.guiProject);
		this.owner = projectService.findProjectOwner(projectId);
		this.loggedOwner = (getOwner().getId() == profile.getId());
	}
	
	/**
	 * @return the project
	 */
	public GuiProject getGuiProject() {
		return this.guiProject;
	}

	/**
	 * @return the members
	 */
	public List<Profile> getMembers() {
		return members;
	}

	public Profile getOwner() {
		return owner;
	}

	public boolean isLoggedOwner() {
		return loggedOwner;
	}
	
}
