package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/*

This gadget uses proxied content for the canvas view, and inline content for the home view:

<?xml version="1.0" encoding="UTF-8"?>
<Module>
  <ModulePrefs title="Proxied Content Example">
</ModulePrefs>
  <Content view="canvas" 
    href="http://gadget-doc-examples.googlecode.com/svn/trunk/opensocial-09/mycontent.html">
  </Content>
  <Content view="home">
    <![CDATA[
      Hello, home view!
    ]]>
  </Content>
</Module>
 */

@XmlRootElement
public class Content {
	
	public static final String CONTENT = "Content";
	
	public static final String TYPE = "type";
	public static final String HREF = "href";
	public static final String PREFERRED_HEIGHT = "preferred_height";
	public static final String PREFERRED_WIDTH = "preferred_width";
	public static final String VIEW = "view";
	public static final String AUTHZ = "authz";
	
	public static final String TYPE_URL_VALUE = "url";
	public static final String TYPE_HTML_VALUE = "html";
	

	private String type;
	private String href;
	private String preferred_height;
	private String preferred_width;
	private String view;
	private String authz;
	private String content;
	
	
	public Content() {
		
		super();
		
		type = Content.TYPE_HTML_VALUE;
	}
	
	public Content(String type, String href, String preferred_height,
			String preferred_width, String view, String authz, String content) {
		
		super();
		
		if(type!=null && !type.isEmpty())
			this.type = type;
		else
			this.type = Content.TYPE_HTML_VALUE;
		
		this.href = href;
		this.preferred_height = preferred_height;
		this.preferred_width = preferred_width;
		this.view = view;
		this.authz = authz;
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	@XmlValue
	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	@XmlAttribute
	public void setType(String type) {
		this.type = type;
	}

	public String getHref() {
		return href;
	}

	@XmlAttribute
	public void setHref(String href) {
		this.href = href;
	}

	public String getPreferred_height() {
		return preferred_height;
	}

	@XmlAttribute
	public void setPreferred_height(String preferred_height) {
		this.preferred_height = preferred_height;
	}

	public String getPreferred_width() {
		return preferred_width;
	}

	@XmlAttribute
	public void setPreferred_width(String preferred_width) {
		this.preferred_width = preferred_width;
	}

	public String getView() {
		return view;
	}

	@XmlAttribute
	public void setView(String view) {
		this.view = view;
	}

	public String getAuthz() {
		return authz;
	}

	@XmlAttribute
	public void setAuthz(String authz) {
		this.authz = authz;
	}
}
