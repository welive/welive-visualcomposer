package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/*
<ModulePrefs title="My gadget">
  <Icon>http://remote/favicon.ico</Icon>
</ModulePrefs>

<ModulePrefs title="My gadget">
  <Icon mode="base64" type="image/png">base64 encoded data</Icon>
</ModulePrefs>
*/

@XmlRootElement
public class Icon {

	public static final String ICON = "Icon";
	
	public static final String MODE = "mode";
	public static final String TYPE = "type";
	
	
	private String mode;
	private String type;
	private String text;
	
	
	public Icon() {
		
		super();
	}
	
	public Icon(String mode, String type, String text) {
		
		super();
		
		this.mode = mode;
		this.type = type;
		this.setText(text);
	}

	public String getMode() {
		return mode;
	}

	@XmlAttribute
	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getType() {
		return type;
	}

	@XmlAttribute
	public void setType(String type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	@XmlValue
	public void setText(String text) {
		this.text = text;
	}
}
