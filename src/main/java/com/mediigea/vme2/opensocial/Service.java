package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
  <ModulePrefs title="OAuth Contacts" scrolling="true">
    <Require feature="locked-domain"/>
    <OAuth>
      <Service name="google">
        <Access url="https://www.google.com/accounts/OAuthGetAccessToken" method="GET" />
        <Request url="https://www.google.com/accounts/OAuthGetRequestToken?scope=http://www.google.com/m8/feeds/" method="GET" />
        <Authorization url="https://www.google.com/accounts/OAuthAuthorizeToken?oauth_callback=http://oauth.gmodules.com/gadgets/oauthcallback" />
      </Service>
    </OAuth>
  </ModulePrefs>
*/

@XmlRootElement
public class Service extends BaseElement {

	public static final String SERVICE = "Service";

	private Request requestToken;
	private Access accessToken;
	private Token token;
	private Authorization authorization;

	public Service() {
		
		super();
	}
	
	public Service(String name, String value, Request requestToken, 
			Access accessToken, Token token, Authorization authorization) {
		
		super(name, value);
		
		this.requestToken = requestToken;
		this.accessToken = accessToken;
		this.token = token;
		this.authorization = authorization;
	}

	public Request getRequestToken() {
		return requestToken;
	}

	@XmlElement(name="Request")
	public void setRequestToken(Request requestToken) {
		this.requestToken = requestToken;
	}

	public Access getAccessToken() {
		return accessToken;
	}

	@XmlElement(name="Access")
	public void setAccessToken(Access accessToken) {
		this.accessToken = accessToken;
	}

	public Token getToken() {
		return token;
	}

	@XmlElement(name="Token")
	public void setToken(Token token) {
		this.token = token;
	}
	
	
	public Authorization getAuthorization() {
		return authorization;
	}
	
	@XmlElement(name="Authorization")
	public void setAuthorization(Authorization authorization) {
		this.authorization = authorization;
	}
}
