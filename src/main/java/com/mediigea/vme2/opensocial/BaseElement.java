package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public abstract class BaseElement {

	public static final String NAME = "name";
	public static final String VALUE = "value";
	
	
	private String name;
	private String value;

	
	public BaseElement() {
		
		super();
	}

	public BaseElement(String name, String value) {
		
		super();
		
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	@XmlAttribute
	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	@XmlValue
	public void setValue(String value) {
		this.value = value;
	}
}
