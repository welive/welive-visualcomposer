package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="msg")
public class Message extends BaseElement {

	public static final String MESSAGE = "msg";

	
	public Message() {
		
		super();
	}
	
	public Message(String name, String value) {
		
		super(name, value);
	}
}
