package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Preload {

	public static final String PRELOAD = "Preload";
	
	public static final String HREF = "href";
	public static final String AUTHZ = "authz";
	public static final String SIGN_OWNER = "sign_owner";
	public static final String SIGN_VIEWR = "sign_viewer";
	public static final String VIEWS = "views";
	
	
	private String href;
	private String authz;
	private String sign_owner;
	private String sign_viewer;
	private String views;
	
	
	public Preload() {
		
		super();
	}

	public Preload(String href, String authz, String sign_owner,
			String sign_viewer, String views) {
		
		super();
		
		this.href = href;
		this.authz = authz;
		this.sign_owner = sign_owner;
		this.sign_viewer = sign_viewer;
		this.views = views;
	}

	public String getHref() {
		return href;
	}

	@XmlAttribute
	public void setHref(String href) {
		this.href = href;
	}

	public String getAuthz() {
		return authz;
	}

	@XmlAttribute
	public void setAuthz(String authz) {
		this.authz = authz;
	}

	public String getSign_owner() {
		return sign_owner;
	}

	@XmlAttribute
	public void setSign_owner(String sign_owner) {
		this.sign_owner = sign_owner;
	}

	public String getSign_viewer() {
		return sign_viewer;
	}

	@XmlAttribute
	public void setSign_viewer(String sign_viewer) {
		this.sign_viewer = sign_viewer;
	}

	public String getViews() {
		return views;
	}

	@XmlAttribute
	public void setViews(String views) {
		this.views = views;
	}	
}
