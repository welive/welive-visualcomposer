package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Authorization extends HttpElement {

	public static final String AUTHORIZATION = "Authorization";
	
	
	public Authorization() {
		
		super();
	}
	
	public Authorization(String url, String method, String param_location) {
		
		super(url, method, param_location);
	}
}
