package com.mediigea.vme2.opensocial;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
<ModulePrefs>
  <Locale lang="en" country="us" />
  <Locale lang="ja" country="jp" />
</ModulePrefs>
*/

@XmlRootElement
public class Locale {

	public static final String LOCALE = "Locale";
	
	public static final String LANG = "lang";
	public static final String COUNTRY = "country";
	public static final String MESSAGES = "messages";
	public static final String LANGUAGE_DIRECTION = "language_direction";
	public static final String VIEWS = "views";
	
	
	private String lang;
	private String country;
	private String language_direction;
	private String views;
	private ArrayList<Message> messages;
	
	
	public Locale() {
		
		super();
	}

	public Locale(String lang, String country, ArrayList<Message> messages,
			String language_direction, String views) {
		
		super();
		
		this.lang = lang;
		this.country = country;
		this.messages = messages;
		this.language_direction = language_direction;
		this.views = views;
	}

	public String getLang() {
		return lang;
	}

	@XmlAttribute
	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getCountry() {
		return country;
	}

	@XmlAttribute
	public void setCountry(String country) {
		this.country = country;
	}

	public void addMessage(Message msg){
		
		if(msg!=null){
		
			if(messages==null)
				messages = new ArrayList<Message>();
			
			messages.add(msg);
		}	
	}
	
	public ArrayList<Message> getMessages() {
		return messages;
	}

	@XmlElement(name="msg", type=com.mediigea.vme2.opensocial.Message.class)
	public void setMessages(ArrayList<Message> messages) {
		this.messages = messages;
	}

	public String getLanguage_direction() {
		return language_direction;
	}

	@XmlAttribute
	public void setLanguage_direction(String language_direction) {
		this.language_direction = language_direction;
	}

	public String getViews() {
		return views;
	}

	@XmlAttribute
	public void setViews(String views) {
		this.views = views;
	}
}
