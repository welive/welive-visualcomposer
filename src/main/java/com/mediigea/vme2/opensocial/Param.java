package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Param extends BaseElement {

	public static final String PARAM = "Param";

	
	public Param() {
		
		super();
	}
	
	public Param(String name, String value) {
		
		super(name, value);
	}
}
