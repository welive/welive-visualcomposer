package com.mediigea.vme2.opensocial;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Module {

	public static final String MODULE = "Module";

	public static final String SPECIFICATION_VERSION = "specificationVersion";
	public static final String SPECIFICATION_DEFAULT_VALUE = "1.0";
	
	
	private String specificationVersion;
	private ModulePrefs modulePrefs;
	private List<UserPref> userPref;
	
	private List<Content> contents;
	
	
	public Module() {
		
		super();
		
		specificationVersion = Module.SPECIFICATION_DEFAULT_VALUE;
	}
	
	public Module(String specificationVersion, ModulePrefs modulePrefs,
			List<UserPref> userPref, ArrayList<Content> contents) {
		
		super();
		
		if(specificationVersion==null || specificationVersion.isEmpty())
			this.specificationVersion = specificationVersion;
		else
			this.specificationVersion = Module.SPECIFICATION_DEFAULT_VALUE;
		
		this.modulePrefs = modulePrefs;
		this.userPref = userPref;
		this.contents = contents;
	}
	
	public String getSpecificationVersion() {
		return specificationVersion;
	}
	
	@XmlAttribute
	public void setSpecificationVersion(String specificationVersion) {
		this.specificationVersion = specificationVersion;
	}
	
	public ModulePrefs getModulePrefs() {
		return modulePrefs;
	}
	
	@XmlElement(name="ModulePrefs")
	public void setModulePrefs(ModulePrefs modulePrefs) {
		this.modulePrefs = modulePrefs;
	}
	
	public List<UserPref> getUserPref() {
		return userPref;
	}
	
	@XmlElement(name="UserPref", type=com.mediigea.vme2.opensocial.UserPref.class)
	public void setUserPref(List<UserPref> userPref) {
		this.userPref = userPref;
	}
	
	public List<Content> getContents() {
		return contents;
	}
	
	@XmlElement(name="Content", type=com.mediigea.vme2.opensocial.Content.class)
	public void setContents(List<Content> contents) {
		this.contents = contents;
	}
	
	public void addContent(Content content) {
		
		if(content!=null){
		
			if(this.contents==null)
				this.contents = new ArrayList<Content>();
			
			this.contents.add(content);
		}
	}
}
