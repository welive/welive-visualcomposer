package com.mediigea.vme2.opensocial;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ModulePrefs {
	
	public static final String MODULE_PREFS = "ModulePrefs";
	
	public static final String TITLE = "title";
	public static final String TITLE_URL = "title_url";
	public static final String DESCRIPTION = "description";
	public static final String AUTHOR = "author";
	public static final String AUTHOR_EMAIL = "author_email";
	public static final String THUMBNAIL = "thumbnail";
	public static final String HEIGHT = "height";
	public static final String WIDTH = "width";
	public static final String DOCTYPE = "doctype";
	
	
	private String title;
	private String title_url;
	private String description;
	private String author;
	private String author_email;
	private String height;
	private String width;
	private String doctype;

	
	private Link screenshotLink;
	private Link thumbnailLink;
	private Link iconLink;
	private Link mediumIconLink;
	private Link largeIconLink;

	private ArrayList<Locale> locales;
	private ArrayList<Optional> optionals;
	private ArrayList<Require> requires;
	private ArrayList<Link> links;

	
	private Preload preload;
	
	private OAuth oauth;
	private OAuth2 oaut2;

	@Deprecated
	private String screenshot;
	
	@Deprecated
	private String thumbnail;
	
	@Deprecated
	private Icon icon;
	
	@Deprecated
	private Icon mediumIcon;
	
	@Deprecated
	private Icon largeIcon;
	
	
	public ModulePrefs() {
		
		super();
	}
	
	public ModulePrefs(String title, String title_url, String description,
			String author, String author_email, String height, String width,
			String doctype, ArrayList<Locale> locales, ArrayList<Optional> optionals,
			ArrayList<Require> requests, ArrayList<Link> links,
			Preload preoload, OAuth oauth, OAuth2 oaut2, String screenshot,
			String thumbnail, Icon icon, Icon mediumIcon, Icon largeIcon) {
		
		super();
		
		this.title = title;
		this.title_url = title_url;
		this.description = description;
		this.author = author;
		this.author_email = author_email;
		this.height = height;
		this.width = width;
		this.doctype = doctype;
		this.locales = locales;
		this.optionals = optionals;
		this.requires = requests;
		this.links = links;
		this.preload = preoload;
		this.oauth = oauth;
		this.oaut2 = oaut2;
		
		this.screenshot = screenshot;
		this.thumbnail = thumbnail;
		this.icon = icon;
		this.mediumIcon = mediumIcon;
		this.largeIcon = largeIcon;
	}

	public ModulePrefs(String title, String title_url, String description,
			String author, String author_email, String height, String width,
			String doctype, Link screenshotLink, Link thumbnailLink,
			Link iconLink, Link mediumIconLink, Link largeIconLink,
			ArrayList<Locale> locales, ArrayList<Optional> optionals,
			ArrayList<Require> requests, ArrayList<Link> links,
			Preload preoload, OAuth oauth, OAuth2 oaut2) {
		
		super();
		
		this.title = title;
		this.title_url = title_url;
		this.description = description;
		this.author = author;
		this.author_email = author_email;
		this.height = height;
		this.width = width;
		this.doctype = doctype;
		this.screenshotLink = screenshotLink;
		this.thumbnailLink = thumbnailLink;
		this.iconLink = iconLink;
		this.mediumIconLink = mediumIconLink;
		this.largeIconLink = largeIconLink;
		this.locales = locales;
		this.optionals = optionals;
		this.requires = requests;
		this.links = links;
		this.preload = preoload;
		this.oauth = oauth;
		this.oaut2 = oaut2;
	}

	public String getTitle() {
		return title;
	}

	@XmlAttribute
	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle_url() {
		return title_url;
	}

	@XmlAttribute
	public void setTitle_url(String title_url) {
		this.title_url = title_url;
	}

	public String getDescription() {
		return description;
	}

	@XmlAttribute
	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	@XmlAttribute
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthor_email() {
		return author_email;
	}

	@XmlAttribute
	public void setAuthor_email(String author_email) {
		this.author_email = author_email;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	@XmlAttribute
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getHeight() {
		return height;
	}

	@XmlAttribute
	public void setHeight(String height) {
		this.height = height;
	}

	public String getWidth() {
		return width;
	}

	@XmlAttribute
	public void setWidth(String width) {
		this.width = width;
	}

	public String getDoctype() {
		return doctype;
	}

	@XmlAttribute
	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}

	public String getScreenshot() {
		return screenshot;
	}

	@XmlAttribute
	public void setScreenshot(String screenshot) {
		this.screenshot = screenshot;
	}

	public Link getScreenshotLink() {
		return screenshotLink;
	}
	
	@XmlElement(name="screenshot")
	public void setScreenshotLink(Link screenshotLink) {
		this.screenshotLink = screenshotLink;
	}

	public Link getThumbnailLink() {
		return thumbnailLink;
	}

	@XmlElement(name="thumbnail")
	public void setThumbnailLink(Link thumbnailLink) {
		this.thumbnailLink = thumbnailLink;
	}

	public Link getIconLink() {
		return iconLink;
	}
	
	@XmlElement(name="icon")
	public void setIconLink(Link iconLink) {
		this.iconLink = iconLink;
	}

	public Link getMediumIconLink() {
		return mediumIconLink;
	}

	@XmlElement(name="mediumIcon")
	public void setMediumIconLink(Link mediumIconLink) {
		this.mediumIconLink = mediumIconLink;
	}

	public Link getLargeIconLink() {
		return largeIconLink;
	}

	@XmlElement(name="largeIcon")
	public void setLargeIconLink(Link largeIconLink) {
		this.largeIconLink = largeIconLink;
	}

	public Preload getPreload() {
		return preload;
	}

	@XmlElement(name="Preload")
	public void setPreload(Preload preoload) {
		this.preload = preoload;
	}

	public OAuth getOauth() {
		return oauth;
	}

	@XmlElement(name="Oauth")
	public void setOauth(OAuth oauth) {
		this.oauth = oauth;
	}

	public OAuth2 getOaut2() {
		return oaut2;
	}

	@XmlElement(name="Oauth2")
	public void setOaut2(OAuth2 oaut2) {
		this.oaut2 = oaut2;
	}

	public Icon getIcon() {
		return icon;
	}

	@XmlElement(name="Icon")
	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	public Icon getMediumIcon() {
		return mediumIcon;
	}

	@XmlElement(name="MediumIcon")
	public void setMediumIcon(Icon mediumIcon) {
		this.mediumIcon = mediumIcon;
	}

	public Icon getLargeIcon() {
		return largeIcon;
	}

	@XmlElement(name="LargeIcon")
	public void setLargeIcon(Icon largeIcon) {
		this.largeIcon = largeIcon;
	}

	public ArrayList<Optional> getOptionals() {
		return optionals;
	}

	@XmlElement(name="Optional", type=com.mediigea.vme2.opensocial.Optional.class)
	public void setOptionals(ArrayList<Optional> optionals) {
		this.optionals = optionals;
	}

	public ArrayList<Require> getRequires() {
		return requires;
	}

	@XmlElement(name="Require", type=com.mediigea.vme2.opensocial.Require.class)
	public void setRequires(ArrayList<Require> requests) {
		this.requires = requests;
	}
	
	public void addRequire(Require require) {
		
		if(require!=null){
			
			if(requires==null)
				requires = new ArrayList<Require>();
			
			requires.add(require);
		}
	}

	public ArrayList<Locale> getLocales() {
		return locales;
	}

	@XmlElement(name="Locale", type=com.mediigea.vme2.opensocial.Locale.class)
	public void setLocales(ArrayList<Locale> locales) {
		this.locales = locales;
	}
	
	public void addLocale(Locale locale) {
		
		if(locale!=null){
			
			if(locales==null)
				locales = new ArrayList<Locale>();
			
			locales.add(locale);
		}
	}

	public ArrayList<Link> getLinks() {
		return links;
	}

	@XmlElement(name="Link", type=com.mediigea.vme2.opensocial.Link.class)
	public void setLinks(ArrayList<Link> links) {
		this.links = links;
	}
	
	public void addLink(Link link) {
		
		if(link!=null){
			
			if(links==null)
				links = new ArrayList<Link>();
			
			links.add(link);
		}
	}
}
