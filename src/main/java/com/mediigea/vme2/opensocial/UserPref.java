package com.mediigea.vme2.opensocial;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
<UserPref name="difficulty" 
     display_name="Difficulty"
     datatype="enum"
     default_value="4">
  <EnumValue value="3" display_value="Easy"/>
  <EnumValue value="4" display_value="Medium"/>
  <EnumValue value="5" display_value="Hard"/>
</UserPref> 
*/

@XmlRootElement
public class UserPref {

	public static final String USER_PREF = "UserPref";
	
	public static final String NAME = "name";
	public static final String DATATYPE = "datatype";
	public static final String DISPLAY_NAME = "display_name";
	public static final String DEFAULT_VALUE = "default_value";
	public static final String REQUIRED = "required";
	
	public static final String REQUIRED_TRUE = "true";
	public static final String REQUIRED_FALSE = "false";
	
	public static final String DATATYPE_STRING_TYPE = "string";
	public static final String DATATYPE_BOOL_TYPE = "bool";
	public static final String DATATYPE_ENUM_TYPE = "enum";
	public static final String DATATYPE_HIDDEN_TYPE = "hidden";
	public static final String DATATYPE_LIST_TYPE = "list";
	
	
	private String name;
	private String datatype;
	private String display_name;
	private String default_value;
	private String required;
	
	private ArrayList<EnumValue> enumValues;
	
	
	public String getName() {
		return name;
	}

	@XmlAttribute
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDatatype() {
		return datatype;
	}

	@XmlAttribute
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	
	public String getDisplay_name() {
		return display_name;
	}

	@XmlAttribute
	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}
	
	public String getDefault_value() {
		return default_value;
	}

	@XmlAttribute
	public void setDefault_value(String default_value) {
		this.default_value = default_value;
	}
	
	public String getRequired() {
		return required;
	}

	@XmlAttribute
	public void setRequired(String required) {
		this.required = required;
	}

	public ArrayList<EnumValue> getEnumValues() {
		return enumValues;
	}
	
	@XmlElement(name="EnumValue", type=com.mediigea.vme2.opensocial.EnumValue.class)
	public void setEnumValues(ArrayList<EnumValue> enumValues) {
		this.enumValues = enumValues;
	}
	
	public void addEnumValue(EnumValue enumValue) {
		
		if(enumValue!=null){
			
			if(enumValues==null)
				enumValues = new ArrayList<EnumValue>();
			
			enumValues.add(enumValue);
		}
	}
}
