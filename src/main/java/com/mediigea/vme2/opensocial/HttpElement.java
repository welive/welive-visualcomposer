package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlAttribute;

public abstract class HttpElement {
	
	public static final String URL = "url";
	public static final String METHOD = "method";
	public static final String PARAM_LOCATION = "param_location";
	
	
	private String url;
	private String method;
	private String param_location;
	
	
	public HttpElement() {
		
		super();
	}
	
	public HttpElement(String url, String method, String param_location) {
		
		super();
		
		this.url = url;
		this.method = method;
		this.param_location = param_location;
	}
	
	public String getUrl() {
		return url;
	}

	@XmlAttribute
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getMethod() {
		return method;
	}

	@XmlAttribute
	public void setMethod(String method) {
		this.method = method;
	}
	
	public String getParam_location() {
		return param_location;
	}

	@XmlAttribute
	public void setParam_location(String param_location) {
		this.param_location = param_location;
	}
}
