package com.mediigea.vme2.opensocial;

import java.util.ArrayList;

public class Require extends Constraint {

	public static final String REQUIRE = "Require";
	
	
	public Require() {
		
		super();
	}

	public Require(String feature, String version, String views, ArrayList<Param> params) {
		
		super(feature, version, views, params);
	}
}
