package com.mediigea.vme2.opensocial;

public class Request extends HttpElement {

	public static final String REQUEST = "Request";
	
	
	public Request() {
		
		super();
	}
	
	public Request(String url, String method, String param_location) {
		
		super(url, method, param_location);
	}
}
