package com.mediigea.vme2.opensocial;

import java.util.ArrayList;

public class Optional extends Constraint {

	public static final String OPTIONAL = "Optional";
	
	
	public Optional() {
		
		super();
	}

	public Optional(String feature, String version, String views, ArrayList<Param> params) {
		
		super(feature, version, views, params);
	}
}
