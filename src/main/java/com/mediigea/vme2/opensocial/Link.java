package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Link {


	private String rel;
	private String href;
	
	
	public Link() {
		
		super();
	}

	public Link(String rel, String href) {
		
		super();
		
		this.rel = rel;
		this.href = href;
	}

	public String getRel() {
		return rel;
	}

	@XmlAttribute
	public void setRel(String rel) {
		this.rel = rel;
	}

	public String getHref() {
		return href;
	}

	@XmlAttribute
	public void setHref(String href) {
		this.href = href;
	}
}
