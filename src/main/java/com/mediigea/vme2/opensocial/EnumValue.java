package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/*
<UserPref name="difficulty" 
     display_name="Difficulty"
     datatype="enum"
     default_value="4">
  <EnumValue value="3" display_value="Easy"/>
  <EnumValue value="4" display_value="Medium"/>
  <EnumValue value="5" display_value="Hard"/>
</UserPref> 
*/

@XmlRootElement
public class EnumValue {

	public static final String ENUM_VALUE = "EnumValue";

	public static final String VALUE = "value";
	public static final String DISPLAY_VALUE = "display_value";
	
	
	private String value;
	private String display_value;
	
	
	public EnumValue() {
		
		super();
	}
	
	public EnumValue(String value, String display_value) {
		
		super();
		
		this.value = value;
		this.display_value = display_value;
	}

	public String getValue() {
		return value;
	}

	@XmlAttribute
	public void setValue(String value) {
		this.value = value;
	}

	public String getDisplay_value() {
		return display_value;
	}

	@XmlAttribute
	public void setDisplay_value(String display_value) {
		this.display_value = display_value;
	}
}
