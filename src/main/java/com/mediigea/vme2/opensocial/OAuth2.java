package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
<ModulePrefs title="OAuth Contacts" scrolling="true">
  <Require feature="locked-domain"/>
  <OAuth>
    <Service name="google">
      <Access url="https://www.google.com/accounts/OAuthGetAccessToken" method="GET" />
      <Request url="https://www.google.com/accounts/OAuthGetRequestToken?scope=http://www.google.com/m8/feeds/" method="GET" />
      <Authorization url="https://www.google.com/accounts/OAuthAuthorizeToken?oauth_callback=http://oauth.gmodules.com/gadgets/oauthcallback" />
    </Service>
  </OAuth>
</ModulePrefs>
*/

@XmlRootElement
public class OAuth2 {

	public static final String OAUTH2 = "OAuth2";

	private Service service;

	
	public OAuth2() {
		
		super();
	}

	public OAuth2(Service service) {
		
		super();
		
		this.service = service;
	}

	public Service getService() {
		return service;
	}

	@XmlElement(name="Service")
	public void setService(Service service) {
		this.service = service;
	}
}
