package com.mediigea.vme2.opensocial;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public abstract class Constraint {

	public static final String FEATURE = "feature";
	public static final String VERSION = "version";
	public static final String VIEWS = "views";
	
	
	private String feature;
	private String version;
	private String views;
	
	private ArrayList<Param> params;
	
	
	public Constraint() {
		
		super();
	}
	
	public Constraint(String feature, String version, String views, ArrayList<Param> params) {
		
		super();
		
		this.feature = feature;
		this.version = version;
		this.views = views;
		this.params = params;
	}

	public String getFeature() {
		return feature;
	}

	@XmlAttribute
	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getVersion() {
		return version;
	}

	@XmlAttribute
	public void setVersion(String version) {
		this.version = version;
	}

	public String getViews() {
		return views;
	}

	@XmlAttribute
	public void setViews(String views) {
		this.views = views;
	}

	public ArrayList<Param> getParams() {
		return params;
	}
	
	@XmlElement(name="Param", type=com.mediigea.vme2.opensocial.Param.class)
	public void setParams(ArrayList<Param> params) {
		this.params = params;
	}
}
