package com.mediigea.vme2.opensocial;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Access extends HttpElement {

	public static final String ACCESS = "Access";
	
	
	public Access() {
		
		super();
	}
	
	public Access(String url, String method, String param_location) {
		
		super(url, method, param_location);
	}
}
