package com.mediigea.vme2.parser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mediigea.vme2.soap.SoapMethod;
import com.mediigea.vme2.soap.SoapPackage;
import com.mediigea.vme2.soap.SoapParameter;
import com.predic8.schema.ComplexType;
import com.predic8.schema.Element;
import com.predic8.schema.Schema;
import com.predic8.schema.Sequence;
import com.predic8.schema.SimpleType;
import com.predic8.schema.restriction.BaseRestriction;
import com.predic8.schema.restriction.facet.EnumerationFacet;
import com.predic8.soamodel.Consts;
import com.predic8.wsdl.Binding;
import com.predic8.wsdl.Definitions;
import com.predic8.wsdl.Message;
import com.predic8.wsdl.Operation;
import com.predic8.wsdl.Part;
import com.predic8.wsdl.Port;
import com.predic8.wsdl.PortType;
import com.predic8.wsdl.Service;
import com.predic8.wsdl.WSDLParser;

public class WsdlParser {

	public static final Logger logger = LoggerFactory.getLogger(WsdlParser.class);

	public static final String MESSAGE_DIRECTION_INPUT = "input";
	public static final String MESSAGE_DIRECTION_OUTPUT = "output";

	public static final String PARAM_TYPE_COMPLEX = "complex";
	public static final String PARAM_TYPE_SIMPLE = "simple";
	public static final String PARAM_LABEL = "label";
	public static final String PARAM_NAME = "name";
	public static final String PARAM_VALUE = "value";
	public static final String PARAM_REQUIRED = "required";
	public static final String PARAM_TYPE = "type";
	public static final String PARAM_XPATH = "xpath";
	public static final String PARAM_REF = "ref";

	private HashMap<String, List<SoapParameter>> parameters = null;

	private WSDLParser parser;
	private Definitions defs;

	public WsdlParser() {
		parser = new WSDLParser();
	}

	/**
	 * 
	 * @param message
	 * @param type
	 */
	private void parseMessageParts(String operationName, Message message, String direction) {
		logger.debug("parseMessageParts");
		parameters = new HashMap<String, List<SoapParameter>>();

		if (message.getParts().size() == 1) {
			logger.debug("Get element");
			Part part = message.getParts().get(0);
			Element element = part.getElement();

			if (element != null) {
				parseElement(direction, element, "/" + element.getName());
			} else {
				parseParts(direction, message.getParts(), "/" + operationName);
			}
		} else {
			parseParts(direction, message.getParts(), "/" + operationName);
		}

	}

	/**
	 * 
	 * @param Key
	 * @param parts
	 * 
	 */
	private void parseParts(String Key, List<Part> parts, String xpath) {

		List<SoapParameter> list = new ArrayList<SoapParameter>();
		for (Part part : parts) {
			SoapParameter param = new SoapParameter();
			param.setName(part.getName());
			param.setLabel(part.getName());
			param.setXpath(xpath);
			param.setMinOccurs("1");
			param.setMaxOccurs("1");
			if (part.getType().getNamespaceURI() == Consts.SCHEMA_NS) {
				param.setType(part.getType().getLocalPart());
			} else {
				logger.info("part type not primitive");
				Schema schema = defs.getSchema(part.getType().getNamespaceURI());

				SimpleType simpleType = null;

				try {
					simpleType = schema.getSimpleType(part.getType().getLocalPart());
				} catch (Exception ex) {
					logger.debug("simpleType null");
				}

				if (simpleType != null) {

					BaseRestriction restr = simpleType.getRestriction();
					if (restr != null) {
						param.setType(restr.getBase().getLocalPart());
						List<EnumerationFacet> enums = restr.getEnumerationFacets();
						if (enums != null) {
							Map<String, String> enumMap = new HashMap<String, String>();
							for (EnumerationFacet ef : enums) {
								enumMap.put(ef.getValue(), "");
							}
							param.setEnumerations(enumMap);
						}
					}

				}
			}

			list.add(param);

		}
		parameters.put(Key, list);
	}

	/***
	 * 
	 * @param Key
	 * @param element
	 * @param xpath
	 */
	private void parseElement(String Key, Element element, String xpath) {
		List<SoapParameter> list = new ArrayList<SoapParameter>();

		try {
			ComplexType complexType = (ComplexType) element.getEmbeddedType();
			if (complexType == null) {
				try {
					complexType = element.getSchema().getComplexType(element.getType().getLocalPart());
				} catch (Exception e) {
					logger.debug("Complex type null");
				}
			}
			if (complexType != null) {
				Sequence seq = complexType.getSequence();
				logger.info("Verify sequence");
				if (seq != null) {
					logger.info("Fetch sequence");
					for (Element e : complexType.getSequence().getElements()) {
						if (e.getType() != null) {
							SoapParameter param = new SoapParameter();
							param.setName(e.getName());
							param.setLabel(e.getName());
							if (e.getType().getNamespaceURI() == Consts.SCHEMA_NS) {
								param.setType(e.getType().getLocalPart());
								param.setXpath(xpath);
								param.setMinOccurs(e.getMinOccurs());
								param.setMaxOccurs(e.getMaxOccurs());
							} else {
								param.setMinOccurs(e.getMinOccurs());
								param.setMaxOccurs(e.getMaxOccurs());
								param.setXpath(xpath);
								SimpleType simpleType = null;

								try {
									simpleType = e.getSchema().getSimpleType(e.getType().getLocalPart());
								} catch (Exception ex) {
									logger.debug("simpleType null");
								}
								if (simpleType != null) {

									BaseRestriction restr = simpleType.getRestriction();
									if (restr != null) {
										param.setType(restr.getBase().getLocalPart());
										List<EnumerationFacet> enums = restr.getEnumerationFacets();
										if (enums != null) {
											Map<String, String> enumMap = new HashMap<String, String>();
											for (EnumerationFacet ef : enums) {
												enumMap.put(ef.getValue(), "");
											}
											param.setEnumerations(enumMap);
										}
									}

								} else {

									param.setType(PARAM_TYPE_COMPLEX);
									param.setXpath(xpath + "/" + e.getName());
									param.setRef(e.getType().getLocalPart());

									//
									parseElement(e.getType().getLocalPart(), e, xpath + "/" + e.getName());

								}

							}
							list.add(param);
						}
					}

				}
			}

		} catch (Exception gnEx) {
			logger.error("ERROR: " + gnEx.getMessage());
			gnEx.printStackTrace();
		}
		parameters.put(Key, list);

	}

	/**
	 * Crea struttura DTO
	 */
	private SoapPackage createPackage() {
		// TODO: parse documentation
		SoapPackage servicePackage = new SoapPackage();

		Service service = defs.getServices().get(0);
		servicePackage.setName(service.getName());

		logger.debug("Fetch Binding Elements");

		Port port = null;
		for (Port pp : service.getPorts()) {
			if (pp.getBinding().getProtocol().equals("SOAP11")) {
				port = pp;
				break;
			}
		}

		if (port != null) {
			logger.debug("SOAP11 ok");

			String endPoint = port.getAddress().getLocation();
			Binding binding = port.getBinding();
			PortType portType = binding.getPortType();

			logger.debug("Verify portType");

			if (portType != null) {
				logger.debug("PortType ok");

				ArrayList<SoapMethod> soapMethodList = new ArrayList<SoapMethod>();
				SoapMethod soapMethod = null;
				for (Operation op : portType.getOperations()) {
					logger.debug("Create operation");
					soapMethod = new SoapMethod();
					soapMethod.setBindingName(binding.getName());
					soapMethod.setPortTypeName(portType.getName());
					soapMethod.setEndPoint(endPoint);
					soapMethod.setName(op.getName());
					soapMethod.setSoapAction(binding.getOperation(op.getName()).getOperation().getSoapAction());

					parseMessageParts(op.getName(), op.getInput().getMessage(), MESSAGE_DIRECTION_INPUT);
					soapMethod.setInputParams(parameters);

					parseMessageParts(op.getName(), op.getOutput().getMessage(), MESSAGE_DIRECTION_OUTPUT);
					soapMethod.setOutputParams(parameters);

					soapMethodList.add(soapMethod);
				}
				servicePackage.setOperations(soapMethodList);
			}
		}

		return servicePackage;
	}

	/***
	 * Parse wsdl from url and create DTO structure
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public SoapPackage parse(String url) throws Exception {
		logger.debug("parsing");
		if (url == null || url.isEmpty())
			throw new Exception("Invalid URL");
		logger.debug("Init parser");
		defs = parser.parse(url);
		return (defs != null) ? createPackage() : null;
	}

	/***
	 * Parse wsdl from file and create DTO structure
	 * 
	 * @param bytes
	 * @throws IOException
	 */
	public SoapPackage parse(byte[] bytes) throws IOException {
		logger.debug("parsing");
		if (bytes == null)
			throw new IOException("Invalid file");

		logger.debug("Init parser");
		InputStream is = new ByteArrayInputStream(bytes);
		try {
			defs = parser.parse(is);
		} finally {
			is.close();
		}
		return (defs != null) ? createPackage() : null;
	}
}
