package com.mediigea.vme2.parser;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.java.dev.wadl.x2009.x02.ApplicationDocument;
import net.java.dev.wadl.x2009.x02.ApplicationDocument.Application;
import net.java.dev.wadl.x2009.x02.DocDocument.Doc;
import net.java.dev.wadl.x2009.x02.MethodDocument.Method;
import net.java.dev.wadl.x2009.x02.OptionDocument.Option;
import net.java.dev.wadl.x2009.x02.ParamDocument.Param;
import net.java.dev.wadl.x2009.x02.ParamStyle.Enum;
import net.java.dev.wadl.x2009.x02.RepresentationDocument.Representation;
import net.java.dev.wadl.x2009.x02.RequestDocument.Request;
import net.java.dev.wadl.x2009.x02.ResourceDocument.Resource;
import net.java.dev.wadl.x2009.x02.ResourcesDocument.Resources;
import net.java.dev.wadl.x2009.x02.ResponseDocument.Response;

import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.XmlException;
import org.w3c.dom.Node;

import wadlExtensions.Attachment;
import wadlExtensions.TagType;
import wadlExtensions.TagsType;

import com.mediigea.vme2.rest.RestApplication;
import com.mediigea.vme2.rest.RestAttachment;
import com.mediigea.vme2.rest.RestDoc;
import com.mediigea.vme2.rest.RestMethod;
import com.mediigea.vme2.rest.RestPackage;
import com.mediigea.vme2.rest.RestParameter;
import com.mediigea.vme2.rest.RestParameter.ParameterStyle;
import com.mediigea.vme2.rest.RestPayload;
import com.mediigea.vme2.rest.RestRepresentation;
import com.mediigea.vme2.rest.RestResource;
import com.mediigea.vme2.rest.RestResponse;

public final class WadlParser {

	private WadlParser() {
	}

	/***
	 * 
	 * @param packageName
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws XmlException
	 */
	public static RestApplication parseFromUrl(final String packageName, final String url) throws IOException, XmlException {

		URL wadlUrl = new URL(url);
		String wadl = IOUtils.toString(wadlUrl.openStream(), "UTF-8");
		return parseWADL(packageName, url, wadl);

	}

	/**
	 * 
	 * @param applicationName
	 * @param wadl
	 * @return
	 * @throws XmlException
	 */
	public static RestApplication parseString(final String applicationName, final String wadl) throws XmlException {
		return parseWADL(applicationName, "", wadl);
	}

	/**
	 * 
	 * @param applicationName
	 * @param url
	 * @param wadl
	 * @return
	 * @throws XmlException
	 */
	private static RestApplication parseWADL(String applicationName, String url, String wadl) throws XmlException {

		ApplicationDocument applicationDocument = ApplicationDocument.Factory.parse(wadl);
		Application application = applicationDocument.getApplication();

		RestApplication restApp = new RestApplication();
		restApp.setName(applicationName);

		List<RestPackage> restPackages = new ArrayList<RestPackage>();

		for (Resources resources : application.getResourcesList()) {
			RestPackage restPack = parseResources(resources);
			restPackages.add(restPack);
		}
		restApp.setPackages(restPackages);

		return restApp;
	}

	/**
	 * 
	 * @param resources
	 * @return
	 */
	private static RestPackage parseResources(Resources resources) {
		String baseURL = resources.getBase();
		if (!baseURL.endsWith("/")) {
			baseURL += "/";
		}

		RestPackage restPack = new RestPackage();
		restPack.setBaseURL(baseURL);
		List<RestResource> restResourceList = new ArrayList<RestResource>();
		for (Resource resource : resources.getResourceList()) {
			restResourceList.addAll(parseResource(resource));
		}
		restPack.setResources(restResourceList);
		return restPack;
	}

	/**
	 * 
	 * @param resource
	 * @return
	 */
	private static List<RestResource> parseResource(final Resource resource) {
		return parseResource(resource, new ArrayList<RestParameter>(), "");
	}

	/**
	 * 
	 * @param resource
	 * @param parentPath
	 * @return
	 */
	private static List<RestResource> parseResource(final Resource resource, List<RestParameter> params, final String parentPath) {
		List<RestResource> result = new ArrayList<RestResource>();

		String path = resource.getPath();
		if (path == null) {
			path = "";
		} else if (!path.startsWith("/") && !parentPath.equals("")) {
			path = "/" + path;
		}
		path = (parentPath + path).replace("//", "/");

		List<RestParameter> parameters = new ArrayList<RestParameter>();
		parameters.addAll(params);

		for (Param param : resource.getParamList()) {
			RestParameter parameter = parseParameter(param);
			// parameter.setStyle(RestParameter.ParameterStyle.TEMPLATE);
			parameters.add(parameter);
		}

		// Resource can contain another Resource
		List<Resource> resourceList = resource.getResourceList();
		if (resourceList != null && resourceList.size() > 0) {
			for (Resource res : resourceList) {
				result.addAll(parseResource(res, parameters, path));
			}
		} else {

			List<Method> methodList = resource.getMethodList();
			if (methodList != null && methodList.size() > 0) {
				for (Method method : methodList) {
					RestResource restResource = parseMethod(method, parameters, path);					
					result.add(restResource);
				}
			}
		}

		return result;
	}

	/**
	 * 
	 * @param method
	 * @param resource
	 * @param path
	 * @return
	 */
	private static RestResource parseMethod(final Method method, final List<RestParameter> params, final String path) {
		List<RestParameter> parameters = new ArrayList<RestParameter>();
		parameters.addAll(params);

		String id = method.getId();
		String displayName = method.getDisplayName();

		RestMethod executeMethod = new RestMethod();
		executeMethod.setUrl(path);
		executeMethod.setHttpMethod(method.getName());

		if (method.getAuthentication() != null) {
			executeMethod.setAuthentication(method.getAuthentication().getRequired());
		}
		if (method.getExample() != null) {
			executeMethod.setExample(method.getExample().getUrl());
		}

		Request request = method.getRequest();
		if (request != null) {

			for (Param param : request.getParamList()) {
				RestParameter parameter = parseParameter(param);
				parameters.add(parameter);
			}

			if (request.getRepresentationList() != null && request.getRepresentationList().size() > 0) {
				List<RestRepresentation> representList = parseRepresentation(request.getRepresentationList());
				executeMethod.setRepresentations(representList);

			}

		}
		executeMethod.setParameters(parameters);

		if (method.getResponseList() != null && method.getResponseList().size() > 0) {
			List<RestResponse> respList = new ArrayList<RestResponse>();
			List<RestParameter> respParameters = new ArrayList<RestParameter>();
			for (Response response : method.getResponseList()) {
				RestResponse restResp = new RestResponse();
				if (response.getStatus() != null) {
					@SuppressWarnings("unchecked")
					List<Long> statusCodes = new ArrayList<Long>(response.getStatus());// IVAN: Integer -> Long (fix 'status' code import error)
					restResp.setStatusCodes(statusCodes);
				}

				for (Param param : response.getParamList()) {
					RestParameter parameter = parseParameter(param);
					respParameters.add(parameter);
				}

				if (response.getRepresentationList() != null && response.getRepresentationList().size() > 0) {
					List<RestRepresentation> representList = parseRepresentation(response.getRepresentationList());
					restResp.setRepresentations(representList);
				}
				
				// IVAN: add docs to response
				if(response.getDocList() != null && response.getDocList().size() > 0)
				{
					List<RestDoc> docs = parseDocs(response.getDocList());
					restResp.setDoc(docs);
				}
				//
				
				respList.add(restResp);
			}

			executeMethod.setResponses(respList);

		}

		RestResource restResource = new RestResource();
		restResource.setName(id);
		restResource.setLabel("".equals(displayName) ? id : displayName);
		restResource.setDoc(parseDocs(method.getDocList()));// IVAN: modified to accept more doc tags
		restResource.setTag(parseTag(method.getTags()));
		restResource.setExecuteMethod(executeMethod);

		return restResource;
	}

	private static String parseTag(TagsType tags) {
		if (tags != null){
			for (TagType tag :tags.getTagList()){
				if (tag.isSetPrimary())
				{
					return tag.getStringValue();
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * @param representations
	 * @return
	 */
	private static List<RestRepresentation> parseRepresentation(List<Representation> representations) {
		List<RestRepresentation> result = new ArrayList<RestRepresentation>();

		for (Representation representation : representations) {
			RestRepresentation restRepresentation = new RestRepresentation();

			// leggo doc
			restRepresentation.setDoc(parseDoc(representation.getDocList()));
			// parametri representation
			if (representation.getParamList() != null && representation.getParamList().size() > 0) {
				List<RestParameter> representParams = new ArrayList<RestParameter>();
				for (Param param : representation.getParamList()) {
					RestParameter parameter = parseParameter(param);
					representParams.add(parameter);
				}
				restRepresentation.setParameters(representParams);

			}
			// mediaType
			if (representation.isSetMediaType()) {
				restRepresentation.setMediaType(representation.getMediaType());
			}
			// specifica attachement
			if (representation.isSetAttachments()) {
				if (representation.getAttachments().getAttachmentList() != null && representation.getAttachments().getAttachmentList().size() > 0) {
					Attachment attach = representation.getAttachments().getAttachmentList().get(0);
					RestAttachment restAtt = new RestAttachment();
					restAtt.setName(attach.getName());
					restAtt.setRequired(attach.getRequired());
					restAtt.setContentDisposition(attach.getContentDisposition());
					restAtt.setContentType(attach.getContentType());
					restAtt.setDoc(parseDoc(attach.getDocList()));

					restRepresentation.setAttachment(restAtt);

				}
			}
			// body payload
			if (representation.isSetPayload()) {
				RestPayload bodyPayload = new RestPayload();
				bodyPayload.setRequired(representation.getPayload().getRequired());
				bodyPayload.setContent(representation.getPayload().getContent());
				restRepresentation.setPayload(bodyPayload);
			}
			result.add(restRepresentation);
		}
		return result;
	}

	/**
	 * 
	 * @param docList
	 * @return
	 */
	private static RestDoc parseDoc(final List<Doc> docList) {
		RestDoc result = null;
		if (docList != null && docList.size() > 0) {
			Doc doc = docList.get(0);
			result = new RestDoc();
			result.setTitle(doc.getTitle());
			result.setUrl(doc.getUrl());
			Node domNode = doc.getDomNode().getFirstChild();
			if (domNode.getNodeType() == Node.TEXT_NODE) {
				result.setText(domNode.getNodeValue());
			}
		}
		return result;
	}
	
	// IVAN
	private static List<RestDoc> parseDocs(final List<Doc> docList) {
		List<RestDoc> result = new ArrayList<RestDoc>();
		Doc doc = null;
		RestDoc rd = null;
		if (docList != null) 
		{
			for(int i = 0; i < docList.size(); i++)
			{
				doc = docList.get(i);
				rd = new RestDoc();
				rd.setTitle(doc.getTitle());
				rd.setUrl(doc.getUrl());
				Node domNode = doc.getDomNode().getFirstChild();
				
				if (domNode.getNodeType() == Node.TEXT_NODE) {
					rd.setText(domNode.getNodeValue());
				}
				
				result.add(rd);
			}
		}
		
		return result;
	}

	/**
	 * 
	 * @param param
	 * @return
	 */
	private static RestParameter parseParameter(final Param param) {
		// Extracting
		String name = param.getName();
		Boolean required = param.getRequired();
		Enum style = param.getStyle();
		String type = param.getType().getLocalPart();
		String fixed = param.getFixed();
		String defaultValue = param.getDefault();
		List<Option> paramOps = param.getOptionList();
		type = convertType(type);

		// Filling
		RestParameter parameter = new RestParameter();
		parameter.setDoc(parseDoc(param.getDocList()));
		parameter.setLabel(name);
		parameter.setName(name);
		parameter.setStyle(ParameterStyle.valueOf(style.toString().toUpperCase()));
		parameter.setType(type);
		parameter.setRequired(required);
		if (fixed != null) {
			parameter.setDefaultValue(fixed);
			parameter.setFixed(true);
		} else {
			parameter.setDefaultValue(defaultValue);
		}
		if (paramOps != null && paramOps.size() > 0) {
			Map<String, String> options = new HashMap<String, String>();
			for (Option op : paramOps) {
				String mt = op.getMediaType() != null ? op.getMediaType() : "";
				options.put(op.getValue(), mt);
			}
			parameter.setOptions(options);
		}
		return parameter;
	}

	/**
	 * 
	 * @param type
	 * @return
	 */
	private static String convertType(final String type) {
		String convertedType = null;
		if ("xsd:int".equals(type)) {
			convertedType = "Integer";
		} else {
			convertedType = "String";
		}
		return convertedType;
	}

}
