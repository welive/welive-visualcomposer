package com.mediigea.vme2.rest;

public class RestPayload {
	private RestDoc doc;
	private String content;
	private boolean required;

	/**
	 * @return the doc
	 */
	public RestDoc getDoc() {
		return doc;
	}

	/**
	 * @param doc
	 *            the doc to set
	 */
	public void setDoc(RestDoc doc) {
		this.doc = doc;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * @param required
	 *            the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}
}
