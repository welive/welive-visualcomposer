package com.mediigea.vme2.rest;

import java.util.List;

public class RestApplication {
	
	private String name;	
	private List<RestPackage> packages;

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<RestPackage> getPackages() {
		return packages;
	}

	public void setPackages(List<RestPackage> packages) {
		this.packages = packages;
	}

}
