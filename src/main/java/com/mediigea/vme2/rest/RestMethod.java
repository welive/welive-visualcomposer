package com.mediigea.vme2.rest;

import java.util.List;

/**
 */
public class RestMethod {

	private String url;
	private String httpMethod;
	private List<RestParameter> parameters;
	private String authentication;
	private RestAuth authInfo;
	private String example;	
	private List<RestRepresentation> representations;	
	private List<RestResponse> responses;

	/**
	 * Gets the URL where is published the service.
	 * 
	 * @return the url the URL where is published the service.
	 */
	public final String getUrl() {
		return url;
	}

	/**
	 * Sets the URL where is published the service.
	 * 
	 * @param url
	 *            the URL where is published the service.
	 */
	public final void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * Gets the parameters accepted by the service.
	 * 
	 * @return the parameters accepted by the service.
	 */
	public final List<RestParameter> getParameters() {
		return parameters;
	}

	/**
	 * Sets the parameters accepted by the service.
	 * 
	 * @param parameters
	 *            the parameters accepted by the service.
	 */
	public final void setParameters(final List<RestParameter> parameters) {
		this.parameters = parameters;
	}

	/**
	 * Sets the HTTP Method for the request.
	 * 
	 * @param httpMethod
	 *            the HTTP method
	 */
	public final void setHttpMethod(final String httpMethod) {
		this.httpMethod = httpMethod;
	}

	/**
	 * Gets the HTTP Method for the request.
	 * 
	 * @return the HTTP Method
	 */
	public final String getHttpMethod() {
		return httpMethod;
	}

	/**
	 * @return the authentication
	 */
	public String getAuthentication() {
		return authentication;
	}

	/**
	 * @param authentication
	 *            the authentication to set
	 */
	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}

	/**
	 * @return the example
	 */
	public String getExample() {
		return example;
	}

	/**
	 * @param example
	 *            the example to set
	 */
	public void setExample(String example) {
		this.example = example;
	}

	
	/**
	 * @return the responses
	 */
	public List<RestResponse> getResponses() {
		return responses;
	}

	/**
	 * @param responses
	 *            the responses to set
	 */
	public void setResponses(List<RestResponse> responses) {
		this.responses = responses;
	}

	/**
	 * @return the representations
	 */
	public List<RestRepresentation> getRepresentations() {
		return representations;
	}

	/**
	 * @param representations the representations to set
	 */
	public void setRepresentations(List<RestRepresentation> representations) {
		this.representations = representations;
	}

	public RestAuth getAuthInfo() {
		return authInfo;
	}

	public void setAuthInfo(RestAuth authInfo) {
		this.authInfo = authInfo;
	}

}
