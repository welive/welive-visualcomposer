package com.mediigea.vme2.rest;

import java.util.List;


public class RestResource {
	private String name;
	private String label;
	private RestMethod executeMethod;
	private List<RestDoc> doc; // IVAN: modified to accept more doc tags
	private String tag;

	/**
	 * 
	 * @return
	 */
	public final String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            the name to set
	 */
	public final void setName(final String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 */
	public final String getLabel() {
		return label;
	}

	/**
	 * Sets the label of the .
	 * 
	 * @param label
	 *            the label of the to set
	 */
	public final void setLabel(final String label) {
		this.label = label;
	}
	
	/**
	 * Returns the execute method of the .
	 * 
	 * @return the execute method of the
	 */
	public final RestMethod getExecuteMethod() {
		return executeMethod;
	}

	/**
	 * Sets the execute method of the .
	 * 
	 * @param executeMethod
	 *            the execute method of the to set
	 */
	public final void setExecuteMethod(final RestMethod executeMethod) {
		this.executeMethod = executeMethod;
	}

	/**
	 * @return the documentation
	 */
	public List<RestDoc> getDoc() {
		return doc;
	}

	/**
	 * @param documentation
	 *            the documentation to set
	 */
	public void setDoc(List<RestDoc> doc) {
		this.doc = doc;
	}
	
	/**
	 * @return the tag
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * @param tag the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}
}
