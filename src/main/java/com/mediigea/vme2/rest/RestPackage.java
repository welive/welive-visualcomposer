package com.mediigea.vme2.rest;

import java.util.List;
import java.util.ArrayList;

/**
 */
public class RestPackage {
	private RestDoc doc;
	private String baseURL;
	private List<RestResource> resources = new ArrayList<RestResource>();

	/**
	 * Gets the base URL the .
	 * 
	 * @return the baseURL
	 */
	public final String getBaseURL() {
		return baseURL;
	}

	/**
	 * Sets the base URL the .
	 * 
	 * @param baseURL
	 *            the baseURL to set
	 */
	public final void setBaseURL(final String baseURL) {
		this.baseURL = baseURL;
	}

	/**
	 * Gets the list of the resources.
	 * 
	 * @return the resources
	 */
	public final List<RestResource> getResources() {
		return resources;
	}

	/**
	 * Sets the list of the resources.
	 * 
	 * @param resources
	 *            the resources to set
	 */
	public final void setResources(final List<RestResource> resources) {
		this.resources = resources;
	}

	/**
	 * @return the doc
	 */
	public RestDoc getDoc() {
		return doc;
	}

	/**
	 * @param doc
	 *            the doc to set
	 */
	public void setDoc(RestDoc doc) {
		this.doc = doc;
	}
}
