package com.mediigea.vme2.rest;

import java.util.List;

public class RestResponse {
	private List<RestDoc> doc; // IVAN: modified to manage more docs
	private List<Long> statusCodes;// IVAN: Integer -> Long (fix 'status' code import error)
	private List<RestRepresentation> representations;
	private List<RestParameter> parameters;
	
	private String schema;

	/**
	 * @return the statusCodes
	 */
	public List<Long> getStatusCodes() {
		return statusCodes;
	}

	/**
	 * @param statusCodes
	 *            the statusCodes to set
	 */
	public void setStatusCodes(List<Long> statusCodes) {
		this.statusCodes = statusCodes;
	}

	/**
	 * @return the parameters
	 */
	public List<RestParameter> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters
	 *            the parameters to set
	 */
	public void setParameters(List<RestParameter> parameters) {
		this.parameters = parameters;
	}

	/**
	 * @return the representations
	 */
	public List<RestRepresentation> getRepresentations() {
		return representations;
	}

	/**
	 * @param representations
	 *            the representations to set
	 */
	public void setRepresentations(List<RestRepresentation> representations) {
		this.representations = representations;
	}

	/**
	 * @return the doc
	 */
	public List<RestDoc> getDoc() {
		return doc;
	}

	/**
	 * @param doc the doc to set
	 */
	public void setDoc(List<RestDoc> doc) {
		this.doc = doc;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

}
