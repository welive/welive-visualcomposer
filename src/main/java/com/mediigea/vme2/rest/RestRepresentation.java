package com.mediigea.vme2.rest;

import java.util.List;

public class RestRepresentation {
	
	private RestPayload payload;
	private RestAttachment attachment;
	private RestDoc doc;
	private List<RestParameter> parameters;
	private String mediaType;
	/**
	 * @return the payload
	 */
	public RestPayload getPayload() {
		return payload;
	}
	/**
	 * @param payload the payload to set
	 */
	public void setPayload(RestPayload payload) {
		this.payload = payload;
	}
	/**
	 * @return the attachment
	 */
	public RestAttachment getAttachment() {
		return attachment;
	}
	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment(RestAttachment attachment) {
		this.attachment = attachment;
	}
	/**
	 * @return the doc
	 */
	public RestDoc getDoc() {
		return doc;
	}
	/**
	 * @param doc the doc to set
	 */
	public void setDoc(RestDoc doc) {
		this.doc = doc;
	}
	/**
	 * @return the parameters
	 */
	public List<RestParameter> getParameters() {
		return parameters;
	}
	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(List<RestParameter> parameters) {
		this.parameters = parameters;
	}
	/**
	 * @return the mediaType
	 */
	public String getMediaType() {
		return mediaType;
	}
	/**
	 * @param mediaType the mediaType to set
	 */
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	
}
