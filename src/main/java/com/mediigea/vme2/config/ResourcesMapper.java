package com.mediigea.vme2.config;

public class ResourcesMapper {

	private String uploadsFolder;
	private String uploadsImageMarketplace;
	private String uploadsMashupMarketplace;
	
	public String getUploadsFolder() {
		return uploadsFolder;
	}

	public void setUploadsFolder(String uploadsFolder) {
		this.uploadsFolder = uploadsFolder;
	}

	public String getUploadsImageMarketplace() {
		return uploadsImageMarketplace;
	}

	public void setUploadsImageMarketplace(String uploadsImageMarketplace) {
		this.uploadsImageMarketplace = uploadsImageMarketplace;
	}

	public String getUploadsMashupMarketplace() {
		return uploadsMashupMarketplace;
	}

	public void setUploadsMashupMarketplace(String uploadsMashupMarketplace) {
		this.uploadsMashupMarketplace = uploadsMashupMarketplace;
	}

	
	
}
