package com.mediigea.vme2.config;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ObjectMapperExt extends ObjectMapper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5259935241993408034L;
	
	public ObjectMapperExt(boolean prettyPrint){
		super();
		this.configure(SerializationFeature.INDENT_OUTPUT, prettyPrint);
		this.setSerializationInclusion(Include.NON_NULL);
	}

}
