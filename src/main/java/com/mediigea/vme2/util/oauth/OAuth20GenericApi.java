package com.mediigea.vme2.util.oauth;

import org.scribe.builder.api.DefaultApi20;
import org.scribe.model.OAuthConfig;
import org.scribe.utils.*;

public class OAuth20GenericApi extends DefaultApi20{

	protected String authorize_url;
	protected String scoped_authorize_url;
	protected String access_token_endpoint;

	public OAuth20GenericApi(String authorize_url, String scoped_authorize_url, String access_token_endpoint) {
		super();
		this.authorize_url = authorize_url;
		this.scoped_authorize_url = scoped_authorize_url;
		this.access_token_endpoint = access_token_endpoint;
	}
	@Override
	  public String getAccessTokenEndpoint()
	  {
	    return access_token_endpoint;
	  }

	  @Override
	  public String getAuthorizationUrl(OAuthConfig config)
	  {
	    Preconditions.checkValidUrl(config.getCallback(), "Must provide a valid url as callback. It does not support OOB");

	    // Append scope if present
	    if(config.hasScope())
	    {
	     return String.format(scoped_authorize_url, config.getApiKey(), OAuthEncoder.encode(config.getCallback()), OAuthEncoder.encode(config.getScope()));
	    }
	    else
	    {
	      return String.format(authorize_url, config.getApiKey(), OAuthEncoder.encode(config.getCallback()));
	    }
	  }
}

