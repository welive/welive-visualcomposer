package com.mediigea.vme2.util.oauth;

import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.Token;

public class OAuth10aGenericApi extends DefaultApi10a{

	protected String authorize_url;
	protected String request_token_resource;
	protected String access_token_resource;

	public OAuth10aGenericApi(String authorize_url,
			String request_token_resource, String access_token_resource) {
		super();
		this.authorize_url = authorize_url;
		this.request_token_resource = request_token_resource;
		this.access_token_resource = access_token_resource;
	}

	@Override
	public String getAccessTokenEndpoint()
	{
		return access_token_resource;
	}

	@Override
	public String getRequestTokenEndpoint()
	{
		return request_token_resource;
	}

	@Override
	public String getAuthorizationUrl(Token requestToken)
	{
		return String.format(authorize_url, requestToken.getToken());
	}
/*
	public static class SSL extends OAuth10aGenericApi
	{
		public SSL(String authorize_url, String request_token_resource,
				String access_token_resource) {
			super(authorize_url, request_token_resource, access_token_resource);
			// TODO Auto-generated constructor stub
		}

		@Override
		public String getAccessTokenEndpoint()
		{
			return "https://" + access_token_resource;
		}

		@Override
		public String getRequestTokenEndpoint()
		{
			return "https://" + request_token_resource;
		}
	}
*/
	/**
	 * Twitter 'friendlier' authorization endpoint for OAuth.
	 *
	 * Uses SSL.
	 */
	/*
	public static class Authenticate extends SSL
	{
		public Authenticate(String authorize_url,
				String request_token_resource, String access_token_resource) {
			super(authorize_url, request_token_resource, access_token_resource);
			// TODO Auto-generated constructor stub
		}

		private static final String AUTHENTICATE_URL = "https://api.twitter.com/oauth/authenticate?oauth_token=%s";

		@Override
		public String getAuthorizationUrl(Token requestToken)
		{
			return String.format(AUTHENTICATE_URL, requestToken.getToken());
		}
	}
*/
	/**
	 * Just an alias to the default (SSL) authorization endpoint.
	 *
	 * Need to include this for symmetry with 'Authenticate' only.
	 */
	/*
	public static class Authorize extends SSL{

		public Authorize(String authorize_url, String request_token_resource,
				String access_token_resource) {
			super(authorize_url, request_token_resource, access_token_resource);
			// TODO Auto-generated constructor stub
		}}
		*/
}

