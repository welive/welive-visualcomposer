package com.mediigea.vme2.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class EmailSender {

	private static final Logger logger = LoggerFactory.getLogger(EmailSender.class);

	private MailSender mailSender;	
	private String senderAddress;

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	public void setSenderAddress(String senderAddress){
		this.senderAddress = senderAddress;
	}

	public void sendMail(String to, String subject, String msg) {

		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

		simpleMailMessage.setFrom(this.senderAddress);
		simpleMailMessage.setTo(to);
		simpleMailMessage.setSubject(subject);
		simpleMailMessage.setText(msg);
		logger.info("Sending registration email");
		mailSender.send(simpleMailMessage);
	}

	

}
