package com.mediigea.vme2.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.mediigea.vme2.dto.ConnectionShapeDTO;

public class ConnectionShapeDeserializer extends
		JsonDeserializer<ConnectionShapeDTO> {

	@Override
	public ConnectionShapeDTO deserialize(JsonParser jsonParser,
			DeserializationContext deserializationContext) throws IOException {

		ObjectCodec oc = jsonParser.getCodec();
		JsonNode node = oc.readTree(jsonParser);

		JsonNode id = node.get("id");
		JsonNode type = node.get("type");
		JsonNode label  = node.get("label");
		JsonNode source = node.get("userData").get("sourceId");
		JsonNode target = node.get("userData").get("targetId");
		
		ConnectionShapeDTO connectionShape = new ConnectionShapeDTO();
		
		if(id!=null){ connectionShape.setId(id.asText());}
		if(type!=null){ connectionShape.setType(type.asText());}
		if(label!=null){ connectionShape.setLabel(label.asText());}		
		if(source!=null){ connectionShape.setSourceId(source.asText());}
		if(target!=null){ connectionShape.setTargetId(target.asText());}
		
		return connectionShape;
	}
}
