package com.mediigea.vme2.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.databind.node.ValueNode;


/**
 * Converts between JSON and XML
 * @author 
 */
public final class FormatConverter {

	private static final Logger logger = LoggerFactory.getLogger(FormatConverter.class);

	private FormatConverter() {
	}

	public enum Format {
		JSON("JSON"),
		XML("XML");

		@SuppressWarnings("unused")
		private String format;

		/**
		 * Creates a instance of a format.
		 * @param formatName format the text which indetified the format.
		 */
		Format(final String formatName) {
			this.format = formatName;
		}
	}

	/**
	 * Converts to JSON.
	 * @param message the message to convert
	 * @param originFormat the format of the given message to convert
	 * @return the JSON message
	 */
	public static String toJson(final String message, final Format originFormat) {
		String formattedMessage = null;
		if (originFormat == Format.XML) {
			try {
				formattedMessage = XML.toJSONObject(message).toString();
			} catch (JSONException ex) {
				logger.warn("Error parsing XML", ex);                
				formattedMessage = "{error:'Proxy Error: Error parsing XML'}";
			}
		}
		return formattedMessage;
	}

	/**
	 * Converts Xml to JSON.
	 * @param message the message to convert
	 * @return the JSON message
	 */
	public static String xmlToJson(final String message) {
		return toJson(message, Format.XML);
	}    

	/**
	 * Converts to XML.
	 * @param message the message to convert
	 * @param originFormat the format of the given message to convert
	 * @return the XML message
	 */
	public static String toXML(final String message, final Format originFormat) {
		String formatedMessage = null;
		if (originFormat == Format.JSON) {
			try {
				formatedMessage = XML.toString(new JSONObject(message));
			} catch (JSONException ex) {
				logger.warn("Error parsing XML", ex);
				formatedMessage = "<error>Proxy Error: Error parsing XML<error>";
			}
		}
		return formatedMessage;
	}

	/**
	 * Converts JSON to XML.
	 * @param message the message to convert
	 * @return the XML message
	 */
	public static String jsonToXml(final String message) {
		return toXML(message, Format.JSON);
	}

	public static JsTreeNode jsonToTreeStruct(JsonNode source, String fatherNodeKey) throws JsonParseException, IOException {

		JsTreeNode obj = new JsTreeNode();

		if (source.isArray() || source.isObject()) 
		{
			if (source.isObject()) 
			{												
				Iterator<Entry<String, JsonNode>> nodeIterator = source.fields();
				
				while (nodeIterator.hasNext()) 
				{
					Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodeIterator.next();
					JsonNode tmpNode = entry.getValue();
					obj.getChildren().add(jsonToTreeStruct(tmpNode, fatherNodeKey+"."+entry.getKey()));
				}
			} 
			else
			{
				Iterator<JsonNode> nodeIterator = source.elements();
				int i=0;
				
				while (nodeIterator.hasNext()) 
				{
					JsonNode tmpNode = nodeIterator.next();
					obj.getChildren().add(jsonToTreeStruct(tmpNode, fatherNodeKey+"["+ i++ +"]"));
				}
			}
		} 

		JsTreeNode.Data data = obj.new Data();
		Map<String, String> metadata = new HashMap<String, String>();
		
		if(fatherNodeKey!=null && !"".equals(fatherNodeKey)){		

			String nodename = "";

			if(fatherNodeKey != null && !"".equals(fatherNodeKey)){
				nodename =  fatherNodeKey.substring(fatherNodeKey.lastIndexOf(".")+1);
			}

			String value = source.asText()!=null && !"".equals(source.asText()) ? " : " +source.asText() : "";

			data.setTitle(nodename + value);

			//Map<String, String> metadata = new HashMap<String, String>();
			metadata.put(VmeConstants.JSTREE_METADATAPATH, fatherNodeKey);
			//obj.setMetadata(metadata);								

			//obj.setData(data);	
		}						
		if(source instanceof ValueNode){			
			metadata.put(VmeConstants.JSTREE_DATATYPE, "text");
			data.setIcon("fa icon-file-text");
		} else if(source instanceof ObjectNode){			
			metadata.put(VmeConstants.JSTREE_DATATYPE, "object");
			data.setIcon("fa icon-folder-close");
		} else if(source instanceof ArrayNode){			
			metadata.put(VmeConstants.JSTREE_DATATYPE, "array");
			data.setIcon("fa icon-th-list");
		}
		
		obj.setMetadata(metadata);								

		obj.setData(data);	
		
		return obj;
	}
	
//	public static JsTreeNode jsonToTreeStructKoMeta(JsonNode source, String fatherNodeKey) throws JsonParseException, IOException {
//
//		JsTreeNode obj = new JsTreeNode();
//
//		if (source.isArray() || source.isObject()) {
//			if (source.isObject()) {												
//				Iterator<Entry<String, JsonNode>> nodeIterator = source .fields();
//				while (nodeIterator.hasNext()) {
//					Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodeIterator.next();
//					JsonNode tmpNode = entry.getValue();
//					obj.getChildren().add(jsonToTreeStructKoMeta(tmpNode, fatherNodeKey!=null ? fatherNodeKey+"."+entry.getKey() : entry.getKey()));
//				}
//			} else{
//				Iterator<JsonNode> nodeIterator = source.elements();
//				int i=0;
//				while (nodeIterator.hasNext()) {
//					JsonNode tmpNode = nodeIterator.next();
//					obj.getChildren().add(jsonToTreeStructKoMeta(tmpNode, fatherNodeKey+"()["+ i++ +"]"));
//				}
//			}
//		} 
//				
//		JsTreeNode.Data data = obj.new Data();
//
//		String nodename = "";
//
//		if(fatherNodeKey != null && !"".equals(fatherNodeKey)){
//			nodename =  fatherNodeKey.substring(fatherNodeKey.lastIndexOf(".")+1);
//			//Rimpiazza le parentesi nel caso di array 
//			//es.: types()[0] -> types[0]
//			nodename = nodename.replaceAll("(\\(\\))(\\[[0-9]*\\])", "$2");			
//		}
//
//		String value = source.asText()!=null && !"".equals(source.asText()) ? " : " +source.asText() : "";
//
//		data.setTitle(nodename + value);
//
//		Map<String, String> metadata = new HashMap<String, String>();
//				
//		if(source instanceof TextNode){
//			metadata.put(VmeConstants.JSTREE_METADATAPATH, fatherNodeKey);
//			metadata.put(VmeConstants.JSTREE_DATATYPE, "text");
//		} else if(source instanceof ObjectNode){
//			metadata.put(VmeConstants.JSTREE_METADATAPATH, fatherNodeKey);
//			metadata.put(VmeConstants.JSTREE_DATATYPE, "object");
//		} else if(source instanceof ArrayNode){
//			metadata.put(VmeConstants.JSTREE_METADATAPATH, fatherNodeKey+"()");
//			metadata.put(VmeConstants.JSTREE_DATATYPE, "array");
//		}
//		obj.setMetadata(metadata);								
//
//		obj.setData(data);	
//				
//
//		return obj;
//	}	
	
	public static JsTreeNode jsonToTreeStructKoMeta(JsonNode source, String fatherNodeKey) throws JsonParseException, IOException {

		JsTreeNode obj = new JsTreeNode();

		if (source.isArray() || source.isObject()) {
			if (source.isObject()) {												
				Iterator<Entry<String, JsonNode>> nodeIterator = source .fields();
				while (nodeIterator.hasNext()) {
					Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodeIterator.next();
					JsonNode tmpNode = entry.getValue();
					obj.getChildren().add(jsonToTreeStructKoMeta(tmpNode, fatherNodeKey!=null ? fatherNodeKey+"['"+entry.getKey()+"']" : entry.getKey()));
				}
			} else{
				Iterator<JsonNode> nodeIterator = source.elements();
				int i=0;
				while (nodeIterator.hasNext()) {
					JsonNode tmpNode = nodeIterator.next();
					obj.getChildren().add(jsonToTreeStructKoMeta(tmpNode, fatherNodeKey+"()["+ i++ +"]"));
				}
			}
		} 
				
		JsTreeNode.Data data = obj.new Data();

		String nodename = "";

		if(fatherNodeKey != null && !"".equals(fatherNodeKey)){
			//nodename =  fatherNodeKey.substring(fatherNodeKey.lastIndexOf(".")+1);
			//Rimpiazza le parentesi nel caso di array 
			//es.: types()[0] -> types[0]
			//nodename = nodename.replaceAll("(\\(\\))(\\[[0-9]*\\])", "$2");
			
			Pattern p = Pattern.compile("\\['([^\\[']*)'\\]$|\\[([0-9]*)\\]$");
			Matcher m = p.matcher(fatherNodeKey);
			
			if (m.find()) {
				String stringContent = m.group(1);
				String numberContent = m.group(2);
				nodename = stringContent != null ? stringContent : numberContent;
			} else {
				nodename = fatherNodeKey;
			}
		}

		String value = source.asText()!=null && !"".equals(source.asText()) ? " : " +source.asText() : "";

		data.setTitle(nodename + value);

		Map<String, String> metadata = new HashMap<String, String>();
		
		// IVAN
		// allow binding with numerical or boolean values in the Mockup Editor
		if(source instanceof ValueNode){
			metadata.put(VmeConstants.JSTREE_METADATAPATH, fatherNodeKey);
			metadata.put(VmeConstants.JSTREE_DATATYPE, "text");
			data.setIcon("fa icon-file-text");
		} else if(source instanceof ObjectNode){
			metadata.put(VmeConstants.JSTREE_METADATAPATH, fatherNodeKey);
			metadata.put(VmeConstants.JSTREE_DATATYPE, "object");
			data.setIcon("fa icon-folder-close");
		} else if(source instanceof ArrayNode){
			metadata.put(VmeConstants.JSTREE_METADATAPATH, fatherNodeKey+"()");
			metadata.put(VmeConstants.JSTREE_DATATYPE, "array");
			data.setIcon("fa icon-th-list");
		}
		obj.setMetadata(metadata);								

		obj.setData(data);	
				

		return obj;
	}	
	
	public static String SOAPBodyToString(SOAPBody body) {
		String strBody = null;
		try {
			Document doc = body.extractContentAsDocument();

			Source source = new DOMSource(doc);
			StringWriter stringWriter = new StringWriter();
			Result result = new StreamResult(stringWriter);
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.transform(source, result);
			strBody = stringWriter.getBuffer().toString();

		} catch (SOAPException e) {
			logger.error("SOAPException while parsing soap body to string: " + e.getMessage());
			logger.debug("Stack Trace: ", e);
		} catch (TransformerConfigurationException e) {
			logger.error("TransformerConfigurationException while parsing soap body to string: " + e.getMessage());
			logger.debug("Stack Trace: ", e);
		} catch (TransformerException e) {
			logger.error("TransformerException while parsing soap body to string: " + e.getMessage());
			logger.debug("Stack Trace: ", e);
		}

		return strBody;
	}
	
	public static String SOAPEnvelopeToString(SOAPEnvelope envelope) {
		String strBody = null;
		try {
			Document doc = envelope.getOwnerDocument();

			Source source = new DOMSource(doc);
			StringWriter stringWriter = new StringWriter();
			Result result = new StreamResult(stringWriter);
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.transform(source, result);
			strBody = stringWriter.getBuffer().toString();

		} catch (TransformerConfigurationException e) {
			logger.error("TransformerConfigurationException while parsing soap body to string: " + e.getMessage());
			logger.debug("Stack Trace: ", e);
		} catch (TransformerException e) {
			logger.error("TransformerException while parsing soap body to string: " + e.getMessage());
			logger.debug("Stack Trace: ", e);
		}

		return strBody;
	}	
	
}
