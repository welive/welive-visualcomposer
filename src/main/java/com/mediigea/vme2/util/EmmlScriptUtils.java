package com.mediigea.vme2.util;

import it.eng.PropertyGetter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.RestOperationDAO;
import com.mediigea.vme2.dto.ConnectionShapeDTO;
import com.mediigea.vme2.dto.InnerProcessShapeDTO;
import com.mediigea.vme2.dto.InputField;
import com.mediigea.vme2.dto.OperatorShapeDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.dto.ShapeContainer;
import com.mediigea.vme2.dto.ShapeDTO;
import com.mediigea.vme2.emml.AssignType;
import com.mediigea.vme2.emml.ConstructorType;
import com.mediigea.vme2.emml.DirectInvokeType;
import com.mediigea.vme2.emml.ForEachType;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.ScriptType;
import com.mediigea.vme2.emml.VariableType;
import com.mediigea.vme2.emml.VariablesType;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.service.RestOperationService;
import com.sun.xml.bind.marshaller.CharacterEscapeHandler;

public class EmmlScriptUtils {
	private static final Logger logger = LoggerFactory
			.getLogger(EmmlScriptUtils.class);

	public static Digraph<ShapeDTO> graphBuilder(ShapeContainer shapeContainer) {

		// Create a Graph with Shape nodes
		Digraph<ShapeDTO> graph = new Digraph<ShapeDTO>();

		List<ShapeDTO> connectionShapes = shapeContainer.getConnections();
		
		HashMap<String, ShapeDTO> serviceShapes = shapeContainer.getServices();
		
		HashMap<String, ShapeDTO> inputShapes = shapeContainer.getInputs();
		
		HashMap<String, ShapeDTO> operatorShapes = shapeContainer.getOperators();
		
		HashMap<String, ShapeDTO> blackboxShapes = shapeContainer.getBlackboxes();
		
		HashMap<String, ShapeDTO> innerProcessShapes = shapeContainer.getInnerProcesses();
		
		HashMap<String, ShapeDTO> openDataShapes = shapeContainer.getOpenData();
		
		ShapeDTO outputShape = shapeContainer.getOutput();

		// itera sulla collezione di shape di tipo connection
		Iterator<ShapeDTO> it = connectionShapes.iterator();
		while (it.hasNext()) {

			ConnectionShapeDTO connection = (ConnectionShapeDTO) it.next();

			String connectionType = connection.getType();

			// In base al tipo di connessione aggiunge i nodi al grafo
			if (connectionType.indexOf(VmeConstants.SHAPE_CONNECTION_SERVICE) != -1) {

				// Prende gli id degli shape coinvolti nella connessione
				String sourceId = connection.getSourceId();
				String targetId = connection.getTargetId();

				// Cattura i nodi dalle collezioni selezionate e popola il grafo
				if (serviceShapes.containsKey(sourceId)
						&& serviceShapes.containsKey(targetId)) {

					graph.add(serviceShapes.get(sourceId),
							serviceShapes.get(targetId));

				} else if (serviceShapes.containsKey(sourceId)
						&& operatorShapes.containsKey(targetId)) {

					graph.add(serviceShapes.get(sourceId),
							operatorShapes.get(targetId));

				} else if (serviceShapes.containsKey(sourceId)
						&& blackboxShapes.containsKey(targetId)) {

					graph.add(serviceShapes.get(sourceId),
							blackboxShapes.get(targetId));

				} else if (serviceShapes.containsKey(sourceId)
						&& targetId.equals(outputShape.getId())) {

					graph.add(serviceShapes.get(sourceId), outputShape);

				} else if (!serviceShapes.containsKey(sourceId)) {

					graph.add(serviceShapes.get(targetId));

				} else if (!serviceShapes.containsKey(targetId)) {

					graph.add(serviceShapes.get(sourceId));
				}

			} else if (connectionType
					.indexOf(VmeConstants.SHAPE_CONNECTION_OPERATOR) != -1) {

				// Prende gli id degli shape coinvolti nella connessione
				String sourceId = connection.getSourceId();
				String targetId = connection.getTargetId();
				// Cattura i nodi dalle collezioni selezionate e popola il grafo
				if (operatorShapes.containsKey(sourceId)
						&& operatorShapes.containsKey(targetId)) {

					graph.add(operatorShapes.get(sourceId),
							operatorShapes.get(targetId));

				} else if (operatorShapes.containsKey(sourceId)
						&& serviceShapes.containsKey(targetId)) {

					graph.add(operatorShapes.get(sourceId),
							serviceShapes.get(targetId));

				} else if (operatorShapes.containsKey(sourceId)
						&& blackboxShapes.containsKey(targetId)) {

					graph.add(operatorShapes.get(sourceId),
							blackboxShapes.get(targetId));

				} else if (operatorShapes.containsKey(sourceId)
						&& innerProcessShapes.containsKey(targetId)) {

					//Nel caso di operatore connesso ad inner process inserisce l'inner nello shape operatore
					OperatorShapeDTO oper = (OperatorShapeDTO) operatorShapes.get(sourceId);
					oper.setInnerProcess((InnerProcessShapeDTO) innerProcessShapes.get(targetId));
					//graph.add(operatorShapes.get(sourceId),
					//		innerProcessShapes.get(targetId));					
					
				} else if (operatorShapes.containsKey(sourceId)
						&& targetId.equals(outputShape.getId())) {

					graph.add(operatorShapes.get(sourceId), outputShape);

				} else if (!operatorShapes.containsKey(sourceId)) {

					graph.add(operatorShapes.get(targetId));

				} else if (!operatorShapes.containsKey(targetId)) {

					graph.add(operatorShapes.get(sourceId));
				}
			} else if (connectionType
					.indexOf(VmeConstants.SHAPE_CONNECTION_BLACKBOX) != -1) {

				// Prende gli id degli shape coinvolti nella connessione
				String sourceId = connection.getSourceId();
				String targetId = connection.getTargetId();
				// Cattura i nodi dalle collezioni selezionate e popola il grafo
				if (blackboxShapes.containsKey(sourceId)
						&& operatorShapes.containsKey(targetId)) {

					graph.add(blackboxShapes.get(sourceId),
							operatorShapes.get(targetId));

				} else if (blackboxShapes.containsKey(sourceId)
						&& serviceShapes.containsKey(targetId)) {

					graph.add(blackboxShapes.get(sourceId),
							serviceShapes.get(targetId));

				} else if (blackboxShapes.containsKey(sourceId)
						&& blackboxShapes.containsKey(targetId)) {

					graph.add(blackboxShapes.get(sourceId),
							blackboxShapes.get(targetId));

				} else if (blackboxShapes.containsKey(sourceId)
						&& targetId.equals(outputShape.getId())) {

					graph.add(blackboxShapes.get(sourceId), outputShape);

				} else if (!blackboxShapes.containsKey(sourceId)) {

					graph.add(blackboxShapes.get(targetId));

				} else if (!blackboxShapes.containsKey(targetId)) {

					graph.add(blackboxShapes.get(sourceId));
				}
			} else if (connectionType
					.indexOf(VmeConstants.SHAPE_CONNECTION_OPENDATA) != -1) {

				// Prende gli id degli shape coinvolti nella connessione
				String sourceId = connection.getSourceId();
				String targetId = connection.getTargetId();
				// Cattura i nodi dalle collezioni selezionate e popola il grafo
				if (openDataShapes.containsKey(sourceId)
						&& operatorShapes.containsKey(targetId)) {

					graph.add(openDataShapes.get(sourceId),
							operatorShapes.get(targetId));

				} else if (openDataShapes.containsKey(sourceId)
						&& serviceShapes.containsKey(targetId)) {

					graph.add(openDataShapes.get(sourceId),
							serviceShapes.get(targetId));

				} else if (openDataShapes.containsKey(sourceId)
						&& blackboxShapes.containsKey(targetId)) {

					graph.add(openDataShapes.get(sourceId),
							blackboxShapes.get(targetId));

				} else if (openDataShapes.containsKey(sourceId)
						&& targetId.equals(outputShape.getId())) {

					graph.add(openDataShapes.get(sourceId), outputShape);

				} else if (!openDataShapes.containsKey(sourceId)) {

					graph.add(openDataShapes.get(targetId));

				} else if (!openDataShapes.containsKey(targetId)) {

					graph.add(openDataShapes.get(sourceId));
				}				
			} else if (connectionType
					.indexOf(VmeConstants.SHAPE_CONNECTION_START) != -1) {

				// Prende gli id degli shape coinvolti nella connessione
				String sourceId = connection.getSourceId();
				String targetId = connection.getTargetId();
				
				if (inputShapes.containsKey(sourceId)
						&& serviceShapes.containsKey(targetId)) {

					graph.add(inputShapes.get(sourceId),
							serviceShapes.get(targetId));

				} else if (inputShapes.containsKey(sourceId)
						&& blackboxShapes.containsKey(targetId)) {

					graph.add(inputShapes.get(sourceId),
							blackboxShapes.get(targetId));
					
				} else if (inputShapes.containsKey(sourceId)
						&& operatorShapes.containsKey(targetId)) {

					graph.add(inputShapes.get(sourceId),
							operatorShapes.get(targetId));	
					
				} else if (!inputShapes.containsKey(sourceId)) {

					graph.add(inputShapes.get(targetId));

				} else if (!inputShapes.containsKey(targetId)) {

					graph.add(inputShapes.get(sourceId));
				}	
			}
		}

		return graph;
	}

	public static ByteArrayOutputStream marshalEmmlXml(Mashup rootTag)
			throws JAXBException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance("com.mediigea.vme2.emml");

			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(CharacterEscapeHandler.class.getName(),
					new CharacterEscapeHandler() {
						@Override
						public void escape(char[] ch, int start, int length,
								boolean isAttVal, Writer writer)
								throws IOException {
							writer.write(ch, start, length);
						}
					});

			marshaller.marshal(rootTag, outputStream);
		} catch (JAXBException e) {
			logger.error("Error marshalling emml script");
			throw e;
		}

		return outputStream;
	}

	public static void writeEmmlScriptFile(String emmlScriptFolder,
			String fileName, byte[] xmlScript) throws IOException {
		String filename = emmlScriptFolder + "/" + fileName + ".emml";

		File file = new File(filename);
		FileUtils.writeByteArrayToFile(file, xmlScript);
	}

	public static void buildOutputScript(Mashup rootTag, int projectId,
			List<String> outputNodes) {
		ObjectFactory of = new ObjectFactory();

		Mashup.Output outputParam = new Mashup.Output();
		String outputVarName = "project"
				+ VmeConstants.EMML_SCRIPT_VAR_SEPARATOR + projectId
				+ VmeConstants.EMML_SCRIPT_VAR_SEPARATOR + "out";
		outputParam.setType(VmeConstants.EMML_VAR_TYPE_STRING);
		outputParam.setName(outputVarName);
		rootTag.getEmmlMetaOrInputOrOutput().add(
				of.createMashupOutput(outputParam));

		String inputs = StringUtils.join(outputNodes, ",");

		StringBuffer scriptBuff = new StringBuffer();
		scriptBuff.append("<![CDATA[ var ").append(outputVarName)
				.append(" = '{ ");

		for (int i = 0; i < outputNodes.size(); i++) {
			String nodetmp = outputNodes.get(i);
			if (i > 0) {
				scriptBuff.append(", ");
			}
			scriptBuff.append(" \"").append(nodetmp).append("\":' + ")
					.append(nodetmp).append(" +' ");
		}

		scriptBuff.append(" }'; ]]>");

		ScriptType script = new ScriptType();
		script.setType("text/javascript");
		script.setOutputvariable("$" + outputVarName);
		script.setInputvariables(inputs);

		script.setValue(scriptBuff.toString());

		rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupScript(script));
	}
	
	public static void buildOauthScript(int operationId,
			ServiceCatalog service, List<InputField> inputs, Object root,
			VariablesType variables, String endpoint, DirectInvokeType invoke,
			boolean isForBlackbox) {

		List<Object> rootTag = null;
		
		// Appende il tag direct invoke al roottag		
		if(root instanceof Mashup){
			rootTag = ((Mashup)root).getEmmlMetaOrInputOrOutput();
		
		} else if (root instanceof ForEachType){
			
			rootTag = ((ForEachType)root).getAnnotateOrAppendresultOrAssign();
		}			

		ObjectFactory of = new ObjectFactory();
		
		// IVAN: auth is set at operation level
		if(service.getAuthType() == null)
		{
			buildOperationOauthScript(operationId, service, inputs, root, variables, endpoint, invoke, isForBlackbox);	
			
			return;
		}

		String authType = service.getAuthType().getType()!=null?service.getAuthType().getType():"";

		if (VmeConstants.OAUTH10VERSION.equals(authType)) {

			if (!inputs.isEmpty()) {
				ConstructorType inputTransformer = of.createConstructorType();

				StringBuilder content = new StringBuilder("<params>");
				for (InputField input : inputs) {
					content.append("<param>").append("<text>")
							.append(input.getText()).append("</text>")
							.append("<value>").append(input.getValue())
							.append("</value>").append("</param>");
				}
				content.append("</params>");

				String variableName = VmeConstants.EMML_OAUTH_INPUTPARAMS_PREFIX
						+ operationId;
				inputTransformer.setOutputvariable(variableName);
				inputTransformer.getContent().add(content.toString());

				rootTag.add(
						of.createMashupConstructor(inputTransformer));

				AssignType assign = new AssignType();
				assign.setFromexpr("$" + variableName + "/*");
				assign.setOutputvariable("$" + variableName + "_str");

				rootTag.add(
						of.createMashupAssign(assign));
			}

			// Crea una variabile dove storare la consumer key
			VariableType oauthKeyParam = of.createVariableType();
			String oauthKeyName = VmeConstants.EMML_OAUTH_CONSUMERKEY_PREFIX
					+ service.getId();
			oauthKeyParam.setName(oauthKeyName);
			oauthKeyParam.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			oauthKeyParam.setDefault(service.getApiKey());
			variables.getVariable().add(oauthKeyParam);

			// Crea una variabile dove storare la consumer secret
			VariableType oauthSecretParam = of.createVariableType();
			String oauthSecretName = VmeConstants.EMML_OAUTH_CONSUMERSECRET_PREFIX
					+ service.getId();
			oauthSecretParam.setName(oauthSecretName);
			oauthSecretParam.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			oauthSecretParam.setDefault(service.getApiSecret());
			variables.getVariable().add(oauthSecretParam);

			// Crea un tag di tipo parametro di input per il token
			// oauth (access token)
			Mashup.Input oauthTokenParam = new Mashup.Input();
			oauthTokenParam.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			String oauthTokenName = VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX
					+ service.getId();
			oauthTokenParam.setName(oauthTokenName);
			// Lo aggiunge al root tag
			rootTag.add(
					of.createMashupInput(oauthTokenParam));

			// Crea un tag di tipo parametro di input per il token
			// secret (access token secret)
			Mashup.Input tokenSecretParam = new Mashup.Input();
			tokenSecretParam.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			String tokenSecretName = VmeConstants.EMML_OAUTH_TOKENSECRET_PREFIX
					+ service.getId();
			tokenSecretParam.setName(tokenSecretName);
			// Lo aggiunge al root tag
			rootTag.add(
					of.createMashupInput(tokenSecretParam));

			// Crea un tag di tipo parametro di input per il token
			// secret (access token secret)
			String jsonParamName = VmeConstants.EMML_OAUTH_INPUTPARAMS_PREFIX
					+ operationId + "_str";

			// Crea una variabile dove storare i parametri
			// dell'header restituiti dalla funzione custom
			VariableType variableResultStr = of.createVariableType();
			String oauthParamsStr = VmeConstants.EMML_OAUTH_HEADERPARAMS_PREFIX_STR
					+ operationId;// L'identificativo è
									// relativo all'operazione
			variableResultStr.setName(oauthParamsStr);
			variableResultStr.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			variables.getVariable().add(variableResultStr);

			// Crea una variabile dove storare i parametri
			// dell'header restituiti dalla funzione custom
			String oauthParams = VmeConstants.EMML_OAUTH_HEADERPARAMS_PREFIX
					+ operationId;// L'identificativo è
									// relativo all'operazione

			ScriptType script = of.createScriptType();
			script.setType(VmeConstants.EMML_SCRIPT_TYPE_JAVASCRIPT);
			script.setInputvariables(oauthKeyName + "," + oauthSecretName + ","
					+ oauthTokenName + "," + tokenSecretName + ","
					+ jsonParamName);
			script.setOutputvariable(oauthParamsStr);
			String scriptBody = "var oauthbean="
					+ VmeConstants.EMML_SCRIPT_OAUTH_GENERATOR_IMPLEMENTATION
					+ "('" + endpoint + "'," + oauthKeyName + ","
					+ oauthTokenName + "," + oauthSecretName + ","
					+ tokenSecretName + "," + jsonParamName + ");"
					+ oauthParamsStr + "="
					+ VmeConstants.EMML_SCRIPT_JAVATOXML_IMPLEMENTATION
					+ "(oauthbean);";

			script.setValue(scriptBody);

			// Inserisce il tag script nel rootTag
			rootTag.add(
					of.createMashupScript(script));

			AssignType assign = of.createAssignType();
			assign.setOutputvariable("$" + oauthParams);
			assign.setFromvariable("$" + oauthParamsStr);

			rootTag.add(
					of.createMashupAssign(assign));

			ConstructorType httpHeaderConstructor = of.createConstructorType();
			String header = "<headers>"
					+ "<User-Agent>Vme2</User-Agent>"
					+ "<Content-Type>application/x-www-form-urlencoded</Content-Type>"
					+ "<Authorization>OAuth " + "oauth_consumer_key=\"{$"
					+ oauthKeyName + "}\", " + "oauth_nonce=\"{$" + oauthParams
					+ "//oauth__nonce/text()}\", " + "oauth_signature=\"{$"
					+ oauthParams + "//oauth__signature/text()}\", "
					+ "oauth_signature_method=\"{$" + oauthParams
					+ "//oauth__signature__method/text()}\", "
					+ "oauth_timestamp=\"{$" + oauthParams
					+ "//oauth__timestamp/text()}\", " + "oauth_token=\"{$"
					+ oauthTokenName + "}\", " + "oauth_version=\"{$"
					+ oauthParams
					+ "//oauth__version/text()}\"</Authorization>"
					+ "</headers>";

			String httpHeaderName = VmeConstants.EMML_OAUTH_HTTPHEADER_PREFIX
					+ operationId;
			httpHeaderConstructor.setOutputvariable(httpHeaderName);
			httpHeaderConstructor.getContent().add(header);

			if(isForBlackbox){
				invoke.getOtherAttributes().put(new QName(httpHeaderName),
						"{$" + httpHeaderName + "}");
			}else{
				invoke.setHeader("$" + httpHeaderName);
			}
			

			rootTag.add(
					of.createMashupConstructor(httpHeaderConstructor));

		} else if (VmeConstants.OAUTH20VERSION.equals(authType)) {
			// Crea un tag di tipo parametro di input per il token
			// oauth (access token)
			Mashup.Input oauthTokenParam = new Mashup.Input();
			oauthTokenParam.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			String oauthTokenName = VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX
					+ service.getId();
			oauthTokenParam.setName(oauthTokenName);
			// Lo aggiunge al root tag
			rootTag.add(
					of.createMashupInput(oauthTokenParam));

			invoke.getOtherAttributes().put(new QName(isForBlackbox ? oauthTokenName : "access_token"),
						"{$" + oauthTokenName + "}");

		} else if (VmeConstants.BASICAUTH.equals(authType)) {

			String username = service.getUsername();
			String password = service.getPassword();

			String string4Base64 = username + ":" + password;

			byte[] encoded = Base64.encodeBase64(string4Base64.getBytes());

			String encodedString = new String(encoded);

			logger.debug("Base64 Encoded String: " + encodedString);

			ConstructorType httpHeaderConstructor = of.createConstructorType();
			String header = "<headers>"
					+ "<User-Agent>Vme2</User-Agent>"
					+ "<Content-Type>application/x-www-form-urlencoded</Content-Type>"
					+ "<Authorization>Basic " + encodedString
					+ "</Authorization>" + "</headers>";

			String httpHeaderName = VmeConstants.EMML_OAUTH_HTTPHEADER_PREFIX
					+ operationId;
			httpHeaderConstructor.setOutputvariable(httpHeaderName);
			httpHeaderConstructor.getContent().add(header);

			invoke.setHeader("$" + httpHeaderName);

			rootTag.add(
					of.createMashupConstructor(httpHeaderConstructor));
		} // IVAN: added to allow WeLive Authorization by AAC
		else if (VmeConstants.WELIVEAUTH.equals(authType)) 
		{
			// Crea un tag di tipo parametro di input per il token
			// oauth (access token)
			Mashup.Input oauthTokenParam = new Mashup.Input();
			oauthTokenParam.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			String oauthTokenName = VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX + service.getId();
			oauthTokenParam.setName(oauthTokenName);
			// Lo aggiunge al root tag
			rootTag.add(of.createMashupInput(oauthTokenParam));
			
			// build authorization header
			ConstructorType httpHeaderConstructor = of.createConstructorType();
			String header = ""
					+ "<header>"
					+ "<Authorization>Bearer " + "{$" + VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX + service.getId() + "}</Authorization>"
					+ "</header>";

			String httpHeaderName = VmeConstants.EMML_OAUTH_HTTPHEADER_PREFIX + service.getId();
			httpHeaderConstructor.setOutputvariable(httpHeaderName);
			httpHeaderConstructor.getContent().add(header);

			invoke.setHeader("$" + httpHeaderName);

			rootTag.add(of.createMashupConstructor(httpHeaderConstructor));
		}
		// }
	}
	
	// IVAN: WeLive authorization - operation level
	private static void buildOperationOauthScript(int operationId,
			ServiceCatalog service, List<InputField> inputs, Object root,
			VariablesType variables, String endpoint, DirectInvokeType invoke,
			boolean isForBlackbox) {

		List<Object> rootTag = null;
		
		// Appende il tag direct invoke al roottag		
		if(root instanceof Mashup){
			rootTag = ((Mashup)root).getEmmlMetaOrInputOrOutput();
		
		} else if (root instanceof ForEachType){
			
			rootTag = ((ForEachType)root).getAnnotateOrAppendresultOrAssign();
		}			

		ObjectFactory of = new ObjectFactory();

			// Crea un tag di tipo parametro di input per il token
			// oauth (access token)
			Mashup.Input oauthTokenParam = new Mashup.Input();
			oauthTokenParam.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			String oauthTokenName = VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX + service.getId() + "_" + operationId;
			oauthTokenParam.setName(oauthTokenName);
			// Lo aggiunge al root tag
			rootTag.add(of.createMashupInput(oauthTokenParam));
			
			// build authorization header
			ConstructorType httpHeaderConstructor = of.createConstructorType();
			String header = ""
					+ "<header>"
					+ "<Authorization>Bearer " + "{$" + VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX + service.getId() + "_" + operationId + "}</Authorization>"
					+ "</header>";

			String httpHeaderName = VmeConstants.EMML_OAUTH_HTTPHEADER_PREFIX + service.getId() + "_" + operationId;
			httpHeaderConstructor.setOutputvariable(httpHeaderName);
			httpHeaderConstructor.getContent().add(header);

			invoke.setHeader("$" + httpHeaderName);

			rootTag.add(of.createMashupConstructor(httpHeaderConstructor));
	}
	
	public static String inputValueDigester(ServiceParameterDTO param, String value, ObjectFactory of, Object rootTag, String nodeId, VariablesType variables){
		String out = value;

		if(value.matches(VmeConstants.SERVICE_INPUT_PARAM_REGEX)){
			//Selezionato parametro di input di tipo link
			//prototipo: {channels_search1_0:$.feed.entry}

			//Prende l'identificativo del servizio source da cui prelevare il valore
			String sourceServiceId = value.substring(value.indexOf("{")+1, value.indexOf(":"));

			//Crea delle variabili d'appoggio per handling dell'elaborazione dei dati intermedi
			VariableType scriptInVar = new VariableType();
			String inputVarName = nodeId+VmeConstants.EMML_SCRIPT_VAR_SEPARATOR+param.getName();
			scriptInVar.setName(inputVarName);
			scriptInVar.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			scriptInVar.getContent().add(EmmlScriptUtils.parsePathVariable(value));
			variables.getVariable().add(scriptInVar);						

			VariableType scriptOutVar = new VariableType();
			String outputVarName = inputVarName + VmeConstants.EMML_SCRIPT_VAR_SEPARATOR + VmeConstants.EMML_SCRIPT_OUTPUT_VAR_SUFFIX;
			scriptOutVar.setName(outputVarName);
			scriptOutVar.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			variables.getVariable().add(scriptOutVar);						

			//Crea un nodo script in cui effettuare la chiamata al java filter per il parsing del risultato dell'invocazione e la selezione del
			//dato tramite JsonPath
			ScriptType script = of.createScriptType();
			script.setType(VmeConstants.EMML_SCRIPT_TYPE_JAVASCRIPT);							
			script.setInputvariables(sourceServiceId+","+inputVarName);
			script.setOutputvariable(outputVarName);
			
			// IVAN
			// for black boxes
			
			boolean blackBox = inputVarName.startsWith("blackbox");			
			//String scriptBody= outputVarName+"="+VmeConstants.EMML_SCRIPT_VALUE_FILTER_IMPLEMENTATION+"("+sourceServiceId+","+inputVarName+","+ Boolean.toString(blackBox) +");";
			String scriptBody= outputVarName+"="+VmeConstants.EMML_SCRIPT_VALUE_FILTER_IMPLEMENTATION+"("+sourceServiceId+","+inputVarName+"," + PropertyGetter.getProperty("emml.getSubtreeNodeMap.replaceDoubleQuotes") + ");";
			
			// for objects parsing
			if(blackBox)
			{
				scriptBody += "var tmp = JSON.parse(" + outputVarName + ");";
				scriptBody += "if(typeof tmp == 'object') " + outputVarName + " = " + outputVarName + ";";
				scriptBody += "else " + outputVarName + " = tmp;";
			}
			//
			
			// IVAN
			// patch for Packages.com.mediigea.jsonutils.JsonUtils.getSubtreeNodeMap replace double quotes
			scriptBody += " var test;";
			scriptBody += " try { test = new Packages.org.json.JSONObject(" + outputVarName + "); } catch(ex) {}";
			scriptBody += " if(!test) { try { test = new Packages.org.json.JSONArray(" + outputVarName + "); } catch(ex) {} }";
			scriptBody += " if(!test) { test = " + VmeConstants.EMML_SCRIPT_VALUE_FILTER_IMPLEMENTATION + "(" + sourceServiceId + ", " + inputVarName + ", true); }";
			scriptBody += " " + outputVarName + " = test;";
			//
			
			script.setValue(scriptBody);

			//Inserisce il tag script nel rootTag
			if(rootTag instanceof Mashup){
				((Mashup)rootTag).getEmmlMetaOrInputOrOutput().add(of.createMashupScript(script));
			
			} else if (rootTag instanceof ForEachType){
				
				((ForEachType)rootTag).getAnnotateOrAppendresultOrAssign().add(of.createMashupScript(script));
			}
			out = "{$"+outputVarName+"}";
		}else{

			//Controlla se il valore del parametro e' di tipo link con un input shape
			//prototipo: {input_0}
			logger.debug("Endpoint parameter value: "+value);							
			Pattern p = Pattern.compile("(\\{)(.*)(\\})");
			Matcher m = p.matcher(value);														

			//se il valore e' di tipo link lo sostituisce con il nome della variabile di input precedentemente creata
			//{input_0} -> {$input_0}
			if(m.find()){
				StringBuffer buffer = new StringBuffer();
				m.appendReplacement(buffer, "$1\\$$2$3");
				out = buffer.toString();
			}
			logger.debug("Endpoint parameter value replacement: "+value);		
		}

		return out;
	}		
	
	public static String parsePathVariable(String value) {
		// Prende la path da cui prelevare il valore
		String sourceParameterPath = value.substring(value.indexOf(":") + 1,
				value.indexOf("}"));

		// Se nella path JsonPath e' presente una sequenza di caratteri
		// ".$" l'api JsonPath lanciano un errore
		// Si provvede quindi a bonificare la path utilizzando la
		// bracket notation prevista
		// TODO: Successivamente si vagliera' se utilizzare la bracket
		// notation per la codifica del path
		// in fase di generazione dell'albero jstree
		logger.debug("source parameter path: " + sourceParameterPath);
		Pattern p = Pattern.compile("(\\.)(\\$.*)(\\.)?");
		Matcher m = p.matcher(sourceParameterPath);
		StringBuffer buffer = new StringBuffer();

		if (m.find()) {
			m.appendReplacement(buffer, "[$2]");
		} else {
			buffer.append(sourceParameterPath);
		}
		logger.debug("source parameter path replacement: " + buffer.toString());

		return buffer.toString();
	}	
	
	public static String getServiceIdFromPath(String value){
		return value.substring(value.indexOf("{") + 1, value.indexOf(":"));
	}
	
	public static String getJsonPathFromPath(String value){
		return value.substring(value.indexOf(":")+1, value.lastIndexOf("}"));
	}
}
