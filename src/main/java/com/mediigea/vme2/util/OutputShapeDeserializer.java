package com.mediigea.vme2.util;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.mediigea.vme2.dto.OutputShapeDTO;

public class OutputShapeDeserializer  extends

JsonDeserializer<OutputShapeDTO> {
	@Override
	public OutputShapeDTO deserialize(JsonParser jsonParser,
			DeserializationContext deserializationContext) throws IOException {
		
		
		ObjectCodec oc = jsonParser.getCodec();
		JsonNode node = oc.readTree(jsonParser);

		JsonNode id = node.get("id");
		JsonNode type = node.get("type");
		JsonNode label  = node.get("label");
		JsonNode inputs = node.get("userData");

		OutputShapeDTO outputShape = new OutputShapeDTO();

		if(id!=null){ outputShape.setId(id.asText());}
		if(type!=null){ outputShape.setType(type.asText());}
		if(label!=null){ outputShape.setLabel(label.asText());}		
		if(inputs!=null){
			ArrayNode array = (ArrayNode) inputs.get("inputs");

			for(int i=0; i<array.size(); i++){
				outputShape.getInputs().add(array.get(i).asText());
			}						
		}

		return outputShape;
	}
}


