package com.mediigea.vme2.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JsTreeNode {
	public JsTreeNode() {
		this.children = new ArrayList<JsTreeNode>();
	}

	Attributes attributes;
	@SuppressWarnings("rawtypes")
	Map metadata;
	Data data;
	String state;
	List<JsTreeNode> children;	
	
	String type;
	
	public Attributes getAttributes() {
		return attributes;
	}

	public void setAttributes(Attributes attributes) {
		this.attributes = attributes;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<JsTreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<JsTreeNode> children) {
		this.children = children;
	}

	@SuppressWarnings("rawtypes")
	public Map getMetadata() {
		return metadata;
	}
	@SuppressWarnings("rawtypes")
	public void setMetadata(Map metadata) {
		this.metadata = metadata;
	}	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public class Attributes {
		String id;
		String rel;
		
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getRel() {
			return rel;
		}
		public void setRel(String rel) {
			this.rel = rel;
		}		
	}

	public class Data {
		String title;
		String icon;
		
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getIcon() {
			return icon;
		}
		public void setIcon(String icon) {
			this.icon = icon;
		}	
	}
}
