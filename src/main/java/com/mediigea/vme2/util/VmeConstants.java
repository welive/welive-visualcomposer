package com.mediigea.vme2.util;

public class VmeConstants {

	/* SESSION VARIABLES */
	public static final String AUTH_FROM_PROVIDER_FOR = "authfor";
	public static final String REGISTRATION_FROM_PROVIDER = "registration";
	public static final String REGISTRATION_FROM_PLATFORM = "registration_from_platform";
	public static final String AUTH_FROM_PROVIDER = "auth";
	public static final String AUTH_FROM_PLATFORM = "auth_from_platform";
	public static final String LOGIN_TYPE = "login_type";
	public static final String LOGGED_ACCOUNT = "logged_account";
	public static final String REQUESTED_URL = "shared_project_url";
	public static final String SESSION_USER_TOKEN_CONNECTOR = "user_connector_id";	
	
	/* FLASH MESSAGE KEYS */
	public static final String MESSAGE_SUCCESS = "success";
	public static final String MESSAGE_WARNING = "warning";
	public static final String MESSAGE_ERROR = "error";
	public static final String MESSAGE_TEXT = "message";
	public static final String MESSAGE_TYPE = "alert";
	public static final String MESSAGE_STACK_TRACE = "stackTrace";
	
	/* SOCIAL AUTH PROVIDERS */
	public static final String AUTH_PROVIDER_FB = "facebook";
	public static final String AUTH_PROVIDER_GOOGLE = "google";
	public static final String AUTH_PROVIDER_VME = "vme";
	
	/* USER ROLES */
	public static final int ACCOUNT_ROLE_ADMIN = 1;
	public static final int ACCOUNT_ROLE_USER = 2;
	public static final String SECURITY_ROLE_USER = "ROLE_USER";
	public static final String SECURITY_ROLE_ADMIN = "ROLE_ADMIN";
	
	// IVAN
	/* USER LEVELS */
	public static final int USER_LEVEL_MOCKUPDESIGNER = 1;
	public static final int USER_LEVEL_DEVELOPER_BASE = 2;
	public static final int USER_LEVEL_DEVELOPER_ADVANCED = 3;
	
	/* PROFILE_WORKGROUP ROLES */
	public static final int PROFILE_WORKGROUP_ROLE_OWNER = 1;
	public static final int PROFILE_WORKGROUP_ROLE_USER = 2;
	
	/* PROFILE_PROJECT ROLES */
	public static final int PROFILE_PROJECT_ROLE_OWNER = 1;
	public static final int PROFILE_PROJECT_ROLE_USER = 2;
	
	/* PROFILE_ORGANIZATION ROLES */
	public static final int PROFILE_ORGANIZATION_NONE = 0;
	public static final int PROFILE_ORGANIZATION_ROLE_OWNER = 1;
	public static final int PROFILE_ORGANIZATION_ROLE_MEMBER = 2;
	
	/* ACCOUNT STATUS */
	public static final int ACCOUNT_STATUS_PENDING = 1;
	public static final int ACCOUNT_STATUS_ENABLED = 2;
	public static final int ACCOUNT_STATUS_DISABLED = 3;
	public static final int ACCOUNT_STATUS_RESET_PASSWORD = 4;
	public static final int ACCOUNT_TO_BE_DELETED = 5;
	
	
	/* GENERIC CONSTANTS AND FLAGS */
	public static final int LOGIN_TYPE_ADMIN = 1;
	public static final int LOGIN_TYPE_USER = 2;
	
	public static final String VME_EMAIL_ADMIN = "admin@vme.local";
	
	
	/**/
	public static final int SERVICECATALOG_TYPE_SOAP = 1;
	public static final int SERVICECATALOG_TYPE_REST = 2;
	
	public static final int SERVICECATALOG_TYPE_REST_WADL = 1;
	public static final int SERVICECATALOG_TYPE_REST_SWAGGER = 2;
	public static final int SERVICECATALOG_TYPE_REST_METAMODEL = 3;
	public static final int SERVICECATALOG_TYPE_SOAP_WSDL = 1;
	
	public static final int SERVICECATALOG_VISIBILITY_PUBLIC = 1;
	public static final int SERVICECATALOG_VISIBILITY_WORKGROUP = 2;
	public static final int SERVICECATALOG_VISIBILITY_PRIVATE = 3;
	public static final int SERVICECATALOG_VISIBILITY_DISABLED = 4;
	
	
	/***/
	public static final String MESSAGE_ACCOUNT_NOT_FOUND = "message.account.notfound";
	public static final String MESSAGE_ACCOUNT_NOT_ENABLED = "message.account.notenabled";
	public static final String MESSAGE_ACCOUNT_NOT_AUTHORIZED = "message.account.notautorized";
	
	public static final String ROUTE_HOME_ADMIN = "/admin/profile/list";
	public static final String ROUTE_HOME_USER = "/user/project/list";
	
	/* WORKFLOW GENERATION CONSTANTS */
	public static final String CANVAS_ROOT_NODE = "content";
	
	public static final String SHAPE_INPUT = "vme2.shape.Start";
	public static final String SHAPE_SERVICE = "vme2.shape.Service";
	public static final String SHAPE_OPERATOR = "vme2.shape.Operator";
	public static final String SHAPE_BLACKBOX = "vme2.shape.Blackbox";
	public static final String SHAPE_OPENDATA = "vme2.shape.OpenData";
	public static final String SHAPE_INNERPROCESS = "vme2.shape.InnerProcess";
	public static final String SHAPE_OUTPUT = "vme2.shape.End";	
	public static final String SHAPE_CONNECTION  = "vme2.shape.Connection";
	public static final String SHAPE_CONNECTION_SERVICE = "vme2.shape.Connection.Service";
	public static final String SHAPE_CONNECTION_OPERATOR = "vme2.shape.Connection.Operator";
	public static final String SHAPE_CONNECTION_BLACKBOX  = "vme2.shape.Connection.Blackbox";
	public static final String SHAPE_CONNECTION_OPENDATA  = "vme2.shape.Connection.OpenData";
	public static final String SHAPE_CONNECTION_START  = "vme2.shape.Connection.Start";	
	public static final String SHAPE_CONNECTION_SERVICE_S2R = "vme2.shape.Connection.Service.S2R";	
	public static final String EMML_SCRIPT_OUTPUT_VAR_SUFFIX = "result";
	public static final String EMML_SCRIPT_VAR_SEPARATOR = "_";
	public static final String EMML_SCRIPT_TYPE_JAVASCRIPT = "text/javascript";
	public static final String EMML_VAR_TYPE_STRING = "string";
	public static final String EMML_VAR_TYPE_DOCUMENT = "document";
	public static final String EMML_SCRIPT_VALUE_FILTER_IMPLEMENTATION="Packages.com.mediigea.jsonutils.JsonUtils.getSubtreeNodeMap";
	public static final String EMML_SCRIPT_VALUE_CONVERTER_IMPLEMENTATION="Packages.com.mediigea.jsonutils.JsonUtils.getSoapAsJson";
	public static final String EMML_SCRIPT_OAUTH_GENERATOR_IMPLEMENTATION="Packages.com.mediigea.jsonutils.OAuthUtils.getOAuthHeaderParams";
	public static final String EMML_SCRIPT_JAVATOXML_IMPLEMENTATION="Packages.org.oma.emml.utils.XMLConversionUtils.javaToXml";
	public static final String EMML_SCRIPT_JSONTOXML_IMPLEMENTATION="Packages.com.mediigea.jsonutils.JsonUtils.jsonToXml";
	
	public static final String EMML_OAUTH_CONSUMERKEY_PREFIX="oauth_key_";
	public static final String EMML_OAUTH_CONSUMERSECRET_PREFIX="oauth_secret_";
	public static final String EMML_OAUTH_ACCESSTOKEN_PREFIX="oauth_token_";
	public static final String EMML_OAUTH_TOKENSECRET_PREFIX="token_secret_";
	public static final String EMML_OAUTH_INPUTPARAMS_PREFIX="inputparams_";
	public static final String EMML_OAUTH_HEADERPARAMS_PREFIX_STR="oauthparams_str_";
	public static final String EMML_OAUTH_HEADERPARAMS_PREFIX="oauthparams_";
	public static final String EMML_OAUTH_HTTPHEADER_PREFIX="httpHeader_";
	
	public static final String SERVICE_INPUT_PARAM_REGEX = "^\\{.*:.*\\}$";
	
	public static final String SERVICE_CHAIN_SERVICEID_PARAM = "serviceid";
	public static final String SERVICE_CHAIN_INPUT_PARAMS = "inputparams";
	
	public static final String SHAPE_INPUT_TEXT_TYPE = "text";
	public static final String SHAPE_INPUT_COMBO_TYPE = "combo";
	
	/* FILE SYSTEM CONSTANTS */
	public static final String MAIN_DIRECTORY = "vme-editor";
	public static final String GADGET_DIR_PREFIX = "prj_";
	public static final String GADGET_RESOURCE_DIR = "resources";
	public static final String GADGET_ARCHIVE_FILENAME_PREFIX = "sme_gadget_";
	public static final String GADGET_FILENAME = "gadget.xml";	
	public static final String WORKFLOW_DIR_PREFIX = "wflow_";
	public static final String WORKFLOW_ARCHIVE_FILENAME_PREFIX = "sme_workflow_";
	public static final String RENDERSCHEMA_FILENAME = "rndSchema.xml";
	public static final String PROJECT_EXPORT_FILENAME_PREFIX = "sme_project_";
	
	public static final String GUIEDITOR_DIRECTORY = "guieditor";
	public static final String GUIPROJECT_REPOSITORY = "project_repository";
	public static final String GUIPROJECT_TEMPLATE = "project_template";
	public static final String GUIPROJECT_TEMPLATE_JS = "js";
	public static final String GUIPROJECT_PALETTE_COMPONENT_PATH = "palette_component";
	public static final String GUIPROJECT_PALETTE_APP = "app";
	public static final String GUIPROJECT_PALETTE_METADATA = "metadata";
	public static final String GUIPROJECT_DIR_PREFIX = "gprj_";
	public static final String GUIPROJECT_HTML_FILE = "file";
	public static final String GUIPROJECT_HTML_WORKING_EXT = ".html.working";
	public static final String GUIPROJECT_HTML_WORKING_SUFFIX = ".working";
	public static final String GUIPROJECT_ARCHIVE_FILENAME_PREFIX = "sme_htmlproject_";
	public static final String GUIPROJECT_CORDOVA_ARCHIVE_FILENAME_PREFIX = "cordova_project_";
		
	/* RENDERING CONSTANTS */
	public static final String COMPLEX_RENDERER_INPUT_ARRAY = "array";
	public static final String SIMPLE_RENDERER_INPUT_CONTENT = "content";
	
	/* JSTREE CONVERSION */
	public static final String JSTREE_METADATAPATH = "path";
	public static final String JSTREE_DATATYPE = "datatype";
	public static final String JSTREE_ROOTNODENAME = "$";
	
	/* AUTH CONSTANTS */
	public static final String OAUTHNONE = "0";
	public static final String OAUTH10VERSION = "1.0";
	public static final String OAUTH20VERSION = "2.0";
	public static final String BASICAUTH = "BASIC";
	// IVAN: added for WeLive
	public static final String WELIVEAUTH = "WELIVEAUTH";
}
