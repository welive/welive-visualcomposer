package com.mediigea.vme2.util;

import java.util.List;

public class PagedSearch<T> {
	private int total;
	private List<T> results;
	
	public PagedSearch() {
	
	}
	
	public PagedSearch(List<T> results, int total){
		this.results = results;
		this.total = total;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<T> getResults() {
		return results;
	}

	public void setResults(List<T> results) {
		this.results = results;
	}
}
