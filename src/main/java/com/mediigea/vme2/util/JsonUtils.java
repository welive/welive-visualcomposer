package com.mediigea.vme2.util;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.mediigea.vme2.config.ObjectMapperExt;

public class JsonUtils {
	
	private static final Logger logger = LoggerFactory
			.getLogger(JsonUtils.class);
	
	public static JsonNode stringToJsonNode(String jsonString, ObjectMapperExt objectMapper) {

		JsonNode nn = null;

		if (jsonString == null || jsonString.isEmpty())
			return null;

		try {
			nn = objectMapper.readTree(jsonString);

		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return nn;
	}
	
	public static JsonNode getSubtreeNodeMap(JsonNode root, String nodepath, ObjectMapperExt objectMapper) throws JsonProcessingException{

		//Map<String, JsonNode> nodeMap;
		JsonNode selectedNode;
		
		String jsonString;
		try {
			jsonString = new ObjectMapper().writeValueAsString(root);
			nodepath=nodepath.replace("{", "").replace("}", "").substring(nodepath.indexOf(":"));
			
			// IVAN
			String nodeAsString;
			
			Object obj = JsonPath.read(jsonString, nodepath);
			
			if(obj instanceof JSONArray){
				Object genericArrElement = ((JSONArray) obj).get(0);
				
				nodeAsString = JSONValue.toJSONString(genericArrElement);
			} else{
				
				nodeAsString = ((JSONObject) obj).toJSONString();
			}	
			// END

			selectedNode = JsonUtils.stringToJsonNode(nodeAsString, objectMapper);
						
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			
			throw e;
		}
		
		return selectedNode;
	}	
	
	public static JsonNode getSubtreeNodeMapKo(JsonNode root, String nodepath, ObjectMapperExt objectMapper) throws JsonProcessingException{

		//Map<String, JsonNode> nodeMap;
		JsonNode selectedNode;
		
		String jsonString;
		try {
			jsonString = new ObjectMapper().writeValueAsString(root);
			
			String nodeAsString;
			
			nodepath=nodepath.replaceAll("\\(\\)", "[0]");
			Object obj = JsonPath.read(jsonString, nodepath);
			
			if(obj instanceof JSONArray){
				Object genericArrElement = ((JSONArray) obj).get(0);
				
				nodeAsString = JSONValue.toJSONString(genericArrElement);
			} else{
				
				nodeAsString = ((JSONObject) obj).toJSONString();
			}			
			
			selectedNode = JsonUtils.stringToJsonNode(nodeAsString, objectMapper);
						
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			
			throw e;
		}
		
		return selectedNode;
	}		
	
	public static String getValueFromJson(JsonNode root, String nodepath, ObjectMapperExt objectMapper) throws JsonProcessingException{

		//Map<String, JsonNode> nodeMap;
		String out;
		
		String jsonString;
		StringBuffer buffer = new StringBuffer();
		try {
			jsonString = new ObjectMapper().writeValueAsString(root);			

			//Se nella path JsonPath e' presente una sequenza di caratteri ".$" l'api JsonPath lanciano un errore
			//Si provvede quindi a bonificare la path utilizzando la bracket notation prevista								
			//TODO: Successivamente si vagliera' se utilizzare la bracket notation per la codifica del path 
			//in fase di generazione dell'albero jstree							
			Pattern p = Pattern.compile("(\\.)(\\$.*)(\\.)?");
			Matcher m = p.matcher(nodepath);														
			
			if(m.find()){
				m.appendReplacement(buffer, "[$2]");
			} else{
				buffer.append(nodepath);
			}																													
			
			out = JsonPath.read(jsonString, buffer.toString()).toString();		
						
		} catch (Exception e) {
			// e.printStackTrace();
			// nodepath not-found!";
			out = ""; //			
			//throw e;
		}
		
		return out;
	}		
	
	public static JSONArray getArrayFromJson(JsonNode root, String nodepath, ObjectMapperExt objectMapper) throws JsonProcessingException{

		//Map<String, JsonNode> nodeMap;
		JSONArray selectedNode;
		
		String jsonString;
		try {
			jsonString = new ObjectMapper().writeValueAsString(root);
			
			//Se nella path JsonPath e' presente una sequenza di caratteri ".$" l'api JsonPath lanciano un errore
			//Si provvede quindi a bonificare la path utilizzando la bracket notation prevista								
			//TODO: Successivamente si vagliera' se utilizzare la bracket notation per la codifica del path 
			//in fase di generazione dell'albero jstree							
			Pattern p = Pattern.compile("(\\.)(\\$.*)(\\.)?");
			Matcher m = p.matcher(nodepath);														
			StringBuffer buffer = new StringBuffer();							

			if(m.find()){
				m.appendReplacement(buffer, "[$2]");
			} else{
				buffer.append(nodepath);
			}
					
			selectedNode = JsonPath.read(jsonString, buffer.toString());		
						
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			
			throw e;
		}
		
		return selectedNode;
	}	
}
