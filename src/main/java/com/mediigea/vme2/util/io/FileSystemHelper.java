package com.mediigea.vme2.util.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

public class FileSystemHelper {

	private static final Logger logger = LoggerFactory.getLogger(FileSystemHelper.class);
	
	public boolean saveFile(MultipartFile file, String filename){
		
		logger.info("Save File");
		
		InputStream inputStream = null;
		OutputStream outputStream = null;
		
		logger.info("Attempt to Save File: " + filename);
		
		try {
			
			inputStream = file.getInputStream();
			outputStream = new FileOutputStream(new File(filename));
	 
			int read = 0;
			byte[] bytes = new byte[1024];
	 
			while ((read = inputStream.read(bytes)) != -1)
				outputStream.write(bytes, 0, read);
			
			logger.info("File saved");
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
		} finally {
			
			if (inputStream != null) {
				
				try {
					
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
			
			if (outputStream != null) {
				
				try {
					
					outputStream.close();
				} catch (IOException e) {
					
					e.printStackTrace();
					return false;
				}
			}
		}
		
		return true;
	}
	
	public static boolean saveFile(InputStream inputStream, String filename){
		
		logger.info("Save File");
		
		OutputStream outputStream = null;
		
		logger.info("Attempt to Save File: " + filename);
		
		try {
			
			outputStream = new FileOutputStream(new File(filename));
	 
			int read = 0;
			byte[] bytes = new byte[1024];
	 
			while ((read = inputStream.read(bytes)) != -1)
				outputStream.write(bytes, 0, read);
			
			logger.info("File saved");
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
		} finally {
			
			if (inputStream != null) {
				
				try {					
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
			
			if (outputStream != null) {
				
				try {
					
					outputStream.close();
				} catch (IOException e) {
					
					e.printStackTrace();
					return false;
				}
			}
		}
		
		return true;
	}	
	
	public int saveAllFile(List<MultipartFile> sources, String path){
		
		logger.info("Save All File");
		
		if(sources==null || sources.isEmpty())
			return -1;
		
		int result = 1;
		File folder = new File(path);
		
		if(folder.exists()){
			
			logger.debug("Gadget folder exist: delete folder");
			
			if(folder.delete()){
				
				logger.debug("Create new gadget folder");
				
				folder.mkdir();
			}
		}else{
			
			logger.debug("Gadget folder not exist: create folder");
			folder.mkdir();
		}
		
		if(folder.exists()){
			
			logger.debug("Attempt to save filse into gadget folder");
			
			for(MultipartFile file: sources){
				
				if(!this.saveFile(file, path + "/" + file.getOriginalFilename()))
					result = 0;
			}
		}
		
		return result;
	}
}
