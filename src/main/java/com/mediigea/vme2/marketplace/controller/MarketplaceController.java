package com.mediigea.vme2.marketplace.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mediigea.vme2.config.ResourcesMapper;
import com.mediigea.vme2.controller.BaseController;
import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Mashup;
import com.mediigea.vme2.marketplace.dto.CategoryDTO;
import com.mediigea.vme2.marketplace.dto.MashupDTO;
import com.mediigea.vme2.entity.Category;
import com.mediigea.vme2.entity.Mashup;
import com.mediigea.vme2.marketplace.service.MarketplaceService;
import com.mediigea.vme2.service.SocialGadgetService;
import com.mediigea.vme2.util.VmeConstants;
 
@Controller
@RequestMapping(value = "/user/marketplace")
public class MarketplaceController extends BaseController {

	@Autowired
	private MarketplaceService marketplaceService;

	@Autowired
	private ResourcesMapper resourcesMapper;

	@Autowired
	private SocialGadgetService socialGadgetService;	
	
	private static final Logger logger = LoggerFactory
			.getLogger(MarketplaceController.class);

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:m:s");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	@RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
	public ModelAndView readMashup(@PathVariable("id") Integer id) {

		logger.info("Mashup read Mashup request");
		AccountDTO a = getLoggedUser();
		ModelAndView mav = getDefaultModelAndView("marketplace/mashup_details");
		MashupDTO mashupDTO = marketplaceService.readMashup(id);
		CategoryDTO categoryList = marketplaceService.createTreeCategory();
		mav.addObject("mashup", mashupDTO);
		mav.addObject("categoryList", categoryList);
		mav.addObject("editMode", true);
		return mav;
	}

	@RequestMapping(value = "{id}/add", method = RequestMethod.GET)
	public ModelAndView addMashup(@PathVariable("id") Integer id) {

		logger.info("Mashup read Mashup request");
		AccountDTO a = getLoggedUser();
		ModelAndView mav = getDefaultModelAndView("marketplace/mashup_details");
		MashupDTO mashupDTO = new MashupDTO();
		mashupDTO.setMockupId(id);
		CategoryDTO categoryList = marketplaceService.createTreeCategory();
		mav.addObject("mashup", mashupDTO);
		mav.addObject("categoryList", categoryList);
		mav.addObject("editMode", false);
		return mav;
	}

	@RequestMapping(value = "mashup/edit/save", method = RequestMethod.POST)
	public ModelAndView saveMashup(@ModelAttribute MashupDTO mashup,
			BindingResult result) {
		ModelAndView mav;
		logger.info("Mashup save or edit mashup request");
		boolean edit=false;
		if(mashup.getId()!=null)
		{
			edit=true;
		}
		AccountDTO a = getLoggedUser();
		MultipartFile mpf = mashup.getImage();
		
		if(!mpf.isEmpty()){
			String nameImagePath = (mpf.getOriginalFilename()).toLowerCase();
	
			File file = new File(resourcesMapper.getUploadsImageMarketplace()
					+ nameImagePath);
			try {
				mpf.transferTo(file);
			
				mashup.setPathImage(nameImagePath);							
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				if(edit)
					mav = getDefaultModelAndView("redirect:/user/marketplace/mashupList/");
				else
					mav = getDefaultModelAndView("redirect:/user/gui/list");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.edit.error");
			}
		}
		
		if(!edit){
			String zipName = mashup.getName().replaceAll(" ", "_").toLowerCase() + ".zip";
			String destPath = resourcesMapper.getUploadsMashupMarketplace() + zipName;	
			File destGadget = new File(destPath);				
			try{	
				InputStream is = socialGadgetService.getGadgetArchive(mashup.getMockupId());
			
				FileUtils.copyInputStreamToFile(is, destGadget);			
			
				mashup.setPathMashupFile(zipName);
			} catch (Exception e) {
				logger.error(e.getMessage());
				if(edit)
					mav = getDefaultModelAndView("redirect:/user/marketplace/mashupList/");
				else
					mav = getDefaultModelAndView("redirect:/user/gui/list");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.edit.error");
			}			
		}
		
		Mashup m = marketplaceService.saveUpdateMashup(mashup);
		
		if(edit) {
			mav = getDefaultModelAndView("redirect:/user/marketplace/mashupList/");
			
			String activityDescription = "Edit Marketplace item: " + mashup.getName();
			activityService.activityStreamRecordSave(a.getProfile(), 
					 								AbstractDAO.ActivityType.EDIT_PUBLISHED_GADGET_METADATA.getValue(), 
					 								activityDescription);				
		}
		else {
			mav = getDefaultModelAndView("redirect:/user/gui/list");
			String activityDescription = "Publish Marketplace item: " + mashup.getName();
			activityService.activityStreamRecordSave(a.getProfile(), 
					 								AbstractDAO.ActivityType.PUBLISH_MOCKUP_GADGET_TO_MARKETPLACE.getValue(), 
					 								activityDescription);				
			
		}	
		
		return mav;
	}

	@RequestMapping(value = "{id}/delete", method = RequestMethod.GET)
	public ModelAndView deleteMashup(@PathVariable("id") Integer id) {

		logger.info("Mashup read Mashup request");
		AccountDTO a = getLoggedUser();	
		String mashupName = marketplaceService.readMashup(id).getName();
		marketplaceService.setResourcesMapper(resourcesMapper);
		marketplaceService.deleteMashup(id);
		ModelAndView mav;
		mav = getDefaultModelAndView("redirect:/user/marketplace/mashupList");
		
		String activityDescription = "Delete Marketplace item: " + mashupName;
		activityService.activityStreamRecordSave(a.getProfile(), 
				 								AbstractDAO.ActivityType.DELETE_PUBLISHED_GADGET.getValue(), 
				 								activityDescription);			
		
		return mav;
	}

	@RequestMapping(value = "/mashupList", method = RequestMethod.GET)
	public ModelAndView list(final HttpServletRequest request) {

		logger.info("Mashup marketplace List request");

		ModelAndView mav = getDefaultModelAndView("marketplace/mashupList");

		List<MashupDTO> mashupList = marketplaceService.findAllMashup();

		mav.addObject("mashupList", mashupList);
		return mav;
	}
}