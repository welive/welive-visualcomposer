package com.mediigea.vme2.marketplace.dto;

import org.springframework.web.multipart.MultipartFile;

import com.mediigea.vme2.entity.Category;

public class MashupDTO {
	private Integer id; 	
	private Integer mockupId; 		
	private String name;	
	private String description;
	private String version;
	private String tags;  
	private String authorGroup;
	private String pathMashupFile;
	private Integer categoryId;
	private MultipartFile image;
	private String pathImage;
	private String categoryName;
	private Category category;	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getAuthorGroup() {
		return authorGroup;
	}
	public void setAuthorGroup(String authorGroup) {
		this.authorGroup = authorGroup;
	}
	public String getPathMashupFile() {
		return pathMashupFile;
	}
	public void setPathMashupFile(String pathMashupFile) {
		this.pathMashupFile = pathMashupFile;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public MultipartFile getImage() {
		return image;
	}
	public void setImage(MultipartFile image) {
		this.image = image;
	}
	public String getPathImage() {
		return pathImage;
	}
	public void setPathImage(String pathImage) {
		this.pathImage = pathImage;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	/**
	 * @return the mockupId
	 */
	public Integer getMockupId() {
		return mockupId;
	}
	/**
	 * @param mockupId the mockupId to set
	 */
	public void setMockupId(Integer mockupId) {
		this.mockupId = mockupId;
	}
}
