package com.mediigea.vme2.marketplace.dto;

import java.util.List;

import com.mediigea.vme2.entity.Category;

public class CategoryDTO {
	private Integer id; 		
	private Category parentCategory;
	private String description;
	private List<CategoryDTO> childrenCategories ;		  	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Category getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(Category parentCategory) {
		this.parentCategory = parentCategory;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<CategoryDTO> getChildrenCategories() {
		return childrenCategories;
	}
	public void setChildrenCategories(List<CategoryDTO> childrenCategories) {
		this.childrenCategories = childrenCategories;
	}
	
	
}
