package com.mediigea.vme2.marketplace.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.config.ResourcesMapper;
import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dao.AccountDAO;
import com.mediigea.vme2.entity.Activity;
import com.mediigea.vme2.entity.Category;
import com.mediigea.vme2.entity.Image;
import com.mediigea.vme2.entity.Mashup;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.marketplace.dao.CategoryDAO;
import com.mediigea.vme2.marketplace.dao.ImageDAO;
import com.mediigea.vme2.marketplace.dao.MashupDAO;
import com.mediigea.vme2.marketplace.dto.CategoryDTO;
import com.mediigea.vme2.marketplace.dto.MashupDTO;

public class MarketplaceServiceImpl implements MarketplaceService {

	@Autowired
	private CategoryDAO categoryDao;

	@Autowired
	private MashupDAO mashupDao;

	@Autowired
	private ImageDAO imageDao;
	
	@Autowired	
	private AccountDAO accountDAO;
	
	private ResourcesMapper resourcesMapper;

	@Override
	public void addCategory(Category category) {

		categoryDao.create(category);

	}

	@Override
	public List<CategoryDTO> findAllCategory() {
		List<Category> list = categoryDao.getAll();
		List<CategoryDTO> outputList = new ArrayList<CategoryDTO>();
		for (Category category : list) {
			CategoryDTO categoryout = new CategoryDTO();
			categoryout.setId(category.getId());
			categoryout.setDescription(category.getDescription());
		
			categoryout.setParentCategory(category.getParentCategory());
			outputList.add(categoryout);
		}

		return outputList;
	}

	@Override
	public CategoryDTO createTreeCategory() {
		Category category = categoryDao.getById(0);
		CategoryDTO tmp = new CategoryDTO();
		tmp = pupolateCategoryDTO(category);
		return tmp;
	}

	private CategoryDTO pupolateCategoryDTO(Category category) {
		CategoryDTO categoryout = new CategoryDTO();
		categoryout.setId(category.getId());
		categoryout.setDescription(category.getDescription());
		ArrayList<CategoryDTO> dummy = new ArrayList<CategoryDTO>();

		for (Category c : category.getChildrenCategories()) {
			dummy.add(pupolateCategoryDTO(c));
		}
		categoryout.setChildrenCategories(dummy);
		return categoryout;
	}

	@Override
	public MashupDTO readMashup(Integer id) {
		Mashup mashup = mashupDao.getById(id);
		
		MashupDTO outputMashup = new MashupDTO();
		if (mashup != null) {
			outputMashup.setAuthorGroup(mashup.getAuthorGroup());
			outputMashup.setCategory(mashup.getCategory());
			outputMashup.setDescription(mashup.getDescription());
			outputMashup.setId(mashup.getId());
			outputMashup.setName(mashup.getName());
			outputMashup.setPathMashupFile(mashup.getPathMashupFile());
			outputMashup.setTags(mashup.getTags());
			outputMashup.setVersion(mashup.getVersion());
			

		}
		return outputMashup;

	}

	@Override
	public Mashup saveUpdateMashup(MashupDTO mashupDTO) {
		Mashup mashup;
		Image image = new Image();

		if (mashupDTO.getId() != null) {
			mashup = mashupDao.getById(mashupDTO.getId());

		} else {
			mashup = new Mashup();
			mashup.setId(mashupDTO.getId());
		}
		mashup.setAuthorGroup(mashupDTO.getAuthorGroup());
		mashup.setCategory(mashupDTO.getCategory());
		mashup.setDescription(mashupDTO.getDescription());
		mashup.setName(mashupDTO.getName());
		if(mashupDTO.getPathMashupFile()!=null) mashup.setPathMashupFile(mashupDTO.getPathMashupFile());
		mashup.setTags(mashupDTO.getTags());
		mashup.setVersion(mashupDTO.getVersion());
		image.setImageType("L");

		image.setPathfileImage(mashupDTO.getPathImage());
		if (mashupDTO.getId() != null) {
			mashupDao.update(mashup);

			mashup.getImageList();
			if (mashupDTO.getPathImage() != null) {
				for (Image img : mashup.getImageList()) {
					img.setPathfileImage(mashupDTO.getPathImage());
					imageDao.update(img);
				}
			}

		} else {
			mashupDao.create(mashup);
			image.setMashup(mashup);
			imageDao.create(image);
			image = new Image();
			image.setMashup(mashup);
			image.setImageType("S");
			image.setPathfileImage(mashupDTO.getPathImage());
			imageDao.create(image);
		}

		return mashup;
	}

	@Override
	public List<MashupDTO> findAllMashup() {
		List<MashupDTO> outputListMashup = new ArrayList<MashupDTO>();
		List<Mashup> mashupList;
		mashupList = mashupDao.getAll();

		for (Mashup m : mashupList) {
			MashupDTO mashup = new MashupDTO();

			mashup.setAuthorGroup(m.getAuthorGroup());
			mashup.setCategory(m.getCategory());
			mashup.setDescription(m.getDescription());
			mashup.setId(m.getId());
			mashup.setName(m.getName());
			mashup.setPathMashupFile(m.getPathMashupFile());
			mashup.setTags(m.getTags());
			mashup.setVersion(m.getVersion());
			outputListMashup.add(mashup);

		}
		return outputListMashup;
	}

	@Override
	public void deleteMashup(Integer id) {
		String projectFile;
		String imageFileS = resourcesMapper.getUploadsImageMarketplace();
		String imageFileL = imageFileS;

		Mashup mashup = mashupDao.getById(id);
		List<Image> images = mashup.getImageList();
		for (Image image : images) {
			if (image.getImageType().equalsIgnoreCase("L")) {
				imageFileL += image.getPathfileImage();
			} else {
				imageFileS += image.getPathfileImage();
			}
			imageDao.deleteById(image.getId());
		}
		mashupDao.delete(mashup);

		projectFile = resourcesMapper.getUploadsMashupMarketplace()
				+ mashup.getPathMashupFile();

		File toDelete = new File(imageFileL);
		if (toDelete.exists())
			toDelete.delete();
		toDelete = new File(imageFileS);
		if (toDelete.exists())
			toDelete.delete();
		toDelete = new File(projectFile);
		if (toDelete.exists())
			toDelete.delete();
	}

	public void setResourcesMapper(ResourcesMapper resourcesMapper) {
		this.resourcesMapper = resourcesMapper;
	}

	@Override
	public CategoryDTO readCategory(Integer id) {
		
		Category category = categoryDao.getById(id);
		
		CategoryDTO categoryout = new CategoryDTO();
		categoryout.setId(category.getId());
		
		categoryout.setDescription(category.getDescription());
		categoryout.setParentCategory(category.getParentCategory());

		return categoryout;

	}

	@Override
	public Category saveUpdateCategory(CategoryDTO categoryDTO, Profile p) {
		
		Category category;

		if (categoryDTO.getId() != null) {
			category = categoryDao.getById(categoryDTO.getId());

		} else {
			category = new Category();
			category.setId(categoryDTO.getId());
		}
		category.setDescription(categoryDTO.getDescription());
		if(categoryDTO.getId()!=null)
		{
			if(categoryDTO.getId()!=0)
				category.setParentCategory(categoryDTO.getParentCategory());
		}
		else
			category.setParentCategory(categoryDTO.getParentCategory());

		Activity ac = new Activity();
		
		if (categoryDTO.getId() != null) {
			categoryDao.update(category);
			ac.setActivityType(AbstractDAO.ActivityType.UPDATE_CATEGORY.getValue());
			ac.setDescription("Update Category:" + category.getDescription());
		} else {
			categoryDao.create(category);
			ac.setActivityType(AbstractDAO.ActivityType.CREATE_CATEGORY.getValue());
			ac.setDescription("Create Category:" + category.getDescription());		
		}
		
		ac.setProfile(p);
		accountDAO.createAudit(ac);

		
		return category;
	}

	@Override
	public void deleteCategory(Integer id, Profile p) {
		Category category=categoryDao.getById(id);
		categoryDao.delete(category);
		
		Activity ac = new Activity();
		ac.setActivityType(AbstractDAO.ActivityType.DELETE_CATEGORY.getValue());
		ac.setDescription("Delete Category:" + category.getDescription());
		ac.setProfile(p);
		accountDAO.createAudit(ac);
		
	}

}
