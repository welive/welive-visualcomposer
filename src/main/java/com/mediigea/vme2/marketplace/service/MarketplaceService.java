package com.mediigea.vme2.marketplace.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.config.ResourcesMapper;
import com.mediigea.vme2.entity.Category;
import com.mediigea.vme2.entity.Mashup;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.marketplace.dto.CategoryDTO;
import com.mediigea.vme2.marketplace.dto.MashupDTO;

@Transactional
public interface MarketplaceService {

	public void addCategory(Category category);

	public List<CategoryDTO> findAllCategory();
	
	public List<MashupDTO> findAllMashup();
	
	public CategoryDTO createTreeCategory();

	public MashupDTO readMashup(Integer id);
	
	public CategoryDTO readCategory(Integer id);

	public Mashup saveUpdateMashup(MashupDTO mashupDTO);
	
	public Category saveUpdateCategory(CategoryDTO categoryDTO, Profile p);
	
	public void deleteMashup(Integer id);
	
	public void deleteCategory(Integer id, Profile p);
	
	public void setResourcesMapper(ResourcesMapper resourcesMapper);
}
