package com.mediigea.vme2.marketplace.dao;

import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.entity.Mashup;
public class MashupDAO extends AbstractDAO<Mashup> {

	public MashupDAO() {
		super(Mashup.class);
	}
	
}

