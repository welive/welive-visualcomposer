package com.mediigea.vme2.marketplace.dao;

import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.entity.Image;
public class ImageDAO extends AbstractDAO<Image> {

	public ImageDAO() {
		super(Image.class);
	}
	
}

