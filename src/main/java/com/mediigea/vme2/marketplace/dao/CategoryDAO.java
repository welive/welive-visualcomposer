package com.mediigea.vme2.marketplace.dao;

import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.entity.Category;
public class CategoryDAO extends AbstractDAO<Category> {

	public CategoryDAO() {
		super(Category.class);
	}
	
}

