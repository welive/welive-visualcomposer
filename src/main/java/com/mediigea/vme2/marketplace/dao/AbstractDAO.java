package com.mediigea.vme2.marketplace.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.google.common.base.Preconditions;

@Repository
public abstract class AbstractDAO<T extends Serializable> {

	private Class<T> clazz;
	@PersistenceContext(unitName="marketplaceSessionFactoryPU")
	private EntityManager marketplaceSession;

	public AbstractDAO(final Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	public T getById(final Integer id) {
		Preconditions.checkArgument(id != null);
		return getMarketplaceSession().find(clazz, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		return getMarketplaceSession().createQuery("from " + clazz.getName()).getResultList();
	}
	
	public List<T> getAllBy(String hql, HashMap<String, Object> parameters) {
		return getAllBy(hql, parameters, 0, 0);		
	}

	@SuppressWarnings("unchecked")
	public List<T> getAllBy(String hql, HashMap<String, Object> parameters, int limit, int offset) {
		
		if(hql == null || hql.isEmpty())
			return null;
		
		Query q = getMarketplaceSession().createQuery(hql);
		if (limit > 0){
			q.setMaxResults(limit);
		}
		if (offset > 0){
			q.setFirstResult(offset);
		}
		
		if(parameters != null && !parameters.isEmpty()){
			
			Object[] keys = parameters.keySet().toArray();
			int size = keys.length;
			
			for(int i=0; i<size; i++)
				q.setParameter(keys[i].toString(), parameters.get(keys[i]));
		}
		
		return q.getResultList();
	}
	
	public void create(final T entity) {
		Preconditions.checkNotNull(entity);
		getMarketplaceSession().persist(entity);
	}
	


	public T update(final T entity) {
		Preconditions.checkNotNull(entity);
		return (T) getMarketplaceSession().merge(entity);
	}

	public void delete(final T entity) {
		Preconditions.checkNotNull(entity);
		getMarketplaceSession().remove(entity);
	}

	public void deleteById(final Integer entityId) {
		final T entity = getById(entityId);
		Preconditions.checkState(entity != null);
		delete(entity);
	}

	/**
	 * @return the entityManager
	 */
	public EntityManager getMarketplaceSession() {
		return marketplaceSession;
	}

	/**
	 * @param entityManager
	 *            the entityManager to set
	 */
	public void setMarketplaceSession(EntityManager marketplaceSession) {
		this.marketplaceSession = marketplaceSession;
	}
}