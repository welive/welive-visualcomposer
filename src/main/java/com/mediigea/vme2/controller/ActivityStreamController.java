package com.mediigea.vme2.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.abdera2.activities.model.Activity;
import org.apache.abdera2.activities.model.Collection;
import org.apache.abdera2.activities.model.Collection.CollectionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mediigea.vme2.exceptions.RestException;
import com.mediigea.vme2.service.ActivityService;

@Controller
@RequestMapping(value = "/activitystream")
public class ActivityStreamController extends BaseController{
	private static final Logger logger = LoggerFactory.getLogger(ActivityStreamController.class);
	
	@Autowired
	private ActivityService activityService;
	
	@Autowired
	private MessageSource messageSource;	
	

	@RequestMapping(value = "/list.json", method = RequestMethod.GET, produces = "application/json")
	public void getBindingInputParams(HttpServletRequest request, HttpServletResponse response) throws RestException {
		
		PrintWriter out;
		List<Activity> streamList = null;
		try {
			out = response.getWriter();
			streamList = activityService.findAllActivity();						
			
			if ((streamList != null) && (streamList.size()>0)) {
				CollectionBuilder<Activity> collection = Collection.<Activity>makeCollection();
				
				for (Activity streamActivity : streamList) {
					collection.item(streamActivity);
					//streamActivity.writeTo(out);
				}
				
				Collection<Activity> activities = collection.get();
				
				activities.writeTo(out);				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}			
	
	@RequestMapping(value = "{id}/list.json", method = RequestMethod.GET, produces = "application/json")
	public void getBindingInputParams(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) throws RestException {
		
		PrintWriter out;
		List<Activity> streamList = null;
		try {
			out = response.getWriter();
			streamList = activityService.findActivityByProfile(id);
			if ((streamList != null) && (streamList.size()>0)) {
				CollectionBuilder<Activity> collection = Collection.<Activity>makeCollection();
				
				for (Activity streamActivity : streamList) {
					collection.item(streamActivity);
					//streamActivity.writeTo(out);
				}
				
				Collection<Activity> activities = collection.get();
				
				activities.writeTo(out);				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}			
		
	
	
}
