
package com.mediigea.vme2.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.dto.BlackboxDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.exceptions.UsedBlackboxException;
import com.mediigea.vme2.service.BlackboxService;
import com.mediigea.vme2.util.VmeConstants;

/**
 * Controller to receive request about management of the blackbox operations 
 * @author Carmine Ruffino
 *
 */

@Controller
@RequestMapping(value = "/user/blackbox")
public class BlackboxController extends BaseController {
	
	@Autowired
	private BlackboxService blackboxService;

	private static final Logger logger = LoggerFactory.getLogger(BlackboxController.class);
	
	/**
	 * Method to add a blackbox from an existing project
	 * @param projectId - mashup project identifier
	 * @return
	 */
	@RequestMapping(value = "/{id}/addblackbox", method = RequestMethod.GET)
	public ModelAndView addBlackbox(@PathVariable("id") int projectId) {

		logger.info("Create a blackbox for selected project: "+projectId);

		ModelAndView mav = getDefaultModelAndView("redirect:/user/project/list");
		
		this.blackboxService.addBlackbox(projectId);
		
		return mav;
	}
	
	/**
	 * Method to remove a blackbox
	 * @param blackboxId - blackbox identifier
	 * @return
	 */
	@RequestMapping(value = "/{id}/deleteblackbox", method = RequestMethod.GET)
	public ModelAndView deleteBlackbox(@PathVariable("id") int blackboxId) {

		logger.info("Delete a blackbox by blackbox id: "+blackboxId);

		ModelAndView mav = getDefaultModelAndView("redirect:/user/project/list");
		
		try {
			this.blackboxService.deleteBlackbox(blackboxId);
		} catch (UsedBlackboxException e) {
			logger.error(e.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.error.blackbox.delete");
		}		
		
		return mav;
	}	
	
	/**
	 * Method to get a list of blackbox by project
	 * @param project_id - mashup project identifier
	 * @return
	 */
	@RequestMapping(value = "/{project_id}/list.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<BlackboxDTO> listJSON(@PathVariable("project_id") int project_id) {

		logger.info("Getting blackbox list by project: "+project_id);

		AccountDTO a = getLoggedUser();
		if (a == null || a.getRole() != VmeConstants.ACCOUNT_ROLE_USER) {

			logger.error("Logged Account is bad");
			return new ArrayList<BlackboxDTO>();
		}

		return blackboxService.findAllByProfileAsDTO(a.getProfile(), project_id);
	}
}
