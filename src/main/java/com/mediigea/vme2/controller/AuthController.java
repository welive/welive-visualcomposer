package com.mediigea.vme2.controller;

import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.spring.bean.SocialAuthTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mediigea.vme2.auth.AuthenticationException;
import com.mediigea.vme2.auth.RegistrationException;
import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dao.AccountDAO;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Activity;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.service.AccountService;
import com.mediigea.vme2.service.ProfileService;
import com.mediigea.vme2.util.EmailSender;
import com.mediigea.vme2.util.VmeConstants;

@Controller
@RequestMapping(value = "/auth")
public class AuthController extends BaseController {

	@Autowired
	private SocialAuthTemplate socialAuthTemplate;

	@Autowired
	private AccountService accountService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private EmailSender emailSender;

	@Autowired
	private MessageSource messageSource;
	
	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

	/**
	 * 
	 */
	private void processIvalidationSession() {

		logger.info("Invalidate session");

		SocialAuthManager manager = socialAuthTemplate.getSocialAuthManager();

		if (manager != null)
			manager.disconnectProvider(manager.getCurrentAuthProvider().getProviderId());
		
		
		AccountDTO lu = getLoggedUser();
		
		if (lu != null) { 
			Account loggetAccount = accountService.getById(new Integer(getLoggedUser().getId()));
			if (loggetAccount != null) {
	
				loggetAccount.setToken(null);
				loggetAccount.setUpdatedAt(new Date());
	
				accountService.updateAccount(loggetAccount);
				accountService.logoutUser(loggetAccount);
			}
		}
		httpSession.invalidate();
		
	}

	private ModelAndView processUserRegistration(Account account, int accountType, boolean social, Locale locale) {

		logger.info("Process user Registration");

		// ModelAndView mav = new ModelAndView("redirect:/auth/logout");

		// if (accountType == VmeConstants.ACCOUNT_ROLE_ADMIN)
		// new ModelAndView("redirect:/auth/admin/login");
		// else
		// new ModelAndView("redirect:/auth/user/login");

		if (account == null) {

			logger.debug("Account object is not set");

			ModelAndView mav = new ModelAndView("user/reg_fail");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.registration.error");

			return mav;
		}

		if (social) {

			logger.debug("Setting account from social form");

			SocialAuthManager manager = socialAuthTemplate.getSocialAuthManager();

			try {

				org.brickred.socialauth.Profile profile = null;
				profile = manager.getCurrentAuthProvider().getUserProfile();

				account.setValidatedOauthId(profile.getValidatedId());
				account.setProvider(profile.getProviderId());
			} catch (Exception gnEx) {

				logger.error("Exception gnEx: " + gnEx.getMessage());
			}
		} else {

			logger.debug("Setting account from classic form");
			account.setProvider(VmeConstants.AUTH_PROVIDER_VME);
		}

		Account a = null;
		try {

			a = accountService.registrationTo(account, accountType);
			logger.debug("Registration success");

			Profile p = a.getProfile();
			String name = p.getFirstName() + " " + p.getLastName();

			String subj = messageSource.getMessage("email.registration.subject", null, locale);
			String msg = messageSource.getMessage("email.registration.message", new Object[] { name }, locale);

			emailSender.sendMail(a.getEmail(), subj, msg);

			processIvalidationSession();

		} catch (RegistrationException regEx) {
			// logger.error("RegistrationException: " + e.getMessage());

			if (social == true) {
				ModelAndView mav = new ModelAndView("user/social");
				mav.addObject("account", account);

				mav.addObject(VmeConstants.MESSAGE_TEXT, regEx.getMessage());

				return mav;

			} else {
				ModelAndView mav = new ModelAndView("user/registration");
				mav.addObject("account", account);
				mav.addObject(VmeConstants.MESSAGE_TEXT, regEx.getMessage());

				return mav;
			}

			// mav = new ModelAndView("user/reg_fail");

		} catch (Exception e) {
			logger.error("RegistrationException: " + e.getMessage());
			ModelAndView mav = new ModelAndView("user/reg_fail");
			mav.addObject("error_message", e.getMessage());
			return mav;

		}

		ModelAndView mav = new ModelAndView("user/reg_success");
		return mav;

	}

	@RequestMapping(value = "/user/login", method = RequestMethod.GET)
	public ModelAndView userLogin() {

		logger.info("Auth user login view");

		Account a = new Account();
		a.setProfile(new Profile());

		ModelAndView mav = new ModelAndView("user/login");
		mav.addObject("account", a);
		return mav;
	}

	@RequestMapping(value = "/user/login/execute", method = RequestMethod.POST)
	public ModelAndView executeLoginFromPlatform(@ModelAttribute Account account, BindingResult result) throws Exception {

		logger.debug("Execute User Login");
		try {
			Account loggedAccount = this.accountService.verifyAccount(account.getEmail(), account.getPsw(), VmeConstants.LOGIN_TYPE_USER);
			logger.debug("User login is correct");
			httpSession.invalidate();
			httpSession.setAttribute(VmeConstants.LOGGED_ACCOUNT, loggedAccount);

			String loginView = VmeConstants.ROUTE_HOME_USER;

			String requestedUrl = (String) httpSession.getAttribute(VmeConstants.REQUESTED_URL);
			if (requestedUrl != null && !requestedUrl.isEmpty())
				loginView = requestedUrl;

			return new ModelAndView("redirect:" + loginView);

		} catch (AuthenticationException e) {
			logger.error(e.getMessage());
			ModelAndView mav = new ModelAndView("redirect:/auth/user/login"); 
			mav.addObject(VmeConstants.MESSAGE_TEXT, e.getMessage());

			return mav;
		}
	}

	@RequestMapping(value = "/admin/login", method = RequestMethod.GET)
	public ModelAndView adminLogin() throws Exception {

		logger.info("Auth admin login view");

		Account a = new Account();
		a.setProfile(new Profile());

		ModelAndView mav = new ModelAndView("admin/login");
		mav.addObject("account", a);
		return mav;
	}

	@RequestMapping(value = "/admin/login/execute", method = RequestMethod.POST)
	public ModelAndView executeAdminLoginFromPlatform(@ModelAttribute Account account, Locale locale) throws Exception {
		logger.debug("Execute Admin Login");
		try {
			Account loggedAccount = this.accountService.verifyAccount(account.getEmail(), account.getPsw(), VmeConstants.LOGIN_TYPE_ADMIN);
			logger.debug("Admin login is correct");
			httpSession.invalidate();
			httpSession.setAttribute(VmeConstants.LOGGED_ACCOUNT, loggedAccount);
			return new ModelAndView("redirect:" + VmeConstants.ROUTE_HOME_ADMIN);
		} catch (AuthenticationException e) {
			logger.error(e.getMessage());
			ModelAndView mav = new ModelAndView("redirect:/admin/login"); // ?alert=error&message="
																			// +
																			// URLEncoder.encode(ae.getMessage()));
			mav.addObject(VmeConstants.MESSAGE_TEXT, e.getMessage());

			return mav;
		}
	}

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/user/registration", method = RequestMethod.GET)
	public ModelAndView registration() {

		logger.info("User Registration View");

		Account a = new Account();
		a.setProfile(new Profile());

		ModelAndView mav = new ModelAndView("user/registration");
		mav.addObject("account", a);

		return mav;
	}

	@RequestMapping(value = "/user/registration/social", method = RequestMethod.GET)
	public ModelAndView socialProfileFromProvider() throws Exception {

		org.brickred.socialauth.Profile profile = null;

		try {
			SocialAuthManager manager = socialAuthTemplate.getSocialAuthManager();
			profile = manager.getCurrentAuthProvider().getUserProfile();
		} catch (Exception gnEx) {
			logger.error("Exception gnEx: " + gnEx.getMessage());
		}

		logger.info("Social auth view");

		Account a = new Account();
		Profile p = new Profile();
		a.setProfile(p);

		if (profile != null) {
			a.setValidatedOauthId(profile.getValidatedId());
			a.setProvider(profile.getProviderId());
			a.setEmail(profile.getEmail() != null ? profile.getEmail() : "");
			p.setFirstName(profile.getFirstName() != null ? profile.getFirstName() : "");
			p.setLastName(profile.getLastName() != null ? profile.getLastName() : "");
			p.setEmail(profile.getEmail() != null ? profile.getEmail() : "");
		}

		ModelAndView mav = new ModelAndView("user/social");
		mav.addObject("account", a);

		return mav;
	}

	@RequestMapping(value = "/user/registration/social/execute", method = RequestMethod.POST)
	public ModelAndView executeSocialProfileFromProvider(@ModelAttribute Account account, BindingResult result, Locale locale) throws Exception {

		logger.info("Execute Social auth registration");

		return processUserRegistration(account, VmeConstants.ACCOUNT_ROLE_USER, true, locale);
	}

	@RequestMapping(value = "/user/registration/execute", method = RequestMethod.POST)
	public ModelAndView executeRegistrationFromPlatform(@ModelAttribute Account account, BindingResult result, Locale locale) throws Exception {

		logger.info("Execute User Registration From Platform");

		return processUserRegistration(account, VmeConstants.ACCOUNT_ROLE_USER, false, locale);
	}

	@RequestMapping(value = "/authsuccess")
	public ModelAndView authSuccess(final HttpServletRequest request) {

		logger.info("Social Auth success");

		SocialAuthManager manager = socialAuthTemplate.getSocialAuthManager();
		AuthProvider provider = null;

		if (manager == null) {

			logger.error("Socialauth manager is null");
			return new ModelAndView("redirect:/auth/user/login?alert=error&message=Error");
		}

		provider = manager.getCurrentAuthProvider();

		if (provider == null) {

			logger.error("Socialauth provider is null");
			return new ModelAndView("redirect:/auth/user/login?alert=error&message=Error");
		}

		try {
			Account loggedAccount = this.accountService.verifyAccount(provider.getUserProfile().getValidatedId());
			logger.debug("Account is correct");

			loggedAccount.setToken(provider.getAccessGrant().getKey());
			loggedAccount.setUpdatedAt(new Date());

			accountService.updateAccount(loggedAccount);
			httpSession.invalidate();
			httpSession.setAttribute(VmeConstants.LOGGED_ACCOUNT, loggedAccount);

			String requestedUrl = (String) httpSession.getAttribute(VmeConstants.REQUESTED_URL);

			ModelAndView mav = null;
			if (requestedUrl != null && !requestedUrl.isEmpty())
				mav = new ModelAndView("redirect:" + requestedUrl);
			else
				mav = new ModelAndView("redirect:/user/project/list");
			return mav;
		} catch (AuthenticationException ae) {
			if (ae.getMessage().equals(VmeConstants.MESSAGE_ACCOUNT_NOT_FOUND)) {
				logger.info("Social account non found -> register");
				return new ModelAndView("redirect:/auth/user/registration/social");
			} else {
				logger.error("Authentication failed: " + ae.getMessage());
				ModelAndView mav = new ModelAndView("redirect:/auth/user/login"); // ?alert=error&message="
																					// +
																					// URLEncoder.encode(ae.getMessage()));
				mav.addObject(VmeConstants.MESSAGE_TEXT, ae.getMessage());

				return mav;
			}
		} catch (Exception e) {
			logger.error("Authentication failed: " + e.getMessage());
			ModelAndView mav = new ModelAndView("redirect:/auth/user/login");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "Authentication failed");

			return mav;
		}
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpSession session) {

		logger.info("Logout");

		processIvalidationSession();

		return new ModelAndView("redirect:/j_spring_cas_security_logout");
	}

	@RequestMapping(value = "/authdenied", method = RequestMethod.GET)
	public String authDenied() {

		return "authDenied";
	}
	
	@RequestMapping(value = "/denied", method = RequestMethod.GET)
	public ModelAndView securityAccessDenied() {
		ModelAndView mav = new ModelAndView("public/denied");
		
		mav.addObject(VmeConstants.MESSAGE_TEXT, "message.access.denied");
		
		return mav;
	}
}
