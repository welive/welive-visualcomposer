package com.mediigea.vme2.controller;

import it.eng.PropertyGetter;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.ckan.CKANException;
import org.ckan.Client;
import org.ckan.Connection;
import org.ckan.Dataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mediigea.vme2.config.ResourcesMapper;
import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.dto.ProfileDTO;
import com.mediigea.vme2.dto.WorkgroupDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.AuthProviderType;
import com.mediigea.vme2.entity.CkanServer;
import com.mediigea.vme2.entity.OpenDataResource;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.marketplace.dto.CategoryDTO;
import com.mediigea.vme2.marketplace.service.MarketplaceService;
import com.mediigea.vme2.service.AccountService;
import com.mediigea.vme2.service.ActivityService;
import com.mediigea.vme2.service.AuthManagerService;
import com.mediigea.vme2.service.CkanServerService;
import com.mediigea.vme2.service.OpenDataService;
import com.mediigea.vme2.service.ProfileService;
import com.mediigea.vme2.service.RestOperationService;
import com.mediigea.vme2.service.ServiceCatalogService;
import com.mediigea.vme2.service.SoapOperationService;
import com.mediigea.vme2.service.WorkgroupService;
import com.mediigea.vme2.util.PagedSearch;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.viewmodel.AddOpenDataViewModel;
import com.mediigea.vme2.viewmodel.AddWadlViewModel;
import com.mediigea.vme2.viewmodel.AddWsdlServiceViewModel;

@Controller
@RequestMapping(value = "/admin")
public class AdminController extends BaseController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private WorkgroupService workgroupService;

	@Autowired
	private ServiceCatalogService catalogService;

	@Autowired
	private RestOperationService restService;

	@Autowired
	private SoapOperationService soapOperationService;

	@Autowired
	private AuthManagerService authManagerService;

	@Autowired
	private HttpSession httpSession;

	@Autowired
	private MarketplaceService marketplaceService;

	@Autowired
	private ResourcesMapper resourcesMapper;

	@Autowired
	private ActivityService activityService;

	@Autowired
	private OpenDataService openDataService;

	@Autowired
	private CkanServerService ckanServerService;
	
	@Autowired
	private MessageSource messageSource;

	private static final Logger logger = LoggerFactory
			.getLogger(AdminController.class);

	@InitBinder
	public void initBinder(WebDataBinder binder) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:m:s");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	// //////////////////////////////////////////

	@RequestMapping(value = "/workgroup/{id}/members/add", method = RequestMethod.POST)
	public ModelAndView workgroupMembersAdd(@PathVariable int id,
			@RequestParam String addmembersId) {
		ModelAndView mav = new ModelAndView("redirect:/admin/workgroup/" + id
				+ "/details");
		try {
			logger.debug("addmembersId:" + addmembersId);
			for (String s : addmembersId.split(",")) {
				Integer pId = Integer.valueOf(s);
				try {
					workgroupService.memberAdd(id, pId);

					String wrkgr = workgroupService.findWorkgroup(id).getName();
					Profile p = profileService.findProfile(pId);
					String person = p.getFirstName() + " " + p.getLastName()
							+ " (" + p.getEmail() + ")";

					AccountDTO adto = getLoggedUser();
					String activityDescription = "Add Member " + person
							+ " to group " + wrkgr;
					activityService.activityStreamRecordSave(adto.getProfile(),
							AbstractDAO.ActivityType.ADD_MEMBER_TO_WORKGROUP
									.getValue(), activityDescription);

				} catch (DataIntegrityViolationException e) {
					logger.debug(e.getMessage());
				}
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.workgroup.members.add.error");

		}
		return mav;
	}

	@RequestMapping(value = "/workgroup/{id}/members/{memberId}/remove", method = RequestMethod.GET)
	public ModelAndView workgroupMembersRemove(@PathVariable Integer id,
			@PathVariable Integer memberId) {
		ModelAndView mav = new ModelAndView("redirect:/admin/workgroup/" + id
				+ "/details");
		try {

			workgroupService.memberRemove(id, memberId);

			String wrkgr = workgroupService.findWorkgroup(id).getName();
			Profile p = profileService.findProfile(memberId);
			String person = p.getFirstName() + " " + p.getLastName() + " ("
					+ p.getEmail() + ")";

			AccountDTO adto = getLoggedUser();
			String activityDescription = "Remove Member " + person
					+ " from group " + wrkgr;
			activityService.activityStreamRecordSave(adto.getProfile(),
					AbstractDAO.ActivityType.REMOVE_MEMBER_FROM_WORKGROUP
							.getValue(), activityDescription);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.workgroup.members.remove.error");

		}
		return mav;
	}

	@RequestMapping(value = "/workgroup/list", method = RequestMethod.GET)
	public ModelAndView workgroupList(final HttpServletRequest request) {

		logger.info("Workgroups View");

		ModelAndView mav = new ModelAndView("admin/workgroup/list");

		List<Workgroup> workgroupList = workgroupService.findAllWithProfiles();

		mav.addObject("workgroupList", workgroupList);
		mav.addObject("loggeduser", getLoggedUser());

		return mav;
	}

	@RequestMapping(value = "/workgroup/{id}/details", method = RequestMethod.GET)
	public ModelAndView workgroupDetails(@PathVariable("id") int id) {

		logger.info("Detail workgroup view");

		ModelAndView mav = null;

		try {

			mav = new ModelAndView("admin/workgroup/detail");

			Workgroup w = workgroupService.findWorkgroup(id);
			List<Profile> profiles = profileService.findByWorkgroup(w);
			mav.addObject("profiles", profiles);
			mav.addObject("workgroup", w);
			mav.addObject("loggeduser", getLoggedUser());
			
		} catch (Exception ex) {
			logger.error(ex.getMessage());

			mav = new ModelAndView("redirect:/admin/workgroup/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.find.error");

		}

		return mav;
	}

	@RequestMapping(value = "/workgroup/add", method = RequestMethod.GET)
	public ModelAndView workgroupAdd() {

		logger.info("Add workgroup view");

		ModelAndView mav = new ModelAndView("admin/workgroup/add");

		mav.addObject("workgroup", new Workgroup());
		mav.addObject("loggeduser", getLoggedUser());

		return mav;
	}

	@RequestMapping(value = "/workgroup/add/execute", method = RequestMethod.POST)
	public ModelAndView workgroupAddExecute(@ModelAttribute Workgroup workgroup) {

		logger.info("Execute Add workgroup");

		workgroupService.addWorkgroup(workgroup);

		AccountDTO adto = getLoggedUser();
		String activityDescription = "Created workgroup " + workgroup.getName();
		activityService.activityStreamRecordSave(adto.getProfile(),
				AbstractDAO.ActivityType.CREATE_WORKGROUP.getValue(),
				activityDescription);

		ModelAndView mav = new ModelAndView("redirect:/admin/workgroup/list");

		return mav;
	}

	@RequestMapping(value = "/workgroup/{id}/edit", method = RequestMethod.GET)
	public ModelAndView workgroupEdit(@PathVariable("id") int id) {

		logger.info("Edit workgroup view");

		ModelAndView mav = null;

		Workgroup w = workgroupService.findWorkgroup(id);

		if (w == null) {

			mav = new ModelAndView("redirect:/admin/workgroup/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.edit.error");

		} else {

			mav = new ModelAndView("admin/workgroup/edit");
			mav.addObject("workgroup", w);
		}

		return mav;
	}

	@RequestMapping(value = "/workgroup/edit/execute", method = RequestMethod.POST)
	public ModelAndView workgroupEditExecute(
			@ModelAttribute("workgroup") Workgroup workgroup,
			BindingResult result) {

		logger.info("Execute Edit workgroup");

		ModelAndView mav = null;
		try {
			Workgroup w = workgroupService.updateWorkgroup(workgroup);

			AccountDTO adto = getLoggedUser();
			String activityDescription = "Updated workgroup "
					+ workgroup.getName();
			activityService.activityStreamRecordSave(adto.getProfile(),
					AbstractDAO.ActivityType.UPDATE_WORKGROUP.getValue(),
					activityDescription);

			mav = new ModelAndView("redirect:/admin/workgroup/" + w.getId()
					+ "/details");
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = new ModelAndView("admin/workgroup/edit");
			mav.addObject("workgroup", workgroup);
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.workgroup.editing.error");

		}

		return mav;
	}

	@RequestMapping(value = "/workgroup/{id}/delete", method = RequestMethod.GET)
	public ModelAndView workgroupDelete(@PathVariable("id") int id) {
		Workgroup w = workgroupService.findWorkgroup(id);
		ModelAndView mav = getDefaultModelAndView("admin/workgroup/delete");

		mav.addObject("workgroup", w);
		return mav;

	}

	@RequestMapping(value = "/workgroup/{id}/delete/execute", method = RequestMethod.POST)
	public ModelAndView workgroupDeleteExecute(@PathVariable("id") int id) {

		ModelAndView mav = new ModelAndView("redirect:/admin/workgroup/list");

		try {
			Workgroup w = workgroupService.findWorkgroup(id);
			workgroupService.deleteWorkgroup(id);

			AccountDTO adto = getLoggedUser();
			String activityDescription = "Deleted workgroup " + w.getName();
			activityService.activityStreamRecordSave(adto.getProfile(),
					AbstractDAO.ActivityType.DELETE_WORKGROUP.getValue(),
					activityDescription);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");

		}

		return mav;
	}

	@RequestMapping(value = "/workgroup/search.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	PagedSearch<WorkgroupDTO> workgroupSearchJson(
			@RequestParam String searchterm,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "20") int pagesize) {
		PagedSearch<WorkgroupDTO> result = null;
		try {
			result = workgroupService.pagedSearch(searchterm, page, pagesize);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			// result = new ProfilePagedSearchDTO();
		}

		return result;
	}

	// ///////////////////////////////////////////////////////

	@RequestMapping(value = "/profile/search.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	PagedSearch<ProfileDTO> profileSearchJson(@RequestParam String searchterm,
			@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "20") int pagesize) {

		PagedSearch<ProfileDTO> result = null;
		try {
			result = profileService.pagedSearch(searchterm, page, pagesize);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			// result = new ProfilePagedSearchDTO();
		}

		return result;
	}

	@RequestMapping(value = "/profile/{id}/workgroups/add", method = RequestMethod.POST)
	public ModelAndView profileWorkgroupsAdd(@PathVariable Integer id,
			@RequestParam String addgroupsId) {
		ModelAndView mav = new ModelAndView("redirect:/admin/profile/" + id
				+ "/details");
		try {
			logger.debug("addgroupsId:" + addgroupsId);

			for (String s : addgroupsId.split(",")) {
				Integer wId = Integer.valueOf(s);
				try {
					profileService.workgroupAdd(id, wId);
				} catch (DataIntegrityViolationException ex) {
					logger.debug(ex.getMessage());
				}
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.profile.workgroups.add.error");

		}
		return mav;
	}

	@RequestMapping(value = "/profile/{id}/workgroups/{workgroupId}/remove", method = RequestMethod.GET)
	public ModelAndView profileWorkgroupsRemove(@PathVariable Integer id,
			@PathVariable Integer workgroupId) {
		ModelAndView mav = new ModelAndView("redirect:/admin/profile/" + id
				+ "/details");
		try {

			profileService.workgroupRemove(id, workgroupId);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.profile.workgroups.remove.error");

		}
		return mav;
	}

	@RequestMapping(value = "/profile/admins", method = RequestMethod.GET)
	public ModelAndView profileAdmins() {

		List<Profile> profileList = profileService
				.findByRole(VmeConstants.ACCOUNT_ROLE_ADMIN);
		ModelAndView mav = getDefaultModelAndView("admin/profile/admins");
		mav.addObject("profileList", profileList);

		return mav;

	}

	@RequestMapping(value = "/profile/admins/add", method = RequestMethod.GET)
	public ModelAndView profileAdminsAdd() {

		Account a = new Account();
		Profile p = new Profile();
		p.setAccount(a);

		ModelAndView mav = getDefaultModelAndView("admin/profile/adminsadd");
		mav.addObject("profile", p);

		return mav;
	}

	@RequestMapping(value = "/profile/admins/add/execute", method = RequestMethod.POST)
	public ModelAndView profileAdminsAddExecute(@ModelAttribute Profile profile)
			throws Exception {

		// logger.info("Execute Admin Registraion From Platform");

		// return processUserRegistration(account,
		// VmeConstants.ACCOUNT_ROLE_ADMIN, false);

		ModelAndView mav = null;
		try {
			profileService.addAdministrator(profile);
			mav = new ModelAndView("redirect:/admin/profile/admins");

			AccountDTO adto = getLoggedUser();
			String activityDescription = "Created admin "
					+ profile.getFirstName() + " " + profile.getLastName()
					+ " (" + profile.getEmail() + ")";

			activityService.activityStreamRecordSave(adto.getProfile(),
					AbstractDAO.ActivityType.CREATE_ADMIN.getValue(),
					activityDescription);

		} catch (PersistenceException pex) {
			logger.error(pex.getMessage());
			mav = getDefaultModelAndView("admin/profile/adminsadd");
			mav.addObject("profile", profile);
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.administrator.add.duplicateemail.error");
			//
		} catch (Exception e) {
			logger.error(e.getMessage());
			mav = getDefaultModelAndView("admin/profile/adminsadd");
			mav.addObject("profile", profile);
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.administrator.add.error");
			//

		}

		return mav;

	}

	@RequestMapping(value = { "", "/", "/profile/list" }, method = RequestMethod.GET)
	public ModelAndView profileList() {

		List<Profile> profileList = profileService
				.findByRole(VmeConstants.ACCOUNT_ROLE_USER);
		// List<Workgroup> workgroupList = workgroupService.findAll();

		ModelAndView mav = getDefaultModelAndView("admin/profile/list");
		mav.addObject("profileList", profileList);
		// mav.addObject("workgroupList", workgroupList);

		return mav;
	}

	@RequestMapping(value = "/profile/{id}/details", method = RequestMethod.GET)
	public ModelAndView profileDetails(@PathVariable("id") int id) {

		logger.info("Profile Detail View");
		ModelAndView mav = null;
		try {
			Profile p = profileService.findProfile(id);
			mav = getDefaultModelAndView("admin/profile/detail");
			mav.addObject("profile", p);
			List<Workgroup> workgroups = workgroupService.findByProfile(p);
			mav.addObject("workgroups", workgroups);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = new ModelAndView("redirect:/admin/profile/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.profile.details.error");

		}

		return mav;
	}

	@RequestMapping(value = { "/profile/{id}/enable",
			"/profile/admins/{id}/enable" }, method = RequestMethod.GET)
	public ModelAndView profileEnable(@PathVariable("id") int id,
			HttpServletRequest request) {

		String redirect = request.getRequestURI().contains("/admins") ? "redirect:/admin/profile/admins"
				: "redirect:/admin/profile/" + id + "/details";

		ModelAndView mav = new ModelAndView(redirect);

		try {
			Profile p = profileService.findProfile(id);
			accountService.setStatus(p.getAccount().getId(), true);

		} catch (Exception e) {
			logger.error(e.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"profile.management.enable.error");

		}

		return mav;
	}

	@RequestMapping(value = { "/profile/{id}/disable",
			"/profile/admins/{id}/disable" }, method = RequestMethod.GET)
	public ModelAndView profileDisable(@PathVariable("id") int id,
			HttpServletRequest request) {

		String redirect = request.getRequestURI().contains("/admins") ? "redirect:/admin/profile/admins"
				: "redirect:/admin/profile/" + id + "/details";
		ModelAndView mav = new ModelAndView(redirect);

		try {
			Profile p = profileService.findProfile(id);
			accountService.setStatus(p.getAccount().getId(), false);
		} catch (Exception e) {
			logger.error(e.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"profile.management.enable.error");

		}

		return mav;
	}

	@RequestMapping(value = "/profile/{id}/delete", method = RequestMethod.GET)
	public ModelAndView profileDelete(@PathVariable("id") int id) {
		Profile p = profileService.findProfile(id);
		ModelAndView mav = getDefaultModelAndView("admin/profile/delete");
		mav.addObject("profile", p);
		return mav;
	}

	@RequestMapping(value = "/profile/{id}/delete/execute", method = RequestMethod.POST)
	public ModelAndView profileDeleteExecute(@PathVariable("id") int id) {

		logger.info("Execte Delete Profile");

		ModelAndView mav = new ModelAndView("redirect:/admin/profile/list");

		try {

			profileService.deleteProfile(id);

		} catch (Exception ex) {
			logger.error(ex.getMessage());

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");

		}

		return mav;
	}
	
	@RequestMapping(value = { "/profile/{id}/level/{level}",
	"/profile/admins/{id}/enable" }, method = RequestMethod.GET)
	public ModelAndView profileChangeLevel(@PathVariable("id") int id, @PathVariable("level") int level, HttpServletRequest request) 
	{	
		String redirect = "redirect:/admin/profile/" + id + "/details";
		
		ModelAndView mav = new ModelAndView(redirect);
		
		try {
			Profile p = profileService.findProfile(id);
			p.setLevel(level);
			profileService.updateProfile(p);
		
		} catch (Exception e) {
			logger.error(e.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"profile.management.enable.error");
		
		}
		
		return mav;
	}

	// ///////////////////////////////////////////////////////

	@RequestMapping(value = { "/servicecatalog", "/servicecatalog/list" }, method = RequestMethod.GET)
	public ModelAndView serviceList() {

		logger.info("Services View");
		ModelAndView mav = getDefaultModelAndView("admin/servicecatalog/list");
		List<ServiceCatalog> list = catalogService.findAdministratorsServices();
		mav.addObject("services", list);

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/{id}/details", method = RequestMethod.GET)
	public ModelAndView serviceDetails(@PathVariable("id") int id) {

		logger.info("Detail service View");

		ModelAndView mav = null;

		ServiceCatalog s = catalogService.findService(id);

		if (s == null) {

			logger.debug("Service Id is bad");

			mav = new ModelAndView("redirect:/admin/servicecatalog/list");

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.find.error");

		} else {

			mav = new ModelAndView("admin/servicecatalog/detail");
			mav.addObject("service", s);

			if (s.getType() == VmeConstants.SERVICECATALOG_TYPE_REST)
				mav.addObject("serviceResourceList",
						restService.findAllByService(s.getId()));
			else
				mav.addObject("soapOperationList",
						soapOperationService.findAllByService(s.getId()));
		}

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/{id}/delete", method = RequestMethod.GET)
	public ModelAndView serviceDelete(@PathVariable("id") int id) {

		logger.info("Execute Delete service");

		ModelAndView mav = new ModelAndView(
				"redirect:/admin/servicecatalog/list");

		AccountDTO a = getLoggedUser();

		Profile p = profileService.findProfileByServiceCatalog(id);

		if (p == null || (p.getId() != a.getProfile().getId())) {

			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.servicecatalog.delete.error");

		} else {

			try {

				catalogService.deleteService(id);

				mav.addObject(VmeConstants.MESSAGE_TEXT,
						"message.delete.success");
				// mav.addObject(VmeConstants.MESSAGE_TYPE,
				// VmeConstants.MESSAGE_SUCCESS);
			} catch (Exception ex) {

				logger.error("Delete Service ERROR: " + ex.getMessage());

				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");

			}
		}

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/{id}/setvisibility", method = RequestMethod.GET)
	public ModelAndView serviceSetVisibility(@PathVariable("id") int serviceId,
			@RequestParam int code) throws Exception {

		logger.info("Change visibility service");

		ModelAndView mav = new ModelAndView(
				"redirect:/admin/servicecatalog/list");

		if ((code == VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC)
				|| (code == VmeConstants.SERVICECATALOG_VISIBILITY_DISABLED)) {
			catalogService.setVisibility(serviceId, code);
		} else {
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.visibility.error");

		}

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/add/wsdl", method = RequestMethod.GET)
	public ModelAndView serviceAddWsdl() {

		logger.info("Add service View");

		ModelAndView mav = new ModelAndView("admin/servicecatalog/addwsdl");
		mav.addObject("model", new AddWsdlServiceViewModel());

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/add/wsdl/url", method = RequestMethod.POST)
	public ModelAndView serviceAddWsdlUrl(
			@ModelAttribute AddWsdlServiceViewModel addWsdlVM,
			BindingResult result) {

		logger.info("Execute Add wsdl service from url");

		ModelAndView mav = null;

		try {

			AccountDTO adto = getLoggedUser();
			
			String url = addWsdlVM.getWsdlUrl();

			catalogService.addWsdlService(url, adto.getProfile());
			
			String activityDescription = "Add WSDL url:" + url;

			activityService.activityStreamRecordSave(adto.getProfile(),
					AbstractDAO.ActivityType.IMPORT_WSDL.getValue(),
					activityDescription);

			mav = new ModelAndView("redirect:/admin/servicecatalog/list");

		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());

			mav = new ModelAndView("redirect:/admin/servicecatalog/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
		}

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/add/wsdl/file", method = RequestMethod.POST)
	public ModelAndView serviceAddWsdlFile(
			@ModelAttribute AddWsdlServiceViewModel addWsdlVM,
			BindingResult result) {

		logger.info("Execute Add wsdl service from file");

		ModelAndView mav = null;

		try {


			AccountDTO adto = getLoggedUser();
			
			List<MultipartFile> files = addWsdlVM.getWsdlFile();

			if (files != null && files.size() > 0) {

				logger.debug("Import wsdl from file");

				catalogService.addWsdlService(files.get(0).getBytes(),
						adto.getProfile());

				String activityDescription = "Add WSDL file";

				activityService.activityStreamRecordSave(adto.getProfile(),
						AbstractDAO.ActivityType.IMPORT_WSDL.getValue(),
						activityDescription);

				mav = new ModelAndView("redirect:/admin/servicecatalog/list");

			} else {

				mav = new ModelAndView("redirect:/admin/servicecatalog/list");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
			}
		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());

			mav = new ModelAndView("redirect:/admin/servicecatalog/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
		}

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/add/wadl", method = RequestMethod.GET)
	public ModelAndView serviceAddWadl() {

		logger.info("Add service View");

		ModelAndView mav = new ModelAndView("admin/servicecatalog/addwadl");

		mav.addObject("model", new AddWadlViewModel());

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/add/wadl/url", method = RequestMethod.POST)
	public ModelAndView serviceAddWadlUrl(
			@ModelAttribute AddWadlViewModel addWadlVM, BindingResult result) {

		logger.info("Execute Add Wadl Service from Url");

		ModelAndView mav = null;

		try {
			AccountDTO adto = getLoggedUser();

			URL wadlUrl = new URL(addWadlVM.getWadlUrl());
			InputStream in = wadlUrl.openStream();
			String wadl = IOUtils.toString(in, "UTF-8");

			catalogService.addFromWadl(addWadlVM.getServiceName(), wadl,
					adto.getProfile());

			in.close();

			String activityDescription = "Add WADL service name: "
					+ addWadlVM.getServiceName();

			activityService.activityStreamRecordSave(adto.getProfile(),
					AbstractDAO.ActivityType.IMPORT_WADL.getValue(),
					activityDescription);

			mav = new ModelAndView("redirect:/admin/servicecatalog/list");

		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage(), ex);

			mav = new ModelAndView("redirect:/admin/servicecatalog/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
		}

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/add/wadl/file", method = RequestMethod.POST)
	public ModelAndView serviceAddWadlFile(
			@ModelAttribute AddWadlViewModel addWadlVM, BindingResult result) {

		logger.info("Execute Add Wadl Service from File ");

		ModelAndView mav = null;

		try {

			AccountDTO adto = getLoggedUser();
			
			List<MultipartFile> files = addWadlVM.getWadlFile();

			if (files != null && files.size() > 0) {

				logger.debug("Import wadl from file");

				String wadl = IOUtils.toString(files.get(0).getInputStream(),
						"UTF-8");
				catalogService.addFromWadl(addWadlVM.getServiceName(), wadl,
						adto.getProfile());

				String activityDescription = "Add WADL file";

				activityService.activityStreamRecordSave(adto.getProfile(),
						AbstractDAO.ActivityType.IMPORT_WADL.getValue(),
						activityDescription);

				mav = new ModelAndView("redirect:/admin/servicecatalog/list");

			} else {

				mav = new ModelAndView("redirect:/admin/servicecatalog/list");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
			}
		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());

			mav = new ModelAndView("redirect:/admin/servicecatalog/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
		}

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/{id}/addoauth", method = RequestMethod.GET)
	public ModelAndView serviceAddOauth(@PathVariable("id") int id) {

		logger.info("Add Oauth");
		ModelAndView mav = getDefaultModelAndView("admin/servicecatalog/addoauth");
		mav.addObject("oauthTypes", authManagerService.getAdminAuthProviders());

		try {
			ServiceCatalog s = catalogService.findService(id);
			if (s.getAuthType() == null) {
				AuthProviderType type = new AuthProviderType();
				type.setType(VmeConstants.OAUTHNONE);
				s.setAuthType(type);
			}
			mav.addObject("service", s);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = getDefaultModelAndView("redirect:admin/servicecatalog");
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.servicecatalog.addoauth.error");
		}

		return mav;
	}

	@RequestMapping(value = "/servicecatalog/{id}/addoauth/execute", method = RequestMethod.POST)
	public ModelAndView serviceAddOauthExecute(@PathVariable("id") int id,
			@ModelAttribute ServiceCatalog service) {

		logger.info("Add Oauth");

		ServiceCatalog dbService = catalogService.findService(id);

		try {
			
			dbService.setAuthType(service.getAuthType());
			
			// IVAN
			// check if WeLive Auth
			if(service.getAuthType().getType().equals(VmeConstants.WELIVEAUTH))
			{
				dbService.setApiKey(PropertyGetter.getProperty("aac.authorization.vc_credentials.client_id"));
				dbService.setApiSecret(PropertyGetter.getProperty("aac.authorization.vc_credentials.client_secret"));
				dbService.setAccessTokenUrl(PropertyGetter.getProperty("aac.authorization.token.url"));
				dbService.setAuthorizeUrl(PropertyGetter.getProperty("aac.authorization.authorize.url") + "&scope=" + service.getScope());
				dbService.setScope(service.getScope());
				//dbService.setRequestTokenUrl(service.getRequestTokenUrl());
			}
			else if(service.getAuthType().getType().equals(VmeConstants.OAUTH10VERSION) || service.getAuthType().getType().equals(VmeConstants.OAUTH20VERSION))
			{
				dbService.setApiKey(service.getApiKey());
				dbService.setApiSecret(service.getApiSecret());
				dbService.setAccessTokenUrl(service.getAccessTokenUrl());
				dbService.setAuthorizeUrl(service.getAuthorizeUrl() + (service.getScope() != "" ? ("&scope=" + service.getScope()) : ""));
				dbService.setRequestTokenUrl(service.getRequestTokenUrl());
				dbService.setScope(service.getScope());
			}
			else
			{
				dbService.setUsername(service.getUsername());
				dbService.setPassword(service.getPassword());
			}
			
			catalogService.updateService(dbService);

			authManagerService.reloadProviders();

		} catch (Exception ex) {
			ex.printStackTrace();;
			ModelAndView mav = getDefaultModelAndView("admin/servicecatalog/addoauth");
			mav.addObject("service", dbService);
			mav.addObject("oauthTypes",
					authManagerService.getAdminAuthProviders());
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.servicecatalog.addoauth.error");
			return mav;
		}

		ModelAndView mav = getDefaultModelAndView("redirect:/admin/servicecatalog/"
				+ service.getId() + "/details");
		return mav;
	}

	@RequestMapping(value = "/category/categoryList", method = RequestMethod.GET)
	public ModelAndView listCategory(final HttpServletRequest request) {

		logger.info("Category marketplace List request");

		ModelAndView mav = getDefaultModelAndView("admin/category/categoryList");

		List<CategoryDTO> categoryList = marketplaceService.findAllCategory();

		mav.addObject("categoryList", categoryList);
		return mav;
	}

	@RequestMapping(value = "/category/{id}/edit", method = RequestMethod.GET)
	public ModelAndView readCategory(@PathVariable("id") Integer id) {

		logger.info("Mashup read Mashup request");
		AccountDTO a = getLoggedUser();
		ModelAndView mav = getDefaultModelAndView("admin/category/category_details");
		CategoryDTO categoryDTO = marketplaceService.readCategory(id);
		CategoryDTO categoryList = marketplaceService.createTreeCategory();
		mav.addObject("category", categoryDTO);
		mav.addObject("categoryList", categoryList);
		return mav;
	}

	@RequestMapping(value = "/category/add", method = RequestMethod.GET)
	public ModelAndView addCategory() {

		logger.info("Mashup read Mashup request");
		AccountDTO a = getLoggedUser();
		ModelAndView mav = getDefaultModelAndView("admin/category/category_details");
		CategoryDTO categoryList = marketplaceService.createTreeCategory();
		CategoryDTO categoryDTO = new CategoryDTO();
		mav.addObject("category", categoryDTO);
		mav.addObject("categoryList", categoryList);
		return mav;
	}

	@RequestMapping(value = "category/edit/save", method = RequestMethod.POST)
	public ModelAndView saveCategory(@ModelAttribute CategoryDTO category,
			BindingResult result) {

		ModelAndView mav;
		logger.info("Mashup save or edit mashup request");
		AccountDTO a = getLoggedUser();

		marketplaceService.saveUpdateCategory(category, a.getProfile());
		mav = getDefaultModelAndView("redirect:/admin/category/categoryList/");

		return mav;
	}

	@RequestMapping(value = "/category/{id}/delete", method = RequestMethod.GET)
	public ModelAndView details(@PathVariable("id") Integer id) {

		logger.info("Detail project View");
		ModelAndView mav = null;

		try {
			AccountDTO a = getLoggedUser();
			marketplaceService.deleteCategory(id, a.getProfile());
			mav = getDefaultModelAndView("redirect:/admin/category/categoryList/");
		} catch (Exception e) {
			logger.error(e.getMessage());
			mav = getDefaultModelAndView("redirect:/admin/category/categoryList/");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");
		}

		return mav;
	}

	@RequestMapping(value = "/activityStream", method = RequestMethod.GET)
	public String readActivityStream() {

		return "admin/activityStream";
	}

	@RequestMapping(value = { "/ckanserver/list", "" }, method = RequestMethod.GET)
	public ModelAndView ckanServerList() {

		ModelAndView mav = null;

		List<CkanServer> list = ckanServerService.getAll();

		mav = getDefaultModelAndView("admin/ckanserver/list");
		mav.addObject("resourceList", list);

		return mav;
	}

	@RequestMapping(value = "/ckanserver/add", method = RequestMethod.GET)
	public ModelAndView ckanServerAdd() {

		logger.info("Add ckan server View");

		ModelAndView mav = getDefaultModelAndView("admin/ckanserver/add");

		return mav;
	}

	@RequestMapping(value = "/ckanserver/add/execute", method = RequestMethod.POST)
	public ModelAndView ckanServerAddExecute(
			@ModelAttribute CkanServer ckanServer, Locale locale) {

		logger.info("Execute Add Ckan server");

		ModelAndView mav = null;

		try {
			AccountDTO a = getLoggedUser();

			Client client = new Client(new Connection(ckanServer.getUrl()), "");

			client.checkPackageList();
			
			
			ckanServerService.add(ckanServer.getName(),
					ckanServer.getDescription(), ckanServer.getUrl(),
					a.getProfile());

			mav = getDefaultModelAndView("redirect:/admin/ckanserver/list");

		} catch (Exception ex) {			
			mav = getDefaultModelAndView("redirect:/admin/ckanserver/list");
			
			if(ex instanceof CKANException){
				CKANException cex = (CKANException) ex;
				
				String msg = messageSource.getMessage("message.add.ckanserver.error", new Object[]{cex.getErrorMessages().get(0)}, locale);
				logger.error("CKANException: " + cex.getErrorMessages().get(0));
				mav.addObject(VmeConstants.MESSAGE_TEXT, msg);
			} else {
				logger.error("Exception: " + ex.getMessage());
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
			}
		}

		return mav;
	}

	
	@RequestMapping(value = "/ckanserver/{id}/delete", method = RequestMethod.GET)
	public ModelAndView ckanServerDelete(@PathVariable("id") Integer id) {

		logger.info("Execute ckan server delete");

		ModelAndView mav = getDefaultModelAndView("redirect:/admin/ckanserver/list");

		try {

			ckanServerService.delete(id);

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.success");
		} catch (Exception ex) {

			logger.error("Delete Service ERROR: " + ex.getMessage());

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");
		}

		return mav;
	}

	@RequestMapping(value = "/ckanserver/{id}/setvisibility", method = RequestMethod.GET)
	public ModelAndView ckanServerSetVisibility(
			@PathVariable("id") int serviceId, @RequestParam int code)
			throws Exception {

		logger.info("Change ckan server visibility");
		ModelAndView mav = new ModelAndView("redirect:/admin/ckanserver/list");

		if ((code == VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC)
				|| (code == VmeConstants.SERVICECATALOG_VISIBILITY_DISABLED)) {
			ckanServerService.setVisibility(serviceId, code);
		} else {
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.visibility.error");
		}
		return mav;
	}
	
	@RequestMapping(value = { "/opendata/list", "" }, method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView mav = null;
		try {

//			List<OpenDataResource> myList = openDataService
//					.findVisibleResources(getLoggedUser().getProfile().getId());
			List<OpenDataResource> publicList = openDataService.findAdministratorsResources();
					

			mav = getDefaultModelAndView("admin/opendata/list");
			mav.addObject("resourceList", publicList);

		} catch (Exception e) {
			logger.error(e.getMessage());
			mav = new ModelAndView("redirect:/admin/profile/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.servicecatalog.list.error");
		}

		return mav;
	}	
	@RequestMapping(value = "/opendata/{id}/delete", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") Integer id) {

		logger.info("Execute open data delete");

		ModelAndView mav = getDefaultModelAndView("redirect:/admin/opendata/list");

		AccountDTO a = getLoggedUser();
//		Profile p = profileService.findProfileByOpenDataCatalog(id);
//
//		if (p == null || (p.getId() != a.getProfile().getId())) {
//
//			mav = getDefaultModelAndView("redirect:/user/opendata/list");
//			mav.addObject(VmeConstants.MESSAGE_TEXT,
//					"message.servicecatalog.delete.error");
//
//		} else {

			try {

				openDataService.delete(id);

				mav.addObject(VmeConstants.MESSAGE_TEXT,
						"message.delete.success");
				// mav.addObject(VmeConstants.MESSAGE_TYPE,
				// VmeConstants.MESSAGE_SUCCESS);
			} catch (Exception ex) {

				logger.error("Delete Service ERROR: " + ex.getMessage());

				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");
			}
//		}

		return mav;
	}

	@RequestMapping(value = "/opendata/{id}/setvisibility", method = RequestMethod.GET)
	public ModelAndView setVisibility(@PathVariable("id") int serviceId,
			@RequestParam int code) throws Exception {

		logger.info("Change open data resource visibility");
		ModelAndView mav = new ModelAndView("redirect:/admin/opendata/list");

		if ((code == VmeConstants.SERVICECATALOG_VISIBILITY_PUBLIC) || (code == VmeConstants.SERVICECATALOG_VISIBILITY_DISABLED)) {
			openDataService.setVisibility(serviceId, code);
		} else {
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.visibility.error");
		}
		return mav;
	}

	@RequestMapping(value = "/opendata/add", method = RequestMethod.GET)
	public ModelAndView add() {

		logger.info("Add open data View");

		ModelAndView mav = getDefaultModelAndView("admin/opendata/add");

		mav.addObject("model", new AddOpenDataViewModel());

		return mav;
	}

	@RequestMapping(value = "/opendata/add/url", method = RequestMethod.POST)
	public ModelAndView addUrl(@ModelAttribute AddOpenDataViewModel addVM,
			BindingResult result, HttpServletRequest request) {

		logger.info("Execute Add Open Data from Url");

		ModelAndView mav = null;

		try {

			AccountDTO a = getLoggedUser();

			openDataService.addFromUrl(addVM.getSourceName(), addVM.getUrl(), a.getProfile(), ';');			

			mav = getDefaultModelAndView("redirect:/admin/opendata/list");

		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());

			mav = getDefaultModelAndView("redirect:/admin/opendata/list");
			mav.addObject(VmeConstants.MESSAGE_TYPE, "message.add.error");
		}

		return mav;
	}

	@RequestMapping(value = "/opendata/add/file", method = RequestMethod.POST)
	public ModelAndView addFile(@ModelAttribute AddOpenDataViewModel addVM,
			BindingResult result) {

		logger.info("Execute Add Open Data from File ");

		ModelAndView mav = null;

		try {

			AccountDTO a = getLoggedUser();
			List<MultipartFile> files = addVM.getFile();

			if (files != null && files.size() > 0) {

				logger.debug("Import opendata from file");

				openDataService.addFromFile(addVM.getSourceName(), files.get(0), a.getProfile(), ';');

				mav = getDefaultModelAndView("redirect:/admin/opendata/list");
			} else {

				mav = getDefaultModelAndView("redirect:/admin/opendata/list");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");

			}
		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());

			mav = getDefaultModelAndView("redirect:/admin/opendata/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
		}

		return mav;
	}
	
	@RequestMapping(value = "/interactions/configure", method = RequestMethod.GET)
	public ModelAndView interactionsConfigure(final HttpServletRequest request) 
	{

		ModelAndView mav = getDefaultModelAndView("admin/interactions/configuration");

		HashMap<String, String> propertyMap = PropertyGetter.getPropertyMap();
		Iterator<String> keys = propertyMap.keySet().iterator();
		
		HashMap<String, String> marketplaceProperties = new HashMap<String, String>();
		HashMap<String, String> artefactsProperties = new HashMap<String, String>();
		HashMap<String, String> ideaProperties = new HashMap<String, String>();
		HashMap<String, String> decisionengineProperties = new HashMap<String, String>();
		
		String key;
		
		while(keys.hasNext())
		{
			key = keys.next();
			
			if(key.startsWith("marketplaceStore") || key.startsWith("usdleditor"))
			{
				if(!key.startsWith("marketplaceStore.serviceTypeId"))
					marketplaceProperties.put(key, propertyMap.get(key));
				else
					artefactsProperties.put(key, propertyMap.get(key));
			}
			
			if(key.startsWith("ims"))
				ideaProperties.put(key, propertyMap.get(key));
			
			if(key.startsWith("decisionEngine"))
				decisionengineProperties.put(key, propertyMap.get(key));
		}

		mav.addObject("marketplaceProperties", marketplaceProperties);
		mav.addObject("artefactsProperties", artefactsProperties);
		mav.addObject("ideaProperties", ideaProperties);
		mav.addObject("decisionengineProperties", decisionengineProperties);
				
		return mav;
	}
	
	@RequestMapping(value = "/interactions/configure/save", method = RequestMethod.POST)
	public ModelAndView interactionsConfigureSave(HttpServletRequest request) 
	{
		ModelAndView mav = null;
		mav = getDefaultModelAndView("redirect:/admin/interactions/configure");
		mav.addObject(VmeConstants.MESSAGE_TEXT, "message.admin.interactions.saved");
		
		Enumeration<String> params = request.getParameterNames();
		
		String key = "";
		String test = "";
		
		while(params.hasMoreElements())
		{
			key = params.nextElement();
			test = PropertyGetter.getProperty(key);
			
			if(test != null)
			{
				PropertyGetter.setProperty(key, request.getParameter(key));
				//System.out.println(key + "=" + request.getParameter(key));
			}
		}
		
		return mav;
	}
}
