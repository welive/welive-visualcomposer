package com.mediigea.vme2.controller;

import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Controller
@RequestMapping(value = "/mock")
public class MockController extends BaseController
{	
	private static HashMap<String, ObjectNode> mockDataObject;
	private static HashMap<String, ArrayNode> mockDataArray;
	
	static {
		mockDataArray = new HashMap<String, ArrayNode>();
		mockDataObject = new HashMap<String, ObjectNode>();
	}
	
	@RequestMapping(value = "/setdata/array/{id}", method=RequestMethod.POST)
	public @ResponseBody ObjectNode setMockDataArray(@RequestBody ArrayNode newMockData, @PathVariable("id") String id)
	{
		ObjectNode response = JsonNodeFactory.instance.objectNode();
		
		try
		{
			mockDataArray.put(id, newMockData);
			response.put("success", true);
		}
		catch(Exception e)
		{
			response.put("success", false);
			response.put("error", e.getMessage());
		}
		
		return response;
	}
	
	@RequestMapping(value = "/setdata/object/{id}", method=RequestMethod.POST)
	public @ResponseBody ObjectNode setMockDataObject(@RequestBody ObjectNode newMockData, @PathVariable("id") String id)
	{
		ObjectNode response = JsonNodeFactory.instance.objectNode();
		
		try
		{
			mockDataObject.put(id, newMockData);
			response.put("success", true);
		}
		catch(Exception e)
		{
			response.put("success", false);
			response.put("error", e.getMessage());
		}
		
		return response;
	}
	
	@RequestMapping(value = "/getdata/array/{id}", method=RequestMethod.POST)
	public @ResponseBody ArrayNode getMockDataArray(@RequestBody String whatever, @PathVariable("id") String id)
	{
		if(mockDataArray.isEmpty() || !mockDataArray.containsKey(id))
			return JsonNodeFactory.instance.arrayNode();
		
		return mockDataArray.get(id);
	}
	
	@RequestMapping(value = "/getrandomdata/{id}", method=RequestMethod.POST)
	public @ResponseBody ObjectNode getMockRandomData(@RequestBody String whatever, @PathVariable("id") String id)
	{
		if(mockDataArray.isEmpty() || !mockDataArray.containsKey(id))
			return JsonNodeFactory.instance.objectNode();
		
		ArrayNode array = mockDataArray.get(id);
		
		int index = (int)(Math.random() * (array.size() - 1));
		
		return (ObjectNode) array.get(index);
	}
	
	@RequestMapping(value = "/getdata/object/{id}", method=RequestMethod.POST)
	public @ResponseBody ObjectNode getMockDataObject(@RequestBody String whatever, @PathVariable("id") String id)
	{
		if(mockDataObject.isEmpty() || !mockDataObject.containsKey(id))
			return JsonNodeFactory.instance.objectNode();
		
		return mockDataObject.get(id);
	}
}
