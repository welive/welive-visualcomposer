package com.mediigea.vme2.controller;

import java.util.ArrayList;
import java.util.List;

import it.eng.model.CDVHospitalPerformance;
import it.eng.model.CDVPerformance;
import it.eng.model.CDVUserInfo;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;

@Controller
@RequestMapping(value = "/cdv/webservices")
public class FakeCDVController extends BaseController
{
	@RequestMapping(value = "/getuserinfo", method=RequestMethod.POST, produces="application/json")
	public @ResponseBody CDVUserInfo getUserInfo(@RequestParam String username, @RequestParam String password, HttpServletRequest request)
	{
		CDVUserInfo info = new CDVUserInfo();
		
				
			if(password.replace("\"","").compareTo("password") == 0)
			{
				info.setSuccess("true");
				info.setName("Antonino");
				info.setSurname("Sirchia");
				info.setCf("SRCNNN87S16G273F");
				info.setAge("28");
				info.setCity("Palermo");
				info.setEmail("asirchia@gmail.com");
				info.setTel("3281234567");
			}
			else
			{
				info.setSuccess("false");
				info.setName("");
				info.setSurname("");
				info.setCf("");
				info.setAge("");
				info.setCity("");
				info.setEmail("");
				info.setTel("");
			}
				
		return info;
	}
	
	@RequestMapping(value = "/gethospitalperformances", method=RequestMethod.POST, produces="application/json")
	public @ResponseBody CDVHospitalPerformance getHospitalEventsByUser(@RequestParam String username, @RequestParam String password, @RequestParam String hospitalId, HttpServletRequest request)
	{
			String[] randPerformance = {"Radiografia", "TAC", "Risonanza Magnetica"};
			String[] randDoctor = {"Dott. Spinnato", "Dott. Giuffrida", "Dott. Ciccarelli", "Dott. Ligotino"};
			
			CDVHospitalPerformance hp = new CDVHospitalPerformance();
			CDVPerformance p;
			
			List<CDVPerformance> list = new ArrayList<CDVPerformance>();
			
			if(password.replace("\"","").compareTo("password") == 0)
			{
				hp.setSuccess("true");				
				
				for(int i = 0; i < 2; i++)
				{
					p = new CDVPerformance();
					p.setPerformance(randPerformance[((int)(Math.random()*100)) % 3]);
					p.setDoctor(randDoctor[((int)(Math.random()*100)) % 4]);
					p.setDate("" + (((int)(Math.random()*100)) % 29) + "/" + (((int)(Math.random()*100)) % 12 + 1) + "/" + "2014");
					
					list.add(p);
				}
				
				hp.setList(list);
			}
			else
			{
				hp.setSuccess("false");
				hp.setList(list);
			}
		
		
		return hp;
	}
	
	@RequestMapping(value = "/booking", method=RequestMethod.POST)
	public @ResponseBody String booking(
			@RequestParam String name,
			@RequestParam String surname,
			@RequestParam String cf,
			@RequestParam String age,
			@RequestParam String city,
			@RequestParam String email,
			@RequestParam String tel,
			@RequestParam String bookingDate,
			HttpServletRequest request)
	{		
		return "" + Integer.toString((int)Math.round(Math.random()*10000)) + "USHK";
	}
}
