package com.mediigea.vme2.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.exceptions.RestException;
import com.mediigea.vme2.exceptions.RestJsonpException;
import com.mediigea.vme2.entity.Activity;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.service.ActivityService;
import com.mediigea.vme2.util.VmeConstants;

@Controller
@SessionAttributes("lastinvokeresult")
public abstract class BaseController {

	private static final Logger logger = LoggerFactory.getLogger(BaseController.class);

	@Autowired
	protected HttpSession httpSession;
	
	@Autowired
	protected ActivityService activityService;

	/*protected Account getLoggedUser(){

		Account userLogged = (Account)httpSession.getAttribute(VmeConstants.LOGGED_ACCOUNT);

		return userLogged;
	}*/
	
	protected AccountDTO getLoggedUser(){
		AccountDTO userDetails = null;
		
		try {
			userDetails =
				 (AccountDTO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		}
		catch (ClassCastException e) {
			logger.warn("SecurityContextHolder.getContext().getAuthentication().getPrincipal() returns a String, not an AccountDTO");
		}
		return userDetails;
	}	
	
	public ModelAndView getDefaultModelAndView(String viewName) {

		if(viewName==null || viewName.isEmpty())
			return null;

		ModelAndView defaultMav = new ModelAndView(viewName);
		defaultMav.addObject("loggeduser", getLoggedUser());

		return defaultMav;
	}
	

	
	public void activityStreamRecordSave(Profile p, String activityType) {
		
		activityService.activityStreamRecordSave(p, activityType);
		
	}
	
	

	@ExceptionHandler(RestException.class)
	public @ResponseBody
	JsonNode handleRestException(RestException ex, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);

		JsonNodeFactory factory = JsonNodeFactory.instance;
		ObjectNode node = factory.objectNode();

		node.put("message", ex.getMessage());		
		node.put("stackTrace", ExceptionUtils.getStackTrace(ex));
		node.put("extendedMessage", ex.getExtendedMessage());

		//HttpStatus.BAD_REQUEST
		response.setStatus(400);
		if (ex.getErrorCode() != null){
			response.setStatus(ex.getErrorCode().value());
			node.put("errorCode", ex.getErrorCode().value());			
		}
		
			
		if (ex.getRedirectUrl() != null){
			node.put("redirectUrl", ex.getRedirectUrl());			
		}
		
		return node;
	}
	
	/**
	 * Exception handler for RestJsonpException type
	 * @param ex
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@ExceptionHandler(RestJsonpException.class)
	public void handleRestException(RestJsonpException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);

		//Compone un nodo json con le informazioni necessarie ad effettuare il redirect per l'autenticazione oauth
		JsonNodeFactory factory = JsonNodeFactory.instance;
		ObjectNode node = factory.objectNode();

		node.put("message", ex.getMessage());		
		node.put("stackTrace", ExceptionUtils.getStackTrace(ex));

		//HttpStatus.BAD_REQUEST
		//response.setStatus(400);
		if (ex.getErrorCode() != null){
			//response.setStatus(ex.getErrorCode().value());
			node.put("errorCode", ex.getErrorCode().value());			
		}
			
		if (ex.getRedirectUrl() != null){
			node.put("redirectUrl", ex.getRedirectUrl());			
		}
		
		ObjectMapper mapper = new ObjectMapper();
		
		//Setta la response in moda da restituire jsonp
		response.setContentType("text/javascript; charset=UTF-8");
		   PrintWriter out = response.getWriter();
		   out.print( ex.getCallback() +"(" + mapper.writeValueAsString(node) + ")");		
	}	
}
