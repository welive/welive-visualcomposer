package com.mediigea.vme2.controller;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_SESSION;

import java.io.IOException;
import java.net.URLEncoder;

import it.eng.PropertyGetter;

import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.Api;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mediigea.vme2.dto.OAuthServiceInfo;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.rest.RestAuth;
import com.mediigea.vme2.service.AccessTokenService;
import com.mediigea.vme2.service.AuthManagerService;
import com.mediigea.vme2.service.RestOperationService;
import com.mediigea.vme2.service.ServiceCatalogService;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.util.oauth.OAuth10aGenericApi;
import com.mediigea.vme2.util.oauth.OAuth20GenericApi;

@Controller
@RequestMapping(value = "/oauth")
public class OAuthController {

	@Autowired
	@Qualifier("oauthManagerService")
	private AuthManagerService providerManager;
	
	@Autowired
	private AccessTokenService accessTokenService;	
	
	@Autowired
	private ServiceCatalogService catalogService;
	
	@Autowired
	private RestOperationService restOperationService;
	
	private static final Logger logger = LoggerFactory.getLogger(OAuthController.class);
	
	private static final Token EMPTY_TOKEN = null;

	private static final String ATTR_OAUTH10_REQUEST_TOKEN = "REQUEST_TOKEN";
	
	@RequestMapping(value={"{provider}/{operation}/authorize"}, method = RequestMethod.GET)
	public String authorize(
			@PathVariable("provider") Integer idProvider, 
			@PathVariable("operation") Integer operationId, // IVAN: added to allow auth at operation level
			@RequestParam(value="userconnector", required=false) String userConnector, 
			WebRequest request) {
		
		if(userConnector==null || userConnector.isEmpty()){
			userConnector = RequestContextHolder.currentRequestAttributes().getSessionId();
		}
		
		request.setAttribute(VmeConstants.SESSION_USER_TOKEN_CONNECTOR, userConnector, SCOPE_SESSION);	
		
		OAuthService service = null;
		
		Token requestToken = EMPTY_TOKEN;
		
		// IVAN: auth at operation level
		if(operationId != null)
		{
			ServiceCatalog serviceCatalog = catalogService.findService(idProvider);
			
			if(serviceCatalog.getType() == VmeConstants.SERVICECATALOG_TYPE_REST)
			{
				RestOperation operation = restOperationService.findRestOperationById(operationId);
				
				RestAuth authInfo = null;
				
				try 
				{
					authInfo = operation.getRestMethod().getAuthInfo();
				} 
				catch (Exception e) 
				{
					// TODO manage error
					e.printStackTrace();
					
					return null;
				} 
				
				String scopes = "";
				
				if(authInfo.getScopes().size() > 1)
					scopes += authInfo.getScopes().get(0);
				
				for(int i = 1; i < authInfo.getScopes().size(); i++)
					scopes += "," + authInfo.getScopes().get(i);
				
				String authorizationUrl = authInfo.getAuthorizationUrl() + "?client_id=%s&redirect_uri=%s&response_type=code&scope=" + scopes;
				
				Api api = new OAuth20GenericApi(authorizationUrl, null, authInfo.getTokenUrl());
				
				String apiKey = PropertyGetter.getProperty("aac.authorization.vc_credentials.client_id");
				String apiSecret = PropertyGetter.getProperty("aac.authorization.vc_credentials.client_secret");
				
				String callback = PropertyGetter.getProperty("oauth.callbackContext") + "/oauth/" + idProvider + "/" + operationId + "/tokencallback";
				
				service = new ServiceBuilder().provider(api).apiKey(apiKey).apiSecret(apiSecret).callback(callback).build();
			}
			else
			{
				// TODO: manage SOAP auth at operation level
			}
		} // backward compatibility
		else
		{
			service = providerManager.getService(idProvider).getService();
			
			String serviceVersion = service.getVersion();
			
			if(VmeConstants.OAUTH10VERSION.equals(serviceVersion))
			{

				requestToken = service.getRequestToken();
				request.setAttribute(ATTR_OAUTH10_REQUEST_TOKEN, requestToken, SCOPE_SESSION);			
			}
		}	
		
		return "redirect:" + service.getAuthorizationUrl(requestToken);
	}

	@RequestMapping(value={"{provider}/{operation}/tokencallback"}, method = RequestMethod.GET)
	public ModelAndView callback(
			@PathVariable("provider") Integer idProvider, 
			@PathVariable("operation") Integer operationId, // IVAN: added to allow auth at operation levele
			@RequestParam(value="code", required=false) String oauth20Verifier, 
			@RequestParam(value="oauth_verifier", required=false) String oauth10Verifier,
			@RequestParam(value="oauth_token", required=false) String oauthToken,
			WebRequest request) {	
		
		Token requestToken = EMPTY_TOKEN;
		
		Verifier verifier = null;
		Token accessToken = null;
		
		OAuthService service = null;
		String serviceVersion = null;
		
		// IVAN: auth at operation level
		if(operationId != null)
		{
			ServiceCatalog serviceCatalog = catalogService.findService(idProvider);
			
			if(serviceCatalog.getType() == VmeConstants.SERVICECATALOG_TYPE_REST)
			{
				RestOperation operation = restOperationService.findRestOperationById(operationId);
				
				serviceVersion = operation.getAuthType().getType();
			}
			else
			{
				// TODO: manage SOAP auth at operation level
			}
		}
		else
		{
			service = providerManager.getService(idProvider).getService();
			
			//String serviceVersion = service.getVersion();
			serviceVersion = catalogService.findService(idProvider).getAuthType().getType();
		}		
		
		if(VmeConstants.OAUTH10VERSION.equals(serviceVersion))
		{
			verifier = new Verifier(oauth10Verifier);
			requestToken = (Token) request.getAttribute(ATTR_OAUTH10_REQUEST_TOKEN, SCOPE_SESSION);
			accessToken = service.getAccessToken(requestToken, verifier);
		} 
		else if(VmeConstants.OAUTH20VERSION.equals(serviceVersion))
		{
			verifier = new Verifier(oauth20Verifier);
			accessToken = service.getAccessToken(requestToken, verifier);
		}
		else // WeLive Auth
		{
			// IVAN
			// get token from AAC
			RestTemplate restTemplate = new RestTemplate();
			
			String baseUrl = PropertyGetter.getProperty("aac.authorization.token.url");
			
			String url = baseUrl
					+ "?"
					+ "client_id=" + PropertyGetter.getProperty("aac.authorization.vc_credentials.client_id")
					+ "&client_secret=" + PropertyGetter.getProperty("aac.authorization.vc_credentials.client_secret")
					+ "&code=" + oauth20Verifier
					+ "&grant_type=authorization_code"
					+ "&redirect_uri=" + PropertyGetter.getProperty("oauth.callbackContext") + "/oauth/" + idProvider + "/" + operationId + "/tokencallback";
			
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		    
		    HttpEntity<String> aac_request = new HttpEntity<String>(headers);
		    //System.out.println("### AAC REQUEST: " + url);		    
		    
		    try
		    {
		    	ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, aac_request, String.class);
		    	//System.out.println("### AAC RESPONSE: " + response.getBody());
		    	
		    	JSONObject json = new JSONObject(response.getBody());
		    	
		    	accessToken = new Token(json.getString("access_token"), "");
		    }
		    catch(RestClientException e)
		    {
		    	logger.error("Error invoking AAC API: " + e.getMessage());
		    } 
		    catch (JSONException e) 
		    {
		    	logger.error("Error parsing AAC response: " + e.getMessage());
			}	    
		}		
		
		String userConnector = (String) request.getAttribute(VmeConstants.SESSION_USER_TOKEN_CONNECTOR, SCOPE_SESSION);
		
		accessTokenService.addToken(userConnector, accessToken, idProvider, operationId);		
		
		request.removeAttribute(VmeConstants.SESSION_USER_TOKEN_CONNECTOR, SCOPE_SESSION);
		
		ModelAndView mav = new ModelAndView("/connect/oauth_success");
		return mav;
	}
}