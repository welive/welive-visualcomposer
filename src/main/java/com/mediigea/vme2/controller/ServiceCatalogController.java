package com.mediigea.vme2.controller;

import it.eng.model.GenericService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.dto.OperationTemplateDTO;
import com.mediigea.vme2.dto.RestOperationDTO;
import com.mediigea.vme2.dto.ServiceItemDTO;
import com.mediigea.vme2.dto.ServiceOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.dto.SoapOperationDTO;
import com.mediigea.vme2.entity.AuthProviderType;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.ServiceCatalogExt;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.exceptions.RestException;
import com.mediigea.vme2.rest.RestAuth;
import com.mediigea.vme2.rest.RestMethod;
import com.mediigea.vme2.service.AccountService;
import com.mediigea.vme2.service.AuthManagerService;
import com.mediigea.vme2.service.ProfileService;
import com.mediigea.vme2.service.ProjectOperationService;
import com.mediigea.vme2.service.RestOperationService;
import com.mediigea.vme2.service.ServiceCatalogExtService;
import com.mediigea.vme2.service.ServiceCatalogService;
import com.mediigea.vme2.service.SoapOperationService;
import com.mediigea.vme2.service.WorkgroupService;
import com.mediigea.vme2.util.FormatConverter;
import com.mediigea.vme2.util.JsTreeNode;
import com.mediigea.vme2.util.JsonUtils;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.viewmodel.AddWadlViewModel;
import com.mediigea.vme2.viewmodel.AddWsdlServiceViewModel;
import com.mediigea.vme2.viewmodel.ServiceCatalogListViewModel;

@Controller
@RequestMapping(value = "/user/servicecatalog")
public class ServiceCatalogController extends BaseController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private WorkgroupService workgroupService;
	
	@Autowired
	private ServiceCatalogService catalogService;
	
	@Autowired
	private ServiceCatalogExtService catalogExtService;

	@Autowired
	private RestOperationService restOperationService;

	@Autowired
	private SoapOperationService soapOperationService;
	
	@Autowired
	private ProjectOperationService projectOperationService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ObjectMapperExt objectMapper;
	
	@Autowired
	private AuthManagerService authManagerService;	

	private static final Logger logger = LoggerFactory
			.getLogger(ServiceCatalogController.class);

	@InitBinder
	public void initBinder(WebDataBinder binder) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:m:s");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));  
	}
	
	// IVAN
	@RequestMapping(value = "/{id}/operation/{opId}/specs", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ObjectNode getOperationSpecs(@PathVariable("id") int serviceId, @PathVariable("opId") int operationId) {

		AccountDTO a = getLoggedUser();
		
		RestOperation restOperation = null;
		RestMethod restMethod = null;
		RestAuth restAuth = null;
		
		ObjectNode response = null;
		
		if (a != null) 
		{
			try
			{
				ObjectMapper mapper = new ObjectMapper();
				restOperation = restOperationService.findRestOperationById(operationId);
				restMethod = restOperation.getRestMethod();
				restAuth = restMethod.getAuthInfo();
						
				String json = mapper.writeValueAsString(restMethod);
				response = (ObjectNode) mapper.readTree(json);
				
				response.put("name", restOperation.getName());
				response.put("description", restOperation.getDoc());
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		return response;
	}

	@RequestMapping(value = "/{id}/operations.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<ServiceOperationDTO> operationsJSON(@PathVariable("id") final int serviceId) {

		AccountDTO a = getLoggedUser();
		if (a == null || a.getRole() != VmeConstants.ACCOUNT_ROLE_USER) {

			logger.error("Logged Account is bad");
			return new ArrayList<ServiceOperationDTO>();
		}

		return catalogService.getServiceOperationsDTO(serviceId);
	}
	/**
	 * Get parameters for selected operation
	 * @param operationId
	 * @param serviceType
	 * @return
	 */
	@RequestMapping(value = "/{type}/{id}/operationparams.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<ServiceParameterDTO> operationParamsJSON(@PathVariable("id") final int operationId, @PathVariable("type") final int serviceType ) {

		AccountDTO a = getLoggedUser();
		if (a == null || a.getRole() != VmeConstants.ACCOUNT_ROLE_USER) {

			logger.error("Logged Account is bad");
			return new ArrayList<ServiceParameterDTO>();
		}

		return catalogService.getOperationParamsDTO(operationId, serviceType);
	}	
	
	/**
	 * Get parameters for selected template
	 * @param templateId
	 * @param serviceType
	 * @return
	 * @throws RestException
	 */
	@RequestMapping(value = "/{type}/{id}/templateparams.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<ServiceParameterDTO> templateParamsJSON(@PathVariable("id") final int templateId, @PathVariable("type") final int serviceType ) throws RestException {
		List<ServiceParameterDTO> templateParams = null;
		
		AccountDTO a = getLoggedUser();
		if (a == null || a.getRole() != VmeConstants.ACCOUNT_ROLE_USER) {

			logger.error("Logged Account is bad");
			return new ArrayList<ServiceParameterDTO>();
		}

		try{
			templateParams = catalogService.getTemplateParamsDTO(templateId, serviceType);
		} catch (Exception e) {
			String error = messageSource.getMessage("message.error.gettemplateparams", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}
				
		return templateParams;
	}		

	@RequestMapping(value = "/list.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<ServiceItemDTO> listJSON(final HttpServletRequest request) {

		logger.info("Services view");

		AccountDTO a = getLoggedUser();
		if (a == null || a.getRole() != VmeConstants.ACCOUNT_ROLE_USER) {

			logger.error("Logged Account is bad");
			return new ArrayList<ServiceItemDTO>();
		}

		return catalogService.getServicesDTO(a.getProfile());
	}

	@RequestMapping(value = {"/list", ""}, method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView mav = null;
		try {
			ServiceCatalogListViewModel vm = new ServiceCatalogListViewModel(catalogService, getLoggedUser().getProfile());
			mav = getDefaultModelAndView("user/servicecatalog/list");
			mav.addObject("model", vm);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			mav = new ModelAndView("redirect:/user/project/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.servicecatalog.list.error");
		}

		return mav;
	}
	
	@RequestMapping(value = "/{id}/details", method = RequestMethod.GET)
	public ModelAndView details(@PathVariable("id") Integer id) {

		logger.info("Detail service View");

		ModelAndView mav = null;

		ServiceCatalog s = catalogService.findService(id);
		Profile p = profileService.findProfileByServiceCatalog(id);
		
		ServiceCatalogExt s_ext;
		
		try
		{
			s_ext = catalogExtService.getById(id);
		}
		catch(Exception e)
		{
			s_ext = null;
		}

		if (s == null) {

			logger.debug("Service Id is bad");

			mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.find.error");

			return mav;
		}

		mav = getDefaultModelAndView("user/servicecatalog/detail");
		mav.addObject("service", s);
		mav.addObject("service_ext", s_ext);
		mav.addObject("owner", ((p != null) ? "owner" : "user"));

		if (s.getType() == VmeConstants.SERVICECATALOG_TYPE_REST)
		{
			// IVAN: unescape docs
			
			List<RestOperation> ops = restOperationService.findAllByService(s.getId());
			
			for(int i = 0; i < ops.size(); i++)
			{
				ops.get(i).setDoc(StringEscapeUtils.unescapeHtml4(ops.get(i).getDoc()));
			}
			
			mav.addObject("serviceResourceList", ops);
		}
		else
			mav.addObject("soapOperationList",
					soapOperationService.findAllByService(s.getId()));

		return mav;
	}
	
	// check if service is currently used in mashup projects
	@RequestMapping(value = "/{id}/delete/check", method = RequestMethod.POST)
	public @ResponseBody ObjectNode checkDelete(@PathVariable("id") Integer id) 
	{
		ObjectNode dependencies = JsonNodeFactory.instance.objectNode();
		
		ArrayNode mashups = JsonNodeFactory.instance.arrayNode();
		
		ObjectNode node;
		
		ServiceCatalog service = catalogService.findService(id);
		
		List<Project> projects = new ArrayList<Project>();
		
		boolean flag;
		
		List<Project> tmp = null;
		
		if(service.getType() == VmeConstants.SERVICECATALOG_TYPE_REST)
		{
			Iterator<RestOperation> restOps = restOperationService.findAllByService(id).iterator();
			
			RestOperation op = null;
			
			while(restOps.hasNext())
			{
				op = restOps.next();
				
				tmp = projectOperationService.findMashupsByOperationId(op.getId(), VmeConstants.SERVICECATALOG_TYPE_REST);
				
				for(int i = 0; i < tmp.size(); i++)
				{
					flag = true;
					
					for(int j = 0; j < projects.size(); j++)
					{
						if(projects.get(j).getId() == tmp.get(i).getId())
						{
							flag = false;
							break;
						}
					}
					
					if(flag)
						projects.add(tmp.get(i));
				}
			}
		}
		else
		{
			Iterator<SoapOperation> soapOps = soapOperationService.findAllByService(id).iterator();
			
			SoapOperation op = null;
			
			while(soapOps.hasNext())
			{
				op = soapOps.next();
				
				tmp = projectOperationService.findMashupsByOperationId(op.getId(), VmeConstants.SERVICECATALOG_TYPE_SOAP);
				
				for(int i = 0; i < tmp.size(); i++)
				{
					flag = true;
					
					for(int j = 0; j < projects.size(); j++)
					{
						if(projects.get(j).getId() == tmp.get(i).getId())
						{
							flag = false;
							break;
						}
					}
					
					if(flag)
						projects.add(tmp.get(i));
				}
			}
		}
		
		for(int i = 0; i < projects.size(); i++)
		{
			node = JsonNodeFactory.instance.objectNode();
			
			node.put("id", projects.get(i).getId());
			node.put("name", projects.get(i).getName());
			
			mashups.add(node);
		}
		
		dependencies.putArray("mashups");
		dependencies.set("mashups", mashups);
		
		return dependencies;		
	}

	@RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") Integer id) {

		logger.info("Execute Delete service");

		ModelAndView mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");

		AccountDTO a = getLoggedUser();
		Profile p = profileService.findProfileByServiceCatalog(id);

		if (p == null || (p.getId() != a.getProfile().getId())) {

			mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.servicecatalog.delete.error");
			mav.addObject(VmeConstants.MESSAGE_TYPE, VmeConstants.MESSAGE_WARNING);
			
		} else {

			try {

				catalogService.deleteService(id);
				catalogExtService.delete(id);

				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.success");
				mav.addObject(VmeConstants.MESSAGE_TYPE, VmeConstants.MESSAGE_SUCCESS);
			} catch (Exception ex) {

				logger.error("Delete Service ERROR: " + ex.getMessage());

				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");
				mav.addObject(VmeConstants.MESSAGE_TYPE, VmeConstants.MESSAGE_ERROR);
			}
		}

		return mav;
	}
	
	@RequestMapping(value = "/{id}/setvisibility", method = RequestMethod.GET)
	public ModelAndView setVisibility(@PathVariable("id") int serviceId, @RequestParam int code ) throws Exception {

		logger.info("Change visibility service");		
		ModelAndView mav = new ModelAndView("redirect:/user/servicecatalog/list");

		if ((code == VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE) || (code == VmeConstants.SERVICECATALOG_VISIBILITY_WORKGROUP) ){
			catalogService.setVisibility(serviceId, code);
		}
		else
		{
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.visibility.error");	
		}
		return mav;
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/add/wsdl", method = RequestMethod.GET)
	public ModelAndView addWsdl() {

		logger.info("Add service View");

		ModelAndView mav = getDefaultModelAndView("user/servicecatalog/addwsdl");

		mav.addObject("model", new AddWsdlServiceViewModel());

		return mav;
	}

	/**
	 * 
	 * @param service
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/add/wsdl/url", method = RequestMethod.POST)
	public ModelAndView addWsdlUrl(
			@ModelAttribute AddWsdlServiceViewModel addWsdlVM,
			BindingResult result) {

		logger.info("Execute Add Wsdl Service from Url");

		ModelAndView mav = null;
		
		ServiceCatalog sc;

		try {

			AccountDTO a = getLoggedUser();
			String url = addWsdlVM.getWsdlUrl();

			sc = catalogService.addWsdlService(url, a.getProfile());
			
			catalogExtService.add(sc.getId(), url);

			mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
			
		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());

			mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
		}

		return mav;
	}

	/**
	 * 
	 * @param service
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/add/wsdl/file", method = RequestMethod.POST)
	public ModelAndView addWsdlFile(
			@ModelAttribute AddWsdlServiceViewModel addWsdlVM,
			BindingResult result) {

		logger.info("Execute Add Wsdl Service from File ");

		ModelAndView mav = null;
		
		ServiceCatalog sc;
		
		try {

			AccountDTO a = getLoggedUser();
			List<MultipartFile> files = addWsdlVM.getWsdlFile();

			if (files != null && files.size() > 0) {

				logger.debug("Import wsdl from file");

				sc = catalogService.addWsdlService(files.get(0).getBytes(),
						a.getProfile());
				
				catalogExtService.add(sc.getId());

				mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
				
			} else {

				mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
			}
		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());

			mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
		}

		return mav;
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/add/wadl", method = RequestMethod.GET)
	public ModelAndView addWadl() {

		logger.info("Add service View");

		ModelAndView mav = getDefaultModelAndView("user/servicecatalog/addwadl");

		mav.addObject("model", new AddWadlViewModel());

		return mav;
	}

	/**
	 * 
	 * @param service
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/add/wadl/url", method = RequestMethod.POST)
	public ModelAndView addWadlUrl(
			@ModelAttribute AddWadlViewModel addWadlVM, BindingResult result) {

		logger.info("Execute Add Wadl Service from Url");

		ModelAndView mav = null;
		
		ServiceCatalog sc;

		try {

			AccountDTO a = getLoggedUser();

			URL wadlUrl = new URL(addWadlVM.getWadlUrl());
			InputStream in = wadlUrl.openStream();
			String wadl = IOUtils.toString(in, "UTF-8");

			sc = catalogService.addFromWadl(addWadlVM.getServiceName(), wadl,
					a.getProfile());
			
			catalogExtService.add(sc.getId(), wadlUrl.toString());

			in.close();

			mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
			
		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());

			mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
		}

		return mav;
	}

	/**
	 * 
	 * @param service
	 * @param result
	 * @return
	 */
	@RequestMapping(value = "/add/wadl/file", method = RequestMethod.POST)
	public ModelAndView addWadlFile(
			@ModelAttribute AddWadlViewModel addWadlVM, BindingResult result) {

		logger.info("Execute Add Wadl Service from File ");

		ModelAndView mav = null;
		
		ServiceCatalog sc;

		try {

			AccountDTO a = getLoggedUser();
			List<MultipartFile> files = addWadlVM.getWadlFile();

			if (files != null && files.size() > 0) {

				logger.debug("Import wsdl from file");

				String wadl = IOUtils.toString(files.get(0).getInputStream(), "UTF-8");
				sc = catalogService.addFromWadl(addWadlVM.getServiceName(), wadl, a.getProfile());
				
				catalogExtService.add(sc.getId());
				
				mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
			} else {

				mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
				
			}
		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());
			ex.printStackTrace();

			mav = getDefaultModelAndView("redirect:/user/servicecatalog/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
		}

		return mav;
	}						
	
	@RequestMapping(value = "/invokeconverter", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	JsonNode invokeConverter(HttpServletRequest request) {

		logger.info("Invoke Json Converter");
		JsonNode responseNode;
		
		String responseString = "{\"message\":\"%s\"}";
		String sourceNode;
		
		StringBuffer jb = new StringBuffer();
		
		try {

			BufferedReader reader = request.getReader();
			
			String line;
			
			while ((line = reader.readLine()) != null) {
				jb.append(line);
			}
			
			sourceNode = jb.toString();
			
			
			logger.debug("RequestBody: " + sourceNode);
		} catch (IOException e1) {

			logger.error("Request input stream convertion failed");
			e1.printStackTrace();

			return JsonUtils.stringToJsonNode(String.format(responseString,
					"Request input stream convertion failed"), objectMapper);
		}

		if (sourceNode == null || sourceNode.isEmpty()) {

			logger.error("InvokeDTO is null");

			return JsonUtils.stringToJsonNode(String.format(responseString,
					"The Data Request is bad"), objectMapper);
		}

		try {
			responseNode = JsonUtils.stringToJsonNode(sourceNode, objectMapper);
			JsTreeNode jsTreeDest = FormatConverter.jsonToTreeStruct(responseNode, VmeConstants.JSTREE_ROOTNODENAME);							
			
			responseNode = (ObjectNode) new ObjectMapper().convertValue(jsTreeDest, JsonNode.class);				

		} catch (Exception e) {
			e.printStackTrace();
			responseNode = JsonNodeFactory.instance.textNode(e.getMessage());
		} 

		return responseNode;
	}		
	
	@RequestMapping(value = "/{id}/addauth", method = RequestMethod.GET)
	public ModelAndView serviceAddAuth(@PathVariable("id") int id) {

		logger.info("Add Oauth");
		ModelAndView mav = getDefaultModelAndView("user/servicecatalog/addauth");
		mav.addObject("oauthTypes", authManagerService.getUserAuthProviders());

		try {
			ServiceCatalog s = catalogService.findService(id);
			if (s.getAuthType() == null) {
				AuthProviderType type = new AuthProviderType();
				type.setType(VmeConstants.OAUTHNONE);
				s.setAuthType(type);
			}
			mav.addObject("service", s);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = getDefaultModelAndView("redirect:/user/servicecatalog");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.servicecatalog.addoauth.error");
		}

		return mav;
	}

	@RequestMapping(value = "/{id}/addauth", method = RequestMethod.POST)
	public ModelAndView serviceAddAuthExecute(@PathVariable("id") int id, @ModelAttribute ServiceCatalog service) {

		logger.info("Add auth");

		ServiceCatalog dbService = catalogService.findService(id);

		try {

			dbService.setAuthType(service.getAuthType());
//			dbService.setApiKey(service.getApiKey());
//			dbService.setApiSecret(service.getApiSecret());
//			dbService.setAccessTokenUrl(service.getAccessTokenUrl());
//			dbService.setAuthorizeUrl(service.getAuthorizeUrl());
//			dbService.setRequestTokenUrl(service.getRequestTokenUrl());
			dbService.setUsername(service.getUsername());
			dbService.setPassword(service.getPassword());
			catalogService.updateService(dbService);

			authManagerService.reloadProviders();

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			ModelAndView mav = getDefaultModelAndView("user/servicecatalog/addauth");
			mav.addObject("service", dbService);
			mav.addObject("authTypes", authManagerService.getUserAuthProviders());
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.servicecatalog.addoauth.error");
			return mav;

		}

		ModelAndView mav = getDefaultModelAndView("redirect:/user/servicecatalog/" + service.getId() + "/details");
		return mav;
	}

	@RequestMapping(value = "/{type}/{id}/addoperationtemplate", method = RequestMethod.GET)
	public ModelAndView addOperationTemplate(@PathVariable("id") int operationId, @PathVariable("type") int serviceType) {

		logger.info("Get service operation details to edit description template");

		ModelAndView mav = null;
		
		if(serviceType == VmeConstants.SERVICECATALOG_TYPE_REST){
			mav = getDefaultModelAndView("user/servicecatalog/addresttemplate");			
	
			RestOperationDTO operation = restOperationService.findRestOperationByIdAsDTO(operationId);
			mav.addObject("operation", operation);
			
			RestOperation ro = restOperationService.findRestOperationById(operationId);								
			mav.addObject("service", ro.getServiceCatalog());			
		} else {
			mav = getDefaultModelAndView("user/servicecatalog/addsoaptemplate");			
			
			SoapOperationDTO operation = soapOperationService.findSoapOperationAsDTO(operationId);			
			mav.addObject("operation", operation);	
			
			SoapOperation ro = soapOperationService.findSoapOperation(operationId);								
			mav.addObject("service", ro.getServiceCatalog());						
		}
		return mav;
	}
	
	@RequestMapping(value = "/addresttemplate/execute", method = RequestMethod.POST)
	public ModelAndView addRestTemplateExecute(@ModelAttribute OperationTemplateDTO operation) {

		logger.info("Create a description template for a Rest operation");

		RestOperation restOperation = this.restOperationService.findRestOperationById(operation.getOperationId());
		
		ModelAndView mav = getDefaultModelAndView("redirect:/user/servicecatalog/" + restOperation.getServiceCatalog().getId() + "/details");
				
		try{
			this.restOperationService.addRestOperationTemplate(operation);
		
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = getDefaultModelAndView("user/servicecatalog/"+restOperation.getServiceCatalog().getType()+"/"+operation.getOperationId()+"addoperationtemplate");
		}
		
		return mav;
	}		
	
	@RequestMapping(value = "/addsoaptemplate/execute", method = RequestMethod.POST)
	public ModelAndView addSoapTemplateExecute(@ModelAttribute OperationTemplateDTO operation) {

		logger.info("Create a description template for a Soap operation");

		SoapOperation soapOperation = this.soapOperationService.findSoapOperation(operation.getOperationId());
		
		ModelAndView mav = getDefaultModelAndView("redirect:/user/servicecatalog/" + soapOperation.getServiceCatalog().getId() + "/details");
				
		try{
			this.soapOperationService.addSoapOperationTemplate(operation);
		
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = getDefaultModelAndView("user/servicecatalog/"+soapOperation.getServiceCatalog().getType()+"/"+operation.getOperationId()+"addoperationtemplate");
		}
		
		return mav;
	}	
	
//	@RequestMapping(value = "/{type}/{id}/editoperationtemplate", method = RequestMethod.GET)
//	public ModelAndView editOperationTemplate(@PathVariable("id") int templateId, @PathVariable("type") int serviceType) {
//
//		logger.info("Get service operation template to edit description template");
//
//		ModelAndView mav = null;				
//		
//		if(serviceType == VmeConstants.SERVICECATALOG_TYPE_REST){
//			mav = getDefaultModelAndView("user/servicecatalog/editresttemplate");										
//				
//			mav.addObject("operation", restOperationService.findTemplateById(templateId));
//		} else {
//			mav = getDefaultModelAndView("user/servicecatalog/editsoaptemplate");						
//				
//			mav.addObject("operation", soapOperationService.findTemplateById(templateId));			
//		}
//		return mav;
//	}
//	
//	@RequestMapping(value = "/editresttemplate/execute", method = RequestMethod.POST)
//	public ModelAndView editRestTemplateExecute(@ModelAttribute RestOperationTemplate template) {
//
//		logger.info("Edit a description template for a Rest operation");		
//		
//		ModelAndView mav = getDefaultModelAndView("redirect:/user/servicecatalog/" + template.getOperation().getServiceCatalog().getId() + "/details");
//				
//		try{
//			this.restOperationService.updateRestOperationTemplate(template);
//		
//		} catch (Exception ex) {
//			logger.error(ex.getMessage());
//			mav = getDefaultModelAndView("user/servicecatalog/"+template.getOperation().getServiceCatalog().getType()+"/"+template.getId()+"editoperationtemplate");
//		}
//		
//		return mav;
//	}		
//			
//	
//	@RequestMapping(value = "/addsoaptemplate/execute", method = RequestMethod.POST)
//	public ModelAndView addSoapTemplateExecute(@ModelAttribute SoapOperationTemplate template) {
//
//		logger.info("Edit a description template for a Soap operation");
//		
//		ModelAndView mav = getDefaultModelAndView("redirect:/user/servicecatalog/" + template.getOperation().getServiceCatalog().getId() + "/details");
//				
//		try{
//			this.soapOperationService.updateSoapOperationTemplate(template);
//		
//		} catch (Exception ex) {
//			logger.error(ex.getMessage());
//			mav = getDefaultModelAndView("user/servicecatalog/"+template.getOperation().getServiceCatalog().getType()+"/"+template.getId()+"editoperationtemplate");
//		}
//		
//		return mav;
//	}	
	
	@RequestMapping(value = "/{type}/{id}/viewoperationtemplate", method = RequestMethod.GET)
	public ModelAndView viewOperationTemplate(@PathVariable("id") int templateId, @PathVariable("type") int serviceType) {

		logger.info("Get service operation template to view description template");

		ModelAndView mav = null;				
		
		try{
			if(serviceType == VmeConstants.SERVICECATALOG_TYPE_REST){
				mav = getDefaultModelAndView("user/servicecatalog/viewresttemplate");																	
				
				mav.addObject("template", restOperationService.findTemplateByIdDTO(templateId));
				
				RestOperation ro = restOperationService.findOperationByTemplate(templateId);								
				mav.addObject("service", ro.getServiceCatalog());
			} else {
				mav = getDefaultModelAndView("user/servicecatalog/viewsoaptemplate");						
					
				mav.addObject("template", soapOperationService.findTemplateByIdDTO(templateId));
				
				SoapOperation ro = soapOperationService.findOperationByTemplate(templateId);				
				mav.addObject("service", ro.getServiceCatalog());
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			if(serviceType == VmeConstants.SERVICECATALOG_TYPE_REST){
				
				RestOperation restOperation = this.restOperationService.findOperationByTemplate(templateId);
				mav = getDefaultModelAndView("redirect:/user/servicecatalog/" + restOperation.getServiceCatalog().getId() + "/details");
				
			}else{
				SoapOperation soapOperation = this.soapOperationService.findOperationByTemplate(templateId);
				mav = getDefaultModelAndView("redirect:/user/servicecatalog/" + soapOperation.getServiceCatalog().getId() + "/details");
			}						
		}
		return mav;
	}

	
	@RequestMapping(value = "/{type}/{id}/deleteoperationtemplate", method = RequestMethod.GET)
	public ModelAndView deleteOperationTemplate(@PathVariable("id") int templateId, @PathVariable("type") int serviceType) {

		logger.info("Delete operation description template");

		ModelAndView mav = null;
		
		if(serviceType==VmeConstants.SERVICECATALOG_TYPE_REST){
			RestOperation restOperation = this.restOperationService.findOperationByTemplate(templateId);
			
			mav = getDefaultModelAndView("redirect:/user/servicecatalog/" + restOperation.getServiceCatalog().getId() + "/details");
			
			this.restOperationService.deleteRestOperationTemplate(templateId);
		} else {
			SoapOperation soapOperation = this.soapOperationService.findOperationByTemplate(templateId);
			
			mav = getDefaultModelAndView("redirect:/user/servicecatalog/" + soapOperation.getServiceCatalog().getId() + "/details");
			
			this.soapOperationService.deleteSoapOperationTemplate(templateId);
			
		}
		return mav;
	}
	
	@RequestMapping(value = "/import/fromurl")
	@ResponseBody
	public ObjectNode addServiceFromUrl(@RequestBody GenericService gs) 
	{
		String name = gs.getServiceName();
		String type = gs.getServiceType();
		String url = gs.getServiceUrl();
		
		logger.info("Execute Add " + type + " Service from Url");

		ObjectNode node = JsonNodeFactory.instance.objectNode();		
		
		ServiceCatalog sc;

		try 
		{
			AccountDTO a = getLoggedUser();
			
			if(type.compareTo("wadl") == 0)
			{
				URL wadlUrl = new URL(url);
				InputStream in = wadlUrl.openStream();
				String wadl = IOUtils.toString(in, "UTF-8");
	
				sc = catalogService.addFromWadl(name, wadl,	a.getProfile());
				
				in.close();
			}
			else
			{
				sc = catalogService.addWsdlService(url, a.getProfile());
				sc.setName(name);
				catalogService.updateService(sc);
			}
			
			catalogExtService.add(sc.getId(), url);		
			
			node.put("message", "Service '" + name + "' correctly imported!");
		} 
		catch (Exception ex) 
		{
			logger.error("Exception adding service: " + ex.getMessage());
			
			node.put("message", "Error: Service '" + name + "' can't be imported!");
		}
		
		return node;
	}
	
	@RequestMapping(value = "/import/fromfile")
	@ResponseBody
	public ObjectNode addServiceFromFile(@RequestBody GenericService gs) 
	{
		String name = gs.getServiceName();
		String type = gs.getServiceType();
		String file = gs.getServiceFile();
		
		logger.info("Execute Add " + type + " Service from File");

		ObjectNode node = JsonNodeFactory.instance.objectNode();		
		
		ServiceCatalog sc;

		try 
		{
			AccountDTO a = getLoggedUser();
			
			if(type.compareTo("wadl") == 0)
			{
				sc = catalogService.addFromWadl(name, file, a.getProfile());
			}
			else
			{
				sc = catalogService.addWsdlService(file.getBytes(), a.getProfile());
				sc.setName(name);
				catalogService.updateService(sc);
			}
			
			catalogExtService.add(sc.getId());		
			
			node.put("message", "Service '" + name + "' correctly imported!");
		} 
		catch (Exception ex) 
		{
			logger.error("Exception adding service: " + ex.getMessage());
			
			node.put("message", "Error: Service '" + name + "' can't be imported!");
		}
		
		return node;
	}
	
	// IVAN
	// Swagger extension
	
	@RequestMapping(value = "/{id}/getswagger.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody JsonNode getSwaggerDescription(@PathVariable("id") int serviceId)
	{
		try {
			return objectMapper.readTree(catalogService.findService(serviceId).getContent());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} 
	}
}
