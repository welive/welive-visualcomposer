package com.mediigea.vme2.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.exceptions.InvokeWorkflowException;
import com.mediigea.vme2.exceptions.RestException;
import com.mediigea.vme2.exceptions.RestJsonpException;
import com.mediigea.vme2.exceptions.UserNotAuthorizedException;
import com.mediigea.vme2.service.GuiEditorService;
import com.mediigea.vme2.service.WorkflowService;

@Controller
@RequestMapping(value = "/gadget")
public class GadgetPublicController extends BaseController{	
		
	@Autowired
	private ObjectMapperExt objectMapper;
	
	@Autowired
	private WorkflowService workflowService;
		
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private GuiEditorService guiEditorService;
	
	private static final Logger logger = LoggerFactory.getLogger(GadgetPublicController.class);	
	
	/**
	 * Public method to execute mashup workflow 
	 * @param projectId
	 * @param callback
	 * @param formInputs
	 * @param request
	 * @param response
	 * @throws RestException
	 * @throws RestJsonpException
	 */
	@RequestMapping(value = "{id}/emmlproxy.json", method = RequestMethod.GET)
	public void emmlProxy(@PathVariable("id") Integer projectId, @RequestParam("callback") String callback, @RequestParam Map<String, String> formInputs, HttpServletRequest request, HttpServletResponse response) throws RestException, RestJsonpException {				
			try {
				logger.debug("Invoking workflow for project [id]: "+projectId);
				
				//Prende il session id in modo da passarlo all'invoker per la verifica eventuale di autenticazione oauth per le operations 
				String userConnector = RequestContextHolder.currentRequestAttributes().getSessionId();
				
				//Invoca il mashup
				String result = workflowService.invokeWorkflow(projectId.intValue(), formInputs, userConnector);

				logger.debug("Invoke result" + result);
				
				//Setta la response in moda da restituire jsonp
				response.setContentType("text/javascript; charset=UTF-8");
				   PrintWriter out = response.getWriter();
				   out.print(callback + "(" + result + ")");

			} catch (UserNotAuthorizedException e) {
				//String warning = messageSource.getMessage("message.error.usernotauthorized", null, Locale.ENGLISH);
				//logger.warn(warning);
				String serverUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
				
				//Nel caso l'utente non abbia eseguito autenticazione oauth viene rilanciata un eccezione contenente gli url di redirect
				//throw new RestJsonpException(callback, HttpStatus.UNAUTHORIZED, serverUrl + "/oauth/" + e.getServiceId() + "/authorize");
				throw new RestJsonpException(callback, HttpStatus.UNAUTHORIZED, serverUrl + "/oauth/" + e.getServiceId() + "/" + e.getOperationId() + "/authorize"); // IVAN: modified to allow auth at operation level
			
			} catch (InvokeWorkflowException e) {
				String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e);

			} catch (JsonProcessingException e) {
				String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e);

			} catch (IOException e) {
				String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e);
			}
	}
	
	// IVAN
	// quel che segue riguarda le estensioni del vme
	
	@RequestMapping(value = "{id}/emmlproxy2.json", method = RequestMethod.GET)
	public void emmlProxy2(@PathVariable("id") Integer id, @RequestParam("callback") String callback, @RequestParam Map<String, String> formInputs, HttpServletRequest request, HttpServletResponse response) throws RestException, RestJsonpException {				
			//try {
				System.out.println("Invoking workflow for mockup project [id]: "+id);
				
				//Prende il session id in modo da passarlo all'invoker per la verifica eventuale di autenticazione oauth per le operations 
				String userConnector = RequestContextHolder.currentRequestAttributes().getSessionId();
				
				List<Project> mashupList = guiEditorService.findMashupProjects(guiEditorService.findProject(id));
				
				JSONObject jsonCombined = new JSONObject();
				JSONObject json;
				
				int mashupId, mid;
				String tmp[];
				String result;
				
				boolean invoke = false;
				
				for(int i = 0; i < mashupList.size(); i++)
				{
					invoke = false;
					
					mashupId = mashupList.get(i).getId();
					
					Map<String, String> inputParameters = new HashMap<String, String>();
					
					Iterator<Entry<String, String>> it = formInputs.entrySet().iterator();
					
					while (it.hasNext()) 
					{						
						Entry<String, String> pairs = it.next();
						logger.debug("[Key: " + pairs.getKey() + ", Value: " + pairs.getValue() + "]");
						if(pairs.getKey().compareTo("callback") == 0) continue;
						
						tmp = pairs.getKey().split("_");
						
						if(pairs.getKey().startsWith("mashup_") && Integer.parseInt(tmp[1]) == mashupId && Boolean.parseBoolean(pairs.getValue()) == true)
						{
							invoke = true;
							continue;
						}
						
						try
						{
							mid = Integer.parseInt(tmp[tmp.length - 1]);
						}
						catch(Exception ex)
						{
							continue;
						}

						if(mashupId == mid)
						{	
							if(pairs.getValue().trim().compareTo("") != 0)
								inputParameters.put(pairs.getKey(), pairs.getValue());
							//System.out.println("..... " + pairs.getKey().substring(0, pairs.getKey().lastIndexOf("_")));
						}
					}
					
					if(invoke)
					{
						try
						{
							result = workflowService.invokeWorkflow(mashupId, inputParameters, userConnector);
						} catch (UserNotAuthorizedException e) {System.out.println("userexception gadgeteditor");
							result = "";
							//String warning = messageSource.getMessage("message.error.usernotauthorized", null, Locale.ENGLISH);
							//logger.warn(warning);
							String serverUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
							
							//Nel caso l'utente non abbia eseguito autenticazione oauth viene rilanciata un eccezione contenente gli url di redirect
							//throw new RestJsonpException(callback, HttpStatus.UNAUTHORIZED, serverUrl + "/oauth/" + e.getServiceId() + "/authorize");
							throw new RestJsonpException(callback, HttpStatus.UNAUTHORIZED, serverUrl + "/oauth/" + e.getServiceId() + "/" + e.getOperationId() + "/authorize"); // IVAN: modified to allow auth at operation level
						
						} catch (InvokeWorkflowException e) {
							result = "";
							String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
							logger.error(error, e);

							throw new RestException(error, e);

						}
					
						logger.debug(mashupList.get(i).getName() + " - invoke result: " + result);
						
						if(result.compareTo("") != 0)
						try 
						{
							json = new JSONObject(result);							
							jsonCombined.put(mashupList.get(i).getName(), json);
							
						} catch (JSONException e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
				logger.debug("Combined result: " + jsonCombined.toString());
				
				
				try
				{
				   PrintWriter out = response.getWriter();
				   // IVAN
				   // for no-jsonp request (via app)
				   if(callback.compareTo("") == 0)
				   {
					   response.setContentType("application/json; charset=UTF-8");
					   
					   out.print(jsonCombined.toString());
				   }
				   else
				   {
					   //Setta la response in moda da restituire jsonp
					   response.setContentType("text/javascript; charset=UTF-8");
					   
					   out.print(callback + "(" + jsonCombined.toString() + ")");
				   }
				   logger.debug("#### response: " + out.toString());
				}
				catch (IOException e) {
					String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
					logger.error(error, e);
			
					throw new RestException(error, e);
				}

			/*} catch (UserNotAuthorizedException e) {
				String warning = messageSource.getMessage("message.error.usernotauthorized", null, Locale.ENGLISH);
				logger.warn(warning);
				String serverUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
				
				//Nel caso l'utente non abbia eseguito autenticazione oauth viene rilanciata un eccezione contenente gli url di redirect
				throw new RestJsonpException(callback, HttpStatus.UNAUTHORIZED, serverUrl + "/oauth/" + e.getServiceId() + "/authorize");
			
			} catch (InvokeWorkflowException e) {
				String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e);

			} catch (JsonProcessingException e) {
				String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e);

			} catch (IOException e) {
				String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e);
			}*/
	}
}