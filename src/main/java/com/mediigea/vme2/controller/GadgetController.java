package com.mediigea.vme2.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.dto.GadgetPrefsDTO;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.service.GuiEditorService;
import com.mediigea.vme2.service.ProjectService;
import com.mediigea.vme2.service.SocialGadgetService;
import com.mediigea.vme2.util.VmeConstants;

/**
 * Controller to receive requests about management of gadgets
 * @author Carmine Ruffino
 *
 */
@Controller
@RequestMapping(value = "/user/gadget")
public class GadgetController extends BaseController{
	private static final Logger logger = LoggerFactory.getLogger(GadgetController.class);
	
	@Autowired
	private ProjectService projectService;

	@Autowired
	private GuiEditorService guiEditorService;
	
	@Autowired
	private SocialGadgetService socialGadgetService;	
	
	@Autowired
	private MessageSource messageSource;	
	
	/*@RequestMapping(value = "{id}/create", method = RequestMethod.GET)
	public @ResponseBody
	void createGadget(@PathVariable("id") Integer id, HttpServletRequest request) throws RestException {

		logger.debug("Saving resources to file system");

		try {
			logger.debug("Creating gadget xml definition");
			String serviceInvokeUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();						
			
			socialGadgetService.writeGadget(id, serviceInvokeUrl);

		} catch (WriteGadgetException e) {
			String error = messageSource.getMessage("message.error.writegadget", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}
	}*/

	@RequestMapping(value = "{id}/savegadget", method = RequestMethod.GET)	
	public ModelAndView gotoEditGadget(@PathVariable("id") Integer id, @ModelAttribute GadgetPrefsDTO gadgetPrefs, HttpServletRequest request) {
			
		GuiProject prj = this.guiEditorService.findProject(id);
		
		logger.info("Reading Gui project info to create/edit gadget");
		AccountDTO a = getLoggedUser();
		ModelAndView mav = getDefaultModelAndView("user/guiproject/gadget");
		
		mav.addObject("project", prj);
									
		return mav;
	}
		
	@RequestMapping(value = "{id}/gadgetoperationexecute", method = RequestMethod.POST, params = { "save" })
	@ResponseStatus(value = HttpStatus.OK)
	public ModelAndView editGadget(@PathVariable("id") Integer id, @ModelAttribute GadgetPrefsDTO gadgetPrefs, HttpServletRequest request) {
		ModelAndView mav = getDefaultModelAndView("redirect:/user/gadget/"+id+"/savegadget");
		
		try {			
			logger.debug("Creating gadget xml definition");
			String serviceInvokeUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
			
			guiEditorService.updateGadgetPrefs(id, gadgetPrefs);
			
			socialGadgetService.writeGadget(id, serviceInvokeUrl);
			
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.gadget.success");
			
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.gadget.error");
		}
		return mav;
	}	
	@RequestMapping(value = "{id}/gadgetoperationexecute", method = RequestMethod.POST, params = { "export" })
	public void exportGadget(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response, Locale locale) {

		AccountDTO a = getLoggedUser();

		if (a != null) {

			try {
				InputStream is = socialGadgetService.getGadgetArchive(id);

				response.setHeader("Content-disposition", "attachment;filename=" + VmeConstants.GADGET_ARCHIVE_FILENAME_PREFIX + id + ".zip");
				response.setContentType("application/zip");
				response.setContentLength(is.available());

				FileCopyUtils.copy(is, response.getOutputStream());
				
				String activityDescription = "Export Gadget: " + guiEditorService.findProject(id).getName();
				activityService.activityStreamRecordSave(a.getProfile(), 
						 								AbstractDAO.ActivityType.EXPORT_MOCKUP_GADGET.getValue(), 
						 								activityDescription);	
				
			} catch (IOException e) {
				String error = messageSource.getMessage("message.error.readgadget", null, locale);
				logger.error(error, e);
			}
		}
	}
	/**
	 * @return the projectService
	 */
	public ProjectService getProjectService() {
		return projectService;
	}

	/**
	 * @param projectService the projectService to set
	 */
	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	/**
	 * @return the socialGadgetService
	 */
	public SocialGadgetService getSocialGadgetService() {
		return socialGadgetService;
	}

	/**
	 * @param socialGadgetService the socialGadgetService to set
	 */
	public void setSocialGadgetService(SocialGadgetService socialGadgetService) {
		this.socialGadgetService = socialGadgetService;
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}	
}
