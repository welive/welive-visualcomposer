package com.mediigea.vme2.controller;

import it.eng.GenericUtils;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/schema")
public class SchemaController  extends BaseController
{
	private static final Logger logger = LoggerFactory.getLogger(SchemaController.class);
	
	@RequestMapping(value = "/xsd/{schemaFilename}.xsd", method=RequestMethod.GET, produces = "application/xml")
	public @ResponseBody String getXSD(@PathVariable("schemaFilename") String schemaFilename)
	{
		InputStream xsd = null;
		
		try 
		{
			xsd = GenericUtils.class.getClassLoader().getResourceAsStream(schemaFilename + ".xsd");
			
			String xsd_s = IOUtils.toString(xsd, "UTF-8");
	    	
			xsd.close();
			
			return xsd_s;
		} 
		catch (Exception e) 
		{
			logger.error("Error reading " + schemaFilename);
			
			return "Error: Schema '" + schemaFilename + ".xsd' doesn't exist";
		}
	}
	
	@RequestMapping(value = "/test/{filename}.xml", method=RequestMethod.GET, produces = "application/xml")
	public @ResponseBody String getTestXML(@PathVariable("filename") String filename)
	{
		InputStream xml = null;
		
		try 
		{
			xml = GenericUtils.class.getClassLoader().getResourceAsStream(filename + ".xml");
			
			String xml_s = IOUtils.toString(xml, "UTF-8");
	    	
			xml.close();
			
			return xml_s;
		} 
		catch (Exception e) 
		{
			logger.error("Error reading " + filename + ".xml");
			
			return "Error: Schema '" + filename + ".xml' doesn't exist";
		}
	}

}
