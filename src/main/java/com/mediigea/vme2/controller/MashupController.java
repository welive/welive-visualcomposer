package com.mediigea.vme2.controller;

import it.eng.PropertyGetter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.service.ProjectOperationService;
import com.mediigea.vme2.service.ProjectService;
import com.mediigea.vme2.service.RestOperationService;
import com.mediigea.vme2.service.WorkflowService;

@Controller
@RequestMapping(value = "/mashup")
public class MashupController extends BaseController 
{
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private ProjectOperationService projectOperationService;	
	
	@Autowired
	private RestOperationService restOperationService;
	
	@Autowired
	private WorkflowService workflowService;
	
	private static final Logger logger = LoggerFactory.getLogger(MashupController.class);
	
	
	@RequestMapping(value = "/invoke/{mashupId}", method=RequestMethod.POST)
	//public @ResponseBody ObjectNode invokeMashup(@PathVariable("mashupId") String mashupId, @RequestBody ObjectNode params)
	public @ResponseBody JsonNode invokeMashup(@PathVariable("mashupId") String mashupId, @RequestParam Map<String, String> params, HttpServletRequest request, HttpServletResponse servletResponse)
	{		
		if(projectService.findProject(Integer.valueOf(mashupId)) == null)
		{
			ObjectNode response = JsonNodeFactory.instance.objectNode();
			
			response.put("error", true);
			response.put("message", "No mashup with id " + mashupId);
			
			return response;
		}
		
		String authorization = request.getHeader("Authorization");
		String access_token = null;
		
		if(authorization != null && (authorization.startsWith("Bearer") || authorization.startsWith("bearer")))
			access_token = authorization.split(" ")[1];
		
		try
		{	
			JsonNode res = workflowService.runMashup(Integer.valueOf(mashupId), params, access_token);	
			
			if(!res.isArray())
			{
				if(res.has("__EMML_ERROR"))
				{
					ObjectNode emmlErr = (ObjectNode)res.get("__EMML_ERROR");
					emmlErr.put("error", true);					
					emmlErr.remove("lineNumber");
					emmlErr.remove("lineDetails");
					
					return emmlErr;					
				}
			}
			
			return res;
		} 
		catch(Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			ObjectNode response = JsonNodeFactory.instance.objectNode();
			
			response.put("error", true);
			response.put("message", e.getMessage());
			
			servletResponse.setStatus(500);
			
			return response;
		} 
	}
	
	@RequestMapping(value = "/invoke/{mashupId}/spec/xwadl", method=RequestMethod.GET, produces = "application/xml")
	public @ResponseBody String getWADL(@PathVariable("mashupId") String mashupId)
	{
		File initialFile = new File(PropertyGetter.getProperty("app.baseDir") 
				+ File.separatorChar + "vme-editor" + File.separatorChar + "wadl" + File.separatorChar + mashupId + ".xml");
	    
		try 
		{
			InputStream wadlStream = new FileInputStream(initialFile);
			
			String wadlString = IOUtils.toString(wadlStream, "UTF-8");
			
			wadlStream.close();
			
			return wadlString;
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
}
