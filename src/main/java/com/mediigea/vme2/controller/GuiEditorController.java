package com.mediigea.vme2.controller;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.swagger.util.Json;
import it.eng.PropertyGetter;
import it.eng.mockuptemplate.TemplateManager;
import it.eng.model.ProjectInfo;
import it.eng.model.Screenshot;
import it.eng.model.SinglePageApplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.dto.BindingInputField;
import com.mediigea.vme2.dto.BindingInputParams;
import com.mediigea.vme2.dto.GuiFileResourceDTO;
import com.mediigea.vme2.dto.GuiProjectDTO;
import com.mediigea.vme2.dto.InputShapeDTO;
import com.mediigea.vme2.dto.ProfileDTO;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.GuiProjectFile;
import com.mediigea.vme2.entity.IdeaWorkgroup;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.exceptions.InvokeWorkflowException;
import com.mediigea.vme2.exceptions.RestException;
import com.mediigea.vme2.exceptions.SelectShapeException;
import com.mediigea.vme2.exceptions.UserNotAuthorizedException;
import com.mediigea.vme2.exceptions.WriteResourceException;
import com.mediigea.vme2.service.GuiEditorService;
import com.mediigea.vme2.service.GuiEditorServiceImpl.ResourceContentType;
import com.mediigea.vme2.service.CommentService;
import com.mediigea.vme2.service.IdeaWorkgroupService;
import com.mediigea.vme2.service.ProfileService;
import com.mediigea.vme2.service.ProjectService;
import com.mediigea.vme2.service.WorkflowService;
import com.mediigea.vme2.service.WorkgroupService;
import com.mediigea.vme2.util.FormatConverter;
import com.mediigea.vme2.util.JsTreeNode;
import com.mediigea.vme2.util.JsonUtils;
import com.mediigea.vme2.util.PagedSearch;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.viewmodel.GuiProjectDetailsViewModel;
import com.mediigea.vme2.viewmodel.GuiProjectMembersViewModel;

/**
 * Controller to receive request about management of the Gui Project (Mockup)
 * @author Carmine Ruffino
 *
 */
@Controller
@RequestMapping(value = "/user/gui")
@SessionAttributes("guiproject")
public class GuiEditorController extends BaseController{
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private GuiEditorService guiEditorService;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private WorkgroupService workgroupService;	
	
	@Autowired
	private IdeaWorkgroupService ideawgService;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private ProfileService profileService;	
	
	@Autowired
	private WorkflowService workflowService;
	
	@Autowired
	private ObjectMapperExt objectMapper;
	
	@Autowired
	private WebServiceController wsController;
	
	private static final Logger logger = LoggerFactory.getLogger(GuiEditorController.class);
	
	private static Configuration cfg;

	static
	{
		cfg = new Configuration(Configuration.VERSION_2_3_25);
		cfg.setDefaultEncoding("UTF-8");
		
		ClassTemplateLoader loader = new ClassTemplateLoader(TemplateManager.class, "/app-templates");
		
		cfg.setTemplateLoader(loader);
	}
	
	private boolean isLoggedUserAuthorized(Profile loggedUser, GuiProject project)
	{
		List<Profile> members = guiEditorService.findProjectMembers(project);
		
		boolean authorized = false;
		
		for(Profile member : members)
		{
			if(member.getId() == loggedUser.getId())
			{
				authorized = true;
				break;
			}
		}
		
		return authorized;
	}
	
	@RequestMapping(value = { "/list", "" }, method = RequestMethod.GET)
	public ModelAndView list(final HttpServletRequest request) {

		logger.info("Gui Project List");

		AccountDTO a = getLoggedUser();

		ModelAndView mav = getDefaultModelAndView("user/guiproject/list");

		List<GuiProjectDTO> projectList = guiEditorService.findAllProjectDTOByProfile(a.getProfile());
		
		// IVAN
		
		boolean canPublishEnabled = Boolean.parseBoolean(PropertyGetter.getProperty("gadget.canPublish.enabled"));
		
		List<Boolean> canPublish = new ArrayList<Boolean>();
		
		boolean authorized = false;
		
		if(canPublishEnabled)
		{
			// call marketplace service to check if logged user is authorized to publish
			RestTemplate restTemplate = new RestTemplate();
			
			String baseUrl = PropertyGetter.getProperty("mkp.canPublish.serviceUrl");
			
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("username", a.getEmail());
		    
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		    
		    HttpEntity<MultiValueMap<String, String>> request2 = new HttpEntity<MultiValueMap<String, String>>(map,headers);
		    
		    ResponseEntity<String> response = restTemplate.exchange(baseUrl, HttpMethod.POST, request2, String.class);
		    	    
		    try 
		    {
				JSONObject json = new JSONObject(response.getBody());
				
				authorized = json.getBoolean("can_publish");
			} 
		    catch (JSONException e) 
		    {
				logger.error("Error parsing response JSON");
				e.printStackTrace();
			}	
	    
		}
				
		IdeaWorkgroup iwg;
		
		Profile owner;
		GuiProject p;
		
		for(int i = 0; i < projectList.size(); i++)
		{
			if(canPublishEnabled)
			{
				p = projectList.get(i).getGuiProject();
				
				owner = guiEditorService.findProjectOwner(p.getId());
				
				iwg = ideawgService.getIdeaWorkgroupByWorkgroupId(p.getWorkgroup().getId());
				
				// if project is associated with an idea...
				if(iwg != null)
				{
					// user can publish if is project owner
					if(owner.getEmail().compareTo(a.getEmail()) == 0)
						canPublish.add(true);
					else
						canPublish.add(false);
				}
				else
				{
					canPublish.add(authorized);			
				}
			}
			else
				canPublish.add(true);
		}
		
		// end
		
		mav.addObject("projectList", projectList);
		mav.addObject("canPublish", canPublish);
		
		return mav;
	}
	
	@RequestMapping(value = { "/search", "" }, method = RequestMethod.POST, params = { "clear" })
	public String clear() {
		
		return "redirect:/user/gui/list";
	}
	
	@RequestMapping(value = { "/search", "" }, method = RequestMethod.POST, params = { "search" })
	public ModelAndView search(@RequestParam String criteria) {

		logger.info("GuiProjects search");
		
		AccountDTO a = getLoggedUser();

		logger.info("GuiProjects View request from user: "+a.getEmail());
		
		ModelAndView mav = getDefaultModelAndView("user/guiproject/list");

		List<GuiProject> projectList = guiEditorService.searchProject(a.getProfile(), criteria);
		
		mav.addObject("projectList", projectList);

		return mav;
	}		
	
	@RequestMapping(value = "{id}/details", method = RequestMethod.GET)
	public ModelAndView details(@PathVariable("id") Integer id, HttpServletResponse servletResponse) {

		logger.info("Detail project View");

		ModelAndView mav = null;
		try 
		{
			GuiProjectDetailsViewModel vm = new GuiProjectDetailsViewModel(guiEditorService, id, getLoggedUser().getProfile());
			
			if(!isLoggedUserAuthorized(getLoggedUser().getProfile(), vm.getProject()))
			{
				servletResponse.setStatus(401);
				return null;
			}
			
			mav = getDefaultModelAndView("user/guiproject/detail");
			mav.addObject("model", vm);
			mav.addObject("guiproject", vm.getProject());
			
			// IVAN
			IdeaWorkgroup ideawg = ideawgService.getIdeaWorkgroupByWorkgroupId(vm.getProject().getWorkgroup().getId());
			mav.addObject("ideawg", ideawg);
			
			List<Project> mashupAssociated = guiEditorService.findMashupProjects(guiEditorService.findProject(id));
			mav.addObject("mashupProjectList", mashupAssociated);
			
			// END
			
			// sono entrato nei dettagli del progetto, quindi cancello 
			// i messaggi non letti
			commentService.setReadedComment(vm.getProject(), getLoggedUser().getProfile());
			
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = new ModelAndView("redirect:/user/guiproject/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.details.error");

		}
		return mav;
	}	
	

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView add() {

		logger.info("Add project View");

		AccountDTO a = getLoggedUser();
		List<Workgroup> wgList = workgroupService.findByProfile(a.getProfile());

		if (wgList == null || wgList.isEmpty()) {

			ModelAndView mav = getDefaultModelAndView("redirect:/user/gui/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.add.workgroupasscoaition.error");

			return mav;
		}

		List<Project> mashupProjects = projectService.findAllByProfile(getLoggedUser().getProfile());
		
		ModelAndView mav = getDefaultModelAndView("user/guiproject/add");
		mav.addObject("mashupProjectList", mashupProjects);
		mav.addObject("workgroupList", wgList);
		mav.addObject("project", new GuiProject());

		return mav;
	}	
	
	@RequestMapping(value = "/wizard", method = RequestMethod.GET)
	public ModelAndView addByWizard() {

		logger.info("Wizard View");

		AccountDTO a = getLoggedUser();
		List<Workgroup> wgList = workgroupService.findByProfile(a.getProfile());

		if (wgList == null || wgList.isEmpty()) {

			ModelAndView mav = getDefaultModelAndView("redirect:/user/gui/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.add.workgroupasscoaition.error");

			return mav;
		}

		List<Project> mashupProjects = projectService.findAllByProfile(getLoggedUser().getProfile());
		
		ModelAndView mav = getDefaultModelAndView("user/guiproject/wizard");
		mav.addObject("mashupProjectList", mashupProjects);
		mav.addObject("workgroupList", wgList);
		mav.addObject("project", new GuiProject());

		return mav;
	}	
	
	// IVAN 
	// create a new mockup project from the wizard
	@RequestMapping(value = "/add/fromwizard", method = RequestMethod.POST)
	public @ResponseBody int addFromWizard(@RequestBody ObjectNode mockup) 
	{
		AccountDTO a = getLoggedUser();
		Profile profile = a.getProfile();
		
		Integer workgroupId = Integer.valueOf(mockup.get("projectInfo").get("workgroupId").textValue());
		
		GuiProject project = new GuiProject();
		project.setName(mockup.get("projectInfo").get("name").textValue());
		project.setDescription(mockup.get("projectInfo").get("description").textValue());
		project.setWorkgroup(workgroupService.findWorkgroup(workgroupId));
		project.setGadgetTitle(mockup.get("projectInfo").get("title").textValue());
		project.setGadgetAuthor(profile.getFirstName() + " " + profile.getLastName());
		project.setGadgetAuthorEmail(profile.getEmail());
		
		project = guiEditorService.addProject(project, profile);
		
		// IVAN
		// comunico all'OIA che e' presente un nuovo progetto per il workgroup associato all'idea
		IdeaWorkgroup iw = ideawgService.getIdeaWorkgroupByWorkgroupId(project.getWorkgroup().getId());
		
		if(iw != null)
		{
			try
			{			
				wsController.updateOIAprojects(project.getId(), project.getName(), true, iw.getIdeaId());
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		// end
		
		//if(profile.getLevel() != VmeConstants.USER_LEVEL_MOCKUPDESIGNER)
		if(mockup.get("projectInfo").has("mashupId") && mockup.get("projectInfo").get("mashupId") != null)
		{
			Integer mashupProjectId = Integer.valueOf(mockup.get("projectInfo").get("mashupId").textValue());
			guiEditorService.mashupProjectAdd(project.getId(), mashupProjectId);
		}		
		
		mockup.put("id", Integer.toString(project.getId()));
		
		TemplateManager.buildTemplate(guiEditorService, projectService, profile, mockup);
		
		return project.getId();
	}
	
	@RequestMapping(value = "/add/execute", method = RequestMethod.POST)
	public ModelAndView addExecute(@ModelAttribute GuiProject project, BindingResult result, @RequestParam String addmashupsId) {

		logger.info("Execute Add project");

		AccountDTO a = getLoggedUser();
		Profile profile = a.getProfile();
		/*
		if(project.getProject().getId()==-1){
			project.setProject(null);			
		}	*/	
		
		guiEditorService.addProject(project, profile);
		
		// IVAN		
		IdeaWorkgroup iw = ideawgService.getIdeaWorkgroupByWorkgroupId(project.getWorkgroup().getId());
		
		if(iw != null)
		{
			try
			{		
				// comunico all'OIA che e' presente un nuovo progetto per il workgroup associato all'idea
				wsController.updateOIAprojects(project.getId(), project.getName(), true, iw.getIdeaId());
				
				// idea workgroup members
				Iterator<Profile> members = profileService.findWorkgroupMembers(project.getWorkgroup().getId()).iterator();
				
				Profile member = null;
				
				while(members.hasNext())
				{
					member = members.next();
					
					if(member.getId() != profile.getId())
					{	
						// add workgroup profiles as project members
						guiEditorService.memberAdd(project.getId(), member.getId());
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		// end
		
		//System.out.println("mashupProjectList "+addmashupsId);
		//add mashup projects to mockup
		for (String s : addmashupsId.split(",")) 
		{
			if(s.compareTo("") != 0)
			{
				Integer mashupProjectId = Integer.valueOf(s);
				try {
					guiEditorService.mashupProjectAdd(project.getId(), mashupProjectId);
					//Profile profile = profileService.findProfile(profileId);					
					/*GuiProject prj = guiEditorService.findProject(project.getId());
					String activityDescription = "Add mashup to Mockup project: " + prj.getName() + " (" + profile.getEmail() + ")";
					activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
							 								AbstractDAO.ActivityType.ADD_MEMBER_TO_MOCKUP.getValue(), 
							 								activityDescription);	*/			
					
				} catch (DataIntegrityViolationException e) {
					logger.debug(e.getMessage());
				}
			}
		}
		
		ModelAndView mav = getDefaultModelAndView("redirect:/user/gui/list");
		
		String activityDescription = "Create Mockup Project: " + project.getName();
		
		activityService.activityStreamRecordSave(profile, 
				 								AbstractDAO.ActivityType.CREATE_MOCKUP_PROJECT.getValue(), 
				 								activityDescription);			
		
		return mav;
	}	
	
	
	@RequestMapping(value = "{id}/export/{type}", method = RequestMethod.GET)
	public void exportHTML(@PathVariable("id") Integer id, @PathVariable("type") String type, HttpServletRequest request, HttpServletResponse response, Locale locale) 
	{

		AccountDTO a = getLoggedUser();
		
		GuiProject project = guiEditorService.findProject(id);

		if (a != null && project != null) 
		{
			try 
			{
				InputStream is = null;
				
				if(type.compareTo("web") == 0)
				{
					is = guiEditorService.getHTMLArchive(id);
					response.setHeader("Content-disposition", "attachment;filename=" + project.getName() + "_html-project.zip");
				}
				else if(type.compareTo("app") == 0)
				{
					is = guiEditorService.getCordovaArchive(id);
					response.setHeader("Content-disposition", "attachment;filename=" + project.getName() + "_cordova-project.zip");
				}
				else
				{
					// TODO manage error
					return;
				}
				
				response.setContentType("application/zip");
				response.setContentLength(is.available());

				FileCopyUtils.copy(is, response.getOutputStream());

			} 
			catch (IOException e) 
			{
				String error = messageSource.getMessage("message.error.readhtmlproject", null, locale);
				logger.error(error, e);
			}
		}
	}	
	
	private int buildAPK(GuiProject project) throws Exception
	{			
		
		System.out.println(">>> Build APK for project " + project.getId());
		
		long startTime = System.currentTimeMillis();
		
		String baseUrl = PropertyGetter.getProperty("oauth.callbackContext");
		
		String appProjectDir = PropertyGetter.getProperty("app.appRepository") + File.separator + "prj_" + project.getId();
		String appBuildDir = PropertyGetter.getProperty("app.appRepository") + File.separator + "app_" + project.getId();
		
		File prjDir = new File(appProjectDir);
		File appDir = new File(appBuildDir);
		
		File platformsDir = new File(appProjectDir + File.separator + "platforms");
	    
		// if app directory for the project doesn't exist then create it
		
	    if (!prjDir.exists()) 
	    {
	    	prjDir.mkdir();
	    }
	    
	    if (!appDir.exists()) 
	    {
	    	appDir.mkdir();
	    }
	    
	    // copy base app to project app directory
	    
	    File sourceDir = new File(PropertyGetter.getProperty("app.appRepository") + File.separator + "base");
	    
	    FileUtils.copyDirectory(sourceDir, prjDir);
	    
	    System.out.println(">>> Project directory '" + prjDir.getAbsolutePath() + "' created");
	    
	    // populate templates
	    
	    Template template = null;
	    
	    StringWriter output = new StringWriter();	    
	    
		    // data to populate templates
	    
		    HashMap<String, String> templateData = new HashMap<String, String>();
		    
		    templateData.put("baseUrl", baseUrl);
		    templateData.put("projectId", Integer.toString(project.getId()));	    
		    
		    Iterator<GuiFileResourceDTO> files = guiEditorService.getResourcesByProject(project.getId(), ResourceContentType.JAVASCRIPT).iterator();
		    
		    GuiFileResourceDTO file = null;
		    
		    String requirejsFiles = "";
		    String filename = "";
		    
		    while(files.hasNext())
		    {
		    	file = files.next();
		    	
		    	if(file.getContentType().compareTo("application/javascript") == 0)
		    	{
		    		filename = file.getFilename().replace(".js", "");
		    		requirejsFiles += "requirejs(['prjjs/" + filename + "'],function(script){window." + filename.replace(" ", "_") + " = script;});";
		    	}
		    }
		    
		    templateData.put("requirejsFiles", requirejsFiles);
		    
		    template = cfg.getTemplate("require_script.ftl");
		    template.process(templateData, output);
		    
		    templateData.put("appScript", output.toString());
		    
		    String htmlContent = guiEditorService.getDefaultHtmlFile(project.getId()).getContent();
		    
		    templateData.put("appContent", htmlContent);
		    
		    templateData.put("appName", project.getName());
		    templateData.put("appDescription", project.getDescription());
		    templateData.put("authorEmail", project.getGadgetAuthorEmail() != null ? project.getGadgetAuthorEmail() : "");
		    templateData.put("authorName", project.getGadgetAuthor() != null ? project.getGadgetAuthor() : "");	    
		    
		    //	    
		
		// populate config.xml
		template = cfg.getTemplate("config_xml.ftl");
		
		output.getBuffer().setLength(0);		
		
		template.process(templateData, output);
		
		String configXML = output.toString();
		
		// populate www/index.html
		template = cfg.getTemplate("index_html.ftl");
		
		output.getBuffer().setLength(0);		
		
		template.process(templateData, output);
		
		String indexHTML = output.toString();
		
		System.out.println(">>> Templates populated");
		
		// end populate
		
		// write files
		File config_xml = new File(appProjectDir + File.separator + "config.xml");
		File index_html = new File(appProjectDir + File.separator + "www" + File.separator + "index.html");
		
		FileUtils.writeStringToFile(config_xml, configXML, false);
		System.out.println(">>> File '" + config_xml.getAbsolutePath() +  "' written");
		
		FileUtils.writeStringToFile(index_html, indexHTML, false);
		System.out.println(">>> File '" + index_html.getAbsolutePath() +  "' written");
	    
		// add android platform to cordova project
	    ProcessBuilder pb = new ProcessBuilder(PropertyGetter.getProperty("app.appRepository") + File.separator + PropertyGetter.getProperty("app.script.initproject"), prjDir.getAbsolutePath());
	    
	    System.out.println(">>> Starting script 'init_project' ...");
	    Process proc = pb.start();

	    BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

	    // read the output from the command

	    String s = null;
	    while ((s = stdInput.readLine()) != null)
	    {
	        System.out.println(s);
	    }
	    
	    int init_result = proc.waitFor();	    
	    System.out.println(">>> Script 'init_project' completed (" + init_result + ")");
	    
	    if(init_result != 0)
	    	return -1;
	    
	    // cordova build
	    pb = new ProcessBuilder(PropertyGetter.getProperty("app.appRepository") + File.separator + PropertyGetter.getProperty("app.script.buildapp"), prjDir.getAbsolutePath(), appDir.getAbsolutePath());
	    
	    System.out.println(">>> Starting script 'build_app' ...");
	    proc = pb.start();

	    stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

	    // read the output from the command
	    
	    while ((s = stdInput.readLine()) != null)
	    {
	        System.out.println(s);
	    }
	    
	    int build_result = proc.waitFor();
	    System.out.println(">>> Script 'build_app' completed (" + build_result + ")");
	    
	    if(build_result != 0)
	    	return -2;
	    
	    // remove cordova platforms directory
	    FileUtils.deleteDirectory(platformsDir);
	    System.out.println(">>> Platforms directory '" + platformsDir.getAbsolutePath() + "' deleted");
	    
	    long endTime = System.currentTimeMillis();
	    
	    System.out.println(">>> Build APK for project " + project.getId() + " completed in " + (endTime - startTime) / 1000 + " seconds");
	    
	    return 0;
	}
	
	/*
	 * OUTPUT
	 * 
	 * {
	 * 	"success" : true/false,
	 * 	"status" : int
	 * }
	 * 
	 * status
	 * - 200 Ok - build done
	 * - 304 Not Modified - build is not necessary
	 * - 401 Unauthorized - missing user authentication
	 * - 403 Forbidden - only owner can build project
	 * - 404 Not Found - project not found
	 * - 409 Conflict - apk is still building
	 * - 500 Internal Server Error - some error occurred
	 */
	
	@RequestMapping(value = "{id}/export_apk", method = RequestMethod.POST)
	public @ResponseBody ObjectNode exportAPK(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response, Locale locale) {

		AccountDTO a = getLoggedUser();
		
		ObjectNode res = JsonNodeFactory.instance.objectNode();
		
		GuiProject project = guiEditorService.findProject(id);

		if (a != null && project != null) 
		{				
			// if apk is still building
			if(project.getIsBuildingApk() == 1)
			{
				res.put("success", false);
				res.put("status", 409);
			}
			// if project has not been updated since last build
			else if(project.getApkBuiltAt() != null && project.getUpdatedAt().compareTo(project.getApkBuiltAt()) <= 0)
			{
				res.put("success", true);
				res.put("status", 304);
			}
			else
			{
				Profile owner = guiEditorService.findProjectOwner(id);
				
				if(a.getProfile().getId() == owner.getId())
				{
					try 
					{
						project.setIsBuildingApk(1);
						guiEditorService.updateProject(project, false);
						
						int build_res = buildAPK(project);
						
						if(build_res == 0)
						{
							project.setApkBuiltAt(new Date());
							
							res.put("success", true);
							res.put("status", 200);
						}
						else
						{
							res.put("success", false);
							res.put("status", 500);
						}
	
					} 
					catch(Exception e) 
					{
						e.printStackTrace();
						
						res.put("success", false);
						res.put("status", 500);
					}	
					
					project.setIsBuildingApk(0);
					guiEditorService.updateProject(project, false);
				}
				else
				{
					res.put("success", false);
					res.put("status", 403);
				}
			}
		}
		else if(a == null)
		{
			res.put("success", false);
			res.put("status", 401);
		}
		else // project == null
		{
			res.put("success", false);
			res.put("status", 404);
		}
		
		return res;
	}
	
	@RequestMapping(value = "{id}/download_apk", method = RequestMethod.GET)
	public void downloadAPK(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response, Locale locale) {

		AccountDTO a = getLoggedUser();

		if (a != null) 
		{
			try 
			{
				GuiProject project = guiEditorService.findProject(id);
				
				String apkPath = PropertyGetter.getProperty("app.appRepository") + File.separator + "app_" + id + File.separator + PropertyGetter.getProperty("app.script.apk");
				
				File apk = new File(apkPath);
				
				if(!apk.exists())
					return;
				
				InputStream is = new FileInputStream(apk);;

				response.setHeader("Content-disposition", "attachment;filename=" + project.getName() + ".apk");
				response.setContentType("application/vnd.android.package-archive");
				response.setContentLength(is.available());

				FileCopyUtils.copy(is, response.getOutputStream());

			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	@RequestMapping(value = "/{id}/editor", method = RequestMethod.GET)
	public ModelAndView draw(@PathVariable("id") Integer id, final HttpServletRequest request, HttpServletResponse servletResponse) throws IOException {

		logger.info("Draw Project View");

		AccountDTO a = getLoggedUser();
		GuiProject prj = guiEditorService.findProject(id);
		
		if(!isLoggedUserAuthorized(getLoggedUser().getProfile(), prj))
		{
			servletResponse.setStatus(401);
			return null;
		}
		
		IdeaWorkgroup iw = ideawgService.getIdeaWorkgroupByWorkgroupId(prj.getWorkgroup().getId());

		if (prj == null) 
		{
			logger.debug("Project not found");

			ModelAndView mav = getDefaultModelAndView("redirect:/user/gui/list");

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.open.error");

			return mav;
		}

		List<Workgroup> workgrs = workgroupService.findByProfile(a.getProfile());

		for (Workgroup w : workgrs) {
			if (w.getId() == prj.getWorkgroup().getId()) {
				
				ModelAndView mav = getDefaultModelAndView("user/guiproject/draw");
				mav.addObject("profile", a.getProfile());
				mav.addObject("project", prj);
				mav.addObject("defaulthtmlfile", this.guiEditorService.getDefaultHtmlFile(prj.getId()));
				mav.addObject("fileresources", this.guiEditorService.getResourcesByProject(id, null));
				mav.addObject("mashupProjectList", projectService.findAllByProfile(getLoggedUser().getProfile()));
				mav.addObject("ideaWorkgroup", iw);
				
				return mav;
			}
		}

		ModelAndView mav = getDefaultModelAndView("redirect:/user/gui/list");

		mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.notinworkgroup");

		mav.addObject("projectIdContextMenu", id);

		return mav;
	}			
	
	@RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable("id") Integer projectId) {

		logger.info("Edit gui project View");

		ModelAndView mav = null;
		//List<String> mashupAssociatedName=new ArrayList<String>();
		try {
			GuiProject p = guiEditorService.findProject(projectId);
			//List<Project> mashupProjects = projectService.findAllByProfile(getLoggedUser().getProfile());
			List<Project> mashupAssociated = guiEditorService.findMashupProjects(guiEditorService.findProject(projectId));
			
			/*
			for(Project m:mashupAssociated){
				mashupAssociatedName.add(m.getName());
			}
			//System.out.println("Mashup associati "+ guiEditorService.findMashupProjects(guiEditorService.findProject(projectId)));
			*/
			
			Profile owner = guiEditorService.findProjectOwner(projectId);
			boolean loggedOwner = owner.getId() == getLoggedUser().getProfile().getId();

			mav = getDefaultModelAndView("user/guiproject/edit");
			mav.addObject("guiproject", p);
			mav.addObject("mashupProjectList", mashupAssociated);
			mav.addObject("loggedOwner", loggedOwner);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = new ModelAndView("redirect:/user/gui/" + projectId + "/details");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.edit.error");

		}

		return mav;
	}	
	
	@RequestMapping(value = "/edit/execute", method = RequestMethod.POST)
	public ModelAndView editExecute(@ModelAttribute("guiproject") GuiProject project, BindingResult result, @RequestParam(required=false) String addmashupsId) {

		logger.info("Execute Edit project");

		ModelAndView mav = null;

		try {
			project = guiEditorService.updateProject(project, true);
			
			if(addmashupsId != null)
			{
				project = guiEditorService.removeAllMashup(project);
				//add mashup projects to mockup
				guiEditorService.addAllMashup(project, addmashupsId);
			}
			
			mav = new ModelAndView("redirect:/user/gui/" + project.getId() + "/details");
			
			String activityDescription = "Edit Mockup Project: " + project.getName();
			activityService.activityStreamRecordSave(getLoggedUser().getProfile(), 
					 								AbstractDAO.ActivityType.EDIT_MOCKUP_METADATA.getValue(), 
					 								activityDescription);				
		} catch (Exception e) {
			e.printStackTrace();
			mav = getDefaultModelAndView("user/guieditor/edit");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.edit.error");
			mav.addObject("project", project);
		}

		return mav;
	}	
	
	@RequestMapping(value = "{id}/mashup/{mashupId}/remove", method = RequestMethod.GET)
	public ModelAndView mashupRemove(@PathVariable Integer id, @PathVariable Integer mashupId) 
	{
		ModelAndView mav = new ModelAndView("redirect:/user/gui/" + id + "/details#projectmashups");
		
		try 
		{
			guiEditorService.mashupRemove(id, mashupId);					
			
			mav.addObject(VmeConstants.MESSAGE_SUCCESS, "message.remove.project.member.success");
		} 
		catch (Exception e) 
		{
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.remove.project.member.error");
		}
		return mav;
	}
	
	@RequestMapping(value = "{mockupid}/link/mashup", method = RequestMethod.POST)
	public ModelAndView linkMashup(@PathVariable("mockupid") Integer mockupId, @RequestParam ("mashupId") String mashupId) {

		logger.info("Execute Edit project");

		ModelAndView mav = null;

		GuiProject mockup = guiEditorService.findProject(mockupId);
		Project mashup = projectService.findProject(Integer.parseInt(mashupId));
		
		try {					
			mockup.setProject(mashup);
			
			guiEditorService.updateProject(mockup, true);
			mav = new ModelAndView("redirect:/user/gui/" + mockupId + "/editor");
			
			String activityDescription = "Edit Mockup Project: " + mockup.getName();
			activityService.activityStreamRecordSave(getLoggedUser().getProfile(), 
					 								AbstractDAO.ActivityType.EDIT_MOCKUP_METADATA.getValue(), 
					 								activityDescription);				
		} catch (Exception e) {

			mav = getDefaultModelAndView("user/guieditor/edit");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.edit.error");
			mav.addObject("project", mockup);
		}

		return mav;
	}	
	
	@RequestMapping(value = "{id}/comments", method = RequestMethod.GET)
	public ModelAndView commentsList(@PathVariable("id") Integer projectId) {

		logger.info("Add comment to project");
		ModelAndView mav = null;
		try {
			GuiProject p = guiEditorService.findProject(projectId);
			Profile owner = guiEditorService.findProjectOwner(projectId);
			boolean loggedOwner = owner.getId() == getLoggedUser().getProfile().getId();

			List<Comment> comments = guiEditorService.findProjectComments(p);
			
			// sono entrato nei commenti del progetto, quindi cancello 
			// i messaggi non letti
			commentService.setReadedComment(p, getLoggedUser().getProfile());
			
			mav = getDefaultModelAndView("user/guiproject/comments");
			mav.addObject("project", p);
			mav.addObject("comments", comments);
			mav.addObject("loggedOwner", loggedOwner);

		} catch (Exception e) {
			logger.error(e.getMessage());
			mav = new ModelAndView("redirect:/user/gui/" + projectId + "/details");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.comments.list.error");

		}
		return mav;
	}

	@RequestMapping(value = "{id}/comments/add", method = RequestMethod.POST)
	public ModelAndView commentAddExecute(@PathVariable("id") Integer projectId, @ModelAttribute Comment formComment, BindingResult result) {

		logger.info("Execute Add comment to project");
		ModelAndView mav = getDefaultModelAndView("redirect:/user/gui/" + projectId + "/details");
		Comment c = null;

		try {
			GuiProject prj = guiEditorService.findProject(projectId);
			Comment comment = new Comment();
			comment.setContent(formComment.getContent());
			comment.setGuiProject(prj);
			comment.setProfile(getLoggedUser().getProfile());

			c = commentService.addComment(comment);

			if (c == null) {

				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.comment.add.error");

			} 
			else {
				String activityDescription = "Comment Mockup project: " + prj.getName();
				activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
						 								AbstractDAO.ActivityType.COMMENT_MOCKUP_PROJECT.getValue(), 
						 								activityDescription);					
			}
		} catch (org.springframework.dao.DataIntegrityViolationException divEx) {

			logger.error("DataIntegrityViolationException: " + divEx.getMessage());

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.comment.add.error");

		} catch (javax.persistence.PersistenceException psEx) {

			logger.error("javax.persistence.PersistenceException: " + psEx.getMessage());

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.comment.add.error");

		} catch (org.hibernate.exception.DataException dataEx) {

			logger.error("DataException: " + dataEx.getMessage());

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.comment.add.error");

		}

		return mav;
	}

	@RequestMapping(value = "{id}/comments/{cId}/delete", method = RequestMethod.GET)
	public ModelAndView commentDelete(@PathVariable("id") Integer projectId, @PathVariable("cId") Integer commentId) {

		Comment comment = commentService.findComment(commentId);

		ModelAndView mav = getDefaultModelAndView("redirect:/user/gui/" + projectId + "/details");
		if (comment.getProfile().getId() == getLoggedUser().getProfile().getId()) {
			try {
				commentService.deleteComment(commentId);
				
				GuiProject prj = guiEditorService.findProject(projectId);
				String activityDescription = "Delete comment on Mockup project: " + prj.getName();
				activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
						 								AbstractDAO.ActivityType.DELETE_COMMENT_MOCKUP_PROJECT.getValue(), 
						 								activityDescription);					
				
			} catch (Exception ex) {
				logger.error("Error delete comment: " + ex.getMessage());
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");
			}
		} else {
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");
		}

		return mav;
	}	
	
	
	@RequestMapping(value = "{id}/delete", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") Integer projectId) {

		ModelAndView mav = null;
		try {
			GuiProject p = guiEditorService.findProject(projectId);
			Profile owner = guiEditorService.findProjectOwner(projectId);
			boolean loggedOwner = owner.getId() == getLoggedUser().getProfile().getId();

			mav = getDefaultModelAndView("user/guiproject/delete");
			mav.addObject("project", p);
			mav.addObject("loggedOwner", loggedOwner);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = new ModelAndView("redirect:/user/gui/" + projectId + "/details");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.delete.error");

		}
		return mav;
	}	
	
	@RequestMapping(value = "{id}/delete/execute", method = RequestMethod.POST)
	public ModelAndView deleteExecute(@PathVariable("id") Integer id) {

		logger.debug("Execute Delete project");

		ModelAndView mav = null;

		try {
			Profile owner = guiEditorService.findProjectOwner(id);
			AccountDTO a = getLoggedUser();
			if (owner != null && owner.getId() == a.getProfile().getId()) {
				
				String prjName = guiEditorService.findProject(id).getName();
				
				commentService.deleteAllByGuiProject(id);
				
				guiEditorService.deleteProject(id);
				mav = new ModelAndView("redirect:/user/gui/list");
				
				String activityDescription = "Delete Mockup Project: " + prjName;
				activityService.activityStreamRecordSave(getLoggedUser().getProfile(), 
						 								AbstractDAO.ActivityType.DELETE_MOCKUP_PROJECT.getValue(), 
						 								activityDescription);	
				
			} else {
				GuiProject p = guiEditorService.findProject(id);
				logger.error("Project delete error: logged user:" + a.getEmail() + "not owner of project " + p.getId() + " " + p.getName());
				mav = getDefaultModelAndView("user/guiproject/delete");
				mav.addObject("project", p);
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.delete.notowner.error");
			}

		} catch (Exception ex) {

			logger.error(ex.getMessage());
			GuiProject p = guiEditorService.findProject(id);
			mav = getDefaultModelAndView("user/guiproject/delete");
			mav.addObject("project", p);
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.delete.error");
		}

		return mav;
	}	
	
	@RequestMapping(value = "{id}/members", method = RequestMethod.GET)
	public ModelAndView membersList(@PathVariable("id") int projectId) {

		ModelAndView mav = null;
		
		try {
			GuiProjectMembersViewModel vm = new GuiProjectMembersViewModel(guiEditorService, projectId, getLoggedUser().getProfile());
			mav = getDefaultModelAndView("user/guiproject/members");
			mav.addObject("model", vm);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = new ModelAndView("redirect:/user/gui/" + projectId + "/details");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.members.error");
		}
		return mav;
	}

	@RequestMapping(value = "{id}/members/add", method = RequestMethod.POST)
	public ModelAndView membersAdd(@PathVariable int id, @RequestParam String addmembersId) {
		ModelAndView mav = new ModelAndView("redirect:/user/gui/" + id + "/details#projectmembers");
		try {
			logger.debug("addmembersId:" + addmembersId);
			for (String s : addmembersId.split(",")) {
				Integer profileId = Integer.valueOf(s);
				try {
					guiEditorService.memberAdd(id, profileId);
					Profile profile = profileService.findProfile(profileId);					
					GuiProject prj = guiEditorService.findProject(id);
					String activityDescription = "Add member to Mockup project: " + prj.getName() + " (" + profile.getEmail() + ")";
					activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
							 								AbstractDAO.ActivityType.ADD_MEMBER_TO_MOCKUP.getValue(), 
							 								activityDescription);				
					
				} catch (DataIntegrityViolationException e) {
					logger.debug(e.getMessage());
				}
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.workgroup.members.add.error");

		}
		return mav;
	}

	@RequestMapping(value = "{id}/members/{profileId}/remove", method = RequestMethod.GET)
	public ModelAndView memberRemove(@PathVariable Integer id, @PathVariable Integer profileId) {
		ModelAndView mav = new ModelAndView("redirect:/user/gui/" + id + "/details#projectmembers");
		try {
			guiEditorService.memberRemove(id, profileId);
						
			Profile profile = profileService.findProfile(profileId);					
			GuiProject prj = guiEditorService.findProject(id);
			String activityDescription = "Remove member from Mockup project: " + prj.getName() + " (" + profile.getEmail() + ")";
			activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
					 								AbstractDAO.ActivityType.REMOVE_MEMBER_FROM_MOCKUP.getValue(), 
					 								activityDescription);				
			
			
			mav.addObject(VmeConstants.MESSAGE_SUCCESS, "message.remove.project.member.success");
		} catch (Exception e) {
			mav.addObject(VmeConstants.MESSAGE_ERROR, "message.remove.project.member.error");

		}
		return mav;
	}
	
	@RequestMapping(value = "{id}/workgroupprofiles/search.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	PagedSearch<ProfileDTO> workgroupProfilesSearchJson(@PathVariable int id, @RequestParam String searchterm, @RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "20") int pagesize) {
		
		PagedSearch<ProfileDTO> result = new PagedSearch<ProfileDTO>();
		
		List<ProfileDTO> list = new ArrayList<ProfileDTO>();
		
		try 
		{
			GuiProject prj = guiEditorService.findProject(id);
			
			Profile owner = guiEditorService.findProjectOwner(id);
			
			PagedSearch<ProfileDTO> wgMembers = profileService.pagedSearchByWorkgroup(prj.getWorkgroup().getId(), searchterm, page, pagesize);
			
			List<Profile> members = guiEditorService.findProjectMembers(prj);
			
			boolean isIn = false;
			int isInCounter = 0;
			
			for(int i = 0; i < wgMembers.getResults().size(); i++)
			{
				if(wgMembers.getResults().get(i).getId() == owner.getId())
					continue;
				
				isIn = false;
				
				for(int j = 0; j < members.size(); j++)
				{
					if(wgMembers.getResults().get(i).getId() != members.get(j).getId())
						continue;
						
					isIn = true;
					isInCounter++;
				}
				
				if(!isIn)
					list.add(wgMembers.getResults().get(i));
			}
			
			result.setResults(list);
			result.setTotal(wgMembers.getTotal() - isInCounter);
			
			// .pagedSearch(searchterm, page, pagesize);
		} 
		catch (Exception ex) 
		{
			logger.error(ex.getMessage());
			// result = new ProfilePagedSearchDTO();
		}

		return result;
	}
	
	// IVAN
	// restituisce la lista dei mashup (di cui l'utente e' membro) di un determinato workgroup
	@RequestMapping(value = "/workgroup/{wgId}/mashupslist.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody PagedSearch<ProjectInfo> getMashupsByWorkgroup(@PathVariable int wgId)
	{
		String hql = "from Project p join fetch p.profileProjects pp where p.workgroup.id=:wgId and pp.pk.profile.id=:profileId";

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("wgId", wgId);
		parameters.put("profileId", getLoggedUser().getProfile().getId());
		
		List<Project> mashups = projectService.findAll(hql, parameters);
		
		List<ProjectInfo> result = new ArrayList<ProjectInfo>();
		ProjectInfo p;
		
		
		for(int i = 0; i < mashups.size(); i++)
		{
			p = new ProjectInfo();
			p.setId(mashups.get(i).getId());
			p.setName(mashups.get(i).getName());
			p.setDescription(mashups.get(i).getDescription());
			
			result.add(p);
		}
		
		return new PagedSearch<ProjectInfo>(result, result.size());
	}
	
	@RequestMapping(value = "/searchmashups.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody PagedSearch<ProjectInfo> searchMashupsByWorkgroupAndTerm(@RequestParam int wgId, @RequestParam String searchterm)
	{
		if(wgId == -1)
			return new PagedSearch<ProjectInfo>(new ArrayList<ProjectInfo>(), 0);
		
		//String hql = "from Project p join fetch p.profileProjects pp where p.workgroup.id=:wgId and pp.pk.profile.id=:profileId and pp.projectRole=1"
		String hql = "from Project p join fetch p.profileProjects pp where p.workgroup.id=:wgId and pp.pk.profile.id=:profileId"
				+ " and ( lower(p.name) like :st or lower(p.description) like :st )";
		
		String st = "%" + searchterm.toLowerCase() + "%";
		
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("wgId", wgId);
		parameters.put("profileId", getLoggedUser().getProfile().getId());
		parameters.put("st", st);
		
		List<Project> mashups = projectService.findAll(hql, parameters);
		
		List<ProjectInfo> result = new ArrayList<ProjectInfo>();
		ProjectInfo p;
		
		
		for(int i = 0; i < mashups.size(); i++)
		{
			p = new ProjectInfo();
			p.setId(mashups.get(i).getId());
			p.setName(mashups.get(i).getName());
			p.setDescription(mashups.get(i).getDescription());
			
			result.add(p);
		}
		
		return new PagedSearch<ProjectInfo>(result, result.size());
	}
	
	@RequestMapping(value = { "/metadata/list.json", "" }, method = RequestMethod.GET, produces = "application/json")
    protected @ResponseBody List<String> handleRequestInternal(HttpServletRequest request) throws Exception {        

        return this.guiEditorService.getPaletteComponent();
    }
	
	@RequestMapping(value = "/{projectId}/file/{id}/save", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
    protected void saveFile(@RequestParam String filecontent, @RequestParam String contenttype, @RequestParam String filename, @PathVariable("projectId") Integer projectId, @PathVariable("id") Integer fileId) throws Exception {
        
		this.guiEditorService.saveFile(projectId, filename, filecontent, ResourceContentType.get(contenttype));		
    }	
	
	
	
	@RequestMapping(value = "{id}/addresource/file", method = RequestMethod.POST)
	public @ResponseBody
	GuiFileResourceDTO addResourceFile(@PathVariable("id") Integer id, MultipartHttpServletRequest request, HttpServletResponse response)
			throws RestException {

		GuiFileResourceDTO dto = null;

		logger.info("Add resource file ");

		try {
			Iterator<String> itr = request.getFileNames();

			MultipartFile mpf = request.getFile(itr.next());

			if (mpf != null) {

				logger.debug("Import resources from file");

				dto = guiEditorService.writeResources(id, mpf);
			}
		} catch (WriteResourceException e) {

			//String error = messageSource.getMessage("message.error.writeresource", null, Locale.ENGLISH);
			logger.error(e.getMessage(), e);

			throw new RestException(e);
		}

		return dto;
	}		
	
	@RequestMapping(value = "{id}/newhtml/file", method = RequestMethod.POST)
	public @ResponseBody
	GuiFileResourceDTO newHtmlFile(@PathVariable("id") Integer projectId, @RequestParam String filename, HttpServletResponse response)
			throws RestException {

		GuiFileResourceDTO dto = null;

		logger.info("Add html file ");

		try 
		{		
			dto = guiEditorService.newHtml(projectId, filename);
			
			// IVAN
			// if this is the first page (after default page), set it as first page in SPA
			GuiProject p = guiEditorService.findProject(projectId);
			
			List<GuiFileResourceDTO> list = getHtmlList(projectId);
			
			if(p.getSpaFirstPageId() == 0 || list.size() == 2)
			{
				p.setSpaFirstPageId(dto.getId());
				guiEditorService.updateProject(p, true);
			}
		} 
		catch (WriteResourceException e) 
		{
			logger.error(e.getMessage(), e);

			throw new RestException(e);
		}

		return dto;
	}		
	
	@RequestMapping(value = "{projectid}/removeresource/{id}/file", method = RequestMethod.GET)
	public @ResponseBody
	void removeResourceFile(@PathVariable("id") Integer id, @PathVariable("projectid") Integer projectId) throws RestException {

		logger.info("remove resource file ");

		try {
			guiEditorService.removeResources(id);
		} catch (WriteResourceException e) {
			//String error = messageSource.getMessage("message.error.writeresource", null, Locale.ENGLISH);
			logger.error(e.getMessage(), e);

			throw new RestException(e);
		}
	}	
	
	@RequestMapping(value = "{projectid}/getimagelist.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	List<GuiFileResourceDTO> getImageList(@PathVariable("projectid") Integer projectId) throws RestException {

		logger.info("read all imported images");

		return guiEditorService.getResourcesByProject(projectId, ResourceContentType.IMAGE);
	}	
	
	@RequestMapping(value = "{projectid}/getcsslist.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	List<GuiFileResourceDTO> getCssList(@PathVariable("projectid") Integer projectId) throws RestException {

		logger.info("read all imported css file ");

		return guiEditorService.getResourcesByProject(projectId, ResourceContentType.CSS);
	}		

	@RequestMapping(value = "{projectid}/getjslist.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	List<GuiFileResourceDTO> getJsList(@PathVariable("projectid") Integer projectId) throws RestException {

		logger.info("read all imported js file ");

		return guiEditorService.getResourcesByProject(projectId, ResourceContentType.JAVASCRIPT);
	}	
	
	@RequestMapping(value = "{projectid}/gethtmllist.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	List<GuiFileResourceDTO> getHtmlList(@PathVariable("projectid") Integer projectId) throws RestException {

		logger.info("read all imported not default html file ");

		return guiEditorService.getResourcesByProject(projectId, ResourceContentType.HTML);
	}
	
	// IVAN
	// quel che segue riguarda le estensioni del vme
	
	@RequestMapping(value = "{projectid}/getspainfo.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	SinglePageApplication getSpaInfo(@PathVariable("projectid") Integer projectId) throws RestException 
	{
		GuiProject p = guiEditorService.findProject(projectId);		

		List<GuiFileResourceDTO> list =  guiEditorService.getResourcesByProject(projectId, ResourceContentType.HTML);
		
		List<GuiFileResourceDTO> pages = new ArrayList<GuiFileResourceDTO>();
		
		for(GuiFileResourceDTO page: list)
		{
			if(!page.getIsdefault())
				pages.add(page);
		}
		
		SinglePageApplication spa = new SinglePageApplication();
		spa.setPages(pages);
		spa.setSpaName(p.getSpaName());
		spa.setSpaFirstPageId(p.getSpaFirstPageId());
		
		return spa;
	}
	
	@RequestMapping(value = "/{projectId}/configuration/save", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
    protected void saveSpaConfiguration(@RequestParam String appName, @RequestParam(defaultValue = "0") int firstPage, @PathVariable("projectId") Integer projectId) throws Exception 
    {
		GuiProject p = guiEditorService.findProject(projectId);
		
		p.setSpaName(appName);
		
		p.setSpaFirstPageId(firstPage);
		
		guiEditorService.updateProject(p, true);
    }
	
	@RequestMapping(value = "/{projectId}/spa/save", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
    protected void saveAsSpa(@RequestParam String htmlWrapper, @PathVariable("projectId") Integer projectId) throws Exception 
	{
		Document spa = null;
		
		GuiProject p = guiEditorService.findProject(projectId);
		String spaName = p.getSpaName();
		int spaFirstPageId = p.getSpaFirstPageId();
		
		if(spaName == null || spaName.compareTo("") == 0)
		{
			spaName = "main";
			p.setSpaName(spaName);
		}
		
		try
		{
			spa = Jsoup.parse(htmlWrapper);
		}
		catch(Exception e)
		{
			logger.error("Error Parsing HTML wrapper");
			return;
		}
		
		// get all HTML files associated to the project
		List<GuiFileResourceDTO> pages = guiEditorService.getResourcesByProject(projectId, ResourceContentType.get("text/html"));
		
		if(pages.size() > 0)
		{
			Element container = spa.select("div.container").first();
			
			int count = 0;
			String content, html, role;
			String filepath;
			File f = null;
			
			String path = PropertyGetter.getProperty("app.baseDir") + File.separator + VmeConstants.MAIN_DIRECTORY + File.separator + VmeConstants.GUIEDITOR_DIRECTORY + File.separator;
			path += VmeConstants.GUIPROJECT_REPOSITORY + File.separator + VmeConstants.GUIPROJECT_DIR_PREFIX + projectId + File.separator;
			
			for(GuiFileResourceDTO page: pages)
			{
				html = "";
				role = "";
				content = "";
				filepath = path + page.getFilename();
				
				// read file
				try
				{
					f = new File(filepath);
					
					content = Jsoup.parse(f, "UTF-8").toString();
				}
				catch(Exception e)
				{
					logger.error("Can't load file " + filepath);
					return;
				}
				
				// build Single Page Application
				if(page.getIsdefault())
				{							
					html = content;				
				}
				else
				{
					if(page.getId() == spaFirstPageId || ((spaFirstPageId == 0 && count == 1)))
						role = ",role: 'start'";						
					
					html = "<div data-bind=\"page: {id: '" + page.getFilename().replace(".html.working", "") + "'";
					html += ",title: '" + page.getFilename().replace(".html.working", "") + "'";
					html += role + "}\">" + content + "</div>";
				}
				
				container.append(html);
				
				count++;
			}
		}
		else
		{
			logger.error("No HTML files associated to the project" + projectId);
			return;
		}
        
		try
		{
			guiEditorService.saveFile(projectId, spaName + ".html", spa.toString(), ResourceContentType.get("text/html"));
			
			guiEditorService.updateProject(p, true);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logger.error("Can't save spa file");
		}
    }
	
	@RequestMapping(value = "{id}/getallmashups.json", method = RequestMethod.GET)
	public @ResponseBody String getAllMashups(@PathVariable("id") Integer id, HttpServletRequest request) throws RestException 
	{
		logger.debug("########## getallmashups.json");
		
		JSONObject responseNode = new JSONObject();
		
		// list of all mashups associated with mockup
		List<Project> mashupList = guiEditorService.findMashupProjects(guiEditorService.findProject(id));
		
		int mashupId;
		String mashupName = "";
		
		JSONObject json;
		try {
			for(int i = 0; i < mashupList.size(); i++)
			{
				mashupId = mashupList.get(i).getId();
				mashupName = mashupList.get(i).getName();
				
				json = new JSONObject();
				
				json.put("id", mashupId);				
				json.put("name", mashupName);
				
				responseNode.append("mashups", json);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return responseNode.toString();
	}
		
	@RequestMapping(value = "{id}/getallbindinginputparams.json", method = RequestMethod.POST)
	public @ResponseBody JsonNode getAllBindingInputParams(@PathVariable("id") Integer id, HttpServletRequest request) throws RestException {
		
		JsonNode responseNode = null;
		
		BindingInputParams bindingInputParams = new BindingInputParams();
		
		List<BindingInputField> inputFieldList = null;
		
		logger.debug("########## getallbindinginputparams.json");
		
		// list of all mashups associated with mockup
		List<Project> mashupList = guiEditorService.findMashupProjects(guiEditorService.findProject(id));
		
		try {
			inputFieldList = new ArrayList<BindingInputField>();
			
			int mashupId;
			String mashupName = "";
			
			for(int i = 0; i < mashupList.size(); i++)
			{
				mashupId = mashupList.get(i).getId();
				mashupName = mashupList.get(i).getName();
				
				List<InputShapeDTO> inputList = this.projectService.readInputShapesList(mashupId);
			
				for(InputShapeDTO inputShape : inputList)
				{
					BindingInputField inputField = new BindingInputField(inputShape.getId()/* + "_" + Integer.toString(mashupId)*/, inputShape.getUserData().getLabel(), mashupName);
					inputField.setDefaultValue(inputShape.getUserData().getDefaultValue());
					inputField.setIsConstant(inputShape.getUserData().isIsConstant());
					inputFieldList.add(inputField);
				}
			}
			
			bindingInputParams.setInputParams(inputFieldList);
			
			// IVAN
			//String actionUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()+"/gadget/"+id+"/emmlproxy2.json";
			String actionUrl = PropertyGetter.getProperty("app.publicUrl") + "/" + PropertyGetter.getProperty("app.url.name") + "/gadget/" + id + "/emmlproxy2.json";
			
			bindingInputParams.setActionUrl(actionUrl);
			
			JsonNode inputParamsNode = (ObjectNode) new ObjectMapper().convertValue(bindingInputParams, JsonNode.class);
			
			// Trasforma il json risultato in albero jstree
			//logger.debug("Convert json result to jstree...");
			//JsTreeNode jsTreeDest = FormatConverter.jsonToTreeStruct(inputParamsNode, VmeConstants.JSTREE_ROOTNODENAME);

			//responseNode = objectMapper.convertValue(jsTreeDest, JsonNode.class);
			
			responseNode = inputParamsNode;
			
		} catch (SelectShapeException e) {
			String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
//			} catch (JsonParseException e) {
//				
//				throw new RestException(e);
//			} catch (IOException e) {
//				
//				throw new RestException(e);
		}
		logger.debug("########## results: " + responseNode.toString());
		return responseNode;
	}
	
	@RequestMapping(value = "{id}/buildinputform", method = RequestMethod.GET)
	public ModelAndView buildInvokeInputForm(@PathVariable("id") Integer id, HttpServletRequest request) throws RestException {
		
		logger.debug("### buildInvokeInputForm");
		
		ModelAndView mav = getDefaultModelAndView("user/project/invoke_input_form");

		List<Project> mashupList = guiEditorService.findMashupProjects(guiEditorService.findProject(id));
		
		/*
		ShapeContainer shapeContainer = new ShapeContainer();
		try {
			shapeContainer.shapeSelector(project.getDashboard(), objectMapper);

		} catch (SelectShapeException e) {
			String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}*/
		
		List<InputShapeDTO> inputList = new ArrayList<InputShapeDTO>();
		List<InputShapeDTO> tmpList;
		InputShapeDTO tmp;
		
		int mashupId;
		
		try
		{
			for(int i = 0; i < mashupList.size(); i++)
			{
				mashupId = mashupList.get(i).getId();
				
				tmpList = this.projectService.readInputShapesList(mashupId);
				
				for(int j = 0; j < tmpList.size(); j++)
				{
					tmp = tmpList.get(j);
					//tmp.setId(tmp.getId() + "_" + Integer.toString(mashupId));
					inputList.add(tmp);
				}
			}
		}
		catch(SelectShapeException e)
		{
			e.printStackTrace();
		}
		
		//Json.prettyPrint(inputList);

		mav.addObject("projectId", id);
		mav.addObject("inputShape", inputList);

		return mav;
	}
	
	@RequestMapping(value = "{id}/invokeemml.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody JsonNode invokeEmml(ModelMap model, @PathVariable("id") Integer id, @RequestParam Map<String, String> formInputs, HttpServletRequest request) throws RestException {		
		JsonNode responseNode = null;
		//try {
		String result = null;
			try {				
				logger.debug("Invoking workflow...");
				String userConnector = RequestContextHolder.currentRequestAttributes().getSessionId();
				
				// get all the mashups associated with the mockup
				List<Project> mashupList = guiEditorService.findMashupProjects(guiEditorService.findProject(id));
				
				JSONObject jsonCombined = new JSONObject();
				JSONObject json;
				
				int mashupId, mid;
				String tmp[];
				String mashupName;
				
				int endIndex1, endIndex2, endIndex;
				
				for(int i = 0; i < mashupList.size(); i++)
				{
					mashupId = mashupList.get(i).getId();
					mashupName = mashupList.get(i).getName();
					
					Map<String, String> inputParameters = new HashMap<String, String>();
					
					Iterator<Entry<String, String>> it = formInputs.entrySet().iterator();
					
					while (it.hasNext()) 
					{
						Entry<String, String> pairs = it.next();
						
						logger.debug("[Key: " + pairs.getKey() + ", Value: " + pairs.getValue() + "]");
						
						if(pairs.getKey().compareTo("parentNodePath") == 0)
						{
							logger.debug("mashupName: " + mashupName);
							
							endIndex1 = pairs.getValue().indexOf('(');
							endIndex2 = pairs.getValue().indexOf('[');
							
							if(endIndex1 == -1)
							{
								endIndex = endIndex2 != -1 ? endIndex2 : pairs.getValue().length();
							}
							else if(endIndex2 == -1)
							{
								endIndex = endIndex1 != -1 ? endIndex1 : pairs.getValue().length();
							}
							else
							{
								endIndex = Math.min(endIndex1, endIndex2);
							}
							
							//if(mashupName.length() <= pairs.getValue().length() && pairs.getValue().substring(0, mashupName.length()).compareTo(mashupName) == 0)
							if(pairs.getValue().substring(0, endIndex).compareTo(mashupName) == 0)
								mid = mashupId;
							else
								continue;
						}
						else
						{						
							tmp = pairs.getKey().split("_");						
							mid = Integer.parseInt(tmp[tmp.length - 1]);
						}

						if(mashupId == mid)
						{	
							if(pairs.getValue().trim().compareTo("") != 0)
							{
								//if(pairs.getKey().compareTo("parentNodePath") != 0)
									//inputParameters.put(pairs.getKey().substring(0, pairs.getKey().lastIndexOf("_")), pairs.getValue());
								//else
									inputParameters.put(pairs.getKey(), pairs.getValue());
							}
									
						}
					}
					
					//if(!inputParameters.isEmpty())
					//{
					result = workflowService.invokeWorkflow(mashupId, inputParameters, userConnector);
				
					logger.debug(mashupList.get(i).getName() + " - invoke result: " + result);
					
					try 
					{
						json = new JSONObject(result);							
						jsonCombined.put(mashupList.get(i).getName(), json);
						
					} catch (JSONException e) 
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//}
				}
				
				logger.debug("Combined result: " + jsonCombined.toString());
				
				//result = workflowService.invokeWorkflow(id.intValue(), formInputs, userConnector);

				//logger.debug("invoke result" + result);

				JsonNode output = objectMapper.readTree(jsonCombined.toString());

				if(formInputs.containsKey("parentNodePath"))
				{
					String parentBindFullPathNode =  formInputs.get("parentNodePath");									
					
					try 
					{						
						output = JsonUtils.getSubtreeNodeMapKo(output, parentBindFullPathNode, objectMapper);
					} 
					catch (JsonProcessingException e) 
					{
						String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
						logger.error(error, e);

						throw new RestException(error, e, result);
					}													
				}
				
				
				//JsonNode chainInvokeResult = objectMapper.readTree(result).get(serviceId.replaceAll("[^A-Za-z0-9]", "_"));//.replace('.', '_').replace('-', '_'));

				// Mette in sessione il risultato json dell'invocazione
				model.addAttribute("lastinvokeresult", output);

				// Trasforma il json risultato in albero jstree
				logger.debug("Convert json result to jstree...");
				JsTreeNode jsTreeDest = FormatConverter.jsonToTreeStructKoMeta(output, null);

				responseNode = (ObjectNode) new ObjectMapper().convertValue(jsTreeDest, JsonNode.class);

			} catch (UserNotAuthorizedException e) {
				//String warning = messageSource.getMessage("message.error.usernotauthorized", null, Locale.ENGLISH);
				//logger.warn(warning);
				String serverUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
				//throw new RestException(HttpStatus.UNAUTHORIZED, serverUrl + "/oauth/" + e.getServiceId() + "/authorize");
				throw new RestException(HttpStatus.UNAUTHORIZED, serverUrl + "/oauth/" + e.getServiceId() + "/" + e.getOperationId() + "/authorize"); // IVAN: modified to allow auth at operation level
			
			} catch (InvokeWorkflowException e) {
				String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e, result);

			} catch (JsonProcessingException e) {
				String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e, result);

			} catch (IOException e) {
				String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e, result);
			}
		/*} catch (Exception e) {
			logger.error(e.getMessage(), e);

			mav = getDefaultModelAndView("redirect:/user/project/" + projectId + "/editor");
			mav.addObject(VmeConstants.MESSAGE_TEXT, e.getMessage());
		}*/

		return responseNode;
	}
	
	@RequestMapping(value = "{id}/publishScreenshot", method = RequestMethod.POST)	
	public @ResponseBody ObjectNode publishScreenshot(@PathVariable("id") Integer id, @RequestBody Screenshot data, HttpServletRequest request) 
	{
		IdeaWorkgroup iwg = ideawgService.getIdeaWorkgroupByWorkgroupId(guiEditorService.findProject(id).getWorkgroup().getId());
		
		return wsController.publishScreenshot(iwg.getIdeaId(), data.getDescription(), data.getEncodedImage(), data.getImageType(), getLoggedUser().getEmail());
	}
	
	@RequestMapping(value = "{id}/publish/{type}", method = RequestMethod.GET)
	public @ResponseBody ObjectNode publish(@PathVariable("id") Integer id, @PathVariable("type") String type, HttpServletRequest request) 
	{
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		try 
		{
			return wsController.publishMockupProject(id, type);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			node.put("error", true);
			node.put("message", e.getMessage());
		}
		
		return node;
	}
}