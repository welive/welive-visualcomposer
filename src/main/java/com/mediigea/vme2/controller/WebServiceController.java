package com.mediigea.vme2.controller;

import io.swagger.models.Swagger;
import io.swagger.parser.SwaggerParser;
import io.swagger.parser.util.SwaggerDeserializationResult;
import io.swagger.util.Json;
import it.eng.GenericUtils;
import it.eng.PropertyGetter;
import it.eng.metamodel.definitions.AuthenticationMeasure;
import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.metamodel.definitions.Dependency;
import it.eng.metamodel.definitions.Entity;
import it.eng.metamodel.definitions.Operation;
import it.eng.metamodel.definitions.Parameter;
import it.eng.metamodel.definitions.Permission;
import it.eng.metamodel.definitions.PublicServiceApplication;
import it.eng.model.AddVcProject;
import it.eng.model.MKTArtefacts;
import it.eng.model.NewScreenshot;
import it.eng.model.ProjectPublication;
import it.eng.model.MashupPublicationResponse;
import it.eng.model.RemoteArtefact;
import it.eng.model.ServiceValidation;
import it.eng.model.SwaggerParsingResponse;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.Principal;
import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.XmlException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.dao.AccountDAO;
import com.mediigea.vme2.dao.ProfileDAO;
import com.mediigea.vme2.dao.ProfileGuiProjectDAO;
import com.mediigea.vme2.dao.ProfileProjectDAO;
import com.mediigea.vme2.dao.ProjectInnerProcessDAO;
import com.mediigea.vme2.dao.WorkgroupDAO;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.dto.InputShapeDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.dto.UD_InputShapeDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.IdeaWorkgroup;
import com.mediigea.vme2.entity.ODSResource;
import com.mediigea.vme2.entity.Organization;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ProfileGuiProject;
import com.mediigea.vme2.entity.ProfileProject;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectDataset;
import com.mediigea.vme2.entity.ProjectInnerProcess;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.ServiceCatalogExt;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.parser.WadlParser;
import com.mediigea.vme2.rest.RestApplication;
import com.mediigea.vme2.service.AccountService;
import com.mediigea.vme2.service.GuiEditorService;
import com.mediigea.vme2.service.IdeaWorkgroupService;
import com.mediigea.vme2.service.ODSDatasetService;
import com.mediigea.vme2.service.OrganizationService;
import com.mediigea.vme2.service.ProfileService;
import com.mediigea.vme2.service.ProjectDatasetService;
import com.mediigea.vme2.service.ProjectOperationService;
import com.mediigea.vme2.service.ProjectService;
import com.mediigea.vme2.service.RestOperationService;
import com.mediigea.vme2.service.ServiceCatalogExtService;
import com.mediigea.vme2.service.ServiceCatalogService;
import com.mediigea.vme2.service.SoapOperationService;
import com.mediigea.vme2.service.WorkgroupService;
import com.mediigea.vme2.util.EmailSender;
import com.mediigea.vme2.util.SimpleCrypto;
import com.mediigea.vme2.util.VmeConstants;

@Controller
@RequestMapping(value = "/webservice")
public class WebServiceController extends BaseController
{
	@Autowired
	private AccountService accountService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private WorkgroupService workgroupService;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private ProjectController projectController;
	
	@Autowired
	private ProfileProjectDAO profileProjectDAO;
	
	@Autowired
	private GuiEditorService guiEditorService;
	
	@Autowired
	private GuiEditorController guiEditorController;
	
	@Autowired
	private ProfileGuiProjectDAO profileGuiProjectDAO;
	
	@Autowired
	private IdeaWorkgroupService ideawgService;
	
	@Autowired
	private ServiceCatalogService catalogService;
	
	@Autowired
	private ServiceCatalogExtService catalogExtService;
	
	@Autowired
	private ODSDatasetService odsDatasetService;
	
	@Autowired
	private ProjectOperationService projectOperationService;		
	
	@Autowired
	private ProjectDatasetService projectDatasetService;
	
	@Autowired
	private SoapOperationService soapOperationService;

	@Autowired
	private RestOperationService restOperationService;
	
	@Autowired
	private ProjectInnerProcessDAO projectInnerProcessDAO;	
	
	@Autowired
	private OrganizationService organizationService;
	
	@Autowired
	private AccountDAO accountDAO;

	@Autowired
	private ProfileDAO profileDAO;
	
	@Autowired
	private WorkgroupDAO workgroupDAO;
	
	@Autowired
	private EmailSender emailSender;

	@Autowired
	private MessageSource messageSource;
	
	private static final Logger logger = LoggerFactory.getLogger(WebServiceController.class);
	
	private static final int MESSAGE_TYPE_SUCCESS = 0;
	private static final int MESSAGE_TYPE_ERROR = 1;
	private static final int MESSAGE_TYPE_WARNING = 2;
	
	private static final int MESSAGE_REGISTRATION_SUCCEED = 0;
	private static final int MESSAGE_REGISTRATION_FAILED = 1;
	private static final int MESSAGE_USER_EXISTS = 2;
	private static final int MESSAGE_JSON_ERROR = 3;
	private static final int MESSAGE_INVALID_SECRET = 4;
	private static final int MESSAGE_DECRYPTION_ERROR = 5;
	private static final int MESSAGE_EMAIL_NOT_SENT = 6;
	private static final int MESSAGE_MISSING_FIELDS = 6;
	
	private static final int MESSAGE_CREATED = 0;
	private static final int MESSAGE_NO_MEMBERS = 1;
	private static final int MESSAGE_NO_USER = 2;
	// MESSAGE_JSON_ERROR = 3
	private static final int MESSAGE_IDEAWG_EXISTS = 4;
	private static final int MESSAGE_SERVER_ERROR = 5;
	private static final int MESSAGE_NO_AUTHORITY = 6;
	
	
	@RequestMapping(value = "/test", method=RequestMethod.GET)
	public @ResponseBody String testService(@RequestParam String strparam, @RequestParam int intparam)
	{
		publishMockupProject(intparam, "apk");
		
		return "OK";
	}
	
	
	// returns the list of users (ccUserID and username)
	@RequestMapping(value = "/administration/getuserslist/{role}", method=RequestMethod.GET)
	public @ResponseBody ArrayNode getUsersList(@PathVariable("role") String role)
	{
		ArrayNode list = JsonNodeFactory.instance.arrayNode();		
		
		List<Profile> profiles = null;
		
		if(role.compareTo("all") == 0)
			profiles = profileService.findAll();
		else if(role.compareTo("user") == 0)	
			profiles = profileService.findByRole(VmeConstants.ACCOUNT_ROLE_USER);
		else if(role.compareTo("admin") == 0)	
			profiles = profileService.findByRole(VmeConstants.ACCOUNT_ROLE_ADMIN);
		else
			return list;		
		
		ObjectNode node;
		
		for(int i = 0; i < profiles.size(); i++)
		{
			node = JsonNodeFactory.instance.objectNode();
			
			node.put("ccUserID", profiles.get(i).getCcUserId());
			node.put("username", profiles.get(i).getEmail());
			
			list.add(node);
		}
		
		return list;
	}
	
	// update list of users
	@RequestMapping(value = "/administration/updateuserlist", method=RequestMethod.POST)
	public @ResponseBody ArrayNode updateUsersList()
	{
		ArrayNode newList = null;
		
		RestTemplate restTemplate = new RestTemplate();
		
		// Add Basic Auth for WeLive components
	    HttpEntity<String> request = new HttpEntity<String>(getBasicAuthHeader());		
	    
	    ResponseEntity<String> response = null;
	    
	    try
	    {
		    response = restTemplate.exchange(PropertyGetter.getProperty("platform.getuserlist.url"), HttpMethod.GET, request, String.class);
		    
		    logger.debug(response.getBody());	
		    
		    ObjectNode json = (ObjectNode) new ObjectMapper().readTree(response.getBody());
		    
		    newList = (ArrayNode) json.get("users");
	    }
	    catch(Exception e)
	    {
	    	logger.error("Failed to retrieve user list: " + e.getMessage());
	    	
	    	return JsonNodeFactory.instance.arrayNode();
	    }		
	    
	    ArrayNode resultList = JsonNodeFactory.instance.arrayNode();	
		ObjectNode resultNode = null;
		
		JsonNode node;
		Profile p = null;
		Account a = null;
		
		int ccUserId = 0;	
		boolean isDeveloper;
		
		String firstName = "";
		String lastName = "";
		String email = "";
		String pilotId = "";
		String extNickname = "";
		String extImageUrl = "";
		String extRole = "";
		
		List<Workgroup> wgs = null;
		Workgroup pilotWg = null;
		boolean inWg;
		
		for(int i = 0; i < newList.size(); i++)
		{
			try
			{		
				resultNode = JsonNodeFactory.instance.objectNode();
				
				node = newList.get(i);
				
				firstName = "";
				lastName = "";
				email = "";
				pilotId = "";
				extNickname = "";
				extImageUrl = "";
				extRole = "";
				
				if(node.has("email"))
				{
					email = node.get("email").textValue();
					
					if(node.has("firstName") && node.has("lastName") && node.has("referredPilot") && node.has("ccUserID") && node.has("isDeveloper") && node.has("liferayScreenName"))
					{						
						firstName = node.get("firstName").textValue();
						lastName = node.get("lastName").textValue();
						pilotId = node.get("referredPilot").textValue();
						ccUserId = node.get("ccUserID").intValue();
						extNickname = node.get("liferayScreenName").textValue();
						isDeveloper = node.get("isDeveloper").booleanValue();
					}
					else
					{
						resultNode.put("user", email);
						resultNode.put("created", false);
						resultNode.put("updated", false);
						resultNode.put("error", true);
						resultNode.put("message", "Missing some required fields");
						
						resultList.add(resultNode);
						
						continue;
					}
				}
				else
				{
					resultNode.put("user", "unknown");
					resultNode.put("created", false);
					resultNode.put("updated", false);
					resultNode.put("error", true);
					resultNode.put("message", "Missing email");
					
					resultList.add(resultNode);
					
					continue;
				}
				
				if(node.has("avatar"))
				{
					extImageUrl = node.get("avatar").textValue();
				}
				
				if(node.has("role"))
				{
					extRole = node.get("role").textValue();
				}
				
				p = profileService.findProfileByEmail(email);
				
				resultNode.put("user", email);
				
				// get pilot workgroup
				pilotWg = workgroupService.findByName(pilotId);
				
				// if email exists update the user
				if(p != null)
				{
					a = p.getAccount();
					
					if(a.getRole() == VmeConstants.ACCOUNT_ROLE_ADMIN)
						continue;
					
					p.setFirstName(firstName);
					p.setLastName(lastName);
					p.setCcUserId(ccUserId);
					p.setPilotId(pilotId);
					p.setLevel(isDeveloper ? VmeConstants.USER_LEVEL_DEVELOPER_ADVANCED : VmeConstants.USER_LEVEL_MOCKUPDESIGNER);
					p.setExtNickname(extNickname);
					p.setExtImageUrl(extImageUrl);
					p.setExtRole(extRole);
					p.setUpdatedAt(new Date());
					
					profileService.updateProfile(p);
					
					// check if the user is already member of its pilot workgroup
					
					wgs = workgroupService.findByProfile(p);					
					
					inWg = false;
					
					for(int w = 0; w < wgs.size(); w++)
					{
						if(wgs.get(w).getId() == pilotWg.getId())
						{
							inWg = true;
							
							break;
						}
					}
					
					if(pilotWg != null && !inWg)
						workgroupService.memberAdd(pilotWg.getId(), p.getId()); // add user to its pilot workgroup
					
					// if user is not enabled, enable it
					if(a.getStatus() == VmeConstants.ACCOUNT_STATUS_PENDING)
					{
						a.setStatus(VmeConstants.ACCOUNT_STATUS_ENABLED);
						accountService.updateAccount(a);
					}
					
					resultNode.put("created", false);
					resultNode.put("updated", true);
					resultNode.put("error", false);
					resultNode.put("message", "User updated");
				}
				else // else create a new user
				{
					p = new Profile();
					
					p.setFirstName(firstName);
					p.setLastName(lastName);
					p.setCreatedAt(new Date());
					p.setUpdatedAt(new Date());
					p.setEmail(email);
					p.setCcUserId(ccUserId);
					p.setPilotId(pilotId);
					p.setLevel(isDeveloper ? VmeConstants.USER_LEVEL_DEVELOPER_ADVANCED : VmeConstants.USER_LEVEL_MOCKUPDESIGNER);
					p.setExtNickname(extNickname);
					p.setExtImageUrl(extImageUrl);
					p.setExtRole(extRole);
					
					p = profileService.addProfile(p);
					
					// add user to its pilot workgroup
					if(pilotWg != null)
						workgroupService.memberAdd(pilotWg.getId(), p.getId());
					
					a = new Account();

					a.setPsw(SimpleCrypto.md5("temporarypassword"));
					a.setEmail(email);
					a.setRole(VmeConstants.ACCOUNT_ROLE_USER);
					a.setStatus(VmeConstants.ACCOUNT_STATUS_ENABLED);
					a.setCreatedAt(new Date());
					a.setUpdatedAt(new Date());
					a.setProvider("vme");
					a.setProfile(p);
					
					accountService.addAccount(a);
					
					p.setAccount(a);
					profileService.updateProfile(p);
					
					resultNode.put("created", true);
					resultNode.put("updated", false);
					resultNode.put("error", false);
					resultNode.put("message", "User created");
				}
			}
			catch(Exception e)
			{
				logger.error(e.getMessage());
				
				resultNode.put("created", false);
				resultNode.put("updated", false);
				resultNode.put("error", true);
				resultNode.put("message", e.getMessage());
			}
			
			resultList.add(resultNode);
		}		
		
		return resultList;
	}
	
	// --------------------------------------------------------------------------------------------------------------------------
	
	/* 	JSON INPUT
	   	{
	   		"ccUserID": <integer>,
	   		"referredPilot": <string (Bilbao, Novisad, Uusimaa, Trento)>,
			"firstName": <string>,
			"lastName":<string>,
			"email": <string>,
			"liferayScreenName": <string>,
			"avatar": <string>,
			"role": <string>,
			"isDeveloper": <boolean>
		}
	*/
	
	// create a new user (propagated by Liferay User Manager)
	@RequestMapping(value = "/registeruser", method=RequestMethod.POST)
	//public @ResponseBody String registerUser(@RequestBody byte[] cipher_text, HttpServletRequest request)
	public @ResponseBody String registerUser(@RequestBody ObjectNode newUser, HttpServletRequest request)
	{
		//String plain_text = new String(cipher_text);
		
		Account a = new Account();
		
		Profile p = new Profile();
		
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		/*
		JSONObject response = null;
		
		try
		{
			response = new JSONObject(plain_text);
		}
		catch(JSONException e)
		{
			logger.error("Error parsing request into JSON");
			e.printStackTrace();
			node.put("message", MESSAGE_JSON_ERROR);
			node.put("type", MESSAGE_TYPE_ERROR);
			return node.toString();
		}*/
		
		String firstName = "";
		String lastName = "";
		String email = "";
		String pilotId = "vc-all";
		String extNickname = "";
		String extImageUrl = "";
		String extRole = "";
		
		int ccUserId = 0;
		boolean isDeveloper = false;
		
		try
		{
			if(newUser.has("firstName") && newUser.has("lastName") && newUser.has("email"))
			{
				firstName = newUser.get("firstName").textValue();
				lastName = newUser.get("lastName").textValue();
				email = newUser.get("email").textValue();
			}
			else
			{
				node.put("message", MESSAGE_MISSING_FIELDS);
				node.put("type", MESSAGE_TYPE_ERROR);
				return node.toString();
			}
			
			if(newUser.has("referredPilot")) pilotId = newUser.get("referredPilot").textValue();
			if(newUser.has("ccUserID")) ccUserId = newUser.get("ccUserID").asInt();
			if(newUser.has("liferayScreenName")) extNickname = newUser.get("liferayScreenName").textValue();
			if(newUser.has("avatar")) extImageUrl = newUser.get("avatar").textValue();
			if(newUser.has("role")) extRole = newUser.get("role").textValue();
			if(newUser.has("isDeveloper")) isDeveloper = newUser.get("isDeveloper").asBoolean();
		}
		catch(Exception e)
		{
			logger.error("Error parsing request into JSON");
			e.printStackTrace();
			node.put("message", MESSAGE_JSON_ERROR);
			node.put("type", MESSAGE_TYPE_ERROR);
			return node.toString();
		}
		
		try 
		{
			if(newUser.has("ccUserID"))
			{
				// check if ccUserId already
				Profile tmp = profileService.findProfileByCcUserId(ccUserId);
				
				if(tmp != null)
				{
					node.put("message", MESSAGE_USER_EXISTS);
					node.put("type", MESSAGE_TYPE_ERROR);
					return node.toString();
				}
			}
			
			// check if email already exists
			Account a1 = accountDAO.getByUserName(email);

			if (a1 != null) 
			{
				node.put("message", MESSAGE_USER_EXISTS);
				node.put("type", MESSAGE_TYPE_ERROR);
				return node.toString();
			}
			
			p.setFirstName(firstName);
			p.setLastName(lastName);
			p.setCreatedAt(new Date());
			p.setUpdatedAt(new Date());
			p.setEmail(email);
			p.setCcUserId(ccUserId);
			p.setPilotId(pilotId);
			p.setLevel(isDeveloper ? VmeConstants.USER_LEVEL_DEVELOPER_ADVANCED : VmeConstants.USER_LEVEL_MOCKUPDESIGNER);
			p.setExtNickname(extNickname);
			p.setExtImageUrl(extImageUrl);
			p.setExtRole(extRole);
			
			profileService.addProfile(p);
			
			// add user to its pilot workgroup
			Workgroup wg = workgroupService.findByName(pilotId);
			
			if(wg != null)
				workgroupService.memberAdd(wg.getId(), p.getId());

			a.setPsw(SimpleCrypto.md5("testuser"));
			a.setEmail(email);
			a.setRole(VmeConstants.ACCOUNT_ROLE_USER);
			a.setStatus(VmeConstants.ACCOUNT_STATUS_ENABLED);
			a.setCreatedAt(new Date());
			a.setUpdatedAt(new Date());
			a.setProvider("vme");
			a.setProfile(p);
			
			accountService.addAccount(a);
			
			p.setAccount(a);
			profileService.updateProfile(p);	
			
			logger.debug("Registration success");
		} 
		catch (Exception e) 
		{
			logger.error("RegistrationException: " + e.getMessage());
			
			node.put("message", MESSAGE_REGISTRATION_FAILED);
			node.put("type", MESSAGE_TYPE_ERROR);
			return node.toString();
		}
		
		node.put("message", MESSAGE_REGISTRATION_SUCCEED);
		node.put("type", MESSAGE_TYPE_SUCCESS);
		
		return node.toString();
	}
	
	/*
	 INPUT 
	 	{
		  "ccOrganizationId": <integer>*,
		  "name": <string>,
		  "info": <string>,
		  "leaderId": <integer (ccUserId)>
		}
	 */
	
	// create/update organization
	@RequestMapping(value = "/upsert-organization", method=RequestMethod.POST)
	public @ResponseBody ObjectNode upsertOrganization(@RequestBody ObjectNode organization)
	{
		ObjectNode responseBody = JsonNodeFactory.instance.objectNode();
		
		if(!organization.has("ccOrganizationId"))
		{
			responseBody.put("error", true);
			responseBody.put("status", 400);
			responseBody.put("message", "missing ccOrganizationId");
			
			return responseBody;
		}
		
		int ccOrganizationId = organization.get("ccOrganizationId").asInt();
		String name = "";
		String info = "";
		int ownerId = -1;
		
		Profile owner = null;
		
		if(organization.has("name"))
			name = organization.get("name").asText();
		
		if(organization.has("info"))
			info = organization.get("info").asText();
		
		// for updating organization owner
		if(organization.has("leaderId"))
		{
			ownerId =  organization.get("leaderId").asInt();
			
			owner = profileService.findProfileByCcUserId(ownerId);
			
			if(owner == null)
			{
				responseBody.put("error", true);
				responseBody.put("status", 404);
				responseBody.put("message", "leaderId not found");
				
				return responseBody;
			}
		}
		
		Organization org = organizationService.findOrganizationByExtId(ccOrganizationId);
		
		try
		{
			if(org != null)
			{
				if(name.compareTo("") != 0)
					org.setName(name);
				
				if(info.compareTo("") != 0)
					org.setInfo(info);
				
				// change organization owner
				if(owner != null)
				{
					Profile currOwner = org.getOwner();
					
					if(currOwner != null)
					{
						currOwner.setOrganizationRole(VmeConstants.PROFILE_ORGANIZATION_NONE);
						currOwner.setOrganization(null);
						
						profileService.updateProfile(currOwner);
					}
					
					owner.setOrganizationRole(VmeConstants.PROFILE_ORGANIZATION_ROLE_OWNER);
					owner.setOrganization(org);
					
					profileService.updateProfile(owner);
					
					org.setOwner(owner);
				}
				
				organizationService.updateOrganization(org);
				
				responseBody.put("error", false);
				responseBody.put("status", 200);
				responseBody.put("message", "organization updated");
			}
			else
			{
				if(name.compareTo("") == 0)
				{
					responseBody.put("error", true);
					responseBody.put("status", 400);
					responseBody.put("message", "missing name");
					
					return responseBody;
				}
				
				org = new Organization();
				org.setExtId(ccOrganizationId);
				org.setName(name);
				org.setInfo(info);
				org.setOwner(owner);
				
				organizationService.addOrganization(org);
				
				// update owner organization role
				if(owner != null)
				{
					owner.setOrganizationRole(VmeConstants.PROFILE_ORGANIZATION_ROLE_OWNER);
					owner.setOrganization(org);
					
					profileService.updateProfile(owner);
				}
				
				responseBody.put("error", false);
				responseBody.put("status", 201);
				responseBody.put("message", "organization created");
			}
		}
		catch(Exception e)
		{
			responseBody.put("error", true);
			responseBody.put("status", 500);
			responseBody.put("message", "internal server error");
		}
		
		return responseBody;
	}
	
	// delete organization by ccOrganizationId
	@RequestMapping(value = "/delete-organization/{ccOrganizationId}", method=RequestMethod.DELETE)
	public @ResponseBody ObjectNode deleteOrganization(@PathVariable("ccOrganizationId") Integer ccOrganizationId)
	{
		ObjectNode responseBody = JsonNodeFactory.instance.objectNode();
		
		if(ccOrganizationId == null)
		{
			responseBody.put("error", true);
			responseBody.put("status", 400);
			responseBody.put("message", "ccOrganizationId");
			
			return responseBody;
		}
		
		Organization org = organizationService.findOrganizationByExtId(ccOrganizationId);
		
		if(org == null)
		{
			responseBody.put("error", true);
			responseBody.put("status", 404);
			responseBody.put("message", "ccOrganizationId " + ccOrganizationId + " not found");
			
			return responseBody;
		}
		
		List<Profile> members = organizationService.findOrganizationMembersByExtId(ccOrganizationId);
		Profile member = null;
		
		try
		{
			for(int i = 0; i < members.size(); i++)
			{
				member = members.get(i);
				
				member.setOrganization(null);
				member.setOrganizationRole(VmeConstants.PROFILE_ORGANIZATION_NONE);
				
				profileService.updateProfile(member);
			}
			
			Profile owner = org.getOwner();
			
			if(owner != null)
			{
				owner.setOrganization(null);
				owner.setOrganizationRole(VmeConstants.PROFILE_ORGANIZATION_NONE);
				
				profileService.updateProfile(owner);
			}
			
			organizationService.deleteOrganization(org.getId());
		}
		catch(Exception e)
		{
			responseBody.put("error", true);
			responseBody.put("status", 500);
			responseBody.put("message", "internal server error");
		}
		
		responseBody.put("error", false);
		responseBody.put("status", 200);
		responseBody.put("message", "organization deleted");
		
		return responseBody;
	}
	
	/*
	 * 	INPUT
	 * 
	 	{
		  "ccOrganizationId": <integer>,
		  "members": [ <integer (ccUserId)> ]
		}
	 */
	
	@RequestMapping(value = "/add-organization-members", method=RequestMethod.POST)
	public @ResponseBody ObjectNode addOrganizationMembers(@RequestBody ObjectNode requestBody)
	{
		ObjectNode responseBody = JsonNodeFactory.instance.objectNode();
		
		if(!(requestBody.has("ccOrganizationId") && requestBody.has("members")))
		{
			responseBody.put("error", true);
			responseBody.put("status", 400);
			responseBody.put("message", "missing required fields");
			
			return responseBody;
		}
		
		int ccOrganizationId = requestBody.get("ccOrganizationId").asInt();
		
		Organization org = organizationService.findOrganizationByExtId(ccOrganizationId);
		
		if(org == null)
		{
			responseBody.put("error", true);
			responseBody.put("status", 404);
			responseBody.put("message", "ccOrganizationId " + ccOrganizationId + " not found");
			
			return responseBody;
		}
		
		ArrayNode members = (ArrayNode) requestBody.get("members");
		
		int memberId;
		
		Profile member = null;
		
		for(int i = 0; i < members.size(); i++)
		{
			memberId = members.get(i).asInt();
			
			member = profileService.findProfileByCcUserId(memberId);
			
			if(member == null)
			{
				responseBody.put("error", true);
				responseBody.put("status", 404);
				responseBody.put("message", "memberId " + memberId + " not found");
				
				return responseBody;
			}
			
			try
			{
				member.setOrganization(org);
				member.setOrganizationRole(VmeConstants.PROFILE_ORGANIZATION_ROLE_MEMBER);
				
				profileService.updateProfile(member);
			}
			catch(Exception e)
			{
				responseBody.put("error", true);
				responseBody.put("status", 500);
				responseBody.put("message", "internal server error");
			}
		}
		
		responseBody.put("error", false);
		responseBody.put("status", 200);
		responseBody.put("message", "members added to organization");
		
		return responseBody;
	}
	
	/*
	 * 	INPUT
	 * 
	 	{
		  "ccOrganizationId": <integer>,
		  "members": [ <integer (ccUserId)> ]
		}
	 */
	
	@RequestMapping(value = "/remove-organization-members", method=RequestMethod.POST)
	public @ResponseBody ObjectNode removeOrganizationMembers(@RequestBody ObjectNode requestBody)
	{
		ObjectNode responseBody = JsonNodeFactory.instance.objectNode();
		
		if(!(requestBody.has("ccOrganizationId") && requestBody.has("members")))
		{
			responseBody.put("error", true);
			responseBody.put("status", 400);
			responseBody.put("message", "missing required fields");
			
			return responseBody;
		}
		
		int ccOrganizationId = requestBody.get("ccOrganizationId").asInt();
		
		Organization org = organizationService.findOrganizationByExtId(ccOrganizationId);
		
		if(org == null)
		{
			responseBody.put("error", true);
			responseBody.put("status", 404);
			responseBody.put("message", "ccOrganizationId " + ccOrganizationId + " not found");
			
			return responseBody;
		}
		
		ArrayNode members = (ArrayNode) requestBody.get("members");
		
		int memberId;
		
		Profile member = null;
		
		for(int i = 0; i < members.size(); i++)
		{
			memberId = members.get(i).asInt();
			
			member = profileService.findProfileByCcUserId(memberId);
			
			if(member == null)
			{
				responseBody.put("error", true);
				responseBody.put("status", 404);
				responseBody.put("message", "memberId " + memberId + " not found");
				
				return responseBody;
			}
			
			try
			{
				member.setOrganization(null);
				member.setOrganizationRole(VmeConstants.PROFILE_ORGANIZATION_NONE);
				
				profileService.updateProfile(member);
			}
			catch(Exception e)
			{
				responseBody.put("error", true);
				responseBody.put("status", 500);
				responseBody.put("message", "internal server error");
			}
		}
		
		responseBody.put("error", false);
		responseBody.put("status", 200);
		responseBody.put("message", "members removed from organization");
		
		return responseBody;
	}
	
	
	/*
	 * 	OUTPUT
	 * 
	 	{
		  "error": true,
		  "message": "",
		  "reason": "",
		  "status": 500
		}
	 */
	
	// delete user by ccUserID
	@RequestMapping(value = "/deleteuser/{ccUserID}", method=RequestMethod.DELETE)
	public @ResponseBody ObjectNode deleteUser(@PathVariable("ccUserID") Integer ccUserID, @RequestParam("cascade") Boolean cascade)
	{
		ObjectNode responseBody = JsonNodeFactory.instance.objectNode();
		
		Profile userToDelete = profileService.findProfileByCcUserId(ccUserID);
		
		// user not found
		if(userToDelete == null)
		{
			responseBody.put("error", true);
			responseBody.put("message", "CCUSERID_NOT_FOUND");
			responseBody.put("reason", "Action failed because the ccUserID does not exist into the DB");
			responseBody.put("status", 404);
			
			return responseBody;
		}
		
		Account userToDeleteAccount = userToDelete.getAccount();
		
		// user that manages projects of deleted users
		Profile deleteAdmin = profileService.findProfileByEmail(PropertyGetter.getProperty("admin.deleted.users.email"));
		
		// search mockups that involve user (as owner or member)
		
		List<GuiProject> mockups = guiEditorService.findAllByProfile(userToDelete);
		GuiProject mockup = null;
		
		for(int i = 0; i < mockups.size(); i++)
		{
			mockup = mockups.get(i);
			
			try
			{
				_deleteMockup(userToDelete, mockup, deleteAdmin, cascade);
			}
			catch(Exception e)
			{
				responseBody.put("error", true);
				responseBody.put("message", "RESOURCE_ERROR");
				responseBody.put("reason", "Action failed because some resource associated to the user can not be deleted");
				responseBody.put("status", 424);
				
				// change userToDelete account status to TO_BE_DELETED
				userToDeleteAccount.setStatus(VmeConstants.ACCOUNT_TO_BE_DELETED);				
				accountService.updateAccount(userToDeleteAccount);
				
				return responseBody;
			}
		}
		
		// search mashups that involve user (as owner or member)
		
		List<Project> mashups = projectService.findAllByProfile(userToDelete);
		Project mashup = null;
		
		for(int i = 0; i < mashups.size(); i++)
		{
			mashup = mashups.get(i);
			
			try
			{
				_deleteMashup(userToDelete, mashup, deleteAdmin, cascade);
			}
			catch(Exception e)
			{
				responseBody.put("error", true);
				responseBody.put("message", "RESOURCE_ERROR");
				responseBody.put("reason", "Action failed because some resource associated to the user can not be deleted");
				responseBody.put("status", 424);
				
				// change userToDelete account status to TO_BE_DELETED
				userToDeleteAccount.setStatus(VmeConstants.ACCOUNT_TO_BE_DELETED);				
				accountService.updateAccount(userToDeleteAccount);
				
				return responseBody;
			}
		}
		
		// remove user from workgroups
		
		List<Workgroup> workgroups = workgroupService.findByProfile(userToDelete);
		
		for(int i = 0; i < workgroups.size(); i++)
		{
			try
			{
				workgroupService.memberRemove(workgroups.get(i).getId(), userToDelete.getId());
			}
			catch(Exception e)
			{
				responseBody.put("error", true);
				responseBody.put("message", "ACTION_FAILED");
				responseBody.put("reason", "Action failed, no more details are provided");
				responseBody.put("status", 500);
				
				// change userToDelete account status to TO_BE_DELETED
				userToDeleteAccount.setStatus(VmeConstants.ACCOUNT_TO_BE_DELETED);				
				accountService.updateAccount(userToDeleteAccount);
				
				return responseBody;
			}			
		}
		
		// delete user
		
		try
		{
			profileService.deleteProfile(userToDelete.getId());
		}
		catch(Exception e)
		{
			responseBody.put("error", true);
			responseBody.put("message", "ACTION_FAILED");
			responseBody.put("reason", "Action failed, no more details are provided");
			responseBody.put("status", 500);
			
			// change userToDelete account status to TO_BE_DELETED
			userToDeleteAccount.setStatus(VmeConstants.ACCOUNT_TO_BE_DELETED);				
			accountService.updateAccount(userToDeleteAccount);
			
			return responseBody;
		}
		
		responseBody.put("error", false);
		responseBody.put("message", "ACTION_SUCCESS");
		responseBody.put("reason", "Action completed correctly");
		responseBody.put("status", 200);
		
		return responseBody;
	}
	
	private void _deleteMockup(Profile userToDelete, GuiProject project, Profile deleteAdmin, Boolean deleteProject) throws Exception
	{
		Profile owner = guiEditorService.findProjectOwner(project.getId());
		
		Profile newProjectOwner = null;
		
		boolean delete = false;
		
		// userToDelete is the owner of the project
		if(owner.getId() == userToDelete.getId())
		{
			List<Profile> members = guiEditorService.findProjectMembers(project);
			
			// if project has other members
			if(members.size() > 1)
			{
				// check if members contain the userToDelete organization owner
				if(userToDelete.getOrganization() != null && userToDelete.getOrganization().getOwner().getId() != userToDelete.getId())
				{
					for(int i = 0; i < members.size(); i++)
					{
						// assign project ownership to userToDelete organization owner
						if(members.get(i).getId() == userToDelete.getOrganization().getOwner().getId())
						{
							newProjectOwner = members.get(i);
							break;
						}
					}
				}

				if(newProjectOwner == null)
				{
					for(int i = 0; i < members.size(); i++)
					{
						// assign project ownership to first member
						if(members.get(i).getId() != userToDelete.getId())
						{
							newProjectOwner = members.get(i);
							break;
						}
					}
				}
			}
			// if project has no other members
			else
			{
				Organization org = userToDelete.getOrganization();
				
				// if userToDelete has organization and it is not the owner of the organization, assign project ownership to the organization owner
				if(org != null && userToDelete.getId() != org.getOwner().getId())
				{
					newProjectOwner = org.getOwner();					
				}
				else
				{
					// if userToDelete asked to delete its projects, delete the project
					if(deleteProject)
					{
						delete = true;
					}
					// otherwise, assign project ownership to deleteAdmin
					else
					{						
						newProjectOwner = deleteAdmin;
					}
				}
			}
		}
		
		// if project ownership has been reassigned
		if(newProjectOwner != null)
		{
			System.out.println("new project owner: " + newProjectOwner.getEmail());
			
			Iterator<ProfileGuiProject> profileProject = guiEditorService.findProfileGuiProject(project).iterator();
			ProfileGuiProject pp = null;
			
			// check if newProjectOwner is already a member
			while(profileProject.hasNext())
			{
				pp = profileProject.next();
				
				// update project ownership
				if(pp.getProfile().getId() == newProjectOwner.getId())
				{					
					guiEditorService.memberRemove(project.getId(), newProjectOwner.getId());
					
					break;
				}
			}
			
			guiEditorService.memberAddWithRole(project.getId(), newProjectOwner.getId(), VmeConstants.PROFILE_PROJECT_ROLE_OWNER);
		}

		// remove userToDelete from project members
		guiEditorService.memberRemove(project.getId(), userToDelete.getId());
		
		if(delete)
		{
			// other members will be removed in deleteProject
			
			guiEditorService.deleteProject(project.getId());
		}		
	}
	
	private void _deleteMashup(Profile userToDelete, Project project, Profile deleteAdmin, Boolean deleteProject) throws Exception
	{
		Profile owner = projectService.findProjectOwner(project.getId());
		
		Profile newProjectOwner = null;
		
		boolean delete = false;
		
		ObjectNode mashupDependencies = null;
		ArrayNode innerProcesses = null;
		ArrayNode mockupDatasources = null;
		
		mashupDependencies = projectController.checkDelete(project.getId());
		innerProcesses = (ArrayNode) mashupDependencies.get("innerProcesses");
		mockupDatasources = (ArrayNode) mashupDependencies.get("mockups");
		
		// userToDelete is the owner of the project
		if(owner.getId() == userToDelete.getId())
		{
			List<Profile> members = projectService.findProjectMembers(project);
			
			// if project has other members
			if(members.size() > 1)
			{
				// check if members contain the userToDelete organization owner
				if(userToDelete.getOrganization() != null && userToDelete.getOrganization().getOwner().getId() != userToDelete.getId())
				{
					for(int i = 0; i < members.size(); i++)
					{
						// assign project ownership to userToDelete organization owner
						if(members.get(i).getId() == userToDelete.getOrganization().getOwner().getId())
						{
							newProjectOwner = members.get(i);
							break;
						}
					}
				}

				if(newProjectOwner == null)
				{
					for(int i = 0; i < members.size(); i++)
					{
						// assign project ownership to first member
						if(members.get(i).getId() != userToDelete.getId())
						{
							newProjectOwner = members.get(i);
							break;
						}
					}
				}
			}
			// if project has no other members
			else
			{
				// if some other resource relies on this project, assign it to deleteAdmin
				if(innerProcesses.size() > 0 || mockupDatasources.size() > 0)
				{
					Organization org = userToDelete.getOrganization();
					
					// if userToDelete has organization and it is not the owner of the organization, assign project ownership to the organization owner
					if(org != null && userToDelete.getId() != org.getOwner().getId())
					{
						newProjectOwner = org.getOwner();					
					}
					// otherwise, assign project ownership to deleteAdmin
					else
					{
						newProjectOwner = deleteAdmin;
					}
				}
				else
				{
					Organization org = userToDelete.getOrganization();
					
					// if userToDelete has organization and it is not the owner of the organization, assign project ownership to the organization owner
					if(org != null && userToDelete.getId() != org.getOwner().getId())
					{
						newProjectOwner = org.getOwner();					
					}
					else
					{
						// if userToDelete asked to delete its projects, delete the project
						if(deleteProject)
						{
							delete = true;
						}
						// otherwise, assign project ownership to deleteAdmin
						else
						{						
							newProjectOwner = deleteAdmin;
						}
					}
				}
			}
		}
		
		// if project ownership has been reassigned
		if(newProjectOwner != null)
		{
			System.out.println("new project owner: " + newProjectOwner.getEmail());
			
			Iterator<ProfileProject> profileProject = projectService.findProfileProject(project).iterator();
			ProfileProject pp = null;
			
			// check if newProjectOwner is already a member
			while(profileProject.hasNext())
			{
				pp = profileProject.next();
				
				// update project ownership
				if(pp.getProfile().getId() == newProjectOwner.getId())
				{
					projectService.memberRemove(project.getId(), newProjectOwner.getId());
					projectService.memberAddWithRole(project.getId(), newProjectOwner.getId(), VmeConstants.PROFILE_PROJECT_ROLE_OWNER);
					
					break;
				}
			}
			
			projectService.memberAddWithRole(project.getId(), newProjectOwner.getId(), VmeConstants.PROFILE_PROJECT_ROLE_OWNER);
		}

		// remove userToDelete from project members
		projectService.memberRemove(project.getId(), userToDelete.getId());
		
		if(delete)
		{
			// other members will be removed in deleteProject
			
			projectService.deleteProject(project.getId());
		}		
	}
	
	
	// --------------------------------------------------------------------------------------------------------------------------
	
	/* JSON INPUT
	   {
		"ideaid": <integer>,
		"ideaname": <string>,
		"authority": <string (email)>,
		"authorityOrCompanyOrganizationId": <integer>,
		"authorityOrCompanyOrganizationName": <string>,
		"isTakenUp": <boolean>,
		"wgname": <string>,
		"members":
			[
				{ "username": <string (email)> },
				...
			]
		}
	*/
	
	// create a new workgroup associated with an idea (from OIA)
	@RequestMapping(value = "/ideaworkgroup", method=RequestMethod.POST)
	public @ResponseBody String ideaWorkgroup(@RequestBody String json, HttpServletRequest request, Principal principal)
	{
		String ideaInfo = json;
		
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		JSONObject message = null;
		JSONArray members = null;
		
		int ideaId;
		//int authorityOrCompanyOrganizationId;
		//String authorityOrCompanyOrganizationName = "";
		//boolean isTakenUp;
		String ideaName = "";
		String cleanIdeaName = "";
		String wgName = "";
		String authority = "";
		
		Workgroup wg = null;
		IdeaWorkgroup ideawg = null;
		Profile owner = null;
		Account a;
		
		List<Account> memberAccount = new ArrayList<Account>();
		
		Project mashup = null;
		GuiProject mockup = null;
		
		boolean warning = false;
		boolean update = false;
		
		String username = "";
		
		try 
		{
			message = new JSONObject(ideaInfo);
			
			ideaName = message.getString("ideaname").trim();
			cleanIdeaName = ideaName.replaceAll("[^A-Za-z0-9]", "");
			
			ideaId = message.getInt("ideaid");
			wgName = "WG_" + cleanIdeaName;
			authority = message.getString("authority");
			members = message.getJSONArray("members");
			
			//authorityOrCompanyOrganizationId = message.getInt("authorityOrCompanyOrganizationId");
			//authorityOrCompanyOrganizationName = message.getString("authorityOrCompanyOrganizationName");
			//isTakenUp = message.getBoolean("isTakenUp");
			
			ideawg = ideawgService.getIdeaWorkgroupByIdeaId(ideaId);
			
			// Check if idea-wg already exists
			// if true...
			if(ideawg != null)
			{
				wg = ideawg.getWg();
				
				// current workgroup members
				List<Profile> current_members = profileService.findWorkgroupMembers(wg.getId());
				
				if(members.length() != 0)
				{
					// ...remove all members but the authority
					for(int i = 0; i < current_members.size(); i++)
					{
						if(current_members.get(i).getEmail().compareTo(authority) == 0)
							continue;
						
						workgroupService.memberRemove(wg.getId(), current_members.get(i).getId());
					}
				}
				
				update = true;
			}				
			
			// Check if authority exists
			if(authority.compareTo("") == 0)
			{
				logger.error("No authority associated");
				node.put("message", MESSAGE_NO_AUTHORITY);
				node.put("type", MESSAGE_TYPE_ERROR);
				
				return node.toString();
			}
			else
			{
				a = accountDAO.getByUserName(authority);
				
				if(a == null)
				{
					logger.error("User '" + authority + "' doesn't exist");
					node.put("message", MESSAGE_NO_USER);
					node.put("type", MESSAGE_TYPE_ERROR);
					
					return node.toString();
				}
				
				if(!update)
					memberAccount.add(a);
				
				// Set authority as project owner
				owner = a.getProfile();
			}
			
			if(members.length() == 0)
			{
				logger.error("No members in the request");
				node.put("message", MESSAGE_NO_MEMBERS);
				node.put("type", MESSAGE_TYPE_WARNING);
				
				warning = true;
			}
			
			if(!update)
			{
				// Create Workgroup
				wg = new Workgroup();
				wg.setName(wgName);
				wg.setDescription("");
				
				wg = workgroupService.addWorkgroup(wg);
			}
			
			// Check if members exist...
			for(int i = 0; i < members.length(); i++)
			{
				username = members.getJSONObject(i).getString("username");
				a = accountDAO.getByUserName(username);
				
				if(a == null)
				{
					logger.error("User '" + username + "' doesn't exist");
					node.put("message", MESSAGE_NO_USER);
					node.put("type", MESSAGE_TYPE_ERROR);
					
					if(!update)
						workgroupService.deleteWorkgroup(wg.getId());
					
					return node.toString();
				}	
				
				memberAccount.add(a);				
			}
			// ... and add them to the workgroup
			for(int i = 0; i < memberAccount.size(); i++)
			{
				workgroupService.memberAdd(wg.getId(), memberAccount.get(i).getProfile().getId());
			}
			
			if(!update)
			{
				ideawg = new IdeaWorkgroup();
				ideawg.setIdeaId(ideaId);
				ideawg.setIdeaName(ideaName);
				ideawg.setWg(wg);
				
				ideawg = ideawgService.addIdeaWorkgroup(ideawg);			
			
				// Create Mashup Project
				mashup = new Project();
				
				//mashup.setName(ideaName + "_mashup");
				mashup.setName(cleanIdeaName);
				mashup.setDescription("");
				mashup.setWorkgroup(wg);
				
				mashup = projectService.addProject(mashup, owner);
				
				mockup = new GuiProject();
				
				//mockup.setName(ideaName + "_mockup");
				mockup.setName(cleanIdeaName);
				mockup.setDescription("");
				mockup.setWorkgroup(wg);
				
				mockup = guiEditorService.addProject(mockup, owner);
				guiEditorService.mashupProjectAdd(mockup.getId(), mashup.getId());
				
				// Add members to mashup project
				for(int k = update ? 0 : 1; k < memberAccount.size(); k++)
				{
					projectService.memberAdd(mashup.getId(), memberAccount.get(k).getProfile().getId());
				}
				
				// Add members to mockup project
				for(int k = update ? 0 : 1; k < memberAccount.size(); k++)
				{
					guiEditorService.memberAdd(mockup.getId(), memberAccount.get(k).getProfile().getId());
				}
			}
			else
			{
				List<Project> mashups = projectService.findAllByWorkgroup(wg);
				List<GuiProject> mockups = guiEditorService.findAllByWorkgroup(wg);
				
				List<Profile> profiles;
				
				// update mashups members
				for(int i = 0; i < mashups.size(); i++)
				{
					profiles = projectService.findProjectMembers(mashups.get(i));
					
					for(int j = 0; j < profiles.size(); j++)
					{	
						if(profiles.get(j).getEmail().compareTo(authority) == 0)
							continue;
						
						projectService.memberRemove(mashups.get(i).getId(), profiles.get(j).getId());
					}
					
					// Add members to mashup projects
					for(int k = update ? 0 : 1; k < memberAccount.size(); k++)
					{
						projectService.memberAdd(mashups.get(i).getId(), memberAccount.get(k).getProfile().getId());
					}
				}
				
				// update mockups members
				for(int i = 0; i < mockups.size(); i++)
				{
					profiles = guiEditorService.findProjectMembers(mockups.get(i));
				
					for(int j = 0; j < profiles.size(); j++)
					{
						if(profiles.get(j).getEmail().compareTo(authority) == 0)
							continue;
						
						guiEditorService.memberRemove(mockups.get(i).getId(), profiles.get(j).getId());
					}
					
					// Add members to mockup projects
					for(int k = update ? 0 : 1; k < memberAccount.size(); k++)
					{
						guiEditorService.memberAdd(mockups.get(i).getId(), memberAccount.get(k).getProfile().getId());
					}
				}
			}
			
		} 
		catch(JSONException e) 
		{
			logger.error("Error parsing request into JSON");
			e.printStackTrace();
			
			node.put("message", MESSAGE_JSON_ERROR);
			node.put("type", MESSAGE_TYPE_ERROR);
			
			return node.toString();
		}
		catch(Exception e)
		{
			logger.error("Error in workgroup or projects creation");
			e.printStackTrace();
			
			node.put("message", MESSAGE_SERVER_ERROR);
			node.put("type", MESSAGE_TYPE_ERROR);
			
			return node.toString();
		}
		
		if(!warning)
		{
			node.put("message", MESSAGE_CREATED);
			node.put("type", MESSAGE_TYPE_SUCCESS);
		}
		
		if(!update)
		{
			node.put("workgroupid", wg.getId());
			node.put("mashupid", mashup.getId());
			node.put("mockupid", mockup.getId());
			node.put("mashupname", mashup.getName());
			node.put("mockupname", mockup.getName());
		}
		
		return node.toString();
	}
	
	// validate REST and SOAP services against wadl/wsdl xsd schema
	@RequestMapping(value = "/validateService", method=RequestMethod.POST)
	public @ResponseBody ObjectNode validateService(@RequestBody ServiceValidation sv, HttpServletRequest request)
	{
		ObjectNode res = JsonNodeFactory.instance.objectNode();
		ObjectNode validation = null;
		
		String type = sv.getType();
		
		if(!(type.compareToIgnoreCase("REST") == 0 || type.compareToIgnoreCase("SOAP") == 0))
		{
			res.put("valid", false);
			res.put("code", "vc-invalid-type");
			res.put("message", "[Error] " + type + " validation not supported.");
			
			return res;
		}
		
		String xsd = type.compareToIgnoreCase("REST") == 0 ? "wadl" : "wsdl";
		
		String xml = "";
		
		// retrieve xml from url
		try
		{			
			URL url = new URL(sv.getUrl());
			InputStream in_xml = url.openStream();
			xml = IOUtils.toString(in_xml, "UTF-8");
			
			in_xml.close();
		}
		catch(IOException e)
		{
			logger.error("Error retrieving wadl from URL " + sv.getUrl());
			
			res.put("valid", false);
			res.put("code", "vc-error-retrieving-wadl");
			res.put("message", "[Error] " + e.getMessage());
			
			return res;
		}				
			
		/*
		try
		{
			InputStream testwadl = GenericUtils.class.getClassLoader().getResourceAsStream("test.xml");
	    	
	    	xml = IOUtils.toString(testwadl, "UTF-8");
	    	
	    	testwadl.close();
		}
		catch(Exception e)
		{
			res.put("valid", false);
			res.put("code", "vc-error-internal");					
			return res;
		}
    	*/
    					
		
		// validate xml
		validation = GenericUtils.validateAgainstXSD(xml, xsd);
		
		// manage validation response
		switch(validation.get("error").asInt())
		{
			case 0:
				break;
			case 1:
				res.put("valid", false);
				res.put("code", "vc-error-internal");	
				res.put("message", "[Error] Validation failed due to internal errors.");
				return res;
			case 2:
				res.put("valid", false);
				res.put("code", "vc-error-retrieving-" + xsd);
				res.put("message", validation.get("message").asText());
				return res;
			case 3:
				res.put("valid", false);
				res.put("code", "vc-error-validating-" + xsd);
				res.put("message", validation.get("message").asText());
				return res;
		}
		
		res.put("valid", true);
		res.put("code", "vc-valid-" + xsd);
		res.put("message", "[Success] " + sv.getUrl() + " is a valid " + xsd + ".");
		
		return res;		
	}
	
	// TEST: to be removed
	@RequestMapping(value = "/parseswagger", method=RequestMethod.POST)
	public @ResponseBody ObjectNode swaggerParser(@RequestBody String swagger, HttpServletRequest request, Principal principal)
	{
		ObjectNode res = JsonNodeFactory.instance.objectNode();
		
		res.put("valid", true);
		res.putArray("errors");
		res.putArray("warnings");
		
		ArrayNode errors = JsonNodeFactory.instance.arrayNode();
		ArrayNode warnings = JsonNodeFactory.instance.arrayNode();
		
		SwaggerParsingResponse validationResponse = catalogService.validateSwaggerService(swagger);
		List<String> validationErrors = validationResponse.getErrors();
		List<String> validationWarnings = validationResponse.getWarnings();
		
		if(validationErrors.size() > 0)
		{
			res.put("valid", false);
			
			for(int i = 0; i < validationErrors.size(); i++)
				errors.add(validationErrors.get(i));
		}
		else // TEST
		{
			try
			{
				catalogService.addFromSwagger(swagger, profileService.findProfile(15));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		for(int i = 0; i < validationWarnings.size(); i++)
			warnings.add(validationWarnings.get(i));	
		
		res.set("errors", errors);
		res.set("warnings", warnings);
		
		return res;
	}
	
	// TEST: to be removed
	@RequestMapping(value = "/importmetamodel/{userId}", method=RequestMethod.POST)
	public @ResponseBody ObjectNode importFromMetamodel(@RequestBody String metamodel, @PathVariable("userId") Integer userId, HttpServletRequest request, Principal principal)
	{
		ObjectNode res = JsonNodeFactory.instance.objectNode();
		//System.out.println("INPUT: " + metamodel);
		
		try
		{
			ServiceCatalog serviceCatalog = catalogService.addRESTFromMetamodel(metamodel, profileService.findProfile(userId));
			
			if(serviceCatalog != null)
				res.put("imported", true);
			else
				res.put("imported", false);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			res.put("imported", false);
		}	
		
		return res;
	}
	
	// TEST: to be removed
	@RequestMapping(value = "/importswagger/{userId}", method=RequestMethod.POST)
	public @ResponseBody ObjectNode importFromSwagger(@RequestBody String swagger, @PathVariable("userId") Integer userId, HttpServletRequest request, Principal principal)
	{
		ObjectNode res = JsonNodeFactory.instance.objectNode();
		//System.out.println("INPUT: " + metamodel);
		
		try
		{
			ServiceCatalog serviceCatalog = catalogService.addFromSwagger(swagger, profileService.findProfile(userId));
			
			if(serviceCatalog != null)
				res.put("imported", true);
			else
				res.put("imported", false);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			res.put("imported", false);
		}	
		
		return res;
	}
	
	// --------------------------------------------------------------------------------------------------------------------------	
	// EXTERNAL WEB SERVICES INVOCATIONS - OIA
	// --------------------------------------------------------------------------------------------------------------------------
	
	// utility: get header for WeLive Basic Auth
	private HttpHeaders getBasicAuthHeader()
	{
		HttpHeaders headers = new HttpHeaders();
		
		if(Boolean.parseBoolean(PropertyGetter.getProperty("basic.auth.enabled")))
		{
			String plainCreds = PropertyGetter.getProperty("welive.tools.username") + ":" + PropertyGetter.getProperty("welive.tools.password");
			byte[] plainCredsBytes = plainCreds.getBytes();
			byte[] base64CredsBytes = Base64.encode(plainCredsBytes);
			String base64Creds = new String(base64CredsBytes);
			
			headers.add("Authorization", "Basic " + base64Creds);
		}
		
		return headers;
	}
	
	// update the list of projects associated with an idea into OIA
	public void updateOIAprojects(int projectId, String projectName, boolean isMockup, int ideaId) throws Exception
	{
		System.out.println("Sending OIA new project information...");		
				
		String newProgectServiceUrl = PropertyGetter.getProperty("idea.newProject.serviceUrl");
		/*	+ "/vmeprojectid/" + Integer.toString(projectId)
			+ "/vmeprojectname/" + projectName
			+ "/ismockup/" + Boolean.toString(isMockup)
			+ "/ideaid/" + Integer.toString(ideaId);*/
		
		//System.out.println(newProgectServiceUrl);
		
		AddVcProject body = new AddVcProject();
		body.setVcProjectId(Integer.toString(projectId));
		body.setVcProjectName(projectName);
		body.setMockup(isMockup);
		body.setIdeaId(Integer.toString(ideaId));
		
		System.out.println("URL: " + newProgectServiceUrl);
		System.out.println("body: " + Json.pretty(body));
		
		RestTemplate restTemplate = new RestTemplate();
		
		// Add Basic Auth for WeLive components
	    //HttpEntity<String> request = new HttpEntity<String>(getBasicAuthHeader());		
	    
	    HttpHeaders headers = getBasicAuthHeader();
	    HttpEntity<AddVcProject> request = new HttpEntity<AddVcProject>(body, headers);
	    
	    ResponseEntity<String> response = restTemplate.exchange(newProgectServiceUrl, HttpMethod.POST, request, String.class);
	    
	    logger.debug(response.getBody());		
	}	

	// publish a screenshot of the mockup project into OIA
	public ObjectNode publishScreenshot(int ideaId, String description, String encodedScreenshot, String imgType, String username)
	{
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		try 
		{
			RestTemplate restTemplate = new RestTemplate();
			
			String baseUrl = PropertyGetter.getProperty("idea.publishScreenshot.serviceUrl");
			
			/*
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("idea_id", Integer.toString(ideaId));
			map.add("description", descroption);
			map.add("encodedScreenShot", encodedScreenshot);
			map.add("imgType", imgType);
			//map.add("mockup_id", Integer.toString(id));
			map.add("username", username);
		    */
			
		    HttpHeaders headers = getBasicAuthHeader();
		    //headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		    
		    NewScreenshot body = new NewScreenshot();
		    body.setIdea_id(Integer.toString(ideaId));
		    body.setEncodedScreenShot(encodedScreenshot);
		    body.setDescription(description);
		    body.setImgMimeType(imgType);
		    body.setUsername(username);
		    
		    //HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map,headers);
		    HttpEntity<NewScreenshot> request = new HttpEntity<NewScreenshot>(body, headers);
		    
		    ResponseEntity<String> response = restTemplate.exchange(baseUrl, HttpMethod.POST, request, String.class);
		    
		    logger.debug(response.getBody());
		    
		    JSONObject json = new JSONObject(response.getBody());
		    
		    boolean rcvResult = json.getBoolean("error");
		    
		    if(rcvResult == false)
		    {
		    	node.put("success", true);
		    	node.put("screenshotPageUrl", PropertyGetter.getProperty("idea.screenshotPage").replace("!IDEA_ID!", Integer.toString(ideaId)));
		    }
		    else
		    	node.put("success", false);
		} 
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			node.put("warning", true);
		}	
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			node.put("success", false);
		}		
									
		return node;
	}
	
	// --------------------------------------------------------------------------------------------------------------------------	
	// EXTERNAL WEB SERVICES INVOCATIONS - MKT
	// --------------------------------------------------------------------------------------------------------------------------
	
	// get a list of artefacts from marketplace (all if search is null)
	public RemoteArtefact[] getArtefactsFromMarketplace(String types, String search, String pilotId) throws Exception
	{
		if(pilotId == null || pilotId.compareTo("") == 0 || pilotId.compareTo("vc-all") == 0)
			pilotId = "All";
		
		if(Boolean.parseBoolean(PropertyGetter.getProperty("mkp.allPilots.enabled")))
			pilotId = "All";
		
		RestTemplate restTemplate = new RestTemplate();

		//Create a list for the message converters		
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();

		//Add the Jackson Message converter
		messageConverters.add(new MappingJackson2HttpMessageConverter());

		//Add the message converters to the restTemplate
		restTemplate.setMessageConverters(messageConverters); 
		
		String marketplaceArtefactsUrl;
		
		if(search != null)
			marketplaceArtefactsUrl = PropertyGetter.getProperty("mkp.searchArtefacts.serviceUrl")
					.replace("!KEYWORDS!", search).replace("!PILOT_ID!", pilotId).replace("!TYPES!", types.compareTo("all") == 0 ? "buildingblock,dataset" : types);
		else
			marketplaceArtefactsUrl = PropertyGetter.getProperty("mkp.allArtefacts.serviceUrl")
					.replace("!PILOT_ID!", pilotId).replace("!TYPES!", types.compareTo("all") == 0 ? "buildingblock,dataset" : types);
		
		//System.out.println(marketplaceArtefactsUrl);
		
		HttpEntity<String> request = new HttpEntity<String>(getBasicAuthHeader());
		ResponseEntity<MKTArtefacts> response = restTemplate.exchange(marketplaceArtefactsUrl, HttpMethod.GET, request, MKTArtefacts.class);
		
		//Json.prettyPrint(response.getBody());
		
		return response.getBody().getArtefacts().toArray(new RemoteArtefact[0]);
	}
	
	// get artefacts by ids (suggested by the decision engine)
	public RemoteArtefact[] getArtefactsByIdsFromMarketplace(String ids) throws Exception
	{
		// call the Marketplace to get artefacts info					    	
		RestTemplate restTemplate = new RestTemplate();

		//Create a list for the message converters		
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();

		//Add the Jackson Message converter
		messageConverters.add(new MappingJackson2HttpMessageConverter());

		//Add the message converters to the restTemplate
		restTemplate.setMessageConverters(messageConverters); 
		
		String url = PropertyGetter.getProperty("mkp.getSuggestedArtefacts.serviceUrl").replace("!ID_LIST!", ids);
		
		HttpEntity<String> request = new HttpEntity<String>(getBasicAuthHeader());
		ResponseEntity<MKTArtefacts> response = restTemplate.exchange(url, HttpMethod.GET, request, MKTArtefacts.class);
		
		return response.getBody().getArtefacts().toArray(new RemoteArtefact[0]);
	}
	
	// publish a mashup project in the marketplace
	public ObjectNode publishMashupProject(Integer mashupId)
	{
		AccountDTO loggedUser = getLoggedUser();
		
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		try 
		{
			Project project = projectService.findProject(mashupId);
			
			// authorization
			List<String> scopes = new ArrayList<String>();
			
			// dependencies
			List<Dependency> dependencies = new ArrayList<Dependency>();
			
			projectService.getAllDependencies(project, dependencies);
			
			/*
			Dependency dependency = null;
			
			List<ProjectOperation> services = projectOperationService.getAllByProject(project.getId());
			List<ProjectDataset> datasets = projectDatasetService.getAllByProject(project.getId());
			
			// add services from inner processes
	        
	        Iterator<ProjectInnerProcess> inners = projectInnerProcessDAO.getAllByProject(Integer.valueOf(mashupId)).iterator();
	        
	        ProjectInnerProcess inner = null;
	        List<ProjectOperation> innerServices = null;
	        
	        while(inners.hasNext())
	        {
	        	inner = inners.next();
	        	
	        	innerServices = projectOperationService.getAllByProject(inner.getInnerProcess().getId());
	        	
	        	if(!innerServices.isEmpty())
	        		services.addAll(innerServices);
	        }
	        
	        //
			
			ServiceCatalog sc = null;
			ServiceCatalogExt ext = null;
			RestOperation restOp = null;
			
			boolean isIn = false;
			
			for(int i = 0; i < services.size(); i++)
			{
				if(services.get(i).getRestOperation() != null)
				{
					restOp = services.get(i).getRestOperation();
					restOp = restOperationService.findRestOperationById(restOp.getId());
					sc = restOp.getServiceCatalog();
				}
				else
				{
					sc = soapOperationService.findSoapOperation(services.get(i).getSoapOperation().getId()).getServiceCatalog();
				}
				
				isIn = false;				
				
				ext = catalogExtService.getById(sc.getId());
				
				if(ext != null && ext.isFrom_marketplace())
				{
					for(int j = 0; j < dependencies.size(); j++)
					{
						if(dependencies.get(j).getId() == ext.getWsmodel_id())
						{
							isIn = true;
							break;
						}
					}
					
					// avoid duplicates
					if(!isIn)
					{
						dependency = new Dependency();
						dependency.setId((long) ext.getWsmodel_id());
						dependency.setTitle(sc.getName());
						
						dependencies.add(dependency);
					}
				}				
			}
			
			for(int i = 0; i < datasets.size(); i++)
			{
				isIn = false;
				
				for(int j = 0; j < dependencies.size(); j++)
				{
					if(dependencies.get(j).getId() == datasets.get(i).getResource().getOdsDataset().getMkpId())
					{
						isIn = true;
						break;
					}
				}
				
				// avoid duplicates
				if(!isIn)
				{
					dependency = new Dependency();
					dependency.setId((long) datasets.get(i).getResource().getOdsDataset().getMkpId());
					dependency.setTitle(sc.getName());
					
					dependencies.add(dependency);
				}
			}
			*/
			
			// mashup input parameters
			//Iterator<ServiceParameterDTO> inputs = projectService.getProjectInputParams(project.getId()).iterator();
			//ServiceParameterDTO input = null;
			
			// retrieve mashup inputs
	        Iterator<InputShapeDTO> inputsDTO = projectService.readInputShapesList(mashupId).iterator();
	        
	        InputShapeDTO inputDTO = null;
	        UD_InputShapeDTO userData = null;
			
			List<Parameter> parameters = new ArrayList<Parameter>();
			Parameter parameter = null;
			
			//while(inputs.hasNext())
			while(inputsDTO.hasNext())
			{
				//input = inputs.next();
				inputDTO = inputsDTO.next();
				userData = inputDTO.getUserData();
				
				if(userData.isIsConstant())
					continue;
				
				parameter = new Parameter();
				//parameter.setName(input.getLabel());
				parameter.setName(userData.getLabel());
				parameter.setIn("query");
				parameter.setType("string");
				
				parameters.add(parameter);
			}
			
			// if services require auth ...
			
			AuthenticationMeasure authMeasure = null;
			
			if(project.getAuthRequired() == 1)
			{			
				// ... populate lusdl authentication measure
				
				List<Permission> permissions = new ArrayList<Permission>();
				Permission permission = null; 
				
				scopes = Arrays.asList(project.getScopes().split(","));
				
				for(int i = 0; i < scopes.size(); i++)
				{
					permission = new Permission();
					permission.setIdentifier(scopes.get(i));
					
					permissions.add(permission);
				}				
				
				authMeasure = new AuthenticationMeasure();
				authMeasure.setTitle("WeLive User Authentication");
				authMeasure.setProtocolType("OAuth2");
				authMeasure.setPermissions(permissions);
				
				// ...add oauth token as query param 
				parameter = new Parameter();
				parameter.setName("access_token");
				parameter.setIn("header");
				parameter.setRequired(true);
				parameter.setType("string");
				parameter.setSchema("Authorization: Bearer <access_token>");
				
				parameters.add(parameter);
			}
			
			// populate lusdl model
			BuildingBlock bb = new BuildingBlock();
			
			bb.setTitle(project.getName());
			bb.setDescription(project.getDescription());
			bb.setType("RESTful Web Service");
			bb.setCreated(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
			bb.setUsedBBs(dependencies);
			bb.setPilot(loggedUser.getProfile().getPilotId());
			
			List<Entity> businessRoles = new ArrayList<Entity>();
			
			Entity owner = new Entity();
			owner.setTitle(loggedUser.getProfile().getFirstName() + " " + loggedUser.getProfile().getLastName());
			owner.setPage("");
			owner.setMbox(loggedUser.getEmail());
			owner.setBusinessRole("Owner");
			
			Entity author = new Entity();
			author.setTitle(loggedUser.getProfile().getFirstName() + " " + loggedUser.getProfile().getLastName());
			author.setPage("");
			author.setMbox(loggedUser.getEmail());
			author.setBusinessRole("Author");
			
			businessRoles.add(owner);
			businessRoles.add(author);
			
			bb.setBusinessRoles(businessRoles);
			
			List<String> produces = new ArrayList<String>();
			produces.add("application/json");
			
			Operation op = new Operation();
			op.setTitle(project.getName());
			op.setDescription(project.getDescription());
			op.setConsumes("application/json");
			op.setProduces(produces);
			op.setMethod("POST");
			op.setEndpoint(PropertyGetter.getProperty("oauth.callbackContext") + "/mashup/invoke/" + project.getId());
			op.setParameters(parameters);
			
			if(project.getAuthRequired() == 1)
				op.setSecurityMeasure(authMeasure);
			
			List<Operation> operations = new ArrayList<Operation>();
			operations.add(op);
			
			bb.setOperations(operations);
			
			// populate publication object
			
			ProjectPublication mashup = new ProjectPublication();
			mashup.setMashupId(project.getId());
			mashup.setMockupId(-1);
			
			IdeaWorkgroup idwg = ideawgService.getIdeaWorkgroupByWorkgroupId(project.getWorkgroup().getId());
			
			if(idwg != null)
				mashup.setIdeaId(idwg.getIdeaId());
			else
				mashup.setIdeaId(-1);
			
			mashup.setCcUserId(loggedUser.getProfile().getCcUserId());
			mashup.setType("bblocks");
			mashup.setLusdl(bb);
			
			//Json.prettyPrint(mashup);
			
			// invoke marketplace publication api
			
			RestTemplate restTemplate = new RestTemplate();	
			
			String publicationUrl = PropertyGetter.getProperty("mkp.artefact.publication.api");
			
			HttpHeaders headers = getBasicAuthHeader();
		    
		    HttpEntity<ProjectPublication> request = new HttpEntity<ProjectPublication>(mashup, headers);
		    
		    ResponseEntity<MashupPublicationResponse> response = restTemplate.exchange(publicationUrl, HttpMethod.POST, request, MashupPublicationResponse.class);
			
		    MashupPublicationResponse publicationResponse = response.getBody();
			
			ObjectMapper mapper = new ObjectMapper();
			
			ObjectNode res = (ObjectNode) mapper.readTree(mapper.writeValueAsString(publicationResponse));
			
			if(publicationResponse.isSuccess())
			{
				project.setMkpId(publicationResponse.getArtefactId());
				projectService.updateProject(project);
				
				String url = PropertyGetter.getProperty("mkp.artefact.publication.url")
					.replace("!ARTEFACT_ID!", Integer.toString(publicationResponse.getArtefactId()))
					.replace("!ARTEFACT_TYPE!", "bblocks");
			
				res.put("url", url);
			}
			
			return res;
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			node.put("success", false);
			node.putArray("errors").add(e.getMessage());
		}		
									
		return node;
	}
	
	// publish a mockup project in the marketplace
	public ObjectNode publishMockupProject(Integer mockupId, String type)
	{
		AccountDTO loggedUser = getLoggedUser();
		
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		try 
		{
			GuiProject project = guiEditorService.findProject(mockupId);
			
			// dependencies
			List<Dependency> dependencies = new ArrayList<Dependency>();
			
			Iterator<Project> usedMashups = guiEditorService.findMashupProjects(project).iterator();
			
			Project usedMashup = null;
			
			while(usedMashups.hasNext())
			{
				usedMashup = usedMashups.next();
				
				projectService.getAllDependencies(usedMashup, dependencies);
			}
			
			// populate lusdl model
			PublicServiceApplication psa = new PublicServiceApplication();
			psa.setTitle(project.getGadgetTitle() != null && project.getGadgetTitle().compareTo("") != 0 ? project.getGadgetTitle() : project.getName());
			psa.setDescription(project.getDescription());
			psa.setType(type.compareTo("apk") == 0 ? "Android Application" : "Web Application");
			psa.setCreated(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
			psa.setUsedBBs(dependencies);
			psa.setPilot(loggedUser.getProfile().getPilotId());
			
			List<Entity> businessRoles = new ArrayList<Entity>();
			
			Entity owner = new Entity();
			owner.setTitle(loggedUser.getProfile().getFirstName() + " " + loggedUser.getProfile().getLastName());
			owner.setPage("");
			owner.setMbox(loggedUser.getEmail());
			owner.setBusinessRole("Owner");
			
			Entity author = new Entity();
			author.setTitle(loggedUser.getProfile().getFirstName() + " " + loggedUser.getProfile().getLastName());
			author.setPage("");
			author.setMbox(loggedUser.getEmail());
			author.setBusinessRole("Author");
			
			businessRoles.add(owner);
			businessRoles.add(author);
			
			psa.setBusinessRoles(businessRoles);
			
			// populate publication object
			
			ProjectPublication mockup = new ProjectPublication();
			mockup.setMockupId(project.getId());
			mockup.setMashupId(-1);
			
			IdeaWorkgroup idwg = ideawgService.getIdeaWorkgroupByWorkgroupId(project.getWorkgroup().getId());
			
			if(idwg != null)
				mockup.setIdeaId(idwg.getIdeaId());
			else
				mockup.setIdeaId(-1);
			
			mockup.setCcUserId(loggedUser.getProfile().getCcUserId());
			mockup.setType("psa");
			mockup.setLusdl(psa);
			
			//Json.prettyPrint(mockup);
			
			// invoke marketplace publication api
			
			RestTemplate restTemplate = new RestTemplate();	
			
			String publicationUrl = PropertyGetter.getProperty("mkp.artefact.publication.api");
			
			HttpHeaders headers = getBasicAuthHeader();
		    
		    HttpEntity<ProjectPublication> request = new HttpEntity<ProjectPublication>(mockup, headers);
		    
		    ResponseEntity<MashupPublicationResponse> response = restTemplate.exchange(publicationUrl, HttpMethod.POST, request, MashupPublicationResponse.class);
			
		    MashupPublicationResponse publicationResponse = response.getBody();
			
			ObjectMapper mapper = new ObjectMapper();
			
			ObjectNode res = (ObjectNode) mapper.readTree(mapper.writeValueAsString(publicationResponse));
			
			if(publicationResponse.isSuccess())
			{
				if(type.compareTo("apk") == 0)
					project.setApkMkpId(publicationResponse.getArtefactId());
				else
					project.setWebMkpId(publicationResponse.getArtefactId());
				
				guiEditorService.updateProject(project, false);
				
				String url = PropertyGetter.getProperty("mkp.artefact.publication.url")
					.replace("!ARTEFACT_ID!", Integer.toString(publicationResponse.getArtefactId()))
					.replace("!ARTEFACT_TYPE!", "psa");
			
				res.put("url", url);
			}
			
			return res;
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			node.put("success", false);
			node.putArray("errors").add(e.getMessage());
		}		
									
		return node;
	}
	
	// --------------------------------------------------------------------------------------------------------------------------	
	// EXTERNAL WEB SERVICES INVOCATIONS - ODS
	// --------------------------------------------------------------------------------------------------------------------------
	
	// get dataset info from ODS
	public String getDatasetByODSid(String odsId) throws Exception
	{
		RestTemplate restTemplate = new RestTemplate();	
		
		String odsGetDatasetUrl = PropertyGetter.getProperty("ods.getDatasetById.serviceUrl").replace("!DATASET_ID!", odsId);
		
		return restTemplate.getForObject(odsGetDatasetUrl, String.class);
	}
	
	// get dataset schema from ods
	public String getDatasetSchema(String odsId, String odsResourceId) throws Exception
	{
		RestTemplate restTemplate = new RestTemplate();	
		
		String odsGetDatasetSchemaUrl = PropertyGetter.getProperty("ods.getDatasetSchema.serviceUrl");
		
		return restTemplate.getForObject(
				odsGetDatasetSchemaUrl.replace("!DATASET_ID!", odsId).replace("!RESOURCE_ID!", odsResourceId), 
				String.class);
	}
	
	// query an ODS resource identified by odsId and odsResourceId
	public String queryODSresource(String odsId, String odsResourceId, String queryText) throws Exception
	{
		RestTemplate restTemplate = new RestTemplate();	
		
		String odsQueryResourceUrl = PropertyGetter.getProperty("ods.queryResource.serviceUrl")
				.replace("!DATASET_ID!", odsId).replace("!RESOURCE_ID!", odsResourceId);
		
		//System.out.println(odsQueryResourceUrl);
		
		HttpHeaders headers = new HttpHeaders();
	    //headers.add("Content-Type", "text/plain;charset=UTF-8");
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8"))); // IMPORTANT!!!
	    
	    HttpEntity<String> request = new HttpEntity<String>(queryText, headers);
	    
	    ResponseEntity<String> response = restTemplate.exchange(odsQueryResourceUrl, HttpMethod.POST, request, String.class);
		
		return response.getBody();
	}
	
	// utility
	public static String getLoggingBBPilotByLiferayPilot(String liferayPilot)
	{		  
	  String val = "";
	  
	  Map<String, String> pilots = new HashMap<String, String>();
	  pilots.put("Bilbao", "Bilbao");
	  pilots.put("Novi Sad", "Novisad");
	  pilots.put("Region on Uusimaa-Helsinki", "Uusimaa");
	  pilots.put("Trento", "Trento");	 
	  
	  val = pilots.get(liferayPilot);  
	  
	  return val;
	}	
}
