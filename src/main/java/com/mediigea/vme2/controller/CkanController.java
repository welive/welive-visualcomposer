package com.mediigea.vme2.controller;

import it.eng.model.CkanDataset;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.ckan.CKANException;
import org.ckan.Client;
import org.ckan.Connection;
import org.ckan.Dataset;
import org.ckan.Dataset.SearchResults;
import org.ckan.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.entity.CkanServer;
import com.mediigea.vme2.exceptions.RestException;
import com.mediigea.vme2.service.CkanServerService;
import com.mediigea.vme2.service.OpenDataService;

@Controller
@RequestMapping(value = "/user/ckan")
@SessionAttributes("lastsearchresult")
public class CkanController extends BaseController {

	// @Autowired
	// private ObjectMapperExt objectMapper;
	//
	@Autowired
	private OpenDataService openDataService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private CkanServerService ckanServerService;

	private static final Logger logger = LoggerFactory
			.getLogger(CkanController.class);

	/**
	 * Public method to read open data resources
	 * 
	 * @param odataId
	 * @param request
	 * @param response
	 * @return
	 * @throws RestException
	 */
	@RequestMapping(value = "{id}/searchdataset.json", produces = "application/json")
	public @ResponseBody
	List<Dataset> searchDataset(ModelMap model, @PathVariable("id") Integer id,
			@RequestParam String query) throws RestException {

		List<Dataset> res = null;

		try {
			CkanServer ckanserver = ckanServerService.getById(id);

			Client client = new Client(new Connection(ckanserver.getUrl()), "");

			Dataset.SearchResults search_results = client.findDatasets(query);

			// Mette in sessione il risultato json dell'invocazione
			model.addAttribute("lastsearchresult", search_results.results);

			res = search_results.results;
		} catch (CKANException e) {
			String error = messageSource.getMessage(
					"message.error.jsonprocessing", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}

		return res;
	}
	
	@RequestMapping(value = "{id}/getalldatasets.json", produces = "application/json")
	public @ResponseBody
	SearchResults getAllDatasets(ModelMap model, @PathVariable("id") Integer id,
			@RequestParam String pagenumber) throws RestException {

		SearchResults res = null;

		try {
			CkanServer ckanserver = ckanServerService.getById(id);

			Client client = new Client(new Connection(ckanserver.getUrl()), "");

			res = client.getAll(10, Integer.parseInt(pagenumber));

			// Mette in sessione il risultato json dell'invocazione
			model.addAttribute("lastsearchresult", res.results);
		} catch (CKANException e) {
			String error = messageSource.getMessage(
					"message.error.jsonprocessing", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}

		return res;
	}	
	

	@RequestMapping(value = "/addresource.json", method = RequestMethod.POST)
	public ResponseEntity<String> addResource(
			@ModelAttribute("lastsearchresult") List<Dataset> search_results,
			//@RequestParam Integer ckanServerId,
			//@RequestParam String datasetId,
			//@RequestParam String resourceId
			@RequestBody CkanDataset data, HttpServletRequest request) throws RestException {
		
		CkanServer selectedCkanServer = null;;
		Dataset selectedDataset = null;
		Resource selectedResource = null;
		try {
			for (Dataset dset : search_results) {
				if (dset.getId().equals(data.getDatasetId())) {
					selectedDataset = dset;

					List<Resource> resources = dset.getResources();
					for (Resource res : resources) {
						if (res.getId().equals(data.getResourceId())) {

							selectedResource = res;
							break;
						}
					}
					break;
				}
			}

			selectedCkanServer = ckanServerService.getById(Integer.parseInt(data.getCkanServerId()));
			
			openDataService.addFromCkan(selectedCkanServer, selectedDataset,
					selectedResource, getLoggedUser().getProfile(), ';');
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RestException(e);
		}
		
		return new ResponseEntity<String>(HttpStatus.OK);
	}
}