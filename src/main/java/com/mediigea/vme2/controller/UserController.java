package com.mediigea.vme2.controller;

import it.eng.PropertyGetter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.Activity;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.service.AccountService;
import com.mediigea.vme2.service.ActivityService;
import com.mediigea.vme2.service.ProfileService;
import com.mediigea.vme2.util.SimpleCrypto;
import com.mediigea.vme2.util.VmeConstants;

@Controller
@RequestMapping(value = "/user")
public class UserController extends BaseController {

	
	@Autowired
	private AccountService accountService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private ActivityService activityService;
	
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView userHome() {

		ModelAndView mav = getDefaultModelAndView("user/home");

		return mav;
	}	
	
	@RequestMapping(value = "/me", method = RequestMethod.GET)
	public ModelAndView editProject() {

		logger.info("Edit project View");

		ModelAndView mav = getDefaultModelAndView("user/me/index");

		return mav;
	}

	@RequestMapping(value = "/me/changepassword", method = RequestMethod.GET)
	public ModelAndView changePassword() {
		
		ModelAndView mav = getDefaultModelAndView("user/me/changepassword");
		return mav;
	}

	@RequestMapping(value = "/me/changepassword/execute", method = RequestMethod.POST)
	public ModelAndView changePassword_execute(@RequestParam("old") String oldp, @RequestParam("new") String newp) {
	
		ModelAndView mav = null;
		try {
			AccountDTO a = getLoggedUser();
			
			if (a.getPsw().equals(SimpleCrypto.md5(oldp))) {
				Account accountToUpdate = accountService.getById(a.getId());
				
				accountToUpdate.setPsw(SimpleCrypto.md5(newp));
				
				accountService.updateAccount(accountToUpdate);
				mav = getDefaultModelAndView("redirect:/user/me/");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.password.change.success");
				
				
				activityService.activityStreamRecordSave(a.getProfile(), AbstractDAO.ActivityType.USER_UPDATE_PASSWORD.getValue());

				/*****
				 * activity log
				 
				Activity ac = new Activity();
				ac.setActivityType();
				ac.setDescription("User Login:" + a.getEmail());
				ac.setProfile(p);
				accountDAO.createAudit(ac);
				/*****
				 * activity log
				 */						
			} else {
				mav = getDefaultModelAndView("user/me/changepassword");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.password.change.erroroldpassword");
				
			}

		} catch(Exception ex) {
			logger.error(ex.getMessage());
			mav = getDefaultModelAndView("user/me/changepassword");			
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.password.change.error");
			
		}

		return mav;
	}
	
	@RequestMapping(value = "/me/switchmode", method = RequestMethod.POST)
	public ModelAndView switchDevMode(@RequestParam("userLevel") String userLevel, HttpServletRequest request) 
	{	
		String redirectUrl = request.getHeader("referer").replace(PropertyGetter.getProperty("oauth.callbackContext"), "");
		
		ModelAndView mav = getDefaultModelAndView("redirect:" + redirectUrl);
		
		try 
		{
			AccountDTO a = getLoggedUser();
			
			Profile profile = a.getProfile();
			profile.setLevel(Integer.parseInt(userLevel));
		
			profileService.updateProfile(profile);
		} 
		catch(Exception ex) 
		{
			logger.error(ex.getMessage());
		}
		
		return mav;
	}

}
