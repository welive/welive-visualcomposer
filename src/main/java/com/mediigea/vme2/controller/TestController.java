package com.mediigea.vme2.controller;

import groovy.xml.MarkupBuilder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.IOUtils;
import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.opensocial.*;
import com.mediigea.vme2.parser.WsdlParser;
import com.mediigea.vme2.service.ProfileService;
import com.mediigea.vme2.service.ServiceCatalogService;
import com.mediigea.vme2.soap.SoapPackage;
import com.mediigea.vme2.soap.SoapParameter;
import com.mediigea.vme2.viewmodel.AddWsdlServiceViewModel;
import com.mediigea.vme2.viewmodel.TestWadlViewModel;
import com.predic8.wsdl.Definitions;
import com.predic8.wsdl.WSDLParser;
import com.predic8.wstool.creator.RequestCreator;
import com.predic8.wstool.creator.SOARequestCreator;

@Controller
public class TestController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(TestController.class);

	@Autowired
	private ProfileService profileService;

	@Autowired
	private ServiceCatalogService serviceCatalogService;

	@RequestMapping(value = "/test/profile", method = RequestMethod.GET)
	public ModelAndView testProfile() {

		logger.info("Hibernate test");
		// profileService.insertProfile();

		return new ModelAndView("redirect:/auth");
	}

	@RequestMapping(value = "/test/wadl", method = RequestMethod.GET)
	public String testWADL() {

		return "test/wadl";
	}

	@RequestMapping(value = "/test/wadl/file", method = RequestMethod.POST)
	//public @ResponseBody RestApplication 
	public String testWADL(@ModelAttribute TestWadlViewModel wadlVM) {
		//RestApplication app = null;
		try {
			String wadl = IOUtils.toString(wadlVM.getWadlFile().get(0).getInputStream());
			//app = WadlParser.parseString("Test", wadl);
			//app.getName();
			Profile profile = profileService.findProfile(1); // ADMIN

			serviceCatalogService.addFromWadl("test", wadl, profile);

		} catch (IOException e) {
			logger.error("ERRORE: " + e.getMessage());
		} catch (XmlException e) {
			logger.error("ERRORE: " + e.getMessage());
		}
		return "redirect:/test/wadl";

	}

	@RequestMapping(value = "/test/wsdl", method = RequestMethod.GET)
	public String testWSDL() {
		// ModelAndView mav = new ModelAndView("/test/wsdl", "command", new
		// WsdlService());

		return "test/wsdl";
	}

	@RequestMapping(value = "/test/wsdl", method = RequestMethod.POST)
	public String testWSDL(@RequestParam("wsdlURL") String url) {

		// WSDLParser parser = new WSDLParser();
		// Definitions defs = parser.parse(url);
		//
		// Service service = defs.getServices().get(0);
		//
		// ServiceCatalog serviceCatalog = new ServiceCatalog();
		// serviceCatalog.setName(service.getName());
		// serviceCatalog.setType(VmeConstants.SERVICECATALOG_TYPE_WSDL);
		// serviceCatalog.setVisibility(VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE);
		// // serviceCatalog.setProfile(profile);
		// serviceCatalog.setCreatedAt(new Date());
		// serviceCatalog.setUpdatedAt(new Date());
		//
		// // serviceCatalogDAO.create(serviceCatalog);
		//
		// SoapWS soap = new SoapWS();
		// soap.setWsdlUrl(url);
		// soap.setName(service.getName());
		//
		// Port port = null;
		// for (Port pp : service.getPorts()) {
		// if (pp.getBinding().getProtocol().equals("SOAP11")) {
		// port = pp;
		//
		// break;
		// }
		// }
		//
		// String endPoint = port.getAddress().getLocation();
		// Binding binding = port.getBinding();
		// PortType portType = binding.getPortType();
		//
		// soap.setBindingName(binding.getName());
		// soap.setPortTypeName(portType.getName());
		//
		// // if (!"".equals(soap.getSoapBinding())) {
		// // soap.setCreatedAt(new Date());
		// // soap.setUpdatedAt(new Date());
		// // } else {
		// // throw new
		// // Exception("Only WebService with SOAP 11 Bindings are supported.");
		// // }
		// // soap.setService(serviceCatalog);
		// // soapWSDAO.create(soap);
		//
		// StringWriter writer = new StringWriter();
		//
		// // SOAPRequestCreator constructor: SOARequestCreator(Definitions,
		// // Creator, MarkupBuilder)
		// // SOARequestCreator creator = new SOARequestCreator(defs, new
		// // RequestTemplateCreator(), new MarkupBuilder(writer));
		//
		// StringBuffer sb = null;
		// for (Operation op : portType.getOperations()) {
		// SoapOperation soapOp = new SoapOperation();
		// soapOp.setSoapWS(soap);
		// logger.info("Operation name:" + op.getName());
		// soapOp.setOperationName(op.getName());
		// Message in = op.getInput().getMessage();
		// logger.info("Input Message:" + in.getQname().toString());
		// soapOp.setInputMessage(in.getQname().toString());
		// logger.info("Input  Message Parts:\n ");
		// for (Part part : in.getParts()) {
		// // logger.info("    Part Name: " + part.getName());
		// // logger.info("    Part Element: " + ((part.getElement() !=
		// // null) ? part.getElement() : "not available!"));
		// // logger.info("    Part Type: " + ((part.getType() != null) ?
		// // part.getType() : "not available!"));
		// logger.info("");
		// sb = new StringBuffer();
		// sb.append("\n");
		// constructElementTree(part.getElement(), "", sb);
		// logger.info(sb.toString());
		// }
		// Message out = op.getOutput().getMessage();
		// logger.info("Output Message:" + out.getQname().toString());
		// soapOp.setOutputMessage(out.getQname().toString());
		// logger.info("Output  Message Parts:\n ");
		// for (Part part : out.getParts()) {
		// // logger.info("    Part Name: " + part.getName());
		// // logger.info("    Part Element: " + ((part.getElement() !=
		// // null) ? part.getElement() : "not available!"));
		// // logger.info("    Part Type: " + ((part.getType() != null) ?
		// // part.getType() : "not available!"));
		// // logger.info("");
		// sb = new StringBuffer();
		// sb.append("\n");
		// constructElementTree(part.getElement(), "", sb);
		// logger.info(sb.toString());
		// }
		// // creator.createRequest(PortType name, Operation name, Binding
		// // name);
		// // writer.getBuffer().setLength(0);
		// // creator.createRequest(portType.getName(), op.getName(),
		// // binding.getName());
		// //
		// // logger.info(writer.toString());
		//
		// // soapWSOperationsDAO.create(soapOp);
		// }

		return "redirect:/test/wsdl";
	}

	@RequestMapping(value = "/test/invoke", method = RequestMethod.GET)
	public String Invoke() {

		/* dati presi dalla tabella soapoperation */
		String wsdlurl = "http://wsf.cdyne.com/WeatherWS/Weather.asmx?WSDL";
		String endPoint = "http://wsf.cdyne.com/WeatherWS/Weather.asmx";
		String portTypeName = "WeatherSoap";
		String bindingName = "WeatherSoap";
		String operationName = "GetCityForecastByZIP";
		String soapAction = "http://ws.cdyne.com/WeatherWS/GetCityForecastByZIP";
		// "[{"label":"ZIP","value":"","name":"ZIP","required":false,"type":"string","xpath":"/GetCityForecastByZIP"}]"

		SoapParameter input = new SoapParameter();
		input.setName("ZIP");
		input.setXpath("/GetCityForecastByZIP");

		WSDLParser parser = new WSDLParser();
		Definitions defs = parser.parse(wsdlurl);

		// setto il valore dei parametri in questo esempio ZIP = 48888
		HashMap<String, String> formParams = new HashMap<String, String>();
		String param = "xpath:" + input.getXpath() + "/" + input.getName();
		formParams.put(param, "48888");

		StringWriter writer = new StringWriter();
		SOARequestCreator creator = new SOARequestCreator(defs, new RequestCreator(), new MarkupBuilder(writer));
		// setto i parametri nel creator
		creator.setFormParams(formParams);

		// creo il Soap xml dentro writer
		creator.createRequest(portTypeName, operationName, bindingName);

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			MessageFactory messageFactory = MessageFactory.newInstance();
			MimeHeaders mimeHeaders = new MimeHeaders();

			// creo soap message dal writer
			SOAPMessage soapRequest = messageFactory.createMessage(mimeHeaders, new ByteArrayInputStream(writer.toString().getBytes()));
			// imposto soap action
			soapRequest.getMimeHeaders().addHeader("SOAPAction", soapAction);

			/* Print the request message */
			System.out.print("Request SOAP Message = ");
			soapRequest.writeTo(System.out);
			System.out.println();

			// Send SOAP Message to SOAP Server
			SOAPMessage soapResponse = soapConnection.call(soapRequest, endPoint);

			// Process the SOAP Response
			printSOAPResponse(soapResponse);

			soapConnection.close();
		} catch (Exception e) {
			System.err.println("Error occurred while sending SOAP Request to Server");
			e.printStackTrace();
		}

		// logger.info(writer.toString());

		return "redirect:/";
	}

	/**
	 * Method used to print the SOAP Response
	 */
	private void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		logger.info("\nResponse SOAP Message = ");
		StreamResult result = new StreamResult(System.out);
		transformer.transform(sourceContent, result);
	}

	@RequestMapping(value = "/test/add/wsdl", method = RequestMethod.GET)
	public ModelAndView addWsdlService() {

		logger.info("Add service View");

		ModelAndView mav = new ModelAndView("test/addwsdl");

		mav.addObject("model", new AddWsdlServiceViewModel());

		return mav;
	}

	@RequestMapping(value = "/test/add/wsdl/json", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	SoapPackage catalogJSON(@ModelAttribute AddWsdlServiceViewModel addWsdlVM, final HttpServletRequest request) {

		logger.info("Test Service Catalog with new wsdlParser");
		logger.info("URL: " + addWsdlVM.getWsdlUrl());

		WsdlParser parser = new WsdlParser();
		SoapPackage item = null;

		try {

			item = parser.parse(addWsdlVM.getWsdlUrl());
		} catch (Exception e) {

			logger.info("ERRORE ENORME");
			e.printStackTrace();
		}

		return item;
	}

	@RequestMapping(value = "/test/add/wsdl/file/json", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	SoapPackage catalogJSON1(@ModelAttribute AddWsdlServiceViewModel addWsdlVM, final HttpServletRequest request) {

		logger.info("Test Service Catalog with new wsdlParser from file");

		List<MultipartFile> files = addWsdlVM.getWsdlFile();

		WsdlParser parser = new WsdlParser();
		SoapPackage item = null;

		try {

			if (files != null && !files.isEmpty())
				item = parser.parse(files.get(0).getBytes());
		} catch (Exception e) {

			logger.info("ERRORE ENORME");
			e.printStackTrace();
		}

		return item;
	}
	
	@RequestMapping(value = "/test/gadget", method = RequestMethod.GET, produces = "application/xml")
	public @ResponseBody
	String createGadget(final HttpServletRequest request) {

		logger.info("Test Gadget xml creation");
		
		//Example
		/*
		   <?xml version="1.0" encoding="UTF-8" ?>
			<Module>
			  <ModulePrefs title="OAuth Contacts" scrolling="true">
			    <Require feature="locked-domain"/>
			    <OAuth>
			      <Service name="google">
			        <Access url="https://www.google.com/accounts/OAuthGetAccessToken" method="GET" />
			        <Request url="https://www.google.com/accounts/OAuthGetRequestToken?scope=http://www.google.com/m8/feeds/" method="GET" />
			        <Authorization url="https://www.google.com/accounts/OAuthAuthorizeToken?oauth_callback=http://oauth.gmodules.com/gadgets/oauthcallback" />
			      </Service>
			    </OAuth>
			  </ModulePrefs>
			  <Content type="html">
			  <![CDATA[
			
			  <!-- shindig oauth popup handling code -->
			  <script src="http://gadget-doc-examples.googlecode.com/svn/trunk/opensocial-gadgets/popup.js"></script>
			
			  <style>
			  #main {
			    margin: 0px;
			    padding: 0px;
			    font-size: small;
			  }
			  </style>
			
			  <div id="main" style="display: none">
			  </div>
			
			  <div id="approval" style="display: none">
			    <img src="http://gadget-doc-examples.googlecode.com/svn/trunk/images/new.gif">
			    <a href="#" id="personalize">Personalize this gadget</a>
			  </div>
			
			  <div id="waiting" style="display: none">
			    Please click
			    <a href="#" id="approvaldone">I've approved access</a>
			    once you've approved access to your data.
			  </div>
			
			  <script type="text/javascript">
			    // Display UI depending on OAuth access state of the gadget (see <divs> above).
			    // If user hasn't approved access to data, provide a "Personalize this gadget" link
			    // that contains the oauthApprovalUrl returned from makeRequest.
			    //
			    // If the user has opened the popup window but hasn't yet approved access, display
			    // text prompting the user to confirm that s/he approved access to data.  The user
			    // may not ever need to click this link, if the gadget is able to automatically
			    // detect when the user has approved access, but showing the link gives users
			    // an option to fetch their data even if the automatic detection fails.
			    //
			    // When the user confirms access, the fetchData() function is invoked again to
			    // obtain and display the user's data.
			    function showOneSection(toshow) {
			      var sections = [ 'main', 'approval', 'waiting' ];
			      for (var i=0; i < sections.length; ++i) {
			        var s = sections[i];
			        var el = document.getElementById(s);
			        if (s === toshow) {
			          el.style.display = "block";
			        } else {
			          el.style.display = "none";
			        }
			      }
			    }
			
			    // Process returned JSON feed to display data.
			    function showResults(result) {
			      showOneSection('main');
			
			      var titleElement = document.createElement('div');
			      var nameNode = document.createTextNode(result.feed.title.$t);
			      titleElement.appendChild(nameNode);
			      document.getElementById("main").appendChild(titleElement);
			      document.getElementById("main").appendChild(document.createElement("br"));
			
			      list = result.feed.entry;
			
			      for(var i = 0; i < list.length; i++) {
			        entry = list[i];
			        var divElement = document.createElement('div');
			        divElement.setAttribute('class', 'name');
			        divElement.appendChild(nameNode);
			        if (entry.gd$email) {
			          var valueNode = document.createTextNode(entry.gd$email[0].address);
			          divElement.appendChild(valueNode);
			        }
			        document.getElementById("main").appendChild(divElement);
			      }
			    }
			
			    // Invoke makeRequest() to fetch data from the service provider endpoint.
			    // Depending on the results of makeRequest, decide which version of the UI
			    // to ask showOneSection() to display. If user has approved access to his
			    // or her data, display data.
			    // If the user hasn't approved access yet, response.oauthApprovalUrl contains a
			    // URL that includes a Google-supplied request token. This is presented in the
			    // gadget as a link that the user clicks to begin the approval process.
			    function fetchData() {
			      var params = {};
			      url = "http://www.google.com/m8/feeds/contacts/default/base?alt=json";
			      params[gadgets.io.RequestParameters.CONTENT_TYPE] = gadgets.io.ContentType.JSON;
			      params[gadgets.io.RequestParameters.AUTHORIZATION] = gadgets.io.AuthorizationType.OAUTH;
			      params[gadgets.io.RequestParameters.OAUTH_SERVICE_NAME] = "google";
			      params[gadgets.io.RequestParameters.OAUTH_USE_TOKEN] = "always";
			      params[gadgets.io.RequestParameters.METHOD] = gadgets.io.MethodType.GET;
			
			      gadgets.io.makeRequest(url, function (response) {
			        if (response.oauthApprovalUrl) {
			          // Create the popup handler. The onOpen function is called when the user
			          // opens the popup window. The onClose function is called when the popup
			          // window is closed.
			          var popup = shindig.oauth.popup({
			            destination: response.oauthApprovalUrl,
			            windowOptions: null,
			            onOpen: function() { showOneSection('waiting'); },
			            onClose: function() { fetchData(); }
			          });
			          // Use the popup handler to attach onclick handlers to UI elements.  The
			          // createOpenerOnClick() function returns an onclick handler to open the
			          // popup window.  The createApprovedOnClick function returns an onclick
			          // handler that will close the popup window and attempt to fetch the user's
			          // data again.
			          var personalize = document.getElementById('personalize');
			          personalize.onclick = popup.createOpenerOnClick();
			          var approvaldone = document.getElementById('approvaldone');
			          approvaldone.onclick = popup.createApprovedOnClick();
			          showOneSection('approval');
			        } else if (response.data) {
			            showOneSection('main');
			            showResults(response.data);
			        } else {
			            // The response.oauthError and response.oauthErrorText values may help debug
			            // problems with your gadget.
			            var main = document.getElementById('main');
			            var err = document.createTextNode('OAuth error: ' +
			              response.oauthError + ': ' + response.oauthErrorText);
			            main.appendChild(err);
			            showOneSection('main');
			        }
			      }, params);
			    }
			    // Call fetchData() when gadget loads.
			    gadgets.util.registerOnLoadHandler(fetchData);
			  </script>
			  ]]>
			  </Content>
			</Module>
		 */
		//END Example
		
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Module>ERROR</Module>";
		
		Access accessToken = new Access();
		accessToken.setMethod("GET");
		accessToken.setUrl("https://www.google.com/accounts/OAuthGetAccessToken");
		
		Request requestToken = new Request();
		requestToken.setMethod("GET");
		requestToken.setUrl("https://www.google.com/accounts/OAuthGetRequestToken?scope=http://www.google.com/m8/feeds/");
		
		Authorization authorization = new Authorization();
		authorization.setMethod("GET");
		authorization.setUrl("https://accounts.google.com/o/oauth2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&state=%2Fprofile&redirect_uri=https%3A%2F%2Foauth2-login-demo.appspot.com%2Fcode&response_type=code&client_id=812741506391.apps.googleusercontent.com&approval_prompt=force");
		
		Service service = new Service();
		service.setName("google");
		service.setAuthorization(authorization);
		service.setRequestToken(requestToken);
		service.setAccessToken(accessToken);
		
		OAuth oauth = new OAuth();
		oauth.setService(service);
		
		//<Locale lang="en" country="us" />
		Locale localeEn = new Locale();
		localeEn.setCountry("us");
		localeEn.setLang("en");

		//<Locale lang="it" country="it" />
		Locale localeIt = new Locale();
		localeIt.setCountry("it");
		localeIt.setLang("it");
		
		//<Require feature="locked-domain"/>
		Require require = new Require();
		require.setFeature("locked-domain");
		
		Module gadget = new Module();
		ModulePrefs modulePrefs = new ModulePrefs();
		
		modulePrefs.setAuthor("John Doe");
		modulePrefs.setAuthor_email("john.doe@example.local");
		modulePrefs.addLocale(localeEn);
		modulePrefs.addLocale(localeIt);
		modulePrefs.setOauth(oauth);
		modulePrefs.addRequire(require);
		
		Content content = new Content();
		content.setType("html");
		content.setContent("<![CDATA[<div id=\"main\" style=\"display: none\"></div>]]>");
		
		Content content2 = new Content();
		content2.setType(Content.TYPE_URL_VALUE);
		content2.setHref("google.it");
		
		gadget.setModulePrefs(modulePrefs);
		gadget.addContent(content);
		gadget.addContent(content2);
		
		try {

		    StringWriter sw = new StringWriter();
		    
			JAXBContext context = JAXBContext.newInstance(Module.class);
		    Marshaller m = context.createMarshaller();
		    
		    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		    m.marshal(gadget, sw);
		    
		    xml = sw.toString();
		}catch(Exception gnEx){
			
			logger.error("Exception error: "  + gnEx.getMessage());
			
			gnEx.getStackTrace();
		}
		
		return xml;
	}
	@RequestMapping(value = "/test/gui", method = RequestMethod.GET)
	public String testGuiEditor() {

		return "user/guieditor/draw";
	}
}
