package com.mediigea.vme2.controller;

import it.eng.PropertyGetter;
import it.eng.model.GenericOpendata;
import it.eng.model.GenericService;
import it.eng.model.RemoteArtefact;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
















import org.apache.commons.io.IOUtils;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.dto.ODSResourceDTO;
import com.mediigea.vme2.dto.OpenDataResourceDTO;
import com.mediigea.vme2.entity.ODSDataset;
import com.mediigea.vme2.entity.ODSResource;
import com.mediigea.vme2.entity.OpenDataResource;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.ServiceCatalogExt;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.service.AccountService;
import com.mediigea.vme2.service.AuthManagerService;
import com.mediigea.vme2.service.ODSDatasetService;
import com.mediigea.vme2.service.ODSResourceService;
import com.mediigea.vme2.service.OpenDataService;
import com.mediigea.vme2.service.ProfileService;
import com.mediigea.vme2.service.ProjectDatasetService;
import com.mediigea.vme2.service.WorkgroupService;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.viewmodel.AddOpenDataViewModel;

@Controller
@RequestMapping(value = "/user/opendata")
public class OpenDataController extends BaseController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private WorkgroupService workgroupService;

	@Autowired
	private OpenDataService openDataService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ObjectMapperExt objectMapper;

	@Autowired
	private AuthManagerService authManagerService;
	
	@Autowired
	private ODSDatasetService odsDatasetService;
	
	@Autowired
	private ODSResourceService odsResourceService;
	
	@Autowired
	private ProjectDatasetService projectDatasetService;
	
	@Autowired
	private WebServiceController wsController;

	private static final Logger logger = LoggerFactory
			.getLogger(OpenDataController.class);
	/*
	@RequestMapping(value = "/list.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	List<OpenDataResourceDTO> listJSON(final HttpServletRequest request) {

		logger.info("Open data resources view");

		AccountDTO a = getLoggedUser();
		if (a == null || a.getRole() != VmeConstants.ACCOUNT_ROLE_USER) {

			logger.error("Logged Account is bad");
			return new ArrayList<OpenDataResourceDTO>();
		}

		return openDataService.getOpenDataResourcesDTO(a.getProfile());
	}
	
	@RequestMapping(value = { "/list", "" }, method = RequestMethod.GET)
	public ModelAndView list() {

		ModelAndView mav = null;
		try {

			List<OpenDataResource> myList = openDataService
					.findVisibleResources(getLoggedUser().getProfile().getId());
			List<OpenDataResource> publicList = openDataService
					.findPublicResources();

			mav = getDefaultModelAndView("user/opendata/list");
			mav.addObject("publicList", publicList);
			mav.addObject("myList", myList);

		} catch (Exception e) {
			logger.error(e.getMessage());
			mav = new ModelAndView("redirect:/user/project/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.servicecatalog.list.error");
		}

		return mav;
	}

	@RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") Integer id) {

		logger.info("Execute open data delete");

		ModelAndView mav = getDefaultModelAndView("redirect:/user/opendata/list");

		AccountDTO a = getLoggedUser();
		Profile p = profileService.findProfileByOpenDataCatalog(id);

		if (p == null || (p.getId() != a.getProfile().getId())) {

			mav = getDefaultModelAndView("redirect:/user/opendata/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.servicecatalog.delete.error");

		} else {

			try {

				openDataService.delete(id);

				mav.addObject(VmeConstants.MESSAGE_TEXT,
						"message.delete.success");
				// mav.addObject(VmeConstants.MESSAGE_TYPE,
				// VmeConstants.MESSAGE_SUCCESS);
			} catch (Exception ex) {

				logger.error("Delete Service ERROR: " + ex.getMessage());

				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");
			}
		}

		return mav;
	}
	
	@RequestMapping(value = "/{id}/setvisibility", method = RequestMethod.GET)
	public ModelAndView setVisibility(@PathVariable("id") int serviceId,
			@RequestParam int code) throws Exception {

		logger.info("Change open data resource visibility");
		ModelAndView mav = new ModelAndView("redirect:/user/opendata/list");

		if ((code == VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE)
				|| (code == VmeConstants.SERVICECATALOG_VISIBILITY_WORKGROUP)) {
			openDataService.setVisibility(serviceId, code);
		} else {
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.visibility.error");
		}
		return mav;
	}
	*/
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView add() {

		logger.info("Add open data View");

		ModelAndView mav = getDefaultModelAndView("user/opendata/add");

		mav.addObject("model", new AddOpenDataViewModel());

		return mav;
	}

	@RequestMapping(value = "/add/url", method = RequestMethod.POST)
	public ModelAndView addUrl(@ModelAttribute AddOpenDataViewModel addVM,
			BindingResult result, HttpServletRequest request) {

		logger.info("Execute Add Open Data from Url");

		ModelAndView mav = null;

		try {

			AccountDTO a = getLoggedUser();

			openDataService.addFromUrl(addVM.getSourceName(), addVM.getUrl(), a.getProfile(), addVM.getSeparator());			

			mav = getDefaultModelAndView("redirect:/user/opendata/list");

		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());

			mav = getDefaultModelAndView("redirect:/user/opendata/list");
			mav.addObject(VmeConstants.MESSAGE_TYPE, "message.add.error");
		}

		return mav;
	}

	@RequestMapping(value = "/add/file", method = RequestMethod.POST)
	public ModelAndView addFile(@ModelAttribute AddOpenDataViewModel addVM,
			BindingResult result) {

		logger.info("Execute Add Open Data from File ");

		ModelAndView mav = null;

		try {

			AccountDTO a = getLoggedUser();
			List<MultipartFile> files = addVM.getFile();

			if (files != null && files.size() > 0) {

				logger.debug("Import opendata from file");

				openDataService.addFromFile(addVM.getSourceName(), files.get(0), a.getProfile(), addVM.getSeparator());

				mav = getDefaultModelAndView("redirect:/user/opendata/list");
			} else {

				mav = getDefaultModelAndView("redirect:/user/opendata/list");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");

			}
		} catch (Exception ex) {

			logger.error("Exception: " + ex.getMessage());

			mav = getDefaultModelAndView("redirect:/user/opendata/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.add.error");
		}

		return mav;
	}
	
	@RequestMapping(value = "/import/fromurl")
	@ResponseBody
	public ObjectNode addOpendataFromUrl(@RequestBody GenericOpendata od)
	{
		logger.info("Execute Add Open Data from Url");
		
		String name = od.getOpendataName();
		String url = od.getOpendataUrl();
		String separator = od.getSeparator().trim();
		
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		try 
		{
			AccountDTO a = getLoggedUser();

			openDataService.addFromUrl(name, url, a.getProfile(), separator.charAt(0));		
			
			node.put("message", "Opendata '" + name + "' correctly imported!");

		} 
		catch (Exception ex) 
		{

			logger.error("Exception: " + ex.getMessage());

			node.put("message", "Error: Opendata '" + name + "' can't be imported!");
		}
		
		return node;
	}
	
	@RequestMapping(value = "/import/fromfile")
	@ResponseBody
	public ObjectNode addOpendataFromFile(@RequestBody GenericOpendata od)
	{
		logger.info("Execute Add Open Data from file");
		
		logger.debug(od.toString());
		
		String name = od.getOpendataName();
		String file = od.getOpendataFile();
		String filename = od.getFilename();
		String type = od.getMimeType();
		long size = od.getSize();
		String separator = od.getSeparator().trim();
		
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		try 
		{
			AccountDTO a = getLoggedUser();

			openDataService.addFromFile(name, file, filename, type, size, a.getProfile(), separator.charAt(0));		
			
			node.put("message", "Opendata '" + name + "' correctly imported!");

		} 
		catch (Exception ex) 
		{

			logger.error("Exception: " + ex.getMessage());

			node.put("message", "Error: Opendata '" + name + "' can't be imported!");
		}
		
		return node;
	}
	
	// IVAN
	// ---------------------------------------------------------------------------------------------------------------------------
	// ODS datasets management
	// ---------------------------------------------------------------------------------------------------------------------------
	
	private static final int OD_IMPORTED = 0;
	private static final int OD_ALREADY_EXISTS = -1;
	private static final int OD_ERROR = -2;
	
	public int importODSDataset(String odsId)
	{
		ODSDataset dataset;
		
		try 
		{			
			AccountDTO a = getLoggedUser();
			
			// check if dataset is already imported in user catalog
			List <ODSDataset> list = odsDatasetService.findVisibleDatasets(a.getProfile().getId());
			
			for(int i = 0; i < list.size(); i++)
			{
				if(list.get(i).getOdsDatasetId().compareTo(odsId) == 0)
				{
					return OD_ALREADY_EXISTS;
				}
			}
			//
			
			// call ODS to get dataset
			String jsonString = wsController.getDatasetByODSid(odsId);
			
			JSONObject jsonDataset = new JSONObject(jsonString).getJSONObject("result");
			
			String title = jsonDataset.getString("title");
			
			dataset = new ODSDataset();
			dataset.setTitle(title);
			dataset.setOdsDatasetId(odsId);
			dataset.setDescription("");
			
			JSONArray jsonResources = jsonDataset.getJSONArray("resources");
			
			ODSResource resource;
			JSONObject current;
			List<ODSResource> resources = new ArrayList<ODSResource>();
			
			boolean error = false;
			
			// for each dataset resource...
			for(int i = 0; i < jsonResources.length(); i++)
			{
				current = jsonResources.getJSONObject(i);
				
				resource = new ODSResource();
				resource.setTitle(current.getString("name"));
				resource.setDescription(current.getString("description"));
				resource.setUrl(current.getString("url"));
				resource.setOdsResourceId(current.getString("id"));
				//resource.setOdsDataset(dataset);
				
				try
				{
					// call ODS to get resource schema
					jsonString = wsController.getDatasetSchema(odsId, resource.getOdsResourceId());
					
					resource.setDataSchema(jsonString);
					
					error = false;
				}
				catch(Exception exi)
				{
					logger.error("Error importing resource '" + resource.getTitle()  + "': No mapping found");

					//return OD_ERROR;
					error = true;
				}
				
				if(!error)
					resources.add(resource);
			}
			
			// check if dataset has resources
			if(!resources.isEmpty())
			{
				dataset = odsDatasetService.addODSDataset(dataset, a.getProfile());
				
				for(int i = 0; i < resources.size(); i++)
				{
					resources.get(i).setOdsDataset(dataset);
					odsResourceService.addODSResource(resources.get(i));
				}
			}
			else
			{
				logger.error("Error importing dataset '" + odsId  + "': No valid resources available");

				return OD_ERROR;
			}		

		} 
		catch (Exception ex) 
		{
			logger.error("Error importing dataset '" + odsId  + "'");
			logger.error("Exception: " + ex.getMessage());
			ex.printStackTrace();

			return OD_ERROR;
		}

		return dataset.getId();
	}
	
	@RequestMapping(value = "/add/fromods", method = RequestMethod.POST)
	public ModelAndView addODSDataset(@ModelAttribute AddOpenDataViewModel addVM, @RequestParam String odsId, HttpServletRequest request) 
	{
		ModelAndView mav = getDefaultModelAndView("redirect:/user/opendata/list");
		
		int res = importODSDataset(odsId);
		
		if(res == OD_ALREADY_EXISTS)
		{
			mav.addObject(VmeConstants.MESSAGE_TYPE, "message.opendata.alreadyexists");				
		}
		else if(res == OD_ERROR)
		{
			mav.addObject(VmeConstants.MESSAGE_TYPE, "message.add.error");
		}

		return mav;
	}
	
	// check if opendata is currently used in mashup projects
	@RequestMapping(value = "/{id}/delete/check", method = RequestMethod.POST)
	public @ResponseBody ObjectNode checkDelete(@PathVariable("id") Integer id) 
	{
		ObjectNode dependencies = JsonNodeFactory.instance.objectNode();
		
		ArrayNode mashups = JsonNodeFactory.instance.arrayNode();
		
		ObjectNode node;
		
		List<Project> projects = new ArrayList<Project>();
		
		boolean flag;
		
		List<Project> tmp = null;
		
		Iterator<ODSResource> resources = odsResourceService.findAllByODSDataset(id).iterator();
		
		ODSResource res = null;
		
		while(resources.hasNext())
		{
			res = resources.next();
			
			tmp = projectDatasetService.findMashupsByResourceId(res.getId());
			
			for(int i = 0; i < tmp.size(); i++)
			{
				flag = true;
				
				for(int j = 0; j < projects.size(); j++)
				{
					if(projects.get(j).getId() == tmp.get(i).getId())
					{
						flag = false;
						break;
					}
				}
				
				if(flag)
					projects.add(tmp.get(i));
			}
		}		
		
		for(int i = 0; i < projects.size(); i++)
		{
			node = JsonNodeFactory.instance.objectNode();
			
			node.put("id", projects.get(i).getId());
			node.put("name", projects.get(i).getName());
			
			mashups.add(node);
		}
		
		dependencies.putArray("mashups");
		dependencies.set("mashups", mashups);
		
		return dependencies;		
	}
	
	@RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") Integer id) {

		logger.info("Execute open data delete");

		ModelAndView mav = getDefaultModelAndView("redirect:/user/opendata/list");

		AccountDTO a = getLoggedUser();
		Profile p = odsDatasetService.findDataset(id).getProfile();

		if (p == null || (p.getId() != a.getProfile().getId())) 
		{
			mav = getDefaultModelAndView("redirect:/user/opendata/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.servicecatalog.delete.error");
		} 
		else 
		{
			try 
			{
				odsDatasetService.deleteODSDataset(id);

				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.success");
			} 
			catch (Exception ex) 
			{
				logger.error("Delete Service ERROR: " + ex.getMessage());

				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");
			}
		}

		return mav;
	}
	
	@RequestMapping(value = "/{id}/setvisibility", method = RequestMethod.GET)
	public ModelAndView setVisibility(@PathVariable("id") int serviceId,
			@RequestParam int code) throws Exception {

		logger.info("Change open data resource visibility");
		ModelAndView mav = new ModelAndView("redirect:/user/opendata/list");

		if ((code == VmeConstants.SERVICECATALOG_VISIBILITY_PRIVATE)
				|| (code == VmeConstants.SERVICECATALOG_VISIBILITY_WORKGROUP)) {
			odsDatasetService.setVisibility(serviceId, code);
		} else {
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.visibility.error");
		}
		return mav;
	}
	
	@RequestMapping(value = { "/list", "" }, method = RequestMethod.GET)
	public ModelAndView list() 
	{

		ModelAndView mav = null;
		
		try 
		{
			List<ODSDataset> datasets = odsDatasetService.findVisibleDatasets(getLoggedUser().getProfile().getId());

			mav = getDefaultModelAndView("user/opendata/list");
			mav.addObject("datasets", datasets);

		} 
		catch (Exception e) 
		{
			logger.error(e.getMessage());
			mav = new ModelAndView("redirect:/user/project/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT,
					"message.servicecatalog.list.error");
		}

		return mav;
	}
	
	@RequestMapping(value = "/{id}/details", method = RequestMethod.GET)
	public ModelAndView details(@PathVariable("id") Integer id) 
	{
		logger.info("Detail dataset View");

		ModelAndView mav = null;

		ODSDataset dataset = odsDatasetService.findDataset(id);
		
		List<ODSResource> resources = odsResourceService.findAllByODSDataset(id);

		if (dataset == null || resources.isEmpty()) 
		{

			logger.debug("Dataset is bad");

			mav = getDefaultModelAndView("redirect:/user/opendata/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.find.error");

			return mav;
		}

		mav = getDefaultModelAndView("user/opendata/detail");
		mav.addObject("dataset", dataset);
		mav.addObject("resources", resources);

		return mav;
	}
	
	@RequestMapping(value = "/{resourceId}/schema", method = RequestMethod.GET)
	public @ResponseBody String getSchema(@PathVariable("resourceId") Integer resourceId) 
	{
		logger.info("Get Resource Schema");

		ODSResource resource = odsResourceService.findODSResource(resourceId);

		if (resource == null) 
		{
			return "";
		}

		return resource.getDataSchema();
	}
	
	@RequestMapping(value = "/{resourceId}/query", method = RequestMethod.POST)
	public @ResponseBody String queryResource(@PathVariable("resourceId") Integer resourceId, @RequestParam String queryText, HttpServletRequest request) 
	{
		logger.info("Query Resource");
		//System.out.println(queryText);
		ODSResource resource = odsResourceService.findODSResource(resourceId);
		ODSDataset dataset = resource.getOdsDataset();
		
		String result = "";
		
		try
		{
			result = wsController.queryODSresource(dataset.getOdsDatasetId(), resource.getOdsResourceId(), queryText);
			
			//System.out.println(result);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return result;
	}
	
	@RequestMapping(value = "/list.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	List<OpenDataResourceDTO> listJSON(final HttpServletRequest request) {

		logger.info("Open data resources view");

		AccountDTO a = getLoggedUser();
		if (a == null || a.getRole() != VmeConstants.ACCOUNT_ROLE_USER) {

			logger.error("Logged Account is bad");
			return new ArrayList<OpenDataResourceDTO>();
		}

		//return openDataService.getOpenDataResourcesDTO(a.getProfile());
		List<ODSDataset> datasets = odsDatasetService.findVisibleDatasets(a.getProfile().getId());
		
		List<OpenDataResourceDTO> list = new ArrayList<OpenDataResourceDTO>();
		OpenDataResourceDTO tmp;
		
		for(int i = 0; i < datasets.size(); i++)
		{
			tmp = new OpenDataResourceDTO();
			tmp.setId(datasets.get(i).getId());
			tmp.setName(datasets.get(i).getTitle());
			
			list.add(tmp);
		}
		
		return list;
	}
	
	@RequestMapping(value = "/{id}/resources.json", method = RequestMethod.GET)
	public @ResponseBody List<ODSResourceDTO> getResourcesJson(@PathVariable("id") Integer id) 
	{
		logger.debug("Get resources for opendata: " + id);
		
		try
		{
			List<ODSResource> list = odsResourceService.findAllByODSDataset(id);
			
			List<ODSResourceDTO> retList = new ArrayList<ODSResourceDTO>();
			ODSResourceDTO tmp;
			
			for (int i = 0; i < list.size(); i++)
			{
				tmp = new ODSResourceDTO();
				tmp.setId(list.get(i).getId());
				tmp.setDatasetId(id);
				tmp.setOdsDatasetId(list.get(i).getOdsDataset().getOdsDatasetId());
				tmp.setOdsResourceId(list.get(i).getOdsResourceId());
				tmp.setTitle(list.get(i).getTitle());
				tmp.setDescription(list.get(i).getDescription());
				
				retList.add(tmp);
			}
			
			return retList;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ArrayList<ODSResourceDTO>();
		}
	}
	
}
