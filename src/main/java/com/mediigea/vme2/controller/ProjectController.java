package com.mediigea.vme2.controller;

import io.swagger.util.Json;
import it.eng.PropertyGetter;
import it.eng.model.RemoteArtefact;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.config.ResourcesMapper;
import com.mediigea.vme2.dao.AbstractDAO;
import com.mediigea.vme2.dao.ProjectInnerProcessDAO;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.dto.BindingInputField;
import com.mediigea.vme2.dto.BindingInputParams;
import com.mediigea.vme2.dto.GuiFileResourceDTO;
import com.mediigea.vme2.dto.InputShapeDTO;
import com.mediigea.vme2.dto.ProfileDTO;
import com.mediigea.vme2.dto.ProjectDTO;
import com.mediigea.vme2.dto.ShapeContainer;
import com.mediigea.vme2.dto.ShapeDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.entity.CkanServer;
import com.mediigea.vme2.entity.Comment;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.GuiProjectProject;
import com.mediigea.vme2.entity.IdeaWorkgroup;
import com.mediigea.vme2.entity.ODSResource;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.entity.ProjectDataset;
import com.mediigea.vme2.entity.ProjectInnerProcess;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.SoapOperation;
import com.mediigea.vme2.entity.UnreadComment;
import com.mediigea.vme2.entity.Workgroup;
import com.mediigea.vme2.exceptions.ImportProjectException;
import com.mediigea.vme2.exceptions.InvokeWorkflowException;
import com.mediigea.vme2.exceptions.RestException;
import com.mediigea.vme2.exceptions.SelectShapeException;
import com.mediigea.vme2.exceptions.UserNotAuthorizedException;
import com.mediigea.vme2.exceptions.WorkflowScriptException;
import com.mediigea.vme2.exceptions.WriteResourceException;
import com.mediigea.vme2.service.AccountService;
import com.mediigea.vme2.service.ActivityService;
import com.mediigea.vme2.service.CkanServerService;
import com.mediigea.vme2.service.CommentService;
import com.mediigea.vme2.service.GuiEditorService;
import com.mediigea.vme2.service.IdeaWorkgroupService;
import com.mediigea.vme2.service.ProfileService;
import com.mediigea.vme2.service.ProjectDatasetService;
import com.mediigea.vme2.service.ProjectInnerProcessService;
import com.mediigea.vme2.service.ProjectOperationService;
import com.mediigea.vme2.service.ProjectService;
import com.mediigea.vme2.service.RestOperationService;
import com.mediigea.vme2.service.SoapOperationService;
import com.mediigea.vme2.service.SocialGadgetService;
import com.mediigea.vme2.service.WorkflowService;
import com.mediigea.vme2.service.WorkgroupService;
import com.mediigea.vme2.util.EmailSender;
import com.mediigea.vme2.util.FormatConverter;
import com.mediigea.vme2.util.JsTreeNode;
import com.mediigea.vme2.util.JsonUtils;
import com.mediigea.vme2.util.PagedSearch;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.viewmodel.ImportXmlViewModel;
import com.mediigea.vme2.viewmodel.ProjectDetailsViewModel;
import com.mediigea.vme2.viewmodel.ProjectMembersViewModel;
import com.sun.xml.internal.ws.api.server.HttpEndpoint;

/**
 * Controller to receive request about management of projects (Mashup)
 * @author Carmine Ruffino
 *
 */
@Controller
@RequestMapping(value = "/user/project")
public class ProjectController extends BaseController {

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private GuiEditorService guiEditorService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private WorkgroupService workgroupService;
	
	@Autowired
	private IdeaWorkgroupService ideawgService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private SocialGadgetService socialGadgetService;

	@Autowired
	private EmailSender mailSenderUtil;

	@Autowired
	private ObjectMapperExt objectMapper;

	@Autowired
	private ResourcesMapper resourcesMapper;

	@Autowired
	private WorkflowService workflowService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ActivityService activityService;
	
	@Autowired
	private CkanServerService ckanServerService;	
	
	@Autowired
	private ProjectOperationService projectOperationService;	
	
	@Autowired
	private ProjectInnerProcessService projectInnerProcessService;
	
	@Autowired
	private ProjectInnerProcessDAO projectInnerProcessDAO;	
	
	@Autowired
	private ProjectDatasetService projectDatasetService;
	
	@Autowired
	private RestOperationService restService;
	
	@Autowired
	private SoapOperationService soapOperationService;
	
	@Autowired
	private WebServiceController wsController;
	
	@Autowired
	private ArtefactController artefactController;
	
	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

	@InitBinder
	public void initBinder(WebDataBinder binder) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:m:s");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
	
	private boolean isLoggedUserAuthorized(Profile loggedUser, Project project)
	{
		List<Profile> members = projectService.findProjectMembers(project);
		
		boolean authorized = false;
		
		for(Profile member : members)
		{
			if(member.getId() == loggedUser.getId())
			{
				authorized = true;
				break;
			}
		}
		
		return authorized;
	}

	@RequestMapping(value = { "/list", "" }, method = RequestMethod.GET)
	public ModelAndView list() {

		logger.info("Projects View");
		
		AccountDTO a = getLoggedUser();

		logger.info("Projects View request from user: "+a.getEmail());
		
		ModelAndView mav = getDefaultModelAndView("user/project/list");

		List<ProjectDTO> projectList = projectService.findAllProjectDTOByProfile(a.getProfile());
		
		mav.addObject("projectList", projectList);

		return mav;
	}
	
	@RequestMapping(value = { "/search", "" }, method = RequestMethod.POST, params = { "clear" })
	public String clear() {
		
		return "redirect:/user/project/list";
	}
	
	@RequestMapping(value = { "/search", "" }, method = RequestMethod.POST, params = { "search" })
	public ModelAndView search(@RequestParam String criteria) {

		logger.info("Projects search");
		
		AccountDTO a = getLoggedUser();

		logger.info("Projects View request from user: "+a.getEmail());
		
		ModelAndView mav = getDefaultModelAndView("user/project/list");

		List<ProjectDTO> projectList = projectService.searchProjectDTO(a.getProfile(), criteria);
		
		mav.addObject("projectList", projectList);

		return mav;
	}	

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView add() {

		logger.info("Add project View");

		AccountDTO a = getLoggedUser();
		List<Workgroup> wgList = workgroupService.findByProfile(a.getProfile());

		if (wgList == null || wgList.isEmpty()) {

			ModelAndView mav = getDefaultModelAndView("redirect:/user/project/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.add.workgroupasscoaition.error");

			return mav;
		}

		ModelAndView mav = getDefaultModelAndView("user/project/add");
		mav.addObject("workgroupList", wgList);
		mav.addObject("project", new Project());

		return mav;
	}

	@RequestMapping(value = "/add/execute", method = RequestMethod.POST)
	public ModelAndView addExecute(@ModelAttribute Project project, BindingResult result) {

		logger.info("Execute Add project");

		AccountDTO a = getLoggedUser();

		Profile profile = a.getProfile();

		project = projectService.addProject(project, profile);
		ModelAndView mav = getDefaultModelAndView("redirect:/user/project/" + Integer.toString(project.getId()) + "/editor");
		
		// IVAN
		IdeaWorkgroup iw = ideawgService.getIdeaWorkgroupByWorkgroupId(project.getWorkgroup().getId());
		
		if(iw != null)
		{
			try
			{			
				// comunico all'OIA che e' presente un nuovo progetto per il workgroup associato all'idea
				wsController.updateOIAprojects(project.getId(), project.getName(), false, iw.getIdeaId());
				
				// idea workgroup members
				Iterator<Profile> members = profileService.findWorkgroupMembers(project.getWorkgroup().getId()).iterator();
				
				Profile member = null;
				
				while(members.hasNext())
				{
					member = members.next();
					
					if(member.getId() != profile.getId())
					{	
						// add workgroup profiles as project members
						projectService.memberAdd(project.getId(), member.getId());
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		// end
		
		String activityDescription = "Create Mashup project: " + project.getName();
		
		activityService.activityStreamRecordSave(profile, 
				 								AbstractDAO.ActivityType.CREATE_MASHUP_PROJECT.getValue(), 
				 								activityDescription);		
		
		return mav;
	}

	@RequestMapping(value = "{id}/edit", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable("id") Integer projectId) {

		logger.info("Edit project View");

		ModelAndView mav = null;

		try {
			Project p = projectService.findProject(projectId);
			Profile owner = projectService.findProjectOwner(projectId);
			boolean loggedOwner = owner.getId() == getLoggedUser().getProfile().getId();

			mav = getDefaultModelAndView("user/project/edit");
			mav.addObject("project", p);
			mav.addObject("loggedOwner", loggedOwner);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = new ModelAndView("redirect:/user/project/" + projectId + "/details");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.edit.error");

		}

		return mav;
	}

	@RequestMapping(value = "/edit/execute", method = RequestMethod.POST)
	public ModelAndView editExecute(@ModelAttribute("project") Project project, BindingResult result, @RequestParam Map<String,String> allRequestParams) {

		logger.info("Execute Edit project");

		ModelAndView mav = null;
		Project currentPrj = null;
		try {
			currentPrj = projectService.findProject(project.getId());
			currentPrj.setName(project.getName());
			currentPrj.setDescription(project.getDescription());
			currentPrj.setTags(project.getTags());
			project = projectService.updateProject(currentPrj);
			mav = new ModelAndView("redirect:/user/project/" + project.getId() + "/details");
			
			String activityDescription = "Edit Mashup project: " + project.getName();
			activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
					 								AbstractDAO.ActivityType.EDIT_MASHUP_PROJECT.getValue(), 
					 								activityDescription);				
			
		} catch (Exception e) {

			mav = getDefaultModelAndView("user/project/edit");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.edit.error");
			mav.addObject("project", project);

		}

		return mav;
	}

	@RequestMapping(value = "{id}/comments", method = RequestMethod.GET)
	public ModelAndView commentsList(@PathVariable("id") Integer projectId) {

		logger.info("Add comment to project");
		ModelAndView mav = null;
		try {
			Project p = projectService.findProject(projectId);
			Profile owner = projectService.findProjectOwner(projectId);
			boolean loggedOwner = owner.getId() == getLoggedUser().getProfile().getId();

			List<Comment> comments = projectService.findProjectComments(p);
			
			// sono entrato nei commenti del progetto, quindi cancello 
			// i messaggi non letti
			commentService.setReadedComment(p, getLoggedUser().getProfile());
			
			mav = getDefaultModelAndView("user/project/comments");
			mav.addObject("project", p);
			mav.addObject("comments", comments);
			mav.addObject("loggedOwner", loggedOwner);

		} catch (Exception e) {
			logger.error(e.getMessage());
			mav = new ModelAndView("redirect:/user/project/" + projectId + "/details");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.comments.list.error");

		}
		return mav;
	}

	@RequestMapping(value = "{id}/comments/add", method = RequestMethod.POST)
	public ModelAndView commentAddExecute(@PathVariable("id") Integer projectId, @ModelAttribute Comment formComment, BindingResult result) {

		logger.info("Execute Add comment to project");
		ModelAndView mav = getDefaultModelAndView("redirect:/user/project/" + projectId + "/details");
		Comment c = null;

		try {
			Project prj = projectService.findProject(projectId);
			Comment comment = new Comment();
			comment.setContent(formComment.getContent());
			comment.setProject(prj);
			comment.setProfile(getLoggedUser().getProfile());

			c = commentService.addComment(comment);

			if (c == null) {

				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.comment.add.error");

			} 
			else {
				String activityDescription = "Comment Mashup project: " + prj.getName();
				activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
						 								AbstractDAO.ActivityType.COMMENT_MASHUP_PROJECT.getValue(), 
						 								activityDescription);					
			}
		} catch (org.springframework.dao.DataIntegrityViolationException divEx) {

			logger.error("DataIntegrityViolationException: " + divEx.getMessage());

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.comment.add.error");

		} catch (javax.persistence.PersistenceException psEx) {

			logger.error("javax.persistence.PersistenceException: " + psEx.getMessage());

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.comment.add.error");

		} catch (org.hibernate.exception.DataException dataEx) {

			logger.error("DataException: " + dataEx.getMessage());

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.comment.add.error");

		}

		return mav;
	}

	@RequestMapping(value = "{id}/comments/{cId}/delete", method = RequestMethod.GET)
	public ModelAndView commentDelete(@PathVariable("id") Integer projectId, @PathVariable("cId") Integer commentId) {

		Comment comment = commentService.findComment(commentId);

		ModelAndView mav = getDefaultModelAndView("redirect:/user/project/" + projectId + "/details");
		if (comment.getProfile().getId() == getLoggedUser().getProfile().getId()) {
			try {
				commentService.deleteComment(commentId);
				
				Project prj = projectService.findProject(projectId);
				String activityDescription = "Delete comment on Mashup project: " + prj.getName();
				activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
						 								AbstractDAO.ActivityType.DELETE_COMMENT_MASHUP_PROJECT.getValue(), 
						 								activityDescription);					
				
			} catch (Exception ex) {
				logger.error("Error delete comment: " + ex.getMessage());
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");
			}
		} else {
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.delete.error");
		}

		return mav;
	}

	@RequestMapping(value = "{id}/details", method = RequestMethod.GET)
	public ModelAndView details(@PathVariable("id") Integer id, HttpServletResponse response) {

		logger.info("Detail project View");

		ModelAndView mav = null;
		
		try 
		{
			ProjectDetailsViewModel vm = new ProjectDetailsViewModel(projectService, id, getLoggedUser().getProfile());	
			
			if(!isLoggedUserAuthorized(getLoggedUser().getProfile(), vm.getProject()))
			{
				response.setStatus(401);
				return null;
			}
			
			mav = getDefaultModelAndView("user/project/detail");
			mav.addObject("model", vm);
			
			// IVAN
			IdeaWorkgroup ideawg = ideawgService.getIdeaWorkgroupByWorkgroupId(vm.getProject().getWorkgroup().getId());
			mav.addObject("ideawg", ideawg);
			
			// dependencies
			List<ProjectOperation> services = projectOperationService.getAllByProject(id);
			List<ProjectDataset> datasets = projectDatasetService.getAllByProject(id);
			
			List<RestOperation> restOperations = new ArrayList<RestOperation>();
			List<SoapOperation> soapOperations = new ArrayList<SoapOperation>();
			List<ODSResource> datasetResources = new ArrayList<ODSResource>();
			
			boolean isIn = false;
			
			for(int i = 0; i < datasets.size(); i++)
			{
				isIn = false;
				
				for(int j = 0; j < datasetResources.size(); j++)
				{
					if(datasetResources.get(j).getId() == datasets.get(i).getResource().getId())
					{
						isIn = true;
						break;
					}
						
				}
				
				// avoid duplicates
				if(!isIn)
					datasetResources.add(datasets.get(i).getResource());
			}
			
			for(int i = 0; i < services.size(); i++)
			{
				if(services.get(i).getRestOperation() != null)
				{
					isIn = false;
					
					for(int j = 0; j < restOperations.size(); j++)
					{
						if(restOperations.get(j).getId() == services.get(i).getRestOperation().getId())
						{
							isIn = true;
							break;
						}
					}
					
					// avoid duplicates
					if(!isIn)
						restOperations.add(restService.findRestResource(services.get(i).getRestOperation().getId()));
				}
				else
				{
					isIn = false;
					
					for(int j = 0; j < soapOperations.size(); j++)
					{
						if(soapOperations.get(j).getId() == services.get(i).getRestOperation().getId())
						{
							isIn = true;
							break;
						}
					}
					
					// avoid duplicates
					if(!isIn)
						soapOperations.add(soapOperationService.findSoapOperation(services.get(i).getSoapOperation().getId()));
				}
			}
			
			mav.addObject("restOperations", restOperations);
			mav.addObject("soapOperations", soapOperations);
			mav.addObject("datasets", datasetResources);
			
			// sono entrato nei dettagli del progetto, quindi cancello 
			// i messaggi non letti
			commentService.setReadedComment(vm.getProject(), getLoggedUser().getProfile());

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			ex.printStackTrace();
			mav = new ModelAndView("redirect:/user/project/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.details.error");

		}
		return mav;
	}

	@RequestMapping(value = "{id}/delete", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") Integer projectId) {

		ModelAndView mav = null;
		try {
			Project p = projectService.findProject(projectId);
			Profile owner = projectService.findProjectOwner(projectId);
			boolean loggedOwner = owner.getId() == getLoggedUser().getProfile().getId();

			mav = getDefaultModelAndView("user/project/delete");
			mav.addObject("project", p);
			mav.addObject("loggedOwner", loggedOwner);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = new ModelAndView("redirect:/user/project/" + projectId + "/details");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.delete.error");

		}
		return mav;
	}
	
	// check if project is referenced by other projects
	@RequestMapping(value = "{id}/delete/check", method = RequestMethod.POST)
	public @ResponseBody ObjectNode checkDelete(@PathVariable("id") Integer id) 
	{		
		ObjectNode dependencies = JsonNodeFactory.instance.objectNode();
		
		ArrayNode innerProcesses = JsonNodeFactory.instance.arrayNode();
		ArrayNode mockups = JsonNodeFactory.instance.arrayNode();
		
		ObjectNode node;
		
		List<ProjectInnerProcess> inner = projectInnerProcessDAO.getAllByInnerProcess(id);		
		Project project = null;
				
		for(int i = 0; i < inner.size(); i++)
		{
			project = inner.get(i).getProject();
			
			node = JsonNodeFactory.instance.objectNode();
			
			node.put("id", project.getId());
			node.put("name", project.getName());
			
			innerProcesses.add(node);
		}
		
		List<GuiProject> guiProjects = projectService.findMockupsProjects(id);
		GuiProject guiProject = null;
		
		for(int i = 0; i < guiProjects.size(); i++)
		{
			guiProject = guiProjects.get(i);
			
			node = JsonNodeFactory.instance.objectNode();
			
			node.put("id", guiProject.getId());
			node.put("name", guiProject.getName());
			
			mockups.add(node);
		}
		
		dependencies.putArray("innerProcesses");
		dependencies.set("innerProcesses", innerProcesses);
		
		dependencies.putArray("mockups");
		dependencies.set("mockups", mockups);
		
		return dependencies;
	}

	@RequestMapping(value = "{id}/delete/execute", method = RequestMethod.POST)
	public ModelAndView deleteExecute(@PathVariable("id") Integer id) {

		logger.debug("Execute Delete project");

		ModelAndView mav = null;

		try 
		{
			Profile owner = projectService.findProjectOwner(id);
			AccountDTO a = getLoggedUser();
			if (owner != null && owner.getId() == a.getProfile().getId()) 
			{			
				String prjName = projectService.findProject(id).getName();
				
				projectInnerProcessService.deleteAllByProject(id);
				
				projectDatasetService.deleteAllByProject(id);

				projectService.deleteProject(id);				
				
				mav = new ModelAndView("redirect:/user/project/list");

				String activityDescription = "Delete Mashup project: " + prjName;
				activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
						 								AbstractDAO.ActivityType.DELETE_MASHUP_PROJECT.getValue(), 
						 								activityDescription);				
				
			} 
			else 
			{
				Project p = projectService.findProject(id);
				logger.error("Project delete error: logged user:" + a.getEmail() + "not owner of project " + p.getId() + " " + p.getName());
				mav = new ModelAndView("redirect:/user/project/" + id + "/details");
				mav.addObject("project", p);
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.delete.notowner.error");
			}

		} 
		catch (Exception ex) 
		{
			logger.error(ex.getMessage());
			Project p = projectService.findProject(id);
			mav = new ModelAndView("redirect:/user/project/" + id + "/details");
			mav.addObject("project", p);
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.delete.error");
		}

		return mav;
	}

	@RequestMapping(value = "{id}/members", method = RequestMethod.GET)
	public ModelAndView membersList(@PathVariable("id") int projectId) {

		ModelAndView mav = null;

		try {
			ProjectMembersViewModel vm = new ProjectMembersViewModel(projectService, projectId, getLoggedUser().getProfile());
			mav = getDefaultModelAndView("user/project/members");
			mav.addObject("model", vm);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav = new ModelAndView("redirect:/user/project/" + projectId + "/details");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.members.error");

		}
		return mav;
	}

	@RequestMapping(value = "{id}/members/add", method = RequestMethod.POST)
	public ModelAndView membersAdd(@PathVariable int id, @RequestParam String addmembersId) {
		//ModelAndView mav = new ModelAndView("redirect:/user/project/" + id + "/members");
		ModelAndView mav = new ModelAndView("redirect:/user/project/" + id + "/details#projectmembers");
		try {
			logger.debug("addmembersId:" + addmembersId);
			for (String s : addmembersId.split(",")) {
				Integer profileId = Integer.valueOf(s);
				try {
					projectService.memberAdd(id, profileId);
					Profile profile = profileService.findProfile(profileId);					
					Project prj = projectService.findProject(id);
					String activityDescription = "Add member to Mashup project: " + prj.getName() + " (" + profile.getEmail() + ")";
					activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
							 								AbstractDAO.ActivityType.ADD_MEMBER_TO_PROJECT.getValue(), 
							 								activityDescription);				
					
				} catch (DataIntegrityViolationException e) {
					logger.debug(e.getMessage());
				}
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage());
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.workgroup.members.add.error");
			mav.addObject(VmeConstants.MESSAGE_TYPE, VmeConstants.MESSAGE_ERROR);
		}
		return mav;
	}

	@RequestMapping(value = "{id}/members/{profileId}/remove", method = RequestMethod.GET)
	public ModelAndView memberRemove(@PathVariable Integer id, @PathVariable Integer profileId) {
		//ModelAndView mav = new ModelAndView("redirect:/user/project/" + id + "/members");
		ModelAndView mav = new ModelAndView("redirect:/user/project/" + id + "/details#projectmembers");
		try {
			projectService.memberRemove(id, profileId);			
			
			Profile profile = profileService.findProfile(profileId);					
			Project prj = projectService.findProject(id);
			String activityDescription = "Remove member from Mashup project: " + prj.getName() + " (" + profile.getEmail() + ")";
			activityService.activityStreamRecordSave(getLoggedUser().getProfile(), AbstractDAO.ActivityType.REMOVE_MEMBER_FROM_PROJECT.getValue(), activityDescription);				
			
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.remove.project.member.success");
			mav.addObject(VmeConstants.MESSAGE_TYPE, VmeConstants.MESSAGE_SUCCESS);
		} catch (Exception e) {
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.remove.project.member.error");
			mav.addObject(VmeConstants.MESSAGE_TYPE, VmeConstants.MESSAGE_ERROR);
		}
		return mav;
	}

	@RequestMapping(value = "{id}/workgroupprofiles/search.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	PagedSearch<ProfileDTO> workgroupProfilesSearchJson(@PathVariable int id, @RequestParam String searchterm, @RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "20") int pagesize) {

		PagedSearch<ProfileDTO> result = new PagedSearch<ProfileDTO>();
		
		List<ProfileDTO> list = new ArrayList<ProfileDTO>();
		
		try 
		{
			Project prj = projectService.findProject(id);
			
			Profile owner = projectService.findProjectOwner(id);
			
			PagedSearch<ProfileDTO> wgMembers = profileService.pagedSearchByWorkgroup(prj.getWorkgroup().getId(), searchterm, page, pagesize);
			
			List<Profile> members = projectService.findProjectMembers(prj);
			
			boolean isIn = false;
			int isInCounter = 0;
			
			for(int i = 0; i < wgMembers.getResults().size(); i++)
			{
				if(wgMembers.getResults().get(i).getId() == owner.getId())
					continue;
				
				isIn = false;
				
				for(int j = 0; j < members.size(); j++)
				{
					if(wgMembers.getResults().get(i).getId() != members.get(j).getId())
						continue;
						
					isIn = true;
					isInCounter++;
				}
				
				if(!isIn)
					list.add(wgMembers.getResults().get(i));
			}
			
			result.setResults(list);
			result.setTotal(wgMembers.getTotal() - isInCounter);
			
			// .pagedSearch(searchterm, page, pagesize);
		} 
		catch (Exception ex) 
		{
			ex.printStackTrace();
			// result = new ProfilePagedSearchDTO();
		}

		return result;
	}

	
	
	@RequestMapping(value = "/{id}/editor", method = RequestMethod.GET)
	public ModelAndView draw(@PathVariable("id") Integer id, final HttpServletRequest request, HttpServletResponse servletResponse) {

		logger.info("Draw Project View");

		AccountDTO a = getLoggedUser();
		Project prj = projectService.findProject(id);
		
		if(!isLoggedUserAuthorized(a.getProfile(), prj))
		{
			servletResponse.setStatus(401);
			return null;
		}

		if (prj == null) 
		{
			logger.debug("Project not found");

			ModelAndView mav = getDefaultModelAndView("redirect:/user/project/list");

			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.open.error");

			return mav;
		}

		List<Workgroup> workgrs = workgroupService.findByProfile(a.getProfile());

		for (Workgroup w : workgrs) 
		{
			if (w.getId() == prj.getWorkgroup().getId()) 
			{
				List<CkanServer> ckanServer = ckanServerService.findPublicCkanServer();
				
				ModelAndView mav = getDefaultModelAndView("user/project/draw");
				
				mav.addObject("profile", a.getProfile().getEmail());
				mav.addObject("project", prj);
				mav.addObject("ckanServerList", ckanServer);
				
				// IVAN
				// if project is associated with an idea, get suggestions for that idea
				
				boolean fake = Boolean.parseBoolean(PropertyGetter.getProperty("suggestions.fake"));
				boolean enabled = Boolean.parseBoolean(PropertyGetter.getProperty("decisionEngine.enabled"));
				
				IdeaWorkgroup iwg = ideawgService.getIdeaWorkgroupByWorkgroupId(w.getId());
				
				RestTemplate restTemplate;
				ResponseEntity<String> response = null;
				
				List<RemoteArtefact> suggestions = null;
				
				boolean skip = false;
				
				if(enabled && (iwg != null || fake))
				{
					// call the Decision Engine to get suggestions					
					try
					{
						restTemplate = new RestTemplate();
						
						String baseUrl = PropertyGetter.getProperty("decisionEngine.getSuggestedArtefacts.serviceUrl");
						
						MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
						map.add("{\"type\"", "\"bb4idea\"}");
						
						HttpHeaders headers = new HttpHeaders();
					    headers.setContentType(MediaType.APPLICATION_JSON);
					    
					    ObjectNode tmp = JsonNodeFactory.instance.objectNode();
					    tmp.put("type", "bb4idea");
					    
					    HttpEntity<JsonNode> request2 = new HttpEntity<JsonNode>(tmp, headers);
					    
					    response = restTemplate.exchange(baseUrl, HttpMethod.POST, request2, String.class);
					    
					    if(skip || response.getBody().compareTo("") == 0 || response.getBody().startsWith("{") == false)
					    	skip = true;
					}
					catch(Exception e)
					{
						skip = true;
						e.printStackTrace();
					}
					
				    String ids = "";
				    
				    if(!skip)
				    {
					    try 
					    {
							JSONObject artefact = new JSONObject(response.getBody());
							JSONArray arr = artefact.getJSONArray("artefact");
							
							if(arr.length() > 0)
							{
								ids = "[";
								
								ids += "\"" + arr.getJSONObject(0).get("id") + "\"";
								
								for(int i = 1; i < arr.length(); i++)
								{
									ids += ",\"" + arr.getJSONObject(i).get("id") + "\"";
								}
								
								ids += "]";
								
								//ids = URLEncoder.encode(ids, "UTF-8");
							}
						} 
					    catch (JSONException e) 
					    {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} //catch (UnsupportedEncodingException e) 
					    
					    if(ids.compareTo("") != 0)
					    {		
					    	try
							{
								RemoteArtefact[] retArray = wsController.getArtefactsByIdsFromMarketplace(ids);
								
						    	suggestions = artefactController.buildArtefacts(retArray);
							}
							catch(Exception e)
							{
								e.printStackTrace();
							}
					    }
				    }
					
					mav.addObject("suggestions", suggestions);
				}
				else
					mav.addObject("suggestions", null);
				
				// end
				
				return mav;
			}
		}

		ModelAndView mav = getDefaultModelAndView("redirect:/user/project/list");

		mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.notinworkgroup");

		mav.addObject("projectIdContextMenu", id);

		return mav;
	}

    

	@RequestMapping(value = "/{id}/dashboardget.json", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	JsonNode getDashboard(@PathVariable("id") Integer id) {

		String dashboard = projectService.getDashboard(id);
		JsonNode nn = null;
		try {
			if (dashboard != null && !dashboard.isEmpty()) {
				nn = objectMapper.readTree(dashboard);
			}
		} catch (JsonProcessingException e) {
			nn = JsonNodeFactory.instance.textNode(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			nn = JsonNodeFactory.instance.textNode(e.getMessage());
			e.printStackTrace();
		}
		return nn;
	}

	@RequestMapping(headers = { "content-type=application/json" }, value = "{id}/dashboardset.json", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8")
	public void setDashboard(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) throws RestException {

		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				jb.append(line);
			}
		} catch (Exception e) {
			String error = messageSource.getMessage("message.error.requestprocessing", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}

		String requestBody = jb.toString();

		if (requestBody != null && requestBody.length() > 0) 
		{

			String content = null;
			
			try 
			{
				requestBody = requestBody.replaceAll("%","%25");
				
				content = URLDecoder.decode(requestBody, "utf-8");
				
				JsonNode nn = objectMapper.readTree(content);

				ShapeContainer shapeContainer = new ShapeContainer();
				shapeContainer.shapeSelector(content, objectMapper);

				logger.debug("Building workflow schema");
				workflowService.buildWorkflowSchema(id, shapeContainer);

				logger.debug("Saving project into db");
				projectService.setDashboard(id, nn.toString());

			} catch (SelectShapeException e) {
				String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e);

			} catch (WorkflowScriptException e) {
				String error = messageSource.getMessage("message.error.workflow", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e);

			} catch (JsonProcessingException e) {
				String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e);

			} catch (IOException e) {
				String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
				logger.error(error, e);

				throw new RestException(error, e);
			}			
		} else {
			String error = messageSource.getMessage("message.error.requestprocessing", null, Locale.ENGLISH);
			logger.error(error);

			throw new RestException(error);
		}
	}

	/*
	@RequestMapping(value = "{id}/saveresources", method = RequestMethod.POST)
	public @ResponseBody
	void saveResources(@PathVariable("id") Integer id, @RequestParam String stylesheet, @RequestParam(required = false) String javascript,
			HttpServletRequest request) throws RestException {

		logger.debug("Saving resources to file system");

		try {

			Project project = projectService.findProject(id);

			ShapeContainer shapeContainer = new ShapeContainer();
			shapeContainer.shapeSelector(project.getDashboard(), objectMapper);

			renderService.writeStylesheet(id, stylesheet);
			renderService.writeJavascript(id, javascript);

			logger.debug("Creating gadget xml definition");
			String serviceInvokeUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
			socialGadgetService.writeGadget(id, serviceInvokeUrl, shapeContainer);

		} catch (WriteResourceException e) {
			String error = messageSource.getMessage("message.error.writeresource", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		} catch (SelectShapeException e) {
			String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		} catch (WriteGadgetException e) {
			String error = messageSource.getMessage("message.error.writegadget", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}
	}*/

	/*
	 * Vecchio metodo di upload risorse per l'editor versione 1
	@RequestMapping(value = "{id}/addresource/file", method = RequestMethod.POST)
	public @ResponseBody
	GadgetFileResourceDTO addResourceFile(@PathVariable("id") Integer id, MultipartHttpServletRequest request, HttpServletResponse response)
			throws RestException {

		GadgetFileResourceDTO dto = null;

		logger.info("Add resource file ");

		try {
			Iterator<String> itr = request.getFileNames();

			MultipartFile mpf = request.getFile(itr.next());

			if (mpf != null) {

				logger.debug("Import resources from file");

				dto = renderService.writeResources(id, mpf);
			}
		} catch (WriteResourceException e) {

			String error = messageSource.getMessage("message.error.writeresource", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}

		return dto;
	}*/	
	/*
	@RequestMapping(value = "{projectid}/removeresource/{id}/file", method = RequestMethod.GET)
	public @ResponseBody
	void removeResourceFile(@PathVariable("id") Integer id, @PathVariable("projectid") Integer projectId) {

		logger.info("remove resource file ");

		renderService.removeResources(id);
	}
	*/

	/*
	@RequestMapping(value = "{id}/exportworkflow", method = RequestMethod.GET)
	public void exportWorkflow(@PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response, Locale locale) {
		// ModelAndView mav = null;

		Account a = getLoggedUser();
		// Project prj = projectService.findProject(id);
		// mav = getDefaultModelAndView("user/project/draw");
		// mav.addObject("profile", a.getProfile().getEmail());
		// mav.addObject("project", prj);

		if (a != null) {
			try {
				InputStream is = workflowService.getWorkflowArchive(id);

				response.setHeader("Content-disposition", "attachment;filename=" + VmeConstants.WORKFLOW_ARCHIVE_FILENAME_PREFIX + id + ".zip");
				response.setContentType("application/zip");
				response.setContentLength(is.available());

				FileCopyUtils.copy(is, response.getOutputStream());

			} catch (IOException e) {
				String error = messageSource.getMessage("message.error.readworkflow", null, locale);
				logger.error(error, e);
			}
		}
	}*/

	@RequestMapping(value = "{id}/invokechain.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	JsonNode invokeServiceChain(ModelMap model, @PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) throws RestException {
		
		StringBuffer jb = new StringBuffer();
		String line = null;
		
		try 
		{
			BufferedReader reader = request.getReader();
			
			while ((line = reader.readLine()) != null) 
			{
				jb.append(line);
			}
		}
		catch (Exception e) 
		{
			String error = messageSource.getMessage("message.error.requestprocessing", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}
		
		String requestBody = jb.toString();
		
		logger.debug("Invoking Service Chain...");
		
		JsonNode responseNode = null;

		if (requestBody != null && requestBody.length() > 0) 
		{
			String content = null;

			String result = null;
			
			try 
			{
				requestBody = requestBody.replaceAll("%","%25");
				
				content = URLDecoder.decode(requestBody, "utf-8");
				
				JsonNode root = objectMapper.readTree(content);

				// Prende l'identificativo del servizio
				JsonNode serviceIdNode = root.get(VmeConstants.SERVICE_CHAIN_SERVICEID_PARAM);

				String serviceId = objectMapper.convertValue(serviceIdNode, String.class);

				//System.out.println("Service id: " + serviceId);

				// Effettua la selezione degli shape
				logger.debug("Selecting shapes...");
				ShapeContainer shapeContainer = new ShapeContainer();
				shapeContainer.shapeSelector(content, objectMapper);

				logger.debug("Building service chain workflow...");
				// Costruisce il workflow temporaneo che traduce l'invocazione
				// della catena di servizi
				workflowService.buildServiceChainWorkflowSchema(id, shapeContainer, serviceId);

				// Prende i parametri di input dal canvas e li inserisce in una
				// map
				JsonNode parentNode = root.get(VmeConstants.SERVICE_CHAIN_INPUT_PARAMS);

				@SuppressWarnings("unchecked")
				List<Map<String, String>> params = objectMapper.convertValue(parentNode, List.class);

				Map<String, String> formInputs = new HashMap<String, String>();

				for (Map<String, String> formInput : params) {
					formInputs.put(formInput.get("name"), formInput.get("value"));
				}

				// Invoca il workflow temporaneo
				logger.debug("Invoking service chain workflow...");
				String userConnector = RequestContextHolder.currentRequestAttributes().getSessionId();
				
				result = workflowService.invokeServiceChain(id.intValue(), formInputs, userConnector);
				
				// check if output contains __EMML_ERROR
				JsonNode output = objectMapper.readTree(result);
				
				if(output.has("__EMML_ERROR"))
				{
					ObjectNode errNode = JsonNodeFactory.instance.objectNode();
					errNode.put("error", "__MASHUP_EXECUTION_ERROR");
					errNode.set("emmlResponse", output.get("__EMML_ERROR"));
					
					return errNode;
				}
				else
				{
					Iterator<String> keys = output.fieldNames();
					
					String key = "";
					
					while(keys.hasNext())
					{
						key = keys.next();
						
						if(output.get(key).has("__EMML_ERROR"))
						{
							ObjectNode errNode = JsonNodeFactory.instance.objectNode();
							errNode.put("error", "__MASHUP_EXECUTION_ERROR");
							errNode.set("emmlResponse", output.get(key).get("__EMML_ERROR"));
							
							return errNode;
						}
					}
				}
				
				JsonNode chainInvokeResult = output.get(serviceId.replaceAll("[^A-Za-z0-9]", "_"));//.replace('.', '_').replace('-', '_'));
				
				// Mette in sessione il risultato json dell'invocazione
				model.addAttribute("lastinvokeresult", chainInvokeResult);

				// Trasforma il json risultato in albero jstree
				logger.debug("Convert json result to jstree...");
				JsTreeNode jsTreeDest = FormatConverter.jsonToTreeStruct(chainInvokeResult, VmeConstants.JSTREE_ROOTNODENAME);
				
				responseNode = (ObjectNode) new ObjectMapper().convertValue(jsTreeDest, JsonNode.class);
				
			} 
			catch (UserNotAuthorizedException e) 
			{
				//String warning = messageSource.getMessage("message.error.usernotauthorized", null, Locale.ENGLISH);
				//logger.warn(warning);
				String serverUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
				//throw new RestException(HttpStatus.UNAUTHORIZED, serverUrl + "/oauth/" + e.getServiceId() + "/authorize");
				throw new RestException(HttpStatus.UNAUTHORIZED, serverUrl + "/oauth/" + e.getServiceId() + "/" + e.getOperationId() + "/authorize"); // IVAN: modified to allow auth at operation level

			} 
			catch (SelectShapeException e) 
			{
				ObjectNode errNode = JsonNodeFactory.instance.objectNode();
				
				errNode.put("error", "__MASHUP_EXECUTION_ERROR");
				errNode.put("message", e.getMessage());
				
				return errNode;

			} 
			catch (WorkflowScriptException e) 
			{
				ObjectNode errNode = JsonNodeFactory.instance.objectNode();
				
				errNode.put("error", "__MASHUP_EXECUTION_ERROR");
				errNode.put("message", e.getMessage());
				
				return errNode;
			} 
			catch (InvokeWorkflowException e) 
			{
				ObjectNode errNode = JsonNodeFactory.instance.objectNode();
				
				errNode.put("error", "__MASHUP_EXECUTION_ERROR");
				errNode.put("message", e.getMessage());
				
				return errNode;

			} 
			catch (JsonProcessingException e) 
			{
				logger.error(e.getMessage());
				
				String json = "";
				
				try 
				{
					json = XML.toJSONObject(result).toString();
					
					JsonNode _errNode = objectMapper.readTree(json); Json.prettyPrint(_errNode);
					
					ObjectNode errNode = JsonNodeFactory.instance.objectNode();
					
					errNode.put("error", "__MASHUP_EXECUTION_ERROR");
					errNode.put("message", ((ArrayNode)_errNode.get("error").get("errorMessage")).get(1).asText());
					
					return errNode;
				} 
				catch (Exception e1) 
				{
					ObjectNode errNode = JsonNodeFactory.instance.objectNode();
					
					errNode.put("error", "__MASHUP_EXECUTION_ERROR");
					errNode.put("message", e.getMessage());
					
					return errNode;
				}

			} 
			catch (IOException e) 
			{
				ObjectNode errNode = JsonNodeFactory.instance.objectNode();
				
				errNode.put("error", "__MASHUP_EXECUTION_ERROR");
				errNode.put("message", e.getMessage());
				
				return errNode;
			}
		}
		return responseNode;
	}

	@RequestMapping(value = "/detail/nodetree.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody
	JsonNode getTreeNodeDetail(@ModelAttribute("lastinvokeresult") JsonNode tree, @RequestParam("nodepath") String nodePath, HttpServletRequest request)
			throws RestException {

		JsonNode columnName = null;

		try {
			columnName = JsonUtils.getSubtreeNodeMap(tree, nodePath, objectMapper);
		} catch (JsonProcessingException e) {
			String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}

		return columnName;
	}

	/*
	@RequestMapping(value = "{id}/invokeworkflow", method = RequestMethod.POST)
	public ModelAndView invokeWorkflow(@PathVariable("id") Integer projectId, @RequestParam Map<String, String> formInputs, HttpServletRequest request) {
		ModelAndView mav = null;

		try {
			try {
				logger.debug("Invoking workflow...");
				String userConnector = RequestContextHolder.currentRequestAttributes().getSessionId();
				String result = workflowService.invokeWorkflow(projectId.intValue(), formInputs, userConnector);

				logger.debug("invoke result" + result);

				JsonNode output = objectMapper.readTree(result);

				//logger.debug("Calling render to get html...");
				//String invokeResultHtml = renderService.renderContainer(projectId, output);

				Project project = projectService.findProject(projectId);

				ShapeContainer shapeContainer = new ShapeContainer();
				shapeContainer.shapeSelector(project.getDashboard(), objectMapper);

				String serviceInvokeUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();

				String htmlContent = socialGadgetService.getHtmlContentPreview(projectId, serviceInvokeUrl, shapeContainer, invokeResultHtml);

				String css = renderService.readStylesheet(projectId);
				String js = renderService.readJavascript(projectId);
				List<GadgetFileResource> gadgetResources = gadgetResourceService.getAllByProject(projectId, null);

				mav = getDefaultModelAndView("/user/project/preview");

				Project prj = projectService.findProject(projectId);
				mav.addObject("project", prj);
				mav.addObject("htmlresult", htmlContent);
				mav.addObject("cssresult", css);
				mav.addObject("jsresult", js);
				mav.addObject("gadgetresources", gadgetResources);

			} catch (UserNotAuthorizedException e) {
				String warning = messageSource.getMessage("message.error.usernotauthorized", null, Locale.ENGLISH);
				logger.warn(warning);

				request.getSession().setAttribute("projectId", projectId);
				mav = getDefaultModelAndView("redirect:" + "/oauth/" + e.getServiceId() + "/authorize");
				return mav;

			} catch (InvokeWorkflowException e) {
				String error = messageSource.getMessage("message.error.invokeworkflow", null, Locale.ENGLISH);
				throw new Exception(error, e);

			} catch (JsonProcessingException e) {
				String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
				throw new Exception(error, e);

			} catch (IOException e) {
				String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
				throw new Exception(error, e);

			} catch (RenderEngineException e) {
				String error = messageSource.getMessage("message.error.renderengine", null, Locale.ENGLISH);
				throw new Exception(error, e);

			} catch (ReadResourceException e) {
				String error = messageSource.getMessage("message.error.readresource", null, Locale.ENGLISH);
				throw new Exception(error, e);

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

			mav = getDefaultModelAndView("redirect:/user/project/" + projectId + "/editor");
			mav.addObject(VmeConstants.MESSAGE_TEXT, e.getMessage());
		}

		return mav;
	}
*/
	@RequestMapping(value = "/jstree.json", method = RequestMethod.POST)
	public @ResponseBody JsonNode convertToJsTree(@RequestBody String json, HttpServletRequest request)
	{
		JsonNode output = null;
		try 
		{
			output = objectMapper.readTree(URLDecoder.decode(json, "UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		JsTreeNode jsTreeDest = null;
		try {
			jsTreeDest = FormatConverter.jsonToTreeStructKoMeta(output, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JsonNode responseNode = (ObjectNode) new ObjectMapper().convertValue(jsTreeDest, JsonNode.class);
		
		logger.debug(responseNode.toString());
		
		return responseNode;
	}
	
	@RequestMapping(value = "{id}/invokeemml.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody JsonNode invokeEmml(ModelMap model, @PathVariable("id") Integer projectId, @RequestParam Map<String, String> formInputs, HttpServletRequest request) throws RestException {		
		JsonNode responseNode = null;
		//try {
		String result = null;
		
		try 
		{				
			logger.debug("Invoking workflow..." + formInputs.toString());
			String userConnector = RequestContextHolder.currentRequestAttributes().getSessionId();
			
			result = workflowService.invokeWorkflow(projectId.intValue(), formInputs, userConnector);
			
			//System.out.println("invoke result: " + result);

			JsonNode output = objectMapper.readTree(result);
			
			// check if output contains __EMML_ERROR
			if(output.has("__EMML_ERROR"))
			{
				ObjectNode errNode = JsonNodeFactory.instance.objectNode();
				errNode.put("error", "__MASHUP_EXECUTION_ERROR");
				errNode.set("emmlResponse", output.get("__EMML_ERROR"));
				
				return errNode;
			}
			else
			{
				Iterator<String> keys = output.fieldNames();
				
				String key = "";
				
				while(keys.hasNext())
				{
					key = keys.next();
					
					if(output.get(key).has("__EMML_ERROR"))
					{
						ObjectNode errNode = JsonNodeFactory.instance.objectNode();
						errNode.put("error", "__MASHUP_EXECUTION_ERROR");
						errNode.set("emmlResponse", output.get(key).get("__EMML_ERROR"));
						
						return errNode;
					}
				}
			}			
			
			if(formInputs.containsKey("parentNodePath")){
				String parentBindFullPathNode =  formInputs.get("parentNodePath");										

				try {
					output = JsonUtils.getSubtreeNodeMapKo(output, parentBindFullPathNode, objectMapper);
				} catch (JsonProcessingException e) {
					String error = messageSource.getMessage("message.error.jsonprocessing", null, Locale.ENGLISH);
					logger.error(error, e);

					throw new RestException(error, e, result);
				}													
			}
			
			//JsonNode chainInvokeResult = objectMapper.readTree(result).get(serviceId.replaceAll("[^A-Za-z0-9]", "_"));//.replace('.', '_').replace('-', '_'));

			// Mette in sessione il risultato json dell'invocazione
			model.addAttribute("lastinvokeresult", output);

			// Trasforma il json risultato in albero jstree
			logger.debug("Convert json result to jstree...");
			JsTreeNode jsTreeDest = FormatConverter.jsonToTreeStructKoMeta(output, null);
			
			responseNode = (ObjectNode) new ObjectMapper().convertValue(jsTreeDest, JsonNode.class);


		} 
		catch (UserNotAuthorizedException e) 
		{
			//String warning = messageSource.getMessage("message.error.usernotauthorized", null, Locale.ENGLISH);
			//logger.warn(warning);
			String serverUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
			throw new RestException(HttpStatus.UNAUTHORIZED, serverUrl + "/oauth/" + e.getServiceId() + "/" + e.getOperationId() + "/authorize"); // IVAN: modified to allow auth at operation level
		
		} 
		catch (InvokeWorkflowException e) 
		{
			ObjectNode errNode = JsonNodeFactory.instance.objectNode();
			
			errNode.put("error", "__MASHUP_EXECUTION_ERROR");
			errNode.put("message", e.getMessage());
			
			return errNode;

		} 
		catch (JsonProcessingException e) 
		{
			logger.error(e.getMessage());
			
			String json = "";
			
			try 
			{
				json = XML.toJSONObject(result).toString();
				
				JsonNode _errNode = objectMapper.readTree(json); Json.prettyPrint(_errNode);
				
				ObjectNode errNode = JsonNodeFactory.instance.objectNode();
				
				errNode.put("error", "__MASHUP_EXECUTION_ERROR");
				errNode.put("message", ((ArrayNode)_errNode.get("error").get("errorMessage")).get(1).asText());
				
				return errNode;
			} 
			catch (Exception e1) 
			{
				ObjectNode errNode = JsonNodeFactory.instance.objectNode();
				
				errNode.put("error", "__MASHUP_EXECUTION_ERROR");
				errNode.put("message", e.getMessage());
				
				return errNode;
			}

		} 
		catch (IOException e) 
		{
			ObjectNode errNode = JsonNodeFactory.instance.objectNode();
			
			errNode.put("error", "__MASHUP_EXECUTION_ERROR");
			errNode.put("message", e.getMessage());
			
			return errNode;
		}

			
		return responseNode;
	}			
	
	@RequestMapping(value = "{id}/export", method = RequestMethod.GET)
	public void export(@PathVariable("id") Integer id, HttpServletResponse response, Locale locale) {

		OutputStream serializedBean = projectService.exportProject(id);

		ByteArrayOutputStream serializedBeanStream = (ByteArrayOutputStream) serializedBean;

		InputStream inputStream = new ByteArrayInputStream(serializedBeanStream.toByteArray());

		response.setHeader("Content-disposition", "attachment;filename=" + VmeConstants.PROJECT_EXPORT_FILENAME_PREFIX + id + ".xml");
		response.setContentType("application/xml");
		try {
			response.setContentLength(inputStream.available());
			FileCopyUtils.copy(inputStream, response.getOutputStream());

			Project prj = projectService.findProject(id);
			String activityDescription = "Export Mashup project: " + prj.getName();
			activityService.activityStreamRecordSave(getLoggedUser().getProfile(),
					 								AbstractDAO.ActivityType.EXPORT_MASHUP_PROJECT.getValue(), 
					 								activityDescription);				
			
			
						
			
		} catch (IOException e) {
			String error = messageSource.getMessage("message.error.exportgadget", null, locale);
			logger.error(error, e);
		}
	}
	
	@RequestMapping(value = "{id}/publish", method = RequestMethod.GET)
	public @ResponseBody ObjectNode publish(@PathVariable("id") Integer id, HttpServletRequest request) 
	{
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		try 
		{
			return wsController.publishMashupProject(id);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			node.put("error", true);
			node.put("message", e.getMessage());
		}
		
		return node;
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/import", method = RequestMethod.GET)
	public ModelAndView importProject() {
		ModelAndView mav;

		AccountDTO a = getLoggedUser();
		List<Workgroup> wgList = workgroupService.findByProfile(a.getProfile());

		if (wgList == null || wgList.isEmpty()) {

			mav = getDefaultModelAndView("redirect:/user/project/list");
			mav.addObject(VmeConstants.MESSAGE_TEXT, "message.project.add.workgroupasscoaition.error");

		} else {

			mav = getDefaultModelAndView("user/project/import");
			mav.addObject("workgroupList", wgList);
		}

		return mav;
	}

	@RequestMapping(value = "/import/execute", method = RequestMethod.POST)
	public ModelAndView importExecute(@ModelAttribute ImportXmlViewModel viewmodel, BindingResult result) {

		logger.info("Execute add xml project definition from File ");

		ModelAndView mav;

		AccountDTO a = getLoggedUser();

		try {

			MultipartFile mpf = viewmodel.getXmlFile().get(0);

			projectService.importProject(mpf, a.getProfile(), viewmodel.getName(), viewmodel.getDescription(), viewmodel.getWorkgroupId());

			mav = getDefaultModelAndView("redirect:/user/project/list");

		} catch (ImportProjectException ex) {
			logger.error("Exception: " + ex.getMessage());

			List<Workgroup> wgList = workgroupService.findByProfile(a.getProfile());

			mav = getDefaultModelAndView("user/project/import");
			mav.addObject("workgroupList", wgList);
			mav.addObject(VmeConstants.MESSAGE_TEXT, ex.getMessage());
		}

		return mav;
	}

	@RequestMapping(value = "{id}/buildinputform", method = RequestMethod.GET)
	public ModelAndView buildInvokeInputForm(@PathVariable("id") Integer id, HttpServletRequest request) throws RestException {

		ModelAndView mav = getDefaultModelAndView("user/project/invoke_input_form");

		Project project = projectService.findProject(id);

		ShapeContainer shapeContainer = new ShapeContainer();
		try {
			shapeContainer.shapeSelector(project.getDashboard(), objectMapper);

		} catch (SelectShapeException e) {
			String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}

		List<InputShapeDTO> inputList = new ArrayList<InputShapeDTO>();

		for (ShapeDTO shape : shapeContainer.getInputs().values()) {

			InputShapeDTO input = (InputShapeDTO) shape;

			inputList.add(input);
		}

		mav.addObject("projectId", id);
		mav.addObject("inputShape", inputList);

		return mav;
	}
	
	
	/*@RequestMapping(value = "{id}/getinputshapes", method = RequestMethod.GET)
	public @ResponseBody List<InputShapeDTO> getInputShapes(@PathVariable("id") Integer id, HttpServletRequest request) throws RestException {
		
		List<InputShapeDTO> inputList = null;
		try {
			inputList = readInputShapesList(id);
			
		} catch (SelectShapeException e) {
			String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}

		return inputList;
	}*/
	
	@RequestMapping(value = "{id}/getbindinginputparams.json", method = RequestMethod.GET)
	public @ResponseBody JsonNode getBindingInputParams(@PathVariable("id") Integer id, HttpServletRequest request) throws RestException {
		
		JsonNode responseNode = null;
		
		BindingInputParams bindingInputParams = new BindingInputParams();
		
		List<BindingInputField> inputFieldList = null;
		
		
		try {
			List<InputShapeDTO> inputList = this.projectService.readInputShapesList(id);
			
			inputFieldList = new ArrayList<BindingInputField>();
			
			for(InputShapeDTO inputShape : inputList){
				BindingInputField inputField = new BindingInputField(inputShape.getId(), inputShape.getUserData().getLabel(), projectService.findProject(id).getName());
				inputField.setDefaultValue(inputShape.getUserData().getDefaultValue());
				inputField.setIsConstant(inputShape.getUserData().isIsConstant());
				inputFieldList.add(inputField);
			}
			
			bindingInputParams.setInputParams(inputFieldList);
			
			String actionUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()+"/gadget/"+id+"/emmlproxy.json";
			
			bindingInputParams.setActionUrl(actionUrl);
			
			JsonNode inputParamsNode = (ObjectNode) new ObjectMapper().convertValue(bindingInputParams, JsonNode.class);
			
			// Trasforma il json risultato in albero jstree
			//logger.debug("Convert json result to jstree...");
			//JsTreeNode jsTreeDest = FormatConverter.jsonToTreeStruct(inputParamsNode, VmeConstants.JSTREE_ROOTNODENAME);

			//responseNode = objectMapper.convertValue(jsTreeDest, JsonNode.class);
			
			responseNode = inputParamsNode;
			
		} catch (SelectShapeException e) {
			String error = messageSource.getMessage("message.error.selectshape", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
//		} catch (JsonParseException e) {
//			
//			throw new RestException(e);
//		} catch (IOException e) {
//			
//			throw new RestException(e);
		}

		return responseNode;
	}
	
	@RequestMapping(value = "/getUnreadComments.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Integer getUnreadComments(final HttpServletRequest request) {
		logger.info("get number on Unread Comments (json): getUnreadComments()");
		AccountDTO a = getLoggedUser();
		int nuc = 0;
		List <UnreadComment> uc = commentService.getUnreadCommentsByProfileId(a.getProfile().getId());
		if (uc != null) {
			nuc = uc.size();
		}
		return nuc;
	}	

	@RequestMapping(value = "{id}/getProjectUnreadComments.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Integer getProjectUnreadComments(@PathVariable("id") Integer idProject) {
		logger.info("get number on Unread Comments (json): getUnreadComments()");
		//String idProjectStr = (String) request.getParameter("idProject");
		//int idProject = Integer.parseInt(idProjectStr);
		AccountDTO a = getLoggedUser();
		int nuc = 0;
		List <UnreadComment> uc = commentService.getUnreadCommentsByProjectIdProfileId(idProject, a.getProfile().getId());
		if (uc != null) {
			nuc = uc.size();
		}
		return nuc;
	}		
	
	@RequestMapping(value = "addxsl/file", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody List<String> addXslFile(MultipartHttpServletRequest request)
			throws RestException {

		logger.info("Add xsl file ");

		List<String> outList = null; 
		
		try {
			Iterator<String> itr = request.getFileNames();

			MultipartFile mpf = request.getFile(itr.next());

			if (mpf != null) {

				logger.debug("Import resources from file");

				workflowService.writeXsl(mpf);
				
				outList = this.listXslFile();
			}
		} catch (WriteResourceException e) {
			
			logger.error(e.getMessage(), e);

			throw new RestException(e);
		}
		
		return outList;
	}	
	
	@RequestMapping(value = "listxsl/file.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<String> listXslFile(){
		Collection<File> list = workflowService.getXslFileList();
		
		List<String> outList = new ArrayList<String>(); 
		
		for(File file : list){
			outList.add(file.getName());
		}
		
		return outList;
	}	
}