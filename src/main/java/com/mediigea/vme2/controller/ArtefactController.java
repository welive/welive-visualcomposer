package com.mediigea.vme2.controller;

import it.eng.PropertyGetter;
import it.eng.metamodel.definitions.BuildingBlock;
import it.eng.model.Artefact;
import it.eng.model.RemoteArtefact;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.marketplace.dto.MashupDTO;
import com.mediigea.vme2.service.GuiEditorService;
import com.mediigea.vme2.service.IdeaWorkgroupService;
import com.mediigea.vme2.service.ODSDatasetService;
import com.mediigea.vme2.service.OpenDataService;
import com.mediigea.vme2.service.ServiceCatalogExtService;
import com.mediigea.vme2.service.ServiceCatalogService;
import com.mediigea.vme2.util.VmeConstants;
import com.mediigea.vme2.dao.ServiceCatalogDAO;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.IdeaWorkgroup;
import com.mediigea.vme2.entity.ODSDataset;
import com.mediigea.vme2.entity.OpenDataResource;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.entity.ServiceCatalogExt;

@Controller
@RequestMapping(value = "/user/mp")
public class ArtefactController extends BaseController
{
	@Autowired
	private ServiceCatalogService catalogService;
	
	@Autowired
	private ServiceCatalogDAO serviceCatalogDAO;
	
	@Autowired
	private ServiceCatalogExtService catalogExtService;
	
	@Autowired
	private OpenDataService openDataService;
	
	@Autowired
	private GuiEditorService guiEditorService;
	
	@Autowired
	private IdeaWorkgroupService ideawgService;
	
	@Autowired
	private ODSDatasetService odsDatasetService;
	
	@Autowired
	private WebServiceController wsController;
	
	@Autowired
	private OpenDataController opendataController;
	
	private static final Logger logger = LoggerFactory
			.getLogger(ServiceCatalogController.class);
	/*
	public class JaxbCharacterEscapeHandler implements CharacterEscapeHandler {

		public void escape(char[] buf, int start, int len, boolean isAttValue, Writer out) throws IOException {

			for (int i = start; i < start + len; i++) {
				char ch = buf[i];
				out.write(ch);
			}
		}
	}*/
	
	// convert InputStream to String
	private static String getStringFromInputStream(InputStream is) 
	{ 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString(); 
	}

	@RequestMapping(value = "/add/artefact", method = RequestMethod.GET)
	public ModelAndView addMPartefact() {

		logger.info("Add artefact View");
		
		ModelAndView mav = getDefaultModelAndView("user/mp/addartefact");

		return mav;
	}
	
	@RequestMapping(value = "/gadget/{id}/publish", method = RequestMethod.GET)
	public ModelAndView publishMPartefact(@PathVariable("id") Integer id) 
	{
		logger.info("Publish artefact View");
		
		AccountDTO a = getLoggedUser();
		MashupDTO mashupDTO = new MashupDTO();
		mashupDTO.setMockupId(id);
		
		ModelAndView mav = getDefaultModelAndView("user/mp/publishartefact");
		mav.addObject("mashup", mashupDTO);
		mav.addObject("loggeduser", a);
		
		// IVAN
		
		boolean canPublishEnabled = Boolean.parseBoolean(PropertyGetter.getProperty("gadget.canPublish.enabled"));
		
		boolean canPublish = false;		
		boolean authorized = false;
		
		if(canPublishEnabled)
		{
			// call marketplace service to check if logged user is authorized to publish
			RestTemplate restTemplate = new RestTemplate();
			
			String baseUrl = PropertyGetter.getProperty("mkp.canPublish.serviceUrl");
			
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("username", a.getEmail());
		    
		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		    
		    HttpEntity<MultiValueMap<String, String>> request2 = new HttpEntity<MultiValueMap<String, String>>(map,headers);
		    
		    ResponseEntity<String> response = restTemplate.exchange(baseUrl, HttpMethod.POST, request2, String.class);
		    
		    //System.out.println(response.getBody());
		    
		    try 
		    {
				JSONObject json = new JSONObject(response.getBody());
				
				authorized = json.getBoolean("can_publish");
			} 
		    catch (JSONException e) 
		    {
				logger.error("Error parsing response JSON");
				e.printStackTrace();
			}	
				
			IdeaWorkgroup iwg;
			
			Profile owner;
			GuiProject p;
			
			p = guiEditorService.findProject(mashupDTO.getMockupId());
				
			owner = guiEditorService.findProjectOwner(p.getId());
			
			iwg = ideawgService.getIdeaWorkgroupByWorkgroupId(p.getWorkgroup().getId());
			
			// if project is associated with an idea...
			if(iwg != null)
			{
				// user can publish if is project owner
				if(owner.getEmail().compareTo(a.getEmail()) == 0)
					canPublish = true;
				else
					canPublish = false;
			}
			else
			{
				canPublish = authorized;			
			}
			
			if(!canPublish)
			{
				mav = new ModelAndView("redirect:/user/gui/list");
				mav.addObject(VmeConstants.MESSAGE_TEXT, "message.mp.cantpublish");
			}
		}
		
		// end

		return mav;
	}
	
	@RequestMapping(value = "/submit/artefact", method=RequestMethod.POST)
	//public String submitMPartefact(@RequestParam(value="artefact_name") String name, @RequestParam(value="artefact_url") String url) 
	public @ResponseBody ObjectNode submitMPartefact(@RequestBody Artefact artefact, HttpServletRequest request) 
	{
		logger.info("Submit artefact");
		
		String name = artefact.getName();
		String url = artefact.getUrl();
		String type = artefact.getType();
		String linkPage = artefact.getLinkPage();
		String description = artefact.getDescription();
		int wsmodelId = artefact.getWsmodelId();
		BuildingBlock metamodel = artefact.getMetamodel();
		
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		
		ServiceCatalogExt service = new ServiceCatalogExt();
		service.setFrom_marketplace(true);
		service.setEndpoint_url(url);
		service.setLink_page(linkPage);
		service.setDescription(description);
		service.setWsmodel_id(wsmodelId);
		
		try 
		{			
			AccountDTO a = getLoggedUser();
			
			ServiceCatalog sc;
			
			if(type.compareTo("ODATA") == 0)
			{
				/*
				OpenDataResource od = openDataService.addFromUrl(name, url, a.getProfile(), ';');
				
				node.put("message", "Artefact added to Catalog");
				node.put("artid", Long.toString(od.getId()));
				*/
				
				int odId = opendataController.importODSDataset(artefact.geteId());
				
				if(odId != -1 && odId != -2)
				{
					ODSDataset dataset = odsDatasetService.findDataset(odId);
					dataset.setMkpId(wsmodelId);
					
					odsDatasetService.updateODSDataset(dataset);
					
					node.put("message", "Artefact added to Catalog");
					node.put("type", "opendata");
					node.put("artid", Integer.toString(odId));
				}
				else
				{
					node.put("message", "Artefact is not a valid service or opendata");
					node.put("artid", "null");
				}
				
				return node;
			}
			
			if(metamodel != null)
			{
				sc = catalogService.addRESTFromMetamodel(metamodel, a.getProfile());
				
				service.setId(sc.getId());
				catalogExtService.add(service);
				
				node.put("message", "Artefact added to Catalog");
				node.put("type", "service");
				node.put("artid", Long.toString(sc.getId()));
				
				return node;
			}
			else
			{
				if(type.compareTo("REST") == 0)
				{
					URL wadlUrl = new URL(url);
					InputStream in = wadlUrl.openStream();
					String wadl = IOUtils.toString(in, "UTF-8");
		
					sc = catalogService.addFromWadl(name, wadl,	a.getProfile());
		
					in.close();
					
					service.setId(sc.getId());
					catalogExtService.add(service);
					
					node.put("message", "Artefact added to Catalog");
					node.put("type", "service");
					node.put("artid", Long.toString(sc.getId()));
				}
				else if(type.compareTo("SOAP") == 0)
				{
					sc = catalogService.addWsdlService(url, a.getProfile());
					sc.setName(name);
					catalogService.updateService(sc);
					
					service.setId(sc.getId());
					catalogExtService.add(service);
					
					node.put("message", "Artefact added to Catalog");
					node.put("type", "service");
					node.put("artid", Long.toString(sc.getId()));
				}
			}			
		} 
		catch (Exception ex)
		{
			logger.error("Exception: " + ex.getMessage());
			
			ex.printStackTrace();

			node.put("message", "Artefact is not a valid service or opendata");
			node.put("artid", "null");
		}

		return node;
	}
	
	public List<RemoteArtefact> buildArtefacts(RemoteArtefact[] retArray)
	{
		List<RemoteArtefact> retList = new ArrayList<RemoteArtefact>();
		
		String marketplaceServiceTypeIdREST = PropertyGetter.getProperty("mkp.serviceTypeId.REST");
		String marketplaceServiceTypeIdSOAP = PropertyGetter.getProperty("mkp.serviceTypeId.SOAP");
		String marketplaceServiceTypeIdODATA = PropertyGetter.getProperty("mkp.serviceTypeId.ODATA");
		
		try
		{
			String hql = "SELECT s FROM ServiceCatalog s JOIN FETCH s.profile p WHERE s.id IN "
					+ "(SELECT e.id FROM ServiceCatalogExt e WHERE e.wsmodel_id=:wsmodelId) AND "
					+ "(p.id=:profileId OR s.visibility=" + VmeConstants.SERVICECATALOG_VISIBILITY_WORKGROUP + ")";
			
			String od_hql = " SELECT distinct s FROM ODSDataset s JOIN FETCH s.profile p join p.profileWorkgroups pw "
					+ " WHERE pw.pk.workgroup.id IN (SELECT pw2.pk.workgroup.id FROM ProfileWorkgroup pw2 WHERE pw2.pk.profile.id = :pId)"
					+ " AND ( s.profile.id = :pId OR s.visibility =" + VmeConstants.SERVICECATALOG_VISIBILITY_WORKGROUP + " )"
					+ " AND s.odsDatasetId = :odsId";
			
			HashMap<String, Object> parameters = new HashMap<String, Object>();
			
			int profileId = getLoggedUser().getProfile().getId();
			
			List<ServiceCatalog> findList;
			
			List<ODSDataset> odFindList;
			
			for(int i = 0; i < retArray.length; i++)
			{
				if(retArray[i].getTypeId() == Long.parseLong(marketplaceServiceTypeIdODATA))
				{
					if(retArray[i].getHasmapping() == false)
						continue;
					
					parameters.clear();	
					parameters.put("pId", profileId);
					parameters.put("odsId", retArray[i].geteId());
					
					odFindList = odsDatasetService.findAll(od_hql, parameters);
					
					if(odFindList.size() == 0)
					{
						retArray[i].setServiceCatalogId("null");
					}
					else
					{
						retArray[i].setServiceCatalogId(Long.toString(odFindList.get(0).getId()));
					}
				}
				else
				{
					parameters.clear();	
					parameters.put("profileId", profileId);
					parameters.put("wsmodelId", (int)retArray[i].getArtefactId());
					
					findList = catalogService.findAll(hql, parameters);
				
					if(findList.size() == 0)
					{
						retArray[i].setServiceCatalogId("null");
					}
					else
					{
						retArray[i].setServiceCatalogId(Long.toString(findList.get(0).getId()));
					}
				}
				
				//retArray[i].setRating(3);
				if(retArray[i].getLinkImage().compareTo("") != 0)
					retArray[i].setLinkImage(PropertyGetter.getProperty("mkp.baseUrl") + retArray[i].getLinkImage());
				retArray[i].setLink(PropertyGetter.getProperty("mkp.artefact.pageUrl") + "" + retArray[i].getArtefactId());
				
				if(retArray[i].getTypeId() == Long.parseLong(marketplaceServiceTypeIdREST))
					retArray[i].setType("REST");
				else if(retArray[i].getTypeId() == Long.parseLong(marketplaceServiceTypeIdSOAP))
					retArray[i].setType("SOAP");
				else if(retArray[i].getTypeId() == Long.parseLong(marketplaceServiceTypeIdODATA))
					retArray[i].setType("ODATA");
				else
					continue;
				
				retList.add(retArray[i]);
			}
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return retList;
	}
	
	@RequestMapping("/artefacts/types/{types}/search/{search}")
	@ResponseBody
	public List<RemoteArtefact> getArtefacts(@PathVariable String types, @PathVariable String search)
	{
		RemoteArtefact[] retArray;
		List<RemoteArtefact> retList = new ArrayList<RemoteArtefact>();
		
		try
		{
			retArray = wsController.getArtefactsFromMarketplace(types, search, getLoggedUser().getProfile().getPilotId());
			
			retList = buildArtefacts(retArray);
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return retList;
	}
	
	@RequestMapping("/artefacts/types/{types}/all")
	@ResponseBody
	public List<RemoteArtefact> getAllArtefacts(@PathVariable String types)
	{
		RemoteArtefact[] retArray;
		List<RemoteArtefact> retList = new ArrayList<RemoteArtefact>();
		
		try
		{
			retArray = wsController.getArtefactsFromMarketplace(types, null, getLoggedUser().getProfile().getPilotId());

			retList = buildArtefacts(retArray);
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return retList;
	}
}
