package com.mediigea.vme2.controller;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.entity.OpenDataResource;
import com.mediigea.vme2.exceptions.RestException;
import com.mediigea.vme2.service.OpenDataService;
import com.mediigea.vme2.util.FormatConverter;
import com.mediigea.vme2.util.JsTreeNode;
import com.mediigea.vme2.util.JsonUtils;
import com.mediigea.vme2.util.VmeConstants;

@Controller
@RequestMapping(value = "/public/opendata")
public class OpenDataPublicController extends BaseController {

	@Autowired
	private ObjectMapperExt objectMapper;

	@Autowired
	private OpenDataService openDataService;

	@Autowired
	private MessageSource messageSource;
	
	@Autowired 
	private OpenDataController opendataController;

	private static final Logger logger = LoggerFactory
			.getLogger(OpenDataPublicController.class);

	/**
	 * Public method to read open data resources
	 * 
	 * @param odataId
	 * @param request
	 * @param response
	 * @return
	 * @throws RestException
	 */
	@RequestMapping(value = "{id}/query/{query}/getastree.json", produces = "application/json")
	public @ResponseBody
	JsonNode getAsTree(ModelMap model, @PathVariable("id") Integer odataId, @PathVariable("query") String queryText)
			throws RestException {

		JsonNode responseNode;

		try {
			logger.debug("Getting open data resource [id]: " + odataId);
			
			/*
			// Legge la risorsa dal database
			OpenDataResource odata = openDataService.getById(odataId);

			// Converte la stringa recuperata dal db in json
			responseNode = JsonUtils.stringToJsonNode(odata.getContent(),
					objectMapper);
			*/
			
			// IVAN
			// invoke Query Mapper to get requested data			
			responseNode = JsonUtils.stringToJsonNode(opendataController.queryResource(odataId, queryText, null),
					objectMapper);

			model.addAttribute("lastinvokeresult", responseNode);
			
			// Converte il json in una struttura di tipo tree
			JsTreeNode jsTreeDest = FormatConverter.jsonToTreeStruct(
					responseNode, VmeConstants.JSTREE_ROOTNODENAME);

			responseNode = (ObjectNode) new ObjectMapper().convertValue(
					jsTreeDest, JsonNode.class);

		} catch (IOException e) {
			String error = messageSource.getMessage(
					"message.error.jsonprocessing", null, Locale.ENGLISH);
			logger.error(error, e);

			throw new RestException(error, e);
		}

		return responseNode;
	}

	@RequestMapping(value = "{id}/read.json", produces = "application/json")
	public @ResponseBody
	JsonNode read(@PathVariable("id") Integer odataId) throws RestException {

		logger.debug("Getting open data resource [id]: " + odataId);

		// Legge la risorsa dal database
		OpenDataResource odata = openDataService.getById(odataId);

		return JsonUtils.stringToJsonNode(odata.getContent(), objectMapper);
	}
}