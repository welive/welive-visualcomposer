
package com.mediigea.vme2.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mediigea.vme2.dto.AccountDTO;
import com.mediigea.vme2.dto.BlackboxDTO;
import com.mediigea.vme2.dto.InnerProcessDTO;
import com.mediigea.vme2.entity.Account;
import com.mediigea.vme2.exceptions.RestException;
import com.mediigea.vme2.exceptions.SelectShapeException;
import com.mediigea.vme2.exceptions.UsedBlackboxException;
import com.mediigea.vme2.exceptions.UsedInnerProcessException;
import com.mediigea.vme2.service.BlackboxService;
import com.mediigea.vme2.service.InnerProcessService;
import com.mediigea.vme2.service.ProjectService;
import com.mediigea.vme2.util.VmeConstants;

/**
 * Controller to receive request about management of the inner process operations 
 * @author Carmine Ruffino
 *
 */

@Controller
@RequestMapping(value = "/user/innerprocess")
public class InnerProcessController extends BaseController {
	
	@Autowired
	private InnerProcessService innerProcessService;	

	private static final Logger logger = LoggerFactory.getLogger(InnerProcessController.class);	
	
	/**
	 * Method to get a list of inner project by project
	 * @param project_id - mashup project identifier
	 * @return
	 * @throws RestException 
	 */
	@RequestMapping(value = "/{project_id}/list.json", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<InnerProcessDTO> listJSON(@PathVariable("project_id") int project_id) throws RestException {

		logger.info("Getting inner process list by project: "+project_id);

		List<InnerProcessDTO> innerProcesses = null; 
		
		try {
			AccountDTO a = getLoggedUser();
			if (a == null || a.getRole() != VmeConstants.ACCOUNT_ROLE_USER) {
	
				logger.error("Logged Account is bad");
				throw new RestException("Logged Account is bad");
			}		
		
			innerProcesses = innerProcessService.findAllByProfileAsDTO(a.getProfile(), project_id);
			
		} catch (SelectShapeException e) {
			logger.error("Error reading project list for inner process palette", e);
			throw new RestException("Error reading project list for inner process palette", e);
		}
		
		return innerProcesses;
	}
}
