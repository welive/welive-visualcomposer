/**
 * 
 */
package com.mediigea.vme2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author carmine
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OperatorShapeDTO extends ShapeDTO {

	private InnerProcessShapeDTO innerProcess;
	
	private UD_OperatorShapeDTO userData;		
	
	/**
	 * @return the innerProcess
	 */
	public InnerProcessShapeDTO getInnerProcess() {
		return innerProcess;
	}
	/**
	 * @param innerProcess the innerProcess to set
	 */
	public void setInnerProcess(InnerProcessShapeDTO innerProcess) {
		this.innerProcess = innerProcess;
	}
	public UD_OperatorShapeDTO getUserData() {
		return userData;
	}
	public void setUserData(UD_OperatorShapeDTO userData) {
		this.userData = userData;
	}
}
