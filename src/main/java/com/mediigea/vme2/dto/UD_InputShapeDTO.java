package com.mediigea.vme2.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UD_InputShapeDTO {
	private String label;
	private String inputType;
	private String defaultValue;
	private List<InputField> combooptions;
	private boolean isConstant;
	
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the inputType
	 */
	public String getInputType() {
		return inputType;
	}
	/**
	 * @param inputType the inputType to set
	 */
	public void setInputType(String inputType) {
		this.inputType = inputType;
	}
	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	/**
	 * @return the combooptions
	 */
	public List<InputField> getCombooptions() {
		return combooptions;
	}
	/**
	 * @param combooptions the combooptions to set
	 */
	public void setCombooptions(List<InputField> combooptions) {
		this.combooptions = combooptions;
	}
	
	/**
	 * @param specifies if this input has constant value
	 */
	public boolean isIsConstant() {
		return isConstant;
	}
	public void setIsConstant(boolean isConstant) {
		this.isConstant = isConstant;
	}		
}
