package com.mediigea.vme2.dto;

import java.util.List;

public class ProfilePagedSearchDTO {
	private int total;
	private List<ProfileDTO> results;
	
	public ProfilePagedSearchDTO() {
	
	}
	
	public ProfilePagedSearchDTO(List<ProfileDTO> results, int total){
		this.results = results;
		this.total = total;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<ProfileDTO> getResults() {
		return results;
	}

	public void setResults(List<ProfileDTO> results) {
		this.results = results;
	}
}
