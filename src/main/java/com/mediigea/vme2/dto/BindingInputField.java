package com.mediigea.vme2.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BindingInputField {	
	
	private String inputName;
	private String label;	
	private String mashupName;
	
	private String defaultValue;
	private boolean isConstant;

	public BindingInputField(String inputName, String label, String mashupName) {
		this.inputName = inputName;
		this.label = label;
		this.mashupName = mashupName;
	}
	
	/**
	 * @return the inputName
	 */
	public String getInputName() {
		return inputName;
	}

	/**
	 * @param inputName the inputName to set
	 */
	public void setInputName(String inputName) {
		this.inputName = inputName;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public String getMashupName() {
		return mashupName;
	}

	public void setMashupName(String mashupName) {
		this.mashupName = mashupName;
	}		
	
	public String getDefaultValue() 
	{
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) 
	{
		this.defaultValue = defaultValue;
	}
	
	public boolean isIsConstant() 
	{
		return isConstant;
	}
	
	public void setIsConstant(boolean isConstant) 
	{
		this.isConstant = isConstant;
	}	
}
