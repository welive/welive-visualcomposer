/**
 * 
 */
package com.mediigea.vme2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author carmine
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InnerProcessShapeDTO extends ShapeDTO {

	private InnerProcessDTO userData;
	
	public InnerProcessDTO getUserData() {
		return userData;
	}
	public void setUserData(InnerProcessDTO userData) {
		this.userData = userData;
	}
}
