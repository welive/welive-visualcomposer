/**
 * 
 */
package com.mediigea.vme2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author carmine
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenDataShapeDTO extends ShapeDTO {
	
	// IVAN
	private OpenDataResourceExtDTO userData;
	
	public OpenDataResourceExtDTO getUserData() {
		return userData;
	}
	public void setUserData(OpenDataResourceExtDTO userData) {
		this.userData = userData;
	}
}
