package com.mediigea.vme2.dto;

public class ProjectExportBean {
	private String dashboard;
	private String renderingSchema;
	/**
	 * @return the dashboard
	 */
	public String getDashboard() {
		return dashboard;
	}
	/**
	 * @param dashboard the dashboard to set
	 */
	public void setDashboard(String dashboard) {
		this.dashboard = dashboard;
	}
	/**
	 * @return the renderingSchema
	 */
	public String getRenderingSchema() {
		return renderingSchema;
	}
	/**
	 * @param renderingSchema the renderingSchema to set
	 */
	public void setRenderingSchema(String renderingSchema) {
		this.renderingSchema = renderingSchema;
	}		
}
