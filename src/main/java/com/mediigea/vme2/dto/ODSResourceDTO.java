package com.mediigea.vme2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ODSResourceDTO 
{
	private int id;
	private int datasetId;
	private String title;
	private String description;
	private String url;
	private String odsDatasetId;
	private String odsResourceId;
	private String dataSchema;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDatasetId() {
		return datasetId;
	}
	public void setDatasetId(int datasetId) {
		this.datasetId = datasetId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getOdsDatasetId() {
		return odsDatasetId;
	}
	public void setOdsDatasetId(String odsDatasetId) {
		this.odsDatasetId = odsDatasetId;
	}
	public String getOdsResourceId() {
		return odsResourceId;
	}
	public void setOdsResourceId(String odsResourceId) {
		this.odsResourceId = odsResourceId;
	}
	public String getDataSchema() {
		return dataSchema;
	}
	public void setDataSchema(String dataSchema) {
		this.dataSchema = dataSchema;
	}

}
