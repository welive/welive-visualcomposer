package com.mediigea.vme2.dto;

import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.Profile;

public class GuiProjectDTO {
	private GuiProject guiProject;
	private Integer unreadComment;
	private Profile owner;
	
	public GuiProject getGuiProject() {
		return guiProject;
	}
	public void setGuiProject(GuiProject guiProject) {
		this.guiProject = guiProject;
	}
	public Integer getUnreadComment() {
		return unreadComment;
	}
	public void setUnreadComment(Integer unreadComment) {
		this.unreadComment = unreadComment;
	}
	public Profile getOwner() {
		return owner;
	}
	public void setOwner(Profile owner) {
		this.owner = owner;
	}
	
	
}
