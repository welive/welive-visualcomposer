/**
 * 
 */
package com.mediigea.vme2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mediigea.vme2.util.ConnectionShapeDeserializer;

/**
 * @author carmine
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = ConnectionShapeDeserializer.class)
public class ConnectionShapeDTO extends ShapeDTO {
	
	private String sourceId;
	
	private String targetId;

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}		
}
