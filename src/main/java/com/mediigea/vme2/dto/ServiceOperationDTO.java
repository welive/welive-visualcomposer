package com.mediigea.vme2.dto;

import java.util.List;

public class ServiceOperationDTO {
	private int id;
	private String name;
	private String doc;
	private String tag;
	private boolean authenticationRequired = false;
	private int serviceId;
	private int serviceType;
	private List<OperationTemplateDTO> templates;
	//private boolean hasTemplate;
	
	private List<ServiceParameterDTO> parameters;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the serviceId
	 */
	public int getServiceId() {
		return serviceId;
	}

	/**
	 * @param serviceId
	 *            the serviceId to set
	 */
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * @return the serviceType
	 */
	public int getServiceType() {
		return serviceType;
	}

	/**
	 * @param serviceType
	 *            the serviceType to set
	 */
	public void setServiceType(int serviceType) {
		this.serviceType = serviceType;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the doc
	 */
	public String getDoc() {
		return doc;
	}

	/**
	 * @param doc
	 *            the doc to set
	 */
	public void setDoc(String doc) {
		this.doc = doc;
	}

	/**
	 * @return the tag
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * @param tag
	 *            the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}

	

	/**
	 * @return the authenticationRequired
	 */
	public boolean isAuthenticationRequired() {
		return authenticationRequired;
	}

	/**
	 * @param authenticationRequired the authenticationRequired to set
	 */
	public void setAuthenticationRequired(boolean authenticationRequired) {
		this.authenticationRequired = authenticationRequired;
	}

	/**
	 * @return the parameters
	 */
	public List<ServiceParameterDTO> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(List<ServiceParameterDTO> parameters) {
		this.parameters = parameters;
	}

	/**
	 * @return the templates
	 */
	public List<OperationTemplateDTO> getTemplates() {
		return templates;
	}

	/**
	 * @param templates the templates to set
	 */
	public void setTemplates(List<OperationTemplateDTO> templates) {
		this.templates = templates;
	}

//	/**
//	 * @return the hasTemplate
//	 */
//	public boolean isHasTemplate() {
//		return hasTemplate;
//	}
//
//	/**
//	 * @param hasTemplate the hasTemplate to set
//	 */
//	public void setHasTemplate(boolean hasTemplate) {
//		this.hasTemplate = hasTemplate;
//	}		
}
