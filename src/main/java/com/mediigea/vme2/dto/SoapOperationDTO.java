package com.mediigea.vme2.dto;

import com.mediigea.vme2.soap.SoapMethod;

public class SoapOperationDTO{
	
	private int id;
    
	private String operationName;
	private String wsdlUrl;
	private String endPoint;
	private String bindingName;
	private String portTypeName;
	
	private String soapAction;
	private String inputParams;
	private String outputParams;
	
	private String wsdlStream;
	private SoapMethod method;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	
	
	public String getWsdlUrl() {
		return wsdlUrl;
	}

	public void setWsdlUrl(String wsdlUrl) {
		this.wsdlUrl = wsdlUrl;
	}
	
	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}	

	public String getBindingName() {
		return bindingName;
	}

	public void setBindingName(String bindingName) {
		this.bindingName = bindingName;
	}

	public String getPortTypeName() {
		return portTypeName;
	}

	public void setPortTypeName(String portTypeName) {
		this.portTypeName = portTypeName;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}
	
	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}	
	
	public String getInputParams() {
		return inputParams;
	}

	public void setInputParams(String inputParams) {
		this.inputParams = inputParams;
	}

	public String getOutputParams() {
		return outputParams;
	}

	public void setOutputParams(String outputParams) {
		this.outputParams = outputParams;
	}

	public String getWsdlStream() {
		return wsdlStream;
	}

	public void setWsdlStream(String wsdlStream) {
		this.wsdlStream = wsdlStream;
	}

	/**
	 * @return the method
	 */
	public SoapMethod getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	public void setMethod(SoapMethod method) {
		this.method = method;
	}
}
