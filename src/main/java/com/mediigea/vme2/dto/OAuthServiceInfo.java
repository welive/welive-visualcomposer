package com.mediigea.vme2.dto;

import org.scribe.oauth.OAuthService;

public class OAuthServiceInfo {
	private OAuthService service;
	private String apiKey;
	private String apiSecret;
	/**
	 * @return the service
	 */
	public OAuthService getService() {
		return service;
	}
	/**
	 * @param service the service to set
	 */
	public void setService(OAuthService service) {
		this.service = service;
	}
	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}
	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	/**
	 * @return the apiSecret
	 */
	public String getApiSecret() {
		return apiSecret;
	}
	/**
	 * @param apiSecret the apiSecret to set
	 */
	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}
}
