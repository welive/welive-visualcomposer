package com.mediigea.vme2.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UD_OperatorShapeDTO {	
	private String name;
	private String type;
	private String emmlmacro;
	private String emmlscriptgenerator;
	private List<OperatorParameterDTO> parameters;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the parameters
	 */
	public List<OperatorParameterDTO> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(List<OperatorParameterDTO> parameters) {
		this.parameters = parameters;
	}
	
	/**
	 * @return the emmlmacro
	 */
	public String getEmmlmacro() {
		return emmlmacro;
	}

	/**
	 * @param emmlmacro the emmlmacro to set
	 */
	public void setEmmlmacro(String emmlmacro) {
		this.emmlmacro = emmlmacro;
	}

	/**
	 * @return the emmlscriptgenerator
	 */
	public String getEmmlscriptgenerator() {
		return emmlscriptgenerator;
	}

	/**
	 * @param emmlscriptgenerator the emmlscriptgenerator to set
	 */
	public void setEmmlscriptgenerator(String emmlscriptgenerator) {
		this.emmlscriptgenerator = emmlscriptgenerator;
	}				
}
