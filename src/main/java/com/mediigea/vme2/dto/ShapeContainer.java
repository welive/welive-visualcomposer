package com.mediigea.vme2.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.mediigea.vme2.config.ObjectMapperExt;
import com.mediigea.vme2.exceptions.SelectShapeException;
import com.mediigea.vme2.util.VmeConstants;

public class ShapeContainer {
	private static final Logger logger = LoggerFactory.getLogger(ShapeContainer.class);
	
	
	private HashMap<String, ShapeDTO> services;
	private List<ShapeDTO> connections;
	private HashMap<String, ShapeDTO> inputs;
	private HashMap<String, ShapeDTO> operators;
	private HashMap<String, ShapeDTO> blackboxes;
	private HashMap<String, ShapeDTO> innerProcesses;
	private HashMap<String, ShapeDTO> openData;
	private ShapeDTO output;
	
	public ShapeContainer(){
		services = new HashMap<String, ShapeDTO>();
		operators = new HashMap<String, ShapeDTO>();
		blackboxes = new HashMap<String, ShapeDTO>();
		openData= new HashMap<String, ShapeDTO>();
		connections = new ArrayList<ShapeDTO>();
		innerProcesses = new HashMap<String, ShapeDTO>();
		inputs = new HashMap<String, ShapeDTO>();
	}
	
	public HashMap<String, ShapeDTO> getServices() {
		return services;
	}
	public void setServices(HashMap<String, ShapeDTO> services) {
		this.services = services;
	}		
	/**
	 * @return the operators
	 */
	public HashMap<String, ShapeDTO> getOperators() {
		return operators;
	}

	/**
	 * @param operators the operators to set
	 */
	public void setOperators(HashMap<String, ShapeDTO> operators) {
		this.operators = operators;
	}

	/**
	 * @return the blackboxes
	 */
	public HashMap<String, ShapeDTO> getBlackboxes() {
		return blackboxes;
	}

	/**
	 * @param blackboxes the blackboxes to set
	 */
	public void setBlackboxes(HashMap<String, ShapeDTO> blackboxes) {
		this.blackboxes = blackboxes;
	}

	/**
	 * @return the openData
	 */
	public HashMap<String, ShapeDTO> getOpenData() {
		return openData;
	}

	/**
	 * @param openData the openData to set
	 */
	public void setOpenData(HashMap<String, ShapeDTO> openData) {
		this.openData = openData;
	}

	public List<ShapeDTO> getConnections() {
		return connections;
	}
	public void setConnections(List<ShapeDTO> connections) {
		this.connections = connections;
	}			
	
	/**
	 * @return the innerProcesses
	 */
	public HashMap<String, ShapeDTO> getInnerProcesses() {
		return innerProcesses;
	}

	/**
	 * @param innerProcesses the innerProcesses to set
	 */
	public void setInnerProcesses(HashMap<String, ShapeDTO> innerProcesses) {
		this.innerProcesses = innerProcesses;
	}

	public HashMap<String, ShapeDTO> getInputs() {
		return inputs;
	}
	public void setInputs(HashMap<String, ShapeDTO> inputs) {
		this.inputs = inputs;
	}		
	public ShapeDTO getOutput() {
		return output;
	}

	public void setOutput(ShapeDTO output) {
		this.output = output;
	}

	public void shapeSelector(String shapeCanvas, ObjectMapperExt objectMapper) throws SelectShapeException {

		JsonNode root;
		try {
			//Converte la stringa JSON rappresentante il canvas in una struttura Json jackson 
			root = objectMapper.readTree(shapeCanvas);

			//Prende i nodi shapes
			Iterator<JsonNode> iter = root.path(VmeConstants.CANVAS_ROOT_NODE).elements();

			while (iter.hasNext()) {
				JsonNode node = iter.next();
				
				//Prende il tipo dallo shape ed effettua il filtraggio
				String shapeType = node.get("type").asText();

				if(shapeType != null){
					if (VmeConstants.SHAPE_INPUT.equalsIgnoreCase(shapeType)) {

						InputShapeDTO myClass = objectMapper.treeToValue(node,
								InputShapeDTO.class);
						this.inputs.put(myClass.getId(), myClass);

						logger.debug("Input Shape found:"+myClass.getType()+" - "+myClass.getId());

					} else if (VmeConstants.SHAPE_SERVICE.equalsIgnoreCase(shapeType)) {

						ServiceShapeDTO myClass = objectMapper.treeToValue(node,
								ServiceShapeDTO.class);
						this.services.put(myClass.getId(), myClass);

						logger.debug("Service Shape found:"+myClass.getType()+" - "+myClass.getId());				

					} else if (shapeType.indexOf(VmeConstants.SHAPE_CONNECTION)!=-1) {

						ConnectionShapeDTO myClass = objectMapper.treeToValue(node,
								ConnectionShapeDTO.class);
						this.connections.add(myClass);

						logger.debug("Connection Shape found:"+myClass.getType()+" - "+myClass.getId());

					} else if (shapeType.indexOf(VmeConstants.SHAPE_OUTPUT)!=-1) {

						OutputShapeDTO myClass = objectMapper.treeToValue(node,
								OutputShapeDTO.class);
						this.output = myClass;

						logger.debug("Output Shape found:"+myClass.getType()+" - "+myClass.getId());

					} else if (shapeType.indexOf(VmeConstants.SHAPE_OPERATOR)!=-1) {

						OperatorShapeDTO myClass = objectMapper.treeToValue(node,
								OperatorShapeDTO.class);
						
						this.operators.put(myClass.getId(), myClass);

						logger.debug("Operator Shape found:"+myClass.getType()+" - "+myClass.getId());

					} else if (shapeType.indexOf(VmeConstants.SHAPE_BLACKBOX)!=-1) {

						BlackboxShapeDTO myClass = objectMapper.treeToValue(node,
								BlackboxShapeDTO.class);
						
						this.blackboxes.put(myClass.getId(), myClass);

						logger.debug("Blackbox Shape found:"+myClass.getType()+" - "+myClass.getId());
						
					} else if (shapeType.indexOf(VmeConstants.SHAPE_INNERPROCESS)!=-1) {

						InnerProcessShapeDTO myClass = objectMapper.treeToValue(node,
								InnerProcessShapeDTO.class);
						
						this.innerProcesses.put(myClass.getId(), myClass);

						logger.debug("Inner Process Shape found:"+myClass.getType()+" - "+myClass.getId());
						
					} else if (shapeType.indexOf(VmeConstants.SHAPE_OPENDATA)!=-1) {

						OpenDataShapeDTO myClass = objectMapper.treeToValue(node,
								OpenDataShapeDTO.class);
						
						this.openData.put(myClass.getId(), myClass);

						logger.debug("OpenData Shape found:"+myClass.getType()+" - "+myClass.getId());

					}
				}
			}			
		} catch (Exception e) {

			logger.error("Error selecting shapes");
			throw new SelectShapeException(e);
		}
	}	
}
