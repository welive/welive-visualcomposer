package com.mediigea.vme2.dto;

import java.util.ArrayList;
import java.util.HashMap;


/*
"{
 "operationId":343,
 "serviceId":7,
 "serviceType":2,
 "parameters":[{
  "name":"user",
  "value":"kamelabsdev"
 }, {
  "name":"fields",
  "value":"gender"
 }]
  
}"
 */
public class InvokeDTO {

	private int operationId;
	private int serviceId;
	private int serviceType;
	
	private ArrayList<HashMap<String, String>> parameters;

	public int getOperationId() {
		return operationId;
	}

	public void setOperationId(int operationId) {
		this.operationId = operationId;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public int getServiceType() {
		return serviceType;
	}

	public void setServiceType(int serviceType) {
		this.serviceType = serviceType;
	}

	public ArrayList<HashMap<String, String>> getParameters() {
		return parameters;
	}

	public void setParameters(ArrayList<HashMap<String, String>> parameters) {
		this.parameters = parameters;
	}
}
