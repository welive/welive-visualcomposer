/**
 * 
 */
package com.mediigea.vme2.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mediigea.vme2.util.OutputShapeDeserializer;

/**
 * @author carmine
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = OutputShapeDeserializer.class)
public class OutputShapeDTO extends ShapeDTO {
	
	public OutputShapeDTO() {
		super();
		inputs = new ArrayList<String>();
	}
	
	private List<String> inputs;

	/**
	 * @return the inputs
	 */
	public List<String> getInputs() {
		return inputs;
	}

	/**
	 * @param inputs the inputs to set
	 */
	public void setInputs(List<String> inputs) {
		this.inputs = inputs;
	}		
}
