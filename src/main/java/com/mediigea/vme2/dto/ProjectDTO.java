package com.mediigea.vme2.dto;

import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;

public class ProjectDTO {
	private Project project;
	private Integer unreadComment;
	private Profile owner;
	
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public Integer getUnreadComment() {
		return unreadComment;
	}
	public void setUnreadComment(Integer unreadComment) {
		this.unreadComment = unreadComment;
	}
	public Profile getOwner() {
		return owner;
	}
	public void setOwner(Profile owner) {
		this.owner = owner;
	}	
}
