/**
 * 
 */
package com.mediigea.vme2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author carmine
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InputShapeDTO extends ShapeDTO {
	private UD_InputShapeDTO userData;

	/**
	 * @return the userData
	 */
	public UD_InputShapeDTO getUserData() {
		return userData;
	}

	/**
	 * @param userData the userData to set
	 */
	public void setUserData(UD_InputShapeDTO userData) {
		this.userData = userData;
	}		
}
