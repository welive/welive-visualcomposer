/**
 * 
 */
package com.mediigea.vme2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author carmine
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceShapeDTO extends ShapeDTO {

	private UD_ServiceShapeDTO userData;
	
	public UD_ServiceShapeDTO getUserData() {
		return userData;
	}
	public void setUserData(UD_ServiceShapeDTO userData) {
		this.userData = userData;
	}
}
