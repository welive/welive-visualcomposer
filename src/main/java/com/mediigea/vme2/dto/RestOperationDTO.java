package com.mediigea.vme2.dto;

import com.mediigea.vme2.rest.RestMethod;

public class RestOperationDTO{

	private int id;

	private String baseURL;
	private String path;
	private String name;
	private String authentication;
	private RestMethod execMethod;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBaseURL() {
		return baseURL;
	}

	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getAuthentication() {
		return authentication;
	}

	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}

	public RestMethod getExecMethod() {
		return execMethod;
	}

	public void setExecMethod(RestMethod definition) {
		this.execMethod = definition;
	}
}
