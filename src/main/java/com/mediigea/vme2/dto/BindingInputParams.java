/**
 * 
 */
package com.mediigea.vme2.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author carmine
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BindingInputParams {
	private List<BindingInputField> inputParams;
	private String actionUrl;
	/**
	 * @return the inputParams
	 */
	public List<BindingInputField> getInputParams() {
		return inputParams;
	}
	/**
	 * @param inputParams the inputParams to set
	 */
	public void setInputParams(List<BindingInputField> inputParams) {
		this.inputParams = inputParams;
	}
	/**
	 * @return the actionUrl
	 */
	public String getActionUrl() {
		return actionUrl;
	}
	/**
	 * @param actionUrl the actionUrl to set
	 */
	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}	
}
