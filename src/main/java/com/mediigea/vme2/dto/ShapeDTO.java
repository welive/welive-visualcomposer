package com.mediigea.vme2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
/*@JsonTypeInfo(  
	    use = JsonTypeInfo.Id.NAME,  
	    include = JsonTypeInfo.As.PROPERTY,  
	    property = "type")  
	@JsonSubTypes({  
	    @Type(value = ServiceShapeDTO.class, name = "vme2.shape.Service")	    
	    ,@Type(value = RendererShapeDTO.class, name = "vme2.shape.Renderer.Iterator")
		,@Type(value = ConnectionShapeDTO.class, name = "vme2.shape.Connection.Service.S2IR") 
	    }) */
public abstract class ShapeDTO {	
	
	private String type;
	private String id;
	private String label;				
	
	public ShapeDTO() {
		super();
	}

	public ShapeDTO(String type, String id, String label) {
		super();
		this.type = type;
		this.id = id;
		this.label = label;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
		
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}		
}
