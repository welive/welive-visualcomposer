package com.mediigea.vme2.dto;

import java.io.Serializable;

public class GadgetPrefsDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5247352148773077918L;

	private String title;
	private String description;
	private String author;
	private String authorEmail;
	private Integer width;
	private Integer height;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthorEmail() {
		return authorEmail;
	}

	public void setAuthorEmail(String authorEmail) {
		this.authorEmail = authorEmail;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}
	

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

}
