package com.mediigea.vme2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OperatorParameterDTO {
	
	private String name;
	private String property;
	private String property2;
	private String operator;
	private String label;
	private String defaultValue;
	private String datatype;
	private String doc;
	private boolean required;
	private boolean droppable;
	private boolean sourceexp;
	private boolean droppableP2;
	private boolean sourceexpP2;
	private boolean bind;
	private boolean custom;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param property the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	/**
	 * @return the doc
	 */
	public String getDoc() {
		return doc;
	}
	/**
	 * @param doc the doc to set
	 */
	public void setDoc(String doc) {
		this.doc = doc;
	}
	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}
	/**
	 * @param required the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}	
	/**
	 * @return the value
	 */
	public String getProperty2() {
		return property2;
	}
	/**
	 * @param value the value to set
	 */
	public void setProperty2(String property2) {
		this.property2 = property2;
	}
	public boolean isBind() {
		return bind;
	}
	public void setBind(boolean bind) {
		this.bind = bind;
	}
	/**
	 * @return the type
	 */
	public String getDatatype() {
		return datatype;
	}
	/**
	 * @param type the type to set
	 */
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	
	/**
	 * @return the droppable
	 */
	public boolean isDroppable() {
		return droppable;
	}
	/**
	 * @param droppable the droppable to set
	 */
	public void setDroppable(boolean droppable) {
		this.droppable = droppable;
	}
	/**
	 * @return the sourceexp
	 */
	public boolean isSourceexp() {
		return sourceexp;
	}
	/**
	 * @param sourceexp the sourceexp to set
	 */
	public void setSourceexp(boolean sourceexp) {
		this.sourceexp = sourceexp;
	}
	/**
	 * @return the custom
	 */
	public boolean isCustom() {
		return custom;
	}
	/**
	 * @param custom the custom to set
	 */
	public void setCustom(boolean custom) {
		this.custom = custom;
	}
	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}
	/**
	 * @param operator the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}
	/**
	 * @return the property
	 */
	public String getProperty() {
		return property;
	}
	/**
	 * @param property the property to set
	 */
	public void setProperty(String property) {
		this.property = property;
	}
	/**
	 * @return the droppableP2
	 */
	public boolean isDroppableP2() {
		return droppableP2;
	}
	/**
	 * @param droppableP2 the droppableP2 to set
	 */
	public void setDroppableP2(boolean droppableP2) {
		this.droppableP2 = droppableP2;
	}
	/**
	 * @return the sourceexpP2
	 */
	public boolean isSourceexpP2() {
		return sourceexpP2;
	}
	/**
	 * @param sourceexpP2 the sourceexpP2 to set
	 */
	public void setSourceexpP2(boolean sourceexpP2) {
		this.sourceexpP2 = sourceexpP2;
	}						
}
