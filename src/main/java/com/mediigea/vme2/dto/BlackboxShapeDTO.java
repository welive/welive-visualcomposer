/**
 * 
 */
package com.mediigea.vme2.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author carmine
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BlackboxShapeDTO extends ShapeDTO {

	private BlackboxDTO userData;
	
	public BlackboxDTO getUserData() {
		return userData;
	}
	public void setUserData(BlackboxDTO userData) {
		this.userData = userData;
	}
}
