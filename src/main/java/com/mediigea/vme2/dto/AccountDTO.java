package com.mediigea.vme2.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.util.VmeConstants;

public class AccountDTO implements UserDetails{
	private int id;
	private Profile profile;
	private String email;
	private String psw;
	private int status;
	private int role;
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the profile
	 */
	public Profile getProfile() {
		return profile;
	}

	/**
	 * @param profile the profile to set
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the psw
	 */
	public String getPsw() {
		return psw;
	}

	/**
	 * @param psw the psw to set
	 */
	public void setPsw(String psw) {
		this.psw = psw;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();

		if(this.role == VmeConstants.ACCOUNT_ROLE_USER){
			authList.add(new SimpleGrantedAuthority( VmeConstants.SECURITY_ROLE_USER ));
			
		} else if (this.role == VmeConstants.ACCOUNT_ROLE_ADMIN){
			
			authList.add(new SimpleGrantedAuthority( VmeConstants.SECURITY_ROLE_ADMIN ));
		}
		
		return authList;
	}

	@Override
	public String getPassword() {
		
		return this.psw;
	}

	@Override
	public String getUsername() {
		
		return this.email;
	}

	/**
	 * @return the role
	 */
	public int getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(int role) {
		this.role = role;
	}

	@Override
	public boolean isAccountNonExpired() {		
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.status==VmeConstants.ACCOUNT_STATUS_PENDING || this.status==VmeConstants.ACCOUNT_STATUS_ENABLED;
	}

	@Override
	public boolean isCredentialsNonExpired() {		
		return true;
	}

	@Override
	public boolean isEnabled() {		
		return this.status==VmeConstants.ACCOUNT_STATUS_ENABLED;
	}
	
	
}
