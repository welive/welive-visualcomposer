package com.mediigea.vme2.emml.operator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.mediigea.vme2.dto.OperatorParameterDTO;
import com.mediigea.vme2.dto.OperatorShapeDTO;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.ScriptType;
import com.mediigea.vme2.emml.VariableType;
import com.mediigea.vme2.emml.VariablesType;
import com.mediigea.vme2.util.EmmlScriptUtils;
import com.mediigea.vme2.util.VmeConstants;

public class TwoSourcesOperator extends AbstractOperator implements IOperator {
	private static final Logger logger = LoggerFactory
			.getLogger(TwoSourcesOperator.class);

	private String macroName;
	private static final String MACRO_INPUTSOURCE_VARIABLE = "inputsource";
	private static final String MACRO_PARAMS_VARIABLE = "params";
	private static final String MACRO_OUTPUT_VARIABLE = "result";

	public TwoSourcesOperator(ObjectFactory of, Mashup rootTag, VariablesType variables,
			OperatorShapeDTO shape, Integer projectId) {

		super(of, rootTag, variables, shape, projectId);
		this.macroName = shape.getUserData().getEmmlmacro();
	}

	@Override
	public void buildEmmlCommand() throws Exception {

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();

			Element root = document.createElementNS(MACRO_NAMESPACE,
					MACRO_PREFIX + ":" + this.macroName);

			StringBuilder params = new StringBuilder();

			StringBuffer scriptInputVariables = new StringBuffer();
			for (OperatorParameterDTO param : inputParams) {
				inputValueDigester(root, param, params, scriptInputVariables);
			}

			if (params.length() > 0) {

				String varName = nodeId + "_params";

				VariableType scriptoutvar = of.createVariableType();

				scriptoutvar.setName(varName);
				scriptoutvar.setType(VmeConstants.EMML_VAR_TYPE_STRING);
				variables.getVariable().add(scriptoutvar);

				ScriptType script = new ScriptType();
				script.setType("text/javascript");
				script.setOutputvariable(varName);
				
				if(scriptInputVariables.length()>0){
					script.setInputvariables(scriptInputVariables.toString());
				}
				
				script.setValue("<![CDATA[var " + varName + "='["
						+ params.toString() + "]';]]>");

				rootTag.getEmmlMetaOrInputOrOutput().add(
						of.createMashupScript(script));

				root.setAttribute(MACRO_PARAMS_VARIABLE, "$" + varName);
			}

			VariableType outputVar = of.createVariableType();

			String varName = nodeId;
			outputVar.setName(varName);
			outputVar.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			variables.getVariable().add(outputVar);

			root.setAttribute(MACRO_OUTPUT_VARIABLE, varName);

			rootTag.getEmmlMetaOrInputOrOutput().add(root);

		} catch (ParserConfigurationException pce) {
			throw pce;
		}
	}

	private void inputValueDigester(Element root, OperatorParameterDTO param,
			StringBuilder params, StringBuffer scriptInputVariables) {

		String property = param.getProperty();
		String defaultValue = param.getDefaultValue();

		if ((property == null || property.equals("")) && defaultValue != null
				&& !defaultValue.equals(""))
			property = defaultValue;

		if (property != null && !property.equals("")) {

			if (params.length() > 0) {
				params.append(",");
			}
			
			property = parseProperty(root, param.isSourceexp(), scriptInputVariables, property, param.getName());

			params.append("{").append("\"name\":").append("\"")
					.append(param.getName()).append("\"").append(",")
					.append("\"property\":").append("\"").append(property)
					.append("\"");

			String property2 = param.getProperty2();
			if (property2 != null && !property2.isEmpty()) {
				
				property2 = parseProperty(root, param.isSourceexpP2(), scriptInputVariables, property2, param.getName());
				
				params.append(",").append("\"property2\":").append("\"")
						.append(property2).append("\"");												
			}

			String operator = param.getOperator();
			if (operator != null && !operator.isEmpty()) {
				params.append(",").append("\"operator\":").append("\"")
						.append(operator).append("\"");
			}

			params.append("}");
		}
	}
	
	private String parseProperty(Element root, boolean isSourceexp, StringBuffer scriptInputVariables, String property, String paramName){
		
		if(isSourceexp){
			if (property.matches(VmeConstants.SERVICE_INPUT_PARAM_REGEX)) {
				// Prende l'identificativo del servizio source da cui prelevare
				// il valore
				String sourceServiceId = property.substring(
						property.indexOf("{") + 1, property.indexOf(":"));
				
				root.setAttribute(MACRO_INPUTSOURCE_VARIABLE + "_" + paramName.split("_")[1], "$" + sourceServiceId);

				property = EmmlScriptUtils.parsePathVariable(property);
			} else {
				//Controlla se il valore del parametro e' di tipo link con un input shape
				//prototipo: {input_0}
				logger.debug("Endpoint parameter value: "+property);							
				Pattern p = Pattern.compile("(\\{)(.*)(\\})");
				Matcher m = p.matcher(property);														

				//se il valore e' di tipo link lo sostituisce con il nome della variabile di input precedentemente creata
				//{input_0} -> {$input_0}
				if(m.find()){
					StringBuffer inputVar = new StringBuffer();					
					
					m.appendReplacement(inputVar, "$1\\$$2$3");
					
					property = ".";
					
					root.setAttribute(MACRO_INPUTSOURCE_VARIABLE, inputVar.toString());										
				}
				logger.debug("Endpoint parameter value replacement: "+property);	
			}
		} else {
			
			logger.debug("Endpoint parameter value: "+property);							
			Pattern p = Pattern.compile("^(\\{)(.*)(\\})");
			Matcher m = p.matcher(property);														

			//se il valore e' di tipo link lo sostituisce con il nome della variabile di input precedentemente creata
			//{input_0} -> {$input_0}
			if(m.find()){
				StringBuffer inputVar = new StringBuffer();					
				StringBuffer inputName = new StringBuffer();
				
				inputVar.append(m.group(1)).append("$").append(m.group(2)).append(m.group(3));
				inputName.append(m.group(2));					
				
				property = "'+"+inputName+"[0]+'";
				
				scriptInputVariables.append(scriptInputVariables.length()>0 ? "," : "").append(inputName);									
			}
			logger.debug("Endpoint parameter value replacement: "+property);					
		}		
		//logger.debug("### buildEmmlCommand: property: " + property.toString());
		return property;
	}
	
}
