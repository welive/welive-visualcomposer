package com.mediigea.vme2.emml.operator;

import java.util.List;

import com.mediigea.vme2.dto.OperatorParameterDTO;
import com.mediigea.vme2.dto.OperatorShapeDTO;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.VariablesType;

public class AbstractOperator {		
	
	protected static final String MACRO_NAMESPACE = "http://www.openemml.org/2009-04-15/EMMLMacro";
	protected static final String MACRO_PREFIX = "macro";	
	
	protected ObjectFactory of;
	protected Mashup rootTag;
	protected OperatorShapeDTO shape;
	protected List<OperatorParameterDTO> inputParams;
	protected VariablesType variables;
	protected String nodeId;
	protected Integer projectId;
	
	public AbstractOperator(ObjectFactory of, Mashup rootTag, VariablesType variables, OperatorShapeDTO shape, Integer projectId){
		this.of = of;
		this.rootTag = rootTag;		
		this.variables = variables;
		this.inputParams = shape.getUserData().getParameters();
		this.nodeId = shape.getId();
		this.projectId = projectId;
	}		
}
