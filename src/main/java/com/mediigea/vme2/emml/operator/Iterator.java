package com.mediigea.vme2.emml.operator;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.mediigea.vme2.dao.ProjectInnerProcessDAO;
import com.mediigea.vme2.dto.InnerProcessShapeDTO;
import com.mediigea.vme2.dto.OperatorParameterDTO;
import com.mediigea.vme2.dto.OperatorShapeDTO;
import com.mediigea.vme2.emml.AssignType;
import com.mediigea.vme2.emml.ForEachType;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.ScriptType;
import com.mediigea.vme2.emml.VariableType;
import com.mediigea.vme2.emml.VariablesType;
import com.mediigea.vme2.emml.service.IBuildEmmlInvoke;
import com.mediigea.vme2.service.ProjectInnerProcessService;
import com.mediigea.vme2.util.EmmlScriptUtils;
import com.mediigea.vme2.util.VmeConstants;

public class Iterator extends AbstractOperator implements IOperator {
	
	@Autowired
	private IBuildEmmlInvoke buildProjectEmmlInvoke;
	
	@Autowired
	private ProjectInnerProcessService projectInnerProcessService;	
	
	private static final Logger logger = LoggerFactory
			.getLogger(Iterator.class);

	private InnerProcessShapeDTO innerProcess;
	
	public Iterator(ObjectFactory of, Mashup rootTag,
			VariablesType variables,			
			OperatorShapeDTO shape,
			Integer projectId) {
		
		super(of, rootTag, variables, shape, projectId);		
		
		this.innerProcess = shape.getInnerProcess();
	}

	@Override
	public void buildEmmlCommand() throws Exception {					
		
		for (OperatorParameterDTO param : inputParams) {
			String property = param.getProperty();
			
			if (property.matches(VmeConstants.SERVICE_INPUT_PARAM_REGEX)) {
				// Prende l'identificativo del servizio source da cui prelevare
				// il valore				
				
				String sourceServiceId = EmmlScriptUtils.getServiceIdFromPath(property);

				String scriptOutVarName = sourceServiceId+"_str";
				
				VariableType scriptOutVar = of.createVariableType();
				scriptOutVar.setName(scriptOutVarName);
				scriptOutVar.setType(VmeConstants.EMML_VAR_TYPE_STRING);
				variables.getVariable().add(scriptOutVar);

				String assignOutVarName = sourceServiceId+"_doc";
				
				VariableType assignoutvar = of.createVariableType();
				assignoutvar.setName(assignOutVarName);
				assignoutvar.setType(VmeConstants.EMML_VAR_TYPE_DOCUMENT);
				variables.getVariable().add(assignoutvar);

				
				String iteratorVarName = nodeId;
				
				VariableType iteratorvar = of.createVariableType();
				iteratorvar.setName(iteratorVarName);
				iteratorvar.setType(VmeConstants.EMML_VAR_TYPE_STRING);
				variables.getVariable().add(iteratorvar);				
				
				//Crea un nodo script in cui effettuare la chiamata al java filter per il parsing in XML del risultato dell'invocazione JSON
				ScriptType script = of.createScriptType();
				script.setType(VmeConstants.EMML_SCRIPT_TYPE_JAVASCRIPT);							
				script.setInputvariables(sourceServiceId);
				script.setOutputvariable(scriptOutVarName);
				String scriptBody= scriptOutVarName+"="+VmeConstants.EMML_SCRIPT_JSONTOXML_IMPLEMENTATION+"("+sourceServiceId+");";
				script.setValue(scriptBody);
				
				rootTag.getEmmlMetaOrInputOrOutput().add(
						of.createMashupScript(script));				
				
				AssignType assign = of.createAssignType();
				assign.setFromvariable("$"+scriptOutVarName);
				assign.setOutputvariable("$"+assignOutVarName);
				
				rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupAssign(assign));
				
				//String arrayName = property.substring(property.lastIndexOf(":")+1,  property.lastIndexOf("}"));
				
				property = EmmlScriptUtils.parsePathVariable(property);
				property = property.replace("$", "/document").replace(".", "/");
				
				//property+="/"+arrayName;
				
				ForEachType foreach = of.createForEachType();
				foreach.setVariable("item");
				
				property += property.substring(property.lastIndexOf("/"));
				
				foreach.setItems("$"+assignOutVarName+ property);
				
				buildProjectEmmlInvoke.buildInvokeEmml(of, foreach, innerProcess.getUserData().getParameters(), variables, innerProcess.getUserData().getId(), innerProcess.getId());																										
				
				projectInnerProcessService.create(projectId, innerProcess.getUserData().getId());
				

				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document document = builder.newDocument();

				//Element macro = document.createElementNS(MACRO_NAMESPACE, MACRO_PREFIX + ":jsonappend");
				
				// IVAN
				Element macro = document.createElementNS(MACRO_NAMESPACE, MACRO_PREFIX + ":jsonconcat");
				
				macro.setAttribute("destination", "$"+nodeId);
				macro.setAttribute("source", "$"+innerProcess.getId());
				macro.setAttribute("rootname", innerProcess.getId()+"_{count($item/preceding-sibling::*)}");
				macro.setAttribute("result", nodeId);
				
				foreach.getAnnotateOrAppendresultOrAssign().add(macro);
								
				rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupForeach(foreach));
			}
		}		
	}
}
