package com.mediigea.vme2.emml.operator;


public interface IOperator {	
	public void buildEmmlCommand() throws Exception;
}
