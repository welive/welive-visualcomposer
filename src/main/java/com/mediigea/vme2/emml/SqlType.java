//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.29 at 04:59:29 PM CET 
//


package com.mediigea.vme2.emml;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sqlType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sqlType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attGroup ref="{http://www.openemml.org/2009-04-15/EMMLSchema}outputVarReq"/>
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="query" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="startrow" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="rowcount" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sqlType")
public class SqlType {

    @XmlAttribute
    protected String name;
    @XmlAttribute(required = true)
    protected String query;
    @XmlAttribute
    protected BigInteger startrow;
    @XmlAttribute
    protected BigInteger rowcount;
    @XmlAttribute(required = true)
    protected String outputvariable;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the query property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuery() {
        return query;
    }

    /**
     * Sets the value of the query property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuery(String value) {
        this.query = value;
    }

    /**
     * Gets the value of the startrow property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStartrow() {
        return startrow;
    }

    /**
     * Sets the value of the startrow property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStartrow(BigInteger value) {
        this.startrow = value;
    }

    /**
     * Gets the value of the rowcount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRowcount() {
        return rowcount;
    }

    /**
     * Sets the value of the rowcount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRowcount(BigInteger value) {
        this.rowcount = value;
    }

    /**
     * Gets the value of the outputvariable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutputvariable() {
        return outputvariable;
    }

    /**
     * Sets the value of the outputvariable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutputvariable(String value) {
        this.outputvariable = value;
    }

}
