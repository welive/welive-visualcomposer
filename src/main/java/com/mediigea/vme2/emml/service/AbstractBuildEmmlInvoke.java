package com.mediigea.vme2.emml.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mediigea.vme2.dto.ServiceOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.ScriptType;
import com.mediigea.vme2.emml.VariableType;
import com.mediigea.vme2.emml.VariablesType;
import com.mediigea.vme2.util.VmeConstants;

public abstract class AbstractBuildEmmlInvoke {
	private static final Logger logger = LoggerFactory.getLogger(AbstractBuildEmmlInvoke.class);
	
	protected ObjectFactory of;
	protected Mashup rootTag;
	protected VariablesType variables;
	protected ServiceOperationDTO operation;
	protected String nodeId;
	
	public AbstractBuildEmmlInvoke(ObjectFactory of, Mashup rootTag,
			VariablesType variables, ServiceOperationDTO operation,
			String nodeId) {
		
		this.of = of;
		this.rootTag = rootTag;
		this.variables = variables;
		this.operation = operation;
		this.nodeId = nodeId;
	}	
}
