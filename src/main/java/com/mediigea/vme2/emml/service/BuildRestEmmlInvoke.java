package com.mediigea.vme2.emml.service;

import io.swagger.util.Json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.ServiceCatalogDAO;
import com.mediigea.vme2.dto.InputField;
import com.mediigea.vme2.dto.RestOperationDTO;
import com.mediigea.vme2.dto.ServiceOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.emml.ConstructorType;
import com.mediigea.vme2.emml.DirectInvokeType;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.VariableType;
import com.mediigea.vme2.emml.VariablesType;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.rest.RestMethod;
import com.mediigea.vme2.rest.RestParameter;
import com.mediigea.vme2.service.RestOperationService;
import com.mediigea.vme2.util.EmmlScriptUtils;
import com.mediigea.vme2.util.VmeConstants;

public class BuildRestEmmlInvoke extends AbstractBuildEmmlInvoke {
	
	private static final Logger logger = LoggerFactory.getLogger(BuildRestEmmlInvoke.class);		
	
	@Autowired
	private RestOperationService restOperationService;
	
	@Autowired
	private ServiceCatalogDAO serviceCatalogDAO;
	
	public BuildRestEmmlInvoke(
			ObjectFactory of, 
			Mashup rootTag,
			VariablesType variables, 
			ServiceOperationDTO operation,
			String nodeId) {
		
		super(of, rootTag, variables, operation, nodeId);
	}

	public void buildInvokeEmml()
	{	
		//Prende i dettagli della rest operation per prendere l'indirizzo endpoint da invocare
		RestOperationDTO restOp = restOperationService.findRestOperationByIdAsDTO(operation.getId());	
		
		RestMethod restMethod = restOp.getExecMethod();
		
		String method = restMethod.getHttpMethod();
		String endpoint = restOp.getBaseURL() + restOp.getPath();
		//String restPayload=null;
		String mediaType = null;
		
		if(restMethod.getRepresentations()!=null)
		{
			//restPayload=restOp.getExecMethod().getRepresentations().get(0).getPayload().getContent();
			mediaType = restMethod.getRepresentations().get(0).getMediaType();
		}
		
		//Crea un tag di tipo direct invoke
		DirectInvokeType invoke = of.createDirectInvokeType();
		invoke.getOtherAttributes().put(new QName("checkresponsecode"), "true");
		
		//Prende la lista dei parametri con cui invocare l'operation
		List<ServiceParameterDTO> invokeInputParams = operation.getParameters();
		
		List<RestParameter> restParams = restMethod.getParameters();
		RestParameter restParam = null;
		
		// lista degli header
		HashMap<String, String> headers = new HashMap<String, String>();

		//List<InputField> inputs = new ArrayList<InputField>();
		
		//Scorre i parametri di input del servizio
		for(ServiceParameterDTO param : invokeInputParams)
		{			
			restParam = null;
			
			for(RestParameter rp : restParams)
			{
				if(rp.getName().compareTo(param.getName()) == 0)
				{
					restParam = rp;
					break;
				}
			}
			
			//Json.prettyPrint(restParam);
			//Json.prettyPrint(param);
			
			String value = param.getValue();			
			String defaultValue = restParam.getDefaultValue();
			
			if ((value == null || value.equals("")) && defaultValue!= null && !defaultValue.equals(""))
				value = defaultValue;

			if(value != null && !value.equals(""))
			{					
				String newValue = EmmlScriptUtils.inputValueDigester(param, value, of, rootTag, nodeId, variables);	
				
				/*
				InputField input = new InputField();
				input.setText(param.getName());
				input.setValue(newValue);
				inputs.add(input);
				*/
				
				//nel caso di POST setto il requestbody e l'HTTP Header
				//if(method.equalsIgnoreCase("POST"))
				if(restParam.getStyle().compareTo(RestParameter.ParameterStyle.BODY) == 0)
				{			
					invoke.setRequestbody(newValue);
					
					if(!headers.containsKey("content-type") && mediaType != null)
					{
						headers.put("content-type", mediaType);
					}
					
					if(!headers.containsKey("accept") && mediaType != null)
					{
						headers.put("accept", mediaType);
					}
					
					if(!headers.containsKey("content-type"))
					{
						headers.put("content-type", "application/json");
					}
				}
				else if(restParam.getStyle().compareTo(RestParameter.ParameterStyle.HEADER) == 0)
				{					
					headers.put(restParam.getName().toLowerCase(), newValue);
				}
				else
				{
					//Sostituisce eventualmente i parametri template presenti nel path endpoint
					//prototipo: http://gdata.youtube.com/feeds/api/users/{username}
					Pattern pEndPoint = Pattern.compile("\\{" + param.getName() + "\\}");
					Matcher mEndPoint = pEndPoint.matcher(endpoint);

					//Se l'endpoint contiene il parametro lo sostituisce altrimenti lo appende
					//come attributo del nodo invoke
					if(mEndPoint.find())
					{
						endpoint = mEndPoint.replaceFirst(newValue.replace("$", "\\$"));
						//endpoint = endpoint.replaceFirst(regex, parameter);
					}
					else
					{
						invoke.getOtherAttributes().put(new QName(param.getName()), newValue);
					}
				}
			}

		} // end for
		
		if(operation.isAuthenticationRequired())
		{
			int idService = operation.getServiceId();	

			ServiceCatalog service = serviceCatalogDAO.getById(idService);
			
			//EmmlScriptUtils.buildOauthScript(operation.getId(), service, inputs, rootTag, variables, endpoint, invoke, false);	
			
			// Crea un tag di tipo parametro di input per il token
			// oauth (access token)
			Mashup.Input oauthTokenParam = new Mashup.Input();
			oauthTokenParam.setType(VmeConstants.EMML_VAR_TYPE_STRING);
			String oauthTokenName = VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX + service.getId() + "_" + operation.getId();
			oauthTokenParam.setName(oauthTokenName);
			// Lo aggiunge al root tag
			rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupInput(oauthTokenParam));
			
			headers.put("Authorization", "Bearer " + "{$" + VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX + service.getId() + "_" + operation.getId() + "}");
		}
		 
		//creo l'header
		
		if(headers.size() > 0)
		{
			ConstructorType httpHeaderConstructor = of.createConstructorType();		
			String header = "<headers>";
					
			Iterator<String> keys = headers.keySet().iterator();
			
			String key = "";
			
			while(keys.hasNext())
			{
				key = keys.next();
				
				header += "<" + key + ">" + headers.get(key) + "</" + key + ">";
			}
			
			header += "</headers>";
	
			httpHeaderConstructor.setOutputvariable("httpHeader_"+nodeId);
			httpHeaderConstructor.getContent().add(header);
	
			invoke.setHeader("$httpHeader_"+nodeId);
	
			rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupConstructor(httpHeaderConstructor));
		}
		
		//

		//Completa la costruzione del tag endpoint settando url e nome della variabile di output
		invoke.setEndpoint(endpoint);
		invoke.setOutputvariable("$"+nodeId);							
		invoke.setMethod(method);
		
		//Appende il tag direct invoke al roottag
		rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupDirectinvoke(invoke));

		//Crea una variabile di output dove andare a storare il risultato dell'invocazione
		VariableType variable = of.createVariableType();							
		variable.setName(nodeId);
		variable.setType(VmeConstants.EMML_VAR_TYPE_STRING);
		variables.getVariable().add(variable);		
	}
}
