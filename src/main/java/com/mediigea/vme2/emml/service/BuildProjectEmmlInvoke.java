package com.mediigea.vme2.emml.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dao.ServiceCatalogDAO;
import com.mediigea.vme2.dto.InputField;
import com.mediigea.vme2.dto.InputShapeDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.dto.UD_InputShapeDTO;
import com.mediigea.vme2.emml.DirectInvokeType;
import com.mediigea.vme2.emml.ForEachType;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.VariableType;
import com.mediigea.vme2.emml.VariablesType;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.exceptions.SelectShapeException;
import com.mediigea.vme2.service.ProjectOperationService;
import com.mediigea.vme2.service.ProjectService;
import com.mediigea.vme2.util.EmmlScriptUtils;
import com.mediigea.vme2.util.VmeConstants;

public class BuildProjectEmmlInvoke implements IBuildEmmlInvoke {

	@Autowired
	private ProjectOperationService projectOperationService;

	@Autowired
	private ServiceCatalogDAO serviceCatalogDAO;
	
	@Autowired
	private ProjectService projectService;

	private static final Logger logger = LoggerFactory
			.getLogger(BuildProjectEmmlInvoke.class);

	
	private String emmlEngineContext;

	/**
	 * @return the emmlEngineContext
	 */
	public String getEmmlEngineContext() {
		return emmlEngineContext;
	}

	/**
	 * @param emmlEngineContext the emmlEngineContext to set
	 */
	public void setEmmlEngineContext(String emmlEngineContext) {
		this.emmlEngineContext = emmlEngineContext;
	}

	@Override
	public void buildInvokeEmml(ObjectFactory of, Mashup rootTag, List<ServiceParameterDTO> parameters, VariablesType variables, int projectId, String nodeId) {
		String endpoint = emmlEngineContext + "/" + projectId;
		DirectInvokeType invoke = of.createDirectInvokeType();

		List<InputField> inputs = new ArrayList<InputField>();
		// Scorre i parametri di input del servizio
		for (ServiceParameterDTO param : parameters) {
			String value = param.getValue();
			String defaultValue = param.getDefaultValue();

			if ((value == null || value.equals("")) && defaultValue != null
					&& !defaultValue.equals(""))
				value = defaultValue;

			if (value != null && !value.equals("")) {

				String newValue = EmmlScriptUtils.inputValueDigester(param, value, of, rootTag, nodeId, variables);

				this.inputInvokeGenerator(invoke, endpoint, newValue, param, false, inputs);				
			}
		}

		this.oauthScriptGenerator(of, rootTag, variables, invoke, endpoint, projectId, nodeId, inputs);		
		
		// Completa la costruzione del tag endpoint settando url e nome della
		// variabile di output
		invoke.setEndpoint(endpoint);
		invoke.setOutputvariable("$" + nodeId);

		// Crea una variabile di output dove andare a storare il risultato
		// dell'invocazione
		VariableType variable = of.createVariableType();
		variable.setName(nodeId);
		variable.setType(VmeConstants.EMML_VAR_TYPE_STRING);
		variables.getVariable().add(variable);	
		
		// Appende il tag direct invoke al roottag		
		rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupDirectinvoke(invoke));		
	}	
	
	@Override
	public void buildInvokeEmml(ObjectFactory of, ForEachType rootTag, List<ServiceParameterDTO> parameters, VariablesType variables, int projectId, String nodeId) 
	{
		String endpoint = emmlEngineContext + "/" + projectId;
		DirectInvokeType invoke = of.createDirectInvokeType();

		List<InputField> inputs = new ArrayList<InputField>();
		
		 // retrieve mashup inputs		
		List<InputShapeDTO> inputsDTO = null;
		
		UD_InputShapeDTO userData = null;
		
        try 
        {
        	inputsDTO = projectService.readInputShapesList(projectId);
		} 
        catch (SelectShapeException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Scorre i parametri di input del servizio
		for (ServiceParameterDTO param : parameters) 
		{
			String value = param.getValue();
			String defaultValue = param.getDefaultValue();
			
			// search current param in the list of mashup inputs			
			if(inputsDTO != null)
			{
				userData = null;
				
				for(InputShapeDTO inputDTO : inputsDTO)
				{
					if(inputDTO.getId().compareTo(param.getName()) == 0)
					{
						userData = inputDTO.getUserData();
						break;
					}					
				}
			}

			if ((value == null || value.equals("")) && defaultValue != null && !defaultValue.equals(""))
			{
				value = defaultValue;
			}

			if (value != null && !value.equals("")) 
			{
				String newValue = value;
				
				// if input is not constant
				if(!userData.isIsConstant())
				{
					String variableName = rootTag.getVariable();
					//$entry/title/_-t/string()
					//iterator_0:$.title.$t 
					
					value = EmmlScriptUtils.getJsonPathFromPath(value);
					
					// IVAN when json path equals $ (entire object)
					if(value.compareTo("$") == 0)
						value = "";
					else
						value = value.replace("$.", "/").replace(".", "/").replace("$", "_-");
					
					newValue = "$"+variableName+value+"/string()";
				}

				this.inputInvokeGenerator(invoke, endpoint, newValue, param, userData.isIsConstant(), inputs);
			}
		}
		
		// IVAN
		// add oauth_tokens as inputs of directinvoke
		
		//this.oauthScriptGenerator(of, rootTag, variables, invoke, endpoint, projectId, nodeId, inputs);
		
		List<ProjectOperation> projectOperations = projectOperationService.getAllByProject(projectId);
		
		ServiceParameterDTO param = null;
		
		for (ProjectOperation oper : projectOperations) 
		{

			if (oper.getRestOperation() != null) 
			{
				RestOperation operation = oper.getRestOperation();

				if (operation.getAuthentication().equalsIgnoreCase("true")) 
				{
					int idService = operation.getServiceCatalog().getId();
					
					param = new ServiceParameterDTO();
					param.setName(VmeConstants.EMML_OAUTH_ACCESSTOKEN_PREFIX + idService + "_" + operation.getId());
					
					this.inputInvokeGenerator(invoke, endpoint, "$" + param.getName(), param, false, inputs);
				}
			}
		}
		
		//
		
		// Completa la costruzione del tag endpoint settando url e nome della
		// variabile di output
		invoke.setEndpoint(endpoint);
		invoke.setOutputvariable("$" + nodeId);

		// Crea una variabile di output dove andare a storare il risultato
		// dell'invocazione
		VariableType variable = of.createVariableType();
		variable.setName(nodeId);
		variable.setType(VmeConstants.EMML_VAR_TYPE_STRING);
		variables.getVariable().add(variable);	
		
		// Appende il tag direct invoke al roottag		
		rootTag.getAnnotateOrAppendresultOrAssign().add(of.createMashupDirectinvoke(invoke));
	}	
	
	private void inputInvokeGenerator(DirectInvokeType invoke, String endpoint, String newValue, ServiceParameterDTO param, boolean isConstant, List<InputField> inputs)
	{
		// Sostituisce eventualmente i parametri template presenti nel
		// path endpoint
		// prototipo:
		// http://gdata.youtube.com/feeds/api/users/{username}
		Pattern pEndPoint = Pattern.compile("\\{" + param.getName() + "\\}");
		Matcher mEndPoint = pEndPoint.matcher(endpoint);
		
		String newValue2 = newValue;
		
		// Se l'endpoint contiene il parametro lo sostituisce altrimenti
		// lo appende
		// come attributo del nodo invoke
		if (mEndPoint.find()) 
		{
			endpoint = mEndPoint.replaceFirst(newValue.replace("$",	"\\$"));
			// endpoint = endpoint.replaceFirst(regex, parameter);
		} 
		else 
		{
			// IVAN
			// remove {} around param name (for black boxes)
			if(newValue.charAt(0) == '{' && newValue.charAt(newValue.length() - 1) == '}')
				newValue2 = newValue.substring(1, newValue.length() - 1);
				
			if(isConstant)
				invoke.getOtherAttributes().put(new QName(param.getName()), newValue2);
			else
				invoke.getOtherAttributes().put(new QName(param.getName()), "{"+newValue2+"}");
		}

		InputField input = new InputField();
		input.setText(param.getName());
		input.setValue(newValue2);
		inputs.add(input);
	}
	
	private void oauthScriptGenerator(
			ObjectFactory of,
			Object rootTag,
			VariablesType variables,
			DirectInvokeType invoke, 
			String endpoint, 
			int projectId, 
			String nodeId, 			
			List<InputField> inputs){
		
		List<ProjectOperation> projectOperations = projectOperationService
				.getAllByProject(projectId);

		for (ProjectOperation oper : projectOperations) {

			if (oper.getRestOperation() != null) {
				RestOperation operation = oper.getRestOperation();

				if (operation.getAuthentication().equalsIgnoreCase("true")) {
					int idService = operation.getServiceCatalog().getId();

					ServiceCatalog service = serviceCatalogDAO
							.getById(idService);

					EmmlScriptUtils.buildOauthScript(operation.getId(),
							service, inputs, rootTag, variables, endpoint,
							invoke, true);
				}
			}
		}	
	}
}