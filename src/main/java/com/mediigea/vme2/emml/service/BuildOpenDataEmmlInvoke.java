package com.mediigea.vme2.emml.service;

import it.eng.PropertyGetter;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.mediigea.vme2.dao.ServiceCatalogDAO;
import com.mediigea.vme2.dto.BlackboxDTO;
import com.mediigea.vme2.dto.InputField;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.emml.AssignType;
import com.mediigea.vme2.emml.ConstructorType;
import com.mediigea.vme2.emml.DirectInvokeType;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.ScriptType;
import com.mediigea.vme2.emml.VariableType;
import com.mediigea.vme2.emml.VariablesType;
import com.mediigea.vme2.entity.OpenDataResource;
import com.mediigea.vme2.entity.ProjectOperation;
import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;
import com.mediigea.vme2.service.BlackboxService;
import com.mediigea.vme2.service.ODSDatasetService;
import com.mediigea.vme2.service.ODSResourceService;
import com.mediigea.vme2.service.ProjectBlackboxService;
import com.mediigea.vme2.service.ProjectOperationService;
import com.mediigea.vme2.util.EmmlScriptUtils;
import com.mediigea.vme2.util.VmeConstants;

public class BuildOpenDataEmmlInvoke {

	@Autowired
	private ProjectBlackboxService projectBlackboxService;

	@Autowired
	private ProjectOperationService projectOperationService;

	@Autowired
	private ServiceCatalogDAO serviceCatalogDAO;
	
	@Autowired
	private ODSDatasetService odsDatasetService;
	
	@Autowired
	private ODSResourceService odsResourceService;

	private static final Logger logger = LoggerFactory
			.getLogger(BuildOpenDataEmmlInvoke.class);

	private ObjectFactory of;
	private Mashup rootTag;
	private VariablesType variables;
	private String nodeId;
	private OpenDataResource openDataResource;
	private String odataEngineContext;
	
	// IVAN
	private String datasetId;
	private String resourceId;
	private String query;

	@Autowired
	private BlackboxService blackboxService;

	public BuildOpenDataEmmlInvoke(ObjectFactory of, Mashup rootTag,
			VariablesType variables,
			OpenDataResource openDataResource, String odataEngineContext, String nodeId) {
		super();
		this.of = of;
		this.rootTag = rootTag;
		this.variables = variables;
		this.openDataResource = openDataResource;
		this.odataEngineContext = odataEngineContext;
		this.nodeId = nodeId;
	}
	
	// IVAN
	public BuildOpenDataEmmlInvoke(ObjectFactory of, Mashup rootTag,
			VariablesType variables,
			String datasetId, String resourceId, String query, String nodeId) {
		super();
		this.of = of;
		this.rootTag = rootTag;
		this.variables = variables;
		
		this.datasetId = datasetId;
		this.resourceId = resourceId;		
		this.query = query.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;"); // http://mdc.jackbe.com/prestodocs/v3.2/wires/propertyEscapeChars.html
		
		this.nodeId = nodeId;
	}

	public void buildInvokeEmml() {
		String endpoint = odataEngineContext + "/" + openDataResource.getId()+"/read.json";
		DirectInvokeType invoke = of.createDirectInvokeType();

		// Completa la costruzione del tag endpoint settando url e nome della
		// variabile di output
		invoke.setEndpoint(endpoint);
		invoke.setOutputvariable("$" + nodeId);

		// Appende il tag direct invoke al roottag
		rootTag.getEmmlMetaOrInputOrOutput().add(
				of.createMashupDirectinvoke(invoke));

		// Crea una variabile di output dove andare a storare il risultato
		// dell'invocazione
		VariableType variable = of.createVariableType();
		variable.setName(nodeId);
		variable.setType(VmeConstants.EMML_VAR_TYPE_STRING);
		variables.getVariable().add(variable);
	}
	
	// IVAN
	// build script to invoke Query Mapper
	public void buildInvokeEmml2() 
	{		
		String endpoint = PropertyGetter.getProperty("ods.queryResource.serviceUrl")
				.replace("!DATASET_ID!", datasetId).replace("!RESOURCE_ID!", resourceId);
		
		DirectInvokeType invoke = of.createDirectInvokeType();
		invoke.setMethod("POST");
		invoke.setRequestbody("$" + nodeId + "_requestBody");
		invoke.setEndpoint(endpoint);
		invoke.setOutputvariable("$" + nodeId);
		
		//creo l'header
		/*
		ConstructorType httpHeaderConstructor = of.createConstructorType();		
		String header = "<headers>" + "<Content-type>" + MediaType.APPLICATION_JSON + "</Content-type>" + "</headers>";
		
		httpHeaderConstructor.setOutputvariable(nodeId + "_httpHeader");
		httpHeaderConstructor.getContent().add(header);

		invoke.setHeader("$" + nodeId + "_httpHeader");
		
		rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupConstructor(httpHeaderConstructor));
		*/
		
		AssignType assign = of.createAssignType();
		assign.setLiteral(query);
		assign.setOutputvariable("$" + nodeId + "_requestBody");
		
		rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupAssign(assign));

		// Appende il tag direct invoke al root tag
		rootTag.getEmmlMetaOrInputOrOutput().add(
				of.createMashupDirectinvoke(invoke));

		// Crea una variabile di output dove andare a storare il risultato
		// dell'invocazione
		VariableType variable = of.createVariableType();
		variable.setName(nodeId);
		variable.setType(VmeConstants.EMML_VAR_TYPE_STRING);
		variables.getVariable().add(variable);
		
		variable = of.createVariableType();
		variable.setName(nodeId + "_requestBody");
		variable.setType(VmeConstants.EMML_VAR_TYPE_STRING);
		variables.getVariable().add(variable);		
		
	}
}
