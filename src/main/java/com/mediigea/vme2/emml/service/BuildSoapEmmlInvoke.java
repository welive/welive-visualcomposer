package com.mediigea.vme2.emml.service;

import java.util.HashMap;
import java.util.List;

import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mediigea.vme2.dto.ServiceOperationDTO;
import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.dto.SoapOperationDTO;
import com.mediigea.vme2.emml.AssignType;
import com.mediigea.vme2.emml.ConstructorType;
import com.mediigea.vme2.emml.DirectInvokeType;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.ScriptType;
import com.mediigea.vme2.emml.VariableType;
import com.mediigea.vme2.emml.VariablesType;
import com.mediigea.vme2.service.SoapOperationService;
import com.mediigea.vme2.util.EmmlScriptUtils;
import com.mediigea.vme2.util.FormatConverter;
import com.mediigea.vme2.util.VmeConstants;

public class BuildSoapEmmlInvoke extends AbstractBuildEmmlInvoke{

	private static final Logger logger = LoggerFactory.getLogger(BuildSoapEmmlInvoke.class);		
	
	@Autowired
	private SoapOperationService soapOperationService;

	public BuildSoapEmmlInvoke(
			ObjectFactory of, 
			Mashup rootTag,
			VariablesType variables, 
			ServiceOperationDTO operation,
			String nodeId) {
		
		super(of, rootTag, variables, operation, nodeId);			
	}
	
	public void buildInvokeEmml() throws Exception{

		//Prende i dettagli della rest operation per prendere l'indirizzo endpoint da invocare
		SoapOperationDTO soapOp = soapOperationService
				.findSoapOperationAsDTO(operation.getId());

		//Crea un tag di tipo direct invoke
		DirectInvokeType invoke = of.createDirectInvokeType();
		invoke.setMethod("post");

		HashMap <String, String> parameters = new HashMap<String, String>();

		//Prende la lista dei parametri con cui invocare l'operation
		List<ServiceParameterDTO> invokeInputParams = operation.getParameters();				

		//Scorre i parametri di input del servizio
		for(ServiceParameterDTO param : invokeInputParams){
			String value = param.getValue();

			if(value!=null && !"".equals(value)){						

				String newValue = EmmlScriptUtils.inputValueDigester(param, value, of, rootTag, nodeId, variables);

				parameters.put(param.getName(), newValue);
			}					
		}

		SOAPMessage soapRequest = soapOperationService.createSoapRequestPrototype(soapOp, parameters);			

		try {
			SOAPPart soapPart = soapRequest.getSOAPPart();

			SOAPEnvelope envelope = soapPart.getEnvelope();					

			String envelopeStr = FormatConverter.SOAPEnvelopeToString(envelope);			
			ConstructorType soapRequestConstructor = of.createConstructorType();
			soapRequestConstructor.setOutputvariable("soapRequest");
			soapRequestConstructor.getContent().add(envelopeStr);

			invoke.setRequestbody("$soapRequest");

			rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupConstructor(soapRequestConstructor));

			//Prende la soap action e crea un tag headers
			String soapAction = soapOp.getSoapAction();
			ConstructorType httpHeaderConstructor = of.createConstructorType();		
			String header = "<headers>"+
					"<SOAPAction>"+soapAction+"</SOAPAction>"+
					"<Content-type>text/xml</Content-type>"+
					"</headers>";

			httpHeaderConstructor.setOutputvariable("httpHeader");
			httpHeaderConstructor.getContent().add(header);

			invoke.setHeader("$httpHeader");

			rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupConstructor(httpHeaderConstructor));
		} catch (SOAPException e) {			
			logger.error("Error building soap message");
			throw e;
		}

		//Completa la costruzione del tag endpoint settando url e nome della variabile di output
		//Setta l'endpoint del servizio
		invoke.setEndpoint(soapOp.getEndPoint());
		invoke.setOutputvariable("$"+nodeId+"_typedoc");							

		//Appende il tag direct invoke al roottag
		rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupDirectinvoke(invoke));

		//Crea una variabile di output dove andare a storare il risultato dell'invocazione
		VariableType variable_doc = of.createVariableType();							
		variable_doc.setName(nodeId+"_typedoc");
		variable_doc.setType(VmeConstants.EMML_VAR_TYPE_DOCUMENT);
		variables.getVariable().add(variable_doc);		

		VariableType variable = of.createVariableType();							
		variable.setName(nodeId);
		variable.setType(VmeConstants.EMML_VAR_TYPE_STRING);
		variables.getVariable().add(variable);				

		AssignType assign = of.createAssignType();
		assign.setOutputvariable("$"+nodeId+"_typedoc");
		assign.setFromexpr("$"+nodeId+"_typedoc"+"/*:Envelope/*:Body/*");

		rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupAssign(assign));

		/*
		 * <script outputvariable="GetCountriesByContinent4_0_string" inputvariables="GetCountriesByContinent4_0" type="text/javascript">GetCountriesByContinent4_0_string=Packages.com.mediigea.jsonutils.JsonUtils.getSoapAsJson(GetCountriesByContinent4_0);</script>
		 */
		ScriptType script = of.createScriptType();
		script.setType(VmeConstants.EMML_SCRIPT_TYPE_JAVASCRIPT);							
		script.setInputvariables(nodeId+"_typedoc");
		script.setOutputvariable(nodeId);
		String scriptBody= nodeId+"="+VmeConstants.EMML_SCRIPT_VALUE_CONVERTER_IMPLEMENTATION+"("+nodeId+"_typedoc"+");";
		script.setValue(scriptBody);

		//Inserisce il tag script nel rootTag
		rootTag.getEmmlMetaOrInputOrOutput().add(of.createMashupScript(script));
	}	
}
