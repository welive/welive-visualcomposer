package com.mediigea.vme2.emml.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.mediigea.vme2.dto.ServiceParameterDTO;
import com.mediigea.vme2.emml.ForEachType;
import com.mediigea.vme2.emml.Mashup;
import com.mediigea.vme2.emml.ObjectFactory;
import com.mediigea.vme2.emml.VariablesType;

@Transactional
public interface IBuildEmmlInvoke {
	public void buildInvokeEmml(ObjectFactory of, Mashup rootTag, List<ServiceParameterDTO> parameters, VariablesType variables, int projectId, String nodeId) throws Exception;
	public void buildInvokeEmml(ObjectFactory of, ForEachType rootTag, List<ServiceParameterDTO> parameters, VariablesType variables, int projectId, String nodeId) throws Exception;
}
