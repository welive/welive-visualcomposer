//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.29 at 04:59:29 PM CET 
//


package com.mediigea.vme2.emml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for macroType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="macroType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;group ref="{http://www.openemml.org/2009-04-15/EMMLSchema}Declarations"/>
 *         &lt;group ref="{http://www.openemml.org/2009-04-15/EMMLSchema}VariablesGroup"/>
 *         &lt;group ref="{http://www.openemml.org/2009-04-15/EMMLSchema}Statements"/>
 *       &lt;/choice>
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "macroType", propOrder = {
    "emmlMetaOrInputOrOutput"
})
public class MacroType {

    @XmlElementRefs({
        @XmlElementRef(name = "assign", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "foreach", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "parallel", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "select", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "template", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "join", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "output", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "sqlCommit", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "input", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "sort", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "group", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "if", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "script", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "sqlRollback", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "constructor", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "appendresult", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "filter", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "annotate", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "user-meta", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "emml-meta", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "assert", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "variable", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "display", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "merge", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "sqlBeginTransaction", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "sql", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "invoke", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "directinvoke", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "xslt", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "variables", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "for", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "datasource", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "while", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class),
        @XmlElementRef(name = "sqlUpdate", namespace = "http://www.openemml.org/2009-04-15/EMMLSchema", type = JAXBElement.class)
    })
    protected List<JAXBElement<?>> emmlMetaOrInputOrOutput;
    @XmlAttribute
    protected String name;

    /**
     * Gets the value of the emmlMetaOrInputOrOutput property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the emmlMetaOrInputOrOutput property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmmlMetaOrInputOrOutput().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link AssignType }{@code >}
     * {@link JAXBElement }{@code <}{@link ParallelType }{@code >}
     * {@link JAXBElement }{@code <}{@link ForEachType }{@code >}
     * {@link JAXBElement }{@code <}{@link SelectType }{@code >}
     * {@link JAXBElement }{@code <}{@link JoinType }{@code >}
     * {@link JAXBElement }{@code <}{@link TemplateType }{@code >}
     * {@link JAXBElement }{@code <}{@link com.mediigea.vme2.emml.Mashup.Output }{@code >}
     * {@link JAXBElement }{@code <}{@link SqlTransactionType }{@code >}
     * {@link JAXBElement }{@code <}{@link com.mediigea.vme2.emml.Mashup.Input }{@code >}
     * {@link JAXBElement }{@code <}{@link GroupType }{@code >}
     * {@link JAXBElement }{@code <}{@link SortType }{@code >}
     * {@link JAXBElement }{@code <}{@link IfType }{@code >}
     * {@link JAXBElement }{@code <}{@link ScriptType }{@code >}
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * {@link JAXBElement }{@code <}{@link ConstructorType }{@code >}
     * {@link JAXBElement }{@code <}{@link AppendResultType }{@code >}
     * {@link JAXBElement }{@code <}{@link AnnotateType }{@code >}
     * {@link JAXBElement }{@code <}{@link FilterType }{@code >}
     * {@link JAXBElement }{@code <}{@link AssertType }{@code >}
     * {@link JAXBElement }{@code <}{@link MetadataType }{@code >}
     * {@link JAXBElement }{@code <}{@link com.mediigea.vme2.emml.Mashup.UserMeta }{@code >}
     * {@link JAXBElement }{@code <}{@link VariableType }{@code >}
     * {@link JAXBElement }{@code <}{@link DisplayType }{@code >}
     * {@link JAXBElement }{@code <}{@link SqlTransactionType }{@code >}
     * {@link JAXBElement }{@code <}{@link UnionType }{@code >}
     * {@link JAXBElement }{@code <}{@link SqlType }{@code >}
     * {@link JAXBElement }{@code <}{@link InvokeType }{@code >}
     * {@link JAXBElement }{@code <}{@link XsltType }{@code >}
     * {@link JAXBElement }{@code <}{@link DirectInvokeType }{@code >}
     * {@link JAXBElement }{@code <}{@link VariablesType }{@code >}
     * {@link JAXBElement }{@code <}{@link ForType }{@code >}
     * {@link JAXBElement }{@code <}{@link com.mediigea.vme2.emml.Mashup.Datasource }{@code >}
     * {@link JAXBElement }{@code <}{@link WhileType }{@code >}
     * {@link JAXBElement }{@code <}{@link SqlUpdateType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getEmmlMetaOrInputOrOutput() {
        if (emmlMetaOrInputOrOutput == null) {
            emmlMetaOrInputOrOutput = new ArrayList<JAXBElement<?>>();
        }
        return this.emmlMetaOrInputOrOutput;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

}
