//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.29 at 04:59:29 PM CET 
//


package com.mediigea.vme2.emml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for xsltType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="xsltType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attGroup ref="{http://www.openemml.org/2009-04-15/EMMLSchema}outputVarReq"/>
 *       &lt;attribute name="script" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="inputvariable" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "xsltType")
public class XsltType {

    @XmlAttribute(required = true)
    protected String script;
    @XmlAttribute
    protected String inputvariable;
    @XmlAttribute(required = true)
    protected String outputvariable;

    /**
     * Gets the value of the script property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScript() {
        return script;
    }

    /**
     * Sets the value of the script property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScript(String value) {
        this.script = value;
    }

    /**
     * Gets the value of the inputvariable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInputvariable() {
        return inputvariable;
    }

    /**
     * Sets the value of the inputvariable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInputvariable(String value) {
        this.inputvariable = value;
    }

    /**
     * Gets the value of the outputvariable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutputvariable() {
        return outputvariable;
    }

    /**
     * Sets the value of the outputvariable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutputvariable(String value) {
        this.outputvariable = value;
    }

}
