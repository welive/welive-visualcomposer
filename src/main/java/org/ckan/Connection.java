package org.ckan;

import it.eng.PropertyGetter;

import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.entity.StringEntity;

/**
 * Connection holds the connection details for this session
 *
 * @author      Ross Jones <ross.jones@okfn.org>
 * @version     1.7
 * @since       2012-05-01
 */
public final class Connection {

    private String m_host;
    private int m_port;
    private String _apikey = null;

    public Connection(  ) {
        this("http://datahub.io", 80);
    }

    public Connection( String host  ) {
        this( host, 80 );
    }

    public Connection( String host, int port ) {
        this.m_host = host;
        this.m_port = port;

        try {
            URL u = new URL( this.m_host + ":" + this.m_port + "/api");
        } catch ( MalformedURLException mue ) {
            System.out.println(mue);
        }

    }

    public void setApiKey( String key ) {
        this._apikey = key;
    }


    /**
    * Makes a POST request
    *
    * Submits a POST HTTP request to the CKAN instance configured within
    * the constructor, returning tne entire contents of the response.
    *
    * @param  path The URL path to make the POST request to
    * @param  data The data to be posted to the URL
    * @returns The String contents of the response
    * @throws A CKANException if the request fails
    */
    protected String Post(String path, String data)
        throws CKANException {
        URL url = null;

        try {
            //url = new URL( this.m_host + ":" + this.m_port + path);
         url = new URL( this.m_host + path);
        } catch ( MalformedURLException mue ) {
            System.err.println(mue);
            return null;
        }

        String body = "";

        HttpClient httpclient = new DefaultHttpClient();
        /*
      * Set an HTTP proxy if it is specified in system properties.
      * 
      * http://docs.oracle.com/javase/6/docs/technotes/guides/net/proxies.html
      * http://hc.apache.org/httpcomponents-client-ga/httpclient/examples/org/apache/http/examples/client/ClientExecuteProxy.java
      */
	 if( isSet(PropertyGetter.getProperty("useProxy")) ) 
	 {
	
	  int port = 80;
	  if( isSet(PropertyGetter.getProperty("proxyPort")) ) 
	  {
	   port = Integer.parseInt(PropertyGetter.getProperty("proxyPort"));
	  }
	  HttpHost proxy = new HttpHost(PropertyGetter.getProperty("proxyHost"), port, "http");
	  httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
	  
	  if( isSet(PropertyGetter.getProperty("useAuth")) )
	  {
		 ((AbstractHttpClient) httpclient).getCredentialsProvider().setCredentials(new AuthScope(PropertyGetter.getProperty("proxyHost"), port),
		(Credentials) new UsernamePasswordCredentials(PropertyGetter.getProperty("proxyUser"),PropertyGetter.getProperty("proxyPassword")));
	  }
	 }
     try {
            HttpPost postRequest = new HttpPost(url.toString());
            System.out.println("URL: " + url);
            postRequest.setHeader( "X-CKAN-API-Key", this._apikey );

      StringEntity input = new StringEntity(data);
      input.setContentType("application/json");
      postRequest.setEntity(input);

            HttpResponse response = httpclient.execute(postRequest);
            int statusCode = response.getStatusLine().getStatusCode();

            BufferedReader rd = new BufferedReader(
                 new InputStreamReader(response.getEntity().getContent()));
         
         StringBuffer result = new StringBuffer();
         String line = "";
         while ((line = rd.readLine()) != null) {
          result.append(line);
         }
         
         body = result.toString();
        } catch(Exception ioe ) {
            ioe.printStackTrace();
        } finally {
            httpclient.getConnectionManager().shutdown();
        }

        return body;
    }
    private static boolean isSet(String string) {
     return string != null && string.length() > 0;
    }
}