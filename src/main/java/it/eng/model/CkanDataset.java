package it.eng.model;


public class CkanDataset 
{
	private String ckanServerId;
	private String datasetId;
	private String resourceId;
	
	public String getCkanServerId() {
		return ckanServerId;
	}
	public void setCkanServerId(String ckanServerId) {
		this.ckanServerId = ckanServerId;
	}
	public String getDatasetId() {
		return datasetId;
	}
	public void setDatasetId(String datasetId) {
		this.datasetId = datasetId;
	}
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
}
