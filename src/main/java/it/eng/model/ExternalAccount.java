package it.eng.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalAccount 
{
	String firstName;
	String lastName;
	String email;
	int ccUserId;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	String secret;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getCcUserId() {
		return ccUserId;
	}
	public void setCcUserId(int ccUserId) {
		this.ccUserId = ccUserId;
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}	
}
