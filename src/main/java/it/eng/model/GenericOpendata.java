package it.eng.model;

public class GenericOpendata 
{
	private String opendataName;
	private String opendataUrl;
	private String opendataFile;
	private String mimeType;
	private String filename;
	private long size;
	private String separator;
	
	public String getOpendataName() {
		return opendataName;
	}
	public void setOpendataName(String opendataName) {
		this.opendataName = opendataName;
	}
	public String getOpendataUrl() {
		return opendataUrl;
	}
	public void setOpendataUrl(String opendataUrl) {
		this.opendataUrl = opendataUrl;
	}
	public String getOpendataFile() {
		return opendataFile;
	}
	public void setOpendataFile(String opendataFile) {
		this.opendataFile = opendataFile;
	}
	public String getSeparator() {
		return separator;
	}
	public void setSeparator(String separator) {
		this.separator = separator;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
}
