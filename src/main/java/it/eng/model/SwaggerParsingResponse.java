package it.eng.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mediigea.vme2.entity.RestOperation;
import com.mediigea.vme2.entity.ServiceCatalog;

public class SwaggerParsingResponse
{
	private ServiceCatalog serviceCatalog;
	private Set<RestOperation> operations;
	private List<String> errors;
	private List<String> warnings;
	
	public SwaggerParsingResponse()
	{
		serviceCatalog = null;
		setOperations(new HashSet<RestOperation>());
		errors = new ArrayList<String>();
		warnings = new ArrayList<String>();
	}
	
	public ServiceCatalog getServiceCatalog() {
		return serviceCatalog;
	}
	public void setServiceCatalog(ServiceCatalog serviceCatalog) {
		this.serviceCatalog = serviceCatalog;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	public List<String> getWarnings() {
		return warnings;
	}
	public void setWarnings(List<String> warnings) {
		this.warnings = warnings;
	}
	public Set<RestOperation> getOperations() {
		return operations;
	}

	public void setOperations(Set<RestOperation> operations) {
		this.operations = operations;
	}
	
	public void addError(String err) {
		errors.add(err);
	}
	public void addWarning(String warn) {
		warnings.add(warn);
	}
	
	public void addOperation(RestOperation op) {
		operations.add(op);
	}

	
}
