package it.eng.model;

public class RemoteServiceInfo 
{
	private String email;
	private String password;
	private int project_id;
	
	public String getEmail() 
	{
		return email;
	}
	
	public void setEmail(String email) 
	{
		this.email = email;
	}
	
	public String getPassword() 
	{
		return password;
	}
	
	public void setPassword(String password) 
	{
		this.password = password;
	}

	public int getProject_id() 
	{
		return project_id;
	}

	public void setProject_id(int project_id) 
	{
		this.project_id = project_id;
	}
}
