package it.eng.model;

public class NewScreenshot 
{
	private String idea_id;
	private String encodedScreenShot;
	private String description;
	private String imgMimeType;
	private String username;	

	public String getIdea_id() {
		return idea_id;
	}
	public void setIdea_id(String idea_id) {
		this.idea_id = idea_id;
	}
	public String getEncodedScreenShot() {
		return encodedScreenShot;
	}
	public void setEncodedScreenShot(String encodedScreenShot) {
		this.encodedScreenShot = encodedScreenShot;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImgMimeType() {
		return imgMimeType;
	}
	public void setImgMimeType(String imgMimeType) {
		this.imgMimeType = imgMimeType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
