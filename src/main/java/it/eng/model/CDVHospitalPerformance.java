package it.eng.model;

import java.util.List;

public class CDVHospitalPerformance 
{
	private String success;
	private List<CDVPerformance> list;
	
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public List<CDVPerformance> getList() {
		return list;
	}
	public void setList(List<CDVPerformance> list) {
		this.list = list;
	}
}
