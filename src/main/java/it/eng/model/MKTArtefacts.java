package it.eng.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MKTArtefacts 
{
	List<RemoteArtefact> artefacts;

	public List<RemoteArtefact> getArtefacts() {
		return artefacts;
	}

	public void setArtefacts(List<RemoteArtefact> artefacts) {
		this.artefacts = artefacts;
	}

}
