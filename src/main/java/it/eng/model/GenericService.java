package it.eng.model;

public class GenericService 
{
	private String serviceName;
	private String serviceUrl;
	private String serviceFile;
	private String serviceType;
	
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	public String getServiceFile() {
		return serviceFile;
	}
	public void setServiceFile(String serviceFile) {
		this.serviceFile = serviceFile;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
}
