package it.eng.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.eng.metamodel.definitions.BuildingBlock;

public class ProjectPublication 
{
	@JsonProperty("mashupid")
	private int mashupId;
	@JsonProperty("mockupid")
	private int mockupId;
	@JsonProperty("ideaid")
	private int ideaId;
	@JsonProperty("ccuserid")
	private int ccUserId;
	private String type;
	
	private Object lusdl;
	
	public int getMashupId() {
		return mashupId;
	}

	public void setMashupId(int mashupId) {
		this.mashupId = mashupId;
	}
	
	public int getMockupId() {
		return mockupId;
	}

	public void setMockupId(int mockupId) {
		this.mockupId = mockupId;
	}

	public int getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(int ideaId) {
		this.ideaId = ideaId;
	}

	public int getCcUserId() {
		return ccUserId;
	}

	public void setCcUserId(int ccUserId) {
		this.ccUserId = ccUserId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getLusdl() {
		return lusdl;
	}

	public void setLusdl(Object lusdl) {
		this.lusdl = lusdl;
	}
}
