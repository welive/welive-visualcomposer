package it.eng.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MashupPublicationResponse 
{
	private boolean success;
	@JsonProperty("mashupid")
	private int mashupId;
	@JsonProperty("artefactid")
	private int artefactId;
	private List<String> warnings;
	private List<String> errors;
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public int getMashupId() {
		return mashupId;
	}
	public void setMashupId(int mashupId) {
		this.mashupId = mashupId;
	}
	public int getArtefactId() {
		return artefactId;
	}
	public void setArtefactId(int artefactId) {
		this.artefactId = artefactId;
	}
	public List<String> getWarnings() {
		return warnings;
	}
	public void setWarnings(List<String> warnings) {
		this.warnings = warnings;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}
