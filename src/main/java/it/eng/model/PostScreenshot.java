package it.eng.model;

public class PostScreenshot 
{
	private String description;
	private String encodedScreenShot;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEncodedScreenShot() {
		return encodedScreenShot;
	}
	public void setEncodedScreenShot(String encodedScreenShot) {
		this.encodedScreenShot = encodedScreenShot;
	}
	
	
}
