package it.eng.model;

import java.util.List;

import com.mediigea.vme2.dto.GuiFileResourceDTO;

public class SinglePageApplication 
{
	String spaName;
	int spaFirstPageId;
	List<GuiFileResourceDTO> pages;
	
	public String getSpaName() {
		return spaName;
	}
	public void setSpaName(String spaName) {
		this.spaName = spaName;
	}
	public int getSpaFirstPageId() {
		return spaFirstPageId;
	}
	public void setSpaFirstPageId(int spaFirstPageId) {
		this.spaFirstPageId = spaFirstPageId;
	}
	public List<GuiFileResourceDTO> getPages() {
		return pages;
	}
	public void setPages(List<GuiFileResourceDTO> pages) {
		this.pages = pages;
	}
}
