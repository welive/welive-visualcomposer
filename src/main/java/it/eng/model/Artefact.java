package it.eng.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import it.eng.metamodel.definitions.BuildingBlock;

public class Artefact 
{
	private String name;
	private String url;
	private String type;
	private String linkPage;
	private String description;
	private int wsmodelId;
	private String eId;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private BuildingBlock metamodel;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLinkPage() {
		return linkPage;
	}
	public void setLinkPage(String linkPage) {
		this.linkPage = linkPage;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getWsmodelId() {
		return wsmodelId;
	}
	public void setWsmodelId(int wsmodel_id) {
		this.wsmodelId = wsmodel_id;
	}
	public String geteId() {
		return eId;
	}
	public void seteId(String eId) {
		this.eId = eId;
	}
	public BuildingBlock getMetamodel() {
		return metamodel;
	}
	public void setMetamodel(BuildingBlock metamodel) {
		this.metamodel = metamodel;
	}
	
	
}
