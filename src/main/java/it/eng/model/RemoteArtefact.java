package it.eng.model;

import it.eng.metamodel.definitions.BuildingBlock;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoteArtefact 
{
	private String name;
	private String interfaceOperation;
	private String description;
	private String type;
	private String eId;
	private long typeId;
	private String link;
	private String linkImage;
	private String serviceCatalogId;
	private double rating;
	private long artefactId;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private BuildingBlock metamodel;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private boolean hasmapping;
	
	public RemoteArtefact()
	{
		
	}
	
	public RemoteArtefact(String name, String interfaceOperation, String description, String type, String link, long typeId)
	{
		this.name = name;
		this.interfaceOperation = interfaceOperation;
		this.description = description;
		this.type = type;
		this.link = link;
		this.typeId = typeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInterfaceOperation() {
		return interfaceOperation;
	}

	public void setInterfaceOperation(String interfaceOperation) {
		this.interfaceOperation = interfaceOperation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public long getTypeId() {
		return typeId;
	}

	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	public String getLinkImage() {
		return linkImage;
	}

	public void setLinkImage(String linkImage) {
		this.linkImage = linkImage;
	}

	public String getServiceCatalogId() 
	{
		return serviceCatalogId;
	}

	public void setServiceCatalogId(String serviceCatalogId) 
	{
		this.serviceCatalogId = serviceCatalogId;
	}

	public double getRating() 
	{
		return rating;
	}

	public void setRating(double rating) 
	{
		this.rating = rating;
	}

	public long getArtefactId() {
		return artefactId;
	}

	public void setArtefactId(long wsmodelId) {
		this.artefactId = wsmodelId;
	}

	public String geteId() {
		return eId;
	}

	public void seteId(String eId) {
		this.eId = eId;
	}

	public BuildingBlock getMetamodel() {
		return metamodel;
	}

	public void setMetamodel(BuildingBlock metamodel) {
		this.metamodel = metamodel;
	}

	public boolean getHasmapping() {
		return hasmapping;
	}

	public void setHasmapping(boolean hasmapping) {
		this.hasmapping = hasmapping;
	}
}
