package it.eng.model;

public class AddVcProject 
{
	private String vcProjectId;
	private String vcProjectName;
	private String ideaId;
	private boolean mockup;
	
	public String getVcProjectId() {
		return vcProjectId;
	}
	public void setVcProjectId(String vcProjectId) {
		this.vcProjectId = vcProjectId;
	}
	public String getVcProjectName() {
		return vcProjectName;
	}
	public void setVcProjectName(String vcProjectName) {
		this.vcProjectName = vcProjectName;
	}
	public String getIdeaId() {
		return ideaId;
	}
	public void setIdeaId(String ideaId) {
		this.ideaId = ideaId;
	}
	public boolean getMockup() {
		return mockup;
	}
	public void setMockup(Boolean mockup) {
		this.mockup = mockup;
	}
}
