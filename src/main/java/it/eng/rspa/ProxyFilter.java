package it.eng.rspa;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.Authenticator;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

/**
 * Servlet Filter implementation class ProxyFilter
 */
public class ProxyFilter implements Filter {

    /**
     * Default constructor. 
     */
	private Boolean useProxy;
	 private String proxyPort;
	 private String proxyHost;
	 private Boolean useAuth;
	 private String proxyUser;
	 private String proxyPassword;  
	 private String nonProxyHosts; 
	 
    public ProxyFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		 // Assign proxy if needed
		// System.out.println("Proxy Filter");
		
		System.out.println("ContentType: " + ((HttpServletRequest)request).getContentType());
		System.out.println("Method: " + ((HttpServletRequest)request).getMethod());
		/*
		StringBuffer jb = new StringBuffer();
		  String line = null;
		  try {
		    BufferedReader reader = request.getReader();
		    while ((line = reader.readLine()) != null)
		      jb.append(line);
		  } catch (Exception e) { }
		  
		  System.out.println("String: " + jb);
		  
		  try {
			    JSONObject jsonObject = new JSONObject(jb.toString());
			  } catch (Exception e) {
			    // crash and burn
			    
			  }
		*/
		  System.setProperty("http.proxySet","false");  
		System.setProperty("http.proxyHost","");  
	 	System.setProperty("http.proxyPort","");
	 	System.setProperty("http.proxyUser","");  
	 	System.setProperty("http.proxyPassword","");
	 	System.setProperty("http.nonProxyHosts", "");	
	 		
		if (useProxy) {
		//	System.out.println("proxy YES");
			System.setProperty("http.proxySet","true");  
			System.setProperty("http.proxyHost",proxyHost);  
 			System.setProperty("http.proxyPort",proxyPort);
 			System.setProperty("http.nonProxyHosts", nonProxyHosts);
 			
 			if(useAuth){
 			//	System.out.println("Auth YES");
 				System.setProperty("http.proxyUser",proxyUser);  
 		 		System.setProperty("http.proxyPassword",proxyPassword);
 				Authenticator.setDefault(new ProxyAuthenticator(proxyUser, proxyPassword)); 
 			}else{
 			//	System.out.println("Auth NO");
 				Authenticator.setDefault(null); 
 			}
		}else{
		//	System.out.println("proxy NO");
			System.setProperty("http.proxyHost","");  
	 		System.setProperty("http.proxyPort","");
	 		System.setProperty("http.proxyUser","");  
	 		System.setProperty("http.proxyPassword","");
	 		System.setProperty("http.nonProxyHosts", "");
	 		
	 		Authenticator.setDefault(null); 
		   }
		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
	public Boolean getUseProxy() {
		  return useProxy;
		 }
		 
		 public void setUseProxy(Boolean useProxy) {
		  this.useProxy = useProxy;
		 }
	 public String getProxyPort() {
		  return proxyPort;
		 }
		 
		 public void setProxyPort(String proxyPort) {
		  this.proxyPort = proxyPort;
		 }
		 
		 public String getProxyHost() {
		  return proxyHost;
		 }
		 
		 public void setProxyHost(String proxyHost) {
		  this.proxyHost = proxyHost;
		 }

		public String getProxyUser() {
			return proxyUser;
		}

		public void setProxyUser(String proxyUser) {
			this.proxyUser = proxyUser;
		}

		public String getProxyPassword() {
			return proxyPassword;
		}

		public void setProxyPassword(String proxyPassword) {
			this.proxyPassword = proxyPassword;
		}

		public Boolean getUseAuth() {
			return useAuth;
		}

		public void setUseAuth(Boolean useAuth) {
			this.useAuth = useAuth;
		}

		public String getNonProxyHosts() {
			return nonProxyHosts;
		}

		public void setNonProxyHosts(String nonProxyHosts) {
			this.nonProxyHosts = nonProxyHosts;
		}
		 
}
