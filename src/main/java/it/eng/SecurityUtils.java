package it.eng;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecurityUtils 
{
	private static final Logger _log = LoggerFactory.getLogger(SecurityUtils.class);
	
	public static Key getPrivateKey()
	{
		String alias = PropertyGetter.getProperty("tomcat.keystore.alias");
		String password = PropertyGetter.getProperty("tomcat.keystore.password");
		String location = PropertyGetter.getProperty("tomcat.keystore.location");
		
		FileInputStream is = null;
		Key private_key = null;
		
		try
		{
			File file = new File(location);
	        is = new FileInputStream(file);
	        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
	        keystore.load(is, password.toCharArray());
			
			private_key = keystore.getKey(alias, password.toCharArray());
			
			if(private_key == null)
				_log.error("No key for alias '" + alias + "' in the keystore");
		}
		catch (FileNotFoundException e) 
		{
	        e.printStackTrace();
	    } 
		catch (KeyStoreException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnrecoverableKeyException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (NoSuchAlgorithmException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally 
		{
	        if(is != null)
	            try {
	                is.close();
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	    }
		
		return private_key;
	}
	
	public static PublicKey getPublicKeyByAlias(String alias)
	{
		FileInputStream is = null;
		
		try {
			String location = System.getProperty("java.home") + "/lib/security/cacerts".replace('/', File.separatorChar);
	        File file = new File(location);
	        is = new FileInputStream(file);
	        KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
	        String password = PropertyGetter.getProperty("jvm.keystore.password");
	        keystore.load(is, password.toCharArray());
	        
	        Certificate certificate = keystore.getCertificate(alias);
	        
	        PublicKey pk = null;
	        
	        if(certificate != null)
	        	pk = certificate.getPublicKey();
	        else
	        	_log.error("No certificate for alias '" + alias + "' in the keystore");
	        
	        return pk;

	    } 
		catch (java.security.cert.CertificateException e) 
		{
	        e.printStackTrace();
	    } 
		catch (NoSuchAlgorithmException e) 
		{
	        e.printStackTrace();
	    } 
		catch (FileNotFoundException e) 
		{
	        e.printStackTrace();
	    } 
		catch (KeyStoreException e) 
		{
	        e.printStackTrace();
	    } 
		catch (IOException e) 
		{
	        e.printStackTrace();
	    }
		finally 
		{
	        if(is != null)
	            try {
	                is.close();
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	    }
		
		return null;
	}
	
	public static byte[] encrypt(String text, PublicKey key) 
	{
	    byte[] cipherText = null;
	    try {
	      // get an RSA cipher object and print the provider
	      final Cipher cipher = Cipher.getInstance(PropertyGetter.getProperty("encryption.algorithm"));
	      // encrypt the plain text using the public key
	      cipher.init(Cipher.ENCRYPT_MODE, key);
	      cipherText = cipher.doFinal(text.getBytes());
	    } 
	    catch (Exception e) 
	    {
	      e.printStackTrace();
	    }
	    
	    return cipherText;
	}
	
	public static String decrypt(byte[] text, Key key) 
	{
	    byte[] dectyptedText = null;
	    try {
	      // get an RSA cipher object and print the provider
	      final Cipher cipher = Cipher.getInstance(PropertyGetter.getProperty("encryption.algorithm"));

	      // decrypt the text using the private key
	      cipher.init(Cipher.DECRYPT_MODE, key);
	      dectyptedText = cipher.doFinal(text);

	    } 
	    catch (Exception ex) 
	    {
	      ex.printStackTrace();
	      return null;
	    }

	    return new String(dectyptedText);
	  }
}
