package it.eng;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.xml.sax.ErrorHandler;

public class GenericUtils 
{
	private static final Logger logger = LoggerFactory.getLogger(GenericUtils.class);
	
	public static ObjectNode validateAgainstXSD(String xml, String type)
	{
		ObjectNode res = JsonNodeFactory.instance.objectNode();
		
		InputStream main_xsd = null;
		
		String xsdFile = type + "-schema.xsd";
		
		try 
		{
			main_xsd = GenericUtils.class.getClassLoader().getResourceAsStream(xsdFile);
		} 
		catch (Exception e) 
		{
			logger.error("Error reading " + xsdFile);
			//logger.error(e.getStackTrace().toString());
			
			res.put("error", 1);
			
			return res;
		}
		
	    try
	    {	    	
	        SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
	        
	        Schema schema = factory.newSchema(new StreamSource(main_xsd));
	        
	        Validator validator = schema.newValidator();
	        validator.setErrorHandler(new XsdErrorHandler());
	        validator.validate(new StreamSource(new ByteArrayInputStream(xml.getBytes())));

	        res.put("error", 0);
			
			return res;
	    }
	    catch(SAXException ex)
	    {
	    	logger.error("Validation error: " + ex.getMessage());
	    	//ex.printStackTrace();
	    	
	    	res.put("error", 3);
			res.put("message", ex.getMessage());
			
			return res;
	    } 
	    catch (IOException e) 
	    {
	    	logger.error("Error validating xml");
	    	//e.printStackTrace();
			
			res.put("error", 1);
			
			return res;
		} 
	}
}

class XsdErrorHandler implements ErrorHandler {

    @Override
    public void warning(SAXParseException exception) throws SAXException {
        handleMessage("Warning", exception);
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        handleMessage("Error", exception);
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        handleMessage("Fatal", exception);
    }

    private String handleMessage(String level, SAXParseException exception) throws SAXException {
        int lineNumber = exception.getLineNumber();
        int columnNumber = exception.getColumnNumber();
        String message = exception.getMessage();
        throw new SAXException("[" + level + "] line: " + lineNumber + "; column: " + columnNumber + "; cause: " + message);
    }
}
