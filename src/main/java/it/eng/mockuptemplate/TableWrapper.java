package it.eng.mockuptemplate;

import java.util.List;

public class TableWrapper 
{
	private String forEachPath;
	private List<SelectedField> selectedFields;
	
	public String getForEachPath() {
		return forEachPath;
	}
	public void setForEachPath(String forEachPath) {
		this.forEachPath = forEachPath;
	}
	public List<SelectedField> getSelectedFields() {
		return selectedFields;
	}
	public void setSelectedFields(List<SelectedField> selectedFields) {
		this.selectedFields = selectedFields;
	}
}
