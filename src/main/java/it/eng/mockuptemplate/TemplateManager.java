package it.eng.mockuptemplate;

import it.eng.PropertyGetter;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mediigea.vme2.dto.InputShapeDTO;
import com.mediigea.vme2.entity.GuiProject;
import com.mediigea.vme2.entity.Profile;
import com.mediigea.vme2.entity.Project;
import com.mediigea.vme2.exceptions.WriteResourceException;
import com.mediigea.vme2.service.GuiEditorService;
import com.mediigea.vme2.service.ProjectService;
import com.mediigea.vme2.service.GuiEditorServiceImpl.ResourceContentType;
import com.mediigea.vme2.util.VmeConstants;

import freemarker.cache.ClassTemplateLoader;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

public class TemplateManager 
{
	private static Configuration cfg;

	static
	{
		cfg = new Configuration(Configuration.VERSION_2_3_25);
		cfg.setDefaultEncoding("UTF-8");
		
		ClassTemplateLoader loader = new ClassTemplateLoader(TemplateManager.class, "/mockup-templates");
		
		cfg.setTemplateLoader(loader);
	}
	
	private static Element processTemplateAndAppend(String templateName, HashMap model, Element parent, StringWriter output) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException
	{
		Template template = null;
		
		template = cfg.getTemplate(templateName);
		
		output.getBuffer().setLength(0);
		
		template.process(model, output);	
		
		return parent.append(output.toString()).children().last();
	}
	
	public static void buildTemplate(GuiEditorService guiEditorService, ProjectService projectService, Profile profile, ObjectNode mockup)
	{
		String templateId = mockup.get("templateId").textValue();	
		
		boolean isMock = !mockup.get("projectInfo").has("mashupId") || mockup.get("projectInfo").get("mashupId") == null;
		
		// retrieve mockup and mashup projects
		GuiProject project = guiEditorService.findProject(Integer.parseInt(mockup.get("id").textValue()));		
		Project mashupProject = null;
		
		Document doc = null;
		
		// create empty html page
		doc = Jsoup.parse("<html></html");
		
		Element head = doc.head();
		Element body = doc.body();
		Element container = null;
		Element columnsContainer = null;
		
		// it will contain data to populate template
		HashMap<String, Object> propertyMap = new HashMap<String,Object>();
		
		propertyMap.put("baseUrl", PropertyGetter.getProperty("oauth.callbackContext"));
		propertyMap.put("projectId", Integer.toString(project.getId()));		
		propertyMap.put("title", mockup.get("projectInfo").get("title").textValue());
		propertyMap.put("description", mockup.get("projectInfo").get("description").textValue());
		
		List<InputShapeDTO> inputList = null;
		
		//if(profile.getLevel() != VmeConstants.USER_LEVEL_MOCKUPDESIGNER)
		if(!isMock)
		{
			Integer mashupId = Integer.parseInt(mockup.get("projectInfo").get("mashupId").textValue());
			mashupProject = projectService.findProject(mashupId);
			
			propertyMap.put("mashupId", Integer.toString(mashupId));
			
			// retrieve mashup input parameters and set ist as data-model inputFields
			// these inputs will populate form template
			try 
			{			
				inputList = projectService.readInputShapesList(mashupId);
				
				// remove constant inputs
				List<InputShapeDTO> notConstantInputList = new ArrayList<InputShapeDTO>();
				
				for(InputShapeDTO input : inputList)
				{
					if(!input.getUserData().isIsConstant())
					{
						notConstantInputList.add(input);
					}
				}
				
				propertyMap.put("inputFields", notConstantInputList);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return;
			}
		}
		
		StringWriter output = new StringWriter();
		
		//String mock = profile.getLevel() == VmeConstants.USER_LEVEL_MOCKUPDESIGNER ? "_mock" : "";
		String mock = isMock ? "_mock" : "";
		
		try 
		{
			processTemplateAndAppend("head.ftl", propertyMap, head, output); // populate <head> section
			
			processTemplateAndAppend("require_script.ftl", propertyMap, body, output); // body child 0
			
			container = body.append("<div class='container'></div>").child(1); // main container
						
			if(templateId.compareTo("fromscretch") != 0)
			{
				processTemplateAndAppend("header.ftl", propertyMap, container, output); // header with title and description
				
				processTemplateAndAppend("form" + mock + ".ftl", propertyMap, container, output); // form
			}
			
			//if(profile.getLevel() != VmeConstants.USER_LEVEL_MOCKUPDESIGNER)
			if(!isMock)
			{
				// build table wrapper
				if(templateId.compareTo("table") == 0 || templateId.compareTo("tablemap") == 0)
				{
					ObjectNode tmp = (ObjectNode)mockup.get("configuration").get("table");
					
					// <mashup name>['<selected array path>']()
					//String forEachPath = mashupProject.getName() + "['" + tmp.get("dataPath").get("path").textValue().replace("()", "") + "']()";
					
					String forEachPath = "$data['" + mashupProject.getName() + "']";
					
					String path = tmp.get("dataPath").get("path").textValue();
					
					int index = path.indexOf('[');
					
					if(index != -1)
					{
						forEachPath += "['" + path.substring(0, index) + "']" + path.substring(index, path.length());
					}
					else
					{
						forEachPath += "['" + path.replace("()", "") + "']()";
					}
					
					Iterator<JsonNode> selectedFields = ((ArrayNode) tmp.get("selectedFields")).iterator();
					ObjectNode selectedField = null;				
					
					List<SelectedField> selFields = new ArrayList<SelectedField>();
					SelectedField selField = null;
					
					String[] parts;
					String _path = "";
					
					// iterate through selected fields
					while(selectedFields.hasNext())
					{
						selectedField = (ObjectNode) selectedFields.next();
						
						parts = selectedField.get("name").textValue().split("\\.");
						
						_path = "$data";
						
						for(int p = 0; p < parts.length; p++)
						{
							_path += "['" + parts[p] + "']";
						}
						
						selField = new SelectedField();
						selField.setHeader(selectedField.get("header").textValue());
						selField.setName(_path);
						
						selFields.add(selField);					
					}
					
					TableWrapper tableWrapper = new TableWrapper();
					tableWrapper.setForEachPath(forEachPath);
					tableWrapper.setSelectedFields(selFields);
					
					propertyMap.put("table", tableWrapper);
				}
				// build map wrapper
				if(templateId.compareTo("map") == 0 || templateId.compareTo("tablemap") == 0)
				{
					ObjectNode tmp = (ObjectNode)mockup.get("configuration").get("map").get("dataPath");
					ObjectNode infoWindow = (ObjectNode)mockup.get("configuration").get("map").get("infoWindow");
					
					// <mashup name>['<selected array path>']()
					//String forEachPath = mashupProject.getName() + "['" + tmp.get("path").textValue().replace("()", "") + "']()";
					
					String forEachPath = "$data['" + mashupProject.getName() + "']";
					
					String path = tmp.get("path").textValue();
					
					int index = path.indexOf('[');
					
					if(index != -1)
					{
						forEachPath += "['" + path.substring(0, index) + "']" + path.substring(index, path.length());
					}
					else
					{
						forEachPath += "['" + path.replace("()", "") + "']()";
					}
					
					String coordsPath = tmp.get("coordpath").textValue();
					
					String infoWindowPrimary = infoWindow.get("primary").textValue();
					String infoWindowSecondary = infoWindow.get("secondary").textValue();
					
					MapWrapper mapWrapper = new MapWrapper();
					mapWrapper.setForEachPath(forEachPath);
					mapWrapper.setCoordsPath(coordsPath);
					mapWrapper.setInfoWindowPrimary(infoWindowPrimary);
					mapWrapper.setInfoWindowSecondary(infoWindowSecondary);
					
					propertyMap.put("map", mapWrapper);
				}
			}
			
			if(templateId.compareTo("blank") != 0 && templateId.compareTo("fromscretch") != 0)
			{
				columnsContainer = container.append("<div class='row' data-vme-dad='sortable' data-vme-type='block' data-vme-sortable='true'></div>").children().last();
				
				if(templateId.compareTo("tablemap") == 0) // two columns layout
				{
					Element column1 = columnsContainer.append("<div class='col s12' data-vme-dad='sortable' data-vme-type='block' data-vme-sortable='true'></div>").children().last();
					Element column2 = columnsContainer.append("<div class='col s12' data-vme-dad='sortable' data-vme-type='block' data-vme-sortable='true'></div>").children().last();
					
					processTemplateAndAppend("table" + mock + ".ftl", propertyMap, column1, output);
					processTemplateAndAppend("map" + mock + ".ftl", propertyMap, column2, output);
				}
				else// single column layout
				{
					Element column = columnsContainer.append("<div class='col s12' data-vme-dad='sortable' data-vme-type='block' data-vme-sortable='true'></div>").children().last();
					
					processTemplateAndAppend(templateId + mock + ".ftl", propertyMap, column, output);
				}
			}
			
			processTemplateAndAppend("spinner.ftl", propertyMap, container, output);
			
			// write html to file
			guiEditorService.saveFile(project.getId(), "file.html.working", container.html(), ResourceContentType.get("text/html"));
			guiEditorService.saveFile(project.getId(), "main.html", doc.html(), ResourceContentType.get("text/html"));
			
			// update gui project
			project.setSpaName("main");
			guiEditorService.updateProject(project, true);
		} 
		catch (TemplateNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedTemplateNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteResourceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
