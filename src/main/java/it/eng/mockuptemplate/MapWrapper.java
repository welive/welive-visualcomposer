package it.eng.mockuptemplate;

public class MapWrapper 
{
	private String forEachPath;
	private String coordsPath;
	private String infoWindowPrimary;
	private String infoWindowSecondary;
	
	public String getForEachPath() {
		return forEachPath;
	}
	public void setForEachPath(String forEachPath) {
		this.forEachPath = forEachPath;
	}
	public String getCoordsPath() {
		return coordsPath;
	}
	public void setCoordsPath(String coordsPath) {
		this.coordsPath = coordsPath;
	}
	public String getInfoWindowPrimary() {
		return infoWindowPrimary;
	}
	public void setInfoWindowPrimary(String infoWindowPrimary) {
		this.infoWindowPrimary = infoWindowPrimary;
	}
	public String getInfoWindowSecondary() {
		return infoWindowSecondary;
	}
	public void setInfoWindowSecondary(String infoWindowSecondary) {
		this.infoWindowSecondary = infoWindowSecondary;
	}
}
