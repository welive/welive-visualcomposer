/*
package it.eng;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyGetter 
{
	private static final Logger logger = LoggerFactory
			.getLogger(PropertyGetter.class);
	
	private static final String BUNDLE_NAME = "/WEB-INF/spring/config"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private PropertyGetter() 
	{
	}

	public static String getProperty(String key) 
	{
		try 
		{
			return RESOURCE_BUNDLE.getString(key);
		} 
		catch (MissingResourceException e) 
		{
			return '!' + key + '!';
		}
	}
}*/

package it.eng;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyGetter 
{
	private static final Logger logger = LoggerFactory.getLogger(PropertyGetter.class);
	
	private static final String BUNDLE_NAME = "/WEB-INF/spring/config"; //$NON-NLS-1$
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	private static HashMap<String, String> propertyMap;
	
	public static final String[] langCodes = {"en_EN", "es_ES", "fi_FI", "it_IT", "sr_RS", "sr_RS_latin"};
	
	private static HashMap<String, HashMap<String, String>> messageMap;

	static
	{
		propertyMap = new HashMap<String,String>();
		messageMap = new HashMap<String, HashMap<String, String>>();
		
		// set properties
		try
		{
			Enumeration<String> keys = RESOURCE_BUNDLE.getKeys();
			
			String key;
			String value;
			
			while(keys.hasMoreElements())
			{
				key = keys.nextElement();
				
				try 
				{ 
					value = RESOURCE_BUNDLE.getString(key);
					
					Pattern pattern = Pattern.compile("\\$\\{([^\\}]+)\\}");
					Matcher matcher = pattern.matcher(value);
					
					while(matcher.find())
					{
						value = value.replaceAll("\\$\\{" + matcher.group(1) + "\\}", RESOURCE_BUNDLE.getString(matcher.group(1)));
					}
					
					propertyMap.put(key, value); 
				}
				catch(Exception e) 
				{ 
					logger.debug("Getting property '"+key+"': "+e.getMessage()); 
				}
			}
		} 
		catch (Exception e)
		{
			logger.debug("Inizializing properties:"+e.getMessage());
		}
		
		// set messages
		try
		{
			ResourceBundle messageBundle = null;
			
			Enumeration<String> keys = null;
			
			String key;
			String value;			
			
			HashMap<String, String> translMap = null;
			
			for(int i = 0; i < langCodes.length; i++)
			{
				messageBundle = ResourceBundle.getBundle("/WEB-INF/messages/messages_" + langCodes[i]);
				
				keys = messageBundle.getKeys();
				
				translMap = new HashMap<String, String>();
				
				while(keys.hasMoreElements())
				{
					key = keys.nextElement();
					
					try 
					{ 
						value = messageBundle.getString(key);
						
						Pattern pattern = Pattern.compile("\\$\\{([^\\}]+)\\}");
						Matcher matcher = pattern.matcher(value);
						
						while(matcher.find())
						{
							value = value.replaceAll("\\$\\{" + matcher.group(1) + "\\}", messageBundle.getString(matcher.group(1)));
						}
						
						translMap.put(key, value); 
					}
					catch(Exception e) 
					{ 
						logger.error("Getting message '"+key+"': "+e.getMessage()); 
					}
				}
				
				messageMap.put(langCodes[i], translMap);
			}
		} 
		catch (Exception e)
		{
			logger.error("Inizializing messages:"+e.getMessage());
		}
	}
			
	private PropertyGetter() 
	{
	}

	public static String getProperty(String key) 
	{
		return propertyMap.get(key);		
	}
	
	public static void setProperty(String key, String value)
	{
		try
		{
			String test = RESOURCE_BUNDLE.getString(key);
			
			propertyMap.put(key, value);
		}
		catch(MissingResourceException e)
		{
			logger.debug("Error setting property '" + key + "': the property doesn't exist");
		}
		
	}
	
	public static HashMap<String, String> getPropertyMap()
	{
		return propertyMap;
	}
	
	public static HashMap<String, String> getMessageMap(String lang)
	{
		return messageMap.get(lang);
	}
}

