<div class="row">
	<div class="col s12">
	<form data-vme-dad="sortable" data-vmert-onrender="form_oam.initForm" data-vmert-invokeenabled="true" data-vme-itemid="vme-form" data-vme-type="block" role="form" class="" data-vme-sortable="true" 
		action="${baseUrl}/gadget/${projectId}/emmlproxy2.json" data-binding_input="true" 
		data-mashup_${mashupId}="true" 
		<#if inputFields?size=0>
		onloadsubmit="true" style="display:none"
		</#if>
		> 
		
		<#list inputFields as field>
		<div data-vme-dad="sortable" data-vme-type="block" class="row" data-vme-sortable="true"> 
			<div data-vme-dad="sortable" data-vme-type="block" class="input-field col s12" data-vme-sortable="true"> 
				<input data-vme-itemid="vme-inputtext" type="text" placeholder="${field.userData.label}" id="${field.id}" name="${field.id}" data-binding_input="true"> 
				<label data-vme-itemid="vme-label" for="${field.id}" class="active">${field.userData.label}</label> 
			</div> 
		</div> 
		</#list>
		
		<div data-vme-dad="sortable" data-vme-type="block" class="row" data-vme-sortable="true"> 
			<div data-vme-dad="sortable" data-vme-type="block" class="input-field col s12" data-vme-sortable="true"> 
				<button data-vme-itemid="vme-button" data-vme-type="inline" type="submit" class="btn waves-effect waves-light">Submit</button> 
			</div> 
		</div> 
	</form> 
	</div> 
</div> 