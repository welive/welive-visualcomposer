<table data-vme-itemid="vme-table" data-vme-type="block" class="striped" data-datatype="array">
    <thead data-vme-itemid="vme-thead" data-vme-type="block">
        <tr>
        	<#list table.selectedFields as field>
            <th>${field.header}</th>
            </#list>
        </tr>
    </thead>
    <tbody data-vme-itemid="vme-tbody" data-vme-type="block" data-vmert-onrender="table_oam.hideTableRows" data-vmert-afterrender="table_oam.showTableRows" data-onlydatabind="true" data-datatype="array" data-databind="foreach" data-databind-type="simple" data-bind-property="foreach" 
    data-bind-foreach="${table.forEachPath}">
        <tr>
        	<#list table.selectedFields as field>
            <td>
                <span data-vme-dad="sortable" data-vme-itemid="vme-span" data-vme-type="inline" data-onlydatabind="false" data-datatype="text" data-databind="text" data-databind-type="simple" data-bind-property="text" data-vme-sortable="true" 
                data-bind-text="${field.name}"></span>
            </td>
            </#list>
        </tr>
    </tbody>
</table>