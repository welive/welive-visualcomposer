<script data-main='${baseUrl}/guieditor/project_template/js/main' src='${baseUrl}/guieditor/project_template/js/lib/require.js'></script>
<link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>
<link type='text/css' rel='stylesheet' href='${baseUrl}/resources/js/materialize/css/materialize.min.css' media='screen,projection'>
<style>
#waiting_layer
{
	z-index: 1000;
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(255,255,255,0.8);
}	

#waiting_layer > div
{
	position: relative;
	width: 100%;
	text-align: center;
}

#waiting_layer h5
{
	text-align: center;
}

#waiting_layer .progress
{
	width: 50% !important;
}
</style>