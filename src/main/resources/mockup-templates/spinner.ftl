<div id="waiting_layer" class="valign-wrapper" style="display: none">

	<div>
		<div class="row">
			<h5>Please wait...</h5>
		</div>
		<br>
		<div class="progress row" style="margin:auto">
			<div class="indeterminate"></div>
		</div>
	</div>

</div>