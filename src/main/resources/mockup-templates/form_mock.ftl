<div class="row">
	<div class="col s12">
		<form data-vme-dad="sortable" data-vmert-onrender="form_oam.initForm" data-vmert-invokeenabled="true" data-vme-itemid="vme-form" data-vme-type="block" role="form" class="" data-vme-sortable="true" 
			action="${baseUrl}/gadget/${projectId}/emmlproxy2.json" data-binding_input="true"> 
			
			<div data-vme-dad="sortable" data-vme-type="block" class="row" data-vme-sortable="true"> 
				<div data-vme-dad="sortable" data-vme-type="block" class="input-field col s12" data-vme-sortable="true"> 
					<input data-vme-itemid="vme-inputtext" type="text" placeholder="Input #1" id="input_1" name="input_1" data-binding_input="true"> 
					<label data-vme-itemid="vme-label" for="input_1" class="active">Input #1</label> 
				</div> 
			</div> 
			<div data-vme-dad="sortable" data-vme-type="block" class="row" data-vme-sortable="true"> 
				<div data-vme-dad="sortable" data-vme-type="block" class="input-field col s12" data-vme-sortable="true"> 
					<input data-vme-itemid="vme-inputtext" type="text" placeholder="Input #2" id="input_2" name="input_2" data-binding_input="true"> 
					<label data-vme-itemid="vme-label" for="input_2" class="active">Input #2</label> 
				</div> 
			</div>
			
			<div data-vme-dad="sortable" data-vme-type="block" class="row" data-vme-sortable="true"> 
				<div data-vme-dad="sortable" data-vme-type="block" class="input-field col s12" data-vme-sortable="true"> 
					<button data-vme-itemid="vme-button" data-vme-type="inline" type="submit" class="btn waves-effect waves-light">Submit</button> 
				</div> 
			</div> 
		</form> 
	</div> 
</div> 