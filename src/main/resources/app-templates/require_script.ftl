<script>
require.config({
	'paths':{
		'prjcss':'${baseUrl}/guieditor/project_repository/gprj_${projectId}/css',
		'prjjs':'${baseUrl}/guieditor/project_repository/gprj_${projectId}/js'
	},
	'shim':{}
});
require(['main_app'], function() {
	${requirejsFiles}
});
</script>