/******************************************
 * Websanova.com
 *
 * Resources for web entrepreneurs
 *
 * @author          Websanova
 * @copyright       Copyright (c) 2012 Websanova.
 * @license         This websanova JSNova jQuery plugin is dual licensed under the MIT and GPL licenses.
 * @link            http://www.websanova.com
 * @docs            http://www.websanova.com/plugins/websanova/jsnova
 * @version         Version 1.0
 *
 ******************************************/

(function($)
		{
	$.fn.wJSNova = function(option, settings)
	{	
		if(typeof option === 'object')
		{
			settings = option;
		}
		else if(typeof option === 'string')
		{
			var data = this.data('_wJSNova');

			if(data)
			{
				if(option == 'resize')	{ 
					data.resize(); 
					return true;
				}
				else if (option == 'run'){
					data.run();
					return true;
				}
				else if (option == 'runWithJs'){
					data.runWithJs();
					return true;
				}				
				else if (option == 'reset'){
					data.reset();
					return true;
				}
				else if (option == 'getCss'){
					return data.getCss();					
				}
				else if (option == 'getJs'){
					return data.getJs();					
				}				
				else if($.fn.wJSNova.defaultSettings[option] !== undefined)
				{
					if(settings !== undefined){
						data.settings[option] = settings;
						return true;
					}
					else return data.settings[option];
				}
				else return false;
			}
			else return false;
		}

		settings = $.extend({}, $.fn.wJSNova.defaultSettings, settings || {});

		return this.each(function()
				{
			var $elem = $(this);

			var $settings = jQuery.extend(true, {}, settings);

			var jsn = new JSNova($settings);

			$elem.append(jsn.generate());
			jsn.resize();

			$elem.data('_wJSNova', jsn);
				});
	};

	$.fn.wJSNova.defaultSettings = {};

	function JSNova(settings)
	{
		this.jsn = null;
		this.settings = settings;

		//this.menu = null;

		//this.sidebar = null;
		//this.jQueryVersion = null;
		//this.jQueryUIVersion = null;
		//this.jQueryUITheme = null;

		this.codeArea = null;		

		//

		//if(boxHTMLId!=null && boxHTMLId!=undefined){
		//	this.boxHTML = $('#'+boxHTMLId).html();
		//	$('#'+boxHTMLId).remove();
		//} else{
		this.boxHTML = null;
		//}

		this.boxCSS = null;
		this.boxJS = null;
		this.boxResult = null;

		return this;
	}

	JSNova.prototype = 
	{
			init: function()
			{
				this.resize();
			},

			generate: function()
			{
				var $this = this;

				if($this.jsn) return $this.jsn;

				/************************************************
				 * Menu
				 ************************************************/
				//var menuButton_run = $('<span class="_wJSNova_menuButton">Run</span>').click(function(){$this.run();});
				//var menuButton_reset = $('<span class="_wJSNova_menuButton">Reset</span>').click(function(){$this.reset();});

				//$this.menu = 
				//$('<div class="_wJSNova_menu"></div>')
				//.append(
				//$('<div class="_wJSNova_menuPadding"></div>')
				//.append(menuButton_run)
				//.append(' <span class="_wJSNova_menuBullet">&bull;</span> ')
				//.append(menuButton_reset)
				//);						

				/************************************************
				 * Code Area
				 ************************************************/

				$this.boxHTML = $('<div id="boxHTML" class="_wJSNova_boxEdit"></div>');
				$this.boxCSS = $('<div id="boxCSS" class="_wJSNova_boxEdit"></div>');
				$this.boxJS = $('<div id="boxJS" class="_wJSNova_boxEdit"></textarea>');
				$this.boxResult = $('<iframe id="iframe" class="_wJSNova_boxEdit" frameBorder="0"></iframe>');

				$.each([$this.boxHTML, $this.boxCSS, $this.boxJS, $this.boxResult], function(index, item)
						{
					item
					.focus(function(){ $(this).parent().children('._wJSNova_boxLabel').fadeOut(); })
					.blur(function(){ $(this).parent().children('._wJSNova_boxLabel').fadeIn(); });
						});

				var jsBox = $('<td class="_wJSNova_box _wJSNova_boxBottom _wJSNova_boxLeft"></td>')
				.append(
						$('<div class="_wJSNova_boxContainer"></div>')
						.append($this.boxJS)
						.append('<div class="_wJSNova_boxLabel">JavaScript</div>')
				);

				$this.codeArea = 
					$('<div class="_wJSNova_codeArea"></div>')
					.append(
							$('<table class="_wJSNova_codeAreaTable" cellpadding="0" cellspacing="1"></table>')
							.append(
									$('<tr></tr>')
									.append(
											$('<td class="_wJSNova_box _wJSNova_boxTop _wJSNova_boxLeft"></td>')
											.append(
													$('<div class="_wJSNova_boxContainer"></div>')
													.append($this.boxHTML)
													.append('<div class="_wJSNova_boxLabel">HTML</div>')	
											)
									)
									.append(
											$('<td class="_wJSNova_box _wJSNova_boxTop _wJSNova_boxRight"></td>')
											.append(
													$('<div class="_wJSNova_boxContainer"></div>')
													.append($this.boxCSS)
													.append('<div class="_wJSNova_boxLabel">CSS</div>')
											)
									)
							)
							.append(
									$('<tr></tr>')
									.append(
											this.jsEditor!=null && this.jsEditor!=undefined ? jsBox : ""
									)
									.append(
											$('<td class="_wJSNova_box _wJSNova_boxBottom _wJSNova_boxRight" colspan="2"></td>')
											.append(
													$('<div class="_wJSNova_boxContainer"></div>')
													.append($this.boxResult)
													.append('<div class="_wJSNova_boxLabel">Result</div>')
											)
									)
							)
					);

				$this.jsn = 
					$('<div class="_wJSNova_holder"></div>')
					//.append($this.menu)
					//.append($this.sidebar)
					.append($this.codeArea);

				return $this.jsn;
			},

			run: function()
			{
				var html = this.htmlEditor.session.getValue();		
				var css = this.cssEditor.session.getValue();

				//var jQuery = '<script type="text/javascript" src="' + this.jQueryVersion.val() + '"></script>';
				//var jQueryUI = '<script type="text/javascript" src="' + this.jQueryUIVersion.val() + '"></script>';
				//var jQueryUITheme = '<link rel="stylesheet" type="text/css" href="' + this.jQueryUITheme.val() + '"/>'						


				var result = '<html><head><script src=\"http://code.jquery.com/jquery-latest.min.js\"></script><style>' + css + '</style></head><body>' + html + '</body></html>';			
				this.writeResult(result);									
			},

			runWithJs: function(){
				var html = this.htmlEditor.session.getValue();		
				var css = this.cssEditor.session.getValue();

				var result;
				
				if(this.jsEditor!=null && this.jsEditor!=undefined){
					var js = this.jsEditor.session.getValue();

					result = '<html><head><script src=\"http://code.jquery.com/jquery-latest.min.js\"></script><style>' + css + '</style><script>'+js+'</script></head><body>' + html + '</body></html>';			
				}else{

					result = '<html><head><script src=\"http://code.jquery.com/jquery-latest.min.js\"></script><style>' + css + '</style></head><body>' + html + '</body></html>';
				}
				this.writeResult(result);	
			},

			getCss: function(){
				return this.cssEditor.session.getValue();
			},

			getJs: function(){
				return this.jsEditor.session.getValue();
			},			

			reset: function()
			{
				//this.boxHTML.val('');
				//this.boxCSS.val('');
				//this.boxJS.val('');
				this.writeResult('');
			},

			writeResult: function(result)
			{
				var iframe = this.boxResult[0];

				if(iframe.contentDocument) doc = iframe.contentDocument;
				else if(iframe.contentWindow) doc = iframe.contentWindow.document;
				else doc = iframe.document;

				doc.open();
				doc.writeln(result);
				doc.close();
			},

			resize: function()
			{
				//var menuHeight = this.menu.outerHeight(true);
				//var jsnHeight = this.jsn.outerHeight(true) - menuHeight;
				var jsnHeight = this.jsn.outerHeight(true);

				//var codeAreaWidth = this.jsn.outerWidth(true) - this.sidebar.outerWidth(true);
				var codeAreaWidth = this.jsn.outerWidth(true);
				//this.sidebar.css({top: menuHeight, height: jsnHeight});

				//this.codeArea.css({top: menuHeight, height: jsnHeight, width: codeAreaWidth});
				this.codeArea.css({height: jsnHeight, width: codeAreaWidth});
				this.initAceEditors();
			},

			initAceEditors: function(){
				var htmlEditor = ace.edit("boxHTML");		

				this.htmlEditor = htmlEditor;

				htmlEditor.setTheme("ace/theme/chrome");
				htmlEditor.setShowPrintMargin(false);
				htmlEditor.session.setMode("ace/mode/html");
				htmlEditor.setReadOnly(true);

				var htmlContentId = this.settings.htmlContentId;		

				if(htmlContentId!=null && htmlContentId!=undefined){
					var prettyHtml = vkbeautify.xml($("#"+htmlContentId).text());
					htmlEditor.session.setValue(prettyHtml);						
				}

				var cssEditor = ace.edit("boxCSS");

				this.cssEditor = cssEditor;

				cssEditor.setTheme("ace/theme/chrome");
				cssEditor.session.setMode("ace/mode/css");
				cssEditor.setShowPrintMargin(false);

				var cssContentId = this.settings.cssContentId;		

				if(cssContentId!=null && cssContentId!=undefined){
					var prettyCss = vkbeautify.css($("#"+cssContentId).text());
					cssEditor.session.setValue(prettyCss);						
				}		

				if(this.settings.cssEditListener!=null && this.settings.cssEditListener!=undefined){
					cssEditor.getSession().on('change', this.settings.cssEditListener);
				}

				if(this.settings.jsContentId!=null && this.settings.jsContentId!=''){
					var jsEditor = ace.edit("boxJS");

					this.jsEditor = jsEditor;

					jsEditor.setTheme("ace/theme/chrome");
					jsEditor.session.setMode("ace/mode/javascript");
					jsEditor.setShowPrintMargin(false);

					var jsContentId = this.settings.jsContentId;		

					if(jsContentId!=null && jsContentId!=undefined){
						var js = $("#"+jsContentId).text();
						if(js!=null && js!=undefined){
							//var prettyJs = vkbeautify.json(js);
							jsEditor.session.setValue(js);						
						}
					}		

					if(this.settings.jsEditListener!=null && this.settings.jsEditListener!=undefined){
						jsEditor.getSession().on('change', this.settings.jsEditListener);
					}
				}
			}
	};
		})(jQuery);
