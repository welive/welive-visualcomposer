/**
 * Connessione di tipo Service To Service
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.OpenData.OD2OD = vme2.shape.Connection.OpenData.extend({
	NAME : "vme2.shape.Connection.OpenData.OD2OD",	
	init: function(sourceId, targetId){
		this._super(sourceId, targetId);

		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			
			this._postInit();			
			this._invokeChainService();
		}
	},
	_postInit: function(){
		this._super();		
		this.targetOpenDataDef = app.getView().getFigure(this.targetId).getOpenDataDef();						
	},	

	_buildConnectionView : function(isEditOperation) {						
		this._buildOpenDataConnectionView(isEditOperation);
	},	
	
	_connectParameter : function() {
		this._connectOpenDataParameter();
	},

	_disconnectParameter : function() {				
		this._disconnectOpenDataParameter();
	},

	_reconnectParameter : function(bindingParameter) {
		this.targetOpenDataDef.parameters = bindingParameter;		
	}
});
