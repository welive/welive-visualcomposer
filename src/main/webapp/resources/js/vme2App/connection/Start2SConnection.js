/**
 * Connessione di tipo Service To Iterator Renderer
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Start.Start2S = vme2.shape.Connection.Start.extend({
	NAME : "vme2.shape.Connection.Start.Start2S",	

	_postInit: function(){		
		this.targetServiceDef = app.getView().getFigure(this.targetId).getServiceDef();						
	},
	_buildConnectionView : function(isEditOperation) {						
		var targetServiceDef = this.targetServiceDef;
		var infoTempl = "<p><strong>SERVICE</strong>: {{operation.name}} ({{name}})</p>";
		var infoTemplCompiled = Handlebars.compile(infoTempl);
		var targetInfoView = infoTemplCompiled(targetServiceDef);
		
		$("#targetinfo").html(targetInfoView).append("<p><strong>Target parameters</strong></p>");		
		
		var compiled = Handlebars.compile(this.connectServiceViewTemplate);
		var view = compiled(targetServiceDef);
	
		var targetContainer = $('#targetparamscontainer');
		$(targetContainer).html(view);		
		
		this._super(isEditOperation);				
	},
	_connectParameter : function() {
		this._connectServiceParameter();
	},
	_disconnectParameter : function() {				
		this._disconnectServiceParameter();
	},	
		
	_reconnectParameter : function(bindingParameter) {
		this.targetServiceDef.operation.parameters = bindingParameter;		
	}	
});
