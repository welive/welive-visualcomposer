/**
 * Connessione di tipo Service To Service
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Service.S2S = vme2.shape.Connection.Service.extend({
	NAME : "vme2.shape.Connection.Service.S2S",	

	_postInit: function(){
		this._super();		
		this.targetServiceDef = app.getView().getFigure(this.targetId).getServiceDef();						
	},	

	_buildConnectionView : function(isEditOperation) {						
		this._buildServiceConnectionView(isEditOperation);
	},	
	
	_connectParameter : function() {
		this._connectServiceParameter();
	},

	_disconnectParameter : function() {				
		this._disconnectServiceParameter();
	},

	_reconnectParameter : function(bindingParameter) {
		this.targetServiceDef.operation.parameters = bindingParameter;		
	}
});
