/**
 * Connessione generica
 * @extends draw2d.shape.state.Connection
 */
draw2d.Connection.DEFAULT_ROUTER = new draw2d.layout.connection.SplineConnectionRouter();
vme2.shape.Connection = draw2d.Connection.extend({
	NAME : "vme2.shape.Connection",
	
	connectOperatorViewTemplate : "<form id='parameterConnections'>"
		+ "<div id='operatorParameterContainer'>"
		+ "{{#each parameters}}"
		
		+ "<div class='row valign-wrapper targetRow'>"
		+ "	<div class='input-field col s11'>"
		+ "			<input type='hidden' name='parameters[{{@index}}][name]' value='{{name}}'/>"
		+ "			{{#if options}}"
		+ "				<select class='paramInput' name='parameters[{{@index}}][property]' data-selectedvalue='{{property}}' {{#if event}}data-method='{{event}}'{{/if}} >"
		+ "				{{#each options}}"
		+ "					<option value='{{.}}' {{#ifeq property .}}selected{{/ifeq}}>{{.}}</option>"			
		+ "				{{/each}}"		
		+ "				</select>"		
		+ "			{{else}}"
		+ "				<input id='{{name}}' class='paramInput {{#if droppable}}jstree-drop'{{/if}}' {{#if event}}data-method='{{event}}'{{/if}} {{#if sourceexp}}data-sourceexp='true'{{/if}} {{#if datatype}}data-datatype='{{datatype}}'{{/if}} name='parameters[{{@index}}][property]' value='{{property}}' type='text' />"
		+ "			{{/if}}"		
		+ "   		<label style='text-transform: capitalize' for='{{name}}'>{{#if label}}{{label}}{{else}}{{name}}{{/if}}</label>"
		+ " </div>" 
		+ "	<div class='input-field col s1'>"
		+ " 	<i class='material-icons tooltipped' data-position='top' data-delay='0' data-tooltip='{{#if doc}}{{doc}}{{/if}}'>help</i>"
		+ " </div>"
		+ "</div>"
		
		+ "{{#if operator}}"
		+ "<div class='row valign-wrapper targetRow'>"
		+ "	<div class='input-field col s12'>"
		+ "			<input class='paramInput' name='parameters[{{@index}}][operator]' type='text' value='{{operator}}' />"
		+ "			<label style='text-transform: capitalize' for='operator'>Operator</label>"
		+ " </div>"
		+ "</div>"
		+ "{{/if}}"
		+ "{{#if property2}}"
		+ "<div class='row valign-wrapper targetRow'>"
		+ "	<div class='input-field col s12'>"
		+ "			<input class='paramInput {{#if droppableP2}}jstree-drop'{{/if}}' {{#if sourceexpP2}}data-sourceexp='true'{{/if}} name='parameters[{{@index}}][property2]' type='text' " 
		+ "               value='{{property2}}' />"
		+ "			<label style='text-transform: capitalize' for='operator'>Property</label>"
		+ " </div>"
		+ "</div>"
		+ "{{/if}}"
		
		+ "{{/each}}"	
		
		+ "<div id='actionsContainer'>"
		+ "	{{#each actions}}"
		+ "	{{#unless hidden}}"
		+ "		<button class='btn btn-mini' type='button' data-method='{{method}}'>{{name}}</button>"
		+ "	{{/unless}}"	
		+ "	{{/each}}"		
		+ "</div>" 
		+ "</div>"				
		+ "</form>"
		+ "<div id='operatorExtraFormContainer'></div>",
	
	
	connectServiceViewTemplate:"<div class='row'><form id='parameterConnections' class='col s12'>"
			+ "{{#each operation.parameters}}"
			+ "      	<div class='row valign-wrapper'>"
			+ "				<div class='input-field col s11'>"
			+ "             	<input class='paramInput jstree-drop' name='{{name}}' id='{{name}}' value='{{value}}' type='text' />"
			+ "             	<label for='{{name}}'>{{label}}</label>"
			+ "				</div>"
			+ "				<div class='input-field col s1'>"
			+ "             	<i class='material-icons tooltipped' data-position='top' data-delay='0' data-tooltip='{{doc}}'>help</i>"
			+ "				</div>"
			+ "          </div>" 
			+ "{{/each}}"
			+ "</form></div>",
			
	connectBlackboxViewTemplate:"<form id='parameterConnections'><fieldset>"
		+ "{{#each parameters}}"
		+ "      	<div class='control-group'>"
		+ "          <label class='control-label' for='{{name}}'> {{label}}"
		+ "              <a href='#' class='paramPop' rel='popover' data-toggle='popover' "
		+ "               data-content='{{doc}}' >"
		+ "            <i class='icon-question-sign'></i></a></label>"
		+ "          <div class='controls'>"
		+ "              <input class='paramInput jstree-drop' name='{{name}}' value='{{value}}' type='text' />"
		+ "          </div>" + "       </div>" + "{{/each}}"
		+ "</fieldset></form>",			
		
	connectInnerProcessViewTemplate:"<form id='parameterConnections' class='col s12'>"
		+ "{{#each parameters}}"
		+ "      	<div class='row valign-wrapper'>"
		+ "				<div class='input-field col s11'>"
		+ "             	<input class='paramInput jstree-drop' name='{{name}}' id='{{name}}' value='{{value}}' type='text' />"
		+ "             	<label for='{{name}}'>{{label}}</label>"
		+ "				</div>"
		+ "				<div class='input-field col s1'>"
		+ "             	<i class='material-icons tooltipped' data-position='top' data-delay='0' data-tooltip='{{doc}}'>help</i>"
		+ "				</div>"
		+ "          </div>" 
		+ "{{/each}}"		
		+ "</form>",				
					
	init : function(sourceId, targetId) {
		this._super();

		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			this.sourceId = sourceId;
			this.targetId = targetId;
			
			this.setUserData({
				sourceId : this.sourceId, 
				targetId:this.targetId}
			);					
		}
		this.setTargetDecorator(new draw2d.decoration.connection.ArrowDecorator());
		this.setRouter(draw2d.Connection.DEFAULT_ROUTER);
	},			
	_getServiceBindingParameterMap : function() {
		var formFields = $("#parameterConnections").serializeArray();

		var parameterMap = [];
		for ( var i = 0; i < formFields.length; i++) {
			if (formFields[i].value.indexOf("{") == 0) {
				parameterMap.push({
					targetInput : formFields[i].name,
					sourceOutput : formFields[i].value
				});
			}
		}
		
		return parameterMap;
	},
	_getOperatorBindingParameterMap : function() {
		
		var formJson = $("#parameterConnections").serializeJSON();
		
		var customParams = formJson.parameters;
		
		delete formJson.parameters;
				
		var parameterMap = [];
		
		for(var key in formJson)
		{
         
			parameterMap.push({
				name : key,
				property : formJson[key]
			});
		}
		
		var indexes = [];
		
		for(var key in customParams)
		{
			indexes.push(key);
		}
		
		//console.log('indexes', indexes);
	
		if(customParams!==undefined)
		{
			for(var i = 0; i < indexes.length; i++)
			{   		 
				var p = customParams[indexes[i]];
				
				var parameter = {   			  
						name : p.name,
						property : p.property,
						property2 : p.property2,
						operator : p.operator
				};
   		  
				var droppable = $("#parameterConnections input[name='parameters["+indexes[i]+"][property]']").hasClass("jstree-drop");
				var datasourceExp = $("#parameterConnections input[name='parameters["+indexes[i]+"][property]']").data("sourceexp");
   		  
				if(droppable)
				{
					parameter.droppable = true;
				}
				
				if(datasourceExp)
				{
					parameter.sourceexp = true;
				}
   		  
				var droppable2 = $("#parameterConnections input[name='parameters["+indexes[i]+"][property2]']").hasClass("jstree-drop");
				var datasourceExp2 = $("#parameterConnections input[name='parameters["+indexes[i]+"][property2]']").data("sourceexp");
				
				if(droppable2)
				{
					parameter.droppableP2 = true;
				}
				
				if(datasourceExp2)
				{
					parameter.sourceexpP2 = true;
				}
   		     		  
				parameterMap.push(parameter);
			}
		}
				
		return parameterMap;
	},	

	/** 
     * @method
     * Connette i parametri di output del source con quelli di input del target
     * Sup-classes must implement this method.
     *
     * @template
     **/
	_connectParameter : function() {		
	},
	
	/** 
     * @method
     * Disconnnette i parametri di input del target
     * Sup-classes must implement this method.
     *
     * @template
     **/	
	_disconnectParameter : function() {						
	},
	
	/** 
     * @method
     * Riconnette i parametri di output del source con quelli di input del target
     * Sup-classes must implement this method.
     *
     * @template
     **/	
	_reconnectParameter : function(bindingParameter) {			
	},
	
	/** 
     * @method
     * Costruisce l'interfaccia che permette la connessione tra gli shape
     * Sup-classes must implement this method.
     *
     * @template
     **/		
	_buildServiceConnectionView : function(isEditOperation) {		
		var targetServiceDef = this.targetServiceDef;
		var infoTempl = "<p><strong>SERVICE</strong>: {{operation.name}} ({{name}})</p>";
		var infoTemplCompiled = Handlebars.compile(infoTempl);
		var targetInfoView = infoTemplCompiled(targetServiceDef);
		
		$("#targetinfo").html(targetInfoView).append("<p><strong>Target parameters</strong></p>");				

		var compiled = Handlebars.compile(this.connectServiceViewTemplate);
		var view = compiled(targetServiceDef);
	
		var targetContainer = $('#targetparamscontainer');
		$(targetContainer).html(view);
		
		this._connectParamsModalInit(isEditOperation);
	},
	
	_buildOperatorConnectionPanel: function(isEditOperation){
		var targetOperatorDef = this.targetOperatorDef; 
		var infoTempl = "<p><strong>OPERATOR</strong>: {{name}}</p>";
		var infoTemplCompiled = Handlebars.compile(infoTempl);
		var targetInfoView = infoTemplCompiled(targetOperatorDef);
		
		$("#targetinfo").html(targetInfoView).append("<p><strong>Target parameters</strong></p>");		

		var compiled = Handlebars.compile(this.connectOperatorViewTemplate);
		var view = compiled(targetOperatorDef);
		
		var targetContainer = $('#targetparamscontainer');
		$(targetContainer).html(view);
		
		if(targetOperatorDef.actions!==undefined){
			$.each(targetOperatorDef.actions, $.proxy(function(i, action){
				var figure = app.getView().getFigure(this.targetId);
				
				//se non è una action hidden
				if(!action.hidden){
					$("button[data-method='"+action.method+"']", "#targetparamscontainer").on("click", $.proxy(figure[action.method], figure));
				}
				//Se è un operazione di creazione e la action è marcata come trigger on new oppure
				//se è un operazione di edit e la action è marcata come trigger on edit
				//esegue il metodo
				if((!isEditOperation && action.triggerOnNew) || (isEditOperation && action.triggerOnEdit)){
					figure[action.method]();
				}				
			}, this));	
		}
		
		if(targetOperatorDef.events!==undefined){
			$.each(targetOperatorDef.events, $.proxy(function(i, event){
				var figure = app.getView().getFigure(this.targetId);
				var element = $("[data-method='"+event.event+"']", "#targetparamscontainer");
				$("[data-method='"+event.event+"']", "#targetparamscontainer").on(event.event, $.proxy(figure[event.method], figure));			
			}, this));	
		}
		
		if(targetOperatorDef.init !== undefined)
		{
			var figure = app.getView().getFigure(this.targetId);
			figure[targetOperatorDef.init.method]();
		}
	},
	
	_buildOperatorConnectionView: function(isEditOperation) {
		
		this._buildOperatorConnectionPanel(isEditOperation);		
		this._connectParamsModalInit(isEditOperation);	
	},
	_buildBlackboxConnectionView : function(isEditOperation) {		
		var targetBlackboxDef = this.targetBlackboxDef;
		var infoTempl = "<p class='text-info'><strong>{{name}}</strong> - {{instanceId}}</p>";
		var infoTemplCompiled = Handlebars.compile(infoTempl);
		var targetInfoView = infoTemplCompiled(targetBlackboxDef);
		
		$("#targetinfo").html(targetInfoView).append("<legend>Target parameters</legend>");				

		var compiled = Handlebars.compile(this.connectBlackboxViewTemplate);
		var view = compiled(targetBlackboxDef);
	
		var targetContainer = $('#targetparamscontainer');
		$(targetContainer).html(view);
		
		this._connectParamsModalInit(isEditOperation);
	},	
	
	_buildInnerProcessConnectionView : function(isEditOperation) {		
		var targetInnerProcessDef = this.targetInnerProcessDef;
		var infoTempl = "<p><strong>INNER PROCESS</strong>: {{name}}</p>";
		var infoTemplCompiled = Handlebars.compile(infoTempl);
		var targetInfoView = infoTemplCompiled(targetInnerProcessDef);
		
		$("#targetinfo").html(targetInfoView).append("<p><strong>Target parameters</strong></p>");				

		var compiled = Handlebars.compile(this.connectInnerProcessViewTemplate);
		var view = compiled(targetInnerProcessDef);
	
		var targetContainer = $('#targetparamscontainer');
		$(targetContainer).html(view);
		
		this._connectParamsModalInit(isEditOperation);
	},	
	
	_connectServiceParameter : function(){
		var parameterMap = this._getServiceBindingParameterMap();

		// setto i binding dei parametri nel servizio target e imposto
		// la property bind a true per mostrare l'input readonly
		$.each(this.targetServiceDef.operation.parameters, function(j, tParam) {
			//resetto
			if (tParam.bind == true) {
				tParam.value = "";
				tParam.bind = false;
			}
			$.each(parameterMap, function(i, bind) {
				if (bind.targetInput == tParam.name) {
					tParam.value = bind.sourceOutput;
					tParam.bind = true;
				}
			});
		});

		$('#connectParamsModal').closeModal();	
	},
	_connectOperatorParameter : function(){
		var parameterMap = this._getOperatorBindingParameterMap();
		
		var arrayNodePath;
		
		for(var i=0; i< this.targetOperatorDef.parameters.length; i++){
			if(this.targetOperatorDef.parameters[i].custom){
				this.targetOperatorDef.parameters.splice(i, 1);
				i--;
			}
		}
		
		$.each(parameterMap, $.proxy(function(i, bind) {
			
			var parameter;
			
			for(var j=0; j< this.targetOperatorDef.parameters.length; j++){
				if (this.targetOperatorDef.parameters[j].name == bind.name && !this.targetOperatorDef.parameters[j].custom){
					
					parameter = this.targetOperatorDef.parameters[j];
					
					if(this.targetOperatorDef.parameters[j].hasinnerprocess){
						arrayNodePath = bind.property;
					}					
					
					break;
				}
			}
			
			if(parameter!==undefined){
				parameter.property = bind.property;
				parameter.property2 = bind.property2;
				parameter.bind = true;
			} else {
				bind.bind = true;
				bind.custom = true;
				this.targetOperatorDef.parameters.push(bind);
			}
		}, this));
		
		if(arrayNodePath!==undefined){
   		app.getBackend().getTreeNodeDetail(arrayNodePath, $.proxy(function(result){			
   			var tableRenderer = this._getTargetFigure();			
   			
   			tableRenderer.getOperatorDef().outputNode = result;			
   	        
   		}, this));
		}
		
		$('#connectParamsModal').closeModal();		
	},
	_connectBlackboxParameter : function(){
		var parameterMap = this._getServiceBindingParameterMap();

		// setto i binding dei parametri nel servizio target e imposto
		// la property bind a true per mostrare l'input readonly
		$.each(this.targetBlackboxDef.parameters, function(j, tParam) {
			//resetto
			if (tParam.bind == true) {
				tParam.value = "";
				tParam.bind = false;
			}
			$.each(parameterMap, function(i, bind) {
				if (bind.targetInput == tParam.name) {
					tParam.value = bind.sourceOutput;
					tParam.bind = true;
				}
			});
		});

		$('#connectParamsModal').closeModal();	
	},	
	
	_connectInnerProcessParameter : function(){
		var parameterMap = this._getServiceBindingParameterMap();

		// setto i binding dei parametri nel servizio target e imposto
		// la property bind a true per mostrare l'input readonly
		$.each(this.targetInnerProcessDef.parameters, function(j, tParam) {
			//resetto
			if (tParam.bind == true) {
				tParam.value = "";
				tParam.bind = false;
			}
			$.each(parameterMap, function(i, bind) {
				if (bind.targetInput == tParam.name) {
					tParam.value = bind.sourceOutput;
					tParam.bind = true;
				}
			});
		});

		$('#connectParamsModal').closeModal();	
	},		

	_disconnectServiceParameter : function(){
		$.each(this.targetServiceDef.operation.parameters, $.proxy(function(j,
				tParam) {
			
				if (tParam.bind==true && tParam.value.indexOf(this.sourceId) !== -1) {
					tParam.value = null;
					tParam.bind = false;
				}			
		}, this));		
	},
	
	_disconnectEndParameter : function() {
		//si genera una eccezione se targetOutputDef e' undefined e quindi si blocca tutto
		if (this.targetOutputDef != undefined) {
			if (this.targetOutputDef.inputs != undefined) {
      		var inputs = this.targetOutputDef.inputs;
      		
      		for(var i=0; i<inputs.length; i++){
      			if(inputs[i] == this.sourceId){
      				inputs.splice(i, 1);
      			}
      		}
			}
		}
	},		
	
	_disconnectOperatorParameter : function(){
		
		for(var i=0; i<this.targetOperatorDef.parameters.length; i++){
			tParam = this.targetOperatorDef.parameters[i];
			
			if(tParam.custom ==true){
				delete this.targetOperatorDef.parameters[i];
				this.targetOperatorDef.parameters.length--;
				
			} else if (tParam.bind==true && tParam.property.indexOf(this.sourceId) !== -1) {
				tParam.property = null;
				tParam.property2 = null;
				tParam.bind = false;
			}	
		}	
	},
	
	_disconnectBlackboxParameter : function(){
		$.each(this.targetBlackboxDef.parameters, $.proxy(function(j,
				tParam) {
			
				if (tParam.bind==true && tParam.value.indexOf(this.sourceId)!==-1) {
					tParam.value = null;
					tParam.bind = false;
				}			
		}, this));		
	},	
	
	_disconnectInnerProcessParameter : function(){
		$.each(this.targetInnerProcessDef.parameters, $.proxy(function(j,
				tParam) {
			
				if (tParam.bind==true && tParam.value.indexOf(this.sourceId)!==-1) {
					tParam.value = null;
					tParam.bind = false;
				}			
		}, this));		
	},		
	
	_invokeChainService : function(isEditOperation) {
		app.buildInvokeWorkflowWindow(true, this.sourceId, $.proxy(function(data) {
			this.resultData = data;
			this._buildConnectionView(isEditOperation);
					
		}, this), $.proxy(function(){
			if(!isEditOperation) this._remove();
		}, this));
	},
	
	// IVAN
	_invokeOpenDataResource : function(openDataID, query){
		app.invokeOpenDataResource(openDataID, query, $.proxy(function(data) {
			this.resultData = data;
			this._buildConnectionView(false);
					
		}, this));
	},
	_connectParamsModalInit : function(isEditOperation){
		this._buildTreePanel();
		
		var container = $('#connectParamsModal');

		container.find('#connectParamsOk').off('click');//Reset di un precedente event bind
		container.find('#connectParamsOk').on('click', $.proxy(function(e) {
			e.preventDefault();
			
			var figure = app.getView().getFigure(this.targetId);
			
			var isValid = true;
			
			if(this.targetOperatorDef && this.targetOperatorDef.isValid !== undefined)
			{
				isValid = figure[this.targetOperatorDef.isValid.method]();
			}
			
			if(isValid)
			{
				if(this.targetOperatorDef && this.targetOperatorDef.onOk !== undefined)
				{						
					figure[this.targetOperatorDef.onOk.method]();
				}
				
				this._connectParameter();	
				
				$(container).closeModal();
			}
		}, this));
		
		container.find('#connectParamsCancel').off('click');//Reset di un precedente event bind
		container.find('#connectParamsCancel').on('click', $.proxy(function(e) {
			e.preventDefault();
			
			if(this.targetOperatorDef && this.targetOperatorDef.onCancel !== undefined)
			{				
				var figure = app.getView().getFigure(this.targetId);
				figure[this.targetOperatorDef.onCancel.method]();
			}
			
			if(!isEditOperation) this._remove();
			
			$(container).closeModal();
		}, this));	
		
		$('.tooltipped').tooltip();
		$('label').addClass('active');
		$('select').material_select();
		
		$(container).openModal({ dismissible:false });
		/*
		$(".paramPop").popover({
   	    trigger: 'hover',
   	    animation: false,
   	    delay: { show: 100, hide: 1 }
		});		*/
	},
	_buildTreePanel : function() {		
		
		var figureUserData;
		
		if(this.sourceServiceDef !== undefined){
			figureUserData = this.sourceServiceDef;
			
		} else if(this.sourceOperatorDef!==undefined){
			figureUserData = this.sourceOperatorDef;
			
		} else if(this.sourceOpenDataDef!==undefined){
			figureUserData = this.sourceOpenDataDef;
			
		}
				
		buildTreePanel(this.sourceId, this.resultData, figureUserData);
	
	},	
	/**
     * @method 
     * Return an objects with all important attributes for XML or JSON serialization
     * 
     * @returns {Object}
     */
    getPersistentAttributes : function()
    {
        var memento = this._super();
        return memento;
    },
    
    /**
     * @method 
     * Read all attributes from the serialized properties and transfer them into the shape.
     * 
     * @param {Object} memento
     * @returns 
     */
    setPersistentAttributes : function(memento)
    {
        this._super(memento);

        try{
        	
			var userData = this.getUserData();
			
			this.sourceId = userData.sourceId;
			this.targetId = userData.targetId;
			
			this._postInit();
        }
        catch(exc)
        {
     
        }

    },
    _getSourceFigure: function(){
    	return app.getView().getFigure(this.sourceId);
    },
    _getTargetFigure: function(){
    	return app.getView().getFigure(this.targetId);
    },
    _remove: function(){
    	
    	try
    	{
	    	var command= new vme2.command.CommandDelete(this);
	    	command.execute();
    	}
    	catch(ex)
    	{
    		console.error(ex);
    	}
    }
});
