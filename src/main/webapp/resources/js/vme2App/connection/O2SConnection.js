vme2.shape.Connection.Operator.O2S = vme2.shape.Connection.Operator.extend({
	NAME : "vme2.shape.Connection.Operator.O2S",	
	
	init: function(sourceId, targetId){
		this._super(sourceId, targetId);

		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			
			this._postInit();			
			this._invokeChainService();
		}
	},	
	_postInit: function(){		
		this._super();	
		this.targetServiceDef = app.getView().getFigure(this.targetId).getServiceDef();				
	},

	_buildConnectionView : function(isEditOperation) {						
		this._buildServiceConnectionView(isEditOperation);
	},		
	
	_connectParameter : function() {
		this._connectServiceParameter();
	},

	_disconnectParameter : function() {				
		this._disconnectServiceParameter();
	},

	_reconnectParameter : function(bindingParameter) {
		this.targetServiceDef.operation.parameters = bindingParameter;		
	}	
});
