/**
 * Connessione di tipo Service To Iterator Renderer
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Start = vme2.shape.Connection.extend({
	NAME : "vme2.shape.Connection.Start",	

	init : function(sourceId, targetId){
		this._super(sourceId, targetId);
		
		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			
			this._postInit();
			
			this._buildConnectionView();
		}		
	},
	
	_buildConnectionView : function(isEditOperation) {						

		this._buildInputPanel();
		
		var container = $('#connectParamsModal');

		container.find('#connectParamsOk').off('click');//Reset di un precedente event bind
		container.find('#connectParamsOk').on('click', $.proxy(function(e) {
			e.preventDefault();
			
			var figure = app.getView().getFigure(this.targetId);
			
			var isValid = true;
			
			if(this.targetOperatorDef && this.targetOperatorDef.isValid !== undefined)
			{
				isValid = figure[this.targetOperatorDef.isValid.method]();
			}
			
			if(isValid)
			{
				if(this.targetOperatorDef && this.targetOperatorDef.onOk !== undefined)
				{						
					figure[this.targetOperatorDef.onOk.method]();
				}
				
				this._connectParameter();	
				
				$(container).closeModal();
			}
		}, this));

		container.find('#connectParamsCancel').off('click');//Reset di un precedente event bind
		container.find('#connectParamsCancel').on('click', $.proxy(function(e) {
			e.preventDefault();
			
			if(this.targetOperatorDef && this.targetOperatorDef.onCancel !== undefined)
			{				
				var figure = app.getView().getFigure(this.targetId);
				figure[this.targetOperatorDef.onCancel.method]();
			}
			
			if(!isEditOperation) this._remove();
		}, this));	
		

		$('.tooltipped').tooltip();
		$('label').addClass('active');
		$('select').material_select();
		
		$(container).openModal({ dismissible:false });
	},
	_buildInputPanel : function() {						
		var label = app.getView().getFigure(this.sourceId).userData.label;
		var sourceInfoView = "<p><strong>INPUT</strong>: " + label + " (" + this.sourceId + ")</p>";	
		
		$("#sourceinfo").html(sourceInfoView).append("<p><strong>Source tree</strong></p>");

		$("#treecontainer").jstree(
				{
					"json_data" : {
						data : {
							data:{
								title: label
							}
						}
					},
					"dnd" : {
						"drop_finish" : $.proxy(function(data) {
							var dest = data.r;
							var path = "{" + this.sourceId +"}";
							$(dest).val(path);
						}, this)
					},
					"themes" : {
						"theme" : "classic",
						 "dots" : false
					},
					'types': {
				        'types' : {
				            'default' : {
				                'icon' : {
				                    'image' : '/visualcomposer/resources/js/jquery/tree/icons/input_icon.png'
				                },
				                'valid_children' : 'default'
				            }
				        }

				    },
				    "crrm" : {
						"move" : {
							"check_move" : function (m) {
								return false;
							}
						}
					},
					"plugins" : [ "themes", "json_data", "ui", "crrm", "dnd", "types" ]
				}).bind("select_node.jstree", function(event, data) {
		});
	},
	/**
	 * doppio click sulla connessione  
	 */
	onDoubleClick : function() {

			this._buildConnectionView(true);
	}
});
