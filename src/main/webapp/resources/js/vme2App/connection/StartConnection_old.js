/**
 * Connessione di tipo Service To Iterator Renderer
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Start = vme2.shape.Connection.extend({
	NAME : "vme2.shape.Connection.Start",	

	init : function(sourceId, targetId){
		this._super(sourceId, targetId);
		
		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			
			this._postInit();
			
			this._buildConnectionView();
		}		
	},
	_postInit: function(){
		//this._super();		
		this.targetServiceDef = app.getView().getFigure(this.targetId).getServiceDef();						
	},
	/**
	 * doppio click sulla connessione  
	 */
	onDoubleClick : function() {

			this._buildConnectionView(true);
	},
	
	_buildConnectionView : function(isEditOperation) {						
		var targetServiceDef = this.targetServiceDef;
		var infoTempl = "<p class='text-info'><strong>{{name}}</strong> - {{operation.name}}:{{instanceId}}</p>";
		var infoTemplCompiled = Handlebars.compile(infoTempl);
		var targetInfoView = infoTemplCompiled(targetServiceDef);
		
		$("#targetinfo").html(targetInfoView).append("<legend>Target parameters</legend>");		

		var compiled = Handlebars.compile(this.connectServiceViewTemplate);
		var view = compiled(targetServiceDef);
	
		var targetContainer = $('#targetparamscontainer');
		$(targetContainer).html(view);
		
		this._buildInputPanel();
		
		var container = $('#connectParamsModal');

		container.find('.btn-primary').off('click');//Reset di un precedente event bind
		container.find('.btn-primary').on('click', $.proxy(function(e) {
			e.preventDefault();
			this._connectServiceParameter();
		}, this));

		container.find('.btn-cancel').off('click');//Reset di un precedente event bind
		container.find('.btn-cancel').on('click', $.proxy(function(e) {
			e.preventDefault();
			if(!isEditOperation) this._remove();
		}, this));	
		
		$(container).modal();
	},

	_disconnectParameter : function() {				
		this._disconnectServiceParameter();
	},

	
	_buildInputPanel : function() {						
		var sourceInfoView = "<p class='text-info'><strong>name</strong> - operation.name:instanceId</p>";		
		
		$("#sourceinfo").html(sourceInfoView).append("<legend>Source Results</legend>");

		$("#treecontainer").jstree(
				{
					"json_data" : {
						data : {
							data:{
								title: this.sourceId
							}
						}
					},
					"dnd" : {
						"drop_finish" : $.proxy(function(data) {
							var dest = data.r;
							var path = "{" + this.sourceId +"}";
							$(dest).val(path);
						}, this)
					},
					"themes" : {
						"theme" : "classic",
						 "dots" : false
					},
					'types': {
				        'types' : {
				            /*'file' : {
				                'icon' : {
				                    'image' : '/admin/views/images/file.png'
				                }
				            },*/
				            'default' : {
				                'icon' : {
				                    'image' : '/visualcomposer/resources/js/jquery/tree/icons/input_icon.png'
				                },
				                'valid_children' : 'default'
				            }
				        }

				    },
				    "crrm" : {
						"move" : {
							"check_move" : function (m) {
								return false;
							}
						}
					},
					"plugins" : [ "themes", "json_data", "ui", "crrm", "dnd", "types" ]
				}).bind("select_node.jstree", function(event, data) {
			//var sss = $.data(data.rslt.obj[0]).path;
			//alert(sss);
		});
	},
		
	_reconnectParameter : function(bindingParameter) {
		this.targetServiceDef.operation.parameters = bindingParameter;		
	}	
});
