/**
 * Connessione generica
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Operator = vme2.shape.Connection.extend({
	NAME : "vme2.shape.Connection.Operator",
	/**
	 * doppio click sulla connessione  
	 */
	
	_postInit: function(){		
		
		this.sourceOperatorDef = app.getView().getFigure(this.sourceId).getOperatorDef();		
	},
	onDoubleClick : function() {
		/*
		if (this.resultData != undefined && this.resultData != null)
			this._buildConnectionView(true);
		else {
			this._invokeChainService(true);
		}*/
		
		// IVAN invoke chain every time
		
		this._invokeChainService(true);
	},
});
