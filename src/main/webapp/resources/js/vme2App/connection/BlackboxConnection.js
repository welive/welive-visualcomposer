/**
 * Connessione generica
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Blackbox = vme2.shape.Connection.extend({
	NAME : "vme2.shape.Connection.Blackbox",
	
	init : function(sourceId, targetId) {
		this._super(sourceId, targetId);

		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			
			this._postInit();
		}
	},
	
	_postInit: function(){		
		
		this.sourceBlackboxDef = app.getView().getFigure(this.sourceId).getBlackboxDef();	
	},
		
	/**
	 * doppio click sulla connessione  
	 */
	onDoubleClick : function() {
		if (this.resultData != undefined && this.resultData != null)
			this._buildConnectionView(true);
		else {
			this._invokeChainService(true);
		}
	},
	_onOk : function() {
		var paramsMap = $("#requiredParamsMap").serializeArray();

		for ( var i = 0; i < paramsMap.length; i++) {
			this.invokeObj.parameters.push({
				name : paramsMap[i].name,
				value : paramsMap[i].value
			});
		}
		this._invokeService();
	}
});
