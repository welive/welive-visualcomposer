vme2.shape.Connection.OpenData.OD2O = vme2.shape.Connection.OpenData.extend({
	NAME : "vme2.shape.Connection.OpenData.OD2O",		
	init: function(sourceId, targetId){
		this._super(sourceId, targetId);

		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			
			this._postInit();		
			// IVAN
			this._invokeOpenDataResource(app.getView().getFigure(this.sourceId).getOpenDataDef().resourceId,
					app.getView().getFigure(this.sourceId).getOpenDataDef().query);
		}
		
		this._super(sourceId, targetId);
	},
	
	_postInit: function(){		
		
		this._super();		
		this.targetOperatorDef = app.getView().getFigure(this.targetId).getOperatorDef();						
	},	
	
	_buildConnectionView : function(isEditOperation) {						
		this._buildOperatorConnectionView(isEditOperation);
	},	

	_connectParameter : function() {
		this._connectOperatorParameter();
	},
	
	_getCustomParameter : function() {		
		
		return $("#parameterConnections").serializeJSON();
	},	
	_disconnectParameter : function() {				
		this._disconnectOperatorParameter();
	},
	_reconnectParameter : function(bindingParameter) {
		this.targetOperatorDef.parameters = bindingParameter;		
	}
});
