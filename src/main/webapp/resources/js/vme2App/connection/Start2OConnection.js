/**
 * Connessione di tipo Service To Iterator Renderer
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Start.Start2O = vme2.shape.Connection.Start.extend({
	NAME : "vme2.shape.Connection.Start.Start2O",	

	_postInit: function(){		
		this.targetOperatorDef = app.getView().getFigure(this.targetId).getOperatorDef();						
	},
	_buildConnectionView : function(isEditOperation) {						
//		var targetOperatorDef = this.targetOperatorDef;
//		var infoTempl = "<p class='text-info'><strong>{{name}}</strong> - {{instanceId}}</p>";		
//		var infoTemplCompiled = Handlebars.compile(infoTempl);
//		var targetInfoView = infoTemplCompiled(targetOperatorDef);
//		
//		$("#targetinfo").html(targetInfoView).append("<legend>Target parameters</legend>");		
//		
//		var compiled = Handlebars.compile(this.connectOperatorViewTemplate);
//		var view = compiled(targetOperatorDef);
//	
//		var targetContainer = $('#targetparamscontainer');
//		$(targetContainer).html(view);		
//		
//		this._super(isEditOperation);			
		
		this._buildOperatorConnectionPanel(isEditOperation);
		this._super(isEditOperation);	
	},
	
	_connectParameter : function() {
		this._connectOperatorParameter();
	},
	_disconnectParameter : function() {				
		this._disconnectOperatorParameter();
	},	
		
	_reconnectParameter : function(bindingParameter) {
		this.targetOperatorDef.parameters = bindingParameter;		
	}	
});
