vme2.shape.Connection.Service.S2O = vme2.shape.Connection.Service.extend({
	NAME : "vme2.shape.Connection.Service.S2O",		
		
	_postInit: function(){		
		
		this._super();		
		this.targetOperatorDef = app.getView().getFigure(this.targetId).getOperatorDef();						
	},	
	
	_buildConnectionView : function(isEditOperation) {						
		this._buildOperatorConnectionView(isEditOperation);
	},	

	_connectParameter : function() {
		this._connectOperatorParameter();
	},
	
	_getCustomParameter : function() {		
		
		return $("#parameterConnections").serializeJSON();
	},	
	_disconnectParameter : function() {				
		this._disconnectOperatorParameter();
	},
	_reconnectParameter : function(bindingParameter) {
		this.targetOperatorDef.parameters = bindingParameter;		
	}
});
