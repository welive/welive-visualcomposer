/**
 * Connessione di tipo Service To Iterator Renderer
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Start.Start2B = vme2.shape.Connection.Start.extend({
	NAME : "vme2.shape.Connection.Start.Start2B",	

	_postInit: function(){		
		this.targetBlackboxDef = app.getView().getFigure(this.targetId).getBlackboxDef();						
	},
	_buildConnectionView : function(isEditOperation) {						
		
		var targetBlackboxDef = this.targetBlackboxDef;
		var infoTempl = "<p class='text-info'><strong>{{name}}</strong> - {{instanceId}}</p>";
		var infoTemplCompiled = Handlebars.compile(infoTempl);
		var targetInfoView = infoTemplCompiled(targetBlackboxDef);
		
		$("#targetinfo").html(targetInfoView).append("<legend>Target parameters</legend>");		
		
		var compiled = Handlebars.compile(this.connectBlackboxViewTemplate);
		var view = compiled(targetBlackboxDef);
	
		var targetContainer = $('#targetparamscontainer');
		$(targetContainer).html(view);
		
		this._super(isEditOperation);
	},
	_connectParameter : function() {
		this._connectBlackboxParameter();
	},
	
	_disconnectParameter : function() {				
		this._disconnectBlackboxParameter();
	},	
		
	_reconnectParameter : function(bindingParameter) {
		this.targetBlackboxDef.parameters = bindingParameter;		
	}	
});
