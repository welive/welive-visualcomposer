vme2.shape.Connection.Blackbox.B2O = vme2.shape.Connection.Blackbox.extend({
	NAME : "vme2.shape.Connection.Blackbox.B2O",		
	init: function(sourceId, targetId){
		this._super(sourceId, targetId);

		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			
			this._postInit();			
			this._invokeChainService();
		}
	},
	
	_postInit: function(){		
		
		this._super();		
		this.targetOperatorDef = app.getView().getFigure(this.targetId).getOperatorDef();						
	},	
	
	_buildConnectionView : function(isEditOperation) {						
		this._buildOperatorConnectionView(isEditOperation);
	},	

	_connectParameter : function() {
		this._connectOperatorParameter();
	},
	
	_getCustomParameter : function() {		
		
		return $("#parameterConnections").serializeJSON();
	},	
	_disconnectParameter : function() {				
		this._disconnectOperatorParameter();
	},
	_reconnectParameter : function(bindingParameter) {
		this.targetOperatorDef.parameters = bindingParameter;		
	}
});
