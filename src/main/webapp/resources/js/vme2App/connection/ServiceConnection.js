/**
 * Connessione generica
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Service = vme2.shape.Connection.extend({
	NAME : "vme2.shape.Connection.Service",
	
	init : function(sourceId, targetId) {
		this._super(sourceId, targetId);

		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			
			this._postInit();
			if(this.NAME!=="vme2.shape.Connection.Service.S2E"){
				this._invokeChainService();
			}
		}
	},
	
	_postInit: function(){		
		
		this.sourceServiceDef = app.getView().getFigure(this.sourceId).getServiceDef();
		
		var serviceDef = this.sourceServiceDef;
		var invokeObj = {};
		this.invokeObj = invokeObj;

		invokeObj.serviceType = serviceDef.operation.serviceType;
		invokeObj.serviceId = serviceDef.operation.serviceId;
		invokeObj.operationId = serviceDef.operation.id;
		invokeObj.parameters = [];

		var requiredParams = [];
		this.requiredParams = requiredParams;

		$.each(serviceDef.operation.parameters, function(idx, param) {
			if (("value" in param && param.value != null &&  param.value != "") || param.required || param.minOccurs>0) {
				if (param.name == 'alt') {
					param.value = 'json';//Provvisorio
				}
				if ((param.required == true || param.minOccurs>0)	&& (param.value == undefined || param.value == null || param.value == "")) {
					requiredParams.push({
						name : param.name
					});
				} else {
					invokeObj.parameters.push({
						name : param.name,
						value : param.value
					});
				}
			}
		});				
	},
		
	/**
	 * doppio click sulla connessione  
	 */
	onDoubleClick : function() {
		/*
		if (this.resultData != undefined && this.resultData != null)
			this._buildConnectionView(true);
		else {
			this._invokeChainService(true);
		}*/
		
		// IVAN invoke chain every time
		
		this._invokeChainService(true);
	},
	_onOk : function() {
		var paramsMap = $("#requiredParamsMap").serializeArray();

		for ( var i = 0; i < paramsMap.length; i++) {
			this.invokeObj.parameters.push({
				name : paramsMap[i].name,
				value : paramsMap[i].value
			});
		}
		this._invokeService();
	}
});
