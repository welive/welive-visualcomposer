/**
 * Connessione di tipo Service To Service
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Service.S2E = vme2.shape.Connection.Service.extend({
	NAME : "vme2.shape.Connection.Service.S2E",
	init:function(sourceId, targetId){
		this._super(sourceId, targetId);
		
		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			this.sourceId = sourceId;
			this.targetId = targetId;
			this.targetFigure = app.getView().getFigure(this.targetId);		
			this.targetOutputDef = this.targetFigure.getUserData();
		
			this._connectParameter();
		}
	},
	_postInit: function(){			
		this.targetOutputDef = app.getView().getFigure(this.targetId).getUserData();						
	},
	_connectParameter : function() {				
		this.targetOutputDef.inputs.push(this.sourceId);
	},
	_disconnectParameter : function() {
		this._disconnectEndParameter();
	},
	_reconnectParameter : function(bindingParameter) {
		this.targetOutputDef.inputs = bindingParameter;		
	}
});
