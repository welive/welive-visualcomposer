/**
 * Connessione di tipo Service To Service
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Service.S2B = vme2.shape.Connection.Service.extend({
	NAME : "vme2.shape.Connection.Service.S2B",	

	_postInit: function(){
		this._super();		
		this.targetBlackboxDef = app.getView().getFigure(this.targetId).getBlackboxDef();						
	},	

	_buildConnectionView : function(isEditOperation) {						
		this._buildBlackboxConnectionView(isEditOperation);
	},	
	
	_connectParameter : function() {
		this._connectBlackboxParameter();
	},

	_disconnectParameter : function() {				
		this._disconnectBlackboxParameter();
	},

	_reconnectParameter : function(bindingParameter) {
		this.targetBlackboxDef.parameters = bindingParameter;		
	}
});
