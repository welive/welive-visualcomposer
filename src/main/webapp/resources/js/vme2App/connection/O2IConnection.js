/**
 * Connessione di tipo Service To Service
 * @extends draw2d.shape.state.Connection
 */
vme2.shape.Connection.Operator.O2I = vme2.shape.Connection.Operator.extend({
	NAME : "vme2.shape.Connection.Operator.O2I",	

	init: function(sourceId, targetId){
		this._super(sourceId, targetId);

		if (sourceId != undefined && sourceId != "" && targetId != undefined && targetId != "") {
			
			this._postInit();			
			this._invokeJSONConverter();
		}
	},
	_postInit: function(){
		this._super();		
		
		this.sourceFigure = app.getView().getFigure(this.sourceId);				
		this.invokeObj = this.sourceFigure.getOperatorDef().outputNode;
		this.targetInnerProcessDef = app.getView().getFigure(this.targetId).getInnerProcessDef();						
	},	

	_invokeJSONConverter: function(isEditOperation){

		app.invokeJSONConverter(this.invokeObj, $.proxy(function(data) {
			this.resultData = data;
			this._buildConnectionView(isEditOperation);

		}, this));
	},
	_buildConnectionView : function(isEditOperation) {						
		this._buildInnerProcessConnectionView(isEditOperation);
	},	
	
	_connectParameter : function() {
		this._connectInnerProcessParameter();
	},

	_disconnectParameter : function() {				
		this._disconnectInnerProcessParameter();
	},

	_reconnectParameter : function(bindingParameter) {
		this.targetInnerProcessDef.parameters = bindingParameter;		
	}
});
