function getDatasetSchema(resourceId)
{
	if(typeof opendataSchemas == 'undefined')
	{
		console.log("Define opendataSchemas");
		opendataSchemas = [];
	}
	else
	{
		if(opendataSchemas[resourceId])
			return opendataSchemas[resourceId];
	}
	
	var schema;
	
	$.ajax({
		type: "GET",
		url: '/visualcomposer/user/opendata/' + resourceId + '/schema',
		async: false,
		success :function(result)
		{
			schema = JSON.parse(result);
		}
	});
	
	console.log("Store schema for resource " + resourceId);
	opendataSchemas[resourceId] = schema;
	
	return schema;
}

function getQueryResults(resourceId, query)
{
	var results;
	
	$.ajax({
		type: "POST",
		url: '/visualcomposer/user/opendata/' + resourceId + '/query',
		data: 'queryText=' + encodeURI(query),
		async: false,
		success :function(data)
		{
			results = JSON.parse(data);
		}
	});	
	
	return results;
}

function drawSchemaTable(schema)
{
	var content = "";
	
	for(var i = 0; i < schema.tables.length; i++)
	{
		content += "<strong  class=\"opSection\">" + Message['catalogs.details.resources.table.title'] + ": " + schema.tables[i].name + "</strong>";
		content += '<div style="padding:1em">'
		+ '<table class="striped">'
			+ '<thead>'
				+ '<tr>'
					+ '<td class="col s4">'
						+ '<strong>' + Message['catalogs.details.resources.table.column_name'] + '</strong>'
					+ '</td>'
					+ '<td class="col s4">'
						+ '<strong>' + Message['catalogs.details.resources.table.type'] + '</strong>'
					+ '</td>'
					+ '<td class="col s4">'
						+ '<strong>' + Message['catalogs.details.resources.table.key'] + '</strong>'
					+ '</td>'
				+ '</tr>'
		+ '</thead>'
		+ '<tbody>';
		
		for(var j = 0; j < schema.tables[i].columns.length; j++)
		{
			content += ''
				+ '<tr>'
					+ '<td class="col s4">'
					+ '<b>' + schema.tables[i].columns[j].name + '</b>'
					+ '</td>'
					+ '<td class="col s4">'
					+ schema.tables[i].columns[j].type
					+ '</td>'
					+ '<td class="col s4">'
					+ schema.tables[i].columns[j].key
					+ '</td>'
				+ '</tr>';
		}
		
		content += ''
		+ '</tbody>'
		+ '</table></div>'
	}
	
	return content;
}

function drawQueryResults(results)
{
	var content = "";			
	content += "<h5>" + "Results" + ": " + results.count + " rows found" + "</h5>";
	
	if(results.count > 0)
	{
		content += ''
		+ '<table class="striped">'
			+ '<thead>'
				+ '<tr>';
		
		// draw table header
		
		var schema = getDatasetSchema($("#resourceIdQuery").val());
		var table;
		
		var selectedColumns = $('#addcolumns').val().trim().split(",");
		
		for(var i = 0; i < schema.tables.length; i++)
		{
			if(schema.tables[i].name == $("#tableNameVar").val())
			{
				table = schema.tables[i];
				break;
			}
		}
		
		for(var j = 0; j < table.columns.length; j++)
		{
			if($('#addcolumns').val().trim() != "" && selectedColumns.indexOf(table.columns[j].name) == -1)
				continue;
			
			content += ''
					+ '<td>'
					+ '<strong>' + table.columns[j].name + '</strong>'
					+ '</td>';
		}
	
		content += ''
				+ '</tr>'
		+ '</thead>'
		+ '<tbody>';
		
		// draw table body
	
		for(var i = 0; i < results.count; i++)
		{
			content += ''
				+ '<tr>';
			
			for(var j = 0; j < table.columns.length; j++)
			{
				if($('#addcolumns').val().trim() != "" && selectedColumns.indexOf(table.columns[j].name) == -1)
					continue;
				
				content += ''
					+ '<td>'
					+ results.rows[i][table.columns[j].name]
					+ '</td>';
			}
			
			content += ''
				+ '</tr>';					
		}
		
		content += ''
			+ '</tbody>'
			+ '</table>';
	}
	
	return content;
}

/* SQL QUERY BUILDER */

function updateQueryText()
{
	var __select = $('#addcolumns').val();
	
	var _select = "*";
	
	if(__select != "")
	{
		var cols = __select.split(",");
		
		_select = "[" + cols[0] + "]";
		
		for(var i = 1; i < cols.length; i++)
		{
			_select += ",[" + cols[i] + "]";
		}
	}
	
	var selectText = "SELECT " + _select + " FROM " + $("#tableNameVar").val();	
	var whereText = "";
	var orderByText = "";
	var limitText = "";
	
	var result = $('#builder').queryBuilder('getSQL', false);
	  
	if(result.sql.length) 
	{
		whereText = " WHERE " + result.sql;
	}
	
	if($("#queryOrderBy").val() != "")
		orderByText = " ORDER BY " + $("#queryOrderBy").val() + " " + $("#queryOrderByDir").val();
	
	if($("#queryLimit").val() != "0")
		limitText = " LIMIT " + $("#queryLimit").val();
	
	$('#queryText').val(selectText + whereText + orderByText + limitText);
}

function updateQueryEditor(q)
{
	//var q = $('#queryText').val().trim();
	var selectPart, fromPart, wherePart, orderbyPart, limitPart;
	
	var whereIndex = q.indexOf(" WHERE ");
	var orderbyIndex = q.indexOf(" ORDER BY ");
	var limitIndex = q.indexOf(" LIMIT ");
	
	// Find SELECT part of the query
	if(whereIndex != -1)
		selectPart = q.substring(0, whereIndex);
	else if(orderbyIndex != -1)
		selectPart = q.substring(0, orderbyIndex);
	else if(limitPart != -1)
		selectPart = q.substring(0, limitPart);
	else
		selectPart = q;
	
	var tmp = selectPart.match(/SELECT (.+) FROM (.+)/);
	selectPart = tmp[1];
	fromPart = tmp[2].trim();
	
	//if(tmp[2] != $("#tableNameVar").val()) return "Invalid table name";
	
	selectPart = selectPart.replace(new RegExp("\\[", 'g'), "").replace(new RegExp("\\]", 'g'), "");
	
	if(selectPart.trim() == "") return "Invalid SELECT statement";
	
	if(selectPart == "*") selectPart = "";
	
	// Find WHERE part of the query
	
	wherePart = "";
	
	if(whereIndex != -1)
	{
		whereIndex += 7;
		
		if(orderbyIndex != -1)
			wherePart = q.substring(whereIndex, orderbyIndex);
		else if(limitPart != -1)
			wherePart = q.substring(whereIndex, limitPart);
		else
			wherePart = q.substring(whereIndex);
	}
	
	// Find ORDER BY part of the query
	
	orderbyPart = "";
	
	if(orderbyIndex != -1)
	{
		orderbyIndex += 10;
		
		if(limitPart != -1)
			orderbyPart = q.substring(orderbyIndex, limitPart);
		else
			orderbyPart = q.substring(orderbyIndex);
	}
	
	// Find LIMIT part of the query
	
	limitPart = "0";
	
	if(limitIndex != -1)
	{
		limitIndex += 7;
		
		limitPart = q.substring(limitIndex);
	}
	
	initComponents(fromPart);
	
	console.log("SELECT: " + selectPart);
	$("#addcolumns").select2("val", selectPart.split(","));
	
	console.log("FROM: " + fromPart);
	$("#tableNameVar").val(fromPart);
	$("#queryFrom").val(fromPart);
	
	console.log("WHERE: " + wherePart);
	if(wherePart != "")
	{
		try
		{
			$('#builder').queryBuilder('setRulesFromSQL', wherePart);
		}
		catch(e)
		{
			console.log(e);
			return "Invalid WHERE statement";
		}
	}
	
	console.log("ORDER BY: " + orderbyPart);
	if(orderbyPart != "")
	{
		var a = orderbyPart.split(" ");
		$("#queryOrderBy").val(a[0]);
		$("#queryOrderByDir").val(a[1]);
	}
	
	console.log("LIMIT: " + limitPart);
	if(limitPart != "0")
		$("#queryLimit").val(limitPart);
	
	$('#queryText').val(q);
}

$('#btn-reset').on('click', function() 
{
	$('#builder').queryBuilder('reset');
	  
	updateQueryText();
});

$('#builder').on('change', '.form-control', function() 
{
	updateQueryText();
});

$('#addcolumns').on('change', function() 
{
	updateQueryText();
});

$('#queryFrom').on('change', function() 
{
	var prevTable = $("#tableNameVar").val();
	
	if(prevTable != $('#queryFrom').val())
	{
		cleanQuery();
		$('#builder').queryBuilder('destroy');
		initComponents($('#queryFrom').val());
		setInitQuery();
	}
});

$('#queryOrderBy').on('change', function() 
{
	updateQueryText();
});

$('#queryOrderByDir').on('change', function() 
{
	updateQueryText();
});

$('#queryLimit').on('change', function() 
{
	updateQueryText();
});

$('#builder').on('click', '.btn', function() 
{
	updateQueryText();	
});

/*
$('#queryText').on('change', function()
{
	updateQueryEditor();
});*/

function format(item) 
{ 
	return item.text; 
}
function formatResult(item) 
{ 
	return '<div ><strong>'+item.text+'</strong></div>';
}

function cleanQuery()
{
	try
	{
		$('#builder').queryBuilder('reset');
	} 
	catch(e){}
	
	$('#addcolumns').val("");
	$("#addcolumns").select2("val", "");
	$("#queryOrderByDir").val("ASC");
	$('#queryOrderBy').val("");
	$('#queryLimit').val(0);
	
	$('select').material_select();	
}

function setInitQuery()
{
	$('#queryText').val("SELECT * FROM " + $("#tableNameVar").val());
}

function initComponents(tableName)
{console.log(tableName);
	var json = getDatasetSchema($("#resourceIdQuery").val());
	
	var table;
	
	var fromOptions = "";
	
	for(var i = 0; i < json.tables.length; i++)
	{
		fromOptions += "<option value='" + json.tables[i].name + "'" + (i == 0 ? " selected" : "") + ">" + json.tables[i].name + "</option>";
	}
	
	$("#queryFrom").html(fromOptions);
	
	if(tableName == null)
	{				
		table = json.tables[0];
	}
	else
	{
		for(var i = 0; i < json.tables.length; i++)
		{
			if(json.tables[i].name == tableName)
			{
				table = json.tables[i];
				break;
			}
		}
	}
	
	$("#tableNameVar").val(table.name);	
	
	var schema = [];
	var selectItems = [];
	
	var col,item;
	var curr_type;
	
	for(var j = 0; j < table.columns.length; j++)
	{
		col = {};
		col.id = table.columns[j].name;
		col.label = table.columns[j].name;
		
		curr_type = table.columns[j].type;
		
		if(curr_type == "VARCHAR")
			col.type = "string";
		else if(curr_type == "FLOAT")
			col.type = "double";
		else
			col.type = table.columns[j].type.toLowerCase();
		
		item = {};
		item.id = table.columns[j].name;
		item.text = table.columns[j].name;
		
		if(col.id.indexOf(' ') == -1)
			schema.push(col);
		
		selectItems.push(item);
	}	
	
	// Order By section
	var options = "<option value=''>None</option>";
	
	for(var i = 0; i < schema.length; i++)
		options += "<option value='" + schema[i].id + "'>" + schema[i].id + "</option>";
		
	$("#queryOrderBy").html(options);
	
	// Query Builder
	$('#builder').queryBuilder({
    	filters: schema
  	});
	
	// Select2
	$('#addcolumns').select2({
		width:'resolve',		
		multiple:true,
	    placeholder: 'Insert column name...',
	    //Does the user have to enter any data before sending the ajax request
	    minimumInputLength: 0,            
	    allowClear: true,
	    data: selectItems,
	    formatSelection: format,
        formatResult: formatResult
	});
	
	$('select').material_select();	
}

function showQuery(resourceId, query)
{
	$("#resourceIdQuery").val(resourceId);
	
	//$('#queryText').val("SELECT * FROM " + $("#tableNameVar").val());
	
	if(query != "")
		updateQueryEditor(query.trim());
	else
	{
		initComponents(null);
		cleanQuery();
		setInitQuery();
	}
	
	$('select').material_select();
	
	$("#queryModal").openModal();
}
