vme2.propertypane.PropertyPaneService = Class.extend({
	NAME : "vme2.propertypane.PropertyPaneService",
	
	init:function(stateFigure){
	    // selected figure
	    this.figure = stateFigure;
	    
	    // all activities provided by the backend
	    this.activities = null;	    	    
	},
	
	injectPropertyView: function( domId)
	{
		var serviceDef = this.figure.getServiceDef();
		
		var templ= "<h4 class='text-info'>{{name}}</h4>" +
			       "<p class='text-success'>{{operation.name}}&nbsp;" +
			       "<a href='#' class='paramPop' rel='popover' data-toggle='popover' " +
			       " data-content='{{operation.doc}}'>" +
			       "<i class='icon-question-sign'></i></a></p>" +
			       "<p class='text-warning'>({{instanceId}})</p>" +
				   "<form><fieldset>"+
			       "<legend>Input parameters</legend>"+			       
				   //'<input type="button" id="btnInvoke" value="Invoke" class="btn btn-primary" />'+	
			       "{{#each operation.parameters}}"+	
			       "      	<div class='control-group'>"+
			       "          <label class='control-label' for='{{name}}'> {{label}}"+
			       "              <a href='#' class='paramPop' rel='popover' data-toggle='popover' " +
			       "               data-content='{{doc}}' >" +
			       "            <i class='icon-question-sign'></i></a></label>"+
			       "          <div class='controls'>"+
			       "            {{#if options}}"+
			       "              <select class='paramInput' name='{{name}}'>"+
			       "              <option></option>" +
			       "              {{#each options}}" +
			       "                <option {{selected this ../value}} >{{this}}</option>" +
			       "              {{/each}}"+		
			       "              </select>"+
				   "            {{else}}"+
				   "              <input class='paramInput' name='{{name}}' type='text' " +
				   "               {{#if bind}}disabled='disabled'{{/if}}  value='{{value}}' />"+
				   "            {{/if}}"+
			       "          </div>"+
			       "       </div>"+
			       "{{/each}}"+
			       "</fieldset></form>";
			       
		
		
		var compiled = Handlebars.compile(templ);
		var view   = $(compiled(serviceDef));
        view.submit(function(e){
            return false;
        });
        
        domId.append(view);
        
        var _this = this;
        
        $(".paramInput").change(function(){
        	_this.onParamChange($(this));
        });
        /*
        $(".paramPop").popover({
        	    trigger: 'hover',
        	    animation: false,
        	    delay: { show: 100, hide: 1 }
        });*/
        
//        $("#btnInvoke").on('click', function(){
//        	_this.invokeService();
//        });
        

	},	
	
	onParamChange: function(input){
		var val = input.val();
		var name = input.attr("name");
		
		var serviceDef = this.figure.getServiceDef();
		$.each(serviceDef.operation.parameters, function(idx, param){
			if (param.name == name){
				param.value = val;
				app.toolbar.saveButton.attr('disabled', false);
		    	
				return false;
			}
		});
		
	},
	
//	invokeService: function(){
//		var serviceDef = this.figure.getServiceDef();
//		var invokeObj = {};
//		invokeObj.serviceType = serviceDef.operation.serviceType;
//		invokeObj.serviceId = serviceDef.operation.serviceId;		 
//		invokeObj.operationId = serviceDef.operation.id;
//		invokeObj.parameters= [];
//		$.each(serviceDef.operation.parameters, function(idx, param){
//			if ("value" in param && param.value != null && param.value != undefined && param.value!=""){
//				invokeObj.parameters.push({name:param.name, value:param.value});
//			}
//		});
//		
//		app.invokeService(invokeObj, function(data){
//			console.log(data);
//		});
//
//	},

	
	
    /**
     * @method
     * called by the framework if the pane has been resized. This is a good moment to adjust the layout if
     * required.
     * 
     */
    onResize: function()
    {
    },
    

    /**
     * @method
     * called by the framework before the pane will be removed from the DOM tree
     * 
     */
    onHide: function()
    {
    }
    



});

