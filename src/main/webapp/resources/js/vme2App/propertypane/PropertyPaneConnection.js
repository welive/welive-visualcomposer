vme2.propertypane.PropertyPaneConnection = Class.extend({
	
	init:function(stateFigure){
	    this.figure = stateFigure;
	    this.table = null;
	},
	
	injectPropertyView: function( domId)
	{
		var connection = this.figure;
		
		var templ= "<h4 class='text-info'>{{id}}</h4>" +
				   "<p class='text-success'>{{sourcePort.parent.id}}:{{sourcePort.name}}</p>" +
			       "<p class='text-success'>{{targetPort.parent.id}}:{{targetPort.name}}</p>" +
				   "<form><fieldset>"+
			       "<legend>Connection parameters</legend>"+			       
			       "{{#each userData.parameterMap}}"+	
			       "      	<div class='control-group'>"+
			       "          <label class='control-label' for='{{targetInput}}'> {{targetInput}}"+
			       //"              <a href='#' class='paramPop' rel='popover' data-toggle='popover' " +
			       //"               data-content='{{doc}}' >" +
			       //"            <i class='icon-question-sign'></i></a>" +
			       "		  </label>"+
			       "          <div class='controls'>"+
				   "              <input class='paramInput' name='{{targetInput}}' type='text' value='{{sourceOutput}}' />"+
			       "          </div>"+
			       "       </div>"+
			       "{{/each}}"+
			       "</fieldset></form>";
			       
		
		
		var compiled = Handlebars.compile(templ);
		var view   = $(compiled(connection));
        view.submit(function(e){
            return false;
        });
        
        domId.append(view);        
	},

    /**
     * @method
     * called by the framework if the pane has been resized. This is a good moment to adjust the layout if
     * required.
     * 
     */
    onResize: function()
    {
        // trigger a resize
        //this.table.handsontable('render');
    },

    /**
     * @method
     * called by the framework before the pane will be removed from the DOM tree
     * 
     */
    onHide: function()
    {
        //this.table.handsontable('destroy');
    }
   

});

