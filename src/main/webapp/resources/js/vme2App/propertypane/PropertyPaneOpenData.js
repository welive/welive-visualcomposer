vme2.propertypane.PropertyPaneOpenData = Class.extend({
	NAME : "vme2.propertypane.PropertyPaneOpenData",
	
	init:function(stateFigure){
		
	    // selected figure
	    this.figure = stateFigure;
	    
	    // all activities provided by the backend
	    this.activities = null;	    	    
	},
	
	injectPropertyView: function(domId)
	{
		var opendataDef = this.figure.getOpenDataDef();
		//console.log(opendataDef);
		
		var templ= "<h4 class='text-info'>{{name}}</h4>" +
			       "<p class='text-success'>{{resourceTitle}}&nbsp;" +
			       "<p class='text-warning'>({{instanceId}})</p><hr>" +
				   "<h5>Query Text</h5>"+
			       "<textarea id='propPaneQueryText' class='input-xxlarge' style='width:95%; resize: none' readonly>{{query}}</textarea>"+
			       "<button class='btn btn-primary' id='editQuery'>Edit</button><br><hr>";
			       
		
		
		var compiled = Handlebars.compile(templ);
		var view   = $(compiled(opendataDef));
		
        view.submit(function(e){
            return false;
        });
        
        var schemaTable = "";
        
        $.ajax({
    		type: "GET",
    		url: '/visualcomposer/user/opendata/' + opendataDef.resourceId + '/schema',
    		async: false,
    		success :function(result)
    		{
    			var schema = JSON.parse(result);
    			
    			var content = "";
    			
    			for(var i = 0; i < schema.tables.length; i++)
    			{
    				content += "<div>";			
    				content += "<h5>" + "Table Name" + ": <span class='text-success'>" + schema.tables[i].name + "</span></h5>";
    				content += ''
    				+ '<table class="table table-striped">'
    					+ '<thead>'
    						+ '<tr>'
    							+ '<td class="span1">'
    								+ '<strong>' + "Column Name" + '</strong>'
    							+ '</td>'
    							+ '<td class="span1">'
    								+ '<strong>' + "Type" + '</strong>'
    							+ '</td>'
    							+ '<td class="span1">'
    								+ '<strong>' + "Key" + '</strong>'
    							+ '</td>'
    						+ '</tr>'
    				+ '</thead>'
    				+ '<tbody>';
    				
    				for(var j = 0; j < schema.tables[i].columns.length; j++)
    				{
    					content += ''
    						+ '<tr>'
    							+ '<td>'
    							+ '<b>' + schema.tables[i].columns[j].name + '</b>'
    							+ '</td>'
    							+ '<td>'
    							+ schema.tables[i].columns[j].type
    							+ '</td>'
    							+ '<td>'
    							+ schema.tables[i].columns[j].key
    							+ '</td>'
    						+ '</tr>';
    				}
    				
    				content += ''
    				+ '</tbody>'
    				+ '</table>'
    				+ '</div><hr />';
    			}	
    			
    			schemaTable = content;
    		}    			
    		
    	});
        
        domId.append(view);
        domId.append(schemaTable);
        
        var _this = this;
        /*
        $("#saveQuery").click(function(){
        	
        	var query = $("#propPaneQueryText").val().trim();
			
			if(query == "")
			{
				alert("Please enter query text!");
				return;
			}
        	
        	_this.onQueryChange(query);
        });     
        */
        
        $("#editQuery").click(function()
        {
	        // IMPORTANT!!! remove previous handlers!
			$('#queryModalPrimary').off('click');			
			$('#queryModalPrimary').val("Save");			
			$('#queryModalPrimary').on('click', $.proxy(function(e) {
				
				var query = $("#queryText").val().trim();
				
				if(query == "")
				{
					alert("Please enter query text!");
					return;
				}
				
				$("#propPaneQueryText").val(query);
				
				_this.onQueryChange(query);
			},this));
	
			var currDef = _this.figure.getOpenDataDef();
			//console.log(currDef.resourceId + " " + currDef.query);
			showQuery(currDef.resourceId, currDef.query);
        }); 
	},	
	
	onQueryChange: function(query){
		var currDef = this.figure.getOpenDataDef();
		currDef.query = query;
		
		app.toolbar.saveButton.attr('disabled', false);	
		
		$("#queryModal").modal("toggle");
	},
	
	
    /**
     * @method
     * called by the framework if the pane has been resized. This is a good moment to adjust the layout if
     * required.
     * 
     */
    onResize: function()
    {
    },
    

    /**
     * @method
     * called by the framework before the pane will be removed from the DOM tree
     * 
     */
    onHide: function()
    {
    }
    



});