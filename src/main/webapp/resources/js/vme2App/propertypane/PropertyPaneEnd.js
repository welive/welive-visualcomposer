

vme2.propertypane.PropertyPaneEnd = Class.extend({

	init:function(stateFigure){
		this.figure = stateFigure;
	},

	injectPropertyView: function(domId)
	{
		var outputDef = this.figure;

		var templ= "<h4 class='text-info'>{{label.text}}</h4>" +			       
		"<p class='text-warning'>({{id}})</p>" +
		"<form><fieldset>"+
		"<legend>Output services</legend>"+			       				   	
		"{{#each userData.inputs}}"+	
		"      	<div class='control-group'>"+
		"          <span class='controls'>"+ 
		"			<h6 style=\"display: inline\">{{@index}}.&nbsp;</h6>"+
		//"			<button class=\"btn btn-mini{{#if @first}} disabled{{/if}} up\" data-inputidx=\"{{@index}}\"\"><i class=\"icon-arrow-up\"></i></button>"+
		//"			<button class=\"btn btn-mini{{#if @last}} disabled{{/if}} down\" data-inputidx=\"{{@index}}\"\"><i class=\"icon-arrow-down\"></i></button>"+			       
		"              <h6 style=\"display: inline\">&nbsp;{{this}}</h6>"+
		"          </span>"+
		"       </div>"+
		"{{/each}}"+
		"</fieldset></form>";			       		

		var compiled = Handlebars.compile(templ);
		var view   = $(compiled(outputDef));

		/*$('button.up',view).off('click');
		$('button.up',view).on('click', $.proxy(function(e){
			e.preventDefault();
			var idx= $(e.currentTarget).attr("data-inputidx");			
			this.moveUp(idx, domId);
		}, this));
		
		$('button.down',view).off('click');
		$('button.down',view).on('click', $.proxy(function(e){
			e.preventDefault();		
			var idx= $(e.currentTarget).attr("data-inputidx");
			this.moveDown(idx, domId);
		}, this));*/
		
		domId.html(view);
	},

	/**
	 * @method
	 * called by the framework if the pane has been resized. This is a good moment to adjust the layout if
	 * required.
	 * 
	 */
	onResize: function()
	{
	},


	/**
	 * @method
	 * called by the framework before the pane will be removed from the DOM tree
	 * 
	 */
	onHide: function()
	{
	},
    /*
    moveUp: function(sourceIdx, domId){
    	var arr = this.figure.getUserData().inputs;
    	    	
    	var element = arr[sourceIdx];
        arr.splice(sourceIdx, 1);
        arr.splice(sourceIdx-1, 0, element);
        
        this.injectPropertyView(domId);
        app.toolbar.saveButton.attr('disabled', false);
    	return false;
    },
    
    moveDown: function(sourceIdx, domId){
    	var arr = this.figure.getUserData().inputs;
    	
    	var element = arr[sourceIdx];
        arr.splice(sourceIdx, 1);
        arr.splice(sourceIdx+1, 0, element);
        
        this.injectPropertyView(domId);
    	app.toolbar.saveButton.attr('disabled', false);
        return false;
    }*/
});

