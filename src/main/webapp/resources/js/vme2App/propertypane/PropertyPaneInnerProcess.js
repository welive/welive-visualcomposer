vme2.propertypane.PropertyPaneInnerProcess = Class.extend({
	NAME : "vme2.propertypane.PropertyPaneInnerProcess",
	
	init:function(stateFigure){
	    // selected figure
	    this.figure = stateFigure;
	    
	    // all activities provided by the backend
	    this.activities = null;	    	    
	},
	
	injectPropertyView: function( domId)
	{
		var innerProcessDef = this.figure.getInnerProcessDef();
		
		var templ= "<h4 class='text-info'>{{name}}</h4>" +
			       "<p class='text-warning'>({{instanceId}})</p>" +
				   "<form><fieldset>"+
			       "<legend>Input parameters</legend>"+			       	
			       "{{#each parameters}}"+	
			       "      	<div class='control-group'>"+
			       "          <label class='control-label' for='{{name}}'> {{label}}"+
			       "              <a href='#' class='paramPop' rel='popover' data-toggle='popover' " +
			       "               data-content='{{doc}}' >" +
			       "            <i class='icon-question-sign'></i></a></label>"+
			       "          <div class='controls'>"+
			       "            {{#if options}}"+
			       "              <select class='paramInput' name='{{name}}'>"+
			       "              <option></option>" +
			       "              {{#each options}}" +
			       "                <option {{selected this ../value}} >{{this}}</option>" +
			       "              {{/each}}"+		
			       "              </select>"+
				   "            {{else}}"+
				   "              <input class='paramInput' name='{{name}}' type='text' " +
				   "               {{#if bind}}disabled='disabled'{{/if}}  value='{{value}}' />"+
				   "            {{/if}}"+
			       "          </div>"+
			       "       </div>"+
			       "{{/each}}"+
			       "</fieldset></form>";
			       
		
		
		var compiled = Handlebars.compile(templ);
		var view   = $(compiled(innerProcessDef));
        view.submit(function(e){
            return false;
        });
        
        domId.append(view);
        
        var _this = this;
        
        $(".paramInput").change(function(){
        	_this.onParamChange($(this));
        });
        /*
        $(".paramPop").popover({
        	    trigger: 'hover',
        	    animation: false,
        	    delay: { show: 100, hide: 1 }
        });     */           
	},	
	
	onParamChange: function(input){
		var val = input.val();
		var name = input.attr("name");
		
		var blackboxDef = this.figure.getBlackboxDef();
		$.each(blackboxDef.parameters, function(idx, param){
			if (param.name == name){
				param.value = val;
				app.toolbar.saveButton.attr('disabled', false);
		    	
				return false;
			}
		});
		
	},
	
    /**
     * @method
     * called by the framework if the pane has been resized. This is a good moment to adjust the layout if
     * required.
     * 
     */
    onResize: function()
    {
    },
    

    /**
     * @method
     * called by the framework before the pane will be removed from the DOM tree
     * 
     */
    onHide: function()
    {
    }
});
