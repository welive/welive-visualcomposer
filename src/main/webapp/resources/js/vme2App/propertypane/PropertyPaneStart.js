vme2.propertypane.PropertyPaneStart = Class.extend({
	
	init:function(stateFigure){
	    this.figure = stateFigure;	    
	},
	
	injectPropertyView: function( domId)
	{
	    
	    var templ= "<h4 class='text-info'>{{label.text}}</h4>" +			       
		"<p class='text-warning'>({{id}})</p>" +
		"<form id='inputDetails'><fieldset>"+
		"<legend>Input shape details</legend>"+			       				   		
		"      	<div class='control-group'>"+
		"			<label class='control-label'>Label: </label>"+		
		"           <span class='controls'>"+		
		"				<input type=\"text\" name=\"label\" value='{{userData.label}}'>"+
		"           </span>"+
		"			<label class='control-label'>Input Type:</label>"+		
		"           <span class='controls'>"+
		"				<select name='inputType' id='inputType'>"+
		"					<option value='text' {{#ifeq userData.inputType 'text'}}selected{{/ifeq}}>Text</option>"+ 
		"					<option value='combo' {{#ifeq userData.inputType 'combo'}}selected{{/ifeq}}>Combo Box</option>"+
		"				</select>"+
		"           </span>"+		
					
		"				<table class=\"table table-condensed {{#ifneq userData.inputType 'combo'}}hidden{{/ifneq}}\" id=\"comboOptionsTable\">"+
		"					<caption>Combobox options definition<caption>"+
		"					<thead>"+
	    "						<tr>"+
	    "							<th>Value</th>"+
	    "							<th>Text</th>"+
	    "							<th class=\"center\"><button class=\"btn btn-mini addoption\"><i class=\"icon-plus\"></i></button></th>"+
	    "						</tr>"+
	    "					</thead>"+
	    "					<tbody>"+
	    "					{{#each userData.combooptions}}"+
	    "						<tr>"+
	    "							<td><input type=\"text\" name=\"combooptions[{{@index}}][value]\" value='{{this.value}}' class=\"span1\"></td>"+
	    "							<td><input type=\"text\" name=\"combooptions[{{@index}}][text]\" value='{{this.text}}' class=\"span1\"></td>"+
	    "							<td/>"+
	    "						</tr>"+	    
	    "					{{/each}}"+
	    "					</tbody>"+
	    "				</table>"+	
		
		"				<label class='control-label'>Default value: </label>"+		
		"           	<div class='controls'>"+		
		"					<input type=\"text\" name=\"defaultValue\" value='{{userData.defaultValue}}' class=\"span1\">"+
		"           	</div>"+					
		"			<button class=\"btn save\"\">Save</button>"+		
		"       </div>"+
		"</fieldset></form>";
	    
		var compiled = Handlebars.compile(templ);
		var view   = $(compiled(this.figure));
	    
		$('#inputType',view).off('change');
		$('#inputType',view).on('change', $.proxy(function(e){
			e.preventDefault();
			
			var elem = $(e.currentTarget).val();
			
			if(elem=='text'){
				$('#comboOptionsTable').addClass("hidden");
			}else{
				$('#comboOptionsTable').removeClass("hidden");
			}
		}, this));
		
		$('button.addoption',view).off('click');
		$('button.addoption',view).on('click', $.proxy(function(e){
			e.preventDefault();
			
			var table = $(e.currentTarget).closest('table');
		    
			var rowCount = $("tbody > tr", table).length; 
			
			var newTableRow = "<tr>"+
		    "	<td><input type=\"text\" name=\"combooptions["+rowCount+"][value]\" class=\"span1\"></td>"+
		    "	<td><input type=\"text\" name=\"combooptions["+rowCount+"][text]\" class=\"span1\"></td>"+
		    "	<td/>"+		    
		    "</tr>";			

			$('tbody', table).append(newTableRow);
			
		}, this));
		
		$('button.save',view).off('click');
		$('button.save',view).on('click', $.proxy(function(e){
			e.preventDefault();
			
	    	var formFields = $("#inputDetails").serializeJSON();
	    	
	    	this.figure.setUserData(formFields);
	    	
	    	app.toolbar.saveButton.attr('disabled', false);
		}, this));
		
	    domId.append(view);
	},

	/**
	 * @method
	 * called by the framework if the pane has been resized. This is a good moment to adjust the layout if
	 * required.
	 * 
	 */
    onResize: function()
    {
    },
    

    /**
     * @method
     * called by the framework before the pane will be removed from the DOM tree
     * 
     */
    onHide: function()
    {
    	//var formFields = $("#inputDetails").serializeFullArray();
    	
    	//this.figure.setUserData(formFields);
    	    	
    	/*var parameterMap = [];
    	
    	for ( var i = 0; i < formFields.length; i++) {
    		if(formFields[i].value != null && formFields[i].value!=''){
    			userdata[formFields[i].name] = formFields[i].value;
    		}
    	}*/
    },
    
    inputTypeSwitch: function(){
    	
    },
    _getInputDetailsMap : function() {
		var formFields = $("#inputDetails").serializeArray();

		var parameterMap = [];
		for ( var i = 0; i < formFields.length; i++) {
				parameterMap.push({
					targetInput : formFields[i].name,
					sourceOutput : formFields[i].value
				});
		}
		
		return parameterMap;
	}
});

