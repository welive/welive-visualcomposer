GuiWizard = Class.extend({
	NAME : "vme2.GuiWizard",
	baseURL : '/visualcomposer',
	mashupInputTemplate : '' +
	   	'{{#each inputParams}}' +
	   	'	<div class="input-field col s12" {{#if isConstant}}style="display:none"{{/if}}>' +
	   	'		<input id="{{inputName}}" name="{{inputName}}" type="text" value="{{defaultValue}}" >' +
	   	'		<label for="{{inputName}}" {{#if defaultValue}}class="active"{{/if}}>{{label}}</label>' +
	   	'	</div>' +
	   	'{{/each}}',
	   	
	// constructor
	init: function(userLevel) {		
		this.tabs = $(".tabs");
		this.dataPaths = [];
		
		this.currentStep = 1;
		
		this.userLevel = userLevel;
		
		$('#wizardPrevButton').hide();
		$('#wizardNextButton').hide();
		$('#wizardDoneButton').hide();
	},
	
	selectTemplate: function(templateId) {
		this.selectedTemplateId = templateId;
		
		this.currentStep = 2;
		
		$('#wizard_1-header').addClass('disabled');
		$('#wizard_2-header').removeClass('disabled');
		
		$(this.tabs).tabs();
		$(this.tabs).tabs('select_tab', 'wizard_2');
		
		if(templateId == 'table')
			$(".selectedTemplate").html(Message['projects.wizard.templates.table.title']);
		else if(templateId == 'map')
			$(".selectedTemplate").html(Message['projects.wizard.templates.map.title']);
		else if(templateId == 'tablemap')
			$(".selectedTemplate").html(Message['projects.wizard.templates.tablemap.title']);
		else if(templateId == 'blank')
			$(".selectedTemplate").html(Message['projects.wizard.templates.form.title']);
		else if(templateId == 'fromscretch')
			$(".selectedTemplate").html(Message['projects.wizard.templates.blank.title']);
		
		$('#wizardPrevButton').show();
		$('#wizardNextButton').show();
	},
	
	nextStep: function() {		
		
		var beforeResult = true;
		
		switch(this.currentStep)
		{
			case 2:
				beforeResult = this._beforeStepThree();
				break;
			case 3:
				beforeResult = this._beforeStepFour();
				break;
			case 4:
				beforeResult = this._beforeStepFive();
				break;
			default: break;
		}		
		
		if(!beforeResult) return;
		
		$('#wizard_' + this.currentStep + '-header').addClass('disabled');
		$('#wizard_' + (this.currentStep + 1) + '-header').removeClass('disabled');
		
		this.currentStep = this.currentStep + 1;
		
		$(this.tabs).tabs();
		$(this.tabs).tabs('select_tab', 'wizard_' + this.currentStep);
	},
	
	prevStep: function() {	
		
		switch(this.currentStep)
		{
			case 2:
				// TODO alert: all your changes will be discarded
				$('#projectInfoForm').find('#name').val('');
				$('#projectInfoForm').find('#title').val('');
				$('#projectInfoForm').find('#description').val('');
				$('#projectInfoForm').find('#workgroup_id').val('');
				
				$('#wizardPrevButton').hide();
				$('#wizardNextButton').hide();
				
				Materialize.updateTextFields();
				$('select').material_select();
				break;
			case 3:
				$('#addmashups').select2('val', '');
				$('#step32').hide();
				break;
			case 4:
				
				break;
			default: break;
		}
		
		$('#wizard_' + this.currentStep + '-header').addClass('disabled');
		$('#wizard_' + (this.currentStep - 1) + '-header').removeClass('disabled');
		
		this.currentStep = this.currentStep - 1;
		
		$(this.tabs).tabs();
		$(this.tabs).tabs('select_tab', 'wizard_' + this.currentStep);
	},
	
	_beforeStepThree: function() {
		
		if($('#projectInfoForm').find('#name').val().trim() == '')
		{
			var toastContent = $('<span class="toastError">' + Message['projects.messages.enter_project_name'] + '</span>');
			Materialize.toast(toastContent, 2000);
			$(".toastError").parent().addClass("toast-error");
			
			return false;
		}
		
		if($('#projectInfoForm').find('#title').val().trim() == '')
		{
			var toastContent = $('<span class="toastError">' + Message['projects.messages.enter_project_title'] + '</span>');
			Materialize.toast(toastContent, 2000);
			$(".toastError").parent().addClass("toast-error");
			
			return false;
		}
		
		if($('#projectInfoForm').find('#description').val().trim() == '')
		{
			var toastContent = $('<span class="toastError">' + Message['projects.messages.enter_project_description'] + '</span>');
			Materialize.toast(toastContent, 2000);
			$(".toastError").parent().addClass("toast-error");
			
			return false;
		}
		
		if($("#workgroup_id option:selected").val() == "")
		{
			var toastContent = $('<span class="toastError">' + Message['projects.messages.select_workgroup'] + '</span>');
			Materialize.toast(toastContent, 2000);
			$(".toastError").parent().addClass("toast-error");
			
			return false;
		}
		
		this.selectedWorkgroupId = $("#workgroup_id option:selected").val();
		
		if(this.userLevel == 1 || this.selectedTemplateId == 'fromscretch')
		{
			this.currentStep = 4;			
			
			return this._beforeStepFive();
		}
		
		return true;
	},
	
	step32: function() {
				
		this.selectedMashupId = $('#addmashups').val();
		
		if(this.selectedTemplateId == 'blank' || this.selectedTemplateId == 'fromscretch')
		{
			this.currentStep = 4;			
			
			return;
		}		
		
		this._fetchMashupData($.proxy(function(data) {
			this.mashupData = data;
			
			// build invocation form
			var template = Handlebars.compile(this.mashupInputTemplate);
			var content = template(this.mashupData);	
			
			content += '<div class="input-field col s12"><a class="waves-effect waves-light btn right" onclick="javascript:guiWizard.invokeWorkflow()">' + Message['general.actions.submit'] + '</a></div>';
			
			$('#invokeMashupForm').html(content);
			
			$(".selectDataContainer").hide();
			
			$("#step32").show();	
			
		}, this));
	},
	
	_beforeStepFour: function() {
		
		if($('#addmashups').val() == '')
		{
			var toastContent = $('<span class="toastError">' + Message['projects.messages.select_mashup'] + '</span>');
			Materialize.toast(toastContent, 2000);
			$(".toastError").parent().addClass("toast-error");

			return false;
		}
		
		// jump to finish
		if(this.selectedTemplateId == 'blank' || this.selectedTemplateId == 'fromscretch')
		{
			this.currentStep = 4;			
			
			return this._beforeStepFive();
		}	
		
		$('.confContainer').hide();

		var selectedItem;
		var selectedItemDatatype;
		
		if(this.selectedTemplateId == 'table' || this.selectedTemplateId == 'tablemap')
		{
			
			selectedItem = $("#tabletree").jstree('get_selected');
			
			if(selectedItem.length == 0)
			{
				var toastContent = $('<span class="toastError">Please select a path for the table</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
				
				return false;
			}						
			
			selectedItemDatatype = $(selectedItem).data('datatype');
			
			// allow binding with numerical or boolean values
			if(selectedItemDatatype == undefined)
				selectedItemDatatype = "text";
			
			if(selectedItemDatatype != "array")
			{			
				var toastContent = $('<span class="toastError">Selected node is not compatible with expected type "array"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
				
				return false;
			}
			
			var path = $(selectedItem).data('path');
			
			if(path == undefined)
				path = $(selectedItem).find("a").get(0).innerText.split(":")[0].trim();
			
			// IVAN 
			// escape di -
			/*
			if(path != undefined && path.indexOf("-") > -1)
			{
				var end = path.indexOf("[");
				
				path = "$data['" + path.substring(0, end) + "']" + path.substring(end, path.length);
			}*/
			
			console.log('path', path);
			
			// get keys from array element
	
			var treeJSON = $("#tabletree").jstree('get_json');								
			var treeChildren = treeJSON[0].children;
			var fields = treeChildren[0].children;
			
			var content = '<form action="#" id="checkboxSortable" class="row">';
			
			for(var i = 0; i < fields.length; i++)
			{
				if(fields[i].metadata.datatype == "array")
					continue;
				
				if(fields[i].children == undefined)
				{
					content += ''
						+ 	'<div class="col">'
						+ 	'<div class="input-field row" style="padding-left: 10px">'
						+ 	'<input type="checkbox" id="checkbox_' + i + '" checked="checked" value="' + fields[i].data.title.split(":")[0].trim() + '" />'
						+ 	'<label for="checkbox_' + i + '"> Select</label>'
						+ 	'</div>'
						+	'<br><br><br>'
						+ 	'<div class="input-field col s12">'
						+		'<input type="text" id="colHead_' + i + '" value="' + fields[i].data.title.split(":")[0].trim() + '" />'
						+ 		'<label for="colHead_' + i + '">Column header for "' + fields[i].data.title.split(":")[0].trim() + '"</label>'
						+ 	'</div>'	
						+ 	'</div>';
				}
				else
				{
					var parentPath = fields[i].data.title.split(":")[0].trim();
					
					var subFields = fields[i].children;
					
					for(var j = 0; j < subFields.length; j++)
					{
						content += ''
							+ 	'<div class="col">'
							+ 	'<div class="input-field row" style="padding-left: 10px">'
							+ 	'<input type="checkbox" id="checkbox_' + i + "_" + j + '" checked="checked" value="' + parentPath + '.' + subFields[j].data.title.split(":")[0].trim() + '" />'
							+ 	'<label for="checkbox_' + i + "_" + j + '"> Select</label>'
							+ 	'</div>'
							+	'<br><br><br>'
							+ 	'<div class="input-field col s12">'
							+		'<input type="text" id="colHead_' + i + "_" + j + '" value="' + parentPath + ' ' + subFields[j].data.title.split(":")[0].trim() + '" />'
							+ 		'<label for="colHead_' + i + "_" + j + '">Column header for "' + parentPath + ' > ' + subFields[j].data.title.split(":")[0].trim() + '"</label>'
							+ 	'</div>'	
							+ 	'</div>';
					}
				}
			}
			
			content += '</form>';
									
			$("#confTable_1").html(content);
			$("#checkboxSortable").sortable();
		    $("#checkboxSortable").disableSelection();
		    
		    Materialize.updateTextFields();
		    
		    $('#confTableContainer').show();
		    
		    this.dataPaths['table'] = {'path': path};
		}
		
		// manage gmap case
		if(this.selectedTemplateId == 'map' || this.selectedTemplateId == 'tablemap')
		{
			selectedItem = $("#maptree").jstree('get_selected');
			
			if(selectedItem.length == 0)
			{
				var toastContent = $('<span class="toastError">Please select a path for the map.</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
				
				return false;
			}
			
			selectedItemDatatype = $(selectedItem).data('datatype');
			
			// allow binding with numerical or boolean values
			if(selectedItemDatatype == undefined)
				selectedItemDatatype = "text";
			
			var path = $(selectedItem).data('path');
			
			if(path == undefined)
				path = $(selectedItem).find("a").get(0).innerText.split(":")[0].trim();
			
			// first check if selected path contains lat and lng keys
			
			var lat = false;
			var lng = false;
			var position = false;
			
			try
			{
				var treeJSON = $("#maptree").jstree('get_json');
				
				var treeChildren = treeJSON[0].children;					
				
				for(var i = 0; i < treeChildren.length; i++)
				{
					if(treeChildren[i].data.title.split(":")[0].trim() == "lat" || treeChildren[i].data.title.split(":")[0].trim() == "latitude")
						lat = true;
					
					if(treeChildren[i].data.title.split(":")[0].trim() == "lng" || treeChildren[i].data.title.split(":")[0].trim() == "longitude")
						lng = true;
					
					if(treeChildren[i].data.title.split(":")[0].trim() == "position")
						position = true;
					
					if((lat && lng) || position)
						break;
				}
			}
			catch(ex)
			{
				
			}
			
			if(!((lat && lng) || position))
			{
				var toastContent = $('<span class="toastError">Map data: Selected path doesn\'t contain lat and lng or position.</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
				
				return false;
			}
			
			var coordspath = ""; 
			
			//var coordspathIndex = path.lastIndexOf("()") + 5; // IVAN: BUG!!!! INDEX OF ARRAY CAN HAVE LENGTH > 1 !!!!!!!!!!!!!!!!!
						
			var coordspathIndex = path.indexOf("]", path.lastIndexOf("()")) + 1;			
			
			var keysArray = eval(path.substring(coordspathIndex));									
			
			if(keysArray)
			{
				coordspath += keysArray[0];
				
				for(var i = 1; i < keysArray.length; i++)
				{
					coordspath += keysArray[i];
				}				
			}
			
			path = path.substring(0, path.lastIndexOf("()") + 2);
			
			//console.log("coordspath", coordspath);
			//console.log("path", path);
			
			// IVAN 
			// escape di -
			/*
			if(path != undefined && path.indexOf("-") > -1)
			{
				var end = path.indexOf("[");
				
				path = "$data['" + path.substring(0, end) + "']" + path.substring(end, path.length);
			}
			*/
			
			this.dataPaths['map'] = {'path': path, 'coordpath': coordspath};
			
			// get element fields for next step
			
			var mapInfoWindowFields = [];
			
			if(parentNodeJson != undefined)
			{
				var fields;
				
				if(parentNodeJson.metadata.datatype != "array")
				{
					fields = parentNodeJson.children;
				}
				else
				{
					fields = parentNodeJson.children[0].children;
				}
				
				for(var i = 0; i < fields.length; i++)
				{
					if(fields[i].metadata.datatype == "array")
						continue;
					
					if(fields[i].children == undefined)
					{
						mapInfoWindowFields.push(fields[i].data.title.split(":")[0].trim());
					}
					else
					{
						var subFields = fields[i].children;
						var parentPath = fields[i].data.title.trim();
						
						for(var j = 0; j < subFields.length; j++)
						{
							mapInfoWindowFields.push(parentPath + "." + subFields[j].data.title.split(":")[0].trim());
						}
					}
				}
			}	
			
			var content = '<form action="#" class="row">';

			var options = "";
			var tmp;
			
			for(var i = 0; i < mapInfoWindowFields.length; i++)
			{
				tmp = mapInfoWindowFields[i].split(".");
				
				options += '<option value="' + mapInfoWindowFields[i] + '">'
					+ tmp[0] + (tmp.length == 2 ? ' > ' + tmp[1] : '') + '</option>';
			}
			
			content += '<div class="input-field col s12">'
				+ '<select id="mapInfoWindowPrimary">'
				+ '<option value="" disabled selected>Choose your option</option>'
				+ options
				+ '</select>'
				+ '<label>Primary field</label>'
				+ '</div>';
			
			content += '<div class="input-field col s12">'
				+ '<select id="mapInfoWindowSecondary">'
				+ '<option value="" disabled selected>Choose your option</option>'
				+ options
				+ '</select>'
				+ '<label>Secondary field</label>'
				+ '</div>';
			
			content += '</form>';
			
			$("#confTable_2").html(content);
			$('#confMapContainer').show();			
			
			$('select').material_select();
		}	
		
		// jump to finish
		/*
		if(this.selectedTemplateId == 'map')
		{
			this.currentStep = 4;			
			
			return this._beforeStepFive();
		}
		*/
		
		return true;
	},
	
	_beforeStepFive: function() {//console.log("finish");
		// collect data and create project
		
		var fields = [];
		
		if(this.selectedTemplateId == 'table' || this.selectedTemplateId == 'tablemap')
		{
			var selectedFields = $("#confTable_1").find("input[type='checkbox']:checked");
			
			for(var i = 0; i < selectedFields.length; i++)
			{
				var tmp = selectedFields[i].id.split('_');
				var index = tmp[1];
				var index2 = tmp.length == 3 ? ("_" + tmp[2]) : "";
				
				var field = {
					'name': selectedFields[i].value,
					'position': i,
					'header': $('#colHead_' + index + index2).val()
				};
				
				fields.push(field);
			}
		}
		
		var infoWindow = {};
		
		if(this.selectedTemplateId == 'map' || this.selectedTemplateId == 'tablemap')
		{
			infoWindow.primary = $("#mapInfoWindowPrimary").val();
			infoWindow.secondary = $("#mapInfoWindowSecondary").val();
		}
		
		var mockup = {				
			'templateId': this.selectedTemplateId,
			'projectInfo': {
				'name': $('#projectInfoForm').find('#name').val().trim(),
				'title': $('#projectInfoForm').find('#title').val().trim(),
				'description': $('#projectInfoForm').find('#description').val().trim(),
				'workgroupId': this.selectedWorkgroupId,
				'mashupId': this.selectedMashupId
			},
			'configuration': {
				'table': {
					'dataPath': this.dataPaths['table'],
					'selectedFields': fields
				},
				'map': {
					'dataPath': this.dataPaths['map'],
					'infoWindow': infoWindow
				}
			}
		};
		
		console.log('mockup', mockup);
		
		var baseURL = this.baseURL;
		
		$.ajaxSetup({
			async : false
		});
		$.ajax({
			url : this.baseURL + "/user/gui/add/fromwizard",
			type : 'POST',
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(mockup),
			success: function(projectId) {

				$('#wizardDoneButton').attr('href', baseURL + "/user/gui/" + projectId + "/editor");
				
				$('#wizardPrevButton').hide();
				$('#wizardNextButton').hide();
				
				$('#wizardDoneButton').show();
			}
		});
		$.ajaxSetup({
			async : true
		});
		
		return true;
	},
	
	// fetch mashup projects
	_fetchMashupProjects : function(callback){
		$.ajaxSetup({
			async : false
		});
		$.ajax({
			url : this.baseURL + "/user/gui/workgroup/" + this.selectedWorkgroupId + "/mashupslist.json",
			type : 'GET',
			success: callback
		});
		$.ajaxSetup({
			async : true
		});
	},	
	// fetch mashup inputs
	_fetchMashupData : function(callback){
		$.ajaxSetup({
			async : false
		});
		$.ajax({
			url : this.baseURL + "/user/project/" + this.selectedMashupId + "/getbindinginputparams.json",
			type : 'GET',
			success: callback
		});
		$.ajaxSetup({
			async : true
		});
	},
	// invoke mashup
	invokeWorkflow : function() {
		
		$("#spinner").show();
		var parametersString = $("#invokeMashupForm").serialize();

		$.ajax({
			url : this.baseURL + "/user/project/" + this.selectedMashupId + "/invokeemml.json",
			type : 'POST',
			data : parametersString,
			success : $.proxy(function(data) {
			
				if(data && data.error && data.error == '__MASHUP_EXECUTION_ERROR')
				{
					var executionErrorMessage = '';
					
					// manage error response from EMML
					if(data.emmlResponse)
					{
						if(data.emmlResponse.code == '__DIRECTINVOKE_ERROR')
						{
							executionErrorMessage += ''
								+ '<div class="row executionErrorMessage">'
								+ '<h5>Error ' + data.emmlResponse.responseCode + ' invoking external service <b style="background-color:white !important;color:red !important;font-family:monospace !important">' + data.emmlResponse.endpoint + '</b></h5>'
								+ '<div>' + data.emmlResponse.responseBody + '</div>'
								+ '</div>';
						} 
						else if(data.emmlResponse.code == '__MACRO_ERROR')
						{
							var macroName = Message['mashup.operators.' + data.emmlResponse.macroName + '.label']
							
							executionErrorMessage += ''
								+ '<div class="row executionErrorMessage">'
								+ '<h5>Error executing operator <b>' + macroName + '</b>: ' + data.emmlResponse.message + '</h5>' // TODO replace macroName with operator name
								+ '</div>';
						}// TODO complete cases
						else
						{
							executionErrorMessage += ''
								+ '<div class="row executionErrorMessage">'
								+ '<h5>' + data.emmlResponse.message + '</h5>'
								+ '</div>';
						}
					}
					else
					{
						executionErrorMessage = data.message;
						
						executionErrorMessage = executionErrorMessage.replace(new RegExp('\r?\n','g'), ' ');
						
						executionErrorMessage = ''
							+ '<div class="row executionErrorMessage">'
							+ executionErrorMessage
							+ '</div>';
					}			
					
					var error_content = ''
						+ '<div class="row">'
						+	'<h5 style="text-align:center;color:red;font-weight:bold;border-bottom:none">' + Message['mashup.messages.error_invoking_workflow'] + '</h5>'
						+ '</div>';
					
					error_content += executionErrorMessage;
					
					$('#mashupExecutionError').html(error_content);
					
					$("#spinner").hide();
					$('#mashupExecutionError').show();
				}
				else
				{
					this._buildTreePanel(data);
					
					$("#spinner").hide();

					if(this.selectedTemplateId == 'map' || this.selectedTemplateId == 'tablemap')
						$('#selectMapDataContainer').show();
					if(this.selectedTemplateId == 'table' || this.selectedTemplateId == 'tablemap')
						$('#selectTableDataContainer').show();	
				}							
				
			}, this)
		});
	},
	// build results tree
	_buildTreePanel : function(data) {
		
		if(this.selectedTemplateId == 'map' || this.selectedTemplateId == 'tablemap')
			$("#maptree").jstree({
				"json_data" : {
					data : data
				},
				"plugins" : [ "themes", "json_data", "ui", "search" ]
			})
			.bind("loaded.jstree", function (e, data) {
				$(".fa").removeClass("jstree-icon");
				
				$("#maptree").find(".fa").css("color", "#bdbdbd");
				
				$('#maptree').jstree('search', 'lat');
				$('#maptree').jstree('search', 'lng');
				$('#maptree').jstree('search', 'position');
				
				$("#maptree").jstree('open_all');
			})
			.bind("search.jstree", function (e, data) {
					
				for(var i = 0; i < data.rslt.nodes.length; i++)
				{
					$(data.rslt.nodes[i]).parent().parent().parent().children("a").first().find("ins").css("color", "#558b2f");
				}
			})
			.bind("select_node.jstree", function(event, data) {
					
				try
				{
					parentNodeJson = $("#maptree").jstree('get_json', data.inst._get_parent())[0];
				}
				catch(ex)
				{
					console.log(ex);
					parentNodeJson = undefined;
				}
			});	
		
		if(this.selectedTemplateId == 'table' || this.selectedTemplateId == 'tablemap')
			$("#tabletree").jstree({
				"json_data" : {
					data : data
				},
				"plugins" : [ "themes", "json_data", "ui" ]
			})
			.bind("loaded.jstree", function (e, data) {
				$(".fa").removeClass("jstree-icon");
				
				$("#tabletree").find(".fa").css("color", "#bdbdbd");
				
				$("#tabletree").find(".icon-th-list").css("color", "#558b2f");
				
				$("#tabletree").jstree('open_all');
			});	
	},
});