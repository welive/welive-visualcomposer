Palette = Class.extend({
	NAME : "vme2.Palette",

	baseURL : '/visualcomposer',

	_templateName : 'InputForm',
	
	init : function(mashupProjectId) {
		this.mashupProjectId = mashupProjectId;
		var items = {};
		this.items = items;

		$.ajaxSetup({
			async : false
		});

		// Recupera in maniera sincrona la lista dei json corrispondenti alla
		// definizione degli item in palette
		
		// IVAN for materialize version
		$.getJSON(this.baseURL+"/guieditor/palette_component/componentlist.json", $.proxy(function(data) {			
			// Crea ed inserisce in palette gli elementi
			$.each(data.categories, $.proxy(function(i, category) {
				
				if(!category.virtual)
				{
					var content = '<li>'
						+ '<div class="collapsible-header"><i class="material-icons">view_module</i>' + Message['mockup.palette.section.' + category.id] + '</div>'
						+ '<div class="collapsible-body" id="elm' +category.id + '">'
						+ '</li>';
					
					$('#customComponentContainer ul').append(content);	
				}
				
				$.each(category.items, $.proxy(function(i, jsonUrl) {
					try {
						var item = new PaletteItem("/guieditor/palette_component/metadata/" + jsonUrl, 'elm'+category.id);
						this.items[item.getId()] = item;
					} catch (e) {
						alert(e);
					}
				}, this));
				
				autoTranslate();
				
			}, this));

		}, this));

		//this.createInputForm();

		$.ajaxSetup({
			async : true
		});
	},

	getItems : function() {
		return this.items;
	},

	getItem : function(itemId) {
		return this.items[itemId];
	},

	// NON ANCORA UTILIZZATA
	handleJsIds : function() {
		this.handleModalIds();
		this.handleAccordionIds();
		this.handleCarouselIds();
		this.handleTabsIds()
	},
	// NON ANCORA UTILIZZATA
	handleAccordionIds : function() {
		var e = $(".demo #myAccordion");
		var t = this.randomNumber();
		var n = "panel-" + t;
		var r;
		e.attr("id", n);
		e.find(".panel").each(function(e, t) {
			r = "panel-element-" + randomNumber();
			$(t).find(".panel-title").each(function(e, t) {
				$(t).attr("data-parent", "#" + n);
				$(t).attr("href", "#" + r)
			});
			$(t).find(".panel-collapse").each(function(e, t) {
				$(t).attr("id", r)
			})
		})
	},
	// NON ANCORA UTILIZZATA
	handleCarouselIds : function() {
		var e = $(".demo #myCarousel");
		var t = this.randomNumber();
		var n = "carousel-" + t;
		e.attr("id", n);
		e.find(".carousel-indicators li").each(function(e, t) {
			$(t).attr("data-target", "#" + n)
		});
		e.find(".left").attr("href", "#" + n);
		e.find(".right").attr("href", "#" + n)
	},
	// NON ANCORA UTILIZZATA
	handleModalIds : function() {
		var e = $(".demo #myModalLink");
		var t = this.randomNumber();
		var n = "modal-container-" + t;
		var r = "modal-" + t;
		e.attr("id", r);
		e.attr("href", "#" + n);
		e.next().attr("id", n)
	},
	// NON ANCORA UTILIZZATA
	handleTabsIds : function() {
		var e = $(".demo #myTabs");
		var t = this.randomNumber();
		var n = "tabs-" + t;
		e.attr("id", n);
		e.find(".tab-pane").each(
				function(e, t) {
					var n = $(t).attr("id");
					var r = "panel-" + randomNumber();
					$(t).attr("id", r);
					$(t).parent().parent().find("a[href=#" + n + "]").attr("href",
							"#" + r)
				})
	},
	// NON ANCORA UTILIZZATA
	randomNumber : function() {
		return this.randomFromInterval(1, 1e6)
	},
	// NON ANCORA UTILIZZATA
	randomFromInterval : function(e, t) {
		return Math.floor(Math.random() * (t - e + 1) + e)
	},
	/*createInputForm : function() {
		if (this.mashupProjectId != undefined) {
			$.ajax({
				url : this.baseURL + "/user/project/" + this.mashupProjectId
						+ "/getinputshapes",
				type : 'GET',
				success : $.proxy(function(response) {
					
					var context = {
						"inputshapes" : response,
						"formactionurl": guieditor.getFullUrl()+"gadget/"+guieditor.getMashupProjectId()+"/emmlproxy.json"
					};
					
					//Prende il template per la generazione dell'item di palette
					guieditor.getTemplateManager().get(this._templateName, $.proxy(function(tmpl) {			
						var template = Handlebars.compile(tmpl);
						$('#elmMashup').append(template(context));
					}, this));																
				}, this)
			});
		}
	}*/
});
