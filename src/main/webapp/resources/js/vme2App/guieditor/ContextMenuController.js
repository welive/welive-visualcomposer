function showContextMenu(event)
{
	guieditor.selectedItem = event.target;
	
	$("#elContextMenu").css("left", event.clientX);
	$("#elContextMenu").css("top", event.clientY);
	
	if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-table')
		$(".elContextMenuTable").show();
	else if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-form')
		$(".elContextMenuForm").show();
	else if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-gmap')
		$(".elContextMenuGmap").show();
	else if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-inputtext'
			|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-textarea')
		$(".elContextMenuInput").show();
	else if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-label') // patch for checkboxes and radios
	{
		var prevEl = $(guieditor.selectedItem).prev();
		
		if($(prevEl).attr('data-vme-itemid') == 'vme-inputcheckbox' || $(prevEl).attr('data-vme-itemid') == 'vme-radio')
		{
			guieditor.selectedItem = prevEl;
			$(".elContextMenuInput").show();
		}
	}
	else if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-h')
		$(".elContextMenuHeading").show();
	
	// patch for select
	if($(guieditor.selectedItem).attr("class") != undefined && $(guieditor.selectedItem).attr("class").indexOf("select-wrapper") >= 0) 
	{
		guieditor.selectedItem = $(guieditor.selectedItem).find("input");
		$(".elContextMenuInput").show();
	}
	
	// enable binding
	if(($(guieditor.selectedItem).attr("data-databind") != undefined && $(guieditor.selectedItem).attr("data-databind") != "")
			|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-table')
	{
		if(enableDatabind)
			$(".elContextMenuBindable").show();
		
		var databind = $(guieditor.selectedItem).attr("data-bind-" + $(guieditor.selectedItem).attr('data-databind'));
		
		if(databind!= undefined && databind != "")
			$(".elContextMenuBindRemove").show()
			
		if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-table')
		{
			databind = $(guieditor.selectedItem).find("tbody").attr("data-bind-" + $(guieditor.selectedItem).find("tbody").attr('data-databind'));
			
			if(databind!= undefined && databind != "")
				$(".elContextMenuBindRemove").show()
		}
	}
	
	// show features according to user level
	switch(guieditor.userLevel)
	{
		case 1:
			// hide developer/advanced features
			$(".level-advanced").hide();
			$(".level-developer").hide();
			
			if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-h'
				|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-p'
					|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-a'
						|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-span'
							|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-li'
								|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-label')
			{
				$(".elContextMenuEditContent").show();
			}
			break;
		case 2:
			// hide advanced features
			$(".level-advanced").hide();
			
			if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-h'
				|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-p'
					|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-a'
						|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-span'
							|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-li'
								|| $(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-label')
			{
				$(".elContextMenuEditContent").show();
			}
			break;
		case 3:
			if($(guieditor.selectedItem).attr('data-vme-itemid') != 'vme-inputtext')
				$(".elContextMenuEditContent").show();
			break;
		default:
			break;
	}
	
	$('.demo *').removeAttr('data-vme-boxshadow');
    $(event.target).attr('data-vme-boxshadow', 'boxshadow');
	
	$("#elContextMenu").show();
	$("#elContextMenuLayer").show();	
}

function contextMenuAction(key)
{
	$(".treeModalSelectedItemRequires").show();
	$(".treeModalSelectedItemMap_object").hide();
	$(".treeModalSelectedItemMap_array").hide();
	
	if(key == 'remove')
		guieditor.removeSelectedItem();
	else if(key == 'editHTML')
	{
		guieditor.editElementHTML();
		$('#editorModal').openModal();	
	}
	else if(key == 'editCSS')
	{
		guieditor.editElementStyle();
		$('#styleModal').openModal();	
	}
	else if(key == 'editProperties')
	{
		guieditor.editElementStyle();
		$('#propertiesModal').openModal();	
	}
	else if(key == 'setInput')
	{
		guieditor.fetchInputData(guieditor.selectedItem);
	}
	else if(key == 'bind')
	{
		// make the user choose between single and multiple markers
		if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-gmap')
		{
			$('#gmapMarkersModal').openModal();
		}
		else
		{
			guieditor.markerType = undefined;
			guieditor.fetchData(guieditor.selectedItem);
		}		
	}
	else if(key == 'removeBind')
	{
		if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-table')
		{
			$(guieditor.selectedItem).find("[data-databind]").each(function() {
				$(this).removeAttr("data-bind-" + $(this).attr('data-databind'));
			});
		}
		else
			$(guieditor.selectedItem).removeAttr("data-bind-" + $(guieditor.selectedItem).attr('data-databind'));
	}
	// vme-table
	else if(key == 'tableSelectBody')
	{
		$("tbody", guieditor.selectedItem).click();
	}
	else if(key == 'tableSelectHead')
	{
		$("thead", guieditor.selectedItem).click();
	}
	else if(key == 'tableModifyTable')
	{		
		tableModal_init(guieditor.selectedItem);
		
		$('#tableModal').openModal();
	}
	// vme-form
	else if(key == 'formSettings')
	{
		guieditor.editElementStyle();
		$('#formSettingsModal').openModal();
	}
	else if(key == 'formAddInput')
	{
		$('#formAddInputModal').openModal();
	}
	// vme-gmap
	else if(key == 'gmapSettings')
	{
		guieditor.editElementStyle();
		$('#gmapSettingsModal').openModal();
	}
	// vme-h
	else if(key == 'headingSettings')
	{
		var tagName = $(guieditor.selectedItem).prop("tagName").toLowerCase();
		
		$("#headingSettingsModalSelect").val(tagName);
		
		$('#headingSettingsModal').openModal();
	}	
	
	$("#elContextMenuLayer").hide();
	$("#elContextMenu").hide();
	$(".elContextMenuSpecial").hide();
}

function headingSettingsUpdate()
{
	var selectedHeading = $("#headingSettingsModalSelect").val();
	
	var currentText = $(guieditor.selectedItem).html();
	var currentId = $(guieditor.selectedItem).attr("id");
	var currentClass = $(guieditor.selectedItem).attr("class");
	var currentStyle = $(guieditor.selectedItem).attr("style");
	var currentBinding = $(guieditor.selectedItem).attr("data-bind-text");
	
	var newHeading = '<' + selectedHeading + ' ' 
		+ (currentId != undefined && currentId != "" ? 'id="' + currentId + '" ' : '')
		+ (currentClass != undefined && currentClass != "" ? 'class="' + currentClass + '" ' : '')
		+ (currentStyle != undefined && currentStyle != "" ? 'style="' + currentStyle + '" ' : '')
		+ (currentBinding != undefined && currentBinding != "" ? 'data-bind-text="' + currentBinding + '" ' : '')
		+ 'data-vme-dad="sortable" data-vme-itemid="vme-h" data-vme-type="block" data-onlydatabind="false" data-datatype="text" data-databind="text" data-databind-type="simple" data-bind-property="text" data-vme-sortable="true" data-vme-boxshadow="boxshadow">' + currentText + '</' + selectedHeading + '>';
	
	$(guieditor.selectedItem).replaceWith(newHeading);
	
	$('.demo *').on('contextmenu', function(e){
		showContextMenu(e);
	});
}

function chooseMarkerType()
{
	var selectedOption = $("#panelGmapMarkersContainer").find("input[name='markersType']:checked").val();
	
	if(selectedOption == undefined || selectedOption == "")
	{
		closeModal('gmapMarkersModal');
		return;
	}
	
	if(selectedOption == 'markersTypeSingle')
	{
		guieditor.markerType = "object";
	}
	else
	{
		guieditor.markerType = "array";
	}
	
	$(".treeModalSelectedItemRequires").hide();
	$(".treeModalSelectedItemMap_" + guieditor.markerType).show();
	
	guieditor.fetchData(guieditor.selectedItem);
}

function addInputToForm()
{
	var selectedOption = $("#panelFormAddInputContainer").find("input[name='inputType']:checked").val();
	
	if(selectedOption == undefined || selectedOption == "")
	{
		closeModal('formAddInputModal');
		return;
	}	
	
	var newInput = '<div data-vme-dad="sortable" data-vme-type="block" class="row" data-vme-sortable="true">'
		+ '<div data-vme-dad="sortable" data-vme-type="block" class="input-field col s12" data-vme-sortable="true">'
	
	if(selectedOption == 'inputTypeText')
	{
		newInput += '<input data-vme-itemid="vme-inputtext" type="text" placeholder="" id="input_id">';
		newInput += '<label data-vme-itemid="vme-label" for="input_id">Label</label>';
	}
	else if(selectedOption == 'inputTypeSelect')
	{
		newInput += '<select data-vme-itemid="vme-select" id="select_id" class="browser-default">'
			+ '<option value="" disabled selected>Choose your option</option>'
			+ '</select>';
		newInput += '<label data-vme-itemid="vme-label" for="select_id">Label</label>';
	}
	else if(selectedOption == 'inputTypeCheckbox')
	{
		newInput += '<input data-vme-itemid="vme-inputcheckbox" type="checkbox" id="checkbox_id">';
		newInput += '<label data-vme-itemid="vme-label" for="checkbox_id">Label</label>';
	}
	else if(selectedOption == 'inputTypeRadio')
	{
		newInput += '<input data-vme-itemid="vme-radio" class="with-gap" name="radio-group" type="radio" id="radio_id" />';
		newInput += '<label data-vme-itemid="vme-label" for="radio_id">Label</label>';
	}
	else if(selectedOption == 'inputTypeTextarea')
	{
		newInput += '<textarea data-vme-itemid="vme-textarea" id="textarea_id" class="materialize-textarea"></textarea>';
		newInput += '<label data-vme-itemid="vme-label" for="textarea_id">Label</label>';
	}
	
	newInput += '</div></div>';
	
	$(guieditor.selectedItem).append(newInput);
	
	if(selectedOption == 'inputTypeSelect')
		$('select').material_select();
	
	$("#panelFormAddInputContainer").find("input[type='radio']").attr('checked', false);
	$("#inputTypeText").attr('checked', true);
	closeModal('formAddInputModal');
}

$(document).ready(function(){
	$("#elContextMenuLayer").on('click contextmenu', function() {
		$("#elContextMenu").hide();
		$("#elContextMenuLayer").hide();
		$(".elContextMenuSpecial").hide();
	});
});