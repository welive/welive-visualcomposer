Panel = Class
		.extend({
			NAME : "vme2.Panel",

			
			baseURL : '/visualcomposer',
			
			// IVAN for materialize version
			
			_templateStyle : 'PanelStyle',
			_templateProperties : 'PanelProperties',
			_templateFormSettings : 'PanelFormSettings',
			_templateGmapSettings : 'PanelGmapSettings',

			init : function(element) {
				this.tag = element;

				// Istanzia un nuovo oggetto json
				var json = {};

				// Effettua la conversione del tag in json e lo inietta nel nodo
				// json element
				json.element = JsonML.fromHTML($(this.tag)[0]);
				
				//Abilita di default il pulsante per l'editing del contenuto del tag 
				json.editContent = true;
				
				//Rimuovere un precedente menu contestuale
				//$.contextMenu('destroy');
				
				if (JsonML.isElement(json.element)) {
					// Prende l'identificativo di palette dell'elemento
					var itemId = $(this.tag).data("vme-itemid");

					if (itemId) {
						// Prende il corrispondente oggetto dalla palette
						var item = guieditor.getPalette().getItem(itemId);

						this.paletteItem = item;
						
						var mashupPrjId = guieditor.getMashupProjectId();
						if(mashupPrjId!==undefined){
							json.bindable = true
						}else{
							json.bindable = false;
						}											
						
						if (item !== undefined && item.getEditContent() !== undefined) {
							json.editContent = item.getEditContent();
						}
						
						if (item !== undefined && item.getProperties() !== undefined) {
							// Prende i metadati delle proprietà custom dell'elemento
							var property = item.getProperties();

							// Le inietta nel nodo custompropery del json
							json.customproperty = property;
						}

						if (item !== undefined && item.getEditActions() !== undefined) {
							// Prende i metadati delle azioni di edit dell'elemento
							var actions = item.getEditActions();																				
							
							// Le inietta nel nodo actions del json
							json.actions = actions;
						}
						// IVAN
						if (item !== undefined && item.getId() == "vme-form") 
						{
							json.mashups = guieditor.mashups;								
							json.landingpages = this.getHtmlPageList();
							
						}
						// IVAN
						if (item !== undefined && item.getId() == "vme-a")
						{							
							json.pages = this.getHtmlPageList();
						}
						// IVAN
						if (item !== undefined && item.getId() == "vme-img")
						{							
							json.images = this.getImages();
						}
					}
				
					this.json = json;
					
					// IVAN for materialize version
					// Cattura il template relativo al pannello dello stile e delle proprietà
					guieditor.getTemplateManager().get(this._templateStyle, $.proxy(this._buildPanel, this));
					guieditor.getTemplateManager().get(this._templateProperties, $.proxy(this._buildPanel, this));
					guieditor.getTemplateManager().get(this._templateFormSettings, $.proxy(this._buildPanel, this));
					guieditor.getTemplateManager().get(this._templateGmapSettings, $.proxy(this._buildPanel, this));
				} else {
					throw "It's not possible to create panel for selected tag";
				}
			},
			getTag : function(){
				return this.tag;
			},
			// IVAN for materialize version
			_buildPanel : function(tmpl, type) {
				
				// Costruisce l'html risultate dalla compilazione del template				
				this.template = Handlebars.compile(tmpl);
				var element = this.template(this.json);//console.log('json', this.json);
				
				var formName = "";
				
				if(type == this._templateStyle)
				{
					$("#panelStyleContainer").html(element);
					formName = "styleForm";
				}
				else if(type == this._templateProperties)
				{
					$("#panelPropertiesContainer").html(element);
					formName = "propertiesForm";
				}
				else if(type == this._templateFormSettings)
				{
					$("#panelFormSettingsContainer").html(element);
					formName = "formSettingsForm";
				}
				else if(type == this._templateGmapSettings)
				{
					$("#panelGmapSettingsContainer").html(element);
					formName = "gmapSettingsForm";
				}
				
				$('ul.tabs').tabs();
				Materialize.updateTextFields();
				$('select').material_select();
				
				autoTranslate();

				// Appende ai campi del pannello delle proprietà l'evento onchange
				$("input,textarea,select", "#" + formName).each(
						$.proxy(function(i, e) {
							$(e).change($.proxy(this.onChangeFieldEvent, this));
						}, this));			

				// Effettua il bind del click del pulsante di edit content
				$('#button-edit-modal').off("click");
				$('#button-edit-modal').click($.proxy(function(e) {
					// Prende l'html del tag relativo al pannello delle proprietà
					// corrente
					var html = $(this.tag).html();
					
					// Effettua un trim ed un beatify del codice html
					var prettyHtml = vkbeautify.xml(html.trim());

					// Inietta l'html nell'editor Ace
					guieditor.getHtmlEditor().session.setValue(prettyHtml);
				}, this)).toggleClass("hide");

				// Effettua il bind del click del pulsante di save content che si
				// trova nel dialog di edit
				$('#savecontent').off("click");
				$("#savecontent").click($.proxy(function(e) {
					e.preventDefault();
					// Prende il codice HTML dall'Ace editor
					var html = guieditor.getHtmlEditor().session.getValue();

					// Lo inietta nel tag relativo al pannello delle proprietà
					// corrente
					$(this.tag).html(html);

					// Pulisce l'editor Ace
					guieditor.getHtmlEditor().session.setValue(" ");
				}, this));

				// Effettua il binding dei pulsanti di edit del pannello delle
				// proprietà
				if (this.paletteItem !== undefined) {
					this.paletteItem.bindEditActions(this.tag, this);
				}
			},
			onChangeFieldEvent : function(e) {
				// Prende il valore corrente del campo
				var attrValue = $(e.target).val();
				
				var inputType = $(e.target).attr("type");								
				if(inputType==="checkbox"){
					attrValue = $(e.target).prop('checked');
				}
				if($(e.target).is("select")){
					//attrValue = $(e.target).val();
					var optionSelectedIndex = $(e.target)[0].selectedIndex;
					var optionSelected=$(e.target).find('option:eq('+optionSelectedIndex+')');
					
					optionSelected.attr("selected","selected");
					$(e.target).attr("value",optionSelected.val());
					attrValue = $(e.target).val();
					console.log(attrValue);
				}
				
				if (attrValue !== undefined) {
					// Prende il nome del campo
					var attrName = $(e.target).attr("name");

					// I nomi dei campi sono del tipo "attr.id"
					attrName = attrName.substring(attrName.indexOf("attr.")
							+ "attr.".length);

					var name = attrName;
					var value = attrValue;
					if (attrName.indexOf("style.") !== -1) {
						// Se il nome dell'attributo contiene "style" significa che
						// dobbiamo modificare
						// le proprietà css dell'elemento
						name = attrName.substring(attrName.indexOf(".") + 1);
						value = attrValue;

						$(this.tag).css(name, value);

					} else {
						// Altrimenti si modifica la proprietà relativa
						if (name.indexOf('data-bind-') !== -1) {
							
							this.bindingAttributeParser(e, name, value);
						
						} else {
							if(name==='text'){
								$(this.tag).text(value);								
							}else{
								
								$(this.tag).attr(name, value);
							}
						}
					}
				}
			},
			/**
			 * Funzione che converte un colore dal formato rgb al corrispondente
			 * formato hex
			 */
			_rgb2hex : function(rgb) {
				rgb = rgb
						.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
				return (rgb && rgb.length === 4) ? "#"
						+ ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2)
						+ ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2)
						+ ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
			}, 
			bindingAttributeParser : function(e, name, value){

				// Prende la proprietà relativa al campo del pannello che
				// contiene il valore di bind
				//var propertyName = $(e.target).data("binding-property");

				var propertyName = $(e.target).data("binding-property");
				
				// Prende i metadati relativi alla proprietà per recuperare
				// il tipo di databind associato
				var property = this.paletteItem.getProperty(propertyName);
				//var databindProperty = property.databind;
				var databindType = property['databind-type'];

				// Prende il valore esistente del campo data-bind del tag
				// in canvas
				var oldValue = $(this.tag).attr(name);

				if (value !== undefined && value !== null && value !== '') {
					// E' stato inserito un valore nel campo
					
					if(databindType=="complex"){
						if (oldValue !== undefined && oldValue !== '') {
							if (oldValue.indexOf(propertyName) != -1) {
								//oldValue contiene già la proprietà quindi si procederà per SOSTITUZIONE
								var regexpBProp = new RegExp(propertyName + ":([^\{\}:,]*)");
								
								value = oldValue.replace(regexpBProp, propertyName + ':' + value);																												
							
							} else {
								//oldValue contiene già binding di tipo attr quindi APPENDE AD ATTR ESISTENTE
								var regexpBProp = new RegExp("(\{)([^\{\}]*)(\})");
								
								value = oldValue.replace(regexpBProp, "$1$2,"+propertyName + ':' + value+"$3");
							}
						} else {
							//oldValue non esiste quindi si procede all'inserimento di un nuovo attributo
							value = "{"+propertyName + ':' + value+"}";
						}
					} 
					
					$(this.tag).attr(name, value);					
					
				} else {
					//Non è stato inserito un valore nel campo o il valore è vuoto
					
					if(databindType=="complex"){
   					// Deve controllare se oldValue contiene la proprietà e nel caso eliminarla
   					if (oldValue.indexOf(propertyName) != -1) {	
   						//oldValue contiene la proprità che va eliminata
   						   						
							var regexpBProp = new RegExp(",?"+propertyName + ":([^\{\}:,]*)"+",?");
   															
							value = oldValue.replace(regexpBProp, "");
   							
							//Nel caso la proprietà attr non contenesse alcuna binding property verrà eliminata
							value = value.replace(/,?\{\},?/g, "");
   						
   						$(this.tag).attr(name, value);									
   					}
   				} else{
   					$(this.tag).removeAttr(name);
   				}															
				}				
			},
			// IVAN
			getHtmlPageList: function()
			{
				var pages = [];
				
				$.ajax({
					url : this.baseURL + "/user/gui/" + guieditor.projectId + "/gethtmllist.json",
					type : 'GET',
					async: false,
					success : $.proxy(function(response)
					{
						for(var i = 0; i < response.length; i++)
						{
							if(response[i].isdefault == false)
								pages.push({"filename" : response[i].filename.replace(".html.working","")});
						}
						
					}, this)
				});
				
				return pages;
			},
			getImages: function()
			{
				var images = [];
				
				$.ajax({
					url : this.baseURL + "/user/gui/" + guieditor.projectId + "/getimagelist.json",
					type : 'GET',
					async: false,
					success : $.proxy(function(response)
					{console.log(response);
						for(var i = 0; i < response.length; i++)
						{
							images.push({"filename" : response[i].filename});
						}
						
					}, this)
				});
				
				return images;
			}
		});