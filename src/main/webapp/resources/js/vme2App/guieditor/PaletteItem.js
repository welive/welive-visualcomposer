PaletteItem = Class.extend({
	NAME : "vme2.PaletteItem",

	baseURL : '/visualcomposer',

	_templateName : 'PaletteItem',
	
	init : function(jsonUrl, categoryId) {
		this.jsonMetadataUrl = jsonUrl;				
		
		//Cattura i metadati dell'item di palette
		$.getJSON(this.baseURL+jsonUrl, $.proxy(function(data) {
			this.metadata = data;
		}, this)).error(function(d, textStatus, error) {			
			throw("Problem occurred while importing json definition for:\n- "+ jsonUrl +"\n  ERROR: " + error.message);
		});
		
		if(!this.metadata.virtual){
   		//Prende il template per la generazione dell'item di palette
   		guieditor.getTemplateManager().get(this._templateName, $.proxy(function(tmpl) {		
   			this.template = Handlebars.compile(tmpl);
   		}, this));					
   		
   		//Costruisce l'item
   		var element = this._buildItem();
   		
   		// IVAN temporaneo: da risolvere
   		//if(this.metadata.id != 'vme-ul')
   			$("#" + categoryId).append(element);
		}
	},

	_buildItem : function() {			
		return this.template(this.metadata);
	},	
	
	getId : function(){
		return this.metadata.id;
	},
	
	getTag : function(){
		return this.metadata.tag;
	},
	
	getProperties : function(){
		return this.metadata.property;
	},
	
	getProperty : function(name){
		for(var i=0; i<this.metadata.property.length; i++){
			if(this.metadata.property[i].name===name){
				return this.metadata.property[i];
			}
		}
	},	
	getEditContent : function(){
		return this.metadata.editContent;
	},	
	
	getEditActions : function(element, newPanel){
		return this.metadata.events !== undefined && this.metadata.events.edit !==undefined ? this.metadata.events.edit.actions : undefined;
	},	
	/**
	 * Effettua il bind tra i pulsanti del pannello delle proprietà e le rispettive funzioni js
	 */
	bindEditActions : function(element, newPanel){												
		if(this.metadata.events!==undefined && this.metadata.events.edit!==undefined){
			//Prende i metadati relativi alle azioni di tipo edit
			var editactions = this.metadata.events.edit.actions;
			
			//Prende il nome del modulo requirejs 
			var module = this.metadata.events.edit.module;
			
			require(["app/"+module], function(a){
				//Per ogni azione di edit cattura il pulsante corrispondente e gli associa il metodo js
				$.each(editactions, $.proxy(function(i, actionDetails){
					$("#"+actionDetails.buttonId).on("click", $.proxy(function(){						
						if (actionDetails.method in a) {					        
					        a[actionDetails.method](element, newPanel);
					    }
					}, this));
				}, this));
				
			});			
		}
	},
	setOnCanvasAction : function(newItem){
		if(this.metadata.events!==undefined && this.metadata.events.oncanvas!==undefined){
			//Prende i metadati relativi alle azioni di tipo oncanvas
			var oncanvasactions = this.metadata.events.oncanvas.actions;
			
			//Prende il nome del modulo requirejs
			var module = this.metadata.events.oncanvas.module;
			
			require(["app/"+module], function(a){
				//Per ogni azione oncanvas esegue il metodo js corrispondente
				$.each(oncanvasactions, $.proxy(function(i, actionDetails){									
					if(actionDetails.method in a){
						a[actionDetails.method](newItem);
					}
				}, this));
			});							
		}
	}	
});
