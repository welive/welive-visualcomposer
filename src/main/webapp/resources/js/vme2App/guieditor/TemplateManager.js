TemplateManager = Class.extend({
	templates : {},

	baseURL : '/visualcomposer',
	
	get : function(id, callback) {
		var template = this.templates[id];

		if (template) {
			callback(template, id);

		} else {

			var that = this;
			$.get(this.baseURL + "/resources/js/vme2App/guieditor/template/" + id + ".html", function(template) {
				that.templates[id] = template;
				callback(template, id);
			});
		}
	}
});