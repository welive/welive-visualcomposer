EditWindow = Class
		.extend({
			NAME : "vme2.EditWindow",

			baseURL : '/visualcomposer',

			init : function(id, filename, contentType, isdefault, projectId, projectName) {
				this.id = id;
				this.filename = filename;
				this.contentType = contentType;
				this.isdefault = isdefault;
				this.projectId = projectId;
				this.projectName = projectName;

				this.tabId = "tab" + this.id;

				this.addTab();
			},

			getFileId : function(){
				return this.id;
			},
			
			getTabId : function() {
				return this.tabId;
			},

			closeTab : function(deletedTabId){
				delete guieditor.editWindows[deletedTabId];
			},
			// IVAN for materialize version		
			addTab : function() {			
				var tabs = $("#canvastabsystem.tabs");		
				
				var tabName = this.filename.replace(".html.working", "");
				if(this.isdefault){
					tabName = this.projectName + " (main)";
				}
				
				$.ajax ({
					url: this.baseURL+ "/guieditor/project_repository/gprj_"+this.projectId+"/"+this.filename,
					type: "GET"                        
				}).done($.proxy(function(data) {
					
					var htmlContent = data;
					
					if(this.isdefault)
					{
						//tabs.addBSTab(this.tabId, tabName, htmlContent);
						$(tabs).append('<li class="tab col s12"><a href="#' + this.tabId + '">' + tabName + '</a></li>');
						$('.tab-content').append('<div id="' + this.tabId + '" class="row tab-pane demo">' + htmlContent + '</div>');
						
						$('.demo *').on('contextmenu', function(e){
							showContextMenu(e);
						});
					} 
					else 
					{
						// TODO to be completed
						//tabs.addBSTab(this.tabId, tabName, htmlContent, true, this.closeTab);
					}
					
					$("#" + this.tabId).addClass("demo");

					guieditor.enableComponentSelection(this.tabId);
					guieditor.enableSortable();

					// Prende gli item contenuti nel canvas che hanno un evento onexport
					// da
					// invocare
					var itemToRender = $("#" + this.tabId + " [data-vmert-onrender]");

					$.each(itemToRender, function(i, element) {
						var paletteItemId = $(element).data("vme-itemid");
						if (paletteItemId !== undefined) {

							var paletteItem = guieditor.getPalette().getItem(
									paletteItemId);
							paletteItem.setOnCanvasAction(element);
						}
					});						
					
				}, this));				
			},
			
			_buildContent : function() {
				// Clona il contenuto del canvas
				var htmlContent = $("#" + this.tabId).clone();				
				
				// Prende gli item contenuti nel canvas che hanno un evento onexport
				// da invocare
				var itemToRender = $("[data-vme-onexport]", htmlContent);

				$.each(itemToRender, function(i, element) {
					var attribute = $(element).data("vme-onexport");
					var idx = attribute.indexOf(".");
					var module = attribute.substring(0, idx);
					var method = attribute.substring(idx + 1);

					// Chiama i metodi tramite app requirejs in maniera sincrona in
					// modo da
					// evitare che il post seguente
					// venga effettuato prima dell'invoke del metodo
					if (require.defined("app/" + module)) {
						var requirejs = require("app/" + module);
						requirejs[method](element);
					}
				});
				
				return htmlContent;
			},

			postFile : function(filecontent, filename, contenttype) {	
				var result = false;
				$.ajax({
	            url: this.baseURL + "/user/gui/" + this.projectId + "/file/" + this.id + "/save",
	            global: false,
	            type: 'POST',
	            data: {
						"filecontent" : filecontent,
						"filename" : filename,
						"contenttype" : contenttype
					},
	            async: false, //blocks window close
	            success: function(){
	            	result = true;
	            }
				});
				
				return result;
			},

			saveContent : function() {
				//Salva i file html e working in quiet mode
				return this.previewPage(true);
			},
			
			// IVAN
			screenshotPage: function()
			{
				var htmlContent = this._buildContent().html();
				var configPaths = guieditor.getRequirejsPaths();
				var scriptForImport = "require.config("+JSON.stringify(configPaths)+");\n";
				
				var linkedCss = guieditor.getLinkedCss();
				
				$.each(linkedCss, function(index, elem){
					scriptForImport+="requirejs(['css!prjcss/"+elem.filename+"']);\n";						
				});
				
				var linkedJs = guieditor.getLinkedJs();
				
				$.each(linkedJs, function(index, elem){
					var filename = elem.filename;
					filename = filename.substring(0, filename.lastIndexOf("."));
					scriptForImport+="requirejs(['prjjs/"+filename+"'],function("+filename+"){window."+filename+" = "+filename+";});\n";						
				});				
				
				var importScript = "<script>"+scriptForImport+"</script>";	
				
				var head = $("<head>")
				.append("<script data-main='"+guieditor.getFullUrl()+"/guieditor/project_template/js/main' src='"+guieditor.getFullUrl()+"/guieditor/project_template/js/lib/require.js'></script>")
				.append("<script src='"+guieditor.getFullUrl()+"/guieditor/project_template/js/lib/jquery-1.10.2.min.js'></script>")
				.append("<script src='"+guieditor.getFullUrl()+"/guieditor/project_template/js/lib/html2canvas.js'></script>")
				.append("<script src='"+guieditor.getFullUrl()+"/guieditor/project_template/js/screenshot.js'></script>")
				// IVAN for materialize version
			    .append('<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">')
			    .append('<link type="text/css" rel="stylesheet" href="' + guieditor.getFullUrl() + '/resources/js/materialize/css/materialize.min.css" media="screen,projection"/>')
				//.append("<link href='//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css' rel='stylesheet'>")
				.append("<link href='"+guieditor.getFullUrl()+"/guieditor/project_template/css/screenshot.css' rel='stylesheet'>");
				//.append("<link href='"+guieditor.getFullUrl()+"resources/css/main.css' rel='stylesheet'>");
				
				var screenshotToolbar = ''
				+ '<div id="screenshotToolbar">'
				+  '<div class="row">'
				+  '    <form class="col s12">'
				+  '      <div class="row valign-wrapper" style="margin-bottom: 0">'
				+  '        <div class="input-field col s10">'
				+  '          <textarea id="screenshotDescription" name="screenshotDescription" class="materialize-textarea"></textarea>'
				+  '          <label for="screenshotDescription" class="active">Description</label>'
				+  '        </div>'
				+  '        <div class="input-field col s2">'
				+  '          <a id="publishButton" class="waves-effect waves-light btn" onclick="javascript:publish('+this.projectId+')"><i class="material-icons left">photo_camera</i>Publish</a>'
				+  '        </div>'
				+  '      </div>'
				+  '    </form>'
				+  '</div>'
				/*'<label id="descrLabel">Description: </label><textarea id="screenshotDescription" />'+
				'<button id="publishButton" class="btn btn-primary" onclick="javascript:publish('+this.projectId+')"><i class="glyphicon glyphicon-camera"></i> Publish</button>'+
				'<a id="downloadImage" class="btn btn-primary" style="display:none"></a>'+*/
				+ '</div>';
				
				var screenshotContainer = '<div id="screenshotContainer" style="display:none"></div>';
				
				var content = $("<html>")
					.append(head)
					.append($("<body>")
							.append(importScript)
							.append(screenshotToolbar).append(screenshotContainer)
							.append($("<div id='mainContainer' style='background: white !important;' class='container'>")
									.append(htmlContent)));
				
				var filename = "screenshot.html";
				
				var result = this.postFile("<!DOCTYPE html>" + $(content)[0].outerHTML, filename, this.contentType);
				
				if(result)
				{
	   				window.open(this.baseURL
	   						+ "/guieditor/project_repository/gprj_"
	   						+ this.projectId + "/" + filename, '_blank');	                   
	   				window.focus();
				}
			},

			previewPage : function(quietly) {
				var htmlContent = this._buildContent();
				
				// save <filename>.working
				var postResult = this.postFile($(htmlContent).html(), this.filename, this.contentType);
				
				/*
				var configPaths = guieditor.getRequirejsPaths();
				
				var scriptForImport = "require.config("+JSON.stringify(configPaths)+");\n";
				
				var linkedCss = guieditor.getLinkedCss();
				
				$.each(linkedCss, function(index, elem){
					scriptForImport+="requirejs(['css!prjcss/"+elem.filename+"']);\n";						
				});
				
				var linkedJs = guieditor.getLinkedJs();
				
				$.each(linkedJs, function(index, elem){
					var filename = elem.filename;
					filename = filename.substring(0, filename.lastIndexOf("."));
					scriptForImport+="requirejs(['prjjs/"+filename+"'],function("+filename+"){window."+filename+" = "+filename+";});\n";						
				});				
				
				var importScript = "<script>"+scriptForImport+"</script>";				
				
				var head = $("<head>")
				//.append(
				//		"<script src='"+guieditor.getFullUrl()+"guieditor/project_template/js/config.js'></script>")
				.append(
						"<script data-main='"+guieditor.getFullUrl()+"guieditor/project_template/js/main' src='"+guieditor.getFullUrl()+"guieditor/project_template/js/lib/require.js'></script>")
				.append(
						"<link href='//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css' rel='stylesheet'>");									
				 				
				var content = $("<html>")
					.append(head)
					.append($("<body>")
							.append(importScript)
							.append($("<div class='container'>")
									.append($(htmlContent).html())));
				
				
				var filename = this.filename.replace(".working", "");

				var result = this.postFile("<!DOCTYPE html>" + $(content)[0].outerHTML, filename, this.contentType);
				*/
				if(postResult && !quietly)
				{
   				window.open(this.baseURL
   						+ "/guieditor/project_repository/gprj_"
   						+ this.projectId + "/" + guieditor.spaName + ".html", '_blank');	                   
   				window.focus();
				}
				
				return postResult ? this.filename : false;
			}
		});