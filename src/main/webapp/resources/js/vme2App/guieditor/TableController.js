function tableModal_init(table)
{
	currentTable = table;
	
	var rows = $(currentTable).find("tbody tr");
	var cols = $(currentTable).find("thead tr th");
	
	$("#tableModalRows").val(rows.length);
	$("#tableModalColumns").val(cols.length);
	
	var list = "";
	$(cols).each(function(index) {
		list += '<li class="collection-item"><div><input style="width:80%" type="text" value="' + $(this).html() + '">'
		+ '<a href="#!" class="secondary-content">'
		+ '<i class="material-icons tooltipped" data-position="top" data-tooltip="Add row before" onclick="javascript:tableModal_addBefore(this)">vertical_align_top</i>'
		+ '<i class="material-icons tooltipped" data-position="top" data-tooltip="Add row after" onclick="javascript:tableModal_addAfter(this)">vertical_align_bottom</i>'
		+ '<i class="material-icons tooltipped" data-position="top" data-tooltip="Delete row" onclick="javascript:tableModal_deleteColumn(this)">delete</i>'
		+ '</a></div></li>';
	});
	
	$("#tableModalColumnList").html(list);
	Materialize.updateTextFields();
	$('.tooltipped').tooltip();
}

function tableModal_addColumn(element, before)
{
	var size = parseInt($("#tableModalColumns").val());
	
	var col = '<li class="collection-item"><div><input style="width:80%" type="text" value="">'
		+ '<a href="#!" class="secondary-content">'
		+ '<i class="material-icons tooltipped" data-position="top" data-tooltip="Add row before" onclick="javascript:tableModal_addBefore(this)">vertical_align_top</i>'
		+ '<i class="material-icons tooltipped" data-position="top" data-tooltip="Add row after" onclick="javascript:tableModal_addAfter(this)">vertical_align_bottom</i>'
		+ '<i class="material-icons tooltipped" data-position="top" data-tooltip="Delete row" onclick="javascript:tableModal_deleteColumn(this)">delete</i>'
		+ '</a></div></li>';
	
	var current = $(element).parent().parent().parent();	
	var list = $("#tableModalColumnList").children();
	var c;
	
	var cellContent = '<span data-vme-dad="sortable" data-vme-itemid="vme-span" data-vme-type="inline" data-onlydatabind="false" data-datatype="text" data-databind="text" data-databind-type="simple" data-bind-property="text" class="" data-vme-sortable="true">Cell content</span>';
	
	for(var i = 0; i < list.length; i++)
	{
		c = $(list).get(i);
		
		if($(c).is(current))
		{
			if(before)
			{
				$(current).before(col);
				$($(currentTable).find("thead tr th").get(i)).before("<th></th>");
				
				$("tbody tr", currentTable).each(function() {
					$($(this).find("td").get(i)).before("<td>" + cellContent + "</td>");
				});
			}
			else
			{
				$(current).after(col);
				$($(currentTable).find("thead tr th").get(i)).after("<th></th>");
				
				$("tbody tr", currentTable).each(function() {
					$($(this).find("td").get(i)).after("<td>" + cellContent + "</td>");
				});
			}
			
			break;
		}
	}
	
	$("#tableModalColumns").val(size + 1);
}

function tableModal_addBefore(element)
{
	tableModal_addColumn(element, true);
}

function tableModal_addAfter(element)
{
	tableModal_addColumn(element, false);
}

function tableModal_deleteColumn(element)
{
	var size = parseInt($("#tableModalColumns").val());
	
	var index = -1;
	
	var current = $(element).parent().parent().parent();	
	var list = $("#tableModalColumnList").children();
	var c;
	
	for(var i = 0; i < list.length; i++)
	{
		c = $(list).get(i);
		
		if($(c).is(current))
		{
			index = i;
			
			break;
		}
	}
	
	$(list).get(index).remove();
	
	$("tr", currentTable).each(function() {
		$("td:eq(" + index + "), th:eq(" + index + ")", this).remove();
	});
	
	$("#tableModalColumns").val(size - 1);
}

function tableModal_update()
{
	var list = $("#tableModalColumnList").children();
	var tableHeads = $(currentTable).find("thead tr th");
	var c;
	
	for(var i = 0; i < list.length; i++)
	{
		c = $(list).get(i);
		
		$($(tableHeads).get(i)).html($(c).find("div input").val());
	}
	
	var currentRows = $(currentTable).find("tbody tr").size();console.log("currentRows",currentRows);
	var rows = parseInt($("#tableModalRows").val());
	
	var lastrow;
	
	if(currentRows < rows)
	{
		for(var i = 0; i < rows - currentRows; i++)
		{
			lastrow = $(currentTable).find("tbody tr:last").clone();
			$(currentTable).find("tbody tr:last").after(lastrow);
		}
	}
	else if(currentRows > rows)
	{
		while(rows != currentRows)
		{
			$(currentTable).find("tbody tr:last").remove();
			currentRows--;
		}
	}	
}

function tableModal_selectDataOpen()
{
	$("#tableSelectDataModal").openModal();
}