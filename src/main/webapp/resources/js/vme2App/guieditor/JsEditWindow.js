EditWindow.JsEditWindow = EditWindow
		.extend({
			NAME : "vme2.EditWindow.JsEditWindow",

			baseURL : '/visualcomposer',

			init : function(id, filename, contentType, projectId) {
				this.id = id;
				this.filename = filename;
				this.contentType = contentType;
				this.isdefault = false;
				this.projectId = projectId;

				this.tabId = "tab" + this.id;

				this.addTab();
			},

			closeTab : function(deletedTabId){
				delete guieditor.editWindows[deletedTabId];
			},
			
			// IVAN for materialize version	
			addTab : function() {
				//var tabs = $("ul.nav-tabs");				
				var tabs = $("#canvastabsystem.tabs");
				
				$(tabs).append('<li class="tab col s12"><a href="#editor_' + this.tabId + '">' + this.filename 
						+ '<i class="material-icons secondary-content" onclick="javascript:guieditor.closeTab(\'' + this.tabId + '\')">close</i></a></li>');
				$('.tab-content').append('<div id="editor_' + this.tabId + '" class="row tab-pane"></div>');
											
				// set the size of the panel
				$("#editor_"+this.tabId).width('100%');
				$("#editor_"+this.tabId).height('600px');
				
				$(tabs).tabs();
		        				
				var aceEditor = ace.edit("editor_"+this.tabId);

				this.aceEditor = aceEditor;

				aceEditor.setTheme("ace/theme/chrome");
				aceEditor.setShowPrintMargin(false);
				aceEditor.session.setMode("ace/mode/javascript");
				aceEditor.session.setTabSize(2);
				aceEditor.session.setUseWrapMode(true);
				
				$.ajax ({
					url: this.baseURL+ "/guieditor/project_repository/gprj_"+this.projectId+"/js/"+this.filename,
					type: "GET"                        
				}).done(function(data) {
					aceEditor.session.setValue(data);					
				});					
			},

			_buildContent : function() {
				return this.aceEditor.session.getValue();
			},

			saveContent : function() {
				var content = this._buildContent();

				var postResult = this.postFile(content, this.filename, this.contentType);
				
				return postResult ? this.filename : false;
			}
		});