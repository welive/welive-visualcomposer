vme2.backend.Backend = Class
		.extend({

			baseURL : '',

			definitions : {},

			init : function(baseURL) {
				$(document).ajaxError(this.ajaxErrorHandling);

				this.baseURL = baseURL;
			},
			
			// IVAN
			_displayErrorLayer: function(data) { 
				
				var executionErrorMessage = '';
				
				if(data && data.error && data.error == '__MASHUP_EXECUTION_ERROR')
				{
					// manage error response from EMML
					if(data.emmlResponse)
					{
						if(data.emmlResponse.code == '__DIRECTINVOKE_ERROR')
						{
							executionErrorMessage += ''
								+ '<div class="row executionErrorMessage">'
								+ '<h5>Error ' + data.emmlResponse.responseCode + ' invoking external service <b style="background-color:white !important;color:red !important;font-family:monospace !important">' + data.emmlResponse.endpoint + '</b></h5>'
								+ '<div>' + data.emmlResponse.responseBody + '</div>'
								+ '</div>';
						} 
						else if(data.emmlResponse.code == '__MACRO_ERROR')
						{
							var macroName = Message['mashup.operators.' + data.emmlResponse.macroName + '.label']
							
							executionErrorMessage += ''
								+ '<div class="row executionErrorMessage">'
								+ '<h5>Error executing operator <b>' + macroName + '</b>: ' + data.emmlResponse.message + '</h5>' // TODO replace macroName with operator name
								+ '</div>';
						}// TODO complete cases
						else
						{
							executionErrorMessage += ''
								+ '<div class="row executionErrorMessage">'
								+ '<h5>' + data.emmlResponse.message + '</h5>'
								+ '</div>';
						}
					}
					else
					{
						executionErrorMessage = data.message;
						
						executionErrorMessage = executionErrorMessage.replace(new RegExp('\r?\n','g'), ' ');
						
						executionErrorMessage = ''
							+ '<div class="row executionErrorMessage">'
							+ executionErrorMessage
							+ '</div>';
					}
					
					
				}
				
				var error_content = ''
					+ '<div class="row">'
					+	'<h5>' + Message['mashup.messages.error_invoking_workflow'] + '</h5>'
					+ '</div>';
				
				error_content += executionErrorMessage;

				error_content += '<br>';
				
				error_content += ''
					+ '<div class="row center">'
					+	'<a id="close_spinner" class="waves-effect waves-light btn"><i class="material-icons left">clear</i>close</a>'
					+ '</div>';
				
				var old_content = $("#build_wait").html();
				
				$("#build_wait").html(error_content);
				
				$("#close_spinner").off('click');
				$("#close_spinner").on('click', function() { 
					$("#spinner_layer").hide();
					$("#build_wait").html(old_content);
				});	
				
				if(app.view.commandStack.getUndoLabel() == 'Connecting Ports')
				{
					app.view.commandStack.undo();
					app.view.commandStack.redostack.pop();
				}
			},

			ajaxErrorHandling : function(event, jqXHR, settings, exception) { console.log(event);console.log(exception);
				
				if ($('#wait').length) {
					$('#wait').hide();
				}
				
				event.preventDefault();

				var response;
				try {
					response = JSON.parse(jqXHR.responseText);
				} catch (err) {
					response = {
						message : jqXHR.statusText,
						stackTrace : ''
					};
				}

				if (jqXHR.status == 401 && "redirectUrl" in response) {// unauthorized
																							// error
																							// code
					app.oauthAuthenticate(event, response.redirectUrl, null);
				} 
				else 
				{					
					var panel = "<div>" + "<h4 class=\"text-info\">"
							+ response.message + "</h4>";

					if (response.extendedMessage !== undefined
							&& response.extendedMessage !== '') {
						var extendedMessage = response.extendedMessage;
						
						panel += parseExtendedErrorMsg(extendedMessage);
					}

					panel += "<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\" class=\"text-error\"><small>view stack trace</small></a>"
							+ "<div id=\"collapseOne\" class=\"panel-collapse collapse text-error\">"
							+ "<small>"
							+ response.stackTrace
							+ "</small>"
							+ "</div>"
							+ "</div>";
					
					// IVAN
					// show error message
					
					//bootbox.alert(panel);
					
					
					var error_content = ''
						+ '<div class="row">'
						+	'<h5>' + Message['mashup.messages.error_invoking_workflow'] + '</h5>'
						+ '</div>';
					
					error_content += '<br>';
					
					error_content += ''
						+ '<div class="row center">'
						+	'<a id="close_spinner" class="waves-effect waves-light btn"><i class="material-icons left">clear</i>close</a>'
						+ '</div>';
					
					var old_content = $("#build_wait").html();
					
					$("#build_wait").html(error_content);
					
					$("#close_spinner").off('click');
					$("#close_spinner").on('click', function() { 
						$("#spinner_layer").hide();
						$("#build_wait").html(old_content);
					});
					
				}
			},

			getServices : function(successCallback, errorCallback) {
				$.getJSON(this.baseURL + "/user/servicecatalog/list.json",
						function(data) {
							successCallback(data);
						});
			},

			getOpenData : function(successCallback, errorCallback) {
				$.getJSON(this.baseURL + "/user/opendata/list.json",
						function(data) {
							successCallback(data);
						});
			},

			getBlackbox : function(projectId, successCallback, errorCallback) {
				$.getJSON(this.baseURL + "/user/blackbox/" + projectId
						+ "/list.json", function(data) {
					successCallback(data);
				});
			},

			getInnerProcess : function(projectId, successCallback, errorCallback) {
				$.getJSON(this.baseURL + "/user/innerprocess/" + projectId
						+ "/list.json", function(data) {
					successCallback(data);
				});
			},

			getOperatorList : function(successCallback, errorCallback) {
				$.getJSON(this.baseURL + "/mashupeditor/operators/list.json",
						function(data) {
							successCallback(data);
						});
			},
			
			getOperatorCategories : function(successCallback, errorCallback) {
				$.getJSON(this.baseURL + "/mashupeditor/operators/operatorCategories.json",
						function(data) {
							successCallback(data);
						});
			},

			getOperator : function(operatorId, successCallback, errorCallback) {
				$.getJSON(this.baseURL + "/mashupeditor/operators/metadata/"
						+ operatorId + ".json", function(data) {
					successCallback(data);
				});
			},

			getOperations : function(serviceId, successCallback, errorCallback) {
				$.getJSON(this.getOperationsServiceUrl(serviceId), function(data) {
					successCallback(data);
				});
			},
			// IVAN
			getResources : function(opendataId, successCallback, errorCallback) {
				$.getJSON(this.getResourcesServiceUrl(opendataId), function(data) {
					successCallback(data);
				});
			},

			getOperationParams : function(operationId, serviceType,
					successCallback) {
				$.getJSON(this.baseURL + "/user/servicecatalog/" + serviceType
						+ "/" + operationId + "/operationparams.json",
						function(data) {
							successCallback(data);
						});
			},

			getOperationTemplateParams : function(templateId, serviceType,
					successCallback) {
				$.getJSON(this.baseURL + "/user/servicecatalog/" + serviceType
						+ "/" + templateId + "/templateparams.json", function(data) {
					successCallback(data);
				});
			},

			getOperationsServiceUrl : function(serviceId) {
				return this.baseURL + "/user/servicecatalog/" + serviceId
						+ "/operations.json";
			},
			
			getResourcesServiceUrl : function(opendataId) {
				return this.baseURL + "/user/opendata/" + opendataId
						+ "/resources.json";
			},

			getRenderers : function(successCallback, errorCallback) {
				$.getJSON(this.baseURL + "/user/renderercatalog/list.json",
						function(data) {
							successCallback(data);
						});
			},

			/*
			 * getRendererCategory : function(successCallback, errorCallback) {
			 * $.getJSON(this.baseURL + "/user/renderercatalog/listcategory.json",
			 * function(data) { successCallback(data); }); },
			 */

			save : function(definitionId, content, successCallback) {
				$.ajax({
					url : this.baseURL + "/user/project/" + definitionId + "/dashboardset.json",
					type : "POST",
					data : JSON.stringify(content),
					dataType : "json",
					contentType : "application/json; charset=utf-8",
					success: function(data) {
						successCallback();
					},
					error: function(error) {
						var toastContent = $('<span class="toastError">Sorry, an error occurred saving project</span>');
						Materialize.toast(toastContent, 2000);
						$(".toastError").parent().addClass("toast-error");
					}

				});

			},

			load : function(definitionId, successCallback, errorCallback) {
				$.getJSON(this.baseURL + "/user/project/" + definitionId
						+ "/dashboardget.json", function(data) {
					successCallback(data);
				});
			},
			/*
			 * invoke : function(params, successCallback, errorCallback) {
			 * $.post(this.baseURL+ "/user/servicecatalog/invoke",
			 * JSON.stringify(params) ).done(function(data) {
			 * successCallback(data); }); },
			 */

			invokeConverter : function(params, successCallback, errorCallback) {				
				$.ajax({
					url : this.baseURL + "/user/servicecatalog/invokeconverter",
					type : 'POST',
					data : JSON.stringify(params),
					contentType : 'application/json',
					success : function(data) {
						successCallback(data);
					}
				});								
			},
			invokeWorkflow : function(projectId, params, successCallback, errorCallback) {	
				
				var _this = this;
				
				$.ajax({
					url : this.baseURL + "/user/project/" + projectId
							+ "/invokeemml.json",
					type : 'POST',
					data : params,					
					success : function(data) {
					
						if(data && data.error && data.error == '__MASHUP_EXECUTION_ERROR')
						{
							_this._displayErrorLayer(data);
						}
						else
						{
							successCallback(data);
						}						
					}
				});
			},
			invokeServiceChain : function(projectId, params, successCallback, errorCallback) {
				
				var paramString = JSON.stringify(params);
				
				var _this = this;
				
				$.ajax({
					url : this.baseURL + "/user/project/" + projectId
							+ "/invokechain.json",
					type : "POST",
					data : paramString,
					dataType : "json",
					contentType : "application/json; charset=utf-8",

				})
				.done(function(data) {
					
					if(data && data.error && data.error == '__MASHUP_EXECUTION_ERROR')
					{
						_this._displayErrorLayer(data);
					}
					else
					{
						successCallback(data);
					}
				});
			},
			// IVAN
			invokeOpenDataResource : function(openDataId, query, successCallback,
					errorCallback) {
				$.ajax(
						{
							url : this.baseURL + "/public/opendata/" + openDataId + "/query/" + encodeURIComponent(query)
									+ "/getastree.json",
							type : "POST",
							dataType : "json",
							contentType : "application/json; charset=utf-8",
						}).done(function(data) {
					successCallback(data);
				});
			},
			getTreeNodeDetail : function(nodepath, successCallback, errorCallback) {
				$.getJSON(this.baseURL
						+ "/user/project/detail/nodetree.json?nodepath=" + nodepath,
						function(data) {
							successCallback(data);
						});
			},
			
			uploadXsl : function(form, successCallback){
				$.ajax({
					url: this.baseURL+ "/user/project/addxsl/file",
					data: form,
					dataType: 'json',
					processData: false,
					contentType: false,
					type: 'POST'
				}).done(function(data) {
					successCallback(data);
				});
			},
			
			getXslFileList : function(successCallback){
				$.getJSON(this.baseURL + "/user/project/listxsl/file.json",
						function(data) {
							successCallback(data);
						}
				);
			},
			
			ckanSearch : function(serverId, query, successCallback, errorCallback) {
				$.getJSON(this.baseURL
						+ "/user/ckan/"+serverId+"/searchdataset.json?query=" + query,
						function(data) {
							successCallback(data);
						});
			},			
			
			ckanGetAll : function(serverId, page, successCallback, errorCallback) {
				$.getJSON(this.baseURL
						+ "/user/ckan/"+serverId+"/getalldatasets.json?pagenumber=" + page,
						function(data) {
							successCallback(data);
						});
			},				
			
			addCkanOpendataResource: function(ckanServerId, datasetId, resourceId, successCallback){
				var data = {
						"ckanServerId": ckanServerId,
						"datasetId": datasetId,
						"resourceId": resourceId
				}
				
				console.log(data);
				
				$.ajax({
					url: this.baseURL+ "/user/ckan/addresource.json",
					contentType : 'application/json; charset=utf-8',
					dataType : 'json',
					data: JSON.stringify(data),
					type: 'POST'
				}).done(function() {
					successCallback();
				});
			}
		});
