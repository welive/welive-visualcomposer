parseExtendedErrorMsg = function(extendedMsg) {
	
	var extendedMessage = extendedMsg.replace("<![CDATA[", "").replace("]]>", "");
	extendedMessage = escapeXml(extendedMessage);
	extendedMessage = extendedMessage.replace(/(?:\r\n|\r|\n)/g, '<br />');
	var panel = "";
	
	var serviceEndpointRE = /endpoint="((https?\:\/\/)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?)"/;
	var serviceEndpointMatch = serviceEndpointRE.exec(extendedMessage);
	var serviceEndpoint = serviceEndpointMatch != null ? serviceEndpointMatch[1]
			: null;

	var serviceInvokeErrRE = /##errorcode:([0-9]*)## - ##responsebody:([\s\S]*)##/gmi;
	var serviceInvokeErrMatch = serviceInvokeErrRE
			.exec(extendedMessage);
	var serviceInvokeErrCode = serviceInvokeErrMatch != null ? serviceInvokeErrMatch[1]
			: null;
	var serviceInvokeResp = serviceInvokeErrMatch != null ? serviceInvokeErrMatch[2]
			: null;

	if (serviceEndpoint != undefined && serviceEndpoint != null) {
		panel += "<h5 class=\"text-info\"><strong>caused by: <span class=\"text-error text-danger\">"
				+ serviceEndpoint + "</span></strong></h5>";
	}

	var message = '';
	if (serviceInvokeErrCode != undefined
			&& serviceInvokeErrCode != null) {
		message += "<strong>Http Response error code:</strong> "
				+ serviceInvokeErrCode + "<br/>";
	}
	if (serviceInvokeResp != undefined && serviceInvokeResp != null) {
		message += "<strong>Http Response:</strong> "
				+ serviceInvokeResp + "<br/>";
	}

	if (message == '') {
		message = extendedMessage;
	}

	panel += "<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseEM\" class=\"text-warning\"><small>view exteded message</small></a>"
			+ "<div id=\"collapseEM\" class=\"panel-collapse collapse text-warning\">"
			+ "<small>" + message + "</small>" + "</div>";
	
	return panel;
};

function escapeXml(unsafe) {
   return unsafe.replace(/[<>&'"]/g, function (c) {
       switch (c) {
           case '<': return '&lt;';
           case '>': return '&gt;';
           case '&': return '&amp;';
           case '\'': return '&apos;';
           case '"': return '&quot;';
       }
   });
}

function convertToJstreeNode(node)
{
	var r;
	
	$.ajax({
		type: 'POST',
		url: '/visualcomposer/user/project/jstree.json',
		data: JSON.stringify(node),
		success:function(result)
		{
			console.log(result);
			r = result;
		}
	});
	
	return r;
}

buildTreePanel = function(sourceId, data, figureUserData, usePreviewPanel){	
	
	//console.log("function: buildTreePanel: START", figureUserData);
	
		this.sourceId = sourceId;
		
		//this.structure = data.structure;
		//this.completeData = data.data;
		
		var sourceInfoView;
		
		var infoTempl = "{{#if .}}<p>"
			+ "{{#if operation}}<strong>SERVICE</strong>: {{operation.name}} ({{name}}){{/if}}"
			+ "{{#if resourceTitle}}<strong>OPENDATA</strong>: {{resourceTitle}} ({{name}}){{/if}}"
			+" {{#if emmlmacro}}<strong>OPERATOR</strong>: {{name}}{{/if}}"
			+ "</p>{{/if}}";
		
		var infoTemplCompiled = Handlebars.compile(infoTempl);
		sourceInfoView = $(infoTemplCompiled(figureUserData));
			
		var sourceInfoSelector = "#sourceinfo";
		var treeContainerSelector = "#treecontainer";
		
		if(usePreviewPanel===true){
			sourceInfoSelector = ".previewinfo";
			treeContainerSelector = ".previewtreecontainer";
		}
				
		$(sourceInfoSelector).html(sourceInfoView);

		if(usePreviewPanel===null || usePreviewPanel===undefined || usePreviewPanel===false){
			$(sourceInfoSelector).append("<p><strong>Source tree</strong></p>");
		}
		
		$(treeContainerSelector).jstree(
			{
				"json_data" : {
					data : data
				},
				"dnd" : {
					"drop_finish" : $.proxy(function(data) {
						var src = data.o;
						var dest = data.r;
						
						var srcDT = $.data(src[0]).datatype;
						var dstDT = $(dest).data("datatype");
						
						if(srcDT!==undefined && dstDT!==undefined){
							if(srcDT!==dstDT){
								var toastContent = $('<span class="toastError">Selected source type "' + srcDT + '" is not compatible with expected target type "' + dstDT + '"</span>');
								Materialize.toast(toastContent, 2000);
								$(".toastError").parent().addClass("toast-error");
								return;
							}
						}
						
						var path = "{" + this.sourceId + ":" + $.data(src[0]).path + "}";
						$(dest).val(path);
						
						// IVAN
						// trigger the drop event handler of the operator
						$(dest).trigger('drop');
						
					}, this)
				},
				"themes" : {
					"theme" : "classic"
				},
				"crrm" : {
					"move" : {
						"check_move" : function (m) {
							return false;
						}
					}
				},
				"plugins" : [ "themes", "json_data", "ui", "crrm", "dnd", "state" ]
			}).bind("select_node.jstree", function(event, data)
					{/*
						var parent = $(treeContainerSelector).jstree("get_selected");
						var path = data.inst.get_path();
						
						var toEval = "completeData";
						var test;
						
						for(i = 1; i < path.length; i++)
						{
							test = Number(path[i]);
							
							if(isNaN(test))
								toEval += "." + path[i];
							else
								toEval += "[" + test + "]";
						}
						
						var nodeToAdd = eval(toEval);
						
						var response = $.ajax({
							async: false,
							type: 'POST',
							url: '/visualcomposer/user/project/jstree.json',
							data: JSON.stringify(node)
						});
						
						
						console.log(response.responseJSON);						
						
						var t = $(treeContainerSelector).jstree("reference"); 
						var newNode = t._parse_json(response.responseJSON);
						
						console.log(newNode);
					    parent.append(newNode); 
						t.clean_node(parent); 
						
					*/});
		
		if(usePreviewPanel===true)
		{
			$(treeContainerSelector).on('loaded.jstree', function() 
   			{
				//$(treeContainerSelector).jstree('close_all');
				//console.log("Loaded");
   			});
		}
		
		//console.log("function: buildTreePanel: END");
}