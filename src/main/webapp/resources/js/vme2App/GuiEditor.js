GuiEditor = Class
		.extend({
			NAME : "vme2.GuiEditor",

			baseURL : '/visualcomposer',

			_timerSave : 2e3,

			inputTable : "<table class=\"striped\" id=\"inputParamTable\">"+
   	   	"	<thead>"+
   	   	"		<tr><th>Input name</th><th>Input id</th><th>Datasource</th></tr>"+
   	   	"	</thead>"+
   	   	"	<tbody>"+
   	   	"		{{#each inputParams}}"+
   	   	"		<tr {{#if isConstant}}style=\"display:none\"{{/if}}>" +
   	   	"			<td><input class='with-gap' type='radio' id=\"{{inputName}}\" name='selection' value=\"{{inputName}}\"/><label for=\"{{inputName}}\" class=\"checkbox-label\">{{label}}</label></td>"+
   	   	"			<td>{{inputName}}</td>"+
   	   	"			<td>{{mashupName}}</td>"+
   	   	"		</tr>"+
   	   	"		{{/each}}"+
   	   	"	</tbody>"+
   	   	"</table>",
   	   	
   	   		spinnerStyle: ''
   	   			+'<style>'
		   	   	+'#waiting_layer'
		   	   	+'{'
		   	   	+	'z-index: 1000;'
		   	   	+	'position: fixed;'
		   	   	+	'top: 0;'
		   	   	+	'left: 0;'
		   	   	+	'width: 100%;'
		   	   	+	'height: 100%;'
		   	   	+	'background: rgba(255,255,255,0.8);'
		   	   	+'}'
		   	   	+'#waiting_layer > div'
		   	   	+'{'
		   	   	+	'position: relative;'
		   	   	+	'width: 100%;'
		   	   	+	'text-align: center;'
		   	   	+'}'		
		   	   	+'#waiting_layer h5'
		   	   	+'{'
		   	   	+	'text-align: center;'
		   	   	+'}'		
		   	   	+'#waiting_layer .progress'
		   	   	+'{'
		   	   	+	'width: 50% !important;'
		   	   	+'}'
		   	   	+'</style>',
			
			init : function(projectId, mashupProjectId, fullUrl, projectName, spaName, userLevel) {
				$(document).ajaxError(this.ajaxErrorHandling);

				this.itemIdMap = new Array();
				this.itemIdMap['vme-h'] = "Heading";
				this.itemIdMap['vme-form'] = "Form";
				this.itemIdMap['vme-table'] = "Table";
				this.itemIdMap['vme-gmap'] = "Map";
				this.itemIdMap['vme-p'] = "Paragraph";
				this.itemIdMap['vme-label'] = "Label";
				this.itemIdMap['vme-inputtext'] = "Input Text";
				this.itemIdMap['vme-button'] = "Button";
				this.itemIdMap['vme-ul'] = "Unordered List";
				this.itemIdMap['vme-ol'] = "Ordered List";
				this.itemIdMap['vme-li'] = "List Element";
				this.itemIdMap['vme-a'] = "Anchor";
				this.itemIdMap['vme-img'] = "Image";
				this.itemIdMap['vme-span'] = "Inline Container";
				this.itemIdMap['vme-div'] = "Block Container";
				
				this.editWindows = {};

				this.projectId = projectId;
				this.mashupProjectId = mashupProjectId;
				this.fullUrl = fullUrl;
				this.projectName = projectName;
				
				if(spaName == undefined || spaName == "")
					this.spaName = "main";
				else
					this.spaName = spaName;
				
				this.userLevel = userLevel;

				// Inizializza gli helpers di handlebars per tutta il guieditor
				new HandlebarsHelpers();

				this.jquerySelectorExtention();

				// ******Temporanee probabilmente da eliminare
				this.currentDocument = null;
				this.demoHtml = $(".demo").html();

				// this.removeElm();
				// this.configurationElm();
				// ******Temporanee probabilmente da eliminare

				// Crea il template manager per il recupero dei template html di
				// handlebars
				this.templateManager = new TemplateManager();

				// Inizializza l'editor HTML ACE
				this.initHtmlEditor();

				this.renderComplexComponents();

				this._projectCss = undefined;
				this._projectJs = undefined;
				this._projectHtml = undefined;

				this._loadLinkedFile();
				
				this.getMashups();
				
			},

			jquerySelectorExtention : function() {
				$.extend($.expr[':'], {
					attrNameStart : function(el, i, props) {
						//console.log(arguments);
						var hasAttribute = false;
						$.each(el.attributes, function(i, attr) {
							if (attr.name.indexOf(props[3]) !== -1) {
								hasAttribute = true;
								return false; // to halt the iteration
							}
						});
						return hasAttribute;
					}
				});
			},

			createEditWindow : function(id, filename, contentType, isdefault) {
				if (isdefault) {
					this.defaultFileId = id;
				}

				var editWin = new EditWindow(id, filename, contentType, isdefault,
						this.projectId, this.projectName);

				this.editWindows[editWin.getTabId()] = editWin;
			},

			renderComplexComponents : function() {
				var elementList = $("[data-vmert-onrender]", '.demo');

				$.each(elementList, function(i, element) {
					var attribute = $(element).data("vmert-onrender");
					var idx = attribute.indexOf(".");
					var module = attribute.substring(0, idx);
					var method = attribute.substring(idx + 1);

					require([ "app/" + module ], function(a) {
						if (method in a) {
							a[method](element);
						}
					});
				});
			},

			_loadLinkedFile : function() {
				var config = new Object();
				config.paths = {
					prjcss : this.baseURL + '/guieditor/project_repository/gprj_'
							+ this.projectId + '/css'
				};
				require.config(config);

				$.getJSON(this.baseURL + "/user/gui/" + this.projectId
						+ "/getcsslist.json", $.proxy(function(data) {
					this._projectCss = data;

					$.each(this._projectCss, function(index, elem) {
						requirejs([ 'css!prjcss/' + elem.filename ]);
					});
				}, this));

				$.getJSON(this.baseURL + "/user/gui/" + this.projectId
						+ "/getjslist.json", $.proxy(function(data) {
					this._projectJs = data;

				}, this));

				$.getJSON(this.baseURL + "/user/gui/" + this.projectId
						+ "/gethtmllist.json", $.proxy(function(data) {
					this._projectHtml = data;

				}, this));
			},

			reloadProjectCss : function(cssFileId) {
				$.each(this._projectCss, function(index, elem) {
					if (elem.id == cssFileId) {
						require.undef('css!prjcss/' + elem.filename);
						requirejs([ 'css!prjcss/' + elem.filename ]);
					}
				});
			},

			getRequirejsPaths : function() {
				var config = new Object();
				
				config.paths = {
					prjcss : this.fullUrl + '/guieditor/project_repository/gprj_'
							+ this.projectId + '/css',
					prjjs : this.fullUrl + '/guieditor/project_repository/gprj_'
							+ this.projectId + '/js'
				};

				return config;
			},

			getLinkedCss : function() {
				return this._projectCss;
			},

			getLinkedJs : function() {
				return this._projectJs;
			},

			initHtmlEditor : function() {
				var htmlEditor = ace.edit("contenteditor");

				this.htmlEditor = htmlEditor;

				htmlEditor.setTheme("ace/theme/chrome");
				htmlEditor.setShowPrintMargin(false);
				htmlEditor.session.setMode("ace/mode/html");
				htmlEditor.session.setTabSize(2);
				htmlEditor.session.setUseWrapMode(true);
			},

			/**
			 * Metodo invocato successivamente alla creazione del GuiEditor
			 */
			postCreate : function() {
				// Crea la palette dei componenti
				this.palette = new Palette(this.mashupProjectId);
				this.buildInvokeWorkflowWindow();
			},

			getPalette : function() {
				return this.palette;
			},

			getHtmlEditor : function() {
				return this.htmlEditor;
			},

			getMashupProjectId : function() {
				return this.mashupProjectId;
			},
			getFullUrl : function() {
				return this.fullUrl;
			},

			enableSortable : function() {
				// Rende sortable il canvas demo
				$(".demo").sortable({
					placeholder : 'highlight-state',
					stop : $.proxy(this._sortableStop, this),
					tolerance : "pointer",
				}).removeClass("ui-sortable").attr("data-vme-sortable", 'true');

				$(".demo [data-vme-dad='sortable']").sortable({
					placeholder : 'highlight-state',
					tolerance : "pointer",
					stop : $.proxy(this._sortableStop, this)
				}).removeClass("ui-sortable").attr("data-vme-sortable", 'true');
				
				$('.demo *').removeAttr('data-vme-boxshadow');
				
				var itemIdMap = this.itemIdMap;
				
				// IVAN
				// set over helper
				$("[data-vme-itemid]").on('mouseover', function() {
					$("#helper-over").html(itemIdMap[$(this).attr("data-vme-itemid")]);
				});
			},
			_sortableStop : function(event, ui) {
				var htmlContent = $("div.view", ui.item).html();
				
				// IVAN for materialize version
				// patch for ul element
				try
				{
					htmlContent = htmlContent.trim();
					htmlContent = htmlContent.substring(4, htmlContent.length - 3);
				}
				catch(ex)
				{
					//console.error(ex);
				}

				if (htmlContent !== undefined) 
				{
					var newDom = $(htmlContent);

					ui.item.replaceWith(newDom);

					this._lastDraggedItem = newDom;
				}
			},
			_dragRevert : function(droppableObj) {
				// if false then no socket object drop occurred.
				if (droppableObj === false) {
					// revert the .myselector object by returning true
					$(droppableObj).data("wasreverted", "true");
					return true;
				} else {
					// return false so that the .myselector object does not revert
					$(droppableObj).data("wasreverted", "false");
					return false;
				}
			},
			_dragStart : function(e, ui) {
				this._lastDraggedItem = undefined;
			},
			_dragHelper : function(event) {
				var helper = $(this).clone();
				$(helper).find("div.view").remove();
				return helper;
			},
			_dragStop : function(e, ui) {
				if (this._lastDraggedItem != undefined) {
					$(".demo [data-vme-dad='sortable']:not([data-vme-sortable='true']")
							.sortable({
								placeholder : 'highlight-state',
								tolerance : "pointer",
								stop : $.proxy(this._sortableStop, this)
							}).removeClass("ui-sortable").attr("data-vme-sortable",
									'true');

					var paletteItemId = $(this._lastDraggedItem).data("vme-itemid");
					if (paletteItemId !== undefined) 
					{
						var paletteItem = this.palette.getItem(paletteItemId);
						paletteItem.setOnCanvasAction(this._lastDraggedItem);
					}
					
					// IVAN auto populate form action
					if(paletteItemId == 'vme-form')
					{
						$(this._lastDraggedItem).attr('action', this.mashupParameters.actionUrl);
						$(this._lastDraggedItem).attr('data-binding_input', 'true');
					}
					
					// IVAN for materialize version
					$('.demo *').on('contextmenu', function(e){
						showContextMenu(e);
					});
					
					var itemIdMap = this.itemIdMap;
					
					// IVAN
					// set over helper
					$("[data-vme-itemid]").on('mouseover', function() {
						$("#helper-over").html(itemIdMap[$(this).attr("data-vme-itemid")]);
					});
				}
			},

			/**
			 * Metodo invocato per effettuare il bind degli eventi
			 */
			bindEvents : function() {
				// Rende draggable tutti gli elementi block
				$("[data-vme-dad='draggable'][data-vme-type='block']")
						.draggable(
								{
									connectToSortable : ".demo, .demo [data-vme-dad='sortable'][data-vme-type='block']",
									// helper : "clone",
									cursor: 'pointer',
									helper : this._dragHelper,
									revert : this._dragRevert,
									start : $.proxy(this._dragStart, this),
									stop : $.proxy(this._dragStop, this)
								});

				// Rende draggable tutti gli elementi block
				$("[data-vme-dad='draggable'][data-vme-type='inline']").draggable({
					connectToSortable : ".demo, .demo [data-vme-dad='sortable']",
					// helper : "clone",
					cursor: 'pointer',
					helper : this._dragHelper,
					revert : this._dragRevert,
					start : $.proxy(this._dragStart, this),
					stop : $.proxy(this._dragStop, this)
				});

				// NON ANCORA UTILIZZATA
				$("[data-target=#downloadModal]").click($.proxy(function(e) {
					e.preventDefault();
					this.downloadLayoutSrc();
				}, this));

				// NON ANCORA UTILIZZATA
				$("#download").click(function() {
					downloadLayout();
					return false;
				});
				// NON ANCORA UTILIZZATA
				$("#downloadhtml").click(function() {
					downloadHtmlLayout();
					return false;
				});

				$("#clear").click($.proxy(function(e) {
					e.preventDefault();
					this.clearDemo();
				}, this));

				$("#remove").click($.proxy(function(e) {
					e.preventDefault();
					this.removeSelectedItem();
				}, this));
				
				/*
				$("#uploadResources").click($.proxy(function(e) {
					$('#uploadModal').modal('show');
				}, this));
				*/

				$("#newHtml").click($.proxy(function(e) {
					$('#newFileModal').modal('show');
				}, this));
				
				$("#mockupConfiguration").click($.proxy(function(e) 
				{
					$.ajax({
						url : this.baseURL + "/user/gui/" + guieditor.projectId + "/getspainfo.json",
						type : 'GET',
						async: false,
						success : $.proxy(function(response)
						{//console.log(response);
							
							if(response.spaName == undefined || response.spaName == "")
								$("#appName").val("main");
							else
								$("#appName").val(response.spaName);
							
							if(response.pages.length >= 1)
							{
								var confContent = "";
								var pagename = "";
								var first = true;
								
								for(var i = 0; i < response.pages.length; i++)
								{				
									pagename = response.pages[i].filename.replace(".html.working","");
										
									if(response.pages[i].id == response.spaFirstPageId || (response.spaFirstPageId == 0 && first))
									{
										confContent += "<input type=\"radio\" name=\"firstPage\" value=\"" + response.pages[i].id + "\" checked />&nbsp;" + pagename;
										first = false;
									}
									else
										confContent += "<input type=\"radio\" name=\"firstPage\" value=\"" + response.pages[i].id + "\" />&nbsp;" + pagename;
									
									confContent += "<br>";
								}
								
								$("#firstPageLabel").show();
								$("#configurationPagesContainer").html(confContent);
							}
							else
							{
								$("#firstPageLabel").hide();
								$("#configurationPagesContainer").html("");
							}
							
						}, this)
					});
					
					$('#configurationModal').modal('show');
				}, this));
				
				$("#screenshot").click($.proxy(function(e) {
					//takeScreenshot();
					//$('#screenshotModal').modal('show');
					this.screenshotPage();
				}, this));

				$("#sourcepreview").click($.proxy(function() {
					this.savePage(); //autosave
					this.previewPage();
				}, this));

				$("#savefile").click($.proxy(function() {
					this.savePage();
				}, this));

				// Definisce il comportamento accordion like del palette container
				$("body")
						.on(
								"click",
								"#paletteContainer .nav-header",
								function() {
									$(
											"#paletteContainer .sidebar-nav .boxes, #paletteContainer .sidebar-nav .rows")
											.hide();
									// $(this).siblings(".boxes, .rows").hide()
									$(this).next().slideDown();
								});
				// Definisce il comportamento accordion like del panel container
				$("body").on("click", "#panelContainer .nav-header", function() {
					// $("#panelContainer .sidebar-nav .boxes, #panelContainer
					// .sidebar-nav .rows").hide();
					$(this).siblings(".boxes, .rows").hide();
					$(this).next().slideDown();
				});

				// Evita che gli elementi anchor dentro il canvas siano cliccabili
				// $(".demo").on("click", "a", function(e) {
				// e.preventDefault();
				// return false;
				// });

				// NON ANCORA UTILIZZATA
				$(document).on('hidden.bs.modal', function(e) {
					$(e.target).removeData('bs.modal');
				});

				// NON ANCORA UTILIZZATA
				$('body').on('click', '#continue-share-non-logged', function() {
					$('#share-not-logged').hide();
					$('#share-logged').removeClass('hide');
					$('#share-logged').show();
				});

				// NON ANCORA UTILIZZATA
				$('body').on('click', '#continue-download-non-logged', function() {
					$('#download-not-logged').hide();
					$('#download').removeClass('hide');
					$('#download').show();
					$('#downloadhtml').removeClass('hide');
					$('#downloadhtml').show();
					$('#download-logged').removeClass('hide');
					$('#download-logged').show();
				});

				$('#structurehelper').on('change', function() {
					$(".demo").toggleClass("structurehelper");
				});
				
				$('#databindhelper').on('change', function() {
					$(".demo").toggleClass("databindhelper");
				});					
				
				$("body").on('shown.bs.tab', 'a[data-toggle="tab"]', $.proxy(function (e) {
					  //e.target // newly activated tab
					  //e.relatedTarget // previous active tab
					
					this.updateInputBindingStatus();
				}, this));
				
			},

			/*
			 * Bindind dell'evento click su tutti gli elementi contenuti nel div di
			 * classe demo
			 */
			enableComponentSelection : function(tabId) {
				// Blocca la selezione dei figli di un componente avente data
				// attribute childrenlock a true
				$("#" + tabId + ".demo").on("click",
						"[data-vme-childrenlock='true'] *", function(e) {
							e.preventDefault();
							e.stopPropagation();
						});

				$("#" + tabId + ".demo").on("click",
						":not([data-vme-childrenlock='true'] *)",
						$.proxy(function(e) {
							// Ferma la propagazione dell'evento agli elementi parent
							// Se si clicca quindi su un div figlio l'evento non sarà
							// propagato
							// al div che lo contiene
							e.preventDefault();
							e.stopPropagation();
							
							try {
								// Crea un pannello delle proprietà
								var panel = new Panel(e.target);

								this.selectedItem = e.target;
								this.currentPanel = panel;

								// Rimuove il bordo di selezione da tutti gli elementi
								$('.demo *').removeAttr('data-vme-boxshadow');

								// Inserisce il bordo di selezione sull'elemento
								// cliccato
								$(e.target).attr('data-vme-boxshadow', 'boxshadow');
								
								// IVAN
								// set selected helper
								$("#helper-selected").html(this.itemIdMap[$(e.target).attr("data-vme-itemid")]);

							} catch (err) {
								console.log(err);
							}
						}, this));

			},

			getTemplateManager : function() {
				return this.templateManager;
			},

			configurationElm : function(e, t) {
				$(".demo").delegate(".configuration > a", "click", function(e) {
					e.preventDefault();
					var t = $(this).parent().next().next().children();
					$(this).toggleClass("active");
					t.toggleClass($(this).attr("rel"));
				});
				$(".demo").delegate(".configuration .dropdown-menu a", "click",
						function(e) {
							e.preventDefault();
							var t = $(this).parent().parent();
							var n = t.parent().parent().next().next().children();
							t.find("li").removeClass("active");
							$(this).parent().addClass("active");
							var r = "";
							t.find("a").each(function() {
								r += $(this).attr("rel") + " ";
							});
							t.parent().removeClass("open");
							n.removeClass(r);
							n.addClass($(this).attr("rel"));
						});
			},

			removeElm : function() {
				$(".demo").delegate(".remove", "click", function(e) {
					e.preventDefault();
					$(this).parent().remove();
					if (!$(".demo .lyrow").length > 0) {
						clearDemo();
					}
				});
			},

			clearDemo : function() {
				$(".demo").empty();
			},

			changeStructure : function(e, t) {
				$("#download-layout ." + e).removeClass(e).addClass(t);
			},

			cleanHtml : function(e) {
				$(e).parent().append($(e).children().html());
			},

			downloadLayoutSrc : function() {
				var e = "";
				$("#download-layout").children().html($(".demo").html());
				var t = $("#download-layout").children();
				t.find(".preview, .configuration, .drag, .remove").remove();
				t.find(".lyrow").addClass("removeClean");
				t.find(".box-element").addClass("removeClean");
				t.find(".lyrow .lyrow .lyrow .lyrow .lyrow .removeClean").each(
						$.proxy(function(i, element) {
							this.cleanHtml(element);
						}, this));
				t.find(".lyrow .lyrow .lyrow .lyrow .removeClean").each(
						$.proxy(function(i, element) {
							this.cleanHtml(element);
						}, this));
				t.find(".lyrow .lyrow .lyrow .removeClean").each(
						$.proxy(function(i, element) {
							this.cleanHtml(element);
						}, this));
				t.find(".lyrow .lyrow .removeClean").each(
						$.proxy(function(i, element) {
							this.cleanHtml(element);
						}, this));
				t.find(".lyrow .removeClean").each($.proxy(function(i, element) {
					this.cleanHtml(element);
				}, this));
				t.find(".removeClean").each($.proxy(function(i, element) {
					this.cleanHtml(element);
				}, this));
				t.find(".removeClean").remove();
				// $("#download-layout .column").removeClass("ui-sortable");
				$("#download-layout .row-fluid").removeClass("clearfix").children()
						.removeClass("column");
				if ($("#download-layout .container").length > 0) {
					this.changeStructure("row-fluid", "row");
				}
				formatSrc = $.htmlClean($("#download-layout").html(), {
					format : true,
					allowedAttributes : [ [ "id" ], [ "class" ], [ "data-toggle" ],
							[ "data-target" ], [ "data-parent" ], [ "role" ],
							[ "data-dismiss" ], [ "aria-labelledby" ],
							[ "aria-hidden" ], [ "data-slide-to" ], [ "data-slide" ] ]
				});
				$("#download-layout").html(formatSrc);
				$("#downloadModal textarea").empty();
				$("#downloadModal textarea").val(formatSrc);
			},

			saveLayout : function() {
				/*
				 * $.ajax({ type : "POST", url : "/build_v3/saveLayout", data : {
				 * 'layout-v3' : $('.demo').html() }, success : function(data) {
				 * //updateButtonsVisibility(); } });
				 */
			},

			downloadLayout : function() {
				$.ajax({
					type : "POST",
					url : "/build_v3/downloadLayout",
					data : {
						'layout-v3' : $('#download-layout').html()
					},
					success : function(data) {
						window.location.href = '/build_v3/download';
					}
				});
			},

			downloadHtmlLayout : function() {
				$.ajax({
					type : "POST",
					url : "/build_v3/downloadLayout",
					data : {
						'layout-v3' : $('#download-layout').html()
					},
					success : function(data) {
						window.location.href = '/build_v3/downloadHtml';
					}
				});
			},

			undoLayout : function() {

				$.ajax({
					type : "POST",
					url : "/build_v3/getPreviousLayout",
					data : {},
					success : function(data) {
						undoOperation(data);
					}
				});
			},

			redoLayout : function() {

				$.ajax({
					type : "POST",
					url : "/build_v3/getPreviousLayout",
					data : {},
					success : function(data) {
						redoOperation(data);
					}
				});
			},

			savePage : function() {
				var successSavedFilenames = $("<ul>");				
				
				var count = 0;
				
				$.each(this.editWindows, function(i, elem) {
					var filename = elem.saveContent();//console.log("GuiEditor savePage: "+ filename);
					
					successSavedFilenames.append("<li>" + filename + "</li>");
					
					count++;
				});
				
				// IVAN
				// build project as a Single Page Application (SPA)
				
				// create the HTML wrapper
				var mainPage = this.getHTMLWrapper();
				
				// write file
				$.ajax({
		            url: this.baseURL + "/user/gui/" + this.projectId + "/spa/save",
		            global: false,
		            type: 'POST',
		            data: {
							"htmlWrapper" : $(mainPage)[0].outerHTML
						},
		            async: false, //blocks window close
		            success: function()
		            {
		            	// IVAN for materialize version
						var toastContent = $('<span class="toastSuccess">Project saved</span>');
						Materialize.toast(toastContent, 2000);
						$(".toastSuccess").parent().addClass("toast-success");
		            }
				});			
				
				//bootbox.alert("<h4 class=\"text-success\">File saved successfully!<h4>" + successSavedFilenames.html());
			},

			previewPage : function() {
				$.each(this._projectHtml, $.proxy(function(index, elem) {
					// Se il file html è aperto in edit invoca la preview
					var editor = this.editWindows["tab" + elem.id];
					if (editor !== undefined) {

						// La preview dei file html non di default avviene in modalità
						// quietly
						editor.previewPage(elem.isdefault ? false : true);
					}
				}, this));
			},
			// IVAN
			screenshotPage : function() {
				$.each(this._projectHtml, $.proxy(function(index, elem) {
					// Se il file html è aperto in edit invoca la preview
					var editor = this.editWindows["tab" + elem.id];
					if (editor !== undefined) {

						// La preview dei file html non di default avviene in modalità
						// quietly
						editor.screenshotPage();
					}
				}, this));
			},
			// IVAN
			getHTMLWrapper: function()
			{
				var linkedJs = this.getLinkedJs();
			    
			    var filename = "";
			    
			    var configPaths = this.getRequirejsPaths();

			    configPaths.shim={};
			    $.each(linkedJs, function(index, elem)
			    {
			     filename = elem.filename;
			     filename = filename.substring(0, filename.lastIndexOf("."));
			    // configPaths.paths.shim["prjjs/"+filename]="{deps:['bootstrap']}";  
			     configPaths.shim["prjjs/"+filename]=new Object();
			     configPaths.shim["prjjs/"+filename].deps=new Array();
			     //configPaths.shim["prjjs/"+filename].deps.push('bootstrap');
			    });
			    //var scriptForImport = "";
			    var scriptForImport = "require.config("+JSON.stringify(configPaths)+");\n";
			    
			    var linkedCss = this.getLinkedCss();
			    
			    $.each(linkedCss, function(index, elem)
			    {
			     //scriptForImport += '<link href="css/' + elem.filename + '" rel="stylesheet" type="text/css">';
			     scriptForImport += "requirejs(['css!prjcss/" + elem.filename + "']);\n";      
			    });
			    
			   
			    scriptForImport += "require(['main'], function() {\n";
			    $.each(linkedJs, function(index, elem)
			    {
			     //scriptForImport += '<script type="text/javascript" src="js/' + elem.filename + '"></script>';
			     
			     filename = elem.filename;
			     filename = filename.substring(0, filename.lastIndexOf("."));
			     scriptForImport += "requirejs(['prjjs/" + filename + "'],function(" + filename + "){window." + filename + " = " + filename + ";});\n";      
			    });     
			    scriptForImport += "});\n";
			    
			    var importScript = "<script>"+scriptForImport+"</script>"; 
			    
			    var head = $("<head>")
			    .append("<script data-main='"+this.getFullUrl()+"/guieditor/project_template/js/main' src='"+this.getFullUrl()+"/guieditor/project_template/js/lib/require.js'></script>")
			    // IVAN for materialize version
			    .append('<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">')
			    .append('<link type="text/css" rel="stylesheet" href="' + this.getFullUrl() + '/resources/js/materialize/css/materialize.min.css" media="screen,projection"/>')
			    .append(this.spinnerStyle);
			    
			    //.append("<link href='//netdna.bootstrapcdn.com/bootstrap/3.0.1/css/bootstrap.min.css' rel='stylesheet'>")
			    //.append("<link href='//maxcdn.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css' rel='stylesheet'>");
			    //.append(scriptForImport); 
			    
			    var content = $("<html>")
			    .append(head)
			    .append($("<body>")
			      .append(importScript)
			      .append($("<div class='container'>")));
			    
			    //content = "<!DOCTYPE html>" + content;
			    
			    return content;
			},
			
			// IVAN for materialize version
			editElementHTML: function() {

				// Prende l'html del tag relativo al pannello delle proprietà
				// corrente
				var html = $(this.selectedItem).html();
				
				// Effettua un trim ed un beatify del codice html
				var prettyHtml = vkbeautify.xml(html.trim());

				// Inietta l'html nell'editor Ace
				this.getHtmlEditor().session.setValue(prettyHtml);

			},
			saveElementHTML: function() {
				// Prende il codice HTML dall'Ace editor
				var html = this.getHtmlEditor().session.getValue();

				// Lo inietta nel tag relativo al pannello delle proprietà
				// corrente
				$(this.selectedItem).html(html);

				// Pulisce l'editor Ace
				this.getHtmlEditor().session.setValue(" ");
			},
			editElementStyle: function() {
				// Crea un pannello delle proprietà
				var panel = new Panel(this.selectedItem);

				this.currentPanel = panel;
			},
			
			fetchData : function(inputField) {
				this._tmpSourceField = inputField;
				// IVAN for materialize version
				$('#invokeWorkflow').openModal();
			},
									
			fetchActionData : function(inputField) {
				this._tmpSourceField = inputField;
				
				var inputGroup = $(this._tmpSourceField).closest("div.input-group");
				var inputField = $(inputGroup).children("input");
				
				$(inputField).val(this.mashupParameters.actionUrl);

				$(inputField).trigger("change");

				var htmlTag = this.currentPanel.getTag();
				$(htmlTag).attr("data-binding_input", true);		
				
				this.updateInputBindingStatus();
			},
			// IVAN modified for new version
			fetchInputData : function(inputField) {
				this._tmpSourceField = inputField;
																																			
				var template = Handlebars.compile(this.inputTable);
				var element = template(this.mashupParameters);

				$("#inputparamstreecontainer").html(element);
				
				var currentInput = $(inputField).attr("name");
				
				if(currentInput != undefined && currentInput != "")
					$("#inputparamstreecontainer").find("input[id='" + currentInput + "']").attr('checked', true);
				
				$('#bindingInputParameter').find('#setInputOk').off('click');// Reset di un precedente event bind
				$('#bindingInputParameter').find('#setInputOk').on('click',
						$.proxy(function(e) {
							
							e.preventDefault();
							// Prende il campo di databind e lo popola con la
							// path catturata dal tree
							//var inputGroup = $(this._tmpSourceField).closest("div.input-group");
							//var inputField = $(inputGroup).children("input");
							
							// get selected input parameter
							var selectedOption = $("#inputParamTable").find("input[name='selection']:checked").val();
							
							if(selectedOption != undefined)
							{
								selectedOption = selectedOption.trim();
								$(this._tmpSourceField).attr("name", selectedOption);
								$(this._tmpSourceField).attr("data-binding_input", true);
							}
							else
							{
								$(this._tmpSourceField).removeAttr("name");
								$(this._tmpSourceField).removeAttr("data-binding_input");
							}
							
							//$(inputField).val(selectedItem.trim());
							//$(inputField).trigger("change");

							//var htmlTag = this.currentPanel.getTag();
							//$(htmlTag).attr("data-binding_input", true);
							
							// IVAN for materialize version
							$("#bindingInputParameter").closeModal();
							
							this.updateInputBindingStatus();
						}, this));
				
				$('#bindingInputParameter').find('#setInputClear').off('click');// Reset di un precedente event bind
				$('#bindingInputParameter').find('#setInputClear').on('click',
						$.proxy(function(e) {
							e.preventDefault();
							
							$("#inputparamstreecontainer").find("input[type='radio']").attr('checked', false);
							
						}, this));				
				
				// IVAN for materialize version
				$('#bindingInputParameter').openModal();
						
			},			
			
			updateInputBindingStatus : function(){	
				if (this.mashupProjectId != undefined) {
   				//prende l'id del tab attivo				
   				var tabId = $("ul#canvastabsystem").siblings("div.tab-content").find("div.tab-pane.active").attr("id");
   				
   				var statusList="";
   				var count = 0;
   				
   				var inputParams = this.mashupParameters.inputParams;
   				
   				$.each(inputParams , $.proxy(function(i, elem){
   					var input = $("#"+tabId).find("input[name='"+elem.inputName+"'][data-binding_input='true']");
   					if(input.length>0){
   						statusList+="<li><i class=\"btn-xs glyphicon glyphicon-ok inputbind complete\"></i> "+elem.inputName +" | "+ elem.label+"</li>";
   						count++;
   					} else {
   						statusList+="<li><i class=\"btn-xs glyphicon glyphicon-remove inputbind nothing\"></i> "+elem.inputName +" | "+ elem.label+"</li>";
   					}					
   				}, this));
   				
   				var formBound = false;
   				var form = $("#"+tabId).find("form[data-binding_input='true']");
   				if(form.length>0){
   					statusList+="<li><i class=\"btn-xs glyphicon glyphicon-ok inputbind complete\"></i> form action</li>";
   					formBound = true;
   				} else {
   					statusList+="<li><i class=\"btn-xs glyphicon glyphicon-remove inputbind nothing\"></i> form action</li>";
   				}	
   				
   				$("#bindstatusoptions").html(statusList);
   				
   				$("#bindstatuscolor").removeClass("complete").removeClass("inprogress").removeClass("nothing");
   				
   				if(inputParams.length==count && formBound){
   					$("#bindstatuscolor").addClass("complete");
   					
   				} else if(count == 0 && !formBound){
   					$("#bindstatuscolor").addClass("nothing");
   					
   				} else {
   					$("#bindstatuscolor").addClass("inprogress");
   					
   				}
				}
			},
			
			_fetchMashupParameters : function(callback){
				//if (this.mashupProjectId != undefined) {
					$.ajaxSetup({
						async : false
					});
					$.ajax({
						url : this.baseURL + "/user/gui/" + this.projectId
								+ "/getallbindinginputparams.json",
						type : 'POST',
						success : callback
					});
					$.ajaxSetup({
						async : true
					});
				//}
			},
			
			buildInvokeWorkflowWindow : function() {					
					
					this._fetchMashupParameters(
							$.proxy(function(data) {
								this.mashupParameters = data;																
							}, this)
					);										
					
					//if (this.mashupProjectId != undefined) {
					//Crea il form per l'invocazione del mashup					
					$.ajax({
						url : this.baseURL + "/user/gui/" + this.projectId
								+ "/buildinputform",
						type : 'GET',
						success : $.proxy(function(response) {
							
							var container = $('#invokeWorkflow');
							container.find('#invokeWorkflowContainer').html(response); //console.log(response);

							// container.find('.btn-primary').off('click');//Reset di
							// un precedente event bind
							container.find('#invokeWorkflowOk').on('click',
									$.proxy(function(e) {
										e.preventDefault();
										this._invokeWorkflow();

									}, this));

							container.find('input').on('keypress',
									$.proxy(function(e) {
										if (e.which == 13) {
											e.preventDefault();
											this._invokeWorkflow();
										}
									}, this));
						}, this)
					});
					//}
			},
			parentBindFullPathRebuild : function(domElement, suffix) {
				var fatherNodePath = suffix;

				var parentBindedElement = $(domElement).parents(
						':attrNameStart(data-bind)');

				if (parentBindedElement !== undefined
						&& parentBindedElement.length > 0) {

					var path = this.parentBindFullPathRebuild(parentBindedElement,
							fatherNodePath);

					if ($(parentBindedElement).attr('data-bind-foreach') !== undefined) {
						fatherNodePath = $(parentBindedElement).attr(
								'data-bind-foreach');
					} else {
						fatherNodePath = $(parentBindedElement)
								.attr('data-bind-with');
					}

					fatherNodePath = path != null && path != '' ? path + "."
							+ fatherNodePath : fatherNodePath;
				}

				return fatherNodePath;
			},

			_invokeWorkflow : function() {
				
				// IVAN
				//var domElement = this.currentPanel.getTag();
				var domElement = this.selectedItem;

				var parametersString = $("#invokeworkflowparams").serialize();

				var fullPath = this.parentBindFullPathRebuild(domElement, "");

				if (fullPath != null && fullPath != '') {
					parametersString += "&parentNodePath=" + fullPath;
				}
				
				$("#spinner_layer").show();

				$.ajax({
					url : this.baseURL + "/user/gui/" + this.projectId
							+ "/invokeemml.json",
					type : 'POST',
					data : parametersString,
					// contentType: 'application/json; charset=UTF-8',
					success : $.proxy(function(data) {
						
						$("#spinner_layer").hide();
						this._buildTreePanel(data);
					}, this)
				});
			},
			_buildTreePanel : function(data) {
				
				//console.log('jstree data', data);
				
				// IVAN get selected item (on canvas) datatype
				var datatype = $(this.selectedItem).data("datatype");
				var selectedVmeItemId = $(this.selectedItem).data("vme-itemid");
				$("#treeModalSelectedItemDataType").html(datatype);
				
				var icon = "";
				var parentNodeJson;
				
				if(datatype == "text")
					icon = '(&nbsp;<i class="fa icon-file-text" style="color:#558b2f"></i>&nbsp;)';
				else if(datatype == "object")
					icon = '(&nbsp;<i class="fa icon-folder-close" style="color:#558b2f"></i>&nbsp;)';
				else if(datatype == "array")
					icon = '(&nbsp;<i class="fa icon-th-list" style="color:#558b2f"></i>&nbsp;)';
				
				$("#treeModalSelectedItemDataTypeIcon").html(icon);
				
				$("#treecontainer").jstree({
					"json_data" : {
						data : data
					},
					"plugins" : [ "themes", "json_data", "ui", "search"]
				})
				.bind("loaded.jstree", function (e, data) {
					$(".fa").removeClass("jstree-icon");
					
					$(".fa").css("color", "#bdbdbd");
					
					if(datatype == "text")
						$(".icon-file-text").css("color", "#558b2f");
					else if(datatype == "object")
						$(".icon-folder-close").css("color", "#558b2f");
					else if(datatype == "array")
						$(".icon-th-list").css("color", "#558b2f");
					
					if(selectedVmeItemId == "vme-gmap")
					{
						$('#treecontainer').jstree('search', 'lat');
						$('#treecontainer').jstree('search', 'lng');
					}
					
					$("#treecontainer").jstree('open_all');
				})
				.bind("search.jstree", function (e, data) {
					
					for(var i = 0; i < data.rslt.nodes.length; i++)
					{
						$(data.rslt.nodes[i]).parent().parent().parent().children("a").first().find("ins").css("color", "#558b2f");
					}
				})
				.bind("select_node.jstree", function(event, data) {
					
					try
					{
						parentNodeJson = $("#treecontainer").jstree('get_json', data.inst._get_parent())[0];
					}
					catch(ex)
					{
						parentNodeJson = undefined;
					}
				});				
				
				// IVAN for materialize version
				$('#invokeWorkflow').closeModal();
				
				$("#treeModal").openModal();

				$("#treeModalOk").off('click');// Reset di un precedente event bind
				$("#treeModalOk").on('click',
					$.proxy(
						function() {
							// Prende il campo di databind e lo
							// popola con la path catturata dal tree
							//var inputGroup = $(this._tmpSourceField).closest("div.input-group");
							//var datatype = $(inputGroup).children("input").data("datatype");

							var selectedItem = $("#treecontainer").jstree('get_selected');
							var selectedItemDatatype = $(selectedItem).data('datatype');
							
							// IVAN
							// allow binding with numerical or boolean values
							if(selectedItemDatatype == undefined)
								selectedItemDatatype = "text";
							
							var datatypeArr = datatype.split('||');
							
							//console.log("path", $(selectedItem).data('path'));
							
							var path = path = $(selectedItem).data('path');
							
							if(path == undefined)
								path = $(selectedItem).find("a").get(0).innerText.split(":")[0].trim();
							
							var isMap = false;
							
							// manage gmap case
							if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-gmap')
							{
								isMap = true;
								
								// first check if selected path contains lat and lng keys
								
								var lat = false;
								var lng = false;
								var position = false;
								
								try
								{
									var treeJSON = $("#treecontainer").jstree('get_json');
									
									var treeChildren = treeJSON[0].children;							
									
									for(var i = 0; i < treeChildren.length; i++)
									{
										if(treeChildren[i].data.title.split(":")[0].trim() == "lat" || treeChildren[i].data.title.split(":")[0].trim() == "latitude")
											lat = true;
										
										if(treeChildren[i].data.title.split(":")[0].trim() == "lng" || treeChildren[i].data.title.split(":")[0].trim() == "longitude")
											lng = true;
										
										if(treeChildren[i].data.title.split(":")[0].trim() == "position")
											position = true;
										
										if((lat && lng) || position)
											break;
									}
								}
								catch(ex)
								{
									
								}
								
								if(!((lat && lng) || position))
								{
									var toastContent = $('<span class="toastError">Selected path doesn\'t contain lat and lng or position.</span>');
									Materialize.toast(toastContent, 2000);
									$(".toastError").parent().addClass("toast-error");
									
									return false;
								}
								
								var coordspath = "";
								
								// if user selected 'multiple markers', set path to parent array
								if(this.markerType == 'array')
								{
									var coordspathIndex = path.indexOf("]", path.lastIndexOf("()")) + 1;			
									
									var keysArray = eval(path.substring(coordspathIndex));									
									
									if(keysArray)
									{
										coordspath += keysArray[0];
										
										for(var i = 1; i < keysArray.length; i++)
										{
											coordspath += keysArray[i];
										}				
									}
									
									path = path.substring(0, path.lastIndexOf("()") + 2);
								}
								
								$(guieditor.selectedItem).attr('coordpath', coordspath);
								
								// IVAN 
								// escape di -
								if(path != undefined && path.indexOf("-") > -1)
								{
									var end = path.indexOf("[");
									
									path = "$data['" + path.substring(0, end) + "']" + path.substring(end, path.length);
								}
								
								if(this.markerType == "object")
								{
									valBind = "ko.toJSON(" + path + ")";
									path = valBind;
								}
								
								$(this.selectedItem).attr('data-bind-' + $(this.selectedItem).data('databind'), "{markers:" + path + "}");
								
								// get element fields for next step
								
								var mapInfoWindowFields = [];
								
								if(parentNodeJson != undefined)
								{
									var fields;
									
									if(parentNodeJson.metadata.datatype != "array")
									{
										fields = parentNodeJson.children;
									}
									else
									{
										fields = parentNodeJson.children[0].children;
									}
									
									for(var i = 0; i < fields.length; i++)
									{
										if(fields[i].metadata.datatype == "array")
											continue;
										
										if(fields[i].children == undefined)
										{
											mapInfoWindowFields.push(fields[i].data.title.split(":")[0].trim());
										}
										else
										{
											var subFields = fields[i].children;
											var parentPath = fields[i].data.title.trim();
											
											for(var j = 0; j < subFields.length; j++)
											{
												mapInfoWindowFields.push(parentPath + "." + subFields[j].data.title.split(":")[0].trim());
											}
										}
									}
								}	
								
								var content = '<form action="#" class="row">';

								var options = "";
								var tmp;
								
								for(var i = 0; i < mapInfoWindowFields.length; i++)
								{
									tmp = mapInfoWindowFields[i].split(".");
									
									options += '<option value="' + mapInfoWindowFields[i] + '">'
										+ tmp[0] + (tmp.length == 2 ? ' > ' + tmp[1] : '') + '</option>';
								}
								
								content += '<div class="input-field col s12">'
									+ '<select id="mapInfoWindowPrimary">'
									+ '<option value="" disabled selected>' + Message['general.messages.choose_option'] + '</option>'
									+ options
									+ '</select>'
									+ '<label>' + Message['projects.wizard.labels.primary_field'] + '</label>'
									+ '</div>';
								
								content += '<div class="input-field col s12">'
									+ '<select id="mapInfoWindowSecondary">'
									+ '<option value="" disabled selected>' + Message['general.messages.choose_option'] + '</option>'
									+ options
									+ '</select>'
									+ '<label>' + Message['projects.wizard.labels.secondary_field'] + '</label>'
									+ '</div>';
								
								content += '</form>';
								
								$("#panelMapSelectDataModalContainer").html(content);
								
								$('select').material_select();

								$("#treeModal").closeModal();
								$("#mapSelectDataModal").openModal();								
							}
							else if($(guieditor.selectedItem).attr('data-vme-itemid') == 'vme-table')
							{
								if(selectedItemDatatype != "array")
								{
									var toastContent = $('<span class="toastError">' + Message['mockup.messages.not_compatible_type'] + ' "array"</span>');
									Materialize.toast(toastContent, 2000);
									$(".toastError").parent().addClass("toast-error");
									
									return;
								}
								
								var tbody = $(guieditor.selectedItem).find("tbody");
								
								// IVAN 
								// escape di -
								if(path != undefined && path.indexOf("-") > -1)
								{
									var end = path.indexOf("[");
									
									path = "$data['" + path.substring(0, end) + "']" + path.substring(end, path.length);
								}
								
								//console.log('path', path);
								
								$(tbody).attr('data-bind-' + $(tbody).data('databind'), path);
								
								$("#treeModal").closeModal();
								
								// get keys from array element
								var treeJSON = $("#treecontainer").jstree('get_json');								
								var treeChildren = treeJSON[0].children;
								var fields = treeChildren[0].children;//console.log("treeChildren", fields);
								
								var content = '<form action="#" id="checkboxSortable" class="row">';
								
								for(var i = 0; i < fields.length; i++)
								{
									if(fields[i].metadata.datatype == "array")
										continue;
									
									if(fields[i].children == undefined)
									{
										content += '<p class="col"><input type="checkbox" id="checkbox_' + i + '" checked="checked" value="' + fields[i].data.title.split(":")[0].trim() + '" /><label for="checkbox_' + i + '" class="checkbox-label">' + fields[i].data.title.split(":")[0].trim() + '</label></p>'
									}
									else
									{
										var parentPath = fields[i].data.title.split(":")[0].trim();
										
										var subFields = fields[i].children;
										
										for(var j = 0; j < subFields.length; j++)
										{
											content += '<p class="col"><input type="checkbox" id="checkbox_' + i + "_" + j
											+ '" checked="checked" value="' 
											+ parentPath + '.' + subFields[j].data.title.split(":")[0].trim() + '" /><label for="checkbox_' + i + "_" + j + '" class="checkbox-label">' 
											+ parentPath + ' > ' + subFields[j].data.title.split(":")[0].trim() + '</label></p>';
										}
									}
								}
								
								content += '</form>';
								
								$("#panelTableSelectDataModalContainer").html(content);
								$("#checkboxSortable").sortable();
							    $("#checkboxSortable").disableSelection();
							      
								$("#tableSelectDataConfirmModal").openModal();
							}							
							else
							{
								if (datatypeArr.indexOf(selectedItemDatatype) >= 0) 
								{
									/*
									path = $(selectedItem).data('path');
									
									if(path == undefined)
										path = $(selectedItem).find("a").get(0).innerText.split(":")[0].trim();
									*/
									// IVAN 
									// escape di -
									if(path != undefined && path.indexOf("-") > -1)
									{
										var end = path.indexOf("[");
										
										path = "$data['" + path.substring(0, end) + "']" + path.substring(end, path.length);
										//console.log(path);
									}
	
									// Rimpiazza il "$." per sottostare
									// alla sintassi per il binding di
									// knockout
									// path = path.replace(/^\$\./, "");
									/*
									var inputGroup = $(this._tmpSourceField).closest("div.input-group");
	
									// Prende il campo corrispondente alla
									// proprietà che si sta bindando e lo
									// disabilita
									var inputParamField = $(inputGroup).siblings("input");
									$(inputParamField).val("");
									$(inputParamField).prop('readonly', true);
									$(inputParamField).trigger("change");*/
	
									// Prende il campo di databind e lo
									// popola con la path catturata dal
									// tree
									
									//var inputParamDataField = $(inputGroup).children("input");
									//var valBind = "";
									
									if(selectedItemDatatype == "object")
									{
										valBind = "ko.toJSON(" + path + ")";
										path = valBind;
									}
									
									//inputParamDataField.val(path);
									//$(inputParamDataField).trigger("change");
									
									$(this.selectedItem).attr('data-bind-' + $(this.selectedItem).data('databind'), path);
	
									$("#treeModal").closeModal();
								} else 
								{
									var toastContent = $('<span class="toastError">Selected type is not compatible with expected type ' + datatype + '</span>');
									Materialize.toast(toastContent, 2000);
									$(".toastError").parent().addClass("toast-error");
								}	
							}
							
						}, this));
			},
			cleanInputData : function(btn) {
				this.cleanData(btn);
				
				var htmlTag = this.currentPanel.getTag();
				$(htmlTag).removeAttr("data-binding_input");	
				
				this.updateInputBindingStatus();
			},
			cleanData : function(btn) {
				var inputGroup = $(btn).closest("div.input-group");

				// Prende il campo corrispondente alla proprietà che si sta bindando
				// e lo disabilita
				var inputParamField = $(inputGroup).siblings("input");
				$(inputParamField).prop('readonly', false);

				// Prende il campo di databind e lo popola con la path catturata dal
				// tree
				var inputParamDataField = $(inputGroup).children("input");
				inputParamDataField.val("");
				$(inputParamDataField).trigger("change");
			},
			removeSelectedItem : function() {
				if (this.selectedItem !== undefined) {
					$(this.selectedItem).remove();
				}
			},
			addResource : function(event) {
				event.preventDefault();
				var form = new FormData(document
						.getElementById('uploadResourceForm'));
				// var form = event.target;
				// var action = $(form).attr('action');
				// var postParams = $(form).serialize();
				$.ajax({
					url : this.baseURL + "/user/gui/" + this.projectId	+ "/addresource/file",
					data : form,
					dataType : 'text',
					processData : false,
					contentType : false,
					type : 'POST',
					success : $.proxy(function(response) {
						var data = jQuery.parseJSON(response);
						this.updateResourceTable(data);

					}, this)
				});
			},
			// IVAN for materialize version
			updateResourceTable : function(data) {
				if (data.contentType == "text/css") {
					this._projectCss.push(data);
					requirejs([ 'css!prjcss/' + data.filename ]);
				} else if (data.contentType == "application/javascript") {
					this._projectJs.push(data);
				} else if (data.contentType == "text/html") {
					this._projectHtml.push(data);
				}

				var content = '<li class="collection-item"><div><strong>' + data.filename + '</strong> [' + data.contentType + ']';
				
				content += '<a href="#!" class="secondary-content projectFilesActions">';
				
				if (data.contentType === "application/javascript" || data.contentType === "text/css" || (data.contentType === "text/html" && !data.isdefault)) 
				{
					content += '<i class="material-icons" onclick="guieditor.openResource(this, '+ data.id + ')">edit</i>';
				}
				
				content += '<i class="material-icons" onclick="guieditor.removeResource(this, '+ data.id + ')">delete</i>';
				content += '</a></div></li>';
				
				$("#resourceTable").append(content);
			},

			removeResource : function(element, resourceId) {
				var tabId = "tab" + resourceId;
				if (this.editWindows[tabId] === undefined) {
					$
							.ajax(
									{
										url : this.baseURL + "/user/gui/" + this.projectId + "/removeresource/" + resourceId + "/file",
										type : "GET"
									})
							.done(
									$.proxy(
										function() {
											$(element).closest("li").remove();
	
											for (var i = 0; i < this._projectCss.length; i++) {
												if (this._projectCss[i].id == resourceId) 
												{//console.log(resourceId);console.log(this._projectCss[i]);
													
													requirejs.undef('css!prjcss/'+ this._projectCss[i].filename);
													
													//$("head link[href$='" + this._projectCss[i].filename	+ "']").remove();
													
													this._projectCss.splice(i, 1);
													i--;
												}
											}
	
											for (var i = 0; i < this._projectJs.length; i++) {
												if (this._projectJs[i].id == resourceId) {
													this._projectJs.splice(i, 1);
													i--;
												}
											}
	
											for (var i = 0; i < this._projectHtml.length; i++) {
												if (this._projectHtml[i].id == resourceId) {
													this._projectHtml.splice(i, 1);
													i--;
												}
											}
										}, this));
				} 
				else 
				{
					bootbox.alert("You cannot remove in editor opened files!");
				}
			},
			addHtmlFile : function(event) {
				event.preventDefault();
				var form = new FormData(document.getElementById('newHtmlForm'));
				$.ajax({
					url : this.baseURL + "/user/gui/" + this.projectId
							+ "/newhtml/file",
					data : form,
					dataType : 'text',
					processData : false,
					contentType : false,
					type : 'POST',
					success : $.proxy(function(response) {
						var data = jQuery.parseJSON(response);
						this.updateResourceTable(data);
						$('#newFileModal').modal('hide');
					}, this)
				});
			},
			openResource : function(element, resourceId) {
				var tabId = "tab" + resourceId;
				
				if (this.editWindows[tabId] === undefined) {
					$.each(this._projectCss, $.proxy(function(index, elem) {
						if (elem.id == resourceId) {
							var editWin = new EditWindow.CssEditWindow(elem.id,
									elem.filename, elem.contentType, this.projectId);

							this.editWindows[tabId] = editWin;
						}
					}, this));

					$.each(this._projectJs, $.proxy(function(index, elem) {
						if (elem.id == resourceId) {
							
							var editWin = new EditWindow.JsEditWindow(elem.id, elem.filename, elem.contentType, this.projectId);

							this.editWindows[tabId] = editWin;
						}
					}, this));

					$.each(this._projectHtml, $.proxy(function(index, elem) {
						if (elem.id == resourceId) {
							var editWin = new EditWindow(elem.id, elem.filename,
									elem.contentType, elem.isdefault, this.projectId,
									this.projectName);

							this.editWindows[tabId] = editWin;
						}
					}, this));		

				} else {
					bootbox.alert("File is already opened in edit!");
				}
			},
			closeTab: function(tabId) {
				// delete editWindow
				delete this.editWindows[tabId];
				
				// remove tab
				var tabs = $("#canvastabsystem.tabs");
				
				$(tabs).find("li a[href='#editor_" + tabId + "']").parent().remove();
				$("#editor_" + tabId).remove();
				
				$(tabs).tabs();
				$(tabs).tabs('select_tab', Object.keys(this.editWindows)[0]);
			},
			ajaxErrorHandling : function(event, jqXHR, settings, exception) {
				event.preventDefault();

				var response;
				try {
					response = JSON.parse(jqXHR.responseText);
				} catch (err) {
					response = {
						message : jqXHR.statusText,
						stackTrace : ''
					};
				}

				if (jqXHR.status == 401 && "redirectUrl" in response) {// unauthorized
																							// error
																							// code
					guieditor.oauthAuthenticate(event, response.redirectUrl, null);
				} else {

					var panel = "<div>" + "<h4 class=\"text-info\"><strong>"
							+ response.message + "</strong></h4>";

					if (response.extendedMessage !== undefined
							&& response.extendedMessage !== '') {
						var extendedMessage = response.extendedMessage;

						panel += parseExtendedErrorMsg(extendedMessage) + "<br/>";
					}

					panel += "<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\" class=\"text-danger\"><small>view stack trace</small></a>"
							+ "<div id=\"collapseOne\" class=\"text-danger panel-collapse collapse\">"
							+ "<small>"
							+ response.stackTrace
							+ "</small>"
							+ "</div>"
							+ "</div>";
					bootbox.alert(panel);
				}
			},
			oauthAuthenticate : function(event, url, serviceId) {
				event.preventDefault();
				var x = screen.width / 2 - 600 / 2;
				var y = screen.height / 2 - 400 / 2;

				var srcUrl = url;
				if (srcUrl == null || srcUrl == '') {
					srcUrl = this.baseURL + "/oauth/" + serviceId + "/authorize";
				}

				window.open(srcUrl, "connectWindow", "width=600,height=400,left="
						+ x + ",top=" + y);
			},
			linkMashup : function(){								
				var mashupId = $("#mashupIdSelect").val();
			
				if(mashupId!=-1){
   				this.savePage();							
   				
   //				window.location.href = this.baseURL + "/user/gui/" + this.projectId + "/link/mashup?mashupId="+mashupId;
   				
   				$.ajax({
   	            url: this.baseURL + "/user/gui/" + this.projectId + "/link/mashup",
   	            global: false,
   	            type: 'POST',
   	            data: {
   						"mashupId" : mashupId
   					},
   	            async: false, //blocks window close
   	            success: function(){
   	            	window.location.reload(true);
   	            }
   				});
				} else {
					bootbox.alert("Please select a valid mashup project!");
				}
				
			},
			getMashups : function()
			{
				$.ajax({
					url : this.baseURL + "/user/gui/" + this.projectId
							+ "/getallmashups.json",
					type : 'GET',
					success : $.proxy(function(response)
					{
						var data = jQuery.parseJSON(response);
						
						this.mashups = data.mashups;
						
						//console.log(this.mashups);
					}, this)
				});
			},
			configurationSave: function()
			{
				if($("input[name='firstPage']").length > 0 && $("input[name='firstPage']:checked").length == 0)
				{
					alert("Please select the first page.")
					return;
				}
				
				if($("#appName").val().trim() == "")
				{
					alert("Please enter a valid name for the application.")
					return;
				}
				
				this.spaName = $("#appName").val();
				
				$.ajax({
					url : this.baseURL + "/user/gui/" + guieditor.projectId + "/configuration/save",
					type : 'POST',
					data : $("#configurationForm").serialize(),
					success : function(response)
					{
						alert("Mockup configuration saved!");
						$('#configurationModal').modal('hide');
					}
				});
				
			}
		});
