/**
 * @class draw2d.command.CommandDelete Command to remove a figure with
 *        CommandStack support.
 * 
 * @extends draw2d.command.Command
 */
vme2.command.CommandDelete = draw2d.command.CommandDelete
		.extend({
			init : function(figure) {
				this._super(figure);
			},

			canExecute : function() {

				return this._super() && !(this.figure instanceof vme2.shape.End);
			},

			execute : function() {
				this.redo();
			},

			getCommandCollectionIfRequired : function() {
				var collection = null;

				if ((this.figure instanceof vme2.shape.Service)
						|| (this.figure instanceof vme2.shape.Operator)
						|| (this.figure instanceof vme2.shape.Blackbox)
						|| (this.figure instanceof vme2.shape.OpenData)) {
					var connections = this.figure.getConnections();
					if (connections != null && connections.size > 0) {
						collection = new draw2d.command.CommandCollection();
						connections.each(function(ix, conn) {
							collection.add(new vme2.command.CommandDelete(conn));
						});
						collection.add(this);
					}
				}
				return collection;
			},

			undo : function() {
				this._super();

				if (this.figure instanceof vme2.shape.Connection) {

					this.figure._reconnectParameter(this.bindingParameters);
				}
			},

			redo : function() {
				this._super();
				if (this.figure instanceof vme2.shape.Connection) {
					if (this.figure instanceof vme2.shape.Connection.Service) {

						if (this.figure instanceof vme2.shape.Connection.Service.S2S) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetServiceDef.operation.parameters);

						} else if (this.figure instanceof vme2.shape.Connection.Service.S2O) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetOperatorDef.parameters);

						} else if (this.figure instanceof vme2.shape.Connection.Service.S2E) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetOutputDef.inputs);

						} else if (this.figure instanceof vme2.shape.Connection.Service.S2B) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetBlackboxDef.parameters);

						}
					} else if (this.figure instanceof vme2.shape.Connection.Operator) {
						if (this.figure instanceof vme2.shape.Connection.Operator.O2S) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetServiceDef.operation.parameters);

						} else if (this.figure instanceof vme2.shape.Connection.Operator.O2O) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetOperatorDef.parameters);

						} else if (this.figure instanceof vme2.shape.Connection.Operator.O2E) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetOutputDef.inputs);

						} else if (this.figure instanceof vme2.shape.Connection.Operator.O2B) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetBlackboxDef.parameters);
						
						} else if (this.figure instanceof vme2.shape.Connection.Operator.O2I) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetInnerProcessDef.parameters);
						}
					} else if (this.figure instanceof vme2.shape.Connection.Blackbox) {
						if (this.figure instanceof vme2.shape.Connection.Blackbox.B2S) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetServiceDef.operation.parameters);

						} else if (this.figure instanceof vme2.shape.Connection.Blackbox.B2O) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetOperatorDef.parameters);

						} else if (this.figure instanceof vme2.shape.Connection.Blackbox.B2E) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetOutputDef.inputs);

						} else if (this.figure instanceof vme2.shape.Connection.Blackbox.B2B) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetBlackboxDef.parameters);

						}
						
						
					} else if (this.figure instanceof vme2.shape.Connection.OpenData) {
						if (this.figure instanceof vme2.shape.Connection.OpenData.OD2S) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetServiceDef.operation.parameters);

						} else if (this.figure instanceof vme2.shape.Connection.OpenData.OD2O) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetOperatorDef.parameters);

						} else if (this.figure instanceof vme2.shape.Connection.OpenData.OD2E) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetOutputDef.inputs);

						} else if (this.figure instanceof vme2.shape.Connection.OpenData.OD2B) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetBlackboxDef.parameters);

						}						
						
					} else if (this.figure instanceof vme2.shape.Connection.Start) {
						if (this.figure instanceof vme2.shape.Connection.Start.Start2S) {

							this.bindingParameters = $.extend(true, [],
									this.figure.targetServiceDef.operation.parameters);
							
						}else if (this.figure instanceof vme2.shape.Connection.Start.Start2B) {
							
							this.bindingParameters = $.extend(true, [],
									this.figure.targetBlackboxDef.parameters);
							
						}else if (this.figure instanceof vme2.shape.Connection.Start.Start2O) {
							
							this.bindingParameters = $.extend(true, [],
									this.figure.targetOperatorDef.parameters);
							
						}

					}

					this.figure._disconnectParameter();
				}
			}
		});