// declare the namespace 
var vme2 = {
	command : {},
	shape : {},
	propertypane : {},
	backend : {},
	dialog : {}
};

draw2d.Connection.createConnection = function(sourcePort, targetPort) { //console.log('sourceport', sourcePort);console.log('targetport', targetPort);console.log('source', sourcePort.parent);console.log('target', targetPort.parent);
	var sourceShape = sourcePort.parent;
	
	var targetShape = targetPort.parent;
	
	var id=sourceShape.id+"-->"+targetShape.id;
	
	if (app.getView().getFigure(id)== null)
	{
		if(sourceShape instanceof vme2.shape.Start)
		{			
			if(targetShape instanceof vme2.shape.Service)
			{				
				var con = new vme2.shape.Connection.Start.Start2S(sourceShape.id, targetShape.id);				
				con.id=id;
				return con;									
							
			}
			else if(targetShape instanceof vme2.shape.Blackbox)
			{
				var con = new vme2.shape.Connection.Start.Start2B(sourceShape.id, targetShape.id);				
				con.id=id;
				return con;									
					
			}
			else if(targetShape instanceof vme2.shape.Operator)
			{
				// IVAN
				// manage iterator case 
				if(targetShape.userData.type == "vme2.shape.Operator.iterator")
				{
					return null;
				}
				
				var con = new vme2.shape.Connection.Start.Start2O(sourceShape.id, targetShape.id);				
				con.id=id;
				return con;									
					
			}
		}
		else if(sourceShape instanceof vme2.shape.Service)
		{			
			if(targetShape instanceof vme2.shape.Service)
			{				
				var con = new vme2.shape.Connection.Service.S2S(sourceShape.id, targetShape.id);
				con.id=id;
				return con;									
							
			} 
			else if(targetShape instanceof vme2.shape.End)
			{			
				var con = new vme2.shape.Connection.Service.S2E(sourceShape.id, targetShape.id);
				con.id=id;
				return con;		
				
			} 
			else if(targetShape instanceof vme2.shape.Operator)
			{
				var con = new vme2.shape.Connection.Service.S2O(sourceShape.id, targetShape.id);
				con.id=id;
				return con;		
				
			} 
			else if(targetShape instanceof vme2.shape.Blackbox)
			{
				var con = new vme2.shape.Connection.Service.S2B(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			}
		}
		else if(sourceShape instanceof vme2.shape.Operator)
		{
			if(targetShape instanceof vme2.shape.End)
			{				
				// IVAN
				// manage iterator case (red port can be connected only with inner process)
				if(sourceShape.userData.type == "vme2.shape.Operator.iterator" && sourcePort.name == "output1")
				{
					return null;
				}	
				
				var con = new vme2.shape.Connection.Operator.O2E(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			} 
			else if(targetShape instanceof vme2.shape.Service)
			{
				// IVAN
				// manage iterator case (red port can be connected only with inner process)
				if(sourceShape.userData.type == "vme2.shape.Operator.iterator" && sourcePort.name == "output1")
				{
					return null;
				}	
				
				var con = new vme2.shape.Connection.Operator.O2S(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			} 
			else if(targetShape instanceof vme2.shape.Operator)
			{			
				// IVAN
				// manage iterator case (red port can be connected only with inner process)
				if(sourceShape.userData.type == "vme2.shape.Operator.iterator" && sourcePort.name == "output1")
				{
					return null;
				}				
				
				var con = new vme2.shape.Connection.Operator.O2O(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			} 
			else if(targetShape instanceof vme2.shape.Blackbox)
			{		
				// IVAN
				// manage iterator case (red port can be connected only with inner process)
				if(sourceShape.userData.type == "vme2.shape.Operator.iterator" && sourcePort.name == "output1")
				{
					return null;
				}	
				
				var con = new vme2.shape.Connection.Operator.O2B(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			} 
			else if(targetShape instanceof vme2.shape.InnerProcess)
			{
				var con = new vme2.shape.Connection.Operator.O2I(sourceShape.id, targetShape.id);
				con.id=id;
				return con;						
			}
		}
		else if(sourceShape instanceof vme2.shape.Blackbox)
		{
			if(targetShape instanceof vme2.shape.End)
			{				
				var con = new vme2.shape.Connection.Blackbox.B2E(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			} 
			else if(targetShape instanceof vme2.shape.Service)
			{
								var con = new vme2.shape.Connection.Blackbox.B2S(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			} 
			else if(targetShape instanceof vme2.shape.Operator)
			{			
				var con = new vme2.shape.Connection.Blackbox.B2O(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			} 
			else if(targetShape instanceof vme2.shape.Blackbox)
			{				
				var con = new vme2.shape.Connection.Blackbox.B2B(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			}
		}
		else if(sourceShape instanceof vme2.shape.OpenData)
		{
			if(targetShape instanceof vme2.shape.End)
			{				
				var con = new vme2.shape.Connection.OpenData.OD2E(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			}
			else if(targetShape instanceof vme2.shape.Service)
			{				
				var con = new vme2.shape.Connection.OpenData.OD2S(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			} 
			else if(targetShape instanceof vme2.shape.Operator)
			{			
				var con = new vme2.shape.Connection.OpenData.OD2O(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			} 
			else if(targetShape instanceof vme2.shape.Blackbox)
			{				
				var con = new vme2.shape.Connection.OpenData.OD2B(sourceShape.id, targetShape.id);
				con.id=id;
				return con;
			}				
		}
	}
	
	return null;
};

/**
 * 
 * 
 * @extends draw2d.ui.parts.GraphicalEditor
 */
vme2.Application = Class.extend({
	NAME : "vme2.Application",
	
	baseURL: '/visualcomposer',
	
	services: [],
	
	operatorTemplate : ''+
      '<li data-name="{{name}}" data-idprefix="{{idprefix}}" data-shape="{{type}}" ' +
      ' class="row palette_node_element operator_element draw2d_droppable valign-wrapper">'+
      '<i class="material-icons col s1">{{icon}}</i>&nbsp;&nbsp;<span class="operator_title col s10" data-title="{{name}}">{{name}}</span>'+     
      '<i class="material-icons col s1 tooltipped operator-guide" data-opid="{{type}}" data-position="right" data-delay="0" data-tooltip="' + Message['mashup.palette.operators.help'] + '">help</i>' +
      '</li>'+
      '',
      
   serviceTemplate : '{{#each this}}'+
      '<li>'+
      '<div data-name="{{name}}" data-svc="{{id}}" data-shape="vme2.shape.Service" ' +
      ' class="row palette_node_element service_element {{#ifeq type "restful"}}rest_element{{/ifeq}}{{#ifeq type "wsdl"}}soap_element{{/ifeq}} valign-wrapper" >'+
      '<i class="material-icons col s1">send</i><sub class="palette_label_service_type">'+
      '{{#ifeq type "restful"}}REST{{/ifeq}}{{#ifeq type "wsdl"}}SOAP{{/ifeq}}'+
      '</sub>&nbsp; <span class="service_title col s10" data-title="{{name}}">{{name}}</span>'+        
      '<i class="material-icons col s1 tooltipped" data-position="right" data-delay="0" data-tooltip="' + Message['mashup.palette.services.help'] + '" ' +
      ' onclick="javascript:app.openServiceWindow({{id}})">help</i>' +
      '</div>'+
      '</li>'+
      '{{/each}}',
      
   operationTemplate : '{{#each this}}'+
      '<li>'+
      '<div data-name="{{name}}" data-svc="{{serviceId}}" data-op="{{id}}" data-shape="vme2.shape.Service" ' +
      ' class="palette_node_element operation_element draw2d_droppable valign-wrapper" >'+
      '<i class="material-icons">send</i>&nbsp; <span>{{name}}</span>'+                                                      
      '</div>'+
      '</li>'+
      '{{/each}}', 
      
      
   openDataTemplate : '{{#each this}}'+
      '<li>'+
      '<div data-name="{{name}}" data-svc="{{id}}" data-idprefix="opendata" data-shape="vme2.shape.OpenData" ' +
      ' class="row palette_node_element opendata_element valign-wrapper" >'+
      '<i class="material-icons col s1">storage</i><sub class="palette_label_service_type">'+
      'ODATA'+
      '</sub>&nbsp; <span class="opendata_title col s10" data-title="{{name}}">{{name}}</span>'+       
      '<i class="material-icons col s1 tooltipped" data-position="right" data-delay="0" data-tooltip="' + Message['mashup.palette.datasets.help'] + '" ' +
      ' onclick="javascript:app.openOpendataWindow({{id}})">help</i>' +
      '</div>'+
      '</li>'+
      '{{/each}}',   
      
   resourceTemplate : '{{#each this}}'+
      '<li>'+
      '<div data-name="{{title}}" data-svc="{{datasetId}}" data-res="{{id}}" data-shape="vme2.shape.OpenData" ' +
      ' class="palette_node_element resource_element draw2d_droppable valign-wrapper" >'+
      '<i class="material-icons">storage</i>&nbsp; <span>{{title}}</span>'+                                                      
      '</div>'+
      '</li>'+
      '{{/each}}', 
      
   blackboxTemplate : '{{#each this}}'+
      '<li>'+
      '<div data-name="{{name}}" data-idprefix="blackbox" data-svc="{{id}}" data-shape="vme2.shape.Blackbox" ' +
      ' class="palette_node_element draw2d_droppable" >'+
      '<i class="icon-cogs"></i>&nbsp;{{name}}'+                                                      
      '</div>'+
      '</li>'+
      '{{/each}}',      
      
      
   innerProcessTemplate : '{{#each this}}'+
      '<li>'+
      '<div data-name="{{name}}" data-idprefix="innerprocess" data-svc="{{id}}" data-shape="vme2.shape.InnerProcess" ' +
      ' class="row palette_node_element innerprocess_element draw2d_droppable valign-wrapper" >'+
      '<i class="material-icons col s1">loop</i>&nbsp;<span class="innerprocess_title col s10" data-title="{{name}}">{{name}}</span>'+    
      '<i class="material-icons col s1 tooltipped" data-position="right" data-delay="0" data-tooltip="' + Message['mashup.palette.innerprocesses.help'] + '" ' +
      ' onclick="javascript:app.openMashupWindow({{id}})">help</i>' +
      '</div>'+
      '</li>'+
      '{{/each}}', 
      
   invokeWorkflowWindowTemplate : "<form id=\"invokeworkflowparams\" method=\"POST\" action=\""+this.baseURL+ "/user/project/"+this.projectId+"/invokeworkflow\">"+
   	"{{#each this}}"+
	"<div class='row valign-wrapper targetRow'>" +
	"	<div class='input-field col s12'>" +
	"		<input id='{{id}}' class='paramInput' name='{{id}}' type='text' value='{{userData.defaultValue}}' {{#ifeq userData.isConstant true}}readonly{{/ifeq}} />" +
	"		<label for='{{id}}'>{{userData.label}}{{#ifeq userData.isConstant true}} (Constant){{/ifeq}}</label>" +
	"   </div>" +
	"</div>" +
   	"{{/each}}" +
   	"</form>" +
   	'<p>' +
    '<input type="checkbox" class="filled-in" id="askForInputs" />' +
    '<label for="askForInputs">' + Message['mashup.modals.invoke_workflow.use_defaults'] + '</label>' +
    '</p>'
   	,
	
	/**
	 * @constructor
	 * 
	 * @param {String}
	 *            canvasId the id of the DOM element to use as paint container
	 */
	init : function(baseURL, projectId, userLevel) {
//	    Handlebars.registerHelper('ifeq', function (a, b, options) {
//	        if (a == b) { return options.fn(this); }
//        });
		Handlebars.registerHelper('ifeq', function(a, b, options) {
			var fnTrue=options.fn, fnFalse=options.inverse;
			return a == b ? fnTrue(this) : fnFalse(this);
		});
		
	    Handlebars.registerHelper('ifneq', function (a, b, options) {
	        if (a != b) { return options.fn(this); }
        });
	    
	    Handlebars.registerHelper('selected', function(v1, v2) {
	    	  return (v1 == v2 ? " selected='selected' " : "");
	    });	    
						
		// RegexColorizer.addStyleSheet();
		this.baseURL = baseURL;
		this.projectId = projectId;
		this.userLevel = userLevel;

		this.backend = new vme2.backend.Backend(this.baseURL);
		this.view = new vme2.View("canvas", this.projectId);		
		
		this.toolbar = new vme2.Toolbar("toolbar", this.view);
		this.propertyPane = new vme2.PropertyPane("property", this.view);

		// layout FIRST the body
		this.contentLayout = $('#container').layout({
			center : {
				resizable : false,
				closable : false,
				spacing_open : 0,
				spacing_closed : 0,
				size : "auto",
				paneSelector : "#content"
			}
		});

		this.editorLayout = $('#content').layout({
			center : {
				resizable : false,
				closable : false,
				spacing_open : 0,
				spacing_closed : 0,
				size : "auto",
				paneSelector : "#editor"
			}
		});

		this.appLayout = $('#editor').layout({
			west : {
				resizable : true,
				closable : true,
				spacing_open : 0,
				spacing_closed : 0,
				size : "20%",
				paneSelector : "#palette"
			},
			center : {
				resizable : true,
				closable : false,
				spacing_open : 0,
				spacing_closed : 0,
				size : "auto",
				paneSelector : "#view"
			}
		});

		this.viewLayout = $('#view').layout({
			east : {
				size: 0
				/*
				resizable : true,
				closable : true,
				spacing_open : 5,
				spacing_closed : 5,
				size : "15%",
				paneSelector : "#property",
				onresize : $.proxy(function() {
					this.propertyPane.onResize();
				}, this) */
			},
			center : {
				resizable : true,
				closable : false,
				spacing_open : 0,
				spacing_closed : 0,
				size : "auto",
				paneSelector : "#canvas"
			}
		});

		this.loadDefinition();
		this.loadServices();
		this.loadOpenData();
		this.loadBlackbox();
		this.loadInnerProcess();
		
		this.operators = [];
		this.loadOperatorCategories();
		this.loadOperators();	
		
		this.askForInputs = true;
		
		$("#canvas").scroll(function() {
			$("#shapeControls a").hide();
		});
	},

	executeCommand : function(cmd) {
		this.view.getCommandStack().execute(cmd);
	},

	/**
	 * @method Return the view or canvas of the application. Required to access
	 *         the document itself
	 * 
	 */
	getView : function() {
		return this.view;
	},

	/**
	 * @method Return the backend data storage for this application
	 * 
	 */
	getBackend : function() {
		return this.backend;
	},	

	saveDefinition : function(silent, callback) {
		
		var writer = new draw2d.io.json.Writer();
		var content = {content : writer.marshal(this.view)};
		
		this.backend.save(
			this.projectId, 
			content, 
			$.proxy(function() {
				
				// IVAN for materialize version
				if(!silent)
				{
					var toastContent = $('<span class="toastSuccess">Project saved</span>');
					Materialize.toast(toastContent, 2000);
					$(".toastSuccess").parent().addClass("toast-success");
				}
				
				this.view.getCommandStack().markSaveLocation();
				
				if(callback)
					callback();
				
			}, this)		
		);
	},
	
	loadDefinition : function() {
		this.view.clear();
		
		this.backend.load(this.projectId, $.proxy(function(jsonDocument) {
			if(jsonDocument!=null){
				var reader = new draw2d.io.json.Reader();
				
				var requirejsLib = [];
				
				for(var i=0; i<jsonDocument.content.length; i++){
					if(jsonDocument.content[i].type.indexOf("vme2.shape.Operator")!==-1){
						requirejsLib.push("app/"+jsonDocument.content[i].type);											
					}
				}
				
				if(requirejsLib.length>0){
   				require(requirejsLib, $.proxy(function(Operator){
   					//window[Operator.prototype.NAME] = Operator;
   					reader.unmarshal(this.view, jsonDocument.content);
   				}, this));
				} else{
					reader.unmarshal(this.view, jsonDocument.content);
				}
				
				
				/*require(["app/vme2.shape.Operator.newobject"], $.proxy(function(Operator){
					window.vme2.shape.Operator.newobject = Operator;
					reader.unmarshal(this.view, jsonDocument.content);
				}, this));*/
			} else{
				this.view.createOutputNode();
				this.view.createInputNode();
			}
		}, this));
	},
	
	getServices: function (){
		return this.services;
	},
	
	getService: function (svcId){
		var sAr = $.grep(this.services, function(svc){
			return svc.id == svcId;
		});
		return (sAr.length == 1 ? sAr[0] : null); 
	},	

	getBlackbox: function (svcId){
		var sAr = $.grep(this.blackboxes, function(svc){
			return svc.id == svcId;
		});
		return (sAr.length == 1 ? sAr[0] : null); 
	},	
	
	getInnerProcess: function (svcId){
		var sAr = $.grep(this.innerprocesses, function(svc){
			return svc.id == svcId;
		});
		return (sAr.length == 1 ? sAr[0] : null); 
	},		
	
	getOpenData: function (svcId){
		var sAr = $.grep(this.opendata, function(svc){
			return svc.id == svcId;
		});
		return (sAr.length == 1 ? sAr[0] : null); 
	},	
	
	loadServices : function() {
		this.backend.getServices($.proxy(function(result){
			this.services = result;	
			
			var container = $('#serviceList').empty();
            
			var compiled = Handlebars.compile(this.serviceTemplate);
			var output = compiled(this.services);
			container.append(output);   
			 
			 // search configuration
			 
			var options = {
				valueNames: [{ attr: 'data-title', name: 'service_title' }]
			};
			
			var serviceList = new List('services', options);
			
			$('.tooltipped').tooltip();	
			
			// on click show/hide service operations
			$("div.palette_node_element.service_element").on('click',
					
				$.proxy(function(data) {
					var serviceElement = data.currentTarget;
					var svcId = $(serviceElement).attr("data-svc");
					
					var _this = this;
					
					var operations = $(".operation_element[data-svc='" + svcId + "']");
					
					// if operations are not loaded yet, then load them
					if(operations.length == 0)
					{
						$.getJSON(this.getBackend().getOperationsServiceUrl(svcId), 
							$.proxy(function(ops) {
								
								compiled = Handlebars.compile(_this.operationTemplate);
								output = compiled(ops);
								$(serviceElement).parent().after(output); 
								
								_this.view.rebindDraggable(); 							
							}
						), _this);
					}
					else // show/hide operations
					{
						$(operations).parent().toggle();
						
						this.view.rebindDraggable(); 
					}
				
				}, this)
			);
			 
			this.view.rebindDraggable();     
			
		}, this));
	},
	loadOpenData : function() {
		this.backend.getOpenData($.proxy(function(result){
			this.opendata = result;	
			
			var container = $('#openDataList').empty();
            
         var compiled = Handlebars.compile(this.openDataTemplate);
         var output = compiled(this.opendata);
         container.append(output);   
         
         var options = {
   			  valueNames: [{ attr: 'data-title', name: 'opendata_title' }]
   			};

   		var opendataList = new List('opendatas', options);
   		
   		$('.tooltipped').tooltip();	
   		
   		// on click show/hide opendata resources
		$("div.palette_node_element.opendata_element").on('click',
				
			$.proxy(function(data) {
				var opendataElement = data.currentTarget;
				var datasetId = $(opendataElement).attr("data-svc");
				
				var _this = this;
				
				var resources = $(".resource_element[data-svc='" + datasetId + "']");
				
				// if operations are not loaded yet, then load them
				if(resources.length == 0)
				{
					$.getJSON(this.getBackend().getResourcesServiceUrl(datasetId), 
						$.proxy(function(res) {
							
							compiled = Handlebars.compile(_this.resourceTemplate);
							output = compiled(res);
							$(opendataElement).parent().after(output); 
							
							_this.view.rebindDraggable(); 
						}
					), _this);
				}
				else // show/hide operations
				{
					$(resources).parent().toggle();
					
					this.view.rebindDraggable(); 
				}
			
			}, this)
		);
         
         this.view.rebindDraggable();                        
		}, this));
	},	
	loadBlackbox: function() {
		this.backend.getBlackbox(this.projectId, $.proxy(function(result){
			this.blackboxes = result;	
			
			var container = $('#blackboxList');
            
         var compiled = Handlebars.compile(this.blackboxTemplate);
         var output = compiled(this.blackboxes);
         container.append('<li class="nav-header project_palette_header">Blackbox</li>').append(output);   
         this.view.rebindDraggable();                        
		}, this));
	},
	
	loadInnerProcess: function() {
		this.backend.getInnerProcess(this.projectId, $.proxy(function(result){
			this.innerprocesses = result;	
			
			var container = $('#innerProcessList');
            
         var compiled = Handlebars.compile(this.innerProcessTemplate);
         var output = compiled(this.innerprocesses);
         container.append(output);   
         
         var options = {
      			  valueNames: [{ attr: 'data-title', name: 'innerprocess_title' }]
      			};

      	 var innerProcessList = new List('innerprocesses', options);
      	 
      	 $('.tooltipped').tooltip();	
         
         this.view.rebindDraggable();    			
		}, this));
	},	
	
	loadOperatorCategories : function() {
		
		this.backend.getOperatorCategories($.proxy(function(data) {
			this.operatorCategories = data;
		}, this));
	},
	
	loadOperators : function() {
		
		this.backend.getOperatorList($.proxy(function(data){
			 
			this.compiledOperatorTemplate = Handlebars.compile(this.operatorTemplate);
			$.ajaxSetup({
				async : false
			});	
			
			$.each(data.categories, $.proxy(function(i, category) {												
				
				if(!(this.userLevel == 2 && category.name == 'Advanced'))
				{
					$('#operatorList').append('<li class="project_palette_header">'+category.name+'</li>');
					
					for(var i=0; i < category.items.length; i++){
						this.operators.push(category.items[i].id);
						
						this.backend.getOperator(category.items[i].id, $.proxy(function(data){		
							
							if(category.items[i].icon)
								data.icon = category.items[i].icon;
							else
								data.icon = "device_hub";
							
							var output = this.compiledOperatorTemplate(data);
							 
							 $('#operatorList').append(output);
						}, this));	
					}	
					
					var options = {
					  valueNames: [{ attr: 'data-title', name: 'operator_title' }]
					};
	
					var operatorList = new List('operators', options);
					
					$('.tooltipped').tooltip();
					
					$('.operator-guide').off('click');
					$('.operator-guide').on('click', function(e) { 
		    			e.preventDefault();
		    			
		    			$("#operatorDocsModalContent").load(Settings.baseUrl + "/mashupeditor/operators/docs/" + $(this).data('opid') + ".html", function() {
		    				$('#operatorDocsModal').openModal();
						});
	
		    		});
				}
				
			}, this));  
			$.ajaxSetup({
				async : true
			});
			
			/*
	      $(".paramPop").popover({
	    	    trigger: 'hover',
	    	    animation: false,
	    	    delay: { show: 100, hide: 1 }
	      }).css('z-index', 2030); */
		}, this));
	},			
	
	getRenderer: function (rndId){
		var result = null;
		var selectedRenderer=null;
		var foundStatus = false;
		
		for(var i=0; i<this.renderers.length;i++){
			var category = this.renderers[i];
			for(var j=0; j<category.renderers.length;j++){
				if(category.renderers[j].id==rndId){
					selectedRenderer = category.renderers[j];
					foundStatus = true;
					break;
				}
			}
			if(foundStatus){
				break;
			}
		}
		//clono l'oggetto perch� viene ritornato il riferimetno e sarebbe condiviso...
		if (selectedRenderer != null)
			 result = $.extend(true, {}, selectedRenderer);
		return result; 
		
	},	
	invokeJSONConverter: function(invokeObj, callback){
		this.backend.invokeConverter(invokeObj, function (data){
			callback(data);
		});
	},
	
	buildInvokeWorkflowWindow: function(isServiceChain, serviceId, callback, dismissCallback){
		
		//Cattura i nodi root
		var rootFigure = this.view.getFigureRoots(serviceId);
				
		// IVAN
		// check if the figure doesn't need inputs
		var figureData = this.view.getFigure(serviceId).userData;
		
		var noInputFlag = false;
		var operatorType = "vme2.shape.Operator";
		
		if(figureData.inputs == undefined)
		{
		// if opendata
		//if(figureData.operation == undefined && figureData.type.substring(0, operatorType.length) != operatorType)
		if(figureData.resourceId)
			noInputFlag = true;
		// if service with no inputs or service with option parameters
		if(figureData.type == "restful" || figureData.type == "wsdl")
		{
			if(figureData.operation.parameters.length == 0)
				noInputFlag = true;
			else
			{
				var allOptionsParameters = true;
				var allUnrequiredParameters = true;
				
				for(var i = 0; i < figureData.operation.parameters.length; i++)
				{
					if(figureData.operation.parameters[i].options == undefined)
						allOptionsParameters = false;
					
					if(figureData.operation.parameters[i].required == true)
						allUnrequiredParameters = false;
				}
				
				if(allOptionsParameters || allUnrequiredParameters)
					noInputFlag = true;
			}
		}
		}
		
		if(rootFigure.length===0 && noInputFlag == false)
		{
			var toastContent = $('<span class="toastError">Selected node is not connected with a valid input or opendata shapes</span>');
			Materialize.toast(toastContent, 2000);
			$(".toastError").parent().addClass("toast-error");
		} 
		else 
		{
	   		var needInputParams = false;
	   		
	   		//Controlla se è necessario mostrare la finestra dei parametri di input
	   		//verificando se tra i nodi root ce n'è almeno uno di tipo "input" (vme2.shape.Start)
	   		for(var i=0; i<rootFigure.length; i++)
	   		{
	   			if(this.view.getFigure(rootFigure[i]) instanceof vme2.shape.Start)
	   			{
	   				needInputParams = true;
	   				break;
	   			}
	   		}
	   		
	   		if(needInputParams)
	   		{
	      		var inputShapes = this.view.getInputShapes();
	      
	      		var compiled = Handlebars.compile(this.invokeWorkflowWindowTemplate);
	      		var output = compiled(inputShapes);
	      		
	      		var container = $('#invokeWorkflow');		
	      		container.find('#invokeWorkflowBody').html(output);
	      		
	      		$('#askForInputs').off('change');//Reset di un precedente event bind
	      		$('#askForInputs').on('change', $.proxy(function(e) {
	      			e.preventDefault();
	      			
	      			this.askForInputs = !$('#askForInputs').is(':checked');
	      			
	      			$("#askForInputsControl").prop('checked', !this.askForInputs);
	      			
	      		}, this));
	      		
	      		container.find('#invokeWorkflowOk').off('click');//Reset di un precedente event bind
	      		container.find('#invokeWorkflowOk').on('click', $.proxy(function(e) {
	      			e.preventDefault();
	      			if(isServiceChain){
	      				this._invokeServiceChainWorkflow(serviceId, callback);
	      			}else{
	      				this._invokeWorkflow(callback);
	      			}
	      			
	      		}, this));
	      		
	      		if(isServiceChain){
	      			container.find('#invokeWorkflowCancel').off('click');//Reset di un precedente event bind
	      			container.find('#invokeWorkflowCancel').on('click', $.proxy(function(e) {
	      				e.preventDefault();
	      				
	      				if(dismissCallback)
	      					dismissCallback();
	      			}, this));	
	      		}
	      		
	      		$('.tooltipped').tooltip('remove');
	      		$('.tooltipped').tooltip();
	      		Materialize.updateTextFields();	      		
	      		
	      		if(this.askForInputs)
	      			$(container).openModal({ dismissible:false });
	      		else
	      		{
	      			if(isServiceChain)
	      			{
	      				this._invokeServiceChainWorkflow(serviceId, callback);
	      			}
	      			else
	      			{
	      				this._invokeWorkflow(callback);
	      			}
	      		}
	   		} 
	   		else 
	   		{
	   			if(isServiceChain)
	   			{console.log("Chain");
	   				this._invokeServiceChainWorkflow(serviceId, callback);
	   			}
	   			else
	   			{console.log("Workflow");
	   				this._invokeWorkflow(callback);
	   			}
	   		}
		}
	},
	_invokeWorkflow: function(callback){		
		var parametersString = $("#invokeworkflowparams").serialize();
		
		$("#spinner_layer").show();
		
		this.backend.invokeWorkflow(this.projectId, parametersString, function (data){
			$('#invokeWorkflow').closeModal();
			$("#spinner_layer").hide();
			callback(data);			
		});
	},
	
	_invokeServiceChainWorkflow: function(serviceId, callback){
				
		var paramsMap = $("#invokeworkflowparams").serializeArray();
		var invokeParams = [];
		
		for ( var i = 0; i < paramsMap.length; i++) {
			invokeParams.push({
				name : paramsMap[i].name,
				value : paramsMap[i].value
			});
		}

		var writer = new draw2d.io.json.Writer();
		var content = {
				content : writer.marshal(this.view), 
				inputparams: invokeParams,
				serviceid: serviceId
		};	
		
		$("#spinner_layer").show();
		
		this.backend.invokeServiceChain(this.projectId, content, function (data){
			$('#invokeWorkflow').closeModal();
			$("#spinner_layer").hide();
			callback(data);			
		});
	},
	
	invokeOpenDataResource: function(openDataId, query, callback){
		
		$("#spinner_layer").show();
		
		this.backend.invokeOpenDataResource(openDataId, query, function (data){		
			
			$("#spinner_layer").hide();
			
			callback(data);			
		});
	},
	oauthAuthenticate: function(event, url, serviceId){
		event.preventDefault();
		var x = screen.width/2 - 600/2;
	    var y = screen.height/2 - 400/2;
	    
	    var srcUrl = url;
	    if(srcUrl==null || srcUrl==''){
	    	srcUrl = this.baseURL+"/oauth/"+serviceId+"/authorize";
	    }
	    
		window.open(srcUrl, "connectWindow", "width=600,height=400,left="+x+",top="+y);
	},
	
	getOperatorParameterFormSize : function(){
		var formJson = $("#parameterConnections").serializeJSON();
		return formJson.parameters!==undefined ? formJson.parameters.length : 0;
	},
	
	_htmlDecode: function(value)
	{ 
	  return $('<div/>').html(value).text(); 
	},
	
	renderSchemas: function()
	{
		var htmlDecode = this._htmlDecode;
		
		$(".schemaContainer").each(function(){
			
			var schema = $(this).find("input").first().val();
			
			var div = $(this).find("div").first();
			
			if(schema != "")
			{
				var view = "";
				
				try
				{
					view = new JSONSchemaView(JSON.parse(schema), 0);
				}
				catch(ex)
				{
					view = new JSONSchemaView(JSON.parse(htmlDecode(schema)), 0);
				}
				
				var content = view.render();
			
				$(div).html(content);	
			}
		});	
	},
	
	updateFigureControlsPosition: function(figure, zoom) 
	{	//console.log('updateFigureControlsPosition');
		$("#shapeControls").hide();
		$('#noteContainer').hide();
		
		var canvas = figure.canvas.html[0];
        
        var offset = $(canvas).offset();
        offset.top -= canvas.scrollTop;
        offset.left -= canvas.scrollLeft;
        
        var margin = 32 + 5;  
        
        $("#shapeControls").css({"left" : offset.left + Math.round(figure.x / zoom), "top" : offset.top + Math.round(figure.y / zoom) - margin}).show();
        
        $("#shapeControls").show();
		
		if(figure.userData.notes && figure.userData.notes != '')
		{
			$('#noteContainer p').html(figure.userData.notes);
			$("#noteContainer").css({"left" : offset.left + Math.round(figure.x / zoom), "top" : offset.top + Math.round(figure.y / zoom) + Math.round(2 / zoom)*margin}).show();
			$('#noteContainer').show();
		}
	},
	
	openServiceWindow: function (id) 
	{
		var win = window.open(Settings.baseUrl + Settings.serviceCatalog.baseUrl + '/' + id + '/details', '_blank');
		win.focus();
	},
	
	openOpendataWindow: function (id) 
	{
		var win = window.open(Settings.baseUrl + Settings.opendataCatalog.baseUrl + '/' + id + '/details', '_blank');
		win.focus();
	},
	
	openMashupWindow: function (id) 
	{
		var win = window.open(Settings.baseUrl + Settings.mashupProjects.baseUrl + '/' + id + '/editor', '_blank');
		win.focus();
	}
});
