HandlebarsHelpers = Class
.extend({
	NAME : "vme2.HandlebarsHelpers",

	baseURL : '/visualcomposer',

	init : function() {	
		// Helper per il concatenamento di stringe
		Handlebars.registerHelper('concat', function(a, b) {
			return a + b;
		});
		
		// IVAN
		// Replace prev with next in s
		Handlebars.registerHelper('replace', function(s, prev, next) {
			
			if(s == undefined) return "";
			
			return s.replace(prev, next);
		});

		// Helper che restituisce, tramite jsonpath, il valore di un
		// elemento dentro un JSON
		Handlebars.registerHelper('getByJsonPath', function(context, path) {
			var result = jsonPath(context, path);
			return result!=false ? (result instanceof Array ? result[0] : result) : undefined;
		});

		// Helper che effettua comparazioni tra 2 valori passati
		Handlebars.registerHelper('ifeq', function(a, b, options) {
			var fnTrue=options.fn, fnFalse=options.inverse;
			return a == b ? fnTrue(this) : fnFalse(this);
		});

		// Helper per la costruzione dei tag html a partire da JsonML
		Handlebars.registerHelper('buildtagfromjson', function(content) {

			// Trasforma il JsonML in HTML Text
			var tagStr = JsonML.toHTMLText(content);

			return new Handlebars.SafeString(tagStr);
		});

		// Helper che serve a catturare una data proprietà di style a
		// partire dall'intera stringa
		// contenente le definizioni di stile
		Handlebars.registerHelper("styleProperty", function(styleStr,
				property) {
			
			/**
			 * L'espressione regolare matcha con: (es.: property=color)
			 * - "color:red"
			 * - "color :red"
			 * - "font-size:12px;color:red" 
			 * - "font-size:12px; color:red"
			 * 
			 * Non matcha con:
			 * - "background-color:red";
			 */
			var regExp = new RegExp('(( +|;)'+property+' *|^'+property+' *):');									
			
			if (styleStr && styleStr.match(regExp)) {
				var ret = "";
				var properties = styleStr.trim().split(";");

				for (var i = 0; i < properties.length; i++) {
					var propertyStr = properties[i];
					if (propertyStr.match(regExp)) {
						propertyStr = propertyStr.substring(propertyStr
								.indexOf(":") + 1);
						ret = propertyStr.trim();
						break;
					}
				}
				return ret;
			}
		});
		
		// Helper che serve a catturare una data proprietà di bind a
		// partire dall'intera stringa
		// contenente le definizioni per il data binding
		Handlebars.registerHelper("dataBindProperty", function(databindStr,
				property, databindType) {
			
			var regexp;
			
			if(databindType==='complex'){
				regexp = new RegExp(property + ":[^\{\}:,]*");						
			}else{
				regexp = new RegExp(".*");
			}																								
			
			if (databindStr && databindStr.match(regexp)) {
				var ret = "";
										
				var matchingArr = regexp.exec(databindStr);
				
				var matchingSubstr = matchingArr[0];
				
				ret = matchingSubstr.substring(matchingSubstr.indexOf(":") + 1);
				ret = ret.trim();
										
				return ret;
			}
		});				

		// Helper che trasforma un array in stringa con separatore
		Handlebars.registerHelper("array2string",
				function(array, separator) {
					if ($.isArray(array)) {
						var ret = "";

						for (var i = 0; i < array.length; i++) {
							ret = ret + separator + array[i];
						}
						return ret.trim();
					}
				});

		// Helper che stabilisce la selezione della option relativa ad una
		// select
		Handlebars.registerHelper('select', function(value, options) {
			var $el = $('<select />').html(options.fn(this));
			
			if(value!==undefined){					
				$el.find('[value="' + value.replace(/'/g, "\\'").replace(/, /g, ",") + '"]').attr({
					'selected' : 'selected'
				});
			}
			return $el.html();
		});	
		
		Handlebars.registerHelper('eqAnyOf', function(value, array) {
			
			var options = eval(array);
			
			for(var i = 0; i < options.length; i++)
			{
				if(value == options[i])
					return true;
			}
			
			return false;
		});
	}
});