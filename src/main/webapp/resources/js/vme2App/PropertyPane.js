

vme2.PropertyPane = Class.extend({
	
	inputSettings_template: ''
		+ '<form id="inputDetails" class="col s12">'
		+	'<div class="row">'
		+		'<div class="input-field col s12">'
		+			'<input id="label" class="validate project-name-chars" name="label" type="text" value="{{label}}">'
		+			'<label for="label">' + Message['mashup.modals.input_settings.label'] + '</label>'
		+		'</div>'
		+	'</div>'
		/*		
 		+	'<div class="row">'
		+		'<div class="input-field col s12">'
		+			'<select id="inputType" name="inputType" value="{{inputType}}">'
		+				'<option value="string">' + Message['mashup.modals.input_settings.type.text'] + '</option>'
		+				'<option value="number">' + Message['mashup.modals.input_settings.type.number'] + '</option>'
		+				'<option value="object">' + Message['mashup.modals.input_settings.type.object'] + '</option>'
		+			'</select>'
		+			'<label for="inputType">' + Message['mashup.modals.input_settings.type'] + '</label>'
		+		'</div>'
		+	'</div>'
		*/
		+	'<div class="row">'
		+		'<div class="input-field col s12">'
		+			'<input id="defaultValue" name="defaultValue" type="text" value="{{defaultValue}}">'
		+			'<label for="defaultValue">' + Message['mashup.modals.input_settings.default'] + '</label>'
		+		'</div>'
		+	'</div>'
		+	'<div class="row">'
		+		'<div class="input-field col s12">'
		+			'<input id="isConstant" name="isConstant" type="checkbox" class="filled-in" {{#ifeq isConstant true}}checked{{/ifeq}} >'
		+			'<label for="isConstant">Use as constant</label>'
		+		'</div>'
		+	'</div>'
		+ '</form>'
	,		
	
	serviceSpecs_template: ''
		+ '<div class="row" style="margin-bottom: 0">'
		+ '	<div class="col s1"><span class="opMethod opMethod_{{httpMethod}}">{{httpMethod}}</span></div>'
		+ '	<div class="col s11"><span class="opPath">{{url}}</span></div>'
		+ '</div>'
		+ '<br>'
		+ '<div>'
		+ '	<strong class="opSection">' + Message['catalogs.details.operations.description'] + '</strong>'
		+ '	<div style="padding: 1em">{{description}}</div>'
		+ '</div>'
		+ '{{#ifeq authentication "true"}}'
		+ '<div>'
		+ '	<strong class="opSection">' + Message['catalogs.details.operations.authorization'] + '</strong>'
		+ '	<div style="padding: 1em">'
		+ '		<strong>' + Message['catalogs.details.operations.authorization.permissions'] + '</strong>'
		+ '		<ul>'
		+ '		{{#each authInfo.scopes}}'
		+ '			<li>{{this}}</li>'
		+ '		{{/each}}'
		+ '		</ul>'
		+ '	</div>'
		+ '</div>'
		+ '{{/ifeq}}'
		+ '<div>'
		+ '	<strong class="opSection">' + Message['catalogs.details.operations.parameters'] + '</strong>'
		+ '	<br><br>'
		+ '	<table class="striped">'
		+ '		<thead>'
		+ '			<tr>'
		+ '				<td class="col s2"><strong>' + Message['catalogs.details.operations.parameters.name'] + '</strong></td>'
		+ '				<td class="col s1"><strong>' + Message['catalogs.details.operations.parameters.type'] + '</strong></td>'
		+ '				<td class="col s5"><strong>' + Message['catalogs.details.operations.parameters.description'] + '</strong></td>'
		+ '				<td class="col s1"><strong>' + Message['catalogs.details.operations.parameters.required'] + '</strong></td>'
		+ '				<td class="col s3"><strong>' + Message['catalogs.details.operations.parameters.schema'] + '</strong></td>'
		+ '			</tr>'
		+ '		</thead>'
		+ '		<tbody>'
		+ '			{{#each parameters}}'
		+ '			<tr>'
		+ '				<td class="col s2">{{name}}</td>'
		+ '				<td class="col s1">{{type}}</td>'
		+ '				<td class="col s5">{{doc.text}}</td>'
		+ '				<td class="col s1">{{required}}</td>'
		+ '				<td class="schemaContainer col s3">'
		+ '					<input type="hidden" value="{{schema}}">'
		+ '					<div></div>'
		+ '				</td>'
		+ '			</tr>'
		+ '			{{/each}}'
		+ '		</tbody>'
		+ '	</table>'
		+ '</div>'
		+ '<br>'
		+ '<div>'
		+ '	<strong class="opSection">' + Message['catalogs.details.operations.responses'] + '</strong>'
		+ '	<br><br>'
		+ '	<table class="striped">'
		+ '		<thead>'
		+ '		<tr>'
		+ '			<td class="col s2"><strong>' + Message['catalogs.details.operations.responses.code'] + '</strong></td>'
		+ '			<td class="col s7"><strong>' + Message['catalogs.details.operations.responses.description'] + '</strong></td>'
		+ '			<td class="col s3"><strong>' + Message['catalogs.details.operations.responses.schema'] + '</strong></td>'
		+ '		</tr>'
		+ '		</thead>'
		+ '		<tbody>'
		+ '			{{#each responses}}'
		+ '			<tr>'
		+ '				<td class="col s2">{{statusCodes.[0]}}</td>'
		+ '				<td class="col s7">{{doc.[0].text}}</td>'
		+ '				<td class="schemaContainer col s3">'
		+ '					<input type="hidden" value="{{schema}}">'
		+ '					<div></div>'
		+ '				</td>'
		+ '			</tr>'
		+ '			{{/each}}'
		+ '		</tbody>'
		+ '	</table>'
		+ '</div>'
	,
	
	init:function(elementId, view){
	    this.selectedFigure = null;
        this.html = $("#"+elementId);
        this.view = view;
        this.pane = null;
        this.view.addSelectionListener(this);
        
        // register as listener to update the property pane if anything has been changed in the model
        //
        view.getCommandStack().addEventListener($.proxy(function(event){
            if(event.isPostChangeEvent()){
                this.onSelectionChanged(this.selectedFigure);
            }
        },this));
	},
	
    /**
     * @method
     * Called if the selection in the canvas has been changed. You must register this
     * class on the canvas to receive this event.
     * 
     * @param {draw2d.Figure} figure
     */
    onSelectionChanged : function(figure) { //console.log('onSelectionChanged', figure);
    	
        this.selectedFigure = figure;
        
        if(this.pane!==null){
            this.pane.onHide();
        }
        
        this.html.html("");

        if(figure===null)
        {
        	$("#shapeControls").hide();
        	$('#noteContainer').hide();
        	
            return;
        } 
        
        $("#shapeControls_settings").hide();
    	$("#shapeControls_preview").hide();
    	$("#shapeControls_help").hide(); 
    	$("#shapeControls_delete").hide();
    	$("#shapeControls_note").hide();
    	
    	$('#noteContainer').hide();
        
        if(figure instanceof vme2.shape.Start)
        {      	  
      	  //this.pane = new vme2.propertypane.PropertyPaneStart(figure);
        	
        	$("#shapeControls_settings").show();
        	$("#shapeControls_delete").show();   
        	$("#shapeControls_note").show();
        	
        	$('#shapeControls_settings').off('click');
    		$('#shapeControls_settings').on('click', $.proxy(function(e){
    			e.preventDefault();
    			
    			var compiled = Handlebars.compile(this.inputSettings_template);
    			var output = compiled(this.selectedFigure.userData);
    			
    			$('#inputSettingsBody').html(output);
    			
    			Materialize.updateTextFields();
    			$('select').material_select();
    			
    			var _this = this;
    			
    			$('#inputSettingsOk').off('click');
        		$('#inputSettingsOk').on('click', $.proxy(function(e){
        			e.preventDefault();
        			
        			var formFields = $("#inputDetails").serializeJSON(); 
        			
        			formFields.isConstant = $('#isConstant').is(':checked');
        	    	
        			_this.selectedFigure.setUserData(formFields);
        			
        			_this.selectedFigure.setLabel(formFields.label);
        			
        			$('#inputSettingsModal').closeModal();

        		}, _this));
    			
    			$('#inputSettingsModal').openModal();

    		}, this));
        } 
        else if(figure instanceof vme2.shape.End)
        {      	  
      	  //this.pane = new vme2.propertypane.PropertyPaneEnd(figure);
        } 
        else if(figure instanceof vme2.shape.Service)
        {      	  
      	  //this.pane = new vme2.propertypane.PropertyPaneService(figure);
        	
        	$("#shapeControls_preview").show();
        	$("#shapeControls_help").show(); 
        	$("#shapeControls_delete").show();
        	$("#shapeControls_note").show();
        	
        	$('#shapeControls_help').off('click');
    		$('#shapeControls_help').on('click', $.proxy(function(e){
    			e.preventDefault();
    			
    			var currDef = this.selectedFigure.getServiceDef();
    			//console.log(currDef);
    			
    			$.ajax({
    				url : Settings.baseUrl + Settings.serviceCatalog.baseUrl + '/' + currDef.operation.serviceId + '/operation/' + currDef.operation.id + '/specs',
    				method : 'GET',
    				beforeSend: function()
    				{
    				},
    				success : $.proxy(function(specs)
    				{
    					console.log(specs);    	
    					var compiled = Handlebars.compile(this.serviceSpecs_template);
    	    			var output = compiled(specs);
    	    			
    	    			$('#serviceSpecsContent').html(output);
    	    			$('#serviceSpecsTitle').html(specs.name);
    	    			
    	    			app.renderSchemas();
    	    			
    	    			$('#serviceSpecsModal').openModal();
    				}, this)
    			});

    		}, this));
        } 
        else if(figure instanceof vme2.shape.Operator)
        {
      	  //this.pane = new vme2.propertypane.PropertyPaneOperator(figure);
        	
        	$("#shapeControls_preview").show();
        	$("#shapeControls_help").show(); 
        	$("#shapeControls_delete").show();
        	$("#shapeControls_note").show();
        	
        	$('#shapeControls_help').off('click');
    		$('#shapeControls_help').on('click', $.proxy(function(e){ 
    			e.preventDefault();
    			
    			$("#operatorDocsModalContent").load(Settings.baseUrl + "/mashupeditor/operators/docs/" + this.selectedFigure.userData.type + ".html", function() {
    				$('#operatorDocsModal').openModal();
				});

    		}, this));
        } 
        else if(figure instanceof vme2.shape.Blackbox)
        {      	  
      	  //this.pane = new vme2.propertypane.PropertyPaneBlackbox(figure);      	  
        } 
        else if(figure instanceof vme2.shape.InnerProcess)
        {      	  
      	  //this.pane = new vme2.propertypane.PropertyPaneInnerProcess(figure);
        	
        	$("#shapeControls_delete").show();
        	$("#shapeControls_note").show();
        } 
        else if(figure instanceof vme2.shape.OpenData)
        {    	  
    	  //this.pane = new vme2.propertypane.PropertyPaneOpenData(figure);
        	
        	$("#shapeControls_settings").show();
        	$("#shapeControls_preview").show();
        	$("#shapeControls_help").show(); 
        	$("#shapeControls_delete").show();
        	$("#shapeControls_note").show();
        	
        	$('#shapeControls_settings').off('click');
    		$('#shapeControls_settings').on('click', $.proxy(function(e){
    			e.preventDefault();
    			
    			var currDef = this.selectedFigure.getOpenDataDef();
    			showQuery(currDef.resourceId, currDef.query);
    			
    			var _this = this;
    			
    			$('#queryModalPrimary').off('click');
        		$('#queryModalPrimary').on('click', $.proxy(function(e){
        			e.preventDefault();
        			
        			var query = $("#queryText").val().trim();
        			
        			var currDef = _this.selectedFigure.getOpenDataDef();
        			currDef.query = query;
        			
        			$('#queryModal').closeModal();

        		}, _this));

    		}, this));
    		
    		$('#shapeControls_help').off('click');
    		$('#shapeControls_help').on('click', $.proxy(function(e){
    			e.preventDefault();
    			
    			var currDef = this.selectedFigure.getOpenDataDef();
    			showSchema(currDef.resourceId);

    		}, this));
    	}
        
        $('#shapeControls_delete').off('click');
		$('#shapeControls_delete').on('click', $.proxy(function(e){
			e.preventDefault();
			
			app.toolbar.deleteCommand();
			
		}, this));
		
		$('#shapeControls_preview').off('click');
		$('#shapeControls_preview').on('click', $.proxy(function(e){
			e.preventDefault();
			
			app.toolbar.previewCommand();
			
		}, this));
		
		$('#shapeControls_note').off('click');
		$('#shapeControls_note').on('click', $.proxy(function(e) {
			
			e.preventDefault();
			
			var userData = this.selectedFigure.userData;
			
			var notes = '';
    		
    		if(userData.notes)
    		{
    			notes = userData.notes;
    		}
    		
    		$('#notesModalTextarea').val(notes);
    		$('#notesModalTextarea').trigger('autoresize');
    		Materialize.updateTextFields();
			
			var _this = this;
			
			$('#notesOk').off('click');
    		$('#notesOk').on('click', $.proxy(function(e){
    			e.preventDefault();
    			
    			var notes = $('#notesModalTextarea').val().trim();
    	    	
    			_this.selectedFigure.userData.notes = notes;
    			
    			$('#noteContainer p').html(notes);
    			
    			$('#notesModal').closeModal();

    		}, _this));
			
			$('#notesModal').openModal();
			
		}, this));
        
        app.updateFigureControlsPosition(this.selectedFigure, this.selectedFigure.canvas.getZoom());
        
        $(window).off("resize");
        $(window).resize($.proxy(function() {
        	app.updateFigureControlsPosition(this.selectedFigure, this.selectedFigure.canvas.getZoom());
		}, this));
        
        /*if(this.pane!==null){
            this.pane.injectPropertyView(this.html);
        }*/
    },
    
    onResize: function()
    {
        if(this.pane!==null){
            this.pane.onResize();
        }
    },
    
    shapeControls_settings: function()
    {
    	
    }
    
});

