vme2.shape.Blackbox = draw2d.shape.layout.VerticalLayout.extend({

	NAME: "vme2.shape.Blackbox",
			
    init : function()
    {
        this._super();
        // init the object with some good defaults for the activity setting.
        
        this.setUserData({});        
        
        this.outPort = this.createPort("output", new draw2d.layout.locator.OutputPortLocator());
  		  this.inPort = this.createPort("input",	new draw2d.layout.locator.InputPortLocator());
        
  		  this.setBackgroundColor("#99CCFF");

        // UI representation
        this.setStroke(1);
        this.setColor("#e0e0e0");
        this.setRadius(5);  
        
        // Compose the top row of the shape
        //
        var top = this.createLabel("Blackbox").setStroke(0);        
        this.label = top;
        
        // the middle part of the shape
        // This part contains the ports for the connection
        //
        var center =  new draw2d.shape.basic.Rectangle();  
        center.getHeight= function(){return 1;};
        center.setMinWidth(90);
        center.setColor("#e0e0e0");
        
        // the bottom of the activity shape
        //
        var bottom = this.createLabel("Blackbox");   
        this.activityLabel = bottom;
        bottom.setMinHeight(30);
        bottom.setStroke(0);
        bottom.setBackgroundColor(null);
        bottom.setFontColor("#a0a0a0");

        // finally compose the shape with top/middle/bottom in VerticalLayout
        //
        this.addFigure(top);
        this.addFigure(center);
        this.addFigure(bottom);        
     },
     
     /**
      * @method
      * Set the text to show if the state shape
      * 
      * @param {String} text
      */
     setLabel: function (text)
     {
         this.label.setText(text);
         
         return this;
     },
     
     
     /**
      * @method
      * Return the label of the shape
      * 
      */
     getLabel: function ()
     {
         return this.label.getText();
     },
     
     /**
      */
     setBlackboxDef: function (blackboxDef)
     {
    	 this.label.setText(blackboxDef.name);
    	 this.activityLabel.setText(blackboxDef.instanceId);    	     	 
    		 	
    	 this.setUserData(blackboxDef);
                  
         return this;
     },
     
     
     /**
      * 
      */
     getBlackboxDef: function ()
     {
         return this.getUserData();
     },
     
     
     /**
      * @method
      * helper method to create some labels
      * 
      * @param {String} txt the label to display
      * @returns {draw2d.shape.basic.Label}
      * @private
      */
     createLabel: function(txt){
    	 var label =new draw2d.shape.basic.Label(txt);
    	 label.setStroke(1);
    	 label.setRadius(0);
    	 label.setBackgroundColor(null);
    	 label.setPadding(5);
    	 label.setColor(this.bgColor.darker(0.2));
    	 label.onDoubleClick=function(angle){/* ignore them for the layout elements*/};
    	    
    	 return label;
     },
     
    /**
     * @method 
     * Return an objects with all important attributes for XML or JSON serialization
     * 
     * @returns {Object}
     */
    getPersistentAttributes : function()
    {
        var memento = this._super();

        memento.label = this.getLabel();
        //memento.hasInPort = this.hasInPort();
                
        return memento;
    },
    
    /**
     * @method 
     * Read all attributes from the serialized properties and transfer them into the shape.
     * 
     * @param {Object} memento
     * @returns 
     */
    setPersistentAttributes : function(memento)
    {
        this._super(memento);

        try{
            this.setLabel(memento.label);
            this.activityLabel.setText(this.getBlackboxDef().instanceId);  
            /*if ("hasInPort" in memento && memento.hasInPort === true) {
            	this.inPort = this.createPort("input", new	 draw2d.layout.locator.InputPortLocator());   			 
            } */           			 
        }
        catch(exc)
        {
     
        }
    }
});
