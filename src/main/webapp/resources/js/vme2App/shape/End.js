
/**
 * @class draw2d.shape.Start
 * 
 *  
 * @extends draw2d.shape.node.Start
 */
vme2.shape.End = draw2d.shape.node.End.extend({

	NAME: "vme2.shape.End",
	
    init : function()
    {
        this._super();
        
        var inputs = [];
        
        this.setUserData({inputs: inputs});
        
        this.setMinWidth(90); 
        
        this.installEditPolicy(new draw2d.policy.figure.AntSelectionFeedbackPolicy());
        this.setBackgroundColor("#f44336");
        this.setStroke(2);
		this.setColor("#000000");
		this.setRadius(2);

        this.label = new draw2d.shape.basic.Label("OUTPUT");
        this.label.setStroke(0);
        this.label.setFontColor("#000000");
        this.label.setBold(true);
        this.label.setFontSize("14");
        // add the new decoration to the connection with a position locator.
        //
        this.addFigure(this.label, new draw2d.layout.locator.CenterLocator(this));
     },
     onDoubleClick: function(){
    	 
     },     
     /**
      * @method
      * Set the text to show if the state shape
      * 
      * @param {String} text
      */
     setLabel: function (text)
     {
         this.label.setText(text);
         
         return this;
     },
     
     
     /**
      * @method
      * Return the label of the shape
      * 
      */
     getLabel: function ()
     {
         return this.label.getText();
     },

    /**
     * @method 
     * Return an objects with all important attributes for XML or JSON serialization
     * 
     * @returns {Object}
     */
    getPersistentAttributes : function()
    {
        var memento = this._super();

        memento.label = this.getLabel();
        
        return memento;
    },
    
    /**
     * @method 
     * Read all attributes from the serialized properties and transfer them into the shape.
     * 
     * @param {Object} memento
     * @returns 
     */
    setPersistentAttributes : function(memento)
    {
        this._super(memento);

        try{
            this.setLabel(memento.label);
        }
        catch(exc)
        {
     
        }
    }
});
