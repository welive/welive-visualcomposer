
/**
 * @class draw2d.shape.Service
 * 
 *  
 * @extends draw2d.shape.layout.VerticalLayout
 */
vme2.shape.Service = draw2d.shape.layout.VerticalLayout.extend({

	NAME: "vme2.shape.Service",
			
    init : function()
    {
        this._super();
        // init the object with some good defaults for the activity setting.
        
        this.setUserData({});        
        
        this.outPort = this.createPort("output", new draw2d.layout.locator.OutputPortLocator());
                
        this.setBackgroundColor("#ffffff");

        // UI representation
        this.setStroke(2);
        this.setColor("#000000");
        this.setRadius(2);
        
        // Compose the top row of the shape
        //
        var top = this.createLabel("Service").setStroke(0);        
        this.label = top;
        top.setPadding("8");
        top.setBold(true);
        top.setFontSize("14");
        
        // the middle part of the shape
        // This part contains the ports for the connection
        //
        var center =  new draw2d.shape.basic.Rectangle();  
        center.getHeight= function(){return 1;};
        center.setMinWidth(90);
        center.setColor("#e0e0e0");
        
        // the bottom of the activity shape
        //
        var bottom = this.createLabel("Service Operation");   
        this.activityLabel = bottom;
        bottom.setMinHeight(30);
        bottom.setStroke(0);
        bottom.setBackgroundColor(null);
        bottom.setFontColor("#000000");
        bottom.setPadding("8");

        // finally compose the shape with top/middle/bottom in VerticalLayout
        //
        this.addFigure(top);
        this.addFigure(center);
        this.addFigure(bottom);        
     },
     
     /**
      * @method
      * Set the text to show if the state shape
      * 
      * @param {String} text
      */
     setLabel: function (text)
     {
         this.label.setText(text);
         
         return this;
     },
     
     
     /**
      * @method
      * Return the label of the shape
      * 
      */
     getLabel: function ()
     {
         return this.label.getText();
     },
     
     /**
      */
     setServiceDef: function (serviceDef)
     {
    	 //this.label.setText(serviceDef.name +"-"+ serviceDef.operation.name);
    	 //this.activityLabel.setText(serviceDef.instanceId);    
    	 
    	 this.label.setText(serviceDef.operation.name);
    	 this.activityLabel.setText(serviceDef.name); 
    	 
		 if ($.isEmptyObject(serviceDef.operation.parameters) === false){
			 this.inPort = this.createPort("input", new	 draw2d.layout.locator.InputPortLocator());
			 //imposto i parametri al valore di default
			 $.each(serviceDef.operation.parameters, function(i, param){
				param.value = param.defaultValue; 
//				if (typeof param.value === "undefined" && "options" in param)
//					param.value = param.options[0];
			 });
		 }
		 /*
		 if(serviceDef.type == "restful")
		 {
			 this.setBackgroundColor("#42a5f5");
		 }	 */
    		 	
    	 this.setUserData(serviceDef);
                  
         return this;
     },
     
     
     /**
      * 
      */
     getServiceDef: function ()
     {
         return this.getUserData();
     },
     
     
     /**
      * @method
      * helper method to create some labels
      * 
      * @param {String} txt the label to display
      * @returns {draw2d.shape.basic.Label}
      * @private
      */
     createLabel: function(txt){
    	 var label =new draw2d.shape.basic.Label(txt);
    	 label.setStroke(1);
    	 label.setRadius(0);
    	 label.setBackgroundColor(null);
    	 label.setPadding(5);
    	 label.setColor(this.bgColor.darker(0.2));
    	 label.onDoubleClick=function(angle){/* ignore them for the layout elements*/};
    	    
    	 return label;
     },
     
     /**
      * @method
      * validate all regular expression from this connection and set a corresponding
      * color for the connection if any errors are in.
      * 
      */
     validate: function(){
         
     },
     
     hasInPort: function() {
    	 return this.inPort != null;
     },
     
     
     
    /**
     * @method 
     * Return an objects with all important attributes for XML or JSON serialization
     * 
     * @returns {Object}
     */
    getPersistentAttributes : function()
    {
        var memento = this._super();

        memento.label = this.getLabel();
        memento.hasInPort = this.hasInPort();
                
        return memento;
    },
    
    /**
     * @method 
     * Read all attributes from the serialized properties and transfer them into the shape.
     * 
     * @param {Object} memento
     * @returns 
     */
    setPersistentAttributes : function(memento)
    {
        this._super(memento);

        try{
            this.setLabel(memento.label);
            this.activityLabel.setText(this.getServiceDef().name);  
            if ("hasInPort" in memento && memento.hasInPort === true) {
            	this.inPort = this.createPort("input", new	 draw2d.layout.locator.InputPortLocator());   			 
            }            
            //this.setServiceDef(memento.userData);
			 
        }
        catch(exc)
        {
     
        }

    }
});
