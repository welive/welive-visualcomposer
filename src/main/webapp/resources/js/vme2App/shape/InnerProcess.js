vme2.shape.InnerProcess = draw2d.shape.layout.VerticalLayout.extend({

	NAME: "vme2.shape.InnerProcess",
			
    init : function()
    {
        this._super();
        // init the object with some good defaults for the activity setting.
        
        this.setUserData({});        
                
		  this.inPort = this.createPort("input", new draw2d.layout.locator.InputPortLocator());  		
		  this.inPort.addCssClass("redport");
		
		  this.setBackgroundColor("#e57373");

        // UI representation
  		this.setStroke(2);
		this.setColor("#000000");
		this.setRadius(2); 
        
        // Compose the top row of the shape
        //
        var top = this.createLabel("Inner Process").setStroke(0);        
        this.label = top;
        top.setFontColor("#000000");
		top.setPadding("8");
		top.setBold(true);
        top.setFontSize("14");
        
        // the middle part of the shape
        // This part contains the ports for the connection
        //
        var center =  new draw2d.shape.basic.Rectangle();  
        center.getHeight= function(){return 1;};
        center.setMinWidth(90);
        center.setColor("#b71c1c");
        
        // the bottom of the activity shape
        //
        var bottom = this.createLabel("Inner Process");   
        this.activityLabel = bottom;
        bottom.setMinHeight(30);
        bottom.setStroke(0);
		bottom.setBackgroundColor(null);
		bottom.setFontColor("#000000");
        bottom.setPadding("8");

        // finally compose the shape with top/middle/bottom in VerticalLayout
        //
        this.addFigure(top);
        this.addFigure(center);
        this.addFigure(bottom);        
     },
     
     /**
      * @method
      * Set the text to show if the state shape
      * 
      * @param {String} text
      */
     setLabel: function (text)
     {
         this.label.setText(text);
         
         return this;
     },
     
     
     /**
      * @method
      * Return the label of the shape
      * 
      */
     getLabel: function ()
     {
         return this.label.getText();
     },
     
     /**
      */
     setInnerProcessDef: function (innerProcessDef)
     {
    	 this.label.setText(innerProcessDef.name);
    	 this.activityLabel.setText("Inner Process");    	     	 
    		 	
    	 this.setUserData(innerProcessDef);
                  
         return this;
     },
     
     
     /**
      * 
      */
     getInnerProcessDef: function ()
     {
         return this.getUserData();
     },
     
     
     /**
      * @method
      * helper method to create some labels
      * 
      * @param {String} txt the label to display
      * @returns {draw2d.shape.basic.Label}
      * @private
      */
     createLabel: function(txt){
    	 var label =new draw2d.shape.basic.Label(txt);
    	 label.setStroke(1);
    	 label.setRadius(0);
    	 label.setBackgroundColor(null);
    	 label.setPadding(5);
    	 label.setColor(this.bgColor.darker(0.2));
    	 label.onDoubleClick=function(angle){/* ignore them for the layout elements*/};
    	    
    	 return label;
     },
     
    /**
     * @method 
     * Return an objects with all important attributes for XML or JSON serialization
     * 
     * @returns {Object}
     */
    getPersistentAttributes : function()
    {
        var memento = this._super();

        memento.label = this.getLabel();
        //memento.hasInPort = this.hasInPort();
                
        return memento;
    },
    
    /**
     * @method 
     * Read all attributes from the serialized properties and transfer them into the shape.
     * 
     * @param {Object} memento
     * @returns 
     */
    setPersistentAttributes : function(memento)
    {
        this._super(memento);

        try{
            this.setLabel(memento.label);
            this.activityLabel.setText("Inner Process");            			 
        }
        catch(exc)
        {
     
        }
    }
});
