/**
 * @class draw2d.shape.Operator
 * 
 *  
 * @extends draw2d.shape.layout.VerticalLayout
 */
vme2.shape.Operator = draw2d.shape.layout.VerticalLayout.extend({

	NAME : "vme2.shape.Operator",
	OPERANDS : [ '=', '<', '<=', '>', '>=', '!=', 'includes', 'not includes', 'matches' ],
	
	init : function(operatorId) {

		this._super();	

		// the bottom of the activity shape
		//
		var bottom = this.createLabel("Service Operation");
		this.activityLabel = bottom;
		bottom.setMinHeight(30);
		bottom.setStroke(0);
		bottom.setBackgroundColor(null);
		bottom.setFontColor("#000000");
        bottom.setPadding("8");
        
        // Compose the top row of the shape
		//
		var top = this.createLabel("Operator").setStroke(0);
		this.label = top;
		top.setFontColor("#000000");
		top.setPadding("8");
		top.setBold(true);
        top.setFontSize("14");
		
		if (operatorId !== undefined) {
			this.operatorId = operatorId;
			
			$.ajaxSetup({
				async : false
			});
			app.getBackend().getOperator(operatorId, $.proxy(function(data) {

				this.setUserData(data);				
				this.label.setText(data.name);
				this.activityLabel.setText(app.operatorCategories[operatorId] + " Operator");
			}, this));
			$.ajaxSetup({
				async : true
			});
		}
		
		this.outPort = this.createPort("output",
				new draw2d.layout.locator.OutputPortLocator());
		
		//if(!this.getOperatorDef().noinputport){
   		this.inPort = this.createPort("input",
   				new draw2d.layout.locator.InputPortLocator());
		//}
		this.setBackgroundColor("#ffd600");

		// UI representation
		this.setStroke(2);
		this.setColor("#000000");
		this.setRadius(2);

		

		// the middle part of the shape
		// This part contains the ports for the connection
		//
		var center = new draw2d.shape.basic.Rectangle();
		center.getHeight = function() {
			return 1;
		};
		center.setMinWidth(90);
		center.setColor("#ffab00");

		// finally compose the shape with top/middle/bottom in VerticalLayout
		//
		this.addFigure(top);
		this.addFigure(center);
		this.addFigure(bottom);
	},

	/**
	 * @method
	 * Set the text to show if the state shape
	 * 
	 * @param {String} text
	 */
	setLabel : function(text) {
		this.label.setText(text);

		return this;
	},

	/**
	 * @method
	 * Return the label of the shape
	 * 
	 */
	getLabel : function() {
		return this.label.getText();
	},
	
	getOperatorDef : function() {
		return this.getUserData();
	},

//	getCustomParamSize : function(){
//		return (this.userData !==undefined && this.userData!==null) ? this.userData.parameters.length :0;
//	},
	
	getOperands : function() {
		return this.OPERANDS;
	},
	
	getAvailableProperty : function(path, callback){
		var treeNodeDetail = app.getBackend().getTreeNodeDetail(path, callback);			
		
		var tree2 = treeNodeDetail;		
	},
	/**
	 * @method
	 * helper method to create some labels
	 * 
	 * @param {String} txt the label to display
	 * @returns {draw2d.shape.basic.Label}
	 * @private
	 */
	createLabel : function(txt) {
		var label = new draw2d.shape.basic.Label(txt);
		label.setStroke(1);
		label.setRadius(0);
		label.setBackgroundColor(null);
		label.setPadding(5);
		label.setColor(this.bgColor.darker(0.2));
		label.onDoubleClick = function(angle) {/* ignore them for the layout elements*/
		};

		return label;
	},

	/**
	 * @method
	 * validate all regular expression from this connection and set a corresponding
	 * color for the connection if any errors are in.
	 * 
	 */
	validate : function() {

	},

	hasInPort : function() {
		return this.inPort != null;
	},

	/**
	 * @method 
	 * Return an objects with all important attributes for XML or JSON serialization
	 * 
	 * @returns {Object}
	 */
	getPersistentAttributes : function() {
		var memento = this._super();

		memento.label = this.getLabel();
		memento.hasInPort = this.hasInPort();

		return memento;
	},

	/**
	 * @method 
	 * Read all attributes from the serialized properties and transfer them into the shape.
	 * 
	 * @param {Object} memento
	 * @returns 
	 */
	setPersistentAttributes : function(memento) {
		this._super(memento);

		try {		
			this.label.setText(this.getOperatorDef().name); 
			this.activityLabel.setText(app.operatorCategories[this.getOperatorDef().type] + " Operator");  		
			//if ("hasInPort" in memento && memento.hasInPort === true) {
			//	this.inPort = this.createPort("input", new	 draw2d.layout.locator.InputPortLocator());   			 
			//}                       			 
		} catch (exc) {
			console.log(exc);
		}
	}
});
