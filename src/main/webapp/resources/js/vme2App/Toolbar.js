
vme2.Toolbar = Class.extend({

	_ckanPaginationTemplate:
		'<div>'+
		'<button class="btn btn-mini prev{{#if disableprev}} disabled{{/if}}">'+
		'	<i class="icon-chevron-left"></i>'+
		'</button>'+
		'<span style="margin: auto 5px;">{{currentpage}}/{{pages}}</span>'+
		'<button class="btn btn-mini next{{#if disablenext}} disabled{{/if}}">'+
		'	<i class="icon-chevron-right"></i>'+
		'</button>'+
		'</div>',	
	
	_ckanResultTemplate: 
		'	<table class="table table-striped table-hover">'+
		'		<thead><tr><th>Title</th><th>Author</th><th>License</th><th/></tr></thead>'+
		'		<tbody>'+
		'		{{#each data}}'+
		'			<tr>'+
		'				<td>'+		
		'					<a href="{{../ckanServerUrl}}/dataset/{{name}}" target="_blank" title="Go to dataset ckan server page">{{#if title}}{{title}}{{else}}{{name}}{{/if}}</a>'+
		'				</td>'+
		'				<td>{{author}}</td>'+
		'				<td>'+
		'					{{#if license_url}}'+
		'						<a href="{{license_url}}" target="_blank" title="Go to license details">{{license_title}}</a>'+
		'					{{else}}'+
		'						{{license_title}}'+
		'					{{/if}}'+
		'				</td>'+		
		'				<td><a href="#" data-toggle="myCollapse" data-target=".{{id}}" class="btn btn-link icon-chevron-right"></a></td>'+		
		'			</tr>'+
		'			{{#if resources}}'+
		'				{{#each resources}}'+		
		'				<tr data-datasetid="{{../id}}" data-resourceid="{{id}}" class="{{../id}} myCollapse">'+
		'					<td colspan="3">'+
		'						<div style="padding-left:20px">'+
		'							<span class="label label-warning">{{format}}</span>'+		
		'							{{name}}{{#if description}}: {{description}}{{/if}}'+
		'						</div>'+
		'					</td>'+
		'					<td>'+
		'						{{#ifeq format "JSON"}}'+
		'							<button href="#" class="btn" title="Click to save opendata resource"><i class="icon-download-alt"/></button>'+
		'						{{else}}'+
		'							{{#ifeq format "CSV"}}'+
		'								<button href="#" class="btn" title="Click to save opendata resource"><i class="icon-download-alt"/></button>'+
		'							{{else}}'+				
		'								{{#ifeq format "XML"}}'+
		'									<button href="#" class="btn" title="Click to save opendata resource"><i class="icon-download-alt"/></button>'+
		'								{{else}}'+		
		'									<button href="#" class="btn" disabled="disabled" title="Not supported format"><i class="icon-download-alt"/></button>'+		
		'								{{/ifeq}}'+
		'							{{/ifeq}}'+	
		'						{{/ifeq}}'+			
		'					</td>'+
		'				</tr>'+
		'				{{/each}}'+		
		'			{{/if}}'+		
		'		{{/each}}'+
		'		</tbody>'+
		'	</table>',
	
	init:function(elementId, view){
		this.html = $("#"+elementId);
		this.view = view;

		this._ckanPageSearch = 1;
		this._ckanPageSize = 10;
		
		// register this class as event listener for the canvas
		// CommandStack. This is required to update the state of 
		// the Undo/Redo Buttons.
		//
		view.getCommandStack().addEventListener(this);

		// Register a Selection listener for the state handling
		// of the Delete Button
		//
		view.addSelectionListener(this);				

		this.saveButton  = $(".saveButton");
		this.saveButton.click($.proxy(function(){
			app.saveDefinition(false);
		},this)).attr("disabled",true);       


		// Inject the UNDO Button and the callbacks
		//
		this.undoButton  = $(".undoButton");
		this.undoButton.click($.proxy(function(){
			this.view.getCommandStack().undo();
		},this)).attr("disabled",true);

		// Inject the REDO Button and the callback
		//
		this.redoButton  = $(".redoButton");
		this.redoButton.click($.proxy(function(){
			this.view.getCommandStack().redo();
		},this)).attr( "disabled", true );	


		// Inject the Zoom In Button
		this.zoomInButton  = $(".zoomInButton");
		this.zoomInButton.click($.proxy(function(){	
			
			if(this.view.getZoom() == 0.25) return;
			
			this.view.setZoom(this.view.getZoom() - 0.25, true);	

			if(app.propertyPane && app.propertyPane.selectedFigure)
			{
				$("#shapeControls").hide();
				var nextZoom = this.view.getZoom() - 0.25;
				setTimeout($.proxy(function(){ app.updateFigureControlsPosition(app.propertyPane.selectedFigure, nextZoom); }, this), 500);				
			}
		},this));

		// Inject the RESET Zoom Button
		//
		this.resetZoomButton  = $(".resetZoomButton");
		this.resetZoomButton.click($.proxy(function(){
			this.view.setZoom(1.0, true);
			
			if(app.propertyPane && app.propertyPane.selectedFigure)
			{
				$("#shapeControls").hide();
				var nextZoom = 1;
				setTimeout($.proxy(function(){ app.updateFigureControlsPosition(app.propertyPane.selectedFigure, nextZoom); }, this), 500);				
			}
			
		},this));

		// Inject the Zoom OUT Button and the callback
		//
		this.zoomOutButton  = $(".zoomOutButton");
		this.zoomOutButton.click($.proxy(function(){	
			
			if(this.view.getZoom() == 2) return;
			
			this.view.setZoom(this.view.getZoom() + 0.25, true);

			if(app.propertyPane && app.propertyPane.selectedFigure)
			{
				$("#shapeControls").hide();
				var nextZoom = this.view.getZoom() + 0.25;
				setTimeout($.proxy(function(){ app.updateFigureControlsPosition(app.propertyPane.selectedFigure, nextZoom); }, this), 500);				
			}
		},this));

		// Inject the DELETE Button
		//
		this.deleteButton  = $(".deleteButton");		
		this.deleteButton.click($.proxy(function(){this.deleteCommand();}, this)).attr("disabled", true );

		this.previewButton  = $(".previewButton");
		this.previewButton.click($.proxy(function(){this.previewCommand();}, this));
		
		this.ckanSearchButton  = $("#ckanSearchButton");
		this.ckanSearchButton.click($.proxy(function(e){
			e.preventDefault();
			this.ckanSearch();
		}, this));
		
		this.ckanGetAllButton = $("#ckanGetAllButton");
		this.ckanGetAllButton.click($.proxy(function(e){
			e.preventDefault();
			this._ckanPageSearch = 1;
			this.ckanGetAll();
		}, this));
		
		// Inject the INVOKE Button and the callback
		//
		/*this.invokeButton  = $(".invokeButton");
		this.invokeButton.click($.proxy(function(){
			app.buildInvokeWorkflowWindow();
		},this)).attr("disabled",false);*/ 
		
		$(window).bind('beforeunload', $.proxy(function() { 
			var disabled = this.saveButton.attr("disabled");
			if (!disabled){
				return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
			}
		},this));				
	},

	/**
	 * @method
	 * Called if the selection in the canvas has been changed. You must register this
	 * class on the canvas to receive this event.
	 * 
	 * @param {draw2d.Figure} figure
	 */
	onSelectionChanged : function(figure){
		this.deleteButton.attr( "disabled", figure===null || figure instanceof vme2.shape.End);
	},

	/**
	 * @method
	 * Sent when an event occurs on the command stack. draw2d.command.CommandStackEvent.getDetail() 
	 * can be used to identify the type of event which has occurred.
	 * 
	 * @template
	 * 
	 * @param {draw2d.command.CommandStackEvent} event
	 **/
	stackChanged:function(event)
	{
		this.undoButton.attr("disabled", !event.getStack().canUndo() );
		this.redoButton.attr("disabled", !event.getStack().canRedo() );

		this.saveButton.attr("disabled",   !event.getStack().canUndo() && !event.getStack().canRedo()  );
	},

	deleteCommand: function(){
		var commandsToExecute = new draw2d.command.CommandCollection();
		
		this.view.getSelection().getAll().each($.proxy(function(j, node){
			var customCommand= new vme2.command.CommandDelete(node);
			if(customCommand.canExecute()){
				var commandCollection = customCommand.getCommandCollectionIfRequired();
				if(commandCollection!=null && commandCollection!=undefined){
					commandsToExecute.add(commandCollection);					
				}else{
					commandsToExecute.add(customCommand);
				}
			}				
		}, this));
		
		if(commandsToExecute.canExecute()){		
			this.view.getCommandStack().execute(commandsToExecute);
		}
	},
	
	previewCommand: function(){		
		
		var _this = this;
		
		app.saveDefinition(true, $.proxy(function() {
		
			var selectedFigure = _this.view.getSelection().getPrimary();				
			
			if(selectedFigure instanceof vme2.shape.Start || selectedFigure instanceof vme2.shape.Connection || selectedFigure instanceof vme2.shape.InnerProcess)
			{
				bootbox.alert("Cannot invoke preview for selected node!");
			} 
			else 
			{
			
		   		if(selectedFigure===null || selectedFigure===undefined)
		   		{
		   			selectedFigure = _this.view.getOutputShape();
		   		}
		   		
		   		var isServiceChain = false;
		   		var figureUserData = null;
		   		var figureId = selectedFigure.getId();
		   		
		   		if(!(selectedFigure instanceof vme2.shape.End))
		   		{			
		   			figureUserData = selectedFigure.getUserData();
		   			isServiceChain = true;
		   		}				
		   		
		   		app.buildInvokeWorkflowWindow(isServiceChain, figureId, $.proxy(function(data) {
		   			
		   			buildTreePanel(null, data, figureUserData, true); 			
		   			
		   			var container = $('#previewModal');
		   			
		   			$(document).ready(function(){
		   				$('ul.tabs').tabs();
		   			});
		   			
		   			$(container).openModal();
		   			
		   		}, _this));
		   	}
			
		}, this));
	},
	
	ckanSearch: function(){		
		$('#wait').show();
		$('#ckanPagination').html("");	
		
		var form = $('#ckanSearch');
		
		var query = $("input[name='query']", form).val();
		var ckanServerId = $("select[name='ckanServer']", form).val();
		this.ckanServerUrl = $("select[name='ckanServer'] option:selected", form).data('url');
	
		app.backend.ckanSearch(ckanServerId, query, $.proxy(this.ckanCb, this));					
	},

	ckanGetAll: function(){		
		$('#wait').show();					
		
		var form = $('#ckanSearch');
		
		$("input[name='query']", form).val("");
				
		var ckanServerId = $("select[name='ckanServer']", form).val();
		this.ckanServerUrl = $("select[name='ckanServer'] option:selected", form).data('url');
	
		app.backend.ckanGetAll(ckanServerId, this._ckanPageSearch, $.proxy(this.ckanGetAllCb, this));		
	},	
	
	ckanCb: function(data){
		var infoTemplCompiled = Handlebars.compile(this._ckanResultTemplate);
		var ckanResult = $(infoTemplCompiled({"data": data, "ckanServerUrl":this.ckanServerUrl}));

		$("[data-toggle=myCollapse]", ckanResult).click(function( ev ) {
		   ev.preventDefault();
		   var target;
		   if (this.hasAttribute('data-target')) {
		   	target = $(this.getAttribute('data-target'));
		   } else {
		   	target = $(this.getAttribute('href'));
		   };
		   target.toggleClass("in");
		   $(ev.target).toggleClass("icon-chevron-right icon-chevron-down");
		 });

		$('button', ckanResult).on('click', $.proxy(this.importOpendataResource, this));						
		
		$('#ckanResultList').html(ckanResult);	
		$('#wait').hide();
	},
	
	ckanGetAllCb: function(data){
		this.ckanCb(data.results);			
		
		var disableprev = false;
		if(this._ckanPageSearch===1){
			disableprev = true;
		}
		
		var pages = Math.ceil(data.count/this._ckanPageSize);
		
		var disablenext = false;
		if(pages == this._ckanPageSearch){
			disablenext = true;
		}
		
		var obj = {};
		obj.disableprev = disableprev;
		obj.disablenext = disablenext;
		obj.currentpage = this._ckanPageSearch;
		obj.pages = pages;
		
		var templCompiled = Handlebars.compile(this._ckanPaginationTemplate);
		var result = $(templCompiled(obj));

		$('button.prev', result).click($.proxy(function(event){
			event.preventDefault();
			
			this._ckanPageSearch--;
			this.ckanGetAll();
		}, this));
		
		$('button.next', result).click($.proxy(function(event){
			event.preventDefault();
			
			this._ckanPageSearch++;
			this.ckanGetAll();
		}, this));
		
		
		$('#ckanPagination').html(result);		
	},
	
	importOpendataResource: function(event){
		$('#wait').show();
		var item = event.target;
		
		var parentTr = $(item).closest("tr");
		
		var datasetId = $(parentTr).data("datasetid");
		var resourceId = $(parentTr).data("resourceid");
		
		var form = $('#ckanSearch');				
		var ckanServerId = $("select[name='ckanServer']", form).val();

		app.backend.addCkanOpendataResource(ckanServerId, datasetId, resourceId, this.importOpendataResourceCb);
	}, 
	
	importOpendataResourceCb : function(){
		
		app.loadOpenData();
		$('#wait').hide();
		$('#ckanSearchModal').modal('hide');
	}
	
});