

vme2.View = draw2d.Canvas.extend({
	
	init:function(id, pid){
		this._super(id);
		
		this.setScrollArea("#"+id);
		this.installEditPolicy(new draw2d.policy.canvas.CoronaDecorationPolicy());
		this.installEditPolicy(new draw2d.policy.canvas.SnapToGridEditPolicy());
		this.currentDropConnection = null;
		
		this.projectId = pid;
		
		// override the default keyUp method to avoid "delete" of elements via ENTF key
		//this.onKeyDown= function(){};		
	},

    /**
     * @method
     * Called if the DragDrop object is moving around.<br>
     * <br>
     * Graphiti use the jQuery draggable/droppable lib. Please inspect
     * http://jqueryui.com/demos/droppable/ for further information.
     * 
     * @param {HTMLElement} droppedDomNode The dragged DOM element.
     * @param {Number} x the x coordinate of the drag
     * @param {Number} y the y coordinate of the drag
     * 
     * @template
     **/
    onDrag:function(droppedDomNode, x, y )
    {
    },
    
    /**
     * @method
     * Called if the user drop the droppedDomNode onto the canvas.<br>
     * <br>
     * Graphiti use the jQuery draggable/droppable lib. Please inspect
     * http://jqueryui.com/demos/droppable/ for further information.
     * 
     * @param {HTMLElement} droppedDomNode The dropped DOM element.
     * @param {Number} x the x coordinate of the drop
     * @param {Number} y the y coordinate of the drop
     * @private
     **/
    onDrop : function(droppedDomNode, x, y)
    {	
        var type = $(droppedDomNode).data("shape");
        
        if (type=="vme2.shape.Service")
        {
        	// service catalog id
   	    	var svcId = $(droppedDomNode).data("svc");
   	    	var opId = $(droppedDomNode).data("op");
   	    	
   	    	var service = app.getService(svcId);
			//clono service
			var serviceDef = $.extend(true, {}, service);
   	    	
   	    	//(new vme2.dialog.AddServiceDialog(svcId, this, x, y)).show();	 
			
			// IVAN
			// add operation to canvas
			app.getBackend().getOperations(svcId, 
				$.proxy(function(operations) {
					
					var operation;
					
					for(var i = 0; i < operations.length; i++)
					{
						if(operations[i].id == opId)
						{
							operation = operations[i];
							break;
						}
					}
					
					var _this = this;
					
					app.getBackend().getOperationParams(operation.id, operation.serviceType, $.proxy(function(data) {
						
						operation.parameters = data;
						serviceDef.operation = operation;
						_this.addShapeService(serviceDef, x, y);
						
					}, _this));
					
				}, this)
			);
            
       	} 
        else if (type=="vme2.shape.OpenData")
        {       		
       		// IVAN
       		var svcId = $(droppedDomNode).data("svc");
       		var resId = $(droppedDomNode).data("res");
       		
       		var service = app.getOpenData(svcId);
			//clono service
			var serviceDef = $.extend(true, {}, service);
       		//(new vme2.dialog.AddOpendataDialog(svcId, this, x, y)).show();	
       		
       		// IVAN
			// add operation to canvas
			app.getBackend().getResources(svcId, 
				$.proxy(function(resources) {
					
					var resource;
					
					for(var i = 0; i < resources.length; i++)
					{
						if(resources[i].id == resId)
						{
							resource = resources[i];
							break;
						}
					}
					
					serviceDef.resourceId = resource.id;
					serviceDef.resourceTitle = resource.title;
					(new vme2.dialog.AddQueryDialog(serviceDef, this, x, y)).show();	
					
					/*
					var _this = this;
					
					app.getBackend().getOperationParams(operation.id, operation.serviceType, $.proxy(function(data) {
						
						operation.parameters = data;
						serviceDef.operation = operation;
						_this.addShapeService(serviceDef, x, y);
						
					}, _this));
					*/
				}, this)
			);           
            
       	}        
        else if (type=="vme2.shape.End"){
   			var output = this.getFigure("output");
   			
   			if(output==undefined || output==null){
   				this.createOutputNode();
   			}else{
   				bootbox.alert("Output node exists!");
   			}    	 
       	} else if (type=="vme2.shape.Start"){
       		var figure = eval("new "+type+"();");    		
       		var prefix = $(droppedDomNode).data("idprefix");
       		var id = this._createInstanceId(prefix);	    	
   	    	figure.id = id;
   	    	figure.setUserData({label:id, inputType:'text'});	    	
           	var command = new draw2d.command.CommandAdd(this, figure, x, y);
               this.getCommandStack().execute(command);
               
       	} else if (type.indexOf("vme2.shape.Operator") != -1){
   			require(["app/"+type], $.proxy(function(Operator){
   	    		
   				var figure = new Operator(type);
   				
   	    		var prefix = $(droppedDomNode).data("idprefix");
   	    		var id = this._createInstanceId(prefix);	    	
   		    	figure.id = id;
   		    	
   		    	figure.getOperatorDef().instanceId = id;	
   		    	
   	        	var command = new draw2d.command.CommandAdd(this, figure, x, y);
               this.getCommandStack().execute(command);
   			}, this));
   		
       	} else if (type=="vme2.shape.Blackbox"){
       		var figure = eval("new "+type+"();");
       		
   	    	var svcId = $(droppedDomNode).data("svc");	
   	    	var blackbox = app.getBlackbox(svcId);
   	    	
       		var prefix = $(droppedDomNode).data("idprefix");
       		var id = this._createInstanceId(prefix);
       		
       		figure.id = id;
       		blackbox.instanceId = id
       		   		   	    	   	    
   	    	figure.setBlackboxDef(blackbox);	 
   	    	
	        	var command = new draw2d.command.CommandAdd(this, figure, x, y);
            this.getCommandStack().execute(command);
            
       	} else if (type=="vme2.shape.InnerProcess"){
       		var figure = eval("new "+type+"();");
       		
   	    	var svcId = $(droppedDomNode).data("svc");	
   	    	var innerprocess = app.getInnerProcess(svcId);
   	    	
       		var prefix = $(droppedDomNode).data("idprefix");
       		var id = this._createInstanceId(prefix);
       		
       		figure.id = id;
       		innerprocess.instanceId = id
       		   		   	    	   	    
   	    	figure.setInnerProcessDef(innerprocess);	 
   	    	
	        	var command = new draw2d.command.CommandAdd(this, figure, x, y);
            this.getCommandStack().execute(command);
       	} 
       	else
       	{
       		var figure = eval("new "+type+"();");
       		
       		var prefix = $(droppedDomNode).data("idprefix");
       		var id = this._createInstanceId(prefix);	    	
   	    	figure.id = id;
   	    	
           	var command = new draw2d.command.CommandAdd(this, figure, x, y);
               this.getCommandStack().execute(command);
       	}
    },
    onKeyDown: function(keyCode, ctrl){
    	if(keyCode==46){
//			var node = this.getCurrentSelection();
//			var customCommand= new vme2.command.CommandDelete(node);
//			this.getCommandStack().execute(customCommand);
    		app.toolbar.deleteCommand();
    	}
    },  
    
    rebindDraggable: function ()
    {
    	$(".draw2d_droppable").draggable({
            appendTo:"body",
            stack:"body",
            zIndex: 27000,
            helper:"clone",
            drag: $.proxy(function(event, ui){ 
                event = this._getEvent(event);
                var pos = this.fromDocumentToCanvasCoordinate(event.clientX, event.clientY);
                this.onDrag(ui.draggable, pos.getX(), pos.getY());
            },this),
            stop: function(e, ui){ 
                this.isInExternalDragOperation=false;
            },
            start: function(e, ui){ 
                this.isInExternalDragOperation=true;
                $(ui.helper).addClass("shadow");
            }
       });
    },
    
	addShapeService: function(serviceDef, x, y){
		var operationName = serviceDef.operation.name;
		var re = new RegExp("[^A-Za-z0-9]", 'g');
		operationName = operationName.replace(re, '_');
		var instanceId = this._createInstanceId(operationName+serviceDef.id);						
		var figure = new vme2.shape.Service();
		figure.id = instanceId;
		serviceDef.instanceId = instanceId;
		figure.setServiceDef(serviceDef);
		//console.log(figure);
		var command = new draw2d.command.CommandAdd(this, figure, x, y);
		this.getCommandStack().execute(command);
	},
	
	addShapeOpenData: function(serviceDef, x, y)
	{
		//console.log("addShapeOpendata");
		//console.log(serviceDef);
		var operationName = serviceDef.resourceTitle;
		var re = new RegExp("[^A-Za-z0-9]", 'g');
		operationName = operationName.replace(re, '');
		var instanceId = this._createInstanceId(operationName);						
		var figure = new vme2.shape.OpenData();
		figure.id = instanceId;
		serviceDef.instanceId = instanceId;
		figure.setOpenDataDef(serviceDef);
		//console.log(figure);
		var command = new draw2d.command.CommandAdd(this, figure, x, y);
		this.getCommandStack().execute(command);
	},
	
	_createInstanceId: function(prefix){
		var i=0;
		var id = prefix + "_" + i + "_" + this.projectId;		
		while(this.getFigure(id) != null){
			i++;
			id = prefix + "_" + i + "_" + this.projectId;			
		}
		return id;		
	},
	
	getInputShapes : function(){
		var inputShapes = [];
		var figures = this.getFigures();
		figures.each(function(i,e){
          if(e instanceof vme2.shape.Start){
        	  inputShapes.push(e);	              
           }
		});
	    return inputShapes;
	},
	getOutputShape: function(){
		var outputShape = null;
		var figures = this.getFigures();
		figures.each(function(i,e){
          if(e instanceof vme2.shape.End){
         	 outputShape = e;
         	 return false;
          }
		});
    return outputShape;
	},	
	
	/**
	 * Metodo ricorsivo che cattura i nodi root a partire da uno shape id
	 */
	getFigureRoots : function(id)
	{
		var sourceArr = [];
		
		var figure =this.getFigure(id);
		//console.log(figure.userData);
		var connections = figure.getConnections();		
		//console.log(connections);		
		
		if(connections!==null && connections!==undefined)
		{
   		for(var i=0; i<connections.size; i++)
   		{
   			var sourceId = connections.data[i].userData.sourceId;
   			var targetId = connections.data[i].userData.targetId;
   			
   			if(id===targetId)
   			{
   				var resultArr = this.getFigureRoots(sourceId);
   				$.merge(sourceArr,resultArr);
   			} 
   			else 
   			{
   				sourceArr.push(id);
   			}
   		}
		} else {
			sourceArr.push(id);
		}
		
		return sourceArr;
	},
	
	createOutputNode: function(){
		//Putting output figure
		var figure = new vme2.shape.End();					    
    	figure.id = "output";    	
    	this.addFigure(figure, 20, 80);
	},
	createInputNode: function(){
		//Putting input figure
		var figure = new vme2.shape.Start();		
		var id = this._createInstanceId("input");	    	
    	figure.id = id;    
    	figure.setUserData({label:id, inputType:'text'});
    	this.addFigure(figure, 20, 20);
	}	
});

