vme2.dialog.AddOpendataDialog = Class.extend({
	_template : 
		'	<table id="resourceList" class="table table-striped table-hover">'+
		'		<thead><tr><th>Name</th></th><th>Description</th><th>Schema</th></tr></thead>'+
		'		<tbody>'+
		'		{{#each this}}'+
		'			<tr data-id="{{id}}" data-title="{{title}}">'+
		'				<td>{{title}}</td>'+
		'				<td>{{description}}</td>'+
		'				<td><a class="btn btn-small btn-primary" onclick="javascript:showSchema(\'{{id}}\')"><i class="icon-table"></i></a></td>'+
		'			</tr>'+		
		'		{{/each}}'+
		'		</tbody>'+
		'	</table>',
		
		init : function(opendataId, canvas, x, y) {
			var service = app.getOpenData(opendataId);
			//clono service
			this.serviceDef = $.extend(true, {}, service);//console.log(this.serviceDef);
			this.x = x;
			this.y = y;
			this.canvas = canvas;

			this.compiledTemplate = Handlebars.compile(this._template);
			this.container = $('#odResourcesModalResourceListSection');
			this.modalWin = $('#odResourcesModal');
		},

		show : function() {
			$("#odResourcesModalLabel").text("Select '"+ this.serviceDef.name+ "' resource");
			$('#odResourcesModalFilterSection').html(
					'<div>'+				
					'	<form class="form-horizontal">'+
					'		<div class="control-group">'+
					'			<label class="control-label" for="resourceFilter">Operations filter</label>'+
					'			<div class="controls">'+
					'				<input class="paramInput" class="input-small" placeholder="Enter a word to filter operations" name="operationFilter" id="operationFilter" type="text">'+
					'			</div>'+
					'		</div>'+
					'	</form>'+
			'</div>');
			
			var footer = '<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>'+
			'<button class="btn btn-primary">OK</button>';

			this.modalWin.find('.modal-footer').html(footer);
			this.modalWin.find('.btn-primary').on('click', $.proxy(function(e) {
				e.preventDefault();       
				var row = $(this.container.find("tr.success"));
				var id = row.data("id");    
				var title = row.data("title"); 
				this._onOk(id,title);
			},this)).attr("disabled",true);


			app.getBackend().getResources(this.serviceDef.id, $.proxy(function(data) {
				
				//console.log(data);
				
				this.resources = data;			         			         

				var output = this.compiledTemplate(this.resources);
				this.container.html(output); 

				this.modalWin.modal();

				$(".docPop").popover();

				var self = this;
				this.container.find("tr").dblclick(function(){
					var row = $(this);
					var id = row.data("id");    
					var title = row.data("title"); 
					self._onOk(id,title);
				});

				this.container.find("tr").click(function(){
					$this = $(this);
					$this.addClass('success').siblings().removeClass('success');
					self.modalWin.find('.modal-footer').find('.btn-primary').attr("disabled",false);
				});   			
				
				$('#resourceList').filterTable({ // apply filterTable to all tables on this page
	            inputSelector: '#resourceFilter', // use the existing input instead of creating a new one
	            minRows:2
	        });
				
			}, this));
		},

		_onOk : function(resourceId, resourceTitle) {
			console.log("OK1");
			this.serviceDef.resourceId = resourceId;
			this.serviceDef.resourceTitle = resourceTitle;
			(new vme2.dialog.AddQueryDialog(this.serviceDef, this.canvas, this.x, this.y)).show();	
			
			this.modalWin.modal('hide');				
		},
		
//		_onUseTemplate : function(operationId) {
//
//			var opAr = $.grep(this.operations, function(op){
//				return op.id == operationId;
//			});
//
//			if (opAr.length == 1){
//				
//				app.getBackend().getOperationTemplateParams(opAr[0].id, opAr[0].serviceType, $.proxy(function(data) {
//				
//					opAr[0].parameters = data;
//					this.serviceDef.operation = opAr[0];
//					app.getView().addShapeService(this.serviceDef, this.x, this.y);
//					
//				}, this));														
//			}
//			
//			this.modalWin.modal('hide');				
//		}		
		_onUseTemplate : function(operationId, templateId) {

			var opAr = $.grep(this.operations, function(op){
				return op.id == operationId;
			});

			if (opAr.length == 1){
				
				app.getBackend().getOperationTemplateParams(templateId, opAr[0].serviceType, $.proxy(function(data) {
				
					opAr[0].parameters = data;
					this.serviceDef.operation = opAr[0];
					app.getView().addShapeService(this.serviceDef, this.x, this.y);
					
				}, this));														
			}
			
			this.modalWin.modal('hide');				
		}			
});