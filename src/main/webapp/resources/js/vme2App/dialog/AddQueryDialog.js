vme2.dialog.AddQueryDialog = Class.extend({

		init : function(currDef, canvas, x, y) {
			//this.serviceDef = currDef;
			this.serviceDef = $.extend(true, {}, currDef);
			this.x = x;
			this.y = y;
			this.canvas = canvas;

			this.modalWin = $('#queryModal');
		},

		show : function() {
			$("#queryModalLabel").text("Query for '"+ this.serviceDef.resourceTitle+ "'");
			$('#queryControls').html('<button id="submit_query" onclick="javascript:queryResource(' + this.serviceDef.resourceId + ')" class="btn btn-primary">View Results</button>');
			$('#queryText').val("");
			$("#queryResults").html("");
			
			// IMPORTANT!!! remove previous handlers!
			this.modalWin.find('#queryModalPrimary').off('click');
			//$('#queryModalPrimary').val("Ok");
			this.modalWin.find('#queryModalPrimary').on('click', $.proxy(function(e) {
				
				var query = $("#queryText").val().trim();
				
				if(query == "")
				{
					alert("Please enter query text!");
					return;
				}
				
				this.serviceDef.query = query;
				
				this._onOk();
			},this));

			//this.modalWin.modal();
			showQuery(this.serviceDef.resourceId,"");
		},

		_onOk : function() {
			
			app.getView().addShapeOpenData(this.serviceDef, this.x, this.y);
			
			this.modalWin.closeModal();				
		}			
});