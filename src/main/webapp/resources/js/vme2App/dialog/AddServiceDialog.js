vme2.dialog.AddServiceDialog = Class.extend({
	_template : 
		'	<table id="operationList" class="table table-striped table-hover">'+
		'		<thead><tr><th>Category</th><th>Operation</th><th></th><th>Guide</th></tr></thead>'+
		'		<tbody>'+
		'		{{#each this}}'+
		'			<tr data-id="{{id}}">'+
		'				<td>{{tag}}</td>'+
		'				<td>'+
		'				{{#if authenticationRequired }}'+
		'					<span class="label label-important"><i class="icon-lock"></i></span>&nbsp;'+
		'				{{else}}'+
		'					<span class="label label-success"><i class="icon-unlock"></i></span>&nbsp;'+				
		'				{{/if}}'+
		'				{{name}}'+
		'				</td>'+
		'				<td>'+	
//		'				{{#if templates}}'+
//		'				<div class="input-append" style="margin-bottom: auto">'+
//		'					<select>'+
//		'						{{#each templates}}'+
//		'						<option value="{{templateId}}">{{name}}</option>'+
//		'						{{/each}}'+
//		'					</select>'+		
//		'					<button type="button" data-usetemplate="true" class="btn btn-default btn-lg"><span class="text-warning icon-circle-arrow-right"/></button>'+
//		'				</div>'+
//		'				{{/if}}'+
		'				</td>'+
		'				<td>'+
		'					<div class="text-center">'+
		'						<a href="#" class="docPop" rel="popover" data-trigger="hover" data-placement="left" data-content="{{doc}}" data-original-title="{{name}}"><i class="icon-book"></i></a>'+
		'					</div>'+
		'				</td>'+			
		'			</tr>'+
		'			{{#if templates}}'+
		'				{{#each templates}}'+		
		'				<tr>'+
		'					<td/>'+
		'					<td>'+
		'						<span class="label label-warning">T</span>&nbsp;'+		
		'						{{name}}'+			
		'					</td>'+
		'					<td class="nopadding">'+
		'						<button type="button" data-usetemplate="true" data-id="{{../id}}" data-templateid="{{templateId}}" class="btn btn-default btn-small"><span class="text-warning icon-circle-arrow-right"/></button>'+
		'					</td>'+	
		'					<td>'+
		'						<div class="text-center">'+
		'							<a href="#" class="docPop" rel="popover" data-trigger="hover" data-placement="left" data-content="{{description}}" data-original-title="{{name}}"><i class="icon-book"></i></a>'+
		'						</div>'+
		'					</td>'+		
		'				</tr>'+
		'				{{/each}}'+		
		'			{{/if}}'+		
		'		{{/each}}'+
		'		</tbody>'+
		'	</table>',

		init : function(serviceId, canvas, x, y) {
			var service = app.getService(serviceId);
			//clono service
			this.serviceDef = $.extend(true, {}, service);console.log(this.serviceDef);
			this.x = x;
			this.y = y;
			this.canvas = canvas;

			this.compiledTemplate = Handlebars.compile(this._template);
			this.container = $('#operationListSection');
			this.modalWin = $('#myModal');
		},

		show : function() {
			$("#myModalLabel").text("Select "+ this.serviceDef.name+ " operation");
			$('#filterSection').html(
					'<div>'+				
					'	<form class="form-horizontal">'+
					'		<div class="control-group">'+
					'			<label class="control-label" for="operationFilter">Operations filter</label>'+
					'			<div class="controls">'+
					'				<input class="paramInput" class="input-small" placeholder="Enter a word to filter operations" name="operationFilter" id="operationFilter" type="text">'+
					'			</div>'+
					'		</div>'+
					'	</form>'+
			'</div>');
			
			var footer = '<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>'+
			'<button class="btn btn-primary">OK</button>';

			this.modalWin.find('.modal-footer').html(footer);
			this.modalWin.find('.btn-primary').on('click', $.proxy(function(e) {
				e.preventDefault();       
				var row = $(this.container.find("tr.success"));
				var id = row.data("id");                
				this._onOk(id);
			},this)).attr("disabled",true);


			app.getBackend().getOperations(this.serviceDef.id, $.proxy(function(data) {

				this.operations = data;			         			         

				var output = this.compiledTemplate(this.operations);
				this.container.html(output); 

				this.modalWin.modal();

				$(".docPop").popover();

				var self = this;
				this.container.find("tr").dblclick(function(){
					var row = $(this);
					var id = row.data("id");
					self._onOk(id);
				});

				this.container.find("tr").click(function(){
					$this = $(this);
					$this.addClass('success').siblings().removeClass('success');
					self.modalWin.find('.modal-footer').find('.btn-primary').attr("disabled",false);
				});         
				
				this.container.find("button[data-usetemplate='true']").click(function(){
					var operationId = $(this).data("id");
					var templateId = $(this).data("templateid");
					self._onUseTemplate(operationId, templateId);
				});				
				
				$('#operationList').filterTable({ // apply filterTable to all tables on this page
	            inputSelector: '#operationFilter', // use the existing input instead of creating a new one
	            minRows:2
	        });
				
			}, this));
		},

		_onOk : function(operationId) {

			var opAr = $.grep(this.operations, function(op){
				return op.id == operationId;
			});

			if (opAr.length == 1){
				
				app.getBackend().getOperationParams(opAr[0].id, opAr[0].serviceType, $.proxy(function(data) {
				
					opAr[0].parameters = data;
					this.serviceDef.operation = opAr[0];
					app.getView().addShapeService(this.serviceDef, this.x, this.y);
					
				}, this));														
			}
			
			this.modalWin.modal('hide');				
		},
		
//		_onUseTemplate : function(operationId) {
//
//			var opAr = $.grep(this.operations, function(op){
//				return op.id == operationId;
//			});
//
//			if (opAr.length == 1){
//				
//				app.getBackend().getOperationTemplateParams(opAr[0].id, opAr[0].serviceType, $.proxy(function(data) {
//				
//					opAr[0].parameters = data;
//					this.serviceDef.operation = opAr[0];
//					app.getView().addShapeService(this.serviceDef, this.x, this.y);
//					
//				}, this));														
//			}
//			
//			this.modalWin.modal('hide');				
//		}		
		_onUseTemplate : function(operationId, templateId) {

			var opAr = $.grep(this.operations, function(op){
				return op.id == operationId;
			});

			if (opAr.length == 1){
				
				app.getBackend().getOperationTemplateParams(templateId, opAr[0].serviceType, $.proxy(function(data) {
				
					opAr[0].parameters = data;
					this.serviceDef.operation = opAr[0];
					app.getView().addShapeService(this.serviceDef, this.x, this.y);
					
				}, this));														
			}
			
			this.modalWin.modal('hide');				
		}			
});