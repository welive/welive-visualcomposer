
Preview = Class.extend({
	NAME : "vme2.Preview",

	baseURL: '/visualcomposer',

	init:function (baseUrl, projectId){

		this.baseUrl = baseUrl;
		this.projectId = projectId;

		//this.backend = new vme2.backend.Backend(this.baseURL);

		var jsn = null;				

		$('#JSNova').height($(window).height() - 50);

		var settings = {
				htmlContentId: 'htmlContent', 
				cssContentId: 'cssContent',
				//jsContentId: 'jsContent', 
				cssEditListener: $.proxy(this.editListener, this)
				//jsEditListener: $.proxy(this.editListener, this),
		};

		jsn = $("#JSNova").wJSNova(settings);	
		jsn.wJSNova('run');

		this.jsn = jsn;

		$(window).resize($.proxy(function()
				{
			$('#JSNova').height($(window).height() -50);
			this.jsn.wJSNova('resize');
				}, this));

		$(document).ajaxError(this.ajaxErrorHandling);

		$(".saveResources").click($.proxy(function(e){

			this.saveResources();
			$(e.target).attr("disabled", true);

		}, this)).attr("disabled", true); 

		$(".refreshView").click($.proxy(function(e){

			this.refreshView();			

		}, this));


		$(".reloadView").click($.proxy(function(e){

			this.buildInvokeWorkflowWindow();			

		}, this));

		$(".uploadResources").click($.proxy(function(e){

			$('#uploadModal').modal('show');

		}, this));

	},

	ajaxErrorHandling: function( event, jqXHR, settings, exception ) {
		bootbox.alert(jqXHR.responseText);
	},

	editListener : function(){
		$(".saveResources").attr("disabled", false);

		//this.jsn.wJSNova('run');	
	},

	refreshView: function(){		

		this.jsn.wJSNova('runWithJs');	
	},	

	saveResources: function(){
		var css = this.jsn.wJSNova('getCss');
		//var js = this.jsn.wJSNova('getJs');

		var url = this.baseUrl + '/user/project/'+ this.projectId +'/saveresources';

		$.ajax ({
			url: url,
			type: "POST",
			//data: {'stylesheet': css, 'javascript':js}
			data: {'stylesheet': css}
		}).done(function(data) {

			//successCallback();
		});
	},

	removeResource: function(element, resourceId){
		$.ajax ({
			url: this.baseURL+ "/user/project/"+this.projectId+"/removeresource/"+resourceId+"/file",
			type: "GET"                        
		}).done(function() {
			$(element).closest("tr").remove();
			$(".saveResources").attr("disabled", false);
		});	
	},
	addResource: function(event){
		event.preventDefault();
		var form = new FormData(document.getElementById('uploadResourceForm'));
		$.ajax({
			url: this.baseURL+ "/user/project/"+this.projectId+"/addresource/file",
			data: form,
			dataType: 'text',
			processData: false,
			contentType: false,
			type: 'POST',
			success: function (response) {
				var data = jQuery.parseJSON(response);   
				var newrow = $("<tr>")
				.append("<td>"+data.filename+"</td>")
				.append("<td>"+data.contentType+"</td>")
				.append("<td><a href='#' class=\"btn btn-primary\" onclick=\"preview.removeResource(this, "+data.id+")\"><i class=\"icon-trash\"></i> Remove</a></td>")
				.append("</tr>");
				$("#resourceTable tbody").append(newrow);
				$(".saveResources").attr("disabled", false);
			}
		});		
	},
	buildInvokeWorkflowWindow: function(){
		$.ajax({
			url: this.baseURL+ "/user/project/"+this.projectId+"/buildinputform",
			type: 'GET',
			success: function (response) {
				var container = $('#invokeWorkflow');		
				container.find('.modal-body').html(response);

				container.find('.btn-primary').off('click');//Reset di un precedente event bind
				container.find('.btn-primary').on('click', $.proxy(function(e) {
					e.preventDefault();				
					$("#invokeworkflowparams").submit();

				}, this));

				container.modal();
			}
		});		
	}	
});





