/**
 * Vme2 Gadget Utils v1.0 | (c) 2013 Mediterranea Igea srl
 */

function getElement(x) {
	return document.getElementById(x);
}

function showOneSection(toshow) {
	var sections = [ 'main', 'approval', 'waiting', 'error', 'mainform' ];
	for (var i = 0; i < sections.length; ++i) {
		var s = sections[i];
		var el = getElement(s);
		if (el != null) {
			if (s === toshow) {
				el.style.display = "block";
			} else {
				el.style.display = "none";
			}
		}
	}
}

function getSessionId(cookie) {
	var valueStart = cookie.indexOf('=');
	var valueEnd = cookie.indexOf(";", valueStart);
	return cookie.substring(valueStart + 1, valueEnd);
}

var jsessionid = '';
function fetchData(formId, invokeUrl) {
	var params = {};
	var postdata = $('#' + formId).serializeArray();
	var jsonString = JSON.stringify(postdata);
	params[gadgets.io.RequestParameters.CONTENT_TYPE] = gadgets.io.ContentType.TEXT;
	params[gadgets.io.RequestParameters.METHOD] = gadgets.io.MethodType.POST;
	params[gadgets.io.RequestParameters.POST_DATA] = jsonString;

	if (jsessionid != undefined && jsessionid != '') {
		var jsessionparam = "JSESSIONID=" + jsessionid;
		params[gadgets.io.RequestParameters.HEADERS] = {
			"Content-Type" : "text/html",
			"Cookie" : jsessionparam
		};
	}

	gadgets.io.makeRequest(invokeUrl, function(response) {
		if (response.rc === 302) {

			if (jsessionid == '') {
				var cookie = response.headers['set-cookie'][0];
				jsessionid = getSessionId(cookie);
			}

			var onOpen = function() {
				showOneSection('waiting');
			};

			var onClose = function() {
				fetchData(formId, invokeUrl);
			};
			var popup = new gadgets.Popup(response.headers.location[0]
					+ "?userconnector=" + jsessionid, null, onOpen, onClose);
			getElement('personalize').onclick = popup.createOpenerOnClick();
			getElement('approvaldone').onclick = popup.createApprovedOnClick();
			showOneSection('approval');

		} else if (response.data) {

			getElement('main').innerHTML = response.data;
			showOneSection('main');

		} else {
			getElement('error_code').appendChild(
					document.createTextNode(response.oauthError));
			getElement('error_uri').appendChild(
					document.createTextNode(response.oauthErrorUri));
			getElement('error_description').appendChild(
					document.createTextNode(response.oauthErrorText));
			getElement('error_explanation').appendChild(
					document.createTextNode(response.oauthErrorExplanation));
			getElement('error_trace').appendChild(
					document.createTextNode(response.oauthErrorTrace));
			showOneSection('error');
		}
	}, params);

	return false;
}
/*
 * function getHtml(formId, invokeUrl) { var params = {}; var
 * postdata=$('#'+formId).serializeArray(); var jsonString =
 * JSON.stringify(postdata); params[gadgets.io.RequestParameters.CONTENT_TYPE] =
 * gadgets.io.ContentType.TEXT; params[gadgets.io.RequestParameters.METHOD] =
 * gadgets.io.MethodType.POST; params[gadgets.io.RequestParameters.POST_DATA] =
 * jsonString; var url = invokeUrl; gadgets.io.makeRequest(url, response,
 * params); return false; };
 * 
 * function response(obj) { document.getElementById('content_div').innerHTML =
 * obj.text; }
 */