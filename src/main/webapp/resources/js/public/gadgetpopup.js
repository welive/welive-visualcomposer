/**
 * Functions to popup from a gadget

 */

var gadgets = gadgets || {};

gadgets.Popup = function(destination, windowOptions, openCallback, closeCallback) {
	this.destination_ = destination;
	this.windowOptions_ = windowOptions;
	this.openCallback_ = openCallback;
	this.closeCallback_ = closeCallback;
	this.win_ = null;
};

gadgets.Popup.prototype.createOpenerOnClick = function() {
	var self = this;
	return function() {
		self.onClick_();
	};
};

gadgets.Popup.prototype.onClick_ = function() {
	// If a popup blocker blocks the window, we do nothing.  The user will
	// need to approve the popup, then click again to open the window.
	// Note that because we don't call window.open until the user has clicked
	// something the popup blockers *should* let us through.
	this.win_ = window.open(this.destination_, '_blank', this.windowOptions_);
	if (this.win_) {
		// Poll every 100ms to check if the window has been closed
		var self = this;
		var closure = function() {
			self.checkClosed_();
		};
		this.timer_ = window.setInterval(closure, 100);
		this.openCallback_();
	}
	return false;
};

gadgets.Popup.prototype.checkClosed_ = function() {
	if ((!this.win_) || this.win_.closed) {
		this.win_ = null;
		this.handleApproval_();
	}
};

gadgets.Popup.prototype.handleApproval_ = function() {
	if (this.timer_) {
		window.clearInterval(this.timer_);
		this.timer_ = null;
	}
	if (this.win_) {
		this.win_.close();
		this.win_ = null;
	}
	this.closeCallback_();
	return false;
};

gadgets.Popup.prototype.createApprovedOnClick = function() {
	var self = this;
	return function() {
		self.handleApproval_();
	};
};