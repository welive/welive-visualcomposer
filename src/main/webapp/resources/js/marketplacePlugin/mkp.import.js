MarketplaceImport = Class.extend({
	search_template: ''
		+ '<div id="marketplaceImportContainer"'
		+ '<div class="row">'
		+ '	<form id="searchForm" class="col s12">'
		+ '		<div class="row">'
		+ '			<div class="input-field col m8">'
		+ '		    	<i class="material-icons prefix">search</i>'
		+ '				<input name="searchArtefact" id="searchArtefact" type="text" placeholder="' + Message['catalogs.search.filter.placeholder'] + '" style="margin-bottom: 0px" class="input-xxlarge"/>'
		+ '			</div>'
		+ '			<div class="input-field col m4">'
		+ '				<input type="submit" id="submit_search" value="' + Message['catalogs.search.actions.search'] + '" class="btn btn-primary"/>'
		+ '				<input type="submit" id="submit_all" value="' + Message['catalogs.search.actions.view_all'] + '" class="btn btn-primary"/>'
		+ '			</div>'
		+ '		</div>'
		+ '		<div class="row">'
		+ '			<p class="col"><input class="with-gap" name="typeGroup" type="radio" id="typeAll" value="all" checked /><label for="typeAll">' + Message['catalogs.search.filter.options.all'] + '</label></p>'
		+ '			<p class="col"><input class="with-gap" name="typeGroup" type="radio" id="typeService" value="buildingblock" /><label for="typeService">' + Message['catalogs.search.filter.options.services'] + '</label></p>'
		+ '			<p class="col"><input class="with-gap" name="typeGroup" type="radio" id="typeOpendata" value="dataset" /><label for="typeOpendata">' + Message['catalogs.search.filter.options.datasets'] + '</label></p>'
		+ '		</div>'
		+ '	</form>'
		+ '</div>'
		+ '<div id="spinner" class="center" style="display:none">'
		+ '	<div class="preloader-wrapper big active">'
		+ '	  <div class="spinner-layer spinner-red-only">'
		+ '	    <div class="circle-clipper left">'
		+ '	      <div class="circle"></div>'
		+ '	    </div><div class="gap-patch">'
		+ '	      <div class="circle"></div>'
		+ '	    </div><div class="circle-clipper right">'
		+ '	      <div class="circle"></div>'
		+ '	    </div>'
		+ '	  </div>'
		+ '	</div>'
		+ '</div>'
		+ '<ul id="Pagination" class="pagination">'
		+ '</ul>'
		+ '<div style="clear:both"></div>'
		+ '<div class="row" id="artefactsList"></div>'
		+ '</div>'
	,
	
	artefactListElement_template: ''
		+ '<div class="artefactListElement col l{{cardSize}} m6 s12" id="artefactListElement_{{index}}">'				
		+ '<div class="card">'
		+ 	'<div class="card-image waves-effect waves-block waves-light">'
		+ 	  '<img src="{{imageUrl}}">'
		+ 	'</div>'
		+ 	'<div class="card-content">'
		+ 	 '<div class="card-title grey-text text-darken-4 artefact-title"><a target="_blank" href="{{pageUrl}}">{{title}}</a></div>'
		+    '<div class="activator"><i class="material-icons right">more_vert</i></div>'
		+ 	  '<p>{{{ratingStars}}}</p>'
		+ 	'</div>'
		+ 	'<div class="card-reveal">'
		+     '<div class="card-title"><i class="material-icons right">close</i></div>'
		+ 	  '<div class="grey-text text-darken-4"><strong>{{artefact.name}}</strong></div>'
		+ 	  '<p>{{artefact.description}}</p>'
		+ 	'</div>'
		+	'<div class="card-action">'
        +     '<a id="artefact_button_{{index}}" {{#ifeq imported true}}style="display:none"{{/ifeq}} class="artefactImportButton action-section"><i class="material-icons right">add</i> ' + Message['catalogs.search.actions.add_to_catalog'] + '</a>'
		+     '<span id="artefact_imported_{{index}}" class="imported_success" {{#ifeq imported false}}style="display:none"{{/ifeq}}>'
		+       '<a target="_blank" href="{{detailsUrl}}"><i class="material-icons right">pageview</i> View details</a>'
		+     '</span>'      
        +   '</div>'        
		+ '<input type="hidden" id="artefact_name_{{index}}" value="{{artefact.name}}" />'
		+ '<input type="hidden" id="artefact_url_{{index}}" value="{{artefact.interfaceOperation}}" />'
		+ '<input type="hidden" id="artefact_type_{{index}}" value="{{artefact.type}}" />'
		+ '<input type="hidden" id="artefact_linkPage_{{index}}" value="{{pageUrl}}" />'
		+ '<input type="hidden" id="artefact_description_{{index}}" value="{{artefact.description}}" />'
		+ '<input type="hidden" id="artefact_wsmodelId_{{index}}" value="{{artefact.artefactId}}" />'
		+ '<input type="hidden" id="artefact_eId_{{index}}" value="{{artefact.eId}}" />'
		+ '<input type="hidden" id="artefact_metamodel_{{index}}" value="{{escapedMetamodel}}" />'        
		+ '</div>'
		+ '</div>'
	,
	
	init: function(wrapperId, itemsPerPage, cardSize, importCallback)
	{
		this.wrapper = $('#' + wrapperId);
		this.itemsPerPage = itemsPerPage;
		this.cardSize = cardSize;
		this.importCallback = importCallback;
		this.titleMaxLength = 35;
		
		(!('ifeq' in Handlebars.helpers))
		{
			// Helper che effettua comparazioni tra 2 valori passati
			Handlebars.registerHelper('ifeq', function(a, b, options) {
				var fnTrue=options.fn, fnFalse=options.inverse;
				return a == b ? fnTrue(this) : fnFalse(this);
			});
		}
		
		var compiled = Handlebars.compile(this.search_template);
		var output = compiled();
		
		$(this.wrapper).html(output);
		
		this.container = $('#marketplaceImportContainer');
		
		this.searchInit();
	},
	
	searchInit: function()
	{
		$(this.container).find('#searchForm').submit($.proxy(function(e) {	
			
			e.preventDefault();
			
			var keywords = $("#searchArtefact").val().trim();
			
			var btn = $(this.container).find("input[type=submit]:focus").attr('id');
			
			if(keywords == "" && btn != "submit_all")
			{
				var toastContent = $('<span class="toastError">Please enter a keyword</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
				
				return;
			}
			
			$("#spinner").show();
			
			var items_per_page = this.itemsPerPage;
			
			var s = "/search/" + keywords;
			
			var searchType = $(this.container).find("input[name='typeGroup']:checked").val();
			
			if(btn == "submit_all")
				s = "/all";
			
			$("#artefactsList").html("");
			$("#Pagination").html("");
			
			var _this = this;
			
			$.getJSON(Settings.serviceCatalog.search.searchUrl + '/types/' + searchType + s, $.proxy(function(data)
			{
				console.log(data);
				
				$("#spinner").hide();
				
				$("#data_length").val(data.length);
				
				if(data.length > 0)
				{					
					var artefactListContent = "";
					
					var flag = true;
						
					// iterate through results
					$.each(data, function(i, artefact)
					{
						var templateData = {};
						templateData.artefact = artefact;
						templateData.escapedMetamodel = escape(JSON.stringify(artefact.metamodel));
						templateData.index = i;
						templateData.cardSize = _this.cardSize;
						templateData.imported = false;
						templateData.detailsUrl = "";
						
						// artefact already in catalog
						if(artefact.serviceCatalogId != "null")
						{
							templateData.imported = true;
							
							if(artefact.type == "ODATA")
								templateData.detailsUrl += Settings.baseUrl + Settings.opendataCatalog.baseUrl + '/' + artefact.serviceCatalogId + '/details';
							else
								templateData.detailsUrl += Settings.baseUrl + Settings.serviceCatalog.baseUrl + '/' + artefact.serviceCatalogId + '/details';
						}
						
						templateData.imageUrl = artefact.linkImage;
						
						if(templateData.imageUrl == "")
						{
							if(artefact.type == "ODATA")
								templateData.imageUrl = Settings.external.marketplace.baseUrl + "/MarketplaceStore-portlet/icons/icone_dataset-01.png";
							else if(artefact.type == "REST")
								templateData.imageUrl = Settings.external.marketplace.baseUrl + "/MarketplaceStore-portlet/icons/icone_servizi_rest-01.png";
							else
								templateData.imageUrl = Settings.external.marketplace.baseUrl + "/MarketplaceStore-portlet/icons/icone_servizi_soap-01.png";
						}
						
						templateData.pageUrl = artefact.link;
						
						var rating = "";
						for(j = 0; j < 5; j++)
						{
							if(j < artefact.rating)
								rating += '<i class="material-icons prefix">star</i>';
							else
								rating += '<i class="material-icons prefix">star_border</i>';
						}
						
						templateData.ratingStars = rating;
						
						templateData.title = artefact.name;
						
						if(templateData.title.length > _this.titleMaxLength)
							templateData.title = templateData.title.substr(0, _this.titleMaxLength - 4) + " ..."
							
						var compiled = Handlebars.compile(_this.artefactListElement_template);
						var output = compiled(templateData);
						
						artefactListContent += output;
							
						if(i == $("#data_length").val() - 1)
						{
							$("#artefactsList").append(artefactListContent);
							
							var __this = _this;
							
							$(".artefactImportButton").off("click");
							$(".artefactImportButton").click($.proxy(function(event) {

								var target = $(event.target);
								
								if(target.is("i"))
									target = target.parent();
								
								var index = parseInt(target.attr("id").replace("artefact_button_", ""));
								
								__this.importArtefact(index);
									
							}, __this));
						}
								
					}); // end iterate through results
					
					$("#Pagination").pagination($("#data_length").val(), {
						items_per_page: items_per_page, 
						callback: _this.pageselectCallback});			
				}
				else
				{
					s = s.replace("/search/", "");
					$("#artefactsList").append('<div class="row"><h5>' + Message['catalogs.search.messages.no_results'] + ' "' + s + '"</h5></div>');
				}
			}, _this));
			
		}, this));
	},
	
	importArtefact: function(i)
	{
		var artType = $("#artefact_type_" + i).val();
		
		var artefact = {
	      "name" : $("#artefact_name_" + i).val(),
	      "url" : $("#artefact_url_" + i).val(),
	      "type" : artType,
	      "linkPage" : $("#artefact_linkPage_" + i).val(),
	      "description" : $("#artefact_description_" + i).val(),
	      "wsmodelId" : $("#artefact_wsmodelId_" + i).val(),
	      "eId" : $("#artefact_eId_" + i).val()
	   }
		
		if($("#artefact_metamodel_" + i).val() != "undefined")
			artefact.metamodel = JSON.parse(unescape($("#artefact_metamodel_" + i).val()));
		
		$.ajax({
			type: "POST",
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			url: Settings.serviceCatalog.search.importUrl,
			data: JSON.stringify(artefact), // Note it is important
			beforeSend: function()
			{
				$("#artefact_button_" + i).fadeOut("slow");
			},
			success : $.proxy(function(result)
			{
				var details = "";
				
				if(result.artid != "null")
				{
					details = "";
					
					if(result.type == "opendata")
						details += '<a class="action-section" target="_blank" href="' + Settings.baseUrl + Settings.opendataCatalog.baseUrl + '/' + result.artid + '/details"><i class="material-icons right">pageview</i>' + result.message + '</a>';
					else
						details += '<a class="action-section" target="_blank" href="' + Settings.baseUrl + Settings.serviceCatalog.baseUrl + '/' + result.artid + '/details"><i class="material-icons right">pageview</i>' + result.message + '</a>';
					
					$("#artefact_imported_" + i).addClass("imported_success");
					
					if(this.importCallback != null)
						this.importCallback(result);
				}
				else
				{
					details = "<i class=\"icon-exclamation\"></i>&nbsp;<b>" + result.message + "</b>";
					$("#artefact_imported_" + i).addClass("imported_error");
				}
				
				$("#artefact_button_" + i).promise().done(function(){
					$("#artefact_imported_" + i).html(details).fadeIn("slow");
					//$("#artefact_imported_" + i).html(details).show("slide", { direction: "left" }, 1000);
				});
			}, this)
		});
	},
	
	pageselectCallback: function(page_index, jq)
	{
		$(".artefactListElement").hide();
		$(".listPage").hide();
		$(".listPage_" + page_index).show();
		
		var items_per_page = this.items_per_page;
		var data_length = $("#data_length").val();
	    // Get number of elements per pagionation page from form
	    var max_elem = Math.min((page_index+1) * items_per_page, data_length);
	    // Iterate through a selection of the content and build an HTML string
	    for(var i = page_index * items_per_page; i < max_elem; i++)
	    {
	        $("#artefactListElement_" + i).show();
	    }

	    // Prevent click eventpropagation
	    return false;
	}
});