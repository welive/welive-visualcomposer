define([], function(){
	vme2.shape.Operator.filter = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.filter",

		initOperator: function() {

			var userData = this.getOperatorDef();

			var parameterProperty = $('#operatorParameterContainer').find('[name*="parameters[1][name]"]')[0];
			var parameterOperator = $('#operatorParameterContainer').find('[name*="parameters[2][name]"]')[0];
			var parameterValue = $('#operatorParameterContainer').find('[name*="parameters[3][name]"]')[0];

			if(userData.sourceProperties && userData.sourceProperties.length > 0)
			{
				this.editCondition = true;

				var propertySelect = $('select[name$="parameters[1][property]"]');
				var operatorSelect = $('select[name$="parameters[2][property]"]');

				$(propertySelect).empty();

				$(propertySelect).append('<option value="" disabled selected>Choose your option</option>');

				for(var i = 0; i < userData.sourceProperties.length; i++)
				{
					$(propertySelect).append("<option>" + userData.sourceProperties[i] + "</option>");
				}

				$(propertySelect).val($(propertySelect).data("selectedvalue"));
				$(operatorSelect).val($(operatorSelect).data("selectedvalue"));

				$('select').material_select();
			}
			else
			{
				this.editCondition = false;

				$(parameterProperty).parent().parent().hide();
				$(parameterOperator).parent().parent().hide();
				$(parameterValue).parent().parent().hide();
			}
		},

		propertyPopulate : function() {

			var propertySelect = $('select[name$="parameters[1][property]"]');

			var arrayPath = $(event.target).closest("form").find('input[data-sourceexp="true"]').val();

			if(arrayPath !== null && arrayPath !== undefined && arrayPath !== '')
			{
				$(propertySelect).empty();

				$(propertySelect).append('<option value="" disabled selected>Choose your option</option>');

				var userData = this.getOperatorDef();

				userData.sourceProperties = [];

				this.getAvailableProperty(arrayPath, $.proxy(function(data)
				{
					for(key in data)
					{
						var nestedData = data[key];

						if(!Array.isArray(nestedData))
						{
							if(typeof nestedData == 'object')
							{
								for(nestedKey in nestedData)
								{
									userData.sourceProperties.push(key + "." + nestedKey);

									$(propertySelect).append("<option>" + key + "." + nestedKey + "</option>");
								}
							}
							else
							{
								userData.sourceProperties.push(key);

								$(propertySelect).append("<option>" + key + "</option>");
							}
						}
					}

					$(propertySelect).val($(propertySelect).data("selectedvalue"));

					$('select').material_select();
				}, this));

				$('select').material_select();
			}
			else
			{
				return false;
			}
		},

		isValid : function() {

			if(this.editCondition)
			{
				var parameterProperty = $('#operatorParameterContainer').find('[name*="parameters[1][property]"]')[0];
				var parameterValue = $('#operatorParameterContainer').find('[name*="parameters[3][property]"]')[0];

				if(!$(parameterProperty).val() || $(parameterProperty).val().trim() == '')
				{
					var toastContent = $('<span class="toastError">You must select a "Property"</span>');
					Materialize.toast(toastContent, 2000);
					$(".toastError").parent().addClass("toast-error");

					return false;
				}

				if(!$(parameterValue).val() || $(parameterValue).val().trim() == '')
				{
					var toastContent = $('<span class="toastError">You must specify a "Value"</span>');
					Materialize.toast(toastContent, 2000);
					$(".toastError").parent().addClass("toast-error");

					return false;
				}
			}
			
			var arrayPath = $('#operatorParameterContainer').find('[name*="parameters[0][property]"]')[0].value;

			if(arrayPath == null || arrayPath == undefined || arrayPath == '')
			{
				var toastContent = $('<span class="toastError">You must specify a "Source array"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");

				return false;
			}

			return true;
		},

		clean : function() {

			this.editCondition = false;
		}

	});

	return vme2.shape.Operator.filter;
});
