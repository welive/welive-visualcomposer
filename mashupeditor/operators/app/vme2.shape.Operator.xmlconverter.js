define([], function(){
	vme2.shape.Operator.xmlconverter = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.xmlconverter",
		
		initOperator : function() {},
		
		isValid : function () {
			
			var stringPath = $('#operatorParameterContainer').find('[name*="parameters[0][property]"]')[0].value;

			if(stringPath == null || stringPath == undefined || stringPath == '')
			{
				var toastContent = $('<span class="toastError">You must specify "XML"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");

				return false;
			}
			
			return true;
		},
		
		clean: function() {}
	});
	
	return vme2.shape.Operator.xmlconverter;
});