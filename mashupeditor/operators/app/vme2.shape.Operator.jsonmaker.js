define([], function(){
	vme2.shape.Operator.jsonmaker = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.jsonmaker",
		
		initOperator : function() {},
		
		addNewField : function(event) {
			event.preventDefault();			
			
			var customParamIndex = app.getOperatorParameterFormSize();
			
			var nameField = $('<label class="control-label">Name</label><div class="controls"><input type="hidden" name="parameters['+customParamIndex+'][name]" value="name"><input class="paramInput" name="parameters['+customParamIndex+'][property]" type="text"></div>');
			customParamIndex++;
			var valueField = $('<label class="control-label">Value</label><div class="controls"><input type="hidden" name="parameters['+customParamIndex+'][name]" value="value"><input class="paramInput jstree-drop" data-sourceexp="true" name="parameters['+customParamIndex+'][property]" type="text"></div>');
			
			$("#operatorParameterContainer").append($("<div class='control-group'>").append(nameField).append(valueField));			
		},
		
		isValid : function () {
			
			var customParamIndex = app.getOperatorParameterFormSize();
			
			if(customParamIndex == 0)
			{
				var toastContent = $('<span class="toastError">You must add a new property</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
					
				return false;
			}
			
			var objPath;
			
			for(var i = 0; i < customParamIndex; i++)
			{
				objPath = $('#operatorParameterContainer').find('[name*="parameters[' + i + '][property]"]')[0].value;
				
				if(objPath == null || objPath == undefined || objPath == '')
				{
					if(i % 2 == 0)
					{
						var toastContent = $('<span class="toastError">You must specify a property name</span>');
						Materialize.toast(toastContent, 2000);
						$(".toastError").parent().addClass("toast-error");

						return false;
					}
					else
					{
						var toastContent = $('<span class="toastError">You must specify a property value</span>');
						Materialize.toast(toastContent, 2000);
						$(".toastError").parent().addClass("toast-error");

						return false;
					}
				}
			}
			
			return true;
		},
		
		clean: function() {}
	});
	
	return vme2.shape.Operator.jsonmaker;
});