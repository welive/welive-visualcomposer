define([], function(){
	vme2.shape.Operator.fuse = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.fuse",
		
		initOperator : function() {},
		
		addNewObject : function(event) {
			event.preventDefault();			
			
			var customParamIndex = app.getOperatorParameterFormSize();
			var index = customParamIndex + 1;
			
			var objectField = $('<label class="control-label">Object #' + index + '</label><div class="controls"><input type="hidden" name="parameters['+customParamIndex+'][name]" value="Object #'+index+'"><input class="paramInput jstree-drop" data-sourceexp="true" name="parameters['+customParamIndex+'][property]" type="text"></div>');
			
			$("#operatorParameterContainer").append($("<div class='control-group'>").append(objectField));			
		},
		
		isValid : function () {
			
			var customParamIndex = app.getOperatorParameterFormSize();
			
			if(customParamIndex == 0)
			{
				var toastContent = $('<span class="toastError">You must add a new object</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
					
				return false;
			}
			
			var objPath;
			
			for(var i = 0; i < customParamIndex; i++)
			{
				objPath = $('#operatorParameterContainer').find('[name*="parameters[' + i + '][property]"]')[0].value;
				
				if(objPath == null || objPath == undefined || objPath == '')
				{
					var toastContent = $('<span class="toastError">You must specify a "Source object"</span>');
					Materialize.toast(toastContent, 2000);
					$(".toastError").parent().addClass("toast-error");

					return false;
				}
			}
			
			return true;
		},
		
		clean: function() {}
	});
	
	return vme2.shape.Operator.fuse;
});
