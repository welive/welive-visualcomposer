define([], function(){
	vme2.shape.Operator.filterobject = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.filterobject",
		
		initOperator: function() {
			
			var userData = this.getOperatorDef();
			
			var customParamIndex = app.getOperatorParameterFormSize();
			var propertySelect;
			var propertyInput;
			var hiddenField;
			var propertyField;
			
			if(userData.sourceProperties && userData.sourceProperties.length > 0)
			{
				for(var i = 1; i < customParamIndex; i++)
				{
					propertyInput = $('input[name$="parameters[' + i + '][property]"]');
					propertySelect = $('<select name="parameters[' + i + '][property]">');

					$(propertySelect).append('<option value="" disabled selected>Choose your option</option>');
					
					for(var j = 0; j < userData.sourceProperties.length; j++)
					{
						$(propertySelect).append("<option>" + userData.sourceProperties[j] + "</option>");
					}
					
					hiddenField = $('<input type="hidden" name="parameters['+i+'][name]" value="condition'+i+'">');

					propertyField = $('<label class="control-label">Property</label>').append($('<div class="controls">').append(propertySelect));
					
					$("#operatorParameterContainer").append($("<div class='control-group'>").append(hiddenField).append(propertyField));

					$(propertySelect).val($(propertyInput).val());
					
					$(propertyInput).parent().parent().remove();
				}
				
				$('select').material_select();
			}
		},

		addNewProperty : function(event) {
			event.preventDefault();

			var customParamIndex = app.getOperatorParameterFormSize();

			var propertySelect = $('<select name="parameters['+customParamIndex+'][property]">');

			var arrayPath = $(event.target).closest("form").find('input[data-sourceexp="true"]').val();
			
			if(arrayPath == null || arrayPath == undefined || arrayPath == '')
			{
				var toastContent = $('<span class="toastError">You must specify a "Source object"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
				
				return;
			}
			
			var userData = this.getOperatorDef();
			
			$(propertySelect).append('<option value="" disabled selected>Choose your option</option>');
			
			for(var i = 0; i < userData.sourceProperties.length; i++)
			{
				$(propertySelect).append("<option>" + userData.sourceProperties[i] + "</option>");
			}

			//$(propertySelect).val($(propertySelect).data("selectedvalue"));
			
			var hiddenField = $('<input type="hidden" name="parameters['+customParamIndex+'][name]" value="condition'+customParamIndex+'">');

			var propertyField = $('<label class="control-label">Property</label>').append($('<div class="controls">').append(propertySelect));
			
			$("#operatorParameterContainer").append($("<div class='control-group'>").append(hiddenField).append(propertyField));

			$('select').material_select();
		},

		buildOperatorSelect : function(selectName){

			var select = $("<select name='"+selectName+"'>");
			$.each(this.getOperands(), function(i, item){
				$(select).append("<option>"+item+"</option>");
			});

			return select;
		},
		
		propertyPopulate : function() {

			var arrayPath = $(event.target).closest("form").find('input[data-sourceexp="true"]').val();

			if(arrayPath !== null && arrayPath !== undefined && arrayPath !== '')
			{
				var userData = this.getOperatorDef();

				userData.sourceProperties = [];

				this.getAvailableProperty(arrayPath, $.proxy(function(data)
				{
					for(key in data)
					{
						userData.sourceProperties.push(key);
					}

				}, this));
			}

		},
		
		isValid: function() {
			
			var _path = $('[name$="parameters[0][property]"]').val();
			
			if(_path == null || _path == undefined || _path == '')
			{
				var toastContent = $('<span class="toastError">You must specify a "Source object"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
				
				return false;
			}
			
			var customParamIndex = app.getOperatorParameterFormSize();
			var propertyValue;
			
			for(var i = 1; i < customParamIndex; i++)
			{
				propertyValue = $('#operatorParameterContainer').find('[name*="parameters[' + i + '][property]"]')[0].value;
				
				if(!propertyValue || propertyValue == '')
				{
					var toastContent = $('<span class="toastError">You must select a "Property"</span>');
					Materialize.toast(toastContent, 2000);
					$(".toastError").parent().addClass("toast-error");
				
				return false;
				}
			}
			
			return true;
		},
		
		clean: function() {
		}
	});

	return vme2.shape.Operator.filterobject;
});
