define([], function(){
	vme2.shape.Operator.stringtemplate = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.stringtemplate",
	
		addCustomFields : function(event) {
			event.preventDefault();			
			
			var customParamIndex = app.getOperatorParameterFormSize();
			
			var nameField = $('<label class="control-label">Name</label><div class="controls"><input type="hidden" name="parameters['+customParamIndex+'][name]" value="name"><input class="paramInput" name="parameters['+customParamIndex+'][property]" type="text"></div>');
			var valueField = $('<label class="control-label">Value</label><div class="controls"><input class="paramInput" name="parameters['+customParamIndex+'][property2]" type="text"></div>');
			
			$("#operatorParameterContainer").append($("<div class='control-group'>").append(nameField).append(valueField));			
		}
	});
	
	return vme2.shape.Operator.stringtemplate;
});
