define([], function(){
	vme2.shape.Operator.joinarray = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.joinarray",

		initOperator : function() {

			var userData = this.getOperatorDef();

			//var arrayPath1 = $("#operatorParameterContainer").closest("form").find('input[name$="parameters[0][property]"]').val();

			//if(arrayPath1 && arrayPath1 !== '')
			if(userData.source1Properties && userData.source1Properties.length > 0)
			{
				//this.property1Populate();

				var propertySelect = $('select[name$="parameters[1][property]"]');

				$(propertySelect).empty();
				$(propertySelect).append('<option value="" disabled selected>Choose your option</option>');

				for(var i = 0; i < userData.source1Properties.length; i++)
				{
					$(propertySelect).append("<option>" + userData.source1Properties[i] + "</option>");
				}

				$(propertySelect).val($(propertySelect).data("selectedvalue"));
			}

			//var arrayPath2 = $("#operatorParameterContainer").closest("form").find('input[name$="parameters[2][property]"]').val();

			//if(arrayPath2 && arrayPath1 !== '')
			if(userData.source2Properties && userData.source2Properties.length > 0)
			{
				//this.property2Populate();

				var propertySelect = $('select[name$="parameters[3][property]"]');

				$(propertySelect).empty();
				$(propertySelect).append('<option value="" disabled selected>Choose your option</option>');

				for(var i = 0; i < userData.source2Properties.length; i++)
				{
					$(propertySelect).append("<option>" + userData.source2Properties[i] + "</option>");
				}

				$(propertySelect).val($(propertySelect).data("selectedvalue"));
			}

			$('select').material_select();
		},

		propertyPopulate : function(event) {

			var id = event.target.id;

			if(id == 'source1')
				this.property1Populate();
			else if(id == 'source2')
				this.property2Populate();
		},

		property1Populate : function() {

			var propertySelect = $('select[name$="parameters[1][property]"]');

			var arrayPath = $("#operatorParameterContainer").closest("form").find('input[name$="parameters[0][property]"]').val();

			if(arrayPath !== null && arrayPath !== undefined && arrayPath !== '')
			{
				$(propertySelect).empty();
				$(propertySelect).append('<option value="" disabled selected>Choose your option</option>');

				var userData = this.getOperatorDef();

				userData.source1Properties = [];

				this.getAvailableProperty(arrayPath, $.proxy(function(data)
				{
					for(key in data)
					{
						userData.source1Properties.push(key);

						$(propertySelect).append("<option>"+key+"</option>");
					}

					$(propertySelect).val($(propertySelect).data("selectedvalue"));

					$('select').material_select();

				}, this));
			}

			$('select').material_select();
		},

		property2Populate : function() {

			var propertySelect = $('select[name$="parameters[3][property]"]');

			$(propertySelect).empty();
			$(propertySelect).append('<option value="" disabled selected>Choose your option</option>');

			var arrayPath = $("#operatorParameterContainer").closest("form").find('input[name$="parameters[2][property]"]').val();

			if(arrayPath!==null && arrayPath!==undefined && arrayPath!=='')
			{
				var userData = this.getOperatorDef();

				userData.source2Properties = [];

				this.getAvailableProperty(arrayPath, $.proxy(function(data)
				{
					for(key in data)
					{
						userData.source2Properties.push(key);

						$(propertySelect).append("<option>"+key+"</option>");
					}

					$(propertySelect).val($(propertySelect).data("selectedvalue"));

					$('select').material_select();
				}, this));
			}

			$('select').material_select();
		},

		isValid : function() {
			
			var arrayPath1 = $("#operatorParameterContainer").closest("form").find('input[name$="parameters[0][property]"]').val();
			var arrayPath2 = $("#operatorParameterContainer").closest("form").find('input[name$="parameters[2][property]"]').val();
			
			var property1 = $("#operatorParameterContainer").closest("form").find('select[name$="parameters[1][property]"]').val();
			var property2 = $("#operatorParameterContainer").closest("form").find('select[name$="parameters[3][property]"]').val();
			
			if(arrayPath1 == "")
			{
				var toastContent = $('<span class="toastError">You must select "Array #1"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");

				return false;
			}
			
			if(arrayPath1 != "")
			{
				if(!property1 || property1.trim() == '')
				{
					var toastContent = $('<span class="toastError">You must select "Field #1"</span>');
					Materialize.toast(toastContent, 2000);
					$(".toastError").parent().addClass("toast-error");

					return false;
				}
			}
			
			if(arrayPath2 != "")
			{
				if(!property2 || property2.trim() == '')
				{
					var toastContent = $('<span class="toastError">You must select "Field #2"</span>');
					Materialize.toast(toastContent, 2000);
					$(".toastError").parent().addClass("toast-error");

					return false;
				}
			}
			
			return true;
		}
	});

	return vme2.shape.Operator.joinarray;
});
