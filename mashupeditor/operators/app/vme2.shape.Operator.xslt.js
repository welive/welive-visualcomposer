define([], function(){
	vme2.shape.Operator.xslt = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.xslt",
	
		uploadXsl : function(event) {
			event.preventDefault();	
			
			this.uploadForm();									
		},
		
		uploadForm : function(){						
			if($("#xslUploadForm").length == 0){
   			var form = $("<form id='xslUploadForm' action='' method='POST' enctype='multipart/form-data'>")
   			.append($('<div class="control-group">')
   				.append($('<label class="control-label">File</label>')
   					.append($('<div class="controls">')
   						.append($('<input type="file" name="file" value="" style="width: 230px;">'))
   					)
   				)				
   			).append($('<div class="control-group">')
   				.append($('<div class="controls">')
   					.append($('<button type="submit" class="btn btn-mini btn-primary">Confirm</button>'))
   				)
   			);
   			
   			$(form).on("submit", $.proxy(this.asyncUpload, this));
   			
   			$("#operatorExtraFormContainer").append(form);
			}
		},
		
		asyncUpload: function(event){
			event.preventDefault();
			var form = new FormData(document.getElementById('xslUploadForm'));

			app.getBackend().uploadXsl(form,  $.proxy(this.populateSelect, this));
		},	
		
		getXslFileList : function(){
			app.getBackend().getXslFileList(this.populateSelect);
		},
		
		populateSelect : function(data){			
			var propertySelect = $('select[name$="[property]"]');	
			
			$(propertySelect).empty();
			
			$.each(data, function(i, elem){
				$(propertySelect).append("<option value='"+elem+"'>"+elem+"</option>");
			});			
			
			$(propertySelect).val($(propertySelect).data("selectedvalue"));
		}
	});
	
	return vme2.shape.Operator.xslt;
});
