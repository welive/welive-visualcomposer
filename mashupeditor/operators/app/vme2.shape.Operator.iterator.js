define([], function(){
	vme2.shape.Operator.iterator = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.iterator",
		
		init : function(sourceId, targetId) {
			this._super(sourceId, targetId);			
			
			this.createEndPort();			
		},
		
		createEndPort: function(){
			
			this.endPort = this.createPort("output", new draw2d.layout.locator.BottomLocator(this));
			this.endPort.addCssClass("redport");
		},

		initOperator: function() {},
		
		isValid: function() {
			
			var arrayPath = $('#operatorParameterContainer').find('[name*="parameters[0][property]"]')[0].value;

			if(arrayPath == null || arrayPath == undefined || arrayPath == '')
			{
				var toastContent = $('<span class="toastError">You must specify an "Array"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");

				return false;
			}
			
			return true;			
		},
		
		clean: function() {}
	});
	
	return vme2.shape.Operator.iterator;
});
