define([], function(){
	vme2.shape.Operator.branch2 = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.branch2",
		
		initOperator: function() {},
		
		isValid: function() {
			
			var conditionPath = $('#operatorParameterContainer').find('[name*="parameters[0][property]"]')[0].value;

			if(conditionPath == null || conditionPath == undefined || conditionPath == '')
			{
				var toastContent = $('<span class="toastError">You must specify a "Condition"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");

				return false;
			}
			
			return true;
		},
		
		clean: function() {}
	});
	
	return vme2.shape.Operator.branch2;
});
