define([], function(){
	vme2.shape.Operator.unique = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.unique",

		propertyPopulate : function() {								
			
			var propertySelect = $('select[name$="[property]"]');
			$(propertySelect).empty();
			
			var arrayPath = $("#operatorParameterContainer").closest("form").find('input[data-sourceexp="true"]').val();
			if(arrayPath!==null && arrayPath!==undefined && arrayPath!==''){
				this.getAvailableProperty(arrayPath, $.proxy(function(data){
					for(key in data){
						$(propertySelect).append("<option>"+key+"</option>");
					}
					$(propertySelect).val($(propertySelect).data("selectedvalue"));
				}, this));			
			}
		}
	});
	
	return vme2.shape.Operator.unique;
});
