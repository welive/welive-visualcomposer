define([], function(){
	vme2.shape.Operator.pluck = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.pluck",
		
		initOperator : function() {
			
			var propertySelect = $('select[name$="[property]"]');
			
			var userData = this.getOperatorDef();

			if(userData.sourceProperties && userData.sourceProperties.length > 0)
			{
				$(propertySelect).empty();

				$(propertySelect).append('<option value="" disabled selected>Choose your option</option>');

				for(var i = 0; i < userData.sourceProperties.length; i++)
				{
					$(propertySelect).append("<option>" + userData.sourceProperties[i] + "</option>");
				}

				$(propertySelect).val($(propertySelect).data("selectedvalue"));

				$('select').material_select();
			}
		
		},

		propertyPopulate : function() {
			
			var propertySelect = $('select[name$="[property]"]');

			$(propertySelect).empty();
			$(propertySelect).append('<option value="" disabled selected>Choose your option</option>');

			var arrayPath = $("#operatorParameterContainer").closest("form").find('input[data-sourceexp="true"]').val();
			
			if(arrayPath !== null && arrayPath !== undefined && arrayPath !== '')
			{
				var userData = this.getOperatorDef();

				userData.sourceProperties = [];
				
				this.getAvailableProperty(arrayPath, $.proxy(function(data)
				{
					for(key in data)
					{
						userData.sourceProperties.push(key);
						
						$(propertySelect).append("<option>"+key+"</option>");
					}
					
					$(propertySelect).val($(propertySelect).data("selectedvalue"));

					$('select').material_select();
					
				}, this));
			}

			$('select').material_select();
		},

		isValid : function() {
			
			var arrayPath = $('#operatorParameterContainer').find('[name*="parameters[0][property]"]')[0].value;

			if(arrayPath == null || arrayPath == undefined || arrayPath == '')
			{
				var toastContent = $('<span class="toastError">You must specify a "Source array"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");

				return false;
			}
			
			var property = $("#operatorParameterContainer").closest("form").find('select[name$="[property]"]').val();

			if(arrayPath != "")
			{
				if(!property || property.trim() == '')
				{
					var toastContent = $('<span class="toastError">You must select a "Property"</span>');
					Materialize.toast(toastContent, 2000);
					$(".toastError").parent().addClass("toast-error");

					return false;
				}
			}

			return true;
		}
	});

	return vme2.shape.Operator.pluck;
});
