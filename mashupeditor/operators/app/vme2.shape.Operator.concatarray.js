define([], function(){
	vme2.shape.Operator.concatarray = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.concatarray",
		
		initOperator : function() {},
		
		addNewArray : function(event) {
			event.preventDefault();			
			
			var customParamIndex = app.getOperatorParameterFormSize();
			var index = customParamIndex + 1;
			
			var arrayField = $('<label class="control-label">Array #' + index + '</label><div class="controls"><input type="hidden" name="parameters['+customParamIndex+'][name]" value="Array #'+index+'"><input class="paramInput jstree-drop" data-sourceexp="true" name="parameters['+customParamIndex+'][property]" type="text"></div>');
			
			$("#operatorParameterContainer").append($("<div class='control-group'>").append(arrayField));			
		},
		
		isValid : function () {
			
			var customParamIndex = app.getOperatorParameterFormSize();
			
			if(customParamIndex == 0)
			{
				var toastContent = $('<span class="toastError">You must add a new array</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
					
				return false;
			}
			
			var objPath;
			
			for(var i = 0; i < customParamIndex; i++)
			{
				objPath = $('#operatorParameterContainer').find('[name*="parameters[' + i + '][property]"]')[0].value;
				
				if(objPath == null || objPath == undefined || objPath == '')
				{
					var toastContent = $('<span class="toastError">You must specify a "Source array"</span>');
					Materialize.toast(toastContent, 2000);
					$(".toastError").parent().addClass("toast-error");

					return false;
				}
			}
			
			return true;
		},
		
		clean: function() {}
	});
	
	return vme2.shape.Operator.concatarray;
});
