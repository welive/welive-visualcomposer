define([], function(){
	vme2.shape.Operator.formula = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.formula",

		addOperand : function(event) {
			event.preventDefault();

			var customParamIndex = app.getOperatorParameterFormSize();

			var operandField = $('<div class="controls"><input type="hidden" name="parameters['+customParamIndex+'][name]" value="operand"><input class="paramInput jstree-drop" data-sourceexp="true" name="parameters['+customParamIndex+'][property]" type="text"></div>');

			$("#operatorParameterContainer").append($("<div class='control-group'>").append(operandField));

			$('select').material_select();
		},

		addOperator : function(event) {
			event.preventDefault();

			var customParamIndex = app.getOperatorParameterFormSize();

			var operators = '<option value="add">+</option><option value="sub">-</option><option value="mul">*</option><option value="div">/</option><option value="ob">(</option><option value="cb">)</option>';

			var operatorField = $('<div class="controls"><input type="hidden" name="parameters['+customParamIndex+'][name]" value="operator"><select name="parameters['+customParamIndex+'][property]">'+operators+'</select></div>');

			$("#operatorParameterContainer").append($("<div class='control-group'>").append(operatorField));

			$('select').material_select();
		}
	});

	return vme2.shape.Operator.formula;
});
