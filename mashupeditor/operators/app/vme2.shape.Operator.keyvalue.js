define([], function(){
	vme2.shape.Operator.keyvalue = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.keyvalue",
		
		initOperator: function() {
		},
		
		isValid: function() {
			
			var _path = $('[name$="parameters[0][property]"]').val();
			
			if(_path == null || _path == undefined || _path == '')
			{
				var toastContent = $('<span class="toastError">You must specify a "Source object"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");
				
				return false;
			}
			
			return true;
		},
		
		clean: function() {
		}
	});
	
	return vme2.shape.Operator.keyvalue;
});
