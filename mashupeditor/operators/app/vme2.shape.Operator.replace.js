define([], function(){
	vme2.shape.Operator.replace = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.replace",
		
		initOperator : function() {
			$('select').material_select();
		},
		
		isValid : function () {
			
			var stringPath = $('#operatorParameterContainer').find('[name*="parameters[0][property]"]')[0].value;

			if(stringPath == null || stringPath == undefined || stringPath == '')
			{
				var toastContent = $('<span class="toastError">You must specify a "Source string"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");

				return false;
			}
			
			var regExp = $('#operatorParameterContainer').find('[name*="parameters[1][property]"]')[0].value;

			if(regExp == null || regExp == undefined || regExp == '')
			{
				var toastContent = $('<span class="toastError">You must specify a "Regular Expression"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");

				return false;
			}
			
			return true;
		},
		
		clean: function() {}
	});
	
	return vme2.shape.Operator.replace;
});
