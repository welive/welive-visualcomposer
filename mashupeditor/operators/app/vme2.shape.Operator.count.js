define([], function(){
	vme2.shape.Operator.count = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.count",
		
		initOperator : function() {},

		isValid : function() {
			
			var arrayPath = $('#operatorParameterContainer').find('[name*="parameters[0][property]"]')[0].value;

			if(arrayPath == null || arrayPath == undefined || arrayPath == '')
			{
				var toastContent = $('<span class="toastError">You must specify a "Source array"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");

				return false;
			}

			return true;
		},
		
		clean : function() {}
	});
	
	return vme2.shape.Operator.count;
});
