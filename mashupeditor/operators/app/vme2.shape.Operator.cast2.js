define([], function(){
	vme2.shape.Operator.cast2 = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.cast2",
		
		initOperator : function() {},
		
		isValid : function () {
			
			var stringPath = $('#operatorParameterContainer').find('[name*="parameters[0][property]"]')[0].value;

			if(stringPath == null || stringPath == undefined || stringPath == '')
			{
				var toastContent = $('<span class="toastError">You must specify a "Input value to cast"</span>');
				Materialize.toast(toastContent, 2000);
				$(".toastError").parent().addClass("toast-error");

				return false;
			}
			
			return true;
		},
		
		clean: function() {}
	});
	
	return vme2.shape.Operator.cast2;
});
