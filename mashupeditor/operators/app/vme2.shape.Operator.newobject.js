define([], function(){
	vme2.shape.Operator.newobject = vme2.shape.Operator.extend({
		NAME: "vme2.shape.Operator.newobject",
	
		addCustomFields : function(event) {
			event.preventDefault();			
			
			var customParamIndex = app.getOperatorParameterFormSize();
			
			var nameField = $('<label class="control-label">Name</label><div class="controls"><input type="hidden" name="parameters['+customParamIndex+'][name]" value="name"><input class="paramInput" name="parameters['+customParamIndex+'][property]" type="text"></div>');
			customParamIndex++;
			var valueField = $('<label class="control-label">Value</label><div class="controls"><input type="hidden" name="parameters['+customParamIndex+'][name]" value="value"><input class="paramInput" name="parameters['+customParamIndex+'][property]" type="text"></div>');
			
			$("#operatorParameterContainer").append($("<div class='control-group'>").append(nameField).append(valueField));			
		}
	});
	
	return vme2.shape.Operator.newobject;
});