function publish(gprjId) 
{
	/*
	var image = new Image();
	image.src = canvas.toDataURL("image/png");
	return image;*/
	
	var width = $("[data-vme-itemid='vme-gmap']").width();
	var height = $("[data-vme-itemid='vme-gmap']").height();
	var margin = $("[data-vme-itemid='vme-gmap']").css("margin");
	var padding = $("[data-vme-itemid='vme-gmap']").css("padding");
	
	$("[data-vme-itemid='vme-gmap']").after('<img class="gmap_fake" src="http://ivan-pc:8080/visualcomposer/guieditor/palette_component/gmap.jpg" height="'+height+'" width="'+width+'" style="margin:'+margin+';padding:'+padding+';display:none" />');
	
	$("[data-vme-itemid='vme-gmap']").hide();
	$(".gmap_fake").show();
	
	screenshot(gprjId);
}

function screenshot(gprjId)
{
	html2canvas($("#mainContainer"), {
		onrendered: function(canvas) 
		{			  			
			$("#screenshotContainer").html(canvas);
			
			/*
			var link = document.getElementById("downloadImage");
			
			link.href = document.getElementsByTagName("canvas")[0].toDataURL("image/png");
			
			link.download = "screenshot.png";
			
			link.click();
			*/
			
			$(".gmap_fake").hide();
			$("[data-vme-itemid='vme-gmap']").show();
			$(".gmap_fake").remove();
			
			var s;
			var screenshotPageUrl;
			
			var screenshot = document.getElementsByTagName("canvas")[0].toDataURL("image/png").replace(/^data:image\/(png|jpeg);base64,/, "");
			
			var data = {
						"encodedImage" : screenshot,
						"imageType" : "image/png",
						"description" : document.getElementById("screenshotDescription").value
					}
			
			$.ajax({
	            type: 'POST',
				url: '/visualcomposer/user/gui/'+gprjId+'/publishScreenshot',
				contentType : 'application/json; charset=utf-8',
				dataType : 'json',
	            data: JSON.stringify(data),
	            async: false,
				success: function(result)
				{
					if(result.success == true)
					{
						s = true;
						screenshotPageUrl = result.screenshotPageUrl;
					}
					else
						s = false;
				}
			});
			
			//$("[data-vme-itemid='vme-gmap']").html(tmp);
			
			if(s == true)
			{
				alert("Screenshot successfully published");
				window.open(screenshotPageUrl);
			}
			else
				alert("Sorry, an error occurred");
		}
	});
}