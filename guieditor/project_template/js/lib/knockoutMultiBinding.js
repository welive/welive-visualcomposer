define(['knockout'], function(ko){
    ko.multiBindingProvider = function() {
      
      var PREFIX = 'data-bind-', 
        ORIGINAL_PREFIX = 'data-bind';
        
      var defaultBindingProvider = new ko.bindingProvider();

      function getBindingsString(node) {
        if (!node.attributes) {
          return null;
        }
        var bindings = [];
        for (var i = 0; i < node.attributes.length; i++) {
          var attr = node.attributes[i];
          var PREFIX = 'data-bind-';
          if (attr.name.indexOf(PREFIX) === 0) {
            bindings.push(attr.name.substr(PREFIX.length) + ' : ' + attr.value);
          }
        }
        if (bindings.length > 0) {
          if (node.attributes[ORIGINAL_PREFIX]) {
            throw new Error('Don\'t mix data-bind-* attributes with a data-bind attribute.');
          }
          return bindings.join(', ');
        }
        return null;
      }

      this.nodeHasBindings = function(node) {
        return !!getBindingsString(node) || defaultBindingProvider.nodeHasBindings(node);
      };
      
      this.getBindings = function(node, bindingContext) {
          var bindingsString = getBindingsString(node, bindingContext) || defaultBindingProvider.getBindingsString(node, bindingContext);                              
          return bindingsString ? defaultBindingProvider.parseBindingsString(bindingsString, bindingContext, node) : null;
      };

      this.getBindingAccessors = function(node, bindingContext) {
          var bindingsString = getBindingsString(node, bindingContext) || defaultBindingProvider.getBindingsString(node, bindingContext);
          return bindingsString ? defaultBindingProvider.parseBindingsString(bindingsString, bindingContext, node, {'valueAccessors':true}) : null;
      };
    };        
    
    ko.bindingProvider.instance = new ko.multiBindingProvider();
    
    return ko;
})
