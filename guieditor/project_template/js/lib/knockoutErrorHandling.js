define(['knockout'], function(ko){
    ko.errorHandlingProvider = function() {      
		var original = new ko.bindingProvider(); 

		//determine if an element has any bindings
		this.nodeHasBindings = original.nodeHasBindings;

		//return the bindings given a node and the bindingContext
		this.getBindings = function(node, bindingContext) {
			var result;
			try {
				result = original.getBindings(node, bindingContext);
			}
			catch (e) {
				if (console && console.log) {
					console.log("Error in binding: " + e.message);   
				}
			}

			return result;
		};	
    };        
    
    ko.bindingProvider.instance = new ko.errorHandlingProvider();
    
    return ko;
})
