requirejs.config({
    baseUrl: 'https://dev.welive.eu/visualcomposer/guieditor/project_template/js/lib',
    paths: {
        app: 'https://dev.welive.eu/visualcomposer/guieditor/palette_component/app',
        customHandlers: 'https://dev.welive.eu/visualcomposer/guieditor/palette_component/customBindingHandlers',
		jquery : '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery',
		jqueryui: '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui',			
		hammerjs: 'https://dev.welive.eu/visualcomposer/guieditor/project_template/js/lib/hammer',
		materialize: 'https://dev.welive.eu/visualcomposer/resources/js/materialize/js/materialize',
		knockout: '//cdnjs.cloudflare.com/ajax/libs/knockout/3.1.0/knockout-min',
		applib: 'https://dev.welive.eu/visualcomposer/guieditor/palette_component/libs'
    },
    shim: {
		'hammerjs':{deps: ['jquery', 'jqueryui'], exports: 'Hammer'},
		'materialize':{deps: ['jquery', 'jqueryui', 'hammerjs']},
        'jqueryui': {deps: ['jquery']}
    },
    deps: ['knockout', 'knockout.mapping', 'knockoutMultiBinding'],  	
    callback: function (ko, mapping) {
		ko.mapping = mapping;				
	}
});

requirejs(['jquery', 'knockout', 'domReady', 'knockout.mapping'], function ($, ko, domReady, ko_mapping) 
{
	handlers = {};

	domReady(function () {	  		
	
		require(["materialize"], function(Materialize){
				var selects = $('select').removeClass('browser-default');
				
				$('select').material_select();
			}
		);
				
		$("head").append("<style>#waiting_layer{z-index: 1000;position: fixed;top: 0;left: 0;width: 100%;height: 100%;background: rgba(255,255,255,0.8);}#waiting_layer > div{position: relative;width: 100%;text-align: center;}#waiting_layer h5{text-align: center;}#waiting_layer .progress{width: 50% !important;}</style>");
		
		require(["app/databinding"], function(db){
			handlers['databinding'] = db;
			
			var itemToRender = $("[data-vmert-onrender]");

			$.each(itemToRender, function(i, item){
				var onrenderevent=$(item).data("vmert-onrender");
				var module = onrenderevent.substring(0, onrenderevent.indexOf("."));console.log(module);
				var method = onrenderevent.substring(onrenderevent.indexOf(".")+1);
				
				var app = "";
			
				if(module == "form_oam")
					app = "_app";
				
				require(["app/"+module+app], function(a){
						a[method](item);
						handlers[module] = a;
					}
				);				
			});
		});									
	});
	
	var json = {};
	
	viewModel = ko_mapping.fromJS(json);
	
	// apply the view-model using KnockoutJS as normal
	try
	{
		ko.applyBindings(viewModel);
	}
	catch(e){}
	
	// TESTING
	
	$.each($("[data-bind-foreach]"), function(i, item)
	{
		ko.cleanNode(item);
	});
	
	$.each($("[data-bind-with]"), function(i, item)
	{
		ko.cleanNode(item);
	});
	
	$.each($("[data-bind-tree]"), function(i, item)
	{
		ko.cleanNode(item);
	});
	
	$.each($("[data-bind-text]"), function(i, item)
	{
		ko.cleanNode(item);
	});
	
	$.each($("[data-bind-value]"), function(i, item)
	{
		ko.cleanNode(item);
	});
	
	$.each($("[data-bind-map]"), function(i, item)
	{
		ko.cleanNode(item);
	});
	
	$.each($("[data-bind-attr]"), function(i, item)
	{
		ko.cleanNode(item);
	});
	
	$.each($("[data-bind-datepicker]"), function(i, item)
	{
		ko.cleanNode(item);
	});
	
	$.each($("[data-bind-youtube]"), function(i, item)
	{
		ko.cleanNode(item);
	});
	
	// END
})
