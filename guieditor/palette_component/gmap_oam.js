define(['async!//maps.google.com/maps/api/js?sensor=false', 'applib/gmap3.min'], function(google, gmail, bh) {

	$('head').append('<link rel="stylesheet" type="text/css" href="/vme2/guieditor/palette_component/gmap_patch.css">');

	function initMap(element) {  	   	      
		$(element).gmap3();  	      
	};
  
	function destroyMap(element) {  	  
		$(element).gmap3("destroy");  	      
	};
					
	return {
		initMap:initMap,
		destroyMap:destroyMap
	};
});
