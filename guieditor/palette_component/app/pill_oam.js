define(function() {
	function initTab(element) {
			
			$(element).on('click',' a[data-toggle="pill"]', function(event) {
				event.preventDefault();
				//$(this).tab("show");
				var page=$(this).attr("href").split("#")[1];
				console.log($("li.active:first",element));
				$(this).parents("li").siblings().removeClass("active");	
				//$("li.active:first",element).removeClass("active");	
				$(this).parents("li").addClass("active");		
				$('#'+page,element).siblings().removeClass("active");	
				$('#'+page,element).addClass("active");			
			
			});
			/*$(element).on('click','.remove-tab ', function(event) {
			console.log($(this));
			var tabId=$(this).parents("a").attr("href").split("#")[1];
			$(".nav-pills a[href='#"+tabId+"']", element).parents("li").remove();
					$("."+tabId, element).remove();
			});
			
			$(element).on('click','.add-tab ', function(event) {
					
			});*/
	}
	return {
		hideControlsFn: function(element, context) {

				 $(".add-tab:first", element).toggle();
				 
				$(".vme-pills:first", element).find( ".remove-tab" ).toggle();
		},
		toggleComponentFn : function(element, context) {

				if($(element).attr("class")==="vme-nav-pills"){
					$(".nav-pills:first", element).switchClass( "nav-pills", "nav-tabs", 10 );
					$(element).removeClass("vme-nav-pills");
					$(element).addClass("vme-nav-tabs");
				}else{
					
					$(".nav-tabs:first", element).switchClass( "nav-tabs", "nav-pills", 10 );
					$(element).removeClass("vme-nav-tabs");
					$(element).addClass("vme-nav-pills");
				}
				
		},
		addPageFn : function(element, context) {
		console.log(element);
				var pageName = window.prompt("Enter Page name", "");
				var tabId = window.prompt("Enter Tab Id", "");
				if (pageName) {console.log(pageName);
					var type=$(element).attr("class").split("-")[2];
					$("ul.nav-"+type+":first",element).append("<li data-vme-itemid='vme-li' class='li'><a  data-vme-itemid='vme-a' href='#"+tabId+"' data-toggle='pill'>" + pageName + " <span class='remove-tab glyphicon glyphicon-minus-sign '></span></a></li>");
					$("div.pill-content:first", element).append('<div  role="tabpanel" class="tab-pane pill '+tabId+'" data-vme-itemid='+tabId+' id='+tabId+'  data-vme-type="block" data-vme-dad="sortable" data-vme-sortable="true">'+pageName+' </div>');

					initTab(element);
				}
		},
		removePageFn : function(element, context) {
				

				var tabId = window.prompt("Enter Tab Id", "");
				if(tabId){
					//$("li:eq(" + pageNumber + ")", element).remove();
					$(".nav-pills a[href='#"+tabId+"']", element).parents("li").remove();
					$("."+tabId, element).remove();

				}
		},
		initTab:initTab
			
	};
});
