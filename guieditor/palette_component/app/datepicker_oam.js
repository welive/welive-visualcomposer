define(["css!applib/jquery-ui-css/smoothness/jquery-ui.min.css", 'jqueryui'], function() {	
		
	function initDP(element) {
		$(element).datepicker();														
	};	
		
	function initDPForEdit(element) {  					
		$(element).datepicker();
	};
	
	function destroyDP(element) { 
		$(element).datepicker('destroy');	 	  
	};			
	
	return {
		initDP:initDP,
		destroyDP:destroyDP,
		initDPForEdit:initDPForEdit		
	};
});
