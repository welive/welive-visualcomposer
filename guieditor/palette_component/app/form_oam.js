define([], function() {
	function initForm(element) { 
		if($(element).data("vmert-invokeenabled")){
			
			var data = $(element).data();
			
			var params = "";
			var mparams = "";
			
			var flag = true;
			
			$.each(data, function(key, value)
			{
				if(key.match("^mashup"))
				{
					if(flag)
					{
						mparams += key + "=" + value;
						flag = false;
					}
					else
					{
						mparams += "&" + key + "=" + value;
					}
				}
			});

			if($(element).attr('onloadsubmit')=="true"){
				var event = jQuery.Event( "submit" );
				event.preventDefault();	
				$( element).trigger( event );
				createFetchDataCall(event,mparams);
			}else{
				$(element).on('submit', function(event) {
					event.preventDefault();		
					createFetchDataCall(event,mparams);
				}); 
			}			
		}
	};  	
	function createFetchDataCall(event,mparams){
					var method= $(event.target).attr('method')!=undefined?$(event.target).attr('method'):"POST";
					//console.log('Method: ' + method);
					var url = $(event.target).attr('action');
					var callbackFunction = $(event.target).attr('callbackfunction') == undefined ? "" : $(event.target).attr('callbackfunction');
					var landingPage = $(event.target).attr('landingpage') == undefined ? "" : $(event.target).attr('landingpage');
					var interval=$(event.target).attr('interval');
					params = mparams + ($(event.target).serialize() != "" ? ("&" + $(event.target).serialize()) : "");	

					var idInterval= setInterval(function(){
						if (interval==""||interval==undefined){
							clearInterval(idInterval);
						}
						
						handlers['databinding'].fetchData(url, params, callbackFunction, landingPage);
						
						/* require(["app/databinding"], function(module){
							module.fetchData(url, params, callbackFunction, landingPage)
						});	*/	
				},interval);
}
	
	return {
		initForm:initForm
	};
});
