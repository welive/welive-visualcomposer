define(["module", "css!applib/bootstrap-tree.css"], function($self, $css) {
	// obtain module path
	//var moduleUrl = $self.uri;
	//$css = $css.replace(new RegExp('__DIR__', 'g'), moduleUrl);
	// create style tag and append it into the head
	//$(['<style type="text/css">', $css, '</style>'].join('')).appendTo('head');		
		
	function initTree(element) {  							
		$('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');				
		
		var listItems = $('.tree li:has(ul)');
		
		$.each(listItems, function(i, elem){
			$(elem).addClass('parent_li').attr('role', 'treeitem');
			$(" > span", elem).attr('title', 'Collapse this branch');
			$(" > span", elem).off('click');
			$(" > span", elem).on('click', function (e) {			
				var children = $(this).parent('li.parent_li').find(' > ul > li');
					if (children.is(':visible')) {
						children.hide('fast');
						$(this).attr('title', 'Expand this branch').find(' > i').addClass('glyphicon glyphicon-plus-sign').removeClass('glyphicon glyphicon-minus-sign');
					}
					else {
						children.show('fast');
						$(this).attr('title', 'Collapse this branch').find(' > i').addClass('glyphicon glyphicon-minus-sign').removeClass('glyphicon glyphicon-plus-sign');
					}
					e.stopPropagation();      
				})
		});						
	};	
		
	function initTreeForEdit(element) {  			
		$('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
		$('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch');
		$('.tree').show();	
	};
	
	function showTree(element){
		$(element).show();
	};
	
	function destroyTree(element) {  	  
		$('.tree > ul').removeAttr('role').find('ul').removeAttr('role');
		$('.tree').find('li:has(ul)').removeClass('parent_li').removeAttr('role').find(' > span').removeAttr('title').off('click');
		$('.tree').hide();
	};			
	
	return {
		initTree:initTree,
		destroyTree:destroyTree,
		initTreeForEdit:initTreeForEdit,
		showTree:showTree
	};
});
