define(['knockout', 'customHandlers/gmap_oam_bindhandler', 'customHandlers/tree_oam_bindhandler', 'customHandlers/datepicker_oam_bindhandler', 'customHandlers/youtube_oam_bindhandler'], function(ko) {
	//var viewModel;	
	
	function fetchData(url, params, callbackFunction, landingPage) 
	{	
		if($("#waiting_layer").length == 0)
		{
			$(".container").append('<div id="waiting_layer" class="valign-wrapper" style="display: none;"><div><div class="row"><h5>Please wait...</h5></div><br><div class="progress row" style="margin:auto"><div class="indeterminate"></div></div></div></div>');
		}
	
		$("#waiting_layer").show();
	
		 $.ajax({
			type: "GET",
			dataType: "jsonp",
			url: url,
			data: params,
			success:function(data, status, jqXHR) {
				
				$("#waiting_layer").hide();
				
				if(data.error && data.error == '__MASHUP_EXECUTION_ERROR')
				{
					alert('Sorry, an error occurred. Please try later');
					
					return;
				}
						
				if(data.errorCode!==undefined && data.errorCode == 401)
				{
					window.open(data.redirectUrl, "_black");
					window.focus();
				}
				else
				{
					//if(viewModel===undefined){
						//viewModel = ko.mapping.fromJS(data);
						
						if(viewModel == undefined)
							viewModel = [];
						
						try
						{
							var currKey;
							var currItem;
							
							// per ogni mashup contenuto in data
							$.each(data, function(key, value){
								//console.log("Key: " + key);
								
								currKey = key;
								
								var index = 0;
								
								// se il viewModel per il mashup corrente non e' ancora stato definito
								if(viewModel[key] == undefined)
								{//console.log("New Mapping");
									// creo il viewModel
									viewModel[key] = ko.mapping.fromJS($(data).get(index));
									
									// per ogni elemento in data binding con il mashup corrente								
									
									$.each($("[data-bind-foreach*='"+key+"']"), function(i, item)
									{
										currItem = item;
										// applico il binding all'elemento
										ko.applyBindings(viewModel[key], item);
									});
									
									$.each($("[data-bind-with*='"+key+"']"), function(i, item)
									{
										currItem = item;
										// applico il binding all'elemento
										ko.applyBindings(viewModel[key], item);
									});
									
									$.each($("[data-bind-tree*='"+key+"']"), function(i, item)
									{
										currItem = item;
										// applico il binding all'elemento
										ko.applyBindings(viewModel[key], item);
									});
									
									$.each($("[data-bind-text*='"+key+"']"), function(i, item)
									{
										currItem = item;
										// applico il binding all'elemento
										ko.applyBindings(viewModel[key], item);
									});
									
									$.each($("[data-bind-value*='"+key+"']"), function(i, item)
									{
										currItem = item;
										// applico il binding all'elemento
										ko.applyBindings(viewModel[key], item);
									});
									
									$.each($("[data-bind-map*='"+key+"']"), function(i, item)
									{
										currItem = item;
										// applico il binding all'elemento
										ko.applyBindings(viewModel[key], item);
									});
									
									$.each($("[data-bind-attr*='"+key+"']"), function(i, item)
									{
										currItem = item;
										// applico il binding all'elemento
										ko.applyBindings(viewModel[key], item);
									});
									
									$.each($("[data-bind-datepicker*='"+key+"']"), function(i, item)
									{
										currItem = item;
										// applico il binding all'elemento
										ko.applyBindings(viewModel[key], item);
									});
									
									$.each($("[data-bind-youtube*='"+key+"']"), function(i, item)
									{
										currItem = item;
										// applico il binding all'elemento
										ko.applyBindings(viewModel[key], item);
									});
								}
								else
								{//console.log($(data).get(index));
									// aggiorno il mapping
									ko.mapping.fromJS($(data).get(index), viewModel[key]);
								}
								
								index++;
							});
						}
						catch(e)
						{
							console.log('key', currKey);
							console.log('item', currItem);
							
							alert('Sorry, an error occurred: ' + e);
					
							return;
						}
						
						//ko.applyBindings(viewModel);									
					//}else{
						//ko.mapping.fromJS(data, viewModel);
					//}
					/*
					var undefinedElements = $("[temp]");
					$.each(undefinedElements, function(i, item){
						console.log("rebind: " + $(item).attr("temp"));
						$(item).attr("data-bind-text", $(item).attr("temp"));
						$(item).bind();
					});*/
					
					var afterRender = $("[data-vmert-afterrender]");
					$.each(afterRender, function(i, item){ 
						var onrenderevent=$(item).data("vmert-afterrender");
						var module = onrenderevent.substring(0, onrenderevent.indexOf("."));
						var method = onrenderevent.substring(onrenderevent.indexOf(".")+1);console.log(module);
						
						handlers[module][method](item);
						/*require(["app/"+module], function(a){
								a[method](item);
							}
						);*/		
					});
					
					if(landingPage != "")
						window.location.href=landingPage;
					
					if(callbackFunction != "")
						eval(callbackFunction)();
				}
			},
			error: function (request, status, error) {
				console.log(request.responseText);
				//alert("Error retrieving data from server!\nPlease try again or contact technical support.");
		   }
		});
		
	}		
		
	return {
		fetchData: fetchData
	};
});
