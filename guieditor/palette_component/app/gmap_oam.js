define(['async!https://maps.google.com/maps/api/js?sensor=false', 'applib/gmap3.min'], function(google, gmail, bh) {
	function initMap(element) {  	   	      
		$(element).gmap3();  	      
	};
  
	function destroyMap(element) {  	  
		$(element).gmap3("destroy");  	      
	};
					
	return {
		initMap:initMap,
		destroyMap:destroyMap
	};
});
