define(['knockout', 'customHandlers/gmap_oam_bindhandler', 'customHandlers/tree_oam_bindhandler' ], function(ko) {
	var viewModel;	
		
	function fetchData(url, params) {	  
		 $.ajax({
			type: "GET",
			dataType: "jsonp",
			url: url,
			data: params,
			success:function(data) {  
				console.log("loaded mashup result")
				console.dir(data);							
				
				if(viewModel===undefined){						
					viewModel = ko.mapping.fromJS(data);	
					ko.applyBindings(viewModel);									
				}else{					
					ko.mapping.fromJS(data, viewModel);	
				}
			}	   		 			
		});
	}		
		
	return {
		fetchData: fetchData
	};
});
