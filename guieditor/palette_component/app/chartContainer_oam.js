define(['applib/chartinator'],function() {
	return {
		
		editChartFn : function(element) {
			
			var tableId= $(element).attr('id');
			console.log($(element).attr('createchart'));
			if($(element).attr('createchart')=="true"){
				
				/*var chartContainer=$('<div/>',{
							id: tableId+'_chart',
							'class': 'chart',
							'data-vme-itemid':'vme-chart',
							}).appendTo($(element).parent()).hide();*/
			console.log($(element).siblings(".chart"));				
			console.log($(element).closest(".chart-container").find(".chart"));
			$("div[data-vme-itemid='vme-chart']").click();
			//chartContainer.click();
			}else{
				alert('Check the "Create Chart" checkbox');
			}
		},
		showChart: function(element)
		{console.log(element);
		
		//$(element).show();
		var tableEl=$(element).find('table');
		var tableId= tableEl.attr('id');
		if(tableEl.attr('createchart')=="true"){
			//create the div element containing the chart
			/*var chartContainer=$('<div/>',{
						id: tableId+'_chart',
						'class': 'className'
						}).appendTo(tableEl.parent());*/
			
			var type = $(element).find('.chart').attr("charttype");
			console.log(type);
			var title= $(element).find('.chart').attr("charttitle");
			var legend=$(element).find('.chart').attr("chartlegend");
			var chart1 = $(element).find(".chart").chartinator({

			// The jQuery selector of the HTML table element to extract the data from - String
			// Default: false
			// If unspecified, the element this plugin is applied to must be the HTML table
			tableSel: ".table-chart",

			// The chart type - String
			// Default: 'BarChart'
			// Options: BarChart, PieChart, ColumnChart, Calendar, GeoChart, Table.
			chartType: type,

			// Google Chart Options

			chartOptions: {

                    // Width of chart in pixels - Number
                    // Default: automatic (unspecified)
                    width: null,

                    // Height of chart in pixels - Number
                    // Default: automatic (unspecified)
                    height: 310,

                    chartArea: {
                        left: "10%",
                        top: 40,
                        width: "85%",
                        height: "65%"
                    },

                    // The font size in pixels - Number
                    // Or use css selectors as keywords to assign font sizes from the page
                    // For example: 'body'
                    // Default: false - Use Google Charts defaults
                    //fontSize: 'body',

                    // The font family name. String
                    // Default: body font family
                    fontName: 'Roboto',

                    // Chart Title - String
                    // Default: Table caption.
                    title: title,

                    titleTextStyle: {

                        // The font size in pixels - Number
                        // Or use css selectors as keywords to assign font sizes from the page
                        // For example: 'body'
                        // Default: false - Use Google Charts defaults
                        fontSize: 'h4'
                    },
                    legend: {

                        // Legend position - String
                        // Options: bottom, top, left, right, in, none.
                        // Default: bottom
                        position: legend
                    },

                    // Array of colours
                   /* colors: ['#94ac27', '#3691ff', '#bf5cff'],*/

                    tooltip: {

                        // Shows tooltip with values on hover - String
                        // Options: focus, none.
                        // Default: focus
                        trigger: 'focus'
                    }
                },
			// Show table as well as chart - String
			// Options: 'show', 'hide', 'remove'
			showTable: 'hide'

			});
			
		}

		}
	};
});
