define(function() {
	return {
		selectTBodyFn : function(element, context) {
			//$("tbody", context).click();
			$("tbody", element).click();
		},	
		selectTHeadFn : function(element, context) {			
			$("thead", element).click();
		},			
		addRowFn : function(element) {
			var lastrow = $(element).find("tr:last").clone();
			$(element).find("tr:last").after(lastrow);
		},
		addColumnFn : function(element) {
			var columnName = window.prompt("Enter Column name", "");
			if (columnName) {
				$(element).find("thead tr").append("<th>" + columnName + "</th>");

				$("tbody tr", element).each(function() {
					$("td:last", this).clone().appendTo(this);
				});
			}
		},
		removeRowFn : function(element) {
			var rowNmb = window.prompt("Enter Row number", "");
			if (rowNmb) {
				$("tr:eq(" + rowNmb + ")", element).remove();
			}
		},
		removeColumnFn : function(element) {
			var columnNmb = window.prompt("Enter Column number", "");
			if (columnNmb) {
				var idx = columnNmb - 1;
				$("tr", element).each(function() {
					$("td:eq(" + idx + "), th:eq(" + idx + ")", this).remove();
				});
			}
		},
		hideTableRows: function(element){
			$(element).hide();
		},
		showTableRows: function(element)
		{
			var l = $("tr", element).length;
			var id = $(element).parent().attr("id");
			
			var cont_id = id + "_cont";
			
			//if($("#" + cont_id).is(':visible'))
			//{
			if(l >= 1)
			{
				$("#" + id + "_noresults").hide();
				$("#" + id + "_message").show();
				$(element).parent().show();
				$(element).show();
			}
			else
			{
				//$(element).parent().hide();
				$("#" + id + "_message").hide();
				//$("#" + id + "_noresults").show();
			}
			//}
		}
	};
});
