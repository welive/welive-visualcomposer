define(['async!//maps.google.com/maps/api/js?sensor=false', 'gmap3.min'], function() {
  	  return {
  		  initMap: function(element) {  	  
  			  $(element).gmap3();  	      
  		  },
  		  destroyMap: function(element) {  	  
  			  $(element).gmap3("destroy");  	      
  		  }
  	  };
});
