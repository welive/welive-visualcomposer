define(['knockout'], function (ko){
	ko.bindingHandlers.map = {
		update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
			try{
				var value = valueAccessor();				
				//You need to use the getter on the value accessor to subscribe to the value changes
				var mapObj = ko.mapping.toJS(value);	
				
				var coordpath = $(element).attr('coordpath');
				var infowindowbuilder = $(element).attr('infowindowbuilder') != undefined ? $(element).attr('infowindowbuilder') : "";
				
				var infowindowPrimary = $(element).attr('infowindow-primary') != undefined ? $(element).attr('infowindow-primary') : "";
				var infowindowSecondary = $(element).attr('infowindow-secondary') != undefined ? $(element).attr('infowindow-secondary') : "";
								
				if(coordpath == undefined || coordpath == "")
					coordpath = "";
				else
					coordpath = "." + coordpath;
				
				var markers = [];	
				
				// IVAN
				
				var info = "";
				/*
				if($("#mainMarkerLat") != undefined && $("#mainMarkerLng") != undefined)
				{
					var mainMarkerLat = $("#mainMarkerLat").val();
					var mainMarkerLng = $("#mainMarkerLng").val();
					
					console.log("MainLatLng: " + mainMarkerLat + "," + mainMarkerLng);
				
					if(mainMarkerLat != undefined && mainMarkerLng != undefined && mainMarkerLat != "" && mainMarkerLng != "")
					{
						markers.push({
							latLng: [mainMarkerLat, mainMarkerLng],					
							labelText: "",
							options:{
								icon: "http://maps.google.com/mapfiles/marker_green.png"
							}
						});
					}
					else
					{
						try
						{
							if(mainMarkerAddress != undefined && mainMarkerAddress != "")
							{
								markers.push({
								address: mainMarkerAddress,					
								data: {
									infowindow: '<h5>' + mainMarkerAddress + '</h5>',
									data: elem
								},
								options:{
									icon: "http://maps.google.com/mapfiles/marker_green.png"
								}
								});
							}
						}
						catch(e){console.log(e)}
					}
				}
				*/
				
				//var lat,lng,title,descr,action,action_par,info;
				var lat,lng,info;
				
				if(mapObj.markers!==undefined)
				{
					if(mapObj.markers instanceof Array)
					{							
						$.each(mapObj.markers, function(i, elem){							
								// IVAN
								
								lat = eval("elem" + coordpath + ".lat");
								lng = eval("elem" + coordpath + ".lng");
								
								if(!lat)
								{
									lat = eval("elem" + coordpath + ".latitude");
								}
								
								if(!lng)
								{
									lng = eval("elem" + coordpath + ".longitude");
								}
								
								info = "";
								
								if(infowindowbuilder != "")
								{
									var fn = window[infowindowbuilder];
									
									if(typeof fn === "function")
									{
										var fnparams = [elem];
										info = fn.apply(null, fnparams);
									}
								}
								else
								{
									if(infowindowPrimary != "")
										info += '<h5 style="color:#428bca">' + eval("elem['" + infowindowPrimary + "']") + '</h5>';
									
									if(infowindowSecondary != "")
										info += '<p>' + eval("elem['" + infowindowSecondary + "']") + '</p>';
								}
								
								if(lat != undefined)
								{
									markers.push({
									latLng: [lat, lng],					
									data: {
										infowindow: info,
										data: elem
									}
									});
								}
								else
								{
									if(elem.position != undefined)
									{
										markers.push({
										latLng: [elem.position[0], elem.position[1]],					
										data: {
											infowindow: info,
											data: elem
										}
										});
									}
								}
						});
					} 
					else 
					{
						var el = JSON.parse(eval("mapObj.markers" + coordpath));
						lat = el.lat;
						lng = el.lng;
						
						if(!lat)
						{
							lat = el.latitude;
						}
						
						if(!lng)
						{
							lng = el.longitude;
						}
						
						info = "";
							
						if(infowindowbuilder != "")
						{
							var fn = window[infowindowbuilder];
							
							if(typeof fn === "function")
							{
								var fnparams = [mapObj.markers];
								info = fn.apply(null, fnparams);
							}
						}
						else
						{
							if(infowindowPrimary != "")
								info += '<h5 style="color:#428bca">' + eval("elem['" + infowindowPrimary + "']") + '</h5>';
							
							if(infowindowSecondary != "")
								info += '<p>' + eval("elem['" + infowindowSecondary + "']") + '</p>';
						}
						
						if(lat != undefined)
						{
							markers.push({
							latLng: [lat, lng],					
							data: {
								infowindow: info,
								data: el
							}
							});
						}
						else
						{
							if(mapObj.markers.position != undefined)
							{
								markers.push({
								latLng: [mapObj.markers.position[0], mapObj.markers.position[1]],					
								data: {
									infowindow: info,
									data: el
								}
								});
							}
						}
					}
				}
				var options = {
					map:{
						options:{
							center: markers[0].latLng
						}
					},
					marker:{
						values: markers,
						events:{ // IVAN
							/*
						  mouseover: function(marker, event, context)
						  {
							var map = $(element).gmap3("get"),
							  infowindow = $(element).gmap3({get:{name:"infowindow"}});
							if (infowindow)
							{
							  infowindow.open(map, marker);
							  infowindow.setContent(context.data.infowindow);
							} 
							else 
							{
							  $(element).gmap3({
								infowindow:{
								  anchor:marker, 
								  options:{content: context.data.infowindow}
								}
							  });
							}
						  },*/
						  click: function(marker, event, context)
						  {
							var map = $(element).gmap3("get"), infowindow = $(element).gmap3({get:{name:"infowindow"}});
							
							if (infowindow)
							{
							  infowindow.open(map, marker);
							  infowindow.setContent(context.data.infowindow);
							} 
							else 
							{
							  $(element).gmap3({
								infowindow:{
								  anchor:marker, 
								  options:{content: context.data.infowindow}
								}
							  });
							}
							  
							if(typeof clickOnMarker === 'function')
								clickOnMarker(marker, context.data.data);
						  }/*,
						  mouseout: function()
						  {
							var infowindow = $(element).gmap3({get:{name:"infowindow"}});
							if (infowindow)
							{
							  infowindow.close();
							}
						  }*/
						}
					},
					autofit:{maxZoom: 14}
				};			
				
				$(element).gmap3({clear: {name:["marker"]}});	
				$(element).gmap3(options);
							
			} catch(e){
				if (console && console.log) {
						$(element).gmap3({clear: {name:["marker"]}});
						console.log("Error in binding: " + e.message);   
				}																                          					
			}
		}
	}				
});
