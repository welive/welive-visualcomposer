define(['knockout', "css!applib/datepicker.css", ], function (ko){
	ko.bindingHandlers.datepicker = {
		update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
			try{
				var value = valueAccessor();				
				//You need to use the getter on the value accessor to subscribe to the value changes
				var dateValue = ko.mapping.toJS(value);		
				
				var selectedDates = [];
				var fulldate = new Date(dateValue);
				var date = new Date(fulldate.setHours(0,0,0,0));
				
				selectedDates.push(date.getTime());
				
				var firstDay = (date.getMonth()+1)+"/"+date.getDate()+"/"+date.getFullYear();
				
				$(element).datepicker("destroy");
				$(element).datepicker({
					//numberOfMonths: 3,
					beforeShowDay: function (date) { 										
						if($.inArray(date.getTime(), selectedDates)==0) {							
							return [true, 'datepicker-highlight', ''];
						} else {							
							return [true, '', ''];
						}						
					},					
					defaultDate: firstDay
				});  				
			} catch(e){
				if (console && console.log) {
						console.log("Error in binding: " + e.message);   
				}																                          					
			}
		}
	}				
});
