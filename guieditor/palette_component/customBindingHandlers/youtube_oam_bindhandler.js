define(['knockout'], function (ko){
	ko.bindingHandlers.youtube = {
		update: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {					
			//You need to use the getter on the value accessor to subscribe to the value changes	
			var value = ko.mapping.toJS(valueAccessor());
			try{
				var id = value.substring(value.indexOf("video:")+6)
				
				$(element).attr('src', "http://www.youtube.com/embed/"+id);
				
			} catch(e){
				if (console && console.log) {
						console.log("Error in binding: " + e.message);   
				}																                          					
			}			
		}		
	}				
});
