define(['knockout', "app/tree_oam"], function (ko, module){
	ko.bindingHandlers.tree = {
		init: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {									
			return ko.bindingHandlers.with.init(element, valueAccessor, allBindingsAccessor, viewModel, context);			
		},
		update: function (element, valueAccessor, allBindingsAccessor, viewModel, context) {					
			//You need to use the getter on the value accessor to subscribe to the value changes	
			var value = ko.mapping.toJS(valueAccessor());
			try{
					
				
				module.initTree(element);
				module.showTree(element);				
			} catch(e){
				if (console && console.log) {
						console.log("Error in binding: " + e.message);   
				}																                          					
			}			
		}		
	}				
});
