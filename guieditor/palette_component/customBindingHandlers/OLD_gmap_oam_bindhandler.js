define(['knockout'], function (ko){
	ko.bindingHandlers.map = {
		update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
			try{
				var value = valueAccessor();				
				//You need to use the getter on the value accessor to subscribe to the value changes
				var mapObj = ko.mapping.toJS(value);	
				
				var coordpath = $(element).attr('coordpath');
				var infotitle = $(element).attr('infotitle') != undefined ? $(element).attr('infotitle') : "";
				var infodescr = $(element).attr('infodescr') != undefined ? $(element).attr('infodescr') : "";
				var markeraction = $(element).attr('markeraction') != undefined ? $(element).attr('markeraction') : "";
				var markeractionname = $(element).attr('markeractionname') != undefined ? $(element).attr('markeractionname') : "Action";
				var markeractionpar = $(element).attr('markeractionpar') != undefined ? $(element).attr('markeractionpar') : "";
				
				if(coordpath == undefined || coordpath == "")
					coordpath = "";
				else
					coordpath = "." + coordpath;
				
				var markers = [];	
				
				// IVAN
				
				if($("#mainMarkerLat") != undefined && $("#mainMarkerLng") != undefined)
				{
					var mainMarkerLat = $("#mainMarkerLat").val();
					var mainMarkerLng = $("#mainMarkerLng").val();
					
					console.log("MainLatLng: " + mainMarkerLat + "," + mainMarkerLng);
				
					if(mainMarkerLat != undefined && mainMarkerLng != undefined && mainMarkerLat != "" && mainMarkerLng != "")
					{
						markers.push({
							latLng: [mainMarkerLat, mainMarkerLng],					
							labelText: "",
							options:{
								icon: "http://maps.google.com/mapfiles/marker_green.png"
							}
						});
					}
					else
					{
						try
						{
						if(mainMarkerAddress != undefined && mainMarkerAddress != "")
						{
							markers.push({
							address: mainMarkerAddress,					
							data: '<h5>' + mainMarkerAddress + '</h5>',
							options:{
								icon: "http://maps.google.com/mapfiles/marker_green.png"
							}
							});
						}
						}
						catch(e){console.log(e)}
					}
				}
				
				var lat,lng,title,descr,action,action_par;
				
				if(mapObj.markers!==undefined){
					if(mapObj.markers instanceof Array){							
						$.each(mapObj.markers, function(i, elem){							
								// IVAN
								
								lat = eval("elem" + coordpath + ".lat");
								lng = eval("elem" + coordpath + ".lng");
								
								try
								{
									title = eval("elem." + infotitle);
								}
								catch(e)
								{
									title = "";
								}
								
								try
								{
									descr = eval("elem." + infodescr);
								}
								catch(e)
								{
									descr = "";
								}
								
								try
								{
									action_par = eval("elem." + markeractionpar);
								}
								catch(e)
								{
									action_par = "";
								}
								
								info = "";
								info_ext = "";
								
								if(markeraction != "")
								{
									info_ext = "<button class='markerAction' onclick='javascript:" + markeraction 
										+ "(" + (action_par != "" ? ("\"" + action_par + "\"") : "") + ")'>" + markeractionname + "</button>";
								}
								
								if(title != "")
								{
									info += '<h5>' + title + '</h5>' + '<p>' + descr + '</p>' + info_ext;
								}
								
								if(lat != undefined)
								{
									markers.push({
									latLng: [lat, lng],					
									data: info
									});
								}
								else
								{
									if(elem.position != undefined)
									{
										markers.push({
										latLng: [elem.position[0], elem.position[1]],					
										data: info
										});
									}
								}
						});
					} else {
					
							lat = eval("mapObj.markers" + coordpath + ".lat");
							lng = eval("mapObj.markers" + coordpath + ".lng");
							
							try
							{
								title = eval("mapObj.markers." + infotitle);
							}
							catch(e)
							{
								title = "";
							}
							
							try
							{
								descr = eval("mapObj.markers." + infodescr);
							}
							catch(e)
							{
								descr = "";
							}
							
							info = "";
							
							if(title != "")
							{
								info += '<h5>' + title + '</h5>' + '<p>' + descr + '</p>';
							}
							
							if(lat != undefined)
							{
								markers.push({
								latLng: [lat, lng],					
								data: info
								});
							}
							else
							{
								if(mapObj.markers.position != undefined)
								{
									markers.push({
									latLng: [mapObj.markers.position.position[0], mapObj.markers.position.position[1]],					
									data: info
									});
								}
							}
					}
				}
				var options = {
					map:{
						options:{
							center: markers[0].latLng
						}
					},
					marker:{
						values: markers,
						events:{ // IVAN
						  mouseover: function(marker, event, context)
						  {
							var map = $(element).gmap3("get"),
							  infowindow = $(element).gmap3({get:{name:"infowindow"}});
							if (infowindow)
							{
							  infowindow.open(map, marker);
							  infowindow.setContent(context.data);
							} 
							else 
							{
							  $(element).gmap3({
								infowindow:{
								  anchor:marker, 
								  options:{content: context.data}
								}
							  });
							}
						  }/*,
						  mouseout: function()
						  {
							var infowindow = $(element).gmap3({get:{name:"infowindow"}});
							if (infowindow)
							{
							  infowindow.close();
							}
						  }*/
						}
					},
					autofit:{maxZoom: 14}
				};			
				
				$(element).gmap3({clear: {name:["marker"]}});	
				$(element).gmap3(options);		
							
			} catch(e){
				if (console && console.log) {
						console.log("Error in binding: " + e.message);   
				}																                          					
			}
		}
	}				
});
